﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interface.ViewModel
{
    /// <summary>
    /// لیست افرادی که در کاتابل فرد مجوز دارند
    /// </summary>
    public class MobilePortFolioPersonViewModel
    {
        /// <summary>
        /// شناسه فرد
        /// </summary>
        public string PersonID { get; set; }
        /// <summary>
        /// نام فرد
        /// </summary>
        public string DisplayName { get; set; }
    }
}
