﻿namespace Interface.ViewModel
{
    /// <summary>
    /// ویو مدلی جهت نمایش اطلاعات دوره در موبایل
    /// </summary>
    public class GetWorkPeriodMobileModel
    {
        /// <summary>
        /// شناسه دوره
        /// </summary>
        public int WorkPeriodID { get; set; }
        /// <summary>
        /// نام
        /// </summary>
        public string WorkPeriodName { get; set; }
        /// <summary>
        /// تاریخ شروع دوره
        /// </summary>
        public string StartDate { get; set; }
        /// <summary>
        /// تاریخ پایان دوره
        /// </summary>
        public string EndDate { get; set; }
        /// <summary>
        /// زمان منقضی شدن در سیستم ما استفاده ندارد
        /// </summary>
        public string ExpireDate { get; set; }
        /// <summary>
        /// ایا منقضی شده است میتوانیم در سیستم خود به عنوان بسته بودن دوره بهش نگاه کنیم
        /// </summary>
        public bool IsExpired { get; set; }
        /// <summary>
        /// دوره جاری
        /// </summary>
        public bool IsCurrent { get; set; }
    }
}
