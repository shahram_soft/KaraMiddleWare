﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interface.ViewModel
{
  public  class FormCodeFirstViewModel
    {
        /// <summary>
        /// مدل ذخیره کد
        /// </summary>
        public FormCodeOriginalViewModel Codes { get; set; }
    }
    /// <summary>
    /// ویو مدلی جهت استفاده در موبایل لیست کد ها
    /// </summary>
    public class FormCodeOriginalViewModel
    {
        /// <summary>
        /// مدل ذخیره کد
        /// </summary>
        public List<FormCodeinMobileViewModel> Code { get; set; }
    }
    /// <summary>
    /// ویو مدلی جهت استفاده در موبایل لیست کد ها
    /// </summary>
    public class FormCodeinMobileViewModel
    {
        /// <summary>
        /// شناسه کد
        /// </summary>
        public string Val { get; set; }
        /// <summary>
        /// نام ترجمه شده
        /// </summary>
        public string Title { get; set; }

    }
}
