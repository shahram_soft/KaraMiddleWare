﻿using System.Collections.Generic;

namespace Interface.ViewModel
{
   public class FormFieldsMobileViewModel
    {
        /// <summary>
        /// شناسه فیلد
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// نام 
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// نام نمایشی
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// نوع واحد
        /// </summary>
        public string Type { get; set; }
        /// <summary>
        /// مقدار پیش فرض
        /// </summary>
        public string DefaultValue { get; set; }
        /// <summary>
        /// ایا حتما باید پر شود
        /// </summary>
        public bool Required { get; set; }
        /// <summary>
        /// لیست داده ها در صورتی که کومبو باشد
        /// </summary>
        public List<ComboDataMobileViewModel> Data { get; set; }
        /// <summary>
        /// این فیلد از فیلد مورد اشاره مقدارش حتما باید بزرگتر باشد
        /// </summary>
        public string Greater { get; set; }
        /// <summary>
        /// این فیلد از فیلد مورد نظر مقدارش از فیلد مشخص شده کوچکتر باید باشد
        /// </summary>
        public string Less { get; set; }
        /// <summary>
        /// فیلدی جهت مشخص کردن فیلد تکلمی که در لئوپارد حتما باید مقدار ترو باشد
        /// </summary>
        public bool IsExtended { get; set; }
        /// <summary>
        /// آیا چند انتخابی هست یا خیر
        /// </summary>
        public bool IsMultiSelect { get; set; }
        /// <summary>
        /// ایکون مورد نظر
        /// </summary>
        public string Icon { get; set; }
        /// <summary>
        /// نام وب سرویس
        /// </summary>
        public string ServiceName { get; set; }
        /// <summary>
        /// غیز قابل تغییر در مقدار
        /// </summary>
       public bool NotEditable { get; set; }

    }
   
    /// <summary>
    /// کلاسی جهت ذخیره اطلاعات کومبو ها در موبایل
    /// </summary>
    public class ComboDataMobileViewModel
    {
        /// <summary>
        /// شناسه مقدار
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// نام مقدار
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// آیکون مورد نظر برای نمایش
        /// </summary>
        public string Icon { get; set; }
       

    }
}
