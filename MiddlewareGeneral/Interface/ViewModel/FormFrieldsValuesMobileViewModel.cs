﻿namespace Interface.ViewModel
{
    /// <summary>
    /// ویو مدلی جهت دی رسریالایز کردن جی سان ثبت مجوز
    /// </summary>
   public class FormFrieldsValuesMobileViewModel
    {
        /// <summary>
        /// نام فیلد
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// شناسه فیلد
        /// </summary>
        public string ID { get; set; }
        /// <summary>
        /// مقدار وارد شده توسط کاربر
        /// </summary>
        public string Value { get; set; }
    }
}
