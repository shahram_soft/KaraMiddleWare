﻿using System;
using System.Collections.Generic;

namespace Interface.ViewModel
{
 public   class CreditTypeViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public List<GetCodePermissionMobileViewModel> CodePermissions { get; set; }
    }
    /// <summary>
    /// ویو مدلی جهت ارسال به سرویس موبایل
    /// </summary>
    public class GetCodePermissionMobileViewModel
    {
        /// <summary>
        /// شناسه فرم
        /// </summary>
        public string CodeID { get; set; }
        /// <summary>
        /// نام فرم
        /// </summary>
        public string CodeName { get; set; }
        /// <summary>
        /// ایا روزانه است
        /// </summary>
        public bool Daily { get; set; }
        /// <summary>
        /// ایا ساعتی است
        /// </summary>
        public bool Timely { get; set; }
        /// <summary>
        /// شناسه گروه در سیستم ما مصرفی ندارد 
        /// </summary>
        public int CodeGroupID { get; set; }
        /// <summary>
        /// نام لاتین گروه کددر سیستم ما مصرفی ندارد
        /// </summary>
        public string CodeGroupAcronym { get; set; }
        /// <summary>
        /// نام گروه کد در سیستم ما مصرفی ندارد
        /// </summary>
        public string CodeGroupName { get; set; }
        /// <summary>
        /// ایا کد کسر کار است
        /// </summary>
        public bool Kasr { get; set; }
        /// <summary>
        /// آیا کد مازاد حضور است
        /// </summary>
        public bool Mazad { get; set; }
        /// <summary>
        /// کد هایی که ترکیب کد با کد این فرم دارد
        /// </summary>
        public List<Guid> CombinationOfCodes { get; set; }

    }

}
