﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interface.ViewModel
{
   public class MobileAccessViewModel
    {
        /// <summary>
        /// غیر قابل استفاده در لئوپارد
        /// </summary>
        public int AllPK;
        /// <summary>
        /// غیر قابل استفاده در لئوپارد
        /// </summary>
        public int EPK;
        /// <summary>
        /// غیر قابل استفاده در لئوپارد
        /// </summary>
        public int ErjaForReport;
        /// <summary>
        /// غیر قابل استفاده در لئوپارد
        /// </summary>
        public int ErjaInfo;
        /// <summary>
        /// غیر قابل استفاده در لئوپارد
        /// </summary>
        public int ErjaRequester;
        /// <summary>
        /// غیر قابل استفاده در لئوپارد
        /// </summary>
        public int ErjaStepBefor;
    }
}
