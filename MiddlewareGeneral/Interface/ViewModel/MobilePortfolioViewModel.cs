﻿namespace Interface.ViewModel
{
   public class MobilePortfolioViewModel
    {
        /// <summary>
        /// از تاریخ
        /// </summary>
        public string RequestSDate;
        /// <summary>
        /// تا تاریخ
        /// </summary>
        public string RequestEDate;
        /// <summary>
        /// کد پرسنلی ثبت کننده درخواست
        /// </summary>
        public string RequesterCode;
        /// <summary>
        /// نام درخواست کننده
        /// </summary>
        public string RequesterName;
        /// <summary>
        /// واحد
        /// </summary>
        public string DeptName;
        /// <summary>
        /// شناسه فرم
        /// </summary>
        public string DocTypeId;
        /// <summary>
        /// نام فرم
        /// </summary>
        public string DoctypeName;
        /// <summary>
        /// شرح سند
        /// </summary>
        public string DocDes;
        /// <summary>
        /// شناسه درخواست
        /// </summary>
        public string Docid;
        /// <summary>
        /// شناسه فرد سند در لئوپارد غیر قابل استفاده است
        /// </summary>
        public int DocMemberId;
        /// <summary>
        /// تاریخ ثبت
        /// </summary>
        public string ActionDate;
        /// <summary>
        /// عملیات در لئوپارد غیر قابل استفاده
        /// </summary>
        public int Action;
        /// <summary>
        /// شماره سطر
        /// </summary>
        public int RowNumber;
        /// <summary>
        /// تعداد صفحه
        /// </summary>
        public int TotalPage;
        /// <summary>
        /// شماره صفحه
        /// </summary>
        public int PageNumber;
        /// <summary>
        /// نمایش اولین بار غیر قابل استفده در لئوپارد
        /// </summary>
        public int ViewState;
        /// <summary>
        /// کامنت کاربر در درخواست
        /// </summary>
        public string RequesterDescription;
    }
}
