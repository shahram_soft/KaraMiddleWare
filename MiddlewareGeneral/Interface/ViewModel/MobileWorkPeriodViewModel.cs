﻿namespace Interface.ViewModel
{
  public  class MobileWorkPeriodViewModel
    {
        /// <summary>
        /// شناسه کد
        /// </summary>
        public string CodeID { get; set; }
        /// <summary>
        /// نام کد
        /// </summary>
        public string CodeName { get; set; }
        /// <summary>
        /// کسر کار
        /// </summary>
        public bool KasrKar { get; set; }
        /// <summary>
        /// مازاد کار
        /// </summary>
        public bool MazadKar { get; set; }
        /// <summary>
        /// ساعتی 
        /// </summary>
        public bool Timely { get; set; }
        /// <summary>
        /// روزانه
        /// </summary>
        public bool Daily { get; set; }
        /// <summary>
        /// مقدار
        /// </summary>
        public string Value { get; set; }
        /// <summary>
        /// رنگ
        /// </summary>
        public string Color { get; set; }
        /// <summary>
        /// نیازمند مجوز
        /// </summary>
        public bool CreditNeed { get; set; }
        /// <summary>
        /// نوع مجوز مورد استفاده در درخواست
        /// </summary>
        public string RequestTypeId { get; set; }
    }
}
