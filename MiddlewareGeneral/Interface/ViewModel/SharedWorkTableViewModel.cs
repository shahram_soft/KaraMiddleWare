﻿namespace Interface.ViewModel
{
    public class SharedWorkTableViewModel
    {
        /// <summary>
        /// ای دی پرسنلی فرد
        /// </summary>
        public string ID { get; set; }
        /// <summary>
        /// نام و نام خانوادگی
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// کد پرسنلی فرد
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// در صورت غیبت شخص
        /// </summary>
        public int OwnerAbsent { get; set; }
    }
}
