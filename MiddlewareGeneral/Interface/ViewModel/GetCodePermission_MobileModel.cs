﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interface.ViewModel
{
  public  class GetCodePermission_MobileModel
    {
        public int CodeID { get; set; }
        public string CodeName { get; set; }
        public bool Daily { get; set; }
        public bool Timely { get; set; }
        public int CodeGroupID { get; set; }
        public string CodeGroupAcronym { get; set; }
        public string CodeGroupName { get; set; }
        public bool Kasr { get; set; }
        public bool Mazad { get; set; }
    }
}
