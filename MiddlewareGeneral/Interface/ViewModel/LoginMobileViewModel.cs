﻿using System;

namespace Interface.ViewModel
{
    public class LoginMobileViewModel
    {
        /// <summary>
        /// پیغام
        /// </summary>
        public string Message { get; set; }
        /// <summary>
        /// شناسه فرد
        /// </summary>
        public string PersonID { get; set; }
        /// <summary>
        /// کد پرسنلی
        /// </summary>
        public string PersonCode { get; set; }
        /// <summary>
        /// نام خانوادگی و نام
        /// </summary>
        public string RevPersonName { get; set; }
        /// <summary>
        /// نام و نام خانوادگی
        /// </summary>
        public string PersonName { get; set; }
        /// <summary>
        /// شناسه یوزر
        /// </summary>
        public string UserID { get; set; }
        /// <summary>
        /// سشن مورد نظر
        /// </summary>
        public string SessionID { get; set; }
        /// <summary>
        /// ورود دوباره 
        /// </summary>
        public bool DuplicateAccess { get; set; }
        /// <summary>
        /// کاربر دارای اعتبار ورود
        /// </summary>
        public bool Credit { get; set; }
        /// <summary>
        /// عکس
        /// </summary>
        public string PersonnelPic { get; set; }
        /// <summary>
        /// شرکت
        /// </summary>
        public string CompanyName { get; set; }
        /// <summary>
        /// سمت
        /// </summary>
        public string PositionName { get; set; }
        /// <summary>
        /// نام واحد سازمانی
        /// </summary>
        public string DepartmentName { get; set; }
        /// <summary>
        /// زبان کاربر
        /// </summary>
        public int LocalId { get; set; }
        /// <summary>
        /// زبان کاربر جهت استفاده در لاگین موبایل
        /// </summary>
        public string Language { get; set; }
    }
}
