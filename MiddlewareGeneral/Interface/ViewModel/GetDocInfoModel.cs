﻿using System;

namespace Interface.ViewModel
{
    /// <summary>
    /// نمایش مجوز ها در موبایل
    /// </summary>
    public class GetDocInfoModel
    {
        /// <summary>
        /// نوع مجوز
        /// </summary>
        public string DocTypeNeme { get; set; }
        /// <summary>
        /// تاریخ ثبت
        /// </summary>
        public string DateRecord { get; set; }
        /// <summary>
        /// عنوان مجوز
        /// </summary>
        public string DocTitle { get; set; }
        /// <summary>
        /// تایید کننده
        /// </summary>
        public string Accepter { get; set; }
        /// <summary>
        /// وضعیت
        /// </summary>
        public string StatusID { get; set; }
        /// <summary>
        /// تعداد کل صفحات
        /// </summary>
        public int TotalPage { get; set; }
        /// <summary>
        /// ثبت کننده مجوز
        /// </summary>
        public string ActorDisplayName { get; set; }
        /// <summary>
        /// شرح مچوز
        /// </summary>
        public string DocDescription { get; set; }
        /// <summary>
        /// شناسه مجوز
        /// </summary>
        public string DocID { get; set; }
        /// <summary>
        /// شناسه نوع مجوز
        /// </summary>
        public string DocTypeID { get; set; }
        /// <summary>
        /// تاریخ ذخیره با فرمت تاریخ و ساعت
        /// </summary>
        public DateTime SubmiteSelectedDate { get; set; }
        /// <summary>
        /// قابل ویرایش
        /// </summary>
        public bool Editable { get; set; }
        /// <summary>
        /// قابل ویرایش
        /// </summary>
        public bool Deletable { get; set; }
    }
}
