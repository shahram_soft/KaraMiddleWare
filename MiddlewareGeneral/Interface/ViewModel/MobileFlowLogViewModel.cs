﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interface.ViewModel
{
   public class MobileFlowLogViewModel
    {
        /// <summary>
        /// عنوان وضعیت
        /// </summary>
        public string StateTitle;
        /// <summary>
        /// شناسه وضعیت
        /// </summary>
        public string State;
        /// <summary>
        /// نام درخواست دهنده
        /// </summary>
        public string RPName;
        /// <summary>
        /// نام فرد
        /// </summary>
        public string PersonName;
        /// <summary>
        /// سمت فرد
        /// </summary>
        public string RoleName;
        /// <summary>
        /// تاریخ انجام عملیات
        /// </summary>
        public string ActionDate;
        /// <summary>
        /// ساعت انجام عملیات
        /// </summary>
        public string ActionTime;
        /// <summary>
        /// کامنت فرد 
        /// </summary>
        public string requesterDes;
        /// <summary>
        /// غیر قابل استفاده در لئوپارد
        /// </summary>
        public string Descr;
        /// <summary>
        /// شرح سند 
        /// </summary>
      //  public string Description;
        /// <summary>
        /// عملیات
        /// </summary>
        public string Action;
        /// <summary>
        /// شماره روند
        /// </summary>
      //  public string StepNo;
        /// <summary>
        /// وضعیت موبایل در لئوپارد همان وضعیت های موجوز اورده میشود
        /// </summary>
        public string StateMobile;
        /// <summary>
        /// عنوان وضعیت موبایل در لئوپارد همان نام وضعیت های موجود اورده میشود
        /// </summary>
        public string StateMobileTitle;
    }
}
