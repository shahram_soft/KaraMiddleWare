﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interface.ViewModel
{
   public class DocTypesViewModel
    {
        /// <summary>
        /// ای دی فرم
        /// </summary>
        public string DocTypeID { get; set; }
        /// <summary>
        /// نام فرم
        /// </summary>
        public string DocTypeName { get; set; }
        /// <summary>
        /// ای دی فرم جهت استفاده در سرویس نمایش مجوز ها
        /// </summary>
        public string DocTypeId { get; set; }
    }
}
