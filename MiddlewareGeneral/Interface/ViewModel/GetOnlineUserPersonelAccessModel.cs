﻿namespace Interface.ViewModel
{
    /// <summary>
    /// کلاس افراد زیرمجموعه
    /// </summary>
  public  class GetOnlineUserPersonelAccessModel
    {
        /// <summary>
        /// شناسه فرد
        /// </summary>
        public string PersonID { get; set; }
        /// <summary>
        /// نام
        /// </summary>
        public string DisplayName { get; set; }
        /// <summary>
        /// کد کاربری در سیستم ما کد پرسنلی زده میشود
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// نام واحد سازمانی
        /// </summary>
        public string depname { get; set; }
        /// <summary>
        /// عکس
        /// </summary>
        public string PersonnelPic { get; set; }
        /// <summary>
        /// فرمت عکس
        /// </summary>
        public string ImageFormat { get; set; }
        /// <summary>
        /// تعداد صفحات در سیتم ما مصرفی ندارد
        /// </summary>
        public int TotalPage { get; set; }
    }
}
