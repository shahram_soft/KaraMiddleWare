﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interface.ViewModel
{
    /// <summary>
    /// ویومدلی که در موبایل برای کادرکس استفاده شده است
    /// </summary>
    public class MobileCardexModel
    {
        /// <summary>
        /// شناسه دوره
        /// </summary>
        public string wpid;
        /// <summary>
        /// شناسه کاردکس
        /// </summary>
        public string cardexID;
        /// <summary>
        /// شناسه دوره کاردکس
        /// </summary>
        public string cardexPeriodID;
        /// <summary>
        /// شناسه فرد
        /// </summary>
        public string personelid;
        /// <summary>
        /// کد پرسنلی
        /// </summary>
        public string personcode;
        /// <summary>
        /// نام کاردکس
        /// </summary>
        public string crdperiod;
        /// <summary>
        /// نام فرد
        /// </summary>
        public string fullname;
        /// <summary>
        /// مقدار اولیه
        /// </summary>
        public string start;
        /// <summary>
        /// مقدار اضافه شده
        /// </summary>
        public string added;
        /// <summary>
        /// مقدار کاهنده
        /// </summary>
        public string reduce;
        /// <summary>
        /// مقدار ملزم  به استفاده
        /// </summary>
        public string molzam;
        /// <summary>
        /// نتیجه نهایی باقیمانده
        /// </summary>
        public string result;
        /// <summary>
        /// غر قابل استفاده در پروژه لئوپارد
        /// </summary>
        public string restminute;
        /// <summary>
        /// مقدار درجریان
        /// </summary>
        public string wait;
        /// <summary>
        /// غیر قابل استفاده
        /// </summary>
        public string calcNowait;
    }
}
