﻿using System;
using System.Collections.Generic;

namespace Interface.ViewModel
{
 public   class GetPersonWorkPeriodDetailResult
    {
        /// <summary>
        /// کدها
        /// </summary>
        public string Codes { get; set; }
        /// <summary>
        /// ساختارها
        /// </summary>
        public string Structures { get; set; }
        /// <summary>
        /// اعتبار دارد
        /// </summary>
        public string HasCredit { get; set; }
        /// <summary>
        /// تاریخ 
        /// </summary>
        public string Date { get; set; }
        /// <summary>
        /// نام روز
        /// </summary>
        public string DayName { get; set; }
        /// <summary>
        /// آیا تعطیل است یا خیر
        /// </summary>
        public bool IsHoliday { get; set; }
        /// <summary>
        /// توضیحات تعطیلات
        /// </summary>
        public string HoldiayDescription { get; set; }
        /// <summary>
        /// نام ساختار
        /// </summary>
        public string StructureName { get; set; }
        /// <summary>
        /// رنگ
        /// </summary>
        public string Color { get; set; }
        /// <summary>
        /// تردد
        /// </summary>
        public string Attendances { get; set; }
        /// <summary>
        /// غیبت
        /// </summary>
        public bool Absence { get; set; }
        /// <summary>
        /// نیازمند مجوز
        /// </summary>
        public bool CreditNeed { get; set; }
        /// <summary>
        /// روز هفته
        /// </summary>
        public int DayOfWeek { get; set; }
        /// <summary>
        /// روز هفته
        /// </summary>
        public Guid Holidy { get; set; }
        /// <summary>
        /// آیا روز جاری هست یا نه
        /// </summary>
        public bool IsCurrent { get; set; }

    }
    public class CodeDetail
    {
        /// <summary>
        /// پارامتر یک
        /// </summary>
        public string a { get; set; }
        /// <summary>
        /// پارامتر 2
        /// </summary>
        public string b { get; set; }
        /// <summary>
        /// پارامتر3
        /// </summary>
        public string c { get; set; }
    }
    /// <summary>
    /// کلاسی برای کد های بیسیک
    /// </summary>
    public class BasicCode
    {
        /// <summary>
        /// شناسه کد
        /// </summary>
        public string CodeId { get; set; }
        /// <summary>
        /// زمان شروع
        /// </summary>
        public string StartTime { get; set; }
        /// <summary>
        /// زمان پایان
        /// </summary>
        public string EndTime { get; set; }

        /// <summary>
        /// زمان 
        /// </summary>
        public string Value { get; set; }
        /// <summary>
        /// نام کد
        /// </summary>
        public string CodeName { get; set; }
        /// <summary>
        /// آیا این کد نیاز به مجوز زدن دارد یا خیر
        /// </summary>
        public bool CreditNeed { get; set; }
        /// <summary>
        /// رنگ کد
        /// </summary>
        public string Color { get; set; }
        /// <summary>
        /// ایا برای این کد مجوزی ثبت شده است
        /// </summary>
        public bool CreditHas { get; set; }

        /// <summary>
        /// جهت نمایش در صفحه
        /// </summary>
        public string Type { get; set; }
        /// <summary>
        /// نوع مجوز مورد استفاده در درخواست
        /// </summary>
        public string RequestTypeId { get; set; }
        /// <summary>
        /// نمایش در صفحه
        /// </summary>
        public bool IncludeCalendar { get; set; }

    }
    /// <summary>
    /// کلاسی برای کد های استراکچر
    /// </summary>
    public class MobileStructure
    {
        /// <summary>
        /// نوع کارکرد
        /// </summary>
        public string DetailType { get; set; }
        /// <summary>
        /// زمان شروع
        /// </summary>
        public string StartTime { get; set; }
        /// <summary>
        /// زمان پایان
        /// </summary>
        public string EndTime { get; set; }

    }
}
