﻿namespace Interface.ViewModel
{
    /// <summary>
    /// ویو مدلی برای برگرداند دادهای روزانه و دوره ای برای موبایل
    /// </summary>
    public class PersonWorkPeriodResultModel
    {
        /// <summary>
        ///  دادهای روزانه وبیسیک
        /// </summary>
        public string WorkPeriodDetailResult { get; set; }
        /// <summary>
        ///  دادهای دوره ای
        /// </summary>
        public string WorkPeriodResult { get; set; }
    }
}
