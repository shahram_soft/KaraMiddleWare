﻿using System.Collections.Generic;
using Interface.ViewModel;

namespace Interface
{
 public   interface IWebServiceMethods
 {
     string LoginMobile(string PersonCode, string ComputerName, string UserSessionID, string Language);

     string GetReserveFoodHeader(string Date, int PersonID, string PersonelCode, int FoodTerm,
         int CompanyFinatialPeriodID, int SessionID, string Language);

     string GetReserveFood(string SDate, string EDate, int FoodTerm, int ResturantNO, string Code, int Flag,
         string CurrentDate, int CompanyFinatialPeriodID, int SessionID, string Language);

     string ModifyFoodReserve(string Code, string XmlSave, string HostAddress, int CompanyFinatialPeriodID,
         int SessionID, int OnlineUser, string Language);

     int validate(string Username, string Password);
     string Login_Mobile(string UserName, string Password, string DeviceUniqueID);
     string GetWorkPeriod_Mobile();

     List<GetCodePermission_MobileModel> GetCodePermission_Mobile(int GroupCodeID, bool IsDaily,
         bool IsTimely, int PageID, int OnlineUserID);

     string GetWorkPeriodDates_Mobile(int PersonID, int WPID, int SessionID);
     string GetPersonWPDateResult_Mobile(int PersonID, int WPID, string Date, int SessionID);
     string GetPersonDateAttendances_Mobile(int PersonID, string Date, int SessionID);

     string ModifyAttendance_Mobile(int OnlineUserID, string PersonCode, string Time, string Date,
         int AttendanceID, int Type, int Delete, string Description, int SessionID);

     string ModifyCredit_Mobile(int Type, string CreditType, int Daily, string StartDate, string EndDate,
         string StartTime, string EndTime, int PersonCode, int JPersonCode, int CreditID, string Description,
         int OnlineUserID, int SessionID);

     string GetEnterCredit_Mobile(int PersonID, int WPID, string Date, string Code, string Type, int SessionID);
     string GetPersonWorkPeriodResult_Mobile(int PersonID, int WPID, int SessionID);

     string GetNewCartabl_Mobile(string CartablOwnerCode, string CartablOwnerID, string StartDate,
         string EndDate, string Requester, string DocType, string DepartmentID, string GroupID, string StrFilter,
         string PageSize, string PageNumber, int Sessionid, string Language);

     string WfGetLog_Mobile(int DocId, int DocTypeID, string Language);
     string ModifyCasingWorkTable_Mobile(string DoX, int sessionId, string Language);
     string ModifyWorkTable_Mobile(string DoX, int sessionId, string Language);
     string GetAccessForCartabl_Mobile(int SessionId);
     string GetWfDocType_Mobile(string OnlineUserID);

     string GetCardexReport_Mobile(string Language, string PersonCode, string WPid, string cardexID,
         string CompanyID, string SessionID);

     string GetCardexRestDetail_Mobile(string PersonCode, string WPid, string cardexID, string PageSize,
         string PageNumber);

     string GetDocInfo_Mobile(string SessionID, string OnlineUserID, string PersonCode, string WpID,
         string StartDate, string EndDate, string DocType, string DocState, string PageSize, string PageNumber);

     string GetOnlineUserPersonelAccess_Mobile(string SessionID, string OnlineUserID, string PersonName,
         string pagesize, string pagenumber);

     string GetCartableFilterValue_Mobile(string SessionID, string OnlineUserID);
     string GetExtendedNWFDoc_Mobile(string SessionID, string OnlineUserID, string DocTypeID);
     string GetPersonNotification_Mobile(string OnlineUserID);
     string usbGetAccess_Mobile(string PersonID, string SessionID);
     string usbSurveyAccess_Mobile(string PersonID);
     string ChangePassword_Mobile(string PersonID, string UserName, string OldPassword, string NewPassword);
     string UpdateNotificationStatus_Mobile(string ID);
     string GetEnterCreditByWorkPeriod_Mobile(string PersonID, string SessionID, string WPID, string Code);
     string ModifyGroupCredit_Mobile(string DocX, string OnlineUserID, string SessionID, string CreditID);
     string GetWebServiceVersion_Mobile();

     string DeleteDocInfo_Mobile(string DocID, string DocTypeID, string DeleteDesc, string SessionID,
         string OnlineUserID);

     string GetPersonShareWorkTable_Mobile(string OnlineUserID, string SessionID);

     string ModifyWorkTableChkShAll_Mobile(string actorpid, string CartablOwnerCode, string StartDate,
         string EndDate, string Requester, string DocType, string StrFilter, string PageNumber, string SessionID);

     string InsertAttendance_Mobile(string Time, string Date, int PersonelID, int CardKhanNo);
     string GetAttendancePastil_Mobile(string Date, int PersonelID);
     string GetTranslate_Mobile(string LanguageKey);
     string GetCreditType_Mobile(int OnlineUserID);
     string GetHelpImage_Mobile(string Version, string Language);
     string DocumentState_Mobile();
     string GetFormFields_Mobile(string CodeID, string AutoCompleteParams, string SessionID, string personCode, string callFrom);
     string GetFormFieldsEdit_Mobile(string requestId, string SessionID);
     string GetLocationsPastil_Mobile(string PersonID);

     string ModifyEditCreditByFormBuilder_Mobile(int PersonCode, int JPersonCode, string requestId, int OnlineUserID,
         int SessionID,
         string FormJson, string XmlRoot);
     string ModifyCreditByFormBuilder_Mobile(int PersonCode, int JPersonCode, string CreditID, int OnlineUserID,
         int SessionID, string FormJson, string XmlRoot);

     string ModifyGroupCreditByFormBuilder_Mobile(string FormJson, string OnlineUserID, string SessionID,
         string CreditID, string xmlRoot);

     string LoginHamster_Mobile(string HashId);
     string PersonAutoCompleteCombo(string onlineUserId, string filterStr);
     string DeleteCreditsBatch_Mobile(string onlineUserId, string data);
 }
}
