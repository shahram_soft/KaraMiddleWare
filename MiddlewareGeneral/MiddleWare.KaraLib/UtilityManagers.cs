﻿using System;
using System.Data;
using System.Security.Cryptography;
using System.Text;

namespace MiddleWare.KaraLib
{
 public   class UtilityManagers
    {
        public static string Decrypt(string cipherText)
        {
            return Decrypt(cipherText, true, "mch1537");
        }
        // Token: 0x06000104 RID: 260 RVA: 0x0000733C File Offset: 0x0000553C
        public static string Decrypt(string cipherString, bool useHashing, string key)
        {
            string result;
            try
            {
                cipherString = cipherString.Replace(" ", "+");
                byte[] array = Convert.FromBase64String(cipherString);
                byte[] key2;
                if (useHashing)
                {
                    MD5CryptoServiceProvider expr_21 = new MD5CryptoServiceProvider();
                    key2 = expr_21.ComputeHash(Encoding.UTF8.GetBytes(key));
                    expr_21.Clear();
                }
                else
                {
                    key2 = Encoding.UTF8.GetBytes(key);
                }
                TripleDESCryptoServiceProvider expr_4B = new TripleDESCryptoServiceProvider();
                expr_4B.Key = key2;
                expr_4B.Mode = CipherMode.ECB;
                expr_4B.Padding = PaddingMode.PKCS7;
                byte[] bytes = expr_4B.CreateDecryptor().TransformFinalBlock(array, 0, array.Length);
                expr_4B.Clear();
                result = Encoding.UTF8.GetString(bytes);
            }
            catch
            {
                result = string.Empty;
            }
            return result;
        }
        public static string EncryptPassword(string Password, string UserName, int key=12)
        {
            string result;
            try
            {
                string[] array = new string[Password.Length];
                UserName = UserName.ToLower();
                string text = "";
                Encoding @default = Encoding.Default;
                byte[] array2 = new byte[UserName.Length];
                byte[] array3 = new byte[Password.Length];
                byte[] bytes = @default.GetBytes(UserName);
                byte[] bytes2 = @default.GetBytes(Password);
                int[] array4 = new int[bytes2.Length];
                for (int i = 0; i < bytes2.Length; i++)
                {
                    array4[i] = (int)bytes2[i];
                }
                short num = 0;
                while ((int)num < UserName.Length)
                {
                    byte b = Convert.ToByte((int)(bytes[(int)num] + Convert.ToByte(key)));
                    array2[(int)num] = b;
                    char[] array5 = new char[@default.GetCharCount(array2, 0, array2.Length)];
                    @default.GetChars(array2, 0, array2.Length, array5, 0);
                    text = new string(array5);
                    num += 1;
                }
                byte[] bytes3 = @default.GetBytes(text);
                short num2 = 0;
                while ((int)num2 < Password.Length)
                {
                    for (int j = 0; j < text.Length; j++)
                    {
                        array4[(int)num2] = (array4[(int)num2] ^ (int)bytes3[j]);
                        byte b2 = Convert.ToByte(array4[(int)num2]);
                        array3[(int)num2] = b2;
                        char[] array6 = new char[@default.GetCharCount(array3, 0, array3.Length)];
                        @default.GetChars(array3, 0, array3.Length, array6, 0);
                        array[(int)num2] = new string(array6);
                    }
                    num2 += 1;
                }
                result = array[Password.Length - 1];
            }
            catch
            {
                result = "";
            }
            return result;
        }
        public static string ConvertToFourDigitNumber(string strHour)
        {
            if (strHour.Length == 1)
            {
                strHour = strHour.Insert(0, "000");
            }
            else if (strHour.Length == 2)
            {
                strHour = strHour.Insert(0, "00");
            }
            else if (strHour.Length == 3)
            {
                strHour = strHour.Insert(0, "0");
            }
            return strHour;
        }
        public static string MakeTimeFormat(string Time)
        {
            if (Time.Length >= 4)
            {
                Time = Time.Insert(Time.Length - 2, ":");
            }
            else if (Time.Length == 3)
            {
                Time = "0" + Time.Insert(Time.Length - 2, ":");
            }
            else if (Time.Length == 2)
            {
                Time = "00" + Time.Insert(Time.Length - 2, ":");
            }
            else if (Time.Length == 1)
            {
                Time = Time.Insert(Time.Length - 1, "00:0");
            }
            else if (Time.Length == 0)
            {
                Time = Time.Insert(Time.Length, "00:00");
            }
            return Time;
        }
        // Token: 0x060000F7 RID: 247 RVA: 0x00006E8C File Offset: 0x0000508C
        public static string ConvertToFiveDigitNumber(string strHour)
        {
            if (strHour.Length == 1)
            {
                strHour = strHour.Insert(0, "0000");
            }
            else if (strHour.Length == 2)
            {
                strHour = strHour.Insert(0, "000");
            }
            else if (strHour.Length == 3)
            {
                strHour = strHour.Insert(0, "00");
            }
            else if (strHour.Length == 4)
            {
                strHour = strHour.Insert(0, "0");
            }
            return strHour;
        }
        // Token: 0x060000F3 RID: 243 RVA: 0x00006CF8 File Offset: 0x00004EF8
        public static string ConvertXorToString(string name)
        {
            Encoding @default = Encoding.Default;
            byte[] bytes = @default.GetBytes(name);
            int[] array = new int[name.Length];
            byte[] array2 = new byte[name.Length];
            string result = "";
            for (int i = 0; i < name.Length; i++)
            {
                array[i] = (int)(25 ^ bytes[i]);
            }
            short num = 0;
            while ((int)num < name.Length)
            {
                byte b = Convert.ToByte(array[(int)num]);
                array2[(int)num] = b;
                char[] array3 = new char[@default.GetCharCount(array2, 0, array2.Length)];
                @default.GetChars(array2, 0, array2.Length, array3, 0);
                result = new string(array3);
                num += 1;
            }
            return result;
        }
        // Token: 0x060000F9 RID: 249 RVA: 0x00006F2F File Offset: 0x0000512F
        public static string ConvertToTowDigitNumber(string strHour)
        {
            if (strHour.Length == 1)
            {
                strHour = strHour.Insert(0, "0");
            }
            return strHour;
        }
        // Token: 0x060000FA RID: 250 RVA: 0x00006F4C File Offset: 0x0000514C
        public static string ConvertToTowDigitNumber(int strHour)
        {
            string result = strHour.ToString();
            if (strHour.ToString().Length == 1)
            {
                result = strHour.ToString().Insert(0, "0");
            }
            return result;
        }
        // Token: 0x060000F5 RID: 245 RVA: 0x00006DDC File Offset: 0x00004FDC
        public static string ConvertMinToTimeFormat(int TotalMin, int DayTotalMin)
        {
            string result = string.Empty;
            int num = TotalMin / DayTotalMin;
            int num2 = num * DayTotalMin;
            if (num > 0)
            {
                result = num + " روز ";
            }
            if (num2 < DayTotalMin)
            {
                result = string.Format(" و {0}:{1}", ConvertToTowDigitNumber((TotalMin - num2) / 60), ConvertToTowDigitNumber((TotalMin - num2) % 60));
            }
            return result;
        }
        // Token: 0x060000F1 RID: 241 RVA: 0x00006B08 File Offset: 0x00004D08
        public static bool ValidateTime(string time)
        {
            string[] expr_20 = time.Replace("_", "0").Split(new char[]
            {
                ':'
            });
            int num = Convert.ToInt32(expr_20[0]);
            int num2 = Convert.ToInt32(expr_20[1]);
            return num <= 23 && num2 <= 59;
        }
        // Token: 0x060000F4 RID: 244 RVA: 0x00006DA5 File Offset: 0x00004FA5
        public static string ConvertToTimeFormat(string strHour)
        {
            if (string.IsNullOrEmpty(strHour) || strHour.Contains(":"))
            {
                return strHour;
            }
            strHour = ConvertToFourDigitNumber(strHour);
            strHour = strHour.Insert(strHour.Length - 2, ":");
            return strHour;
        }
    }

}
