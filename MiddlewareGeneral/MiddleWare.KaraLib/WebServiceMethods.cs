﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Interface;
using Interface.ViewModel;
using Interface.ViewModel.General;
using Newtonsoft.Json;
using System.Globalization;
using System.Text.RegularExpressions;
using MiddleWare.KaraLib.ViewModels;
using Newtonsoft.Json.Linq;
using PW.WorkFlows.SqlDal;

namespace MiddleWare.KaraLib
{
   public class WebServiceMethods: IWebServiceMethods
    {
       public string LoginMobile(string PersonCode, string ComputerName, string UserSessionID, string Language)
       {
           throw new NotImplementedException();
       }

       public string GetReserveFoodHeader(string Date, int PersonID, string PersonelCode, int FoodTerm, int CompanyFinatialPeriodID,
           int SessionID, string Language)
       {
           throw new NotImplementedException();
       }

       public string GetReserveFood(string SDate, string EDate, int FoodTerm, int ResturantNO, string Code, int Flag,
           string CurrentDate, int CompanyFinatialPeriodID, int SessionID, string Language)
       {
           throw new NotImplementedException();
       }

       public string ModifyFoodReserve(string Code, string XmlSave, string HostAddress, int CompanyFinatialPeriodID, int SessionID,
           int OnlineUser, string Language)
       {
           throw new NotImplementedException();
       }

       public int validate(string Username, string Password)
       {
           throw new NotImplementedException();
       }
        /// <summary>
        /// لاگین موبایل
        /// </summary>
        /// <param name="UserName"></param>
        /// <param name="Password"></param>
        /// <param name="DeviceUniqueID"></param>
        /// <returns></returns>
       public string Login_Mobile(string UserName, string Password, string DeviceUniqueID)
       {
          
           try
           {
               if (!Regex.IsMatch(UserName, "^[a-zA-Z0-9]*$"))
               {
                    /* your code */
                    var resultGeneralUnValid = new ResultViewModel
                    {
                        Validate = false,
                        Message = "",
                        ValidateMessage = "لطفا از صفحه کلید انگلیسی جهت وارد کردن اطلاعات استفاده کنید"
                    };
                    return JsonConvert.SerializeObject(resultGeneralUnValid);
                }
                UserName = RegisterRequests.ToEnglishNumber(UserName);
                var validLogin = false;
             //  var userisLogginedIn = GetKaraPWLogin(UserName, Password);
                 var userObj = new LoginMobileViewModel();
               var orgId = "";
                var constre = ConfigurationManager.ConnectionStrings["connectionStringProviderSystem"].ToString();
                        var connection = new SqlConnection(constre);

                        if (connection.State == ConnectionState.Open)
                        {
                            connection.Close();
                        }

                        string query = @"select emy.EMP_NO,emy.NAME,emy.FAMILY,emy.Img,pos.TITLE,emy.SEC_NO from Users as usrs
                                              join EMPLOYEE  as emy on usrs.USERS_EmpNo=emy.EMP_NO
                                              join POSITION as pos on emy.POS_NO=pos.POS_NO
                                            where USERS_ID = @UserName and USERS_ID = @Password";
                Password = UtilityManagers.EncryptPassword(Password, UserName);
                connection.Open();
                //var command = new SqlCommand(query, connection);
                //command.Parameters.Add("@UserName", SqlDbType.VarChar, 2000).Value = UserName;
                //command.Parameters.Add("@Password", SqlDbType.VarChar, 2000).Value = Password;
                var command = new SqlCommand("WF_EMPLOYEEGetDataByIdAndPassword", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@WFUserName", SqlDbType.VarChar, 2000).Value = UserName;
                command.Parameters.Add("@WFUserPass", SqlDbType.VarChar, 2000).Value = Password;

                var result =  command.ExecuteReader();
                           while (result.Read())
                           {
                               validLogin = true;
                               userObj.PersonID = result["EMP_NO"].ToString();
                               userObj.PersonName = result["NAME"]+" "+ result["FAMILY"];
                               userObj.RevPersonName = result["FAMILY"] + " " + result["NAME"];
                               userObj.LocalId = 1065;
                               userObj.Language = "1065";
                               userObj.PersonCode = result["EMP_NO"].ToString();
                               userObj.Credit = true;
                               userObj.DuplicateAccess = false;
                               userObj.PersonnelPic =
                                   "iVBORw0KGgoAAAANSUhEUgAAAKAAAABsCAYAAADt5bniAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAABCWSURBVHhe7Z1pcBzHdcehi45kWZKrlMOWZDtWyU4+xFLFRyS7bH/xB+dDKoq/KFWJyyVbvlRJKhXFUqIc8gdbSqpkXRQtUiRuAiRhSjxF8ca1i4sgAYIgAII4iN3pmT1mZo+ZAQVR2vZ7gyZFAI+4CGC7Z+dV/QqUZnem+73/vtc9R09ZqVuhMPwx22afyVnaI46lPeaY8X+Fv897lrZl0mJ7PIs1ezY7BZx3LS0Jf7Pw1wOmgA8F+G9PbMPPnIfvncLvurgPW9/sWfqvxb4f83KJhycntfvw2KIZoQXZOOc3cNu+00vHvupmtMcdW/t/14rvAXEMgWAc12aXQDR8LcFj4rE9WxuEv7vh7/+5pva4l9a/YkNbeVnZDaL5oalmhULhY64dfxCC+iRksmoIcB/8G4JNi0E+oK0WO+1lWFU+rT3p2IkvYZ9E90KTzSDD3ehl9S9DyXwaStwhEJxFB1ZhLGY6pnZwMqP9ws2wv8Q+i+6HVgzL59ndk5b29zC2qoOMkSKDFmQsLQU/tDrwwWN5xu4WbgltNc1xEn8M47Yfw/jtqGv7g346OCUG+gL8cgR4ouAYfyTcFdpKGOfstulMx/bDLz4U3YJoLvhqH862OWO3CTeGtlRzk/EHJzP6K5Dt0rSjQxYCfrCpSVt7ecpJ/IVwa2jzGQys18HM7+/AecfBeR/OdmjI8kBfQnk+Br59lPP+dcLdoV22ZLL/9nxa+zmUjvOUA0NWEIsNw8TtpwXD+Lhwf+laIZ3+xKSlP+WaLEY6K2TVgKw4AWPrf0unBz8hwlE6hmUATxK7NotTzglZO2AGHfey2s/6+0ugNDc0NNyUN+Pfg46fm+2IkOICyWBoeowY0BPcjh3/EmS9gzAYDicXkoKxcWztXceKBWfWHIvFbsW7S6BzF6lOh8gHjA8vwmTlV5yP/4EIo5o2aevfgg6dnd3BEDUAIZ6dtNk3RTjVsULBz3ovQCemZncqRC1gbDiFFawAlUyEV27Lm/qfwy+nk+qMElgx7qVGuasPcDd+irsXOrk7GuHO+SbuDB/jztBh4CB3BpF3p/8OHeLOOfj/uH2kmbtjUe5OnOCu1svdxBD30uOw3zh9PEUAIbbn0+zPRJjlNP8qhsUyVAekBEThGoPcudABAjrKnf7dPN+7g+dP1a08p3/HnbP7QMiNIM5u7iXPQxu0uW2SGNfUMiDCR0W45TE8h+RByYXM9wHVcKlIjUFW6/KzV753Oy2WtQJFOXwUMm0P98wJur2SgTF20trznPNbRPiLa7Y9eiek551UY6UBMx0E2Rk8wPM99bQYig38GJxzR6D0n4U2y50ZId4FoME0zTuEDIpjXnri067FOqhGSgEKb6KL5/veooMuKU7/XvjB9EL7pS/Rba6b+pSQw9oaTM8/Aw0YmtUgaXC1Pu6ceZsMsCrgmBHHqFT/JGLA0rT7hCzWxnJp7YswFhgjGlN8YCyFs1EqoGpSDzPrFqln0aCF0fdy8QeEPFbXLmaT90NpGKUaUnRSI1C+9hBBVB/Mhnh6iOy3BIAIRy5mE58XMlkdsyztPhDfANWAYuMmh2Gst5MMXmCAsSz2k+q/JPR7nnmPkMvKGs54YNzXRRy0+KTHgi++y4AIpc6EttaZTqdX9h5DzvlNMNvdQx2w6MDYKD+wnw5WUIFhhn/FhvKHBIAId6FmhHyu3zxLx+u65MGKjTPeTgcp4OAlP8ofsuBY2q+FfK7PvDT7W9dm71MHKTpYensbyAAFnp5t09eZKb9IAFRM0Iz2N0JGy7NsYuLzsJMEdQAZcMba6OCUCHgTBOUXiTAuZozPCTktzTjvvgV28M6sHcoDjP2cM7vJwJQMPdu5lxyh/SMLFtvHu7uXft0Yxn0/cS2tQO5UBsDxeJKWDEwJ4UycoP0jCaghKMdPCFktzvD6nmdpUq9I4LI+MiClBl71ofwjFbiQkpv8EyGvhQ3GfW+SO5IIvOGTCkip4QwcIP0jGzCR3SjkNb+5Ge0h+IL09/U5Y1EyIKUGXnpU4cZWKMUf4JORQmbXNvjgLmoHsuGMtpABKTn63vYnZJSPZGPSZjuFzGjz11C2NDnP+c0CT8SSASk18NKcxFdFrgbK8JSX1r8s5DbXXDNeR31RRkIBChQSIOJYWo2Q20ybtPXPwgdys78gK2EJFihUggXZSYu4gRXq838SH5YWt8SvglwGT8YrcOv+DCYz7Gkhu2nDJ5wcM44vYyG/ICP+sx5EQEoN/0ZVwj8y49raCdDczUJ+MPaz4w/CAHHNX9ZyPbjaaTIgpYYC14PnABPdSzMWQALxPUt9UGbwThAqIKWGM9JE+kd2QHP/4YuPP/fcjfAfR6kPSQ3eitWzjQxKKYFLh5D+kR7t8HOgvTL/HWoKzX6vYMX8UxBUUEoJl52h/SM/2QI+2J4z4w8TG5XAX+mACErJABVA+tux5sOMfa0sb2k/IzcqQMmfCzy9U7VzgDOx4j8pcy22gdyoALh8BRmYEgEXWKL8ogowG15f5llsP7VRCVIjJT0RwZPxpF8UAZLfXrz75TS1UQ00nj+ziwxO8KkXK2lRflEEi/WUwT+MORsUAhd8pAMUcHp34CCe9IkquDZjkAHjeWqjKpTqFREVr4DMxjW1LJZgpS7BzcGcWL1ldSUGH0kg/aEQMAacQgEq/+IYzAZUkIJL/fTC54QvVALvP0ABKv8CGVx+lw5UMHEG3iH9oBqgPQ8FmKQ2KgUMxou+4Pga4q+yT/lBOTSjzLW1fnqjWgRrVdR58Ge/F0gfKIel9ZU5lnaI3KgYrjFAByxgOMPHyf6riXYASjD7Lb1RMSzNvzuYClpwqFdh0fJFA9p7HU9E/9PsDarixk4RQQsO05MPtZ7/mB/tybJJi32D3qggeI9ggC/N4Ul3st+qkks8fPmGVKWvhlxNYNeMwYePFHv6bX60XKFg3oFPxN3oZVgT/SEFgSwYvHUDYewXtOxns+OoPf+5ENfU/pv4gLJMn5gOztqBeOd3sMZ+ECM7/qwvPrSpjPaQq8KbLhcNzIjxbZhEMJWjB7JfQup3gywZ1JqbjD8o5Oc/mL7Os1gv9WFlwRVUA3B1xBmL0P1TGYud4rx/nZDftLlmXLlngxdC9QmJv+qBys98XAPXYs8I2X1kFzPGn7q25lBfUBcoxapeoutt4K7/VnWqX+oCGsvjQlhCdjMNNtZTX1IanBUPHqSDLCs929W/3f4awIR3q5DbXMPnNCE9KrFA5ZIwQYSK3DOYw0kH66f7oTgw+Xjfy86zQCXYDZ4Z30d9WXlgLJXq2EoGXRbsrho+3lhOtz8AgAB3C51d29xMEk/JKH+XNMXwoY18/MgGnu2uJQVQTFJtFXxw38v8HLSRarvq4KmXRS1SjgZfqJi9gyAwfHgT79/9Ih/a/zJPRitIIaw1+GOYOPZbv11IUAUIE8LNQl4LG4yZ7vEkf1HNctCi9fzsnt9cCfbY4de51VlNCmO1yZ2s40bLZj/rXW4PMnI0iCVYS3npiU8LeS3OPEv/KaRNeV/VtQySXb/jsWMb+cDel64EHAWJZdnsrCKFstLkTm7lRusWfu6dV2YIDzn3zqs80dlAtl1VUENuhv1YyGrxxvv714FyD1I7VRUUoN68hWuNb0IZniuAkYOv+eJAkVDiuR5wghFv3OiX/9nHnT72637bsI1U25XF1A7gEtBCVkszPDkNO5H2da1LJdnV4AfZp2kLH4WgU2LArIjl2Wjd7AuHEtRCoIjNjiquNW3iwwdeJY+DYDaeOPrGlXYFTIBGJjO+vNe1XjYvE/8epFG1H15HrDhPRGs/EqAgfnwTmQ2vBrMWCjLeuIknIuW+sFCYmRO1PnZXNU+3V/rZM3Z8I2Sz9TPKPMVZAD/HmjbPaI8R3RqUy2/ve2n2qJDR9ZmXkfeV/YslO9YFAS6fEeyruXDkjTkTgoXATHn1pGYxnN3zImTE13zhU+3ANmbH1F/1ACaxK/PKfjSo4TfBDpVdxi03cZobkWoi2HPBcoiTAUo81wMKdeTd9fMI7yOwrbmYujegOmZ8L2pGyGdlDG/dh1J8gjqgrDjJUW6eOcz1lmtnvmuBE5WxQxsWLM/zgaLD8R9m19mldkGgzWb/Yb8PVN9kBTTS6a/9vBpW8OL3ulZ8iDqwLLipcZ4dP8nTvfthTFZBB3eJoHgwM+KkBbPj4L6XrpRfLKn4dwAYghJ+Hsrr2OENPHZsE2fEvpYK9gH7gn3CvlF9lgUQ34Dnxe4Rclkdey8XfwAONEY1YG3RuIsrY7EBnhnthGxxxJ9BGq2VZCBXGhTl1VCfWWmwb9hH7GtmtMvvO/oAfUH7aE0ZuZhN3i9ksrr2Xk77AohwnGjE6oAPnRvDPHvhFLeHmiEj7OOJjm1QplYmwykN+AB9gVkSfYM+Ql+t5RN0mJAwMQl5rI1dzBifc212jmrQ8gGhwZgnF+vj9nCUm30H1zSrBYkr2RJ8iL5En06PJ1dWmDgks/Ux+gbT1Ta8vgfq76AathBuepzntbM8M9IBJeUoT53cNX2ebhkThpBFAr41wMfoa/Q5+h5j4KaXt9gRxL7dTV34lJBDcSyTGb/LseLXfs0/3gyqD8Eguptbg4083bOXJ9rrw/IpE1jGISYYG4wRxgpjhrGjYgqVrwDie8u2R+8UMiiu4bU+GHO8kDeGP8xd6IGxSAtPnz7Ak507wvKpMH4ZhxhiLO1zLTw30cPzifMfOGnt+WVf311NS7bXPQ5p3qU6E6I+rBli21b/AxFuOS3Z2fAQa6k4Q3UgRGFaK04nO7Z/9DC5zGZ2HLjDaK15gzVv+YDsTIg6tJRfMiI1GzCmIrzqWKp9219DNrxAdixEeiB248m2+u+KcKpp5vCBO/RI9XrWtOV9qpMhUjKViFS/qmTWu5YZbXVfM1qrolCWC0SHQyQAkkRBb62KpDvrvyLCFizjvPHmRHvtP+otFROUA0KKBw6VWFvdP/DGxptFuIJrrHvvbUZLzTMgxDTljJC1w2iuSBmR6l8UYm23ivCUjmX79n8Sxof/y8AJlHNCVg/WXJ7UW6v/J9Oz6y4RjtI1X4jRmn+HKX+MclbICoLDn2j1U6HwCIu1td0Kzvk+a6ns8gfElANDlsz05KKyIwFjvFjbjtIrtcsxo73+r/RI1Ra9uTxHOTVkYaDM5oxo1Zvpru1fFW4NbalmdzfcaURrfwiztOPg1KnZTg6ZCUMftVQeM6I1j+M5WOHG0FbC7JM7P8uilf9itFZGwdmhGAUoOqO1KgKz2X/Wo1uLc3NoqVm8o/ZeFql9An7tb8PAuvTKdAuU10jVW0ZbzY9gXLe6DwKFNr91d2+6Re+q+7YRrf4lZMcmCI5DBk1hGPSJNVc2Ga3Vv0x1bv8WntQX3Q9NNsMT3cnotq8notVP6ZHKHZAhz0MQlSnZ0+M4aHMLtL21+qlEpO4R7JPoXmiqGef8Bv3Ujj/UIXPo0donQZTrIbiHIVvGYJbtFeOUz/Qxyz0d2wBt0VurXktGan6e6tj2zXz33ruxzaL5oQXZkv2Nt2st276Q6Nr+HTwHqUeq/wsE8QpQB/8+pjdXdIBQBkEoSRCsB5npEmsuR/FcJahyPOVR0FsrLvmfwc+2VAww/G5r1TH4zlbcZyJS+yweI9FW951U+84H8NiiGSVqZWW/BzJV9J2VwP5IAAAAAElFTkSuQmCC";
                               userObj.SessionID = result["EMP_NO"].ToString();
                    userObj.UserID = result["EMP_NO"].ToString();
                               userObj.CompanyName = "";
                               userObj.DepartmentName = result["TITLE"].ToString();
                                     userObj.PositionName = "";
                               userObj.Message = "";
                              // orgId = result["SEC_NO"].ToString();
                    }
                        connection.Close();
                        connection.Dispose();
               if (!validLogin)
               {
                    var resultGeneralUnValid = new ResultViewModel
                    {
                        Validate = false,
                        Message = "",
                        ValidateMessage = "نام کاربری یا رمز عبور اشتباه است"
                    };
                    return JsonConvert.SerializeObject(resultGeneralUnValid);
                }
                DataRow dataByEmpNo = WorkFlowsDatabase.Instance.Employee.GetDataByEmpNo(double.Parse(userObj.PersonID));
                byte[] array = (dataByEmpNo["Img"] is DBNull) ? null : ((byte[])dataByEmpNo["Img"]);
                if (array != null)
                {
                    userObj.PersonnelPic = Convert.ToBase64String(array);
                }
                var orgTitle = GetUsersSection(userObj.PersonID);
                userObj.PositionName = orgTitle;
                var resultGeneral = new ResultViewModel
                {
                    Validate = true,
                    Message =  JsonConvert.SerializeObject(userObj)
                };
                return JsonConvert.SerializeObject(resultGeneral);
           }
           catch (Exception ex)
           {

                var resultGeneral = new ResultViewModel
                {
                    Validate = false,
                    Message ="",
                    ValidateMessage = ex.Message
                };
                return JsonConvert.SerializeObject(resultGeneral);
            }
         
        }

       public bool GetKaraPWLogin(string userNAme, string Password)
       {
           try
           {
                var constre = ConfigurationManager.ConnectionStrings["connectionStringProviderSystem"].ToString();
                var connection = new SqlConnection(constre);
                Password = UtilityManagers.EncryptPassword(Password, userNAme);
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
                connection.Open();
                var command = new SqlCommand("WF_EMPLOYEEGetDataByIdAndPassword", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@WFUserName", SqlDbType.VarChar, 2000).Value = userNAme;
                command.Parameters.Add("@WFUserPass", SqlDbType.VarChar, 2000).Value = Password;

                var result = command.ExecuteReader();
               while (result.Read())
               {

               }
               connection.Close();
                connection.Dispose();
                return true;
           }
           catch (Exception ex)
           {

               return false;
           }
       }
       /// <summary>
       /// بدست اوردن عنوان یک واحد سازمانی
       /// </summary>
       /// <param name="orgId">شناسه واحد</param>
       /// <returns>نام واحد</returns>
       public string GetUsersSection(string orgId)
       {
            try
            {
                var orgName = "";
                var constre = ConfigurationManager.ConnectionStrings["connectionStringProviderSystem"].ToString();
                var connection = new SqlConnection(constre);

                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }

                string query = @"select pos.TITLE from
                                     EMPLOYEE  as emy 
                                              join POSITION as pos on emy.POS_NO=pos.POS_NO
                                            where emy.EMP_NO =" + orgId;

                connection.Open();
                var command = new SqlCommand(query, connection);
             
                var result = command.ExecuteReader();
                while (result.Read())
                {
                    orgName = result["TITLE"].ToString();


                }
                connection.Close();
                connection.Dispose();
              
                return orgName;
            }
            catch (Exception ex)
            {

              
                return "بدون واحد";
            }
        }

       /// <summary>
       /// بدست اوردن واحد سازمانی
       /// </summary>
       /// <param name="empId">شناسه فرد</param>
       /// <param name="userOrgManagerId">شناسه واحدی که فرد مدیر ان است</param>
       /// <returns>لیست واحد</returns>
       public List<SectionViewModel> GetAllSections(string empId,ref string userOrgManagerId)
        {
            try
            {
                var allOrgs=new List<SectionViewModel>();
                var constre = ConfigurationManager.ConnectionStrings["connectionStringProviderSystem"].ToString();
                var connection = new SqlConnection(constre);

                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }

                string query = @"select TITLE,SEC_NO,EmpManager,TMFather,TFather from SECTIONS";

                connection.Open();
                var command = new SqlCommand(query, connection);

                var result = command.ExecuteReader();
                while (result.Read())
                {
                    var selectedOrg = new SectionViewModel
                    {
                        Id = result["SEC_NO"].ToString(),
                        Title = result["TITLE"].ToString(),
                        EmpManager = result["EmpManager"].ToString(),
                        Tfather = result["TFather"].ToString(),
                        TmFather = result["TMFather"].ToString()
                    };
                    allOrgs.Add(selectedOrg);

                }
                connection.Close();
                connection.Dispose();
                var userManagerOfOrg = allOrgs.FirstOrDefault(x => x.EmpManager == empId);
                if (userManagerOfOrg != null)
                {
                    userOrgManagerId = userManagerOfOrg.Id;
                }
                return allOrgs;
            }
            catch (Exception ex)
            {


                return new List<SectionViewModel>();
            }
        }

        //public string ListUnderOrderOrgs(List<SectionViewModel> allOrgs, string userOrgManager)
        //{
        //    try
        //    {
        //        var lstSections = new List<string>();
        //        foreach (var sectionViewModel in allOrgs)
        //        {
        //            if (userOrgManager == sectionViewModel.Tfather)
        //            {
        //                lstSections.Add(sectionViewModel.Id);
        //            }
        //            return 
        //        }
        //    }
        //    catch (Exception ex)
        //    {

        //        return "";
        //    }
        //}
        /// <summary>
        /// لیست دوره ها
        /// </summary>
        /// <returns></returns>
        public string GetWorkPeriod_Mobile()
       {
           try
           {
                var lstWp=new List<GetWorkPeriodMobileModel>();
                System.Globalization.PersianCalendar p = new System.Globalization.PersianCalendar();
               var currentMonthNumber = p.GetMonth(DateTime.Now);
               var currentYear = p.GetYear(DateTime.Now);
                var requestFrmMng=new RequestFormLoader();
              //var currentMonthName= System.Globalization.CultureInfo.CreateSpecificCulture("fa")
              //     .DateTimeFormat.GetMonthName(currentMonthNumber);
               var currentMonthName = requestFrmMng.WorkPeriodNameInShamsi(currentMonthNumber);
               var sdate = PersianDate.ConvertDate.ToEn(currentYear, currentMonthNumber, 1);
               var endDate = 30;
                if (currentMonthNumber == 12)
                {
                    endDate = 29;
                }
                var edate = PersianDate.ConvertDate.ToEn(currentYear, currentMonthNumber, endDate);
               if (currentMonthNumber <= 6)
               {
                    edate = PersianDate.ConvertDate.ToEn(currentYear, currentMonthNumber, 31);
                }
                var currentWP = new GetWorkPeriodMobileModel
               {
                   WorkPeriodID = int.Parse(currentYear.ToString()+ currentMonthNumber.ToString()),
                   WorkPeriodName = currentMonthName + " " + currentYear,
                   StartDate = sdate.Year + "\\" + sdate.Month + "\\" + sdate.Day,
                    EndDate = edate.Year + "\\" + edate.Month + "\\" + edate.Day,
                    ExpireDate = "",
                   IsCurrent = true,
                   IsExpired =false

               };
                lstWp.Add(currentWP);
               for (int i = 0; i < 5; i++)
               {
                   currentMonthNumber--;
                   if (currentMonthNumber < 1)
                   {
                       currentMonthNumber = 12;
                       currentYear--;
                   }
                  //  currentMonthName = System.Globalization.CultureInfo.CreateSpecificCulture("fa")
                  //.DateTimeFormat.GetMonthName(currentMonthNumber);
                  currentMonthName = requestFrmMng.WorkPeriodNameInShamsi(currentMonthNumber);
                    sdate = PersianDate.ConvertDate.ToEn(currentYear, currentMonthNumber, 1);
                     endDate = 30;
                    if (currentMonthNumber == 12)
                    {
                        endDate = 29;
                    }
                    edate = PersianDate.ConvertDate.ToEn(currentYear, currentMonthNumber, endDate);
                    if (currentMonthNumber <= 6)
                    {
                        edate = PersianDate.ConvertDate.ToEn(currentYear, currentMonthNumber, 31);
                    }
                    currentWP = new GetWorkPeriodMobileModel
                    {
                        WorkPeriodID = int.Parse(currentYear.ToString() + currentMonthNumber.ToString()),
                        WorkPeriodName = currentMonthName + " " + currentYear,
                        StartDate = sdate.Year + "\\" + sdate.Month + "\\" + sdate.Day,
                        EndDate = edate.Year + "\\" + edate.Month + "\\" + edate.Day,
                        ExpireDate = "",
                        IsCurrent = false,
                        IsExpired = false

                    };
                    lstWp.Add(currentWP);
                }
               lstWp = lstWp.OrderBy(x => x.StartDate).ToList();
               var generalResult=new ResultViewModel
               {
                   Validate = true,
                   Message = JsonConvert.SerializeObject(lstWp)
               };
               return JsonConvert.SerializeObject(generalResult);
           }
           catch (Exception ex)
           {
                var generalResult = new ResultViewModel
                {
                    Validate = false,
                    Message = "",
                    ValidateMessage = ex.Message
                };
                return JsonConvert.SerializeObject(generalResult);

            }
       }
       
       public List<GetCodePermission_MobileModel> GetCodePermission_Mobile(int GroupCodeID, bool IsDaily, bool IsTimely, int PageID, int OnlineUserID)
       {
           throw new NotImplementedException();
       }

       public string GetWorkPeriodDates_Mobile(int PersonID, int WPID, int SessionID)
       {
           throw new NotImplementedException();
       }

       public string GetPersonWPDateResult_Mobile(int PersonID, int WPID, string Date, int SessionID)
       {
           throw new NotImplementedException();
       }

       public string GetPersonDateAttendances_Mobile(int PersonID, string Date, int SessionID)
       {
           throw new NotImplementedException();
       }

       public string ModifyAttendance_Mobile(int OnlineUserID, string PersonCode, string Time, string Date, int AttendanceID, int Type,
           int Delete, string Description, int SessionID)
       {
           throw new NotImplementedException();
       }

       public string ModifyCredit_Mobile(int Type, string CreditType, int Daily, string StartDate, string EndDate, string StartTime,
           string EndTime, int PersonCode, int JPersonCode, int CreditID, string Description, int OnlineUserID, int SessionID)
       {
           throw new NotImplementedException();
       }

       public string GetEnterCredit_Mobile(int PersonID, int WPID, string Date, string Code, string Type, int SessionID)
       {
           throw new NotImplementedException();
       }
        public List<DailyInfoViewModel> GetAttendances(int PersonID, int WPID, int SessionID)
        {
            try
            {
                var wpSdate = new DateTime();
                var wpEdate = new DateTime();
                FindWorkPreiodDays(WPID.ToString(),PersonID.ToString(), ref wpSdate, ref wpEdate);
                var lstDailyData = new List<DailyInfoViewModel>();
                var constre = ConfigurationManager.ConnectionStrings["connectionStringGeneral"].ToString();
                var connection = new SqlConnection(constre);

                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }

                string query = @"SELECT  distinct
	                                        alternt.CARD,
	                                        CArdvalue,
	                                        date

                                      FROM [dbo].[Alternative_DailWork]  as alternt
                                      join CodeDaily as coded on alternt.CODE=coded.IdCode
                                  where Emp_no=@EmpNo and Date>=@Sdate and Date<=@Edate";

                connection.Open();
                var command = new SqlCommand(query, connection);
                command.Parameters.Add("@EmpNo", SqlDbType.VarChar, 2000).Value = PersonID;
                command.Parameters.Add("@Sdate", SqlDbType.DateTime, 2000).Value = wpSdate;
                command.Parameters.Add("@Edate", SqlDbType.DateTime, 2000).Value = wpEdate;
                var result = command.ExecuteReader();
                while (result.Read())
                {
                    var readData = new DailyInfoViewModel();

                    readData.CARD = result["CARD"].ToString();
                    readData.CardValue = result["CArdvalue"].ToString();
                    readData.Date =DateTime.Parse(result["date"].ToString());
                    lstDailyData.Add(readData);
                }
                connection.Close();
                connection.Dispose();
                return lstDailyData;
            }
            catch (Exception ex)
            {

                return new List<DailyInfoViewModel>();
            }
        }
        public List<ShiftsViewModel> GetAllShifts()
        {
            try
            {
                
                var lstDailyData = new List<ShiftsViewModel>();
                var constre = ConfigurationManager.ConnectionStrings["connectionStringProviderSystem"].ToString();
                var connection = new SqlConnection(constre);

                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }

                string query = @"  select SHIFT_NO,TITLE,START_TIME,END_TIME from SHIFTS";

                connection.Open();
                var command = new SqlCommand(query, connection);
              
                var result = command.ExecuteReader();
                while (result.Read())
                {
                    var readData = new ShiftsViewModel();

                    readData.Id = result["SHIFT_NO"].ToString();
                    readData.Title = result["TITLE"].ToString();
                    readData.StartTime = result["START_TIME"].ToString();
                    readData.EndTime = result["END_TIME"].ToString();
                    lstDailyData.Add(readData);
                }
                connection.Close();
                connection.Dispose();
                return lstDailyData;
            }
            catch (Exception ex)
            {

                return new List<ShiftsViewModel>();
            }
        }
        public List<UserShifts> GetUserShifts(string empNumber,string wpId)
        {
            try
            {
                var year = int.Parse(wpId.Substring(0, 4));
                var month = int.Parse(wpId.Substring(4, (wpId.Length - 4)));
                var lstDailyData = new List<UserShifts>();
                var constre = ConfigurationManager.ConnectionStrings["connectionStringProviderSystem"].ToString();
                var connection = new SqlConnection(constre);

                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }

                string query = @"  select EMP_NO,Code,Value from [dbo].[EMP_SHIF] 
                        UNPIVOT 
	                        (
	                          Value FOR Code in (S1 ,S2 ,S3 ,S4 ,S5 ,S6 , S7 ,S8 ,S9 ,S10 ,S11 ,S12 ,S13 ,S14 ,S15 ,S16 ,S17 ,S18 ,S19 ,S20,S21,S22,S23,S24,S25,S26 ,S27,S28,S29,S30,S31)
	                        ) code
                         where EMP_NO='"+ empNumber + "' and YEAR_='"+ year + "' and MONTH_='"+ month + "'";

                connection.Open();
                var command = new SqlCommand(query, connection);

                var result = command.ExecuteReader();
                while (result.Read())
                {
                    var readData = new UserShifts();
                    
                        var codeUser= result["Code"].ToString().Replace("S",string.Empty);
                    readData.DayNumber = codeUser;
                    readData.ShiftNumber = result["Value"].ToString();
                   
                    lstDailyData.Add(readData);
                }
                connection.Close();
                connection.Dispose();
                return lstDailyData;
            }
            catch (Exception ex)
            {

                return new List<UserShifts>();
            }
        }

       public List<UserShifts> GetEmpGroupShift(string empNumber, string wpId)
       {
           try
           {
                var year = int.Parse(wpId.Substring(0, 4));
                var month = int.Parse(wpId.Substring(4, (wpId.Length - 4)));
                var lstDailyData = new List<UserShifts>();
                var query = @" select Code, Value, GrpShift_GrpNo, GrpShift_DaysNo from GrpShift as grpsh
                             UNPIVOT
                                   (
                                     Value FOR Code in (grpsh.grpShift_Day1, grpsh.grpShift_Day2, grpsh.grpShift_Day3, grpsh.grpShift_Day4, grpsh.grpShift_Day5, grpsh.grpShift_Day6
                                      , grpsh.grpShift_Day7, grpsh.grpShift_Day8
                                      , grpsh.grpShift_Day9, grpsh.grpShift_Day10, grpsh.grpShift_Day11, grpsh.grpShift_Day12, grpsh.grpShift_Day13, grpsh.grpShift_Day14, grpsh.grpShift_Day15
                                       , grpsh.grpShift_Day16, grpsh.grpShift_Day17
                                       , grpsh.grpShift_Day18, grpsh.grpShift_Day19, grpsh.grpShift_Day20, grpsh.grpShift_Day21, grpsh.grpShift_Day22, grpsh.grpShift_Day23, grpsh.grpShift_Day24
                                       , grpsh.grpShift_Day25, grpsh.grpShift_Day26, grpsh.grpShift_Day27, grpsh.grpShift_Day28, grpsh.grpShift_Day29, grpsh.grpShift_Day30, grpsh.grpShift_Day31)
	                                ) code
                            --join Emp_Grps as empGr  on empGr.Grp_No = grpsh.GrpShift_GrpNo

                            where GrpShift_Year = '"+ year + @"' and GrpShift_Month = '"+month+@"'
                            and GrpShift_GrpNo in (
                            select Grp_No from Emp_Grps where
                             Emp_No = '"+ empNumber + "')";
                var constre = ConfigurationManager.ConnectionStrings["connectionStringProviderSystem"].ToString();
                var connection = new SqlConnection(constre);

                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }

               
                connection.Open();
                var command = new SqlCommand(query, connection);

                var result = command.ExecuteReader();
                while (result.Read())
                {
                    var readData = new UserShifts();

                    var codeUser = result["Code"].ToString().Replace("GrpShift_Day", string.Empty);
                    readData.DayNumber = codeUser;
                    readData.ShiftNumber = result["Value"].ToString();

                    lstDailyData.Add(readData);
                }
                connection.Close();
                connection.Dispose();
               if (lstDailyData.Count == 0)
               {
                         query = @" select Code, Value, GrpShift_GrpNo, GrpShift_DaysNo from GrpShift as grpsh
                                 UNPIVOT
                                       (
                                         Value FOR Code in (grpsh.grpShift_Day1, grpsh.grpShift_Day2, grpsh.grpShift_Day3, grpsh.grpShift_Day4, grpsh.grpShift_Day5, grpsh.grpShift_Day6
                                          , grpsh.grpShift_Day7, grpsh.grpShift_Day8
                                          , grpsh.grpShift_Day9, grpsh.grpShift_Day10, grpsh.grpShift_Day11, grpsh.grpShift_Day12, grpsh.grpShift_Day13, grpsh.grpShift_Day14, grpsh.grpShift_Day15
                                           , grpsh.grpShift_Day16, grpsh.grpShift_Day17
                                           , grpsh.grpShift_Day18, grpsh.grpShift_Day19, grpsh.grpShift_Day20, grpsh.grpShift_Day21, grpsh.grpShift_Day22, grpsh.grpShift_Day23, grpsh.grpShift_Day24
                                           , grpsh.grpShift_Day25, grpsh.grpShift_Day26, grpsh.grpShift_Day27, grpsh.grpShift_Day28, grpsh.grpShift_Day29, grpsh.grpShift_Day30, grpsh.grpShift_Day31)
	                                    ) code
                                --join Emp_Grps as empGr  on empGr.Grp_No = grpsh.GrpShift_GrpNo

                                where GrpShift_Year = '" + year + @"' and GrpShift_Month = '" + month + @"'
                                and GrpShift_GrpNo in (
                                select Grp_No from EMPLOYEE where
                                 Emp_No = '" + empNumber + "')";
                     constre = ConfigurationManager.ConnectionStrings["connectionStringProviderSystem"].ToString();
                     connection = new SqlConnection(constre);

                    if (connection.State == ConnectionState.Open)
                    {
                        connection.Close();
                    }


                    connection.Open();
                     command = new SqlCommand(query, connection);

                     result = command.ExecuteReader();
                    while (result.Read())
                    {
                        var readData = new UserShifts();

                        var codeUser = result["Code"].ToString().Replace("GrpShift_Day", string.Empty);
                        readData.DayNumber = codeUser;
                        readData.ShiftNumber = result["Value"].ToString();

                        lstDailyData.Add(readData);
                    }
                    connection.Close();
                    connection.Dispose();
                }
                return lstDailyData;

            }
           catch (Exception ex)
           {
               
               return new List<UserShifts>();
           }
       } 
        public List<DailyInfoViewModel> GetWpData(List<DailyInfoViewModel> lstDailyData)
       {
           try
           {
               var lstDailyDataCode = (from lstDataDays in lstDailyData
                                   group lstDataDays by new { lstDataDays.CODE }
                                    into grp
                                   select new 
                                   {
                                       CODE = grp.Key.CODE
                                      
                                   }).ToList();
                var lstDailyDataSumData = new List<DailyInfoViewModel>();
               foreach (var codes in lstDailyDataCode)
               {
                   var resultCode = lstDailyData.Where(x => x.CODE == codes.CODE).ToList();
                   var houreOfCode = 0;
                    var minetsOfCode = 0;
                    var houre = "";
                    var minet = "";
                    foreach (var dailyInfoViewModel in resultCode)
                   {
                       
                       if (dailyInfoViewModel.Value.Length == 3)
                       {
                             houre = dailyInfoViewModel.Value.Substring(0,1);
                             minet= dailyInfoViewModel.Value.Substring(1, 2);
                        }
                        else if(dailyInfoViewModel.Value.Length <3)
                        {
                            houre = "0";
                            minet = dailyInfoViewModel.Value;
                        }
                       else
                       {
                             houre = dailyInfoViewModel.Value.Substring(0, 2);
                             minet = dailyInfoViewModel.Value.Substring(2, 2);
                        }
                       if (minetsOfCode + int.Parse(minet) > 60)
                       {
                           houreOfCode++;
                           minetsOfCode = (minetsOfCode + int.Parse(minet)) - 60;
                       }
                       else
                       {
                           minetsOfCode = (minetsOfCode + int.Parse(minet));
                       }
                       houreOfCode = houreOfCode + int.Parse(houre);

                   }
                   var resultData = resultCode.FirstOrDefault();
                   if (resultData != null)
                   {
                       resultData.Value = houreOfCode + ":" + minetsOfCode;
                        lstDailyDataSumData.Add(resultData);
                   }

               }
                return lstDailyDataSumData;
           }
           catch (Exception ex)
           {
               
              return new List<DailyInfoViewModel>();
           }
       } 
       public string GetPersonWorkPeriodResult_Mobile(int PersonID, int WPID, int SessionID)
       {
           try
           {
                var wpSdate=new DateTime();
                var wpEdate=new DateTime();
                var lstDailyData=new List<DailyInfoViewModel>();
               var resultPeriods = FindWorkPreiodDays(WPID.ToString(),PersonID.ToString(), ref wpSdate, ref wpEdate);
                var year = int.Parse(WPID.ToString().Substring(0, 4));
                var month = int.Parse(WPID.ToString().Substring(4, (WPID.ToString().Length - 4)));
                var portfolioMngr=new PortfolioManager(new WorkFlowsDatabase());
               var resultDateFiles = portfolioMngr.GetDataFile(year, month, PersonID);
                var constre = ConfigurationManager.ConnectionStrings["connectionStringGeneral"].ToString();
                var connection = new SqlConnection(constre);

                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }

                string query = @"SELECT  distinct [Emp_no]
     
                                          ,[CODE]
	                                      ,coded.CodeName
                                          ,[Value]
                                          ,[PrintText]
                                          ,[Shift_no]
                                          ,[PersianDate]
                                          ,[Date]
                                          ,coded.Translate
                                          ,coded.Color
                                  FROM [dbo].[Alternative_DailWork]  as alternt
                                  join CodeDaily as coded on alternt.CODE=coded.IdCode
                                  where Emp_no=@EmpNo and Date>=@Sdate and Date<=@Edate
                                        and IsShowInMobile=1
";

                connection.Open();
                var command = new SqlCommand(query, connection);
                command.Parameters.Add("@EmpNo", SqlDbType.VarChar, 2000).Value = PersonID;
                command.Parameters.Add("@Sdate", SqlDbType.DateTime, 2000).Value = wpSdate;
                command.Parameters.Add("@Edate", SqlDbType.DateTime, 2000).Value = wpEdate;
                var result = command.ExecuteReader();
                while (result.Read())
                {
                    var readData = new DailyInfoViewModel();
                    readData.Empno = result["Emp_no"].ToString();
                    readData.CODE = result["CODE"].ToString();
                    readData.CodeName = result["CodeName"].ToString();
                    readData.Value = result["Value"].ToString();
                    readData.PrintText = result["PrintText"].ToString();
                    readData.Shiftno = result["Shift_no"].ToString();
                    readData.PersianDate = result["PersianDate"].ToString();
                    readData.Date = DateTime.Parse(result["Date"].ToString());
                    readData.Translate = result["Translate"].ToString();
                    readData.Color = result["Color"].ToString();
                    lstDailyData.Add(readData);
                }
                connection.Close();
                connection.Dispose();
               var lstAnotherDaily = lstDailyData.Select(x=>new DailyInfoViewModel
               {
                   CODE = x.CODE,
                   Value = x.Value,
                   Date = x.Date,
                   Translate = x.Translate,
                   Color = x.Color
               }).ToList();
               lstAnotherDaily.RemoveAll(x => x.Value == "0");
               var wPData = GetWpData(lstAnotherDaily);
               var attendances = GetAttendances(PersonID, WPID, SessionID);
               var allShifts = GetAllShifts();
               var groupShiftUSer = GetEmpGroupShift(PersonID.ToString(), WPID.ToString());
               var userShifts = GetUserShifts(PersonID.ToString(), WPID.ToString());
                var workPeriodResult = wPData.Select(c => new MobileWorkPeriodViewModel
                {
                    CodeID = c.CODE,
                    CodeName = c.Translate,
                    Color = c.Color,
                    KasrKar = false,
                    MazadKar = false,
                    CreditNeed = false,
                    Timely = false,
                    Daily = false,
                    Value = c.Value,
                    RequestTypeId = ""
                }).ToList();
                var lstBasicResult = new List<GetPersonWorkPeriodDetailResult>();
                var lstBasicCount = wpEdate-wpSdate;
               var currentdate = wpSdate;
                for (var i = 0; i <= lstBasicCount.Days; i++)
                {
                    var basicResult = new GetPersonWorkPeriodDetailResult();
                    var dayResult = lstDailyData.Where(x => x.Date == currentdate).ToList();
                    basicResult.Date = currentdate.Date.ToString();
                    basicResult.DayName = GetPersianDateString(currentdate);
                    basicResult.DayOfWeek = 0;
                    basicResult.IsCurrent = currentdate.Date==DateTime.Now.Date;
                    basicResult.IsHoliday = false;
                    basicResult.HoldiayDescription = "";
                    basicResult.StructureName = "";
                    basicResult.CreditNeed = false;

                    var shiftOfDay = allShifts.FirstOrDefault(x =>
                    {
                        var dailyInfoViewModel = dayResult.FirstOrDefault();
                        return dailyInfoViewModel != null && x.Id == dailyInfoViewModel.Shiftno;
                    });
                    if (shiftOfDay != null)
                    {
                        basicResult.StructureName = shiftOfDay.Title;
                        var myDayInfo = resultDateFiles[i];
                        if (myDayInfo != null)
                        {
                            basicResult.StructureName = basicResult.StructureName +"-"+ myDayInfo.DayStatusName;
                        }
                        var currentShift=  new List<MobileStructure> { 
                            new MobileStructure
                        {
                            DetailType = "کارکرد",
                            StartTime = ConvertValueToStringHoures(shiftOfDay.StartTime),
                            EndTime = ConvertValueToStringHoures(shiftOfDay.EndTime)
                        }
                        };
                      
                        basicResult.Structures = JsonConvert.SerializeObject(currentShift);
                    }
                    else
                    {
                        var userShift = userShifts.FirstOrDefault(x => int.Parse(x.DayNumber) == (i + 1));
                        if (userShift != null)
                        {
                            var shiftOfDayName = allShifts.FirstOrDefault(x =>x.Id == userShift.ShiftNumber );
                            if (shiftOfDayName != null)
                            {
                                basicResult.StructureName = shiftOfDayName.Title;
                                var myDayInfo = resultDateFiles[i];
                                if (myDayInfo != null)
                                {
                                    basicResult.StructureName = basicResult.StructureName + myDayInfo.DayStatusName;
                                }
                                var currentShift = new List<MobileStructure> {
                            new MobileStructure
                        {
                            DetailType = "کارکرد",
                            StartTime = ConvertValueToStringHoures(shiftOfDayName.StartTime),
                            EndTime = ConvertValueToStringHoures(shiftOfDayName.EndTime)
                        }
                        };

                                basicResult.Structures = JsonConvert.SerializeObject(currentShift);
                            }
                            else
                            {
                                switch (userShift.ShiftNumber)
                                {
                                    case "50":
                                        basicResult.StructureName = "تعطیل";
                                        break;
                                    case "51":
                                        basicResult.StructureName = "تعطیل شیفت";
                                        break;
                                    case "52":
                                        basicResult.StructureName = "تعطیل خاص";
                                        break;
                                    default:
                                        basicResult.StructureName = "نا مشخص";
                                        break;

                                }
                              
                                var currentShift = new List<MobileStructure> {
                            new MobileStructure
                        {
                            DetailType = "کارکرد",
                            StartTime = "00:00",
                            EndTime = "23:59"
                        }};

                                basicResult.Structures = JsonConvert.SerializeObject(currentShift);
                            }
                        }
                        else
                        {
                            // var dayRequestInfo = resultDateFiles.FirstOrDefault(x => x.DateOfRequest == currentdate.Date);
                            var dayRequestInfo = resultDateFiles[i];
                            if (dayRequestInfo != null)
                            {
                                basicResult.StructureName = dayRequestInfo.DayStatusName+ dayRequestInfo.EndDayNames;
                                switch (dayRequestInfo.EndDayNames)
                                {
                                    case "50":
                                        basicResult.StructureName = dayRequestInfo.DayStatusName + "تعطیل";
                                        var currentShiftHoliday = new List<MobileStructure>
                                {
                                    new MobileStructure
                                    {
                                        DetailType = "کارکرد",
                                        StartTime = "00:00",
                                        EndTime = "23:59"
                                    }
                                };
                                        basicResult.Structures = JsonConvert.SerializeObject(currentShiftHoliday);
                                        break;
                                    case "51":
                                        basicResult.StructureName = dayRequestInfo.DayStatusName + "تعطیل شیفت";
                                        var currentShiftShiftHoly = new List<MobileStructure>
                                {
                                    new MobileStructure
                                    {
                                        DetailType = "کارکرد",
                                        StartTime = "00:00",
                                        EndTime = "23:59"
                                    }
                                };
                                        basicResult.Structures = JsonConvert.SerializeObject(currentShiftShiftHoly);
                                        break;
                                    case "52":
                                        basicResult.StructureName = dayRequestInfo.DayStatusName + "تعطیل خاص";
                                        var currentShiftSpecial = new List<MobileStructure>
                                {
                                    new MobileStructure
                                    {
                                        DetailType = "کارکرد",
                                        StartTime = "00:00",
                                        EndTime = "23:59"
                                    }
                                };
                                        basicResult.Structures = JsonConvert.SerializeObject(currentShiftSpecial);
                                        break;
                                    default:
                                        basicResult.StructureName = dayRequestInfo.DayStatusName ;
                                //        var currentShift = new List<MobileStructure>
                                //{
                                //    new MobileStructure
                                //    {
                                //        DetailType = "کارکرد",
                                //        StartTime = "00:00",
                                //        EndTime = "23:59"
                                //    }
                                //};
                                //        basicResult.Structures = JsonConvert.SerializeObject(currentShift);
                                        var groupShiftUSers = groupShiftUSer.FirstOrDefault(x => int.Parse(x.DayNumber) == (i + 1));
                                        if (groupShiftUSers != null)
                                        {
                                            var shiftOfDayName =
                                                allShifts.FirstOrDefault(x => x.Id == groupShiftUSers.ShiftNumber);
                                            if (shiftOfDayName != null)
                                            {
                                                basicResult.StructureName =
                                                    !String.IsNullOrEmpty(dayRequestInfo.DayStatusName)
                                                        ? dayRequestInfo.DayStatusName + "--" + shiftOfDayName.Title
                                                        : shiftOfDayName.Title;
                                                var currentShift = new List<MobileStructure>
                                                {
                                                    new MobileStructure
                                                    {
                                                        DetailType = "کارکرد",
                                                        StartTime = ConvertValueToStringHoures(shiftOfDayName.StartTime),
                                                        EndTime = ConvertValueToStringHoures(shiftOfDayName.EndTime)
                                                    }
                                                };

                                                basicResult.Structures = JsonConvert.SerializeObject(currentShift);
                                            }
                                            else
                                            {
                                                switch (groupShiftUSers.ShiftNumber)
                                                {
                                                    case "50":
                                                        basicResult.StructureName = "تعطیل";
                                                        break;
                                                    case "51":
                                                        basicResult.StructureName = "تعطیل شیفت";
                                                        break;
                                                    case "52":
                                                        basicResult.StructureName = "تعطیل خاص";
                                                        break;
                                                    default:
                                                        basicResult.StructureName = "نا مشخص";
                                                        break;

                                                }

                                                var currentShiftGeneral = new List<MobileStructure>
                                                {
                                                    new MobileStructure
                                                    {
                                                        DetailType = "کارکرد",
                                                        StartTime = "00:00",
                                                        EndTime = "23:59"
                                                    }
                                                };

                                                basicResult.Structures = JsonConvert.SerializeObject(currentShiftGeneral);
                                            }
                                        }
                                        else
                                        {
                                                                basicResult.StructureName = "نا مشخص";
                                        var currentShiftUnknown = new List<MobileStructure> {
                            new MobileStructure
                        {
                            DetailType = "کارکرد",
                            StartTime = "00:00",
                            EndTime = "23:59"
                        }};

                                        basicResult.Structures = JsonConvert.SerializeObject(currentShiftUnknown);
                                        }
                    
                                
                                break;

                                }
                               
                            }
                            else
                            {
                                
                          
                               basicResult.StructureName = "نا مشخص";
                       var currentShift = new List<MobileStructure> {
                            new MobileStructure
                        {
                            DetailType = "کارکرد",
                            StartTime = "00:00",
                            EndTime = "23:59"
                        }};

                        basicResult.Structures = JsonConvert.SerializeObject(currentShift);  }
                        }
                     
                    }
                    var resultAttendaceDay = attendances.Where(x => x.Date.Date == currentdate.Date).Select(x => new AttendanceModel
                    {
                        Type = "I",
                        Time = ConvertValueToStringHoures(x.CardValue),
                        Color = "#25C481"
                    });
                    basicResult.Attendances = JsonConvert.SerializeObject(resultAttendaceDay);
                    basicResult.HasCredit = false.ToString();
                    var lstCodeTranslated = (from c in dayResult.Where(x=>x.Value!="0").ToList()
                                                select new BasicCode
                                             {
                                                 CodeName = c.Translate ,
                                                 CodeId = c.CODE,
                                                 Value = ConvertValueToStringHoures(c.Value),
                                                 EndTime = "",
                                                 StartTime = "",
                                                 CreditNeed = false,
                                                 Color = c.Color,
                                                 CreditHas = false,
                                                 Type = "WC",
                                                 RequestTypeId = "",
                                                 IncludeCalendar = true
                                             }).ToList();
                    basicResult.Codes = JsonConvert.SerializeObject(lstCodeTranslated);
                    lstBasicResult.Add(basicResult);
                    currentdate= currentdate.AddDays(1);
                }
                var calcBrDrWp = new PersonWorkPeriodResultModel
                {
                    WorkPeriodDetailResult = JsonConvert.SerializeObject(lstBasicResult),
                    WorkPeriodResult = JsonConvert.SerializeObject(workPeriodResult)
                };
                var resultService = new ResultViewModel
                {
                    Validate = true,
                    Message = JsonConvert.SerializeObject(calcBrDrWp),
                    ExceptionMessage = "",
                    ValidateMessage = ""
                };
                return JsonConvert.SerializeObject(resultService);
            }
           catch (Exception ex)
           {

                var resultService = new ResultViewModel
                {
                    Validate = false,
                    Message = "",
                    ExceptionMessage = "",
                    ValidateMessage = ex.Message
                };
                return JsonConvert.SerializeObject(resultService);
            }
       }

       public string GetPersianDateString(DateTime date)
       {
           PersianCalendar pc = new PersianCalendar();
           string dayOfWeek;
           switch (pc.GetDayOfWeek(date))
           {
               case DayOfWeek.Sunday:
                   dayOfWeek = "یکشنبه";
                   break;
               case DayOfWeek.Monday:
                   dayOfWeek = "دوشنبه";
                   break;
               case DayOfWeek.Tuesday:
                   dayOfWeek = "سه شنبه";
                   break;
               case DayOfWeek.Wednesday:
                   dayOfWeek = "چهارشنبه";
                   break;
               case DayOfWeek.Thursday:
                   dayOfWeek = "پنج شنبه";
                   break;
               case DayOfWeek.Friday:
                   dayOfWeek = "جمعه";
                   break;
               case DayOfWeek.Saturday:
                   dayOfWeek = "شنبه";
                   break;
               default:
                   dayOfWeek = "";
                   break;
           }
           return dayOfWeek;
       }

       public string GetNewCartabl_Mobile(string CartablOwnerCode, string CartablOwnerID, string StartDate, string EndDate,
           string Requester, string DocType, string DepartmentID, string GroupID, string StrFilter, string PageSize,
           string PageNumber, int Sessionid, string Language)
       {
           try
           {
                var workflowObj=new WorkFlowsDatabase();
               // workflowObj.ConnectionString = ConfigurationManager.ConnectionStrings["connectionStringProviderSystem"].ToString();
                var portfoliomng=new PortfolioManager(workflowObj);
               var resultPortfo = portfoliomng.GetPortfolio(CartablOwnerCode, CartablOwnerID, StartDate, EndDate,
                   Requester, DocType, DepartmentID, GroupID, StrFilter, PageSize,
                   PageNumber, Sessionid, Language);
              
                return JsonConvert.SerializeObject(resultPortfo);
            }
           catch (Exception ex)
           {
                var resultService = new ResultViewModel
                {
                    Validate = false,
                    Message = "",
                    ExceptionMessage = "",
                    ValidateMessage = ex.Message
                };
                return JsonConvert.SerializeObject(resultService);
            }
       }

       public string WfGetLog_Mobile(int DocId, int DocTypeID, string Language)
       {
           try
           {
                var portfolioMng=new PortfolioManager(new WorkFlowsDatabase());
               var result = portfolioMng.GetFlowList(DocId.ToString());
               
                return JsonConvert.SerializeObject(result);
            }
           catch (Exception ex )
           {

                var resultService = new ResultViewModel
                {
                    Validate = false,
                    Message = "",
                    ExceptionMessage = "",
                    ValidateMessage = ex.Message
                };
                return JsonConvert.SerializeObject(resultService);
            }
       }

       public string ModifyCasingWorkTable_Mobile(string DoX, int sessionId, string Language)
       {
           try
           {
                var portMng=new PortfolioManager(new WorkFlowsDatabase());
               var result = portMng.AcceptOrRejectOrSendUpRequest(DoX, sessionId.ToString());
               return JsonConvert.SerializeObject(result);
           }
           catch (Exception ex)
           {
                var resultService = new ResultViewModel
                {
                    Validate = false,
                    Message = "",
                    ExceptionMessage = "",
                    ValidateMessage = ex.Message
                };
                return JsonConvert.SerializeObject(resultService);

            }
       }

       public string ModifyWorkTable_Mobile(string DoX, int sessionId, string Language)
       {
           throw new NotImplementedException();
       }

       public string GetAccessForCartabl_Mobile(int SessionId)
       {
           try
           {
                var accessModel = new MobileAccessViewModel();
                var jsonResult = JsonConvert.SerializeObject(accessModel);
                var result = new ResultViewModel
                {
                    Validate = true,
                    Message = jsonResult
                };
                return JsonConvert.SerializeObject(result);
            }
           catch (Exception ex)
           {
                var resultService = new ResultViewModel
                {
                    Validate = false,
                    Message = "",
                    ExceptionMessage = "",
                    ValidateMessage = ex.Message
                };
                return JsonConvert.SerializeObject(resultService);

            }
       }

       public string GetWfDocType_Mobile(string OnlineUserID)
       {
           try
           {
                var lstModel=new List<DocTypesViewModel>();
                var reqstmng = new RequestFormLoader();
                var lstTypeRequest = reqstmng.GetUserRequestTypes(OnlineUserID);
               foreach (var requestModel in lstTypeRequest)
               {
                   var allactionsOfModel = reqstmng.GetAllFormCodesForModel(requestModel.Id);
                   var resuttLst = allactionsOfModel.Select(x => new DocTypesViewModel
                   {
                       DocTypeId = x.CodeID,
                       DocTypeName =x.CodeName
                   }).ToList();
                   if (resuttLst.All(x => x.DocTypeName != null))
                   {
                                lstModel.AddRange(resuttLst);
                   }
           
               }
                var resultService = new ResultViewModel
                {
                    Validate = true,
                    Message = JsonConvert.SerializeObject(lstModel),
                    ExceptionMessage = "",
                    ValidateMessage = ""
                };
                return JsonConvert.SerializeObject(resultService);
            }
           catch (Exception ex)
           {

                var resultService = new ResultViewModel
                {
                    Validate = false,
                    Message = "",
                    ExceptionMessage = "",
                    ValidateMessage = ex.Message
                };
                return JsonConvert.SerializeObject(resultService);
            }
       }

       public string GetCardexReport_Mobile(string Language, string PersonCode, string WPid, string cardexID, string CompanyID,
           string SessionID)
       {
           try
           {
               // var webServiceMng=new PWKara.RequestServiceClient();
               //var cardexRemaining = webServiceMng.GetRaminKardexWithString(double.Parse(PersonCode), DateTime.Now);
               var remainData = "";
               var registerMng=new RegisterRequests();
               var remainKardex = registerMng.GetRemainKardex(double.Parse(PersonCode));
                if (remainKardex != null && remainKardex.Rows.Count > 0)
                {
                    int num6 = Convert.ToInt32(remainKardex.Rows[0]["RemainKardex"].ToString().Replace(":", ""));
                    if (num6 > 100)
                    {
                        var resultHr = num6/100;
                        var resultminCardex = num6%100;
                        var resultMn = resultminCardex!=0?resultminCardex.ToString():"00";
                        remainData = resultHr + ":" + resultMn;
                    }
                    else
                    {
                        remainData = num6.ToString();
                    }
                    //long num7;
                    //if (Convert.ToBoolean(remainKardex.Rows[0]["IS_MOR_DAY"]))
                    //{
                    //    num7 = (long)(Convert.ToInt32(num6) * num);
                    //}
                    //else
                    //{
                    //    int num8 = num6 / 100;
                    //    long num9 = (long)(num6 % 100);
                    //    if (num8 < 0)
                    //    {
                    //        num9 *= -1L;
                    //        num7 = (long)(num8 * 60) + num9;
                    //    }
                    //    else
                    //    {
                    //        num7 = (long)(num8 * 60) + num9;
                    //    }
                    //}
                    //long num10 = num7 - (long)num3;
                    //long num11 = num10 % (long)num;
                    //limitDay = string.Concat(new object[]
                    //{
                    //num10 / (long)num,
                    //" روز ",
                    //num11 / 60L,
                    //" ساعت ",
                    //num11 % 60L,
                    //" دقیقه "
                    //});
                    //result = (num7 - (long)num3 >= (long)num2);
                }
                var lstCardex = new List<MobileCardexModel>
               {
                   new MobileCardexModel
                   {
                       wpid = WPid,
                       cardexID = cardexID,
                       cardexPeriodID = WPid,
                       personcode = PersonCode,
                       personelid = PersonCode,
                       fullname = "",
                       crdperiod = "استحقاقی",
                       start = "",
                       added = "",
                       reduce = "",
                       molzam = "00:00",
                       result = remainData+"ساعت",
                       wait = ""

                   }
               };
                var result=new ResultViewModel
                {
                    Validate = true,
                    Message = JsonConvert.SerializeObject(lstCardex)
                };
               return JsonConvert.SerializeObject(result);
           }
           
           catch (Exception ex)
           {
               var result=new ResultViewModel
               {
                   Validate = false,
                   ValidateMessage = ex.Message
               };

               return JsonConvert.SerializeObject(result);
           }
       }

       public string GetCardexRestDetail_Mobile(string PersonCode, string WPid, string cardexID, string PageSize, string PageNumber)
       {
           throw new NotImplementedException();
       }
        /// <summary>
        /// نمایش مجوز ها
        /// </summary>
        /// <param name="SessionID"></param>
        /// <param name="OnlineUserID"></param>
        /// <param name="PersonCode"></param>
        /// <param name="WpID"></param>
        /// <param name="StartDate"></param>
        /// <param name="EndDate"></param>
        /// <param name="DocType"></param>
        /// <param name="DocState"></param>
        /// <param name="PageSize"></param>
        /// <param name="PageNumber"></param>
        /// <returns></returns>
       public string GetDocInfo_Mobile(string SessionID, string OnlineUserID, string PersonCode, string WpID, string StartDate,
           string EndDate, string DocType, string DocState, string PageSize, string PageNumber)
       {
           try
           {
                var portfolioMng=new PortfolioManager(new WorkFlowsDatabase());
               var result = portfolioMng.GetShowRequest(SessionID, OnlineUserID, PersonCode, WpID, StartDate,
                   EndDate, DocType, DocState, PageSize, PageNumber);
               return JsonConvert.SerializeObject(result);
           }
           catch (Exception ex)
           {

                var result = new ResultViewModel
                {
                    Validate = false,
                    ValidateMessage = ex.Message
                };

                return JsonConvert.SerializeObject(result);
            }
       }
        /// <summary>
        /// افراد زیر مجموعه
        /// </summary>
        /// <param name="SessionID"></param>
        /// <param name="OnlineUserID"></param>
        /// <param name="PersonName"></param>
        /// <param name="pagesize"></param>
        /// <param name="pagenumber"></param>
        /// <returns></returns>
       public string GetOnlineUserPersonelAccess_Mobile(string SessionID, string OnlineUserID, string PersonName, string pagesize,
           string pagenumber)
       {
           try
           {
                //var result=new List<GetOnlineUserPersonelAccessModel>();

                //var modelResult=new ResultViewModel
                //{
                //    Validate = true,
                //    Message = ""
                //};
                var formLoaderMng=new RequestFormLoader();
               var modelResult = formLoaderMng.GetPersonUnderOrder(OnlineUserID, PersonName);
               return JsonConvert.SerializeObject(modelResult);
           }
           catch (Exception ex)
           {
               
               var result=new ResultViewModel
               {
                   Validate = false,
                   ValidateMessage = ex.Message
               };
               return JsonConvert.SerializeObject(result);
           }
       }

       public string GetCartableFilterValue_Mobile(string SessionID, string OnlineUserID)
       {
           try
           {
                var portfolioMng=new PortfolioManager(new WorkFlowsDatabase());
                var resultPortfolio = portfolioMng.GetByManagerEmpNo(double.Parse(OnlineUserID), new DateTime(2000, 01, 01),
                DateTime.MaxValue, null).ToList().Distinct();
               var allPersons = resultPortfolio.Select(x => new MobilePortFolioPersonViewModel
               {
                   PersonID = x.RequesterCode,
                   DisplayName = x.RequesterName
               }).ToList();
                var resultPersons= allPersons
                                  .GroupBy(p => new { p.PersonID })
                                  .Select(g => g.First())
                                  .ToList();
                var reqstmng = new RequestFormLoader();
                var lstTypeRequest = reqstmng.GetUserRequestTypes(OnlineUserID.ToString());
                var lstRequestTypes = lstTypeRequest.Select(x => new DocTypesViewModel
                {
                    DocTypeID = x.Id,
                    DocTypeName = x.Title
                   
                }).ToList();
                var strjsonservice = "{" + '"' + "Persons" + '"' + ":" + JsonConvert.SerializeObject(resultPersons) + "," + '"' + "DocTypes" + '"' + ":" + JsonConvert.SerializeObject(lstRequestTypes) + "}";
                var result= new ResultViewModel
                {
                    Validate = true,
                    Message = strjsonservice
                };
                return JsonConvert.SerializeObject(result);
            }
           catch (Exception ex)
           {

                var result = new ResultViewModel
                {
                    Validate = false,
                    ValidateMessage = ex.Message
                };
                return JsonConvert.SerializeObject(result);
            }
       }

       public string GetExtendedNWFDoc_Mobile(string SessionID, string OnlineUserID, string DocTypeID)
       {
           try
           {
                var reqstmng = new RequestFormLoader();
               var resultCodes= reqstmng.GetFormCodesForModel(OnlineUserID.ToString(), DocTypeID);
               var resultData = resultCodes.Select(x => new FormCodeinMobileViewModel
               {
                   Title = x.CodeName,
                   Val = x.CodeID
               }).ToList();
                var resultGeneal = new FormCodeFirstViewModel
                {
                    Codes = new FormCodeOriginalViewModel
                    {
                        Code = resultData
                    }
                };
                var jsonGeneal = JsonConvert.SerializeObject(resultGeneal);
                var resultService= new ResultViewModel
                {
                    Validate = true,
                    Message = JsonConvert.SerializeObject(jsonGeneal)
                };
                return JsonConvert.SerializeObject(resultService);
            }
           catch (Exception ex)
           {

                var result = new ResultViewModel
                {
                    Validate = false,
                    ValidateMessage = ex.Message
                };
                return JsonConvert.SerializeObject(result);
            }
       }

       public string GetPersonNotification_Mobile(string OnlineUserID)
       {
           throw new NotImplementedException();
       }
        /// <summary>
        /// دسترسی منو ها
        /// </summary>
        /// <param name="PersonID"></param>
        /// <param name="SessionID"></param>
        /// <returns></returns>
       public string usbGetAccess_Mobile(string PersonID, string SessionID)
       {
            var result= new ResultViewModel
            {
                Validate = true,
                Message = "[{\"MenuItemID\":1302,\"Access\":1,\"Title\":\"مديريت کارکرد روزانه\"},{\"MenuItemID\":1306,\"Access\":1,\"Title\":\"مديريت کارکرد ماهانه\"},{\"MenuItemID\":13129,\"Access\":1,\"Title\":\"نمايش مجوزها\"},{\"MenuItemID\":13133,\"Access\":0,\"Title\":\"کاربران مجاز به ثبت مجوز\"},{\"MenuItemID\":13147,\"Access\":0,\"Title\":\"درخواست تردد\"},{\"MenuItemID\":13158,\"Access\":1,\"Title\":\"درخواست مجوز\"},{\"MenuItemID\":111119,\"Access\":0,\"Title\":\"کارتابل جديد اسناد\"},{\"MenuItemID\":111121,\"Access\":1,\"Title\":\"کارتابل کاري\"},{\"MenuItemID\":131923,\"Access\":0,\"Title\":\"ثبت مجوز گروهی\"},{\"MenuItemID\":141160,\"Access\":0,\"Title\":\"گزارش وضعيت ماهانه پرسنل ها\"}]"
            };
            var portfolioManager=new PortfolioManager(new WorkFlowsDatabase());
            if (!portfolioManager.IsUserManager(double.Parse(PersonID)))
            {
                result = new ResultViewModel
                {
                    Validate = true,
                    Message = "[{\"MenuItemID\":1302,\"Access\":1,\"Title\":\"مديريت کارکرد روزانه\"},{\"MenuItemID\":1306,\"Access\":1,\"Title\":\"مديريت کارکرد ماهانه\"},{\"MenuItemID\":13129,\"Access\":1,\"Title\":\"نمايش مجوزها\"},{\"MenuItemID\":13133,\"Access\":0,\"Title\":\"کاربران مجاز به ثبت مجوز\"},{\"MenuItemID\":13147,\"Access\":0,\"Title\":\"درخواست تردد\"},{\"MenuItemID\":13158,\"Access\":1,\"Title\":\"درخواست مجوز\"},{\"MenuItemID\":111119,\"Access\":0,\"Title\":\"کارتابل جديد اسناد\"},{\"MenuItemID\":111121,\"Access\":0,\"Title\":\"کارتابل کاري\"},{\"MenuItemID\":131923,\"Access\":0,\"Title\":\"ثبت مجوز گروهی\"},{\"MenuItemID\":141160,\"Access\":0,\"Title\":\"گزارش وضعيت ماهانه پرسنل ها\"}]"
                };
            }
           return JsonConvert.SerializeObject(result);
       }

       public string usbSurveyAccess_Mobile(string PersonID)
       {
            var result= new ResultViewModel
            {
                Validate = true,
                Message = "[{\"BtnRegCredit\":1,\"BtnRequestIO\":0}]"
            };
           return JsonConvert.SerializeObject(result);
       }

       public string ChangePassword_Mobile(string PersonID, string UserName, string OldPassword, string NewPassword)
       {
           try
           {
                var portMng=new PortfolioManager(new WorkFlowsDatabase());
               var result = portMng.ChangePassword(PersonID, UserName, OldPassword, NewPassword);
               return JsonConvert.SerializeObject(result);
           }
           catch (Exception ex)
           {
               
               var result= new ResultViewModel
               {
                   Validate = false,
                   ValidateMessage = ex.Message,
                   ExceptionMessage = ex.Message
               };
               return JsonConvert.SerializeObject(result);
           }
       }

       public string UpdateNotificationStatus_Mobile(string ID)
       {
           throw new NotImplementedException();
       }

       public string GetEnterCreditByWorkPeriod_Mobile(string PersonID, string SessionID, string WPID, string Code)
       {
           throw new NotImplementedException();
       }

       public string ModifyGroupCredit_Mobile(string DocX, string OnlineUserID, string SessionID, string CreditID)
       {
           throw new NotImplementedException();
       }
        /// <summary>
        /// نسخه وب سرویس ها
        /// </summary>
        /// <returns></returns>
       public string GetWebServiceVersion_Mobile()
       {
            var result= new ResultViewModel
            {
                Validate = true,
                Message = "1.9.7",
                ExceptionMessage = "",
                ValidateMessage = ""
            };
           return JsonConvert.SerializeObject(result);
       }

       public string DeleteDocInfo_Mobile(string DocID, string DocTypeID, string DeleteDesc, string SessionID, string OnlineUserID)
       {
           throw new NotImplementedException();
       }

       public string GetPersonShareWorkTable_Mobile(string OnlineUserID, string SessionID)
       {
           try
           {
                var lstOwnerPortfolio=new List<SharedWorkTableViewModel>
                {
                    new SharedWorkTableViewModel
                        {
                            ID = OnlineUserID,
                            Code = OnlineUserID,
                            Name = "کاربر آنلاین",
                            OwnerAbsent = 0
                        }
            };

                var result = new ResultViewModel
                {
                    Validate = true,
                    Message = JsonConvert.SerializeObject(lstOwnerPortfolio),
                    ExceptionMessage = "",
                    ValidateMessage = ""
                };
                return JsonConvert.SerializeObject(result);
            }
           catch (Exception ex)
           {

                var result = new ResultViewModel
                {
                    Validate = false,
                    Message = "0",
                    ExceptionMessage = ex.Message,
                    ValidateMessage = ""
                };
                return JsonConvert.SerializeObject(result);
            }
       }

       public string ModifyWorkTableChkShAll_Mobile(string actorpid, string CartablOwnerCode, string StartDate, string EndDate,
           string Requester, string DocType, string StrFilter, string PageNumber, string SessionID)
       {
           try
           {
                var portfolioMng=new PortfolioManager(new WorkFlowsDatabase());
               var resultModel = portfolioMng.AcceptAllPortfolio(actorpid, CartablOwnerCode, StartDate, EndDate,
                   Requester, DocType, "", "", StrFilter, "", PageNumber, int.Parse(SessionID), "en");
               return JsonConvert.SerializeObject(resultModel);
            }
           catch (Exception ex)
           {
                var result = new ResultViewModel
                {
                    Validate = false,
                    Message = "0",
                    ExceptionMessage = ex.Message,
                    ValidateMessage = ""
                };
                return JsonConvert.SerializeObject(result);

            }
       }

       public string InsertAttendance_Mobile(string Time, string Date, int PersonelID, int CardKhanNo)
       {
           throw new NotImplementedException();
       }

       public string GetAttendancePastil_Mobile(string Date, int PersonelID)
       {
           throw new NotImplementedException();
       }

       public string GetTranslate_Mobile(string LanguageKey)
       {
           try
           {
                var jsonTranslate = "{";
                jsonTranslate = jsonTranslate + "\"Portfolio\":\"" + "کارتابل" + "\"";
                jsonTranslate = jsonTranslate + ",\"PORTFOLIO\":\"" + "کارتابل" + "\"";
                jsonTranslate = jsonTranslate + ",\"YES\":\"" + "بله" + "\"";
                jsonTranslate = jsonTranslate + ",\"NO\":\"" + "خیر" + "\"";
                jsonTranslate = jsonTranslate + ",\"VIEW CREDITS\":\"" + "نمایش مجوز ها" + "\"";
                jsonTranslate = jsonTranslate + ",\"ERROR\":\"" + "خطا" + "\"";
                jsonTranslate = jsonTranslate + ",\"EMPTY_MESSAGE\":\"" + "اطلاعاتی جهت نمایش وجود ندارد" + "\"";
                jsonTranslate = jsonTranslate + ",\"EXIT_MESSAGE\":\"" + "خروج" + "\"";
                jsonTranslate = jsonTranslate + ",\"Save_Attendance\":\"" + "ذخیره تردد" + "\"";
                jsonTranslate = jsonTranslate + ",\"Req_License\":\"" + "درخواست مجوز" + "\"";
                jsonTranslate = jsonTranslate + ",\"EXIT_APP\":\"" + "خروج از برنامه" + "\"";
                jsonTranslate = jsonTranslate + ",\"EXIT\":\"" + "خروج" + "\"";
                jsonTranslate = jsonTranslate + ",\"IP_ADDRESS_IS_INCORRECT\":\"" + "آدرس اشتباه است" + "\"";
                jsonTranslate = jsonTranslate + ",\"ERROR_NEW_VERSION\":\"" + "خطا نسخه جدید" + "\"";
                jsonTranslate = jsonTranslate + ",\"ERROR_SERVER_CONNECTION\":\"" + "خطا در سرویس دهنده" + "\"";
                jsonTranslate = jsonTranslate + ",\"ERROR_INTERNET_CONNECTION\":\"" + "خطا در ارتباط با اینترنت" + "\"";
                jsonTranslate = jsonTranslate + ",\"SERVER_NOT_FOUND\":\"" + "سرویس دهنده یافت نشد" + "\"";
                jsonTranslate = jsonTranslate + ",\"LOADING\":\"" + "درحال بارگذاری" + "\"";
                jsonTranslate = jsonTranslate + ",\"Sum\":\"" + "جمع" + "\"";
                jsonTranslate = jsonTranslate + ",\"TITLE\":\"" + "عنوان" + "\"";
                jsonTranslate = jsonTranslate + ",\"Till\":\"" + "تا" + "\"";
                jsonTranslate = jsonTranslate + ",\"From\":\"" + "از" + "\"";
                jsonTranslate = jsonTranslate + ",\"HOUR\":\"" + "ساعت" + "\"";
                jsonTranslate = jsonTranslate + ",\"DAY\":\"" + "روز" + "\"";
                jsonTranslate = jsonTranslate + ",\"TIME\":\"" + "ساعت" + "\"";
                jsonTranslate = jsonTranslate + ",\"VIEW_DASHBOARD\":\"" + "نمایش کارتابل" + "\"";
                jsonTranslate = jsonTranslate + ",\"DESCRIPTION\":\"" + "شرح" + "\"";
                jsonTranslate = jsonTranslate + ",\"Description\":\"" + "وضعیت" + "\"";
                jsonTranslate = jsonTranslate + ",\"CANCEL\":\"" + "لغو" + "\"";
                jsonTranslate = jsonTranslate + ",\"SAVE\":\"" + "ذخیره" + "\"";
                jsonTranslate = jsonTranslate + ",\"SET\":\"" + "انتخاب" + "\"";
                jsonTranslate = jsonTranslate + ",\"TODAY\":\"" + "امروز" + "\"";
                jsonTranslate = jsonTranslate + ",\"CLOSE\":\"" + "بستن" + "\"";
                jsonTranslate = jsonTranslate + ",\"PERIOD\":\"" + "دوره" + "\"";
                jsonTranslate = jsonTranslate + ",\"Please_Enter_Server_Address\":\"" + "لطفا آدرس سرویس دهنده را وارد کنید" + "\"";
                jsonTranslate = jsonTranslate + ",\"SATURDAY\":\"" + "شنبه" + "\"";
                jsonTranslate = jsonTranslate + ",\"SUNDAY\":\"" + "یک شنبه" + "\"";
                jsonTranslate = jsonTranslate + ",\"MONDAY\":\"" + "دوشنبه" + "\"";
                jsonTranslate = jsonTranslate + ",\"TUESDAY\":\"" + "سه شنبه" + "\"";
                jsonTranslate = jsonTranslate + ",\"WEDNSDAY\":\"" + "چهارشنبه" + "\"";
                jsonTranslate = jsonTranslate + ",\"THURSDAY\":\"" + "پنج شنبه" + "\"";
                jsonTranslate = jsonTranslate + ",\"FRIDAY\":\"" + "جمعه" + "\"";
                jsonTranslate = jsonTranslate + ",\"WAIT_FINE_SERVER\":\"" + "درحال یافتن سرویس دهنده" + "\"";
                jsonTranslate = jsonTranslate + ",\"CONFIRM_ACCEPT_ALL_PORTFOLIO\":\"" + "آیا مایل به تایید تمام کارتابل خود هستید؟" + "\"";
                jsonTranslate = jsonTranslate + ",\"Credits Details and WorkFlow\":\"" + "روندگردش کار مجوز" + "\"";
                jsonTranslate = jsonTranslate + ",\"Calendar Type\":\"" + "نوع تقویم" + "\"";
                jsonTranslate = jsonTranslate + ",\"MESSAGE\":\"" + "پیغام" + "\"";
                jsonTranslate = jsonTranslate + ",\"Change password\":\"" + "تغییر رمز عبور" + "\"";
                jsonTranslate = jsonTranslate + ",\"Change IP\":\"" + "تغییرای پی " + "\"";
                jsonTranslate = jsonTranslate + ",\"Current password\":\"" + "رمز جاری" + "\"";
                jsonTranslate = jsonTranslate + ",\"Current IP\":\"" + "آدرس سرویس دهنده جاری" + "\"";
                jsonTranslate = jsonTranslate + ",\"New password\":\"" + "رمز جدید" + "\"";
                jsonTranslate = jsonTranslate + ",\"New IP\":\"" + "آدرس جدید" + "\"";
                jsonTranslate = jsonTranslate + ",\"Confirm new password\":\"" + "تایید رمز جدید" + "\"";
                jsonTranslate = jsonTranslate + ",\"Settings\":\"" + "تنظیمات" + "\"";
                jsonTranslate = jsonTranslate + ",\"Username\":\"" + "نام کاربری" + "\"";
                jsonTranslate = jsonTranslate + ",\"Wednesday\":\"" + "چهارشنبه" + "\"";
                jsonTranslate = jsonTranslate + ",\"Attendance\":\"" + "تردد" + "\"";
                jsonTranslate = jsonTranslate + ",\"Index Card\":\"" + "کاردکس" + "\"";
                jsonTranslate = jsonTranslate + ",\"Return\":\"" + "بازگشت" + "\"";
                jsonTranslate = jsonTranslate + ",\"Save Attendance\":\"" + "ذخیره تردد" + "\"";
                jsonTranslate = jsonTranslate + ",\"Show Credits\":\"" + "نمایش مجوز ها" + "\"";
                jsonTranslate = jsonTranslate + ",\"Date\":\"" + "تاریخ" + "\"";
                jsonTranslate = jsonTranslate + ",\"Attendance Time\":\"" + "زمان تردد" + "\"";
                jsonTranslate = jsonTranslate + ",\"Minute\":\"" + "دقیقه" + "\"";
                jsonTranslate = jsonTranslate + ",\"Description :\":\"" + "شرح" + "\"";
                jsonTranslate = jsonTranslate + ",\"Send\":\"" + "ارسال" + "\"";
                jsonTranslate = jsonTranslate + ",\"Save Request\":\"" + "ذخیره درخواست" + "\"";
                jsonTranslate = jsonTranslate + ",\"From Date\":\"" + "از تاریخ" + "\"";
                jsonTranslate = jsonTranslate + ",\"Credit Type\":\"" + "نوع مجوز" + "\"";
                jsonTranslate = jsonTranslate + ",\"Meritorious Hourly Credit\":\"" + "مجوز ساعتی " + "\"";
                jsonTranslate = jsonTranslate + ",\"Hourly OverTime Credit\":\"" + "مجوز اضافه کار ساعتی" + "\"";
                jsonTranslate = jsonTranslate + ",\"Meritorious Daily Credit\":\"" + "مجوز روزانه" + "\"";
                jsonTranslate = jsonTranslate + ",\"Daily OverTime Credit\":\"" + "مجوز اضافه کار روزانه" + "\"";
                jsonTranslate = jsonTranslate + ",\"Meritorious Credits\":\"" + "مجوز" + "\"";
                jsonTranslate = jsonTranslate + ",\"Credit Title\":\"" + "عنوان مجوز" + "\"";
                jsonTranslate = jsonTranslate + ",\"Till Date\":\"" + "تا تاریخ" + "\"";
                jsonTranslate = jsonTranslate + ",\"From Time\":\"" + "از تاریخ" + "\"";
                jsonTranslate = jsonTranslate + ",\"To Time\":\"" + "تا تاریخ" + "\"";
                jsonTranslate = jsonTranslate + ",\"Sum Total Of Day Result\":\"" + "مجموع روزها" + "\"";
                jsonTranslate = jsonTranslate + ",\"No Result Calculated\":\"" + "هیچ داده ای پردازش نشده است" + "\"";
                jsonTranslate = jsonTranslate + ",\"Daily Attendances\":\"" + "تردد روزانه" + "\"";
                jsonTranslate = jsonTranslate + ",\"No Attendance Saved\":\"" + "هیچ ترددی ذخیره نشد" + "\"";
                jsonTranslate = jsonTranslate + ",\"Credit Request\":\"" + "ثبت مجوز" + "\"";
                jsonTranslate = jsonTranslate + ",\"Occasions\":\"" + "مناسبت" + "\"";
                jsonTranslate = jsonTranslate + ",\"Shift\":\"" + "شیفت کاری" + "\"";
                jsonTranslate = jsonTranslate + ",\"No Shifted Structure Seen\":\"" + "هیچ شیفتی مشاهده نشد" + "\"";
                jsonTranslate = jsonTranslate + ",\"Quick Software Entry\":\"" + "تردد سریع" + "\"";
                jsonTranslate = jsonTranslate + ",\"Login To Software\":\"" + "ورود به نرم افزار" + "\"";
                jsonTranslate = jsonTranslate + ",\"Close Help\":\"" + "بستن راهنما" + "\"";
                jsonTranslate = jsonTranslate + ",\"Help\":\"" + "راهنما" + "\"";
                jsonTranslate = jsonTranslate + ",\"Check\":\"" + "چک" + "\"";
                jsonTranslate = jsonTranslate + ",\"Time Attendance System\":\"" + "سیستم حضور و غیاب" + "\"";
                jsonTranslate = jsonTranslate + ",\"Connection to Server\":\"" + "ارتباط با سرویس دهنده" + "\"";
                jsonTranslate = jsonTranslate + ",\"Karsa Mobile\":\"" + "سیستم موبایل" + "\"";
                jsonTranslate = jsonTranslate + ",\"Password\":\"" + "رمز عبور" + "\"";
                jsonTranslate = jsonTranslate + ",\"Login\":\"" + "ورود" + "\"";
                jsonTranslate = jsonTranslate + ",\"There Is No Data for this Period\":\"" + "اطلاعاتی برای این دوره وجود ندارد" + "\"";
                jsonTranslate = jsonTranslate + ",\"Periods\":\"" + "دوره" + "\"";
                jsonTranslate = jsonTranslate + ",\"Sum Of Periods\":\"" + "جمع دوره" + "\"";
                jsonTranslate = jsonTranslate + ",\"Detailed Period\":\"" + "جزئیات" + "\"";
                jsonTranslate = jsonTranslate + ",\"Requester :\":\"" + "درخواست دهنده" + "\"";
                jsonTranslate = jsonTranslate + ",\"Incorrect IP Address\":\"" + "آدرس اشتباه است" + "\"";
                jsonTranslate = jsonTranslate + ",\"Loading…\":\"" + "درحال بارگذاری" + "\"";
                jsonTranslate = jsonTranslate + ",\"View Portfolio\":\"" + "نمایش کارتابل" + "\"";
                jsonTranslate = jsonTranslate + ",\"Document Description\":\"" + "سرح مجوز" + "\"";
                jsonTranslate = jsonTranslate + ",\"Confirmation\":\"" + "تایید" + "\"";
                jsonTranslate = jsonTranslate + ",\"refuse\":\"" + "عدم تایید" + "\"";
                jsonTranslate = jsonTranslate + ",\"refer\":\"" + "ارجاع" + "\"";
                jsonTranslate = jsonTranslate + ",\"Work Flow\":\"" + "گردش کار" + "\"";
                jsonTranslate = jsonTranslate + ",\"End\":\"" + "پایان" + "\"";
                jsonTranslate = jsonTranslate + ",\"Person Code\":\"" + "کد پرسنلی" + "\"";
                jsonTranslate = jsonTranslate + ",\"Personnel Access\":\"" + "پرسنلی" + "\"";
                jsonTranslate = jsonTranslate + ",\"Document Type\":\"" + "نوع سند" + "\"";
                jsonTranslate = jsonTranslate + ",\"Credit Code\":\"" + "کد درخواست" + "\"";
                jsonTranslate = jsonTranslate + ",\"Filter\":\"" + "فیلتر" + "\"";
                jsonTranslate = jsonTranslate + ",\"Search\":\"" + "جستجو" + "\"";
                jsonTranslate = jsonTranslate + ",\"Start\":\"" + "شروع" + "\"";
                jsonTranslate = jsonTranslate + ",\"Added\":\"" + "اضافه شده" + "\"";
                jsonTranslate = jsonTranslate + ",\"Reduced\":\"" + "کسر شده" + "\"";
                jsonTranslate = jsonTranslate + ",\"Result\":\"" + "مانده مرخصی" + "\"";
                jsonTranslate = jsonTranslate + ",\"Must Use\":\"" + "ملزم به استفاده" + "\"";
                jsonTranslate = jsonTranslate + ",\"By Swiping  Left And Right, You Can See The Next and Previous Period\":\"" + "با کشیدن به چپ و راست میتوانید اطلاعات دورههای بعدی و قبلی را مشاهده کنید" + "\"";
                jsonTranslate = jsonTranslate + ",\"By Tapping On Period Title You Can See Periods List\":\"" + "با لمس نام دوره لیست کلیه دورهها را مشاهده کنید" + "\"";
                jsonTranslate = jsonTranslate + ",\"By Tapping On The Icon Of Period Calendar Status, You Can Switch Between Listed And Tabled View\":\"" + "با لمس نشانه بالای تقویم میتوانید نوع نمایش را تغییر دهید" + "\"";
                jsonTranslate = jsonTranslate + ",\"To Access Menu, You Can Tap On Menu Icon\":\"" + "جهت دسترسی به منو ها میتوانید  نشانه منو را لمس کنید" + "\"";
                jsonTranslate = jsonTranslate +
                                ",\"Orange Triangle In Calendar Shows The Intended Day Needs SaveRequest\":\"" + "\"";
                jsonTranslate = jsonTranslate + ",\"To See Data Details Of A Day, You Can Tap On The Day In Calendar\":\"" + "\"";
                jsonTranslate = jsonTranslate + ",\"You Can Save Request In Day Page By Tapping On Day Result\":\"" + "\"";
                jsonTranslate = jsonTranslate + ",\"You Can Also Save Request And Attendance In Day Page By Tapping On  Their Buttons\":\"" + "\"";
                jsonTranslate = jsonTranslate + ",\"You Can Save Request And Attendance On Software Menu\":\"" + "\"";
                jsonTranslate = jsonTranslate + ",\"To Log Out Of Software Tap On Exit\":\"" + "\"";
                jsonTranslate = jsonTranslate + ",\"To Date\":\"" + "تا تاریخ" + "\"";
                jsonTranslate = jsonTranslate + ",\"Kasra Time Attendance\":\"" + "سیستم حضور و غیاب" + "\"";
                jsonTranslate = jsonTranslate + ",\"OK\":\"" + "تایید" + "\"";
                jsonTranslate = jsonTranslate + ",\"Accepter\":\"" + "تایید کننده" + "\"";
                jsonTranslate = jsonTranslate + ",\"Refer To\":\"" + "ارجاع به" + "\"";
                jsonTranslate = jsonTranslate + ",\"Deleted by\":\"" + "حذف توسط" + "\"";
                jsonTranslate = jsonTranslate + ",\"Document State\":\"" + "وضعیت سند" + "\"";
                jsonTranslate = jsonTranslate + ",\"Accepted\":\"" + "تایید شده" + "\"";
                jsonTranslate = jsonTranslate + ",\"In Progress\":\"" + "درجریان" + "\"";
                jsonTranslate = jsonTranslate + ",\"Deleted\":\"" + "حذف" + "\"";
                jsonTranslate = jsonTranslate + ",\"Request Code\":\"" + "شناسه درخواست" + "\"";
                jsonTranslate = jsonTranslate + ",\"Hourly Merit\":\"" + "تشویقی ساعتی" + "\"";
                jsonTranslate = jsonTranslate + ",\"Hourly OverTime\":\"" + "اضاف کار ساعتی" + "\"";
                jsonTranslate = jsonTranslate + ",\"Daily Merit\":\"" + "تشویقی روزانه" + "\"";
                jsonTranslate = jsonTranslate + ",\"Save Group Request\":\"" + "ذخیره درخواست گروهی" + "\"";
                jsonTranslate = jsonTranslate + ",\"Set Manually\":\"" + "تایید دستی" + "\"";
                jsonTranslate = jsonTranslate + ",\"To scan your QR - code touch the below or enter your server IP\":\"" + "\"";
                jsonTranslate = jsonTranslate + ",\"Daily Deficit\":\"" + "کسر روزانه" + "\"";
                jsonTranslate = jsonTranslate + ",\"Submission Portfolio :\":\"" + "تفویض کارتابل" + "\"";
                jsonTranslate = jsonTranslate + ",\"You can change server IP that you connected to from this page\":\"" + "\"";
                jsonTranslate = jsonTranslate + ",\"Hourly Deficit\":\"" + "کسر ساعتی" + "\"";
                jsonTranslate = jsonTranslate + ",\"Delete\":\"" + "حذف" + "\"";
                jsonTranslate = jsonTranslate + ",\"Date :\":\"" + "تاریخ" + "\"";
                jsonTranslate = jsonTranslate + ",\"Start Time :\":\"" + "زمان شروع" + "\"";
                jsonTranslate = jsonTranslate + ",\"End Time :\":\"" + "زمان پایان" + "\"";
                jsonTranslate = jsonTranslate + ",\"Show Credits Of This Person\":\"" + "نمایش مجوز های این کاربر" + "\"";
                jsonTranslate = jsonTranslate + ",\"Accept All\":\"" + "تایید همه" + "\"";
                jsonTranslate = jsonTranslate + ",\"Credits Of This Person\":\"" + "مجوز های کاربر" + "\"";
                jsonTranslate = jsonTranslate + ",\"Credits Send Time :\":\"" + "زمان ارسال مجوز" + "\"";
                jsonTranslate = jsonTranslate + ",\"Credits Date :\":\"" + "تاریخ انجام عملیات" + "\"";
                jsonTranslate = jsonTranslate + ",\"Sent to :\":\"" + "" + "\"";
                jsonTranslate = jsonTranslate + ",\"Sender :\":\"" + "" + "\"";
                jsonTranslate = jsonTranslate + ",\"more\":\"" + "روند" + "\"";
                jsonTranslate = jsonTranslate + ",\"Docid\":\"" + "شناسه درخواست" + "\"";
                jsonTranslate = jsonTranslate + ",\"ActionTitle\":\"" + "عنوان درخواست" + "\"";
                jsonTranslate = jsonTranslate + ",\"State\":\"" + "وضعیت" + "\"";
                jsonTranslate = jsonTranslate + ",\"StateTitle\":\"" + "عنوان وضعیت" + "\"";
                jsonTranslate = jsonTranslate + ",\"PersonID\":\"" + "شناسه فرد" + "\"";
                jsonTranslate = jsonTranslate + ",\"PersonName\":\"" + "نام فرد" + "\"";
                jsonTranslate = jsonTranslate + ",\"RoleName\":\"" + "نام " + "\"";
                jsonTranslate = jsonTranslate + ",\"RoleName :\":\"" + "نام" + "\"";
                jsonTranslate = jsonTranslate + ",\"RPName\":\"" + "نام" + "\"";
                jsonTranslate = jsonTranslate + ",\"R_P_ID\":\"" + "شناسه" + "\"";
                jsonTranslate = jsonTranslate + ",\"ActionTime\":\"" + "زمان عملیات" + "\"";
                jsonTranslate = jsonTranslate + ",\"ActionDate\":\"" + "تاریخ عملیات" + "\"";
                jsonTranslate = jsonTranslate + ",\"Descr\":\"" + "شرح" + "\"";
                jsonTranslate = jsonTranslate + ",\"StepNo\":\"" + "مرحله" + "\"";
                jsonTranslate = jsonTranslate + ",\"ClientIP\":\"" + "شناسه مشتری" + "\"";
                jsonTranslate = jsonTranslate + ",\"EffectiveScripts\":\"" + "اسکریپت موئثر" + "\"";
                jsonTranslate = jsonTranslate + ",\"ActorPID\":\"" + "انجام دهنده" + "\"";
                jsonTranslate = jsonTranslate + ",\"requesterDes\":\"" + "شرح درخواست" + "\"";
                jsonTranslate = jsonTranslate + ",\"DevActorPID\":\"" + "انام دهنده" + "\"";
                jsonTranslate = jsonTranslate + ",\"Confirm All\":\"" + "تایید همه" + "\"";
                jsonTranslate = jsonTranslate + ",\"undefined\":\"" + "نا مشخص" + "\"";
                jsonTranslate = jsonTranslate + ",\"DONE_SUCCESSFULLY\":\"" + "با موفقیت انجام شد" + "\"";
                jsonTranslate = jsonTranslate + ",\"moveUp\":\"" + "ارجاع" + "\"";
                jsonTranslate = jsonTranslate + ",\"CONFIRM\":\"" + "تاییدیه" + "\"";
                jsonTranslate = jsonTranslate + ",\"DENIED\":\"" + "عدم تایید" + "\"";
                jsonTranslate = jsonTranslate + ",\"ACCEPT\":\"" + "تایید" + "\"";
                jsonTranslate = jsonTranslate + ",\"DELETE_CREDIT_CONFERM_MESSAGE\":\"" + "آیا مایل به حذف مجوز ها هستید؟" + "\"";
                jsonTranslate = jsonTranslate + ",\"View Credits\":\"" + "نمایش مجوز ها" + "\"";
                jsonTranslate = jsonTranslate + ",\"ERORR_IN_CHOISE_FILE\":\"" + "خطا در انتخاب فایل" + "\"";
                jsonTranslate = jsonTranslate + ",\"CLOSE\":\"" + "بستن" + "\"";
                jsonTranslate = jsonTranslate + ",\"ATTACHMENT_TITLE\":\"" + "انتخاب عکس" + "\"";
                jsonTranslate = jsonTranslate + ",\"CAMERA\":\"" + "دوربین" + "\"";
                jsonTranslate = jsonTranslate + ",\"GALLERY\":\"" + "گالری" + "\"";
                jsonTranslate = jsonTranslate + ",\"Day\":\"" + "ساعت" + "\"";
                jsonTranslate = jsonTranslate + ",\"Rejected by\":\"" + "عدم تایید توسط" + "\"";
                jsonTranslate = jsonTranslate + "}";
                var result = new ResultViewModel
                {
                    Validate = true,
                    Message = jsonTranslate
                };
                return JsonConvert.SerializeObject(result);
            }
           catch (Exception ex)
           {
               
               var result=new ResultViewModel
               {
                   Validate = false,
                   Message =ex.Message
               };
               return JsonConvert.SerializeObject(result);
           }
       }

       public string GetCreditType_Mobile(int OnlineUserID)
       {
           try
           {
                var lstRequest=new List<RequestModel>();
                //var constre = ConfigurationManager.ConnectionStrings["connectionStringProviderSystem"].ToString();
                //var connection = new SqlConnection(constre);

                //if (connection.State == ConnectionState.Open)
                //{
                //    connection.Close();
                //}

                //string query = @"select ID,TITLE,IS_DAY,CARD_NO from CARDS";

                //connection.Open();
                //var command = new SqlCommand(query, connection);
               

                //var result = command.ExecuteReader();
                //while (result.Read())
                //{
                //   var request=new RequestModel
                //   {
                //       Id = result["CARD_NO"].ToString(),
                //       Title =  result["TITLE"].ToString(),
                //       IsDaily = bool.Parse(result["IS_DAY"].ToString())
                //   };
                //    lstRequest.Add(request);
                //}
                //connection.Close();
                //connection.Dispose();
                var reqstmng=new RequestFormLoader();
               var lstTypeRequest = reqstmng.GetUserRequestTypes(OnlineUserID.ToString());
               var lstRequestTypes = lstTypeRequest.Select(x => new CreditTypeViewModel
               {
                   Id = x.Id,
                   Name = x.Title,
                   CodePermissions = reqstmng.GetFormCodesForModel(OnlineUserID.ToString(),x.Id)
               }).ToList();
                //var lstRequestTypes=new List<CreditTypeViewModel>
                //{
                //    new CreditTypeViewModel
                //    {
                //        Id = "1",
                //        Name = "روزانه",
                //        CodePermissions = lstRequest.Where(x=>x.IsDaily && (x.Id=="57"||x.Id=="58"||x.Id=="59"||x.Id=="60")).Select(x=>new GetCodePermissionMobileViewModel
                //        {
                //            CodeID = x.Id.ToString(),
                //            CodeName = x.Title.ToString()
                //        }).ToList()
                //    },new CreditTypeViewModel
                //    {
                //        Id = "3",
                //        Name = "ساعتی",
                //        CodePermissions = lstRequest.Where(x=>!x.IsDaily && x.Id=="17").Select(x=>new GetCodePermissionMobileViewModel
                //        {
                //            CodeID = x.Id.ToString(),
                //            CodeName = x.Title.ToString()
                //        }).ToList()
                //    }
                //};
                var generalresult=new ResultViewModel
                {
                    Validate = true,
                    Message = JsonConvert.SerializeObject(lstRequestTypes)
                };
               return JsonConvert.SerializeObject(generalresult);

           }
           catch (Exception ex)
           {
               
               var result=new ResultViewModel
               {
                   Validate = false,
                   ValidateMessage = ex.Message
               };
               return JsonConvert.SerializeObject(result);
           }
       }

       public string GetHelpImage_Mobile(string Version, string Language)
       {
           throw new NotImplementedException();
       }

       public string DocumentState_Mobile()
       {
           try
           {
               var inprogress = new
               {
                   id = "201",
                   name = "در جریان"
               };
                var accept = new
                {
                    id = "203",
                    name = "تایید"
                };
                var reject = new
                {
                    id = "205",
                    name = "عدم تایید"
                };
               var lstobj = new List<object>();

                lstobj.Add(inprogress);
                lstobj.Add(accept);
                lstobj.Add(reject);
                var jsonStatus = JsonConvert.SerializeObject(lstobj);
                var result = new ResultViewModel
                {
                    Validate = true,
                    Message = jsonStatus
                };
             
                return JsonConvert.SerializeObject(result);
            }
           catch (Exception ex)
           {

                var result = new ResultViewModel
                {
                    Validate = false,
                    ValidateMessage = ex.Message
                };
                return JsonConvert.SerializeObject(result);
            }
       }

       public string GetFormFields_Mobile(string CodeID, string AutoCompleteParams, string SessionID, string personCode, string callFrom)
       {
           try
           {
               bool lockPersonCombo = callFrom != "2";
                var splitedCodes = CodeID.Split('-');
               var actionId = splitedCodes[0];
               var selectedCodeId = splitedCodes[1];
                List<FormFieldsMobileViewModel> list = new List<FormFieldsMobileViewModel>();
               var selectedDate = DateTime.Now.Date;
               if (AutoCompleteParams != "\n      ")
               {
                   var objectAutoParam = JObject.Parse(AutoCompleteParams);
                    selectedDate=DateTime.Parse(objectAutoParam["FromDate"].ToString());
                    
               }
              
                
                var requestfrmManager = new RequestFormLoader();
                switch (actionId)
               {
                    case "4":
                       list = requestfrmManager.GetIoCorrectionForm(personCode, null, lockPersonCombo, selectedDate);
                        break;
                    case "5":
                        list = requestfrmManager.GetOverTimeForm(personCode,null,lockPersonCombo, selectedDate);
                        break;
                    case "7":
                        list = requestfrmManager.GetAddReduceHolidaysForm(personCode, null, lockPersonCombo, selectedDate);
                        break;
                    case "8":
                        list = requestfrmManager.GetForGetAttendanceForm(personCode, null, lockPersonCombo, selectedDate);
                        break;
                    case "13":
                        list = requestfrmManager.GetIoTypeCorrectionForm(personCode, null, lockPersonCombo, selectedDate);
                        break;
                    case "1":
                        list = requestfrmManager.GetNormalDailyForm(personCode, null, lockPersonCombo, selectedDate);
                        break;
                    case "2":
                        list = requestfrmManager.GetMissonDailyForm(personCode, null, lockPersonCombo, selectedDate);
                        break;
                    case "3":
                        list = requestfrmManager.GetNormalHrlyForm(personCode, null, lockPersonCombo, selectedDate);
                        break;
                    case "9":
                        list = requestfrmManager.GetMissonHrlyForm(personCode, null, lockPersonCombo, selectedDate);
                       break;
                    case "12":
                            list = requestfrmManager.GetMissonHrlyWhouthReturnForm(personCode, null, lockPersonCombo, selectedDate);
                        break;
                }
                var resultGeneral = new ResultViewModel
                {
                    Validate = true,
                    Message = JsonConvert.SerializeObject(list),
                    ValidateMessage = ""
                };
                return JsonConvert.SerializeObject(resultGeneral);
            }
           catch (Exception ex)
           {
               
               var result=new ResultViewModel
               {
                   Validate = false,
                   ValidateMessage = ""
               };
               return JsonConvert.SerializeObject(result);
           }
       }
       
        public string GetFormFieldsEdit_Mobile(string requestId, string SessionID)
        {
            var requestfrmManager = new RequestFormLoader();
            var result = requestfrmManager.GetFormFieldForEdit(SessionID, requestId);
            return JsonConvert.SerializeObject(result);
        }

        public string GetLocationsPastil_Mobile(string PersonID)
       {
           throw new NotImplementedException();
       }

       public string ModifyEditCreditByFormBuilder_Mobile(int PersonCode, int JPersonCode, string requestId, int OnlineUserID,
           int SessionID,
           string FormJson, string XmlRoot)
       {
           try
           {
                var registerform=new RegisterRequests();
               var dataResult = registerform.SaveFormForEdit(PersonCode, JPersonCode, requestId, OnlineUserID,
                   SessionID,
                   FormJson, XmlRoot);
                //var result = new ResultViewModel
                //{
                //    Validate = true,
                //    ValidateMessage = JsonConvert.SerializeObject(dataResult)
                //};
                return JsonConvert.SerializeObject(dataResult);
            }
           catch (Exception ex)
           {
                var result = new ResultViewModel
                {
                    Validate = false,
                    ValidateMessage = ex.Message
                };
                return JsonConvert.SerializeObject(result);
            }
       }

       public string ModifyCreditByFormBuilder_Mobile(int PersonCode, int JPersonCode, string CreditID, int OnlineUserID, int SessionID,
           string FormJson, string XmlRoot)
       {
           try
           {
               var jsonFormModel = JsonConvert.DeserializeObject<List<FormFrieldsValuesMobileViewModel>>(FormJson);
                var lstPresons=new List<int>();
                var lstResult=new List<ResultViewModel>();
                var splitedCodes = CreditID.Split('-');
                var actionId = splitedCodes[0];
                var selectedCodeId = splitedCodes[1];
                List<FormFieldsMobileViewModel> list = new List<FormFieldsMobileViewModel>();
                var requestfrmManager = new RequestFormLoader();
               var selectedPersonCode = "";
              
                    var objPersonCombo=jsonFormModel.FirstOrDefault(x => x.ID == "99");
                   if (objPersonCombo != null)
                   {
                       selectedPersonCode = objPersonCombo.Value;
                       if (selectedPersonCode == "")
                       {
                        var result = new ResultViewModel
                        {
                            Validate = false,
                            ValidateMessage = "لطفا یک پرسنل را انتخاب کنید",
                            ExceptionMessage = "لطفا یک پرسنل را انتخاب کنید"
                        };
                        return JsonConvert.SerializeObject(result);
                    }
                       if (!selectedPersonCode.Contains(','))
                       {
                           //PersonCode = Int32.Parse(selectedPersonCode);
                           lstPresons.Add(Int32.Parse(selectedPersonCode));
                       }
                       else
                       {
                        //selectedPersonCode = selectedPersonCode.Replace('[', ' ');
                        //selectedPersonCode = selectedPersonCode.Replace(']', ' ');
                           var lstAllNumbers = selectedPersonCode.Split(',');
                        lstPresons = lstAllNumbers.Select(c => Convert.ToInt32(c.ToString())).ToList();
                    }
                   }
               foreach (var personChecked in lstPresons)
               {
                   PersonCode = personChecked;


                   switch (actionId)
                   {
                       case "4":
                           var startDateIoCorrect = jsonFormModel.FirstOrDefault(x => x.ID == "1");
                           var selectedTimeIoCorrect = jsonFormModel.FirstOrDefault(x => x.ID == "3");
                           var newSelectedTimeIoCorrect = jsonFormModel.FirstOrDefault(x => x.ID == "4");
                           var requestTitleIoCorrect = jsonFormModel.FirstOrDefault(x => x.ID == "9");
                           var descriptionIoCorrect = jsonFormModel.FirstOrDefault(x => x.ID == "8");
                           if (startDateIoCorrect != null && selectedTimeIoCorrect != null &&
                               newSelectedTimeIoCorrect != null &&
                               requestTitleIoCorrect != null && descriptionIoCorrect != null)
                           {
                               var requstMng = new RegisterRequests();
                               var resultRequest =
                                   requstMng.RegisterForCorectIoRequest(double.Parse(PersonCode.ToString()),
                                       (short) int.Parse(selectedCodeId),
                                       Convert.ToDateTime(startDateIoCorrect.Value), selectedTimeIoCorrect.Value
                                       , newSelectedTimeIoCorrect.Value, descriptionIoCorrect.Value,
                                       requestTitleIoCorrect.Value, OnlineUserID.ToString());

                                lstResult.Add(resultRequest);
                           }
                           //list = requestfrmManager.GetIoCorrectionForm(PersonCode.ToString());
                           break;
                       case "5":
                           var startDateOverTime = jsonFormModel.FirstOrDefault(x => x.ID == "1");
                           var endDateOverTime = jsonFormModel.FirstOrDefault(x => x.ID == "2");
                           var endTimeOverTime = jsonFormModel.FirstOrDefault(x => x.ID == "3");
                           var descriptionOverTime = jsonFormModel.FirstOrDefault(x => x.ID == "8");
                           if (startDateOverTime != null && endDateOverTime != null && endTimeOverTime != null &&
                               descriptionOverTime != null)
                           {
                               var requstMng = new RegisterRequests();
                               var resultValidate =
                                   requstMng.ValidParamOverTimeRequest(double.Parse(PersonCode.ToString()),Convert.ToDateTime(startDateOverTime.Value),
                                       Convert.ToDateTime(endDateOverTime.Value), descriptionOverTime.Value);
                               if (resultValidate.Validate)
                               {
                                   var resultRequest = requstMng.RegisterOverTimeRequest(PersonCode.ToString(),
                                       selectedCodeId,
                                       Convert.ToDateTime(startDateOverTime.Value),
                                       Convert.ToDateTime(endDateOverTime.Value), endTimeOverTime.Value,
                                       descriptionOverTime.Value, OnlineUserID.ToString());

                                    lstResult.Add(resultRequest);
                                    break;
                                }
                                lstResult.Add(resultValidate);
                           }
                           //list = requestfrmManager.GetOverTimeForm();
                           break;
                       case "7":
                           //list = requestfrmManager.GetAddReduceHolidaysForm();
                           var startDateAddReduce = jsonFormModel.FirstOrDefault(x => x.ID == "1");
                           var endTimeAddReduce = jsonFormModel.FirstOrDefault(x => x.ID == "3");
                           var typeAddReduce = jsonFormModel.FirstOrDefault(x => x.ID == "9");
                           var descriptionAddReduce = jsonFormModel.FirstOrDefault(x => x.ID == "8");
                           if (startDateAddReduce != null && endTimeAddReduce != null && typeAddReduce != null &&
                               descriptionAddReduce != null)
                           {
                               var requstMng = new RegisterRequests();
                               var resultRequest = requstMng.RegisterAddReduceHolidays(PersonCode.ToString(),
                                   selectedCodeId,
                                   Convert.ToDateTime(startDateAddReduce.Value), endTimeAddReduce.Value,
                                   typeAddReduce.Value,
                                   descriptionAddReduce.Value, OnlineUserID.ToString());

                                lstResult.Add(resultRequest);
                           }
                           break;
                       case "8":
                           //list = requestfrmManager.GetForGetAttendanceForm();
                           var startDateForget = jsonFormModel.FirstOrDefault(x => x.ID == "1");
                           var selectedTimeForget = jsonFormModel.FirstOrDefault(x => x.ID == "3");
                           var descriptionForget = jsonFormModel.FirstOrDefault(x => x.ID == "8");
                           var typeForget = jsonFormModel.FirstOrDefault(x => x.ID == "9");
                           if (startDateForget != null && selectedTimeForget != null && descriptionForget != null &&
                               typeForget != null)
                           {
                               var requstMng = new RegisterRequests();
                               var resultValidate = requstMng.ValidParamForgetIo(PersonCode.ToString(),
                                   Convert.ToDateTime(startDateForget.Value), selectedTimeForget.Value, descriptionForget.Value);
                               if (resultValidate.Validate)
                               {
                                   var resultRequest = requstMng.RegisterForgetIoRequest(Convert.ToDouble(PersonCode),
                                       (short) int.Parse(selectedCodeId),
                                       Convert.ToDateTime(startDateForget.Value), selectedTimeForget.Value,
                                       descriptionForget.Value, typeForget.Value, OnlineUserID.ToString());

                                    lstResult.Add(resultRequest);
                                    break;
                                }
                                lstResult.Add(resultValidate);
                           }
                           break;

                       case "13":
                           //list = requestfrmManager.GetIoTypeCorrectionForm(PersonCode.ToString());
                           var startDateIoCorrecionType = jsonFormModel.FirstOrDefault(x => x.ID == "1");
                           var selectedTimeIoCorrecionType = jsonFormModel.FirstOrDefault(x => x.ID == "3");
                           var descriptionIoCorrecionType = jsonFormModel.FirstOrDefault(x => x.ID == "8");
                           var requestTitleIoCorrecionType = jsonFormModel.FirstOrDefault(x => x.ID == "9");
                           if (startDateIoCorrecionType != null && selectedTimeIoCorrecionType != null &&
                               descriptionIoCorrecionType != null && requestTitleIoCorrecionType != null)
                           {
                               var requstMng = new RegisterRequests();
                               var resultRequest =
                                   requstMng.RegisterCorrectionTypeIoRequest(Convert.ToDouble(PersonCode),
                                       (short) int.Parse(selectedCodeId),
                                       Convert.ToDateTime(startDateIoCorrecionType.Value),
                                       selectedTimeIoCorrecionType.Value, descriptionIoCorrecionType.Value,
                                       requestTitleIoCorrecionType.Value, OnlineUserID.ToString());

                                lstResult.Add(resultRequest);

                           }
                           break;
                       case "1":
                           var sDate = jsonFormModel.FirstOrDefault(x => x.ID == "5");
                           var eDate = jsonFormModel.FirstOrDefault(x => x.ID == "6");
                           var descriptions = jsonFormModel.FirstOrDefault(x => x.ID == "7");
                           if (sDate != null && eDate != null && descriptions != null)
                           {
                               var requstMng = new RegisterRequests();
                               var resultValidate = requstMng.ValidParam(Convert.ToDateTime(sDate.Value),
                                   Convert.ToDateTime(eDate.Value), selectedCodeId.ToString(), descriptions.Value,
                                   PersonCode.ToString());
                               if (resultValidate.Validate)
                               {
                                   var resultRequest = requstMng.RegisterDailyRequest(Convert.ToDouble(PersonCode),
                                       (short) int.Parse(selectedCodeId),
                                       Convert.ToDateTime(sDate.Value), Convert.ToDateTime(eDate.Value), null, null,
                                       ((Convert.ToDateTime(eDate.Value) - Convert.ToDateTime(sDate.Value)).TotalDays +
                                        1)
                                           .ToString(),
                                       Convert.ToDouble(OnlineUserID), 5, descriptions.Value, ActionEnum.DailyLeave);
                                   if (resultRequest.Validate && resultValidate.ValidateMessage!="")
                                   {
                                       resultRequest.ValidateMessage = resultRequest.ValidateMessage + " " + resultValidate.ValidateMessage;
                                   }
                                    lstResult.Add(resultRequest);
                                    break;
                                }
                              
                                lstResult.Add(resultValidate);
                           }
                           break;
                       case "2":
                           var sDateMissDl = jsonFormModel.FirstOrDefault(x => x.ID == "5");
                           var eDateMissDl = jsonFormModel.FirstOrDefault(x => x.ID == "6");
                           var descriptionsMissDl = jsonFormModel.FirstOrDefault(x => x.ID == "12");

                           var fromPlaceMissDl = jsonFormModel.FirstOrDefault(x => x.ID == "7");
                           var desctenitionMissDl = jsonFormModel.FirstOrDefault(x => x.ID == "8");
                           var toVehicelMissDl = jsonFormModel.FirstOrDefault(x => x.ID == "9");
                           var returnVehicelMissDl = jsonFormModel.FirstOrDefault(x => x.ID == "10");
                           var stayStatusMissDl = jsonFormModel.FirstOrDefault(x => x.ID == "13");
                           var transferPriceMissDl = jsonFormModel.FirstOrDefault(x => x.ID == "11");
                           if (sDateMissDl != null && eDateMissDl != null && descriptionsMissDl != null
                               && fromPlaceMissDl != null && desctenitionMissDl != null && toVehicelMissDl != null
                               && returnVehicelMissDl != null && stayStatusMissDl != null && transferPriceMissDl != null)
                           {
                               var requstMng = new RegisterRequests();
                               var resultValidate =
                                   requstMng.ValidParamDailyMisson(Convert.ToDateTime(sDateMissDl.Value),
                                       Convert.ToDateTime(eDateMissDl.Value), PersonCode.ToString(),
                                       descriptionsMissDl.Value);
                               if (resultValidate.Validate)
                               {
                                   var companyLocation = stayStatusMissDl.Value == "1";
                                   var resultRequest = requstMng.RegisterMissonDailyRequest(
                                       Convert.ToDouble(PersonCode),
                                       (short) int.Parse(selectedCodeId),
                                       Convert.ToDateTime(sDateMissDl.Value), Convert.ToDateTime(eDateMissDl.Value),
                                       companyLocation, transferPriceMissDl.Value, toVehicelMissDl.Value,
                                       returnVehicelMissDl.Value
                                       , Convert.ToDouble(OnlineUserID.ToString()), 3,
                                       descriptionsMissDl.Value
                                       , ActionEnum.DailyDuty, fromPlaceMissDl.Value, desctenitionMissDl.Value);
                                   if (resultRequest.Validate && resultValidate.ValidateMessage != "")
                                   {
                                       resultRequest.ValidateMessage = resultRequest.ValidateMessage + " " + resultValidate.ValidateMessage;
                                   }
                                    lstResult.Add(resultRequest);
                                    break;
                                }
                                lstResult.Add(resultValidate);
                           }
                           break;
                       case "3":
                           var sDateHr = jsonFormModel.FirstOrDefault(x => x.ID == "1");
                           var sTime = jsonFormModel.FirstOrDefault(x => x.ID == "2");
                           var duration = jsonFormModel.FirstOrDefault(x => x.ID == "3");
                           var isFirstShuft = jsonFormModel.FirstOrDefault(x => x.ID == "89");
                           var descriptionsHr = jsonFormModel.FirstOrDefault(x => x.ID == "8");
                           if (sDateHr != null && sTime != null && duration != null && descriptionsHr != null && isFirstShuft!=null)
                           {
                               var requstMng = new RegisterRequests();
                               if (isFirstShuft.Value == "2")
                               {
                                   var resulttime = requstMng.SetStartTimeShift(double.Parse(PersonCode.ToString()),
                                       Convert.ToDateTime(sDateHr.Value));
                                   sTime.Value = resulttime;
                               }
                               var resultValidate = requstMng.ValidParamTimeLeave(duration.Value, selectedCodeId,
                                   descriptionsHr.Value, PersonCode.ToString());
                               if (resultValidate.Validate)
                               {
                                   var resultRequest = requstMng.RegisterTimeLeaveRequest(sTime.Value, duration.Value,
                                       Convert.ToDateTime(sDateHr.Value), PersonCode.ToString(), descriptionsHr.Value,
                                       selectedCodeId, OnlineUserID.ToString());

                                    lstResult.Add(resultRequest);
                                    break;
                                }
                                lstResult.Add(resultValidate);
                           }
                           break;
                       case "9":
                           
                               var sDatemissHr = jsonFormModel.FirstOrDefault(x => x.ID == "1");
                               var sTimemisson = jsonFormModel.FirstOrDefault(x => x.ID == "2");
                               var durationmisson = jsonFormModel.FirstOrDefault(x => x.ID == "3");
                               var priceMisson = jsonFormModel.FirstOrDefault(x => x.ID == "11");
                               var toVehicelMissonHr = jsonFormModel.FirstOrDefault(x => x.ID == "9");
                               var returnVehicleMissonHr = jsonFormModel.FirstOrDefault(x => x.ID == "10");
                               var descriptionHrMisson = jsonFormModel.FirstOrDefault(x => x.ID == "8");
                               if (sDatemissHr != null && sTimemisson != null && durationmisson != null &&
                                   priceMisson != null && toVehicelMissonHr != null && returnVehicleMissonHr != null &&
                                   descriptionHrMisson != null)
                               {
                                   var requstMng = new RegisterRequests();
                                   var resultValidate = requstMng.ValidHrMissonParam(double.Parse(PersonCode.ToString()),durationmisson.Value,
                                       descriptionHrMisson.Value);
                                   if (resultValidate.Validate)
                                   {
                                       var resultRequest = requstMng.RegisterHrMissonRequest(PersonCode.ToString(),
                                           selectedCodeId, DateTime.Parse(sDatemissHr.Value), sTimemisson.Value
                                           , durationmisson.Value, priceMisson.Value, toVehicelMissonHr.Value,
                                           returnVehicleMissonHr.Value, descriptionHrMisson.Value,
                                           OnlineUserID.ToString());

                                        lstResult.Add(resultRequest);
                                        break;
                                    }
                                    lstResult.Add(resultValidate);
                               }
                            // list = requestfrmManager.GetMissonHrlyForm();
                            break;


                        case "12":
                           {
                               var startDate = jsonFormModel.FirstOrDefault(x => x.ID == "1");
                               var startTime = jsonFormModel.FirstOrDefault(x => x.ID == "2");
                               var endTime = jsonFormModel.FirstOrDefault(x => x.ID == "3");
                               var startMisson = jsonFormModel.FirstOrDefault(x => x.ID == "9");
                               var descriptionHrMissons = jsonFormModel.FirstOrDefault(x => x.ID == "8");
                               if (startDate != null && startTime != null && endTime != null && startMisson != null &&
                                   descriptionHrMissons != null)
                               {
                                   var requstMng = new RegisterRequests();
                                   var resultValidate = requstMng.ValidHrMissonParam(double.Parse(PersonCode.ToString()),"01:00", descriptionHrMissons.Value);
                                   if (resultValidate.Validate)
                                   {
                                       var resultRequest =
                                           requstMng.RegisterHrMissonRequestWithoutResturn(PersonCode.ToString(),
                                               selectedCodeId, DateTime.Parse(startDate.Value)
                                               , startTime.Value
                                               , endTime.Value, startMisson.Value, descriptionHrMissons.Value,
                                               OnlineUserID.ToString());

                                        lstResult.Add(resultRequest);
                                        break;
                                    }
                                    lstResult.Add(resultValidate);
                               }
                           }
                           // list = requestfrmManager.GetMissonHrlyWhouthReturnForm();
                           break;
                   }
               }
               if (lstResult.Count > 1)
               {
                   var countSucces = lstResult.Count(x => x.Validate);
                    var countFails = lstResult.Count(x => !x.Validate);
                   if (countFails > 0)
                   {
                       var resultGeneral = new ResultViewModel
                       {
                           Validate = false,
                           ValidateMessage =
                               "تعداد " + countSucces + "با موفقیت و تعداد " + countFails + "نا موفق ثبت شد"
                       };
                       return JsonConvert.SerializeObject(resultGeneral);
                   }
                   else
                   {
                        var resultGeneral = new ResultViewModel
                        {
                            Validate = true,
                            ValidateMessage =
                               "با موفقیت ثبت شد"
                        };
                        return JsonConvert.SerializeObject(resultGeneral);
                    }
               }
               return JsonConvert.SerializeObject(lstResult.FirstOrDefault());
             

            }
           catch (Exception ex)
           {
               var result=new ResultViewModel
               {
                   Validate = false,
                   ValidateMessage = ex.Message,
                   ExceptionMessage = ex.Message
               };
               return JsonConvert.SerializeObject(result);

           }
       }

       public string ModifyGroupCreditByFormBuilder_Mobile(string FormJson, string OnlineUserID, string SessionID, string CreditID,
           string xmlRoot)
       {
           throw new NotImplementedException();
       }

       public string LoginHamster_Mobile(string HashId)
       {
           throw new NotImplementedException();
       }

       public bool FindWorkPreiodDays(string workPeriodId,string empNumber,ref DateTime sdate,ref DateTime edate)
       {
           try
           {
               var year =int.Parse(workPeriodId.Substring(0, 4));
               var month =int.Parse(workPeriodId.Substring(4, (workPeriodId.Length - 4)));
                var constre = ConfigurationManager.ConnectionStrings["connectionStringProviderSystem"].ToString();
                var connection = new SqlConnection(constre);
               var toDays = 30;
               if (month < 7)
               {
                   toDays = 31;
               }
               if (month == 12)
               {
                   toDays = 29;
               }
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }

                string query = @"select DAYS from EMP_SHIF where EMP_NO=@EmpNumber and YEAR_=@Year and MONTH_=@Month";

                connection.Open();
                var command = new SqlCommand(query, connection);
                command.Parameters.Add("@Year", SqlDbType.VarChar, 2000).Value = year;
                command.Parameters.Add("@Month", SqlDbType.VarChar, 2000).Value = month;
                command.Parameters.Add("@EmpNumber", SqlDbType.VarChar, 2000).Value = empNumber;
                var result = command.ExecuteReader();
                while (result.Read())
                {
                    toDays = int.Parse(result["DAYS"].ToString());
                }
                connection.Close();
                connection.Dispose();
                sdate = PersianDate.ConvertDate.ToEn(year, month, 1);
                edate = PersianDate.ConvertDate.ToEn(year, month, toDays);
               return true;

           }
           catch (Exception ex)
           {

               return false;
           }
       }

       public string ConvertValueToStringHoures(string value)
       {
           try
           {
               if (value.Contains(":"))
               {
                   return value;
               }
                var resultTime = "00:00";
               if (value.Length == 3)
               {
                   var houre = value.Substring(0, 1);
                   var minets = value.Substring(1, 2);
                   resultTime = houre + ":" + minets;
               }else if (value.Length < 3)
               {
                    resultTime = "00" + ":" + value;
                }
               else
               {
                    var houre = value.Substring(0, 2);
                    var minets = value.Substring(2, 2);
                   if (int.Parse(houre) > 24)
                   {
                       houre = (int.Parse(houre) - 24).ToString();
                        resultTime = houre + ":" + minets +" روز بعد";
                    }
                   else
                   {
                       resultTime = houre + ":" + minets;
                   }
               }
               return resultTime;
           }
           catch (Exception ex)
           {

               return "00:00";
           }
       }

        public string PersonAutoCompleteCombo(string onlineUserId, string filterStr)
        {
            try
            {
                var mngRequestFrmField=new RequestFormLoader();
                var result = mngRequestFrmField.GetPersonAutoComplete(onlineUserId,filterStr);
                return JsonConvert.SerializeObject(result);
            }
            catch (Exception ex)
            {

                var result = new ResultViewModel
                {
                    Validate = false,
                    ValidateMessage = ex.Message,
                    ExceptionMessage = ex.Message
                };
                return JsonConvert.SerializeObject(result);
            }
        }
        public string DeleteCreditsBatch_Mobile(string onlineUserId, string data)
        {
            try
            {
                var portfolioMng=new PortfolioManager(new WorkFlowsDatabase());
                  var modelData = JsonConvert.DeserializeObject<List<DeleteRequestViewModel>>(data);
                foreach (var deleteRequestViewModel in modelData)
                {
                    if (deleteRequestViewModel.StatusID == "201")
                    {
                         portfolioMng.DeleteRequests(onlineUserId, deleteRequestViewModel.DocID);
                    }
                    else if (deleteRequestViewModel.StatusID == "203")
                    {
                         portfolioMng.DeleteAcceptedRequests(onlineUserId, deleteRequestViewModel.DocID);
                    }
                }
               
                var result = new ResultViewModel
                {
                    Validate = true,
                    ValidateMessage = "حذف با موفقیت به اتمام رسید"
                };
                return JsonConvert.SerializeObject(result);
            }
            catch (Exception ex)
            {

                var result = new ResultViewModel
                {
                    Validate = false,
                    ExceptionMessage = ex.Message
                };
                return JsonConvert.SerializeObject(result);
            }
        }
    }
    /// <summary>
    /// ویو مدل جهت حذف در نمایش مجوز ها
    /// </summary>
    public class DeleteRequestViewModel
    {
        /// <summary>
        /// شناسه درخواست
        /// </summary>
        public string DocID { get; set; }
        /// <summary>
        /// شناسه وضعیت
        /// </summary>
        public string StatusID { get; set; }

    }
    public class RequestModel
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public bool IsDaily { get; set; }
    }
    public class ShiftsViewModel
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
    }
    public class DailyInfoViewModel
    {
        public string Empno { get; set; }
        public string shDate { get; set; }
        public string CARD { get; set; }
        public string CardValue { get; set; }
        public string CODE { get; set; }
        public string CodeName { get; set; }
        public string Value { get; set; }
        public string PrintText { get; set; }
        public string Shiftno { get; set; }
        public string PersianDate { get; set; }
        public DateTime Date { get; set; }
        public string Translate { get; set; }
        public string Color { get; set; }
    }

    public class AttendanceModel
    {
        public string Time { get; set; }
        public string Type { get; set; }
        public string TypeName { get; set; }
        public string Color { get; set; }
    }

    public class SectionViewModel
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string EmpManager { get; set; }
        public string TmFather { get; set; }
        public string Tfather { get; set; }
    }
}
