﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;
using System.Xml;
using Interface.ViewModel;
using Interface.ViewModel.General;
using MiddleWare.KaraLib.ViewModels;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PersianDate;
using PW.WorkFlows.SqlDal;
using System.Web.Script.Serialization;

namespace MiddleWare.KaraLib
{
    public class PortfolioManager : BaseDAL
    {
        /// <summary>
        /// بدست اوردن مدیر فرد
        /// </summary>
        /// <param name="empCode">شناسه پرسنلی</param>
        /// <returns>مدیر بودن یا نبودن فرد</returns>
        public bool IsUserManager(double empCode)
        {
            try
            {
                var isManager = false;
                var empData=WorkFlowsDatabase.Instance.Employee.GetDataByEmpNoForLogin(empCode);
                if (empData.IsManager||empData.IsEmpMngr || empData.IsParallelApprove)
                {
                    isManager = true;
                }
                return isManager;

            }
            catch (Exception ex)
            {

                return true;
            }
        }
        public string SetCountInfoForgetIO(double empCode)
        {
            var genetalText = "";
            var mngFormFields = new RegisterRequests();
            ParmfileRow paramFileRows = mngFormFields.GetDataRow();
            string text;
            string text2;
            string toolTip;
            string toolTip2;
            WorkFlowsDatabase.Instance.Cards.GetCount(100, empCode, (int)paramFileRows.StartDay, out text, out text2, out toolTip, out toolTip2);
            genetalText += text;

            genetalText +="--"+ text2;
            return genetalText;

        }
        public string SetCountInfo(double empCode)
        {
            var genetalText = "";
            var mngFormFields = new RegisterRequests();
            ParmfileRow paramFileRows = mngFormFields.GetDataRow();
            string text;
            string text2;
            string toolTip;
            string toolTip2;
            WorkFlowsDatabase.Instance.Cards.GetCount(103, empCode, (int)paramFileRows.StartDay, out text, out text2, out toolTip, out toolTip2);
            genetalText +="--"+ text;
           // genetalText += "--" + toolTip;
            genetalText += "--" + text2;
           // genetalText += "--" + toolTip2;
            List<DateTime> currentStartAndEndPeriod = ShDate.GetCurrentStartAndEndPeriod((int)paramFileRows.StartDay);
            List<int> summaryOverTime = WorkFlowsDatabase.Instance.Requests.GetSummaryOverTime(empCode, currentStartAndEndPeriod[0], currentStartAndEndPeriod[1]);
            genetalText += "--" + "جمع تایید دوره جاری :" + UtilityManagers.ConvertToTimeFormat(summaryOverTime[0].ToString());
           // genetalText += "--" + string.Format("از تاریخ {0} تا تاریخ {1}", ShDate.DateToStr(currentStartAndEndPeriod[0]), ShDate.DateToStr(currentStartAndEndPeriod[1]));
            TimeSpan[] startTimeAndEndTimeByEmp = WorkFlowsDatabase.Instance.Users.GetStartTimeAndEndTimeByEmp(empCode);
            genetalText += "--" + ((startTimeAndEndTimeByEmp == null) ? "شیفت نامعلوم" : ("شیفت از " + startTimeAndEndTimeByEmp[0].ToString().Substring(0, 5) + " تا " + startTimeAndEndTimeByEmp[1].ToString().Substring(0, 5)));
            return genetalText;
        }
        public string GetRemainCardexForForm(string empCode)
        {
            try
            {
                var genetalText = "";
                var mngFormFields = new RegisterRequests();
                ParmfileRow paramFileRows = mngFormFields.GetDataRow();
                if (!paramFileRows.ShowRemainKardex)
                {
                   return "نمایش مانده مرخصی غیر فعال می باشد";

                }
                DataRow dataRow = WorkFlowsDatabase.Instance.MorMam.SumTimeRequestedInYear(double.Parse(empCode));
                genetalText = ((dataRow != null)
                    ? string.Format("مرخصی ساعتی مصرفی از اول سال {0} به تعداد {1}",
                        UtilityManagers.ConvertToTimeFormat(dataRow["sumTime"].ToString()), dataRow["cnt"])
                    : string.Empty);
                DataRow dataRow2 = WorkFlowsDatabase.Instance.MorMam.SumOtherMorMamInYear(double.Parse(empCode));
                genetalText +="--"+ ((dataRow2 != null)
                    ? string.Format("سایر مرخصی های مصرفی از اول سال {0} به تعداد {1}",
                        UtilityManagers.ConvertToTimeFormat(dataRow2["sumOther"].ToString()), dataRow2["cnt"])
                    : string.Empty);

                genetalText += "--" + WorkFlowsDatabase.Instance.MorMam.GetRemainKardexFormat(double.Parse(empCode),
                    paramFileRows);
                return genetalText;
            }
            catch (Exception ex){
                return "";
            }
      }
      public ResultViewModel GetPortfolio(string CartablOwnerCode, string CartablOwnerID, string StartDate, string EndDate,
           string Requester, string DocType, string DepartmentID, string GroupID, string StrFilter, string PageSize,
           string PageNumber, int Sessionid, string Language)
      {
          try
          {
              if (StartDate.Trim() == "2/1/2010" && EndDate.Trim() == "2/1/2050")
              {
                  StartDate = "\n      ";
                  EndDate = "\n      ";
              }
              var sDate = StartDate!= "\n      "?DateTime.Parse(StartDate): DateTime.Now.AddMonths(-1).AddDays(-1);
                var eDate= EndDate != "\n      " ? DateTime.Parse(EndDate): DateTime.MaxValue;
              
                var result = GetByManagerEmpNo(double.Parse(CartablOwnerCode), sDate,
                  eDate, null);
                if (StrFilter != "\n      ")
                {
                    var splitedCodes = StrFilter.Split('-');
                    var actionId = splitedCodes[0];
                    var selectedCodeId = splitedCodes[1];
                    result = result.Where(x => x.DocTypeId == selectedCodeId).ToList();
                }
                else
                {
                    if (DocType != "\n      ")
                    {
                        var reqstmng = new RequestFormLoader();
                        var resultCodes = reqstmng.GetFormCodesForModel(CartablOwnerCode, DocType);
                        var lstCodes=new List<string>();
                        foreach (var getCodePermissionMobileViewModel in resultCodes)
                        {
                            var splitedCodes = getCodePermissionMobileViewModel.CodeID.Split('-');
                           // var actionId = splitedCodes[0];
                            var selectedCodeId = splitedCodes[1];
                            lstCodes.Add(selectedCodeId);
                        }
                        if (lstCodes.Count > 0)
                        {
                            var resultGeneral = (from resultportFo in result
                                join lstMyCodes in lstCodes on resultportFo.DocTypeId equals lstMyCodes
                                select resultportFo).ToList();
                            result = resultGeneral;
                        }
                    }
                }
                if (Requester != "\n      "&& Requester!="0")
                {
                    result = result.Where(x => x.RequesterCode == Requester).ToList();
                }
              if (result.Count == 0)
              {
                    return new ResultViewModel
                    {
                        Validate = true,
                        Message = "",
                        ValidateMessage = "بدون داده"
                    };
                }
                return new ResultViewModel
                {
                    Validate = true,
                    Message = JsonConvert.SerializeObject(result)

                };
            }
          catch (Exception ex)
          {
              return new ResultViewModel
              {
                  Validate = false,
                  ValidateMessage = ex.Message
                
              };
              
          }
           
      }
/// <summary>
/// متدی جهت حذف و یا تایید و یا رد مجوز ها در کارتابل
/// </summary>
/// <param name="xmlData">داده بدست امده از موبایل</param>
/// <param name="onlineUser">کاربر انلاین</param>
/// <returns>تایید انجام کار</returns>
      public ResultViewModel AcceptOrRejectOrSendUpRequest(string xmlData, string onlineUser)
      {
          try
          {
              var moveUp = false;
                //RemoveCDataInXML
               var xmlRequest = RemoveCData(xmlData);

                //RemoveCDataInXML

                XmlDocument doc = new XmlDocument();

                doc.LoadXml(xmlRequest);
                string jsonText = JsonConvert.SerializeXmlNode(doc);
                JObject rootJson = JObject.Parse(jsonText);
                var jsonRoot = rootJson["Root"].ToString();

                var resultservice = new RootObject();
                try
                {
                    resultservice = JsonConvert.DeserializeObject<RootObject>(jsonRoot);
                }
                catch
                {
                    var resultSinglerow = JsonConvert.DeserializeObject<RootObjectSingleRow>(jsonRoot);
                    resultservice.ActionTb = resultSinglerow.ActionTb;
                    resultservice.Tb = new List<Tb>();
                    resultservice.Tb.Add(resultSinglerow.Tb);

                }
                var lstselected = resultservice.Tb.ToList();
                var list = "";
                int num = 0;
                var mngFormFields = new RegisterRequests();
                ParmfileRow paramFileRows = mngFormFields.GetDataRow();
                foreach (var tb in lstselected)
              {
                  RequestRow dataById = GetDataById(int.Parse(tb.DocID));
                  string empty = string.Empty;
                  if (resultservice.ActionTb.Action == "1")
                  {
                      if (
                          !SaveToPwKara(dataById, double.Parse(onlineUser), onlineUser, ref empty,
                              paramFileRows.RegisterTolerance))
                      {
                          num++;
                      }
                      if (!string.IsNullOrEmpty(empty))
                      {
                          list+=(empty+"   ");
                      }
                  }
                    else if (resultservice.ActionTb.Action == "2")
                    {
                        if (dataById.AcceptCode == 21)
                        {
                            if (!WorkFlowsDatabase.Instance.Requests.AcceptRequest(true, double.Parse(onlineUser), dataById.RequestsId, tb.ActorDesc, 23))
                            {
                                num++;
                            }
                        }
                        else if (!WorkFlowsDatabase.Instance.Requests.AcceptRequest(false, double.Parse(onlineUser), dataById.RequestsId, tb.ActorDesc, 0))
                        {
                            num++;
                        }
                    }
                    else if (resultservice.ActionTb.Action == "3")
                    {
                        moveUp = true;
                        if (dataById.RequestsId >= 1)
                        {
                            if (dataById.AcceptCode == 21)
                            {
                               
                                num++;
                            }
                            else
                            {
                                string text = dataById.CurSection.ToString();
                                var moveToEmpNo = WorkFlowsDatabase.Instance.Sections.GetManagerOfSection(dataById.CurEmpNo, ref text);
                                if (dataById.CurSection.ToString() == text && moveToEmpNo != dataById.CurEmpNo)
                                {
                                    moveToEmpNo = WorkFlowsDatabase.Instance.Sections.GetManagerOfSection(moveToEmpNo, ref text);
                                }
                                if (moveToEmpNo == dataById.CurEmpNo)
                                {
                                    num++;
                                   
                                }
                                else
                                {
                                    bool isDay = dataById.IsDay;
                                    DataRow conferment = WorkFlowsDatabase.Instance.Conferment.GetConferment(moveToEmpNo, GetActionRequest(dataById, isDay), Convert.ToInt32(text));
                                    if (conferment != null)
                                    {
                                        moveToEmpNo = Convert.ToDouble(conferment["EMP_NO"]);
                                    }
                                    if (moveToEmpNo > 0.0 && WorkFlowsDatabase.Instance.Requests.MoveUp(moveToEmpNo, text, dataById.RequestsId))
                                    {
                                        WorkFlowsDatabase.Instance.MoveUp.Insert(dataById.RequestsId, dataById.CurEmpNo, moveToEmpNo, (int)dataById.Type, (double)dataById.CurSection, Convert.ToDouble(text));
                                    }
                                }
                            }
                        }
                    }
                }
              if (num > 0)
              {
                  if (moveUp)
                  {
                      list += "عدم امکان ارجاع" + num + "درخواست";
                  }
                  return new ResultViewModel
                  {
                      Validate = false,
                      ValidateMessage =  list

                  };
              }
              return new ResultViewModel
                {
                    Validate = true,
                    ValidateMessage = "عملیات با موفقیت به اتمام رسید"

                };
            }
          catch (Exception ex)
          {

                return new ResultViewModel
                {
                    Validate = false,
                    ValidateMessage = ex.Message

                };
            }
      }
        // Token: 0x06000010 RID: 16 RVA: 0x00002354 File Offset: 0x00000554
        public int GetActionRequest(RequestRow ReqInc, bool IS_DAY)
        {
            if (ReqInc.Type < 100)
            {
                if (ReqInc.OperationsId == 5)
                {
                    if (!IS_DAY)
                    {
                        return 1;
                    }
                    return 3;
                }
                else if (ReqInc.OperationsId == 3)
                {
                    if (!IS_DAY)
                    {
                        return 2;
                    }
                    return 9;
                }
                else if (ReqInc.OperationsId == 4)
                {
                    return 12;
                }
            }
            else
            {
                switch (ReqInc.Type)
                {
                    case 100:
                        return 8;
                    case 101:
                        return 4;
                    case 102:
                        return 6;
                    case 103:
                        return 5;
                    case 104:
                        return 7;
                }
            }
            return 0;
        }
        /// <summary>
        /// حذف المان خطا ساز در ایکس ام ال
        /// </summary>
        /// <param name="xmlData">متن ایکس ام ال</param>
        /// <returns>ایکس ام ال قابل پارس</returns>
        public string RemoveCData(string xmlData)
        {
            try
            {
                var result = xmlData;
                if (xmlData.Contains(XmlStaticTexts.BeginningCdata))
                {
                    var indexCdata = xmlData.IndexOf(XmlStaticTexts.BeginningCdata, StringComparison.Ordinal);
                    result = xmlData.Substring(indexCdata + 9, xmlData.Length - 12);

                }
                return result;
            }
            catch (Exception ex)
            {
                
                return xmlData;
            }
        }
        public ResultViewModel AcceptAllPortfolio(string CartablOwnerCode, string CartablOwnerID, string StartDate, string EndDate,
         string Requester, string DocType, string DepartmentID, string GroupID, string StrFilter, string PageSize,
         string PageNumber, int Sessionid, string Language)
        {
            try
            {

                var sDate = StartDate != "\n      " ? DateTime.Parse(StartDate) : new DateTime(2010, 01, 01);
                var eDate = EndDate != "\n      " ? DateTime.Parse(EndDate) : DateTime.MaxValue;

                var result = GetByManagerEmpNo(double.Parse(CartablOwnerCode), sDate,
                  eDate, null);
                if (StrFilter != "\n      ")
                {
                    var splitedCodes = StrFilter.Split('-');
                    var actionId = splitedCodes[0];
                    var selectedCodeId = splitedCodes[1];
                    result = result.Where(x => x.DocTypeId == selectedCodeId).ToList();
                }
                if (Requester != "\n      "&& Requester!="0")
                {
                    result = result.Where(x => x.RequesterCode == Requester).ToList();
                }
                IList list = new ArrayList();
                int num = 0;
                var mngFormFields=new RegisterRequests();
                ParmfileRow paramFileRows = mngFormFields.GetDataRow();
                foreach (var mobilePortfolioViewModel in result)
                {
                    RequestRow dataById = GetDataById(int.Parse(mobilePortfolioViewModel.Docid));
                    string empty = string.Empty;
                    if (!SaveToPwKara(dataById, double.Parse(CartablOwnerCode), CartablOwnerCode, ref empty, paramFileRows.RegisterTolerance))
                    {
                        num++;
                    }
                    if (!string.IsNullOrEmpty(empty))
                    {
                        list.Add(empty);
                    }
                }
                if (num > 0)
                {
                    return new ResultViewModel
                    {
                        Validate = false,
                        ValidateMessage = "تعداد" + num + "با خطا روبرو شد"

                    };
                }
                return new ResultViewModel
                {
                    Validate = true,
                    ValidateMessage = "عملیات با موفقیت به اتمام رسید"

                };
            }
            catch (Exception ex)
            {
                return new ResultViewModel
                {
                    Validate = false,
                    ValidateMessage = ex.Message

                };

            }
        }
        // Token: 0x060001C9 RID: 457 RVA: 0x000126B0 File Offset: 0x000108B0
        public bool SaveToPwKara(RequestRow reqInc, double currentEmpNo, string userPassword, ref string msg, short registerTolerance)
        {
            string empty = string.Empty;
            if (!this.CheckAccess(reqInc, currentEmpNo, ref empty))
            {
                msg = string.Concat(new object[]
                {
                    reqInc.RequestsId,
                    "#",
                    string.Format("شما مجاز به تایید مجوز " + reqInc.Title + " نمی باشید.", new object[0]),
                    empty
                });
                return false;
            }
            string text = "";
            DataRow dataRow = null;
            AcceptCodeEnum acceptCodeEnum = AcceptCodeEnum.Accept;
            bool flag = Convert.ToBoolean(reqInc.IsDay);
            if (reqInc.AcceptCode == 21)
            {
                return RevokeRequest(reqInc, flag, ref dataRow, currentEmpNo) < 1;
            }
            if (WorkFlowsDatabase.Instance.ParallelApprove.HaveNotAccept(reqInc.RequestsId, (int)reqInc.Type))
            {
                acceptCodeEnum = AcceptCodeEnum.ParallelApproval;
                msg = reqInc.RequestsId + "#نیاز به تایید کارشناس دارد";
            }
            else
            {
                switch (reqInc.Type)
                {
                    case 100:
                        if (!WorkFlowsDatabase.Instance.DataFile.Insert((reqInc.Duration == "") ? (short)0 : short.Parse(reqInc.Duration), reqInc.StartDate.ToOADate(), (short)reqInc.StartHour, reqInc.EmpNo, true, 0))
                        {
                            msg = reqInc.RequestsId + "#خطا در ثبت تردد وجود دارد";
                            return false;
                        }
                        break;
                    case 101:
                        {
                            DataTable dataTable = WorkFlowsDatabase.Instance.DataFile.GetDataByDateAndTime(reqInc.EmpNo, reqInc.StartDate.ToOADate(), (short)reqInc.StartHour);
                            if (dataTable == null || dataTable.Rows.Count <= 0)
                            {
                                msg = string.Concat(new object[]
                                {
                            reqInc.RequestsId,
                            "#در تاریخ ",
                            ShDate.CompleteDateName(reqInc.StartDate),
                            " ترددی برای ",
                            reqInc.FullName,
                            " وجود ندارد"
                                });
                                return false;
                            }
                            if (reqInc.EndHour < 1)
                            {
                                WorkFlowsDatabase.Instance.DataFile.Delete(reqInc.StartDate.ToOADate(), (short)reqInc.StartHour, reqInc.EmpNo);
                            }
                            else
                            {
                                WorkFlowsDatabase.Instance.DataFile.UpdateByModify(Convert.ToDateTime(dataTable.Rows[0]["DATE_"]).Date.ToOADate(), (short)reqInc.StartHour, (short)reqInc.EndHour, reqInc.EmpNo, (reqInc.Duration == "") ? (short)0 : short.Parse(reqInc.Duration), true);
                            }
                            break;
                        }
                    case 102:
                        if (WorkFlowsDatabase.Instance.EmpGrps.Insert(reqInc.EmpNo, reqInc.StartDate.Date.ToOADate(), reqInc.EndHour, new int?(reqInc.StartHour)) && reqInc.Duration != "0" && !WorkFlowsDatabase.Instance.EmpGrps.Insert(reqInc.EmpNo, reqInc.StartDate.AddDays(double.Parse(reqInc.Duration)).ToOADate(), reqInc.StartHour, new int?(reqInc.EndHour)))
                        {
                            msg = reqInc.RequestsId + "#در برنامه حضور غیاب اعمال نشد!!";
                            return false;
                        }
                        break;
                    case 103:
                        if (reqInc.EndDate.HasValue)
                        {
                            TimeSpan timeSpan = reqInc.EndDate.Value.Date.Subtract(reqInc.StartDate.Date);
                            for (int i = 0; i <= timeSpan.Days; i++)
                            {
                                DataTable dataTable = WorkFlowsDatabase.Instance.Overs.GetDataByEmpNo(reqInc.EmpNo, reqInc.StartDate.AddDays((double)i).ToOADate());
                                if (dataTable != null && dataTable.Rows.Count > 0)
                                {
                                    if (!WorkFlowsDatabase.Instance.Overs.Update(reqInc.EmpNo, Convert.ToDateTime(dataTable.Rows[0]["DATE_"]).ToOADate(), short.Parse(reqInc.Duration), DateTime.Now.Date.ToOADate(), currentEmpNo.ToString(), Convert.ToInt16(dataTable.Rows[0]["TIME_"])))
                                    {
                                        msg = string.Concat(new object[]
                                        {
                                        reqInc.RequestsId,
                                        "#",
                                        reqInc.Title,
                                        " برای ",
                                        reqInc.FullName,
                                        "در برنامه حضور غیاب اعمال نشد!!"
                                        });
                                        return false;
                                    }
                                }
                                else if (!WorkFlowsDatabase.Instance.Overs.Insert(reqInc.EmpNo, reqInc.StartDate.AddDays((double)i).ToOADate(), short.Parse(reqInc.Duration), DateTime.Now.Date.ToOADate(), currentEmpNo.ToString()))
                                {
                                    msg = string.Concat(new object[]
                                    {
                                    reqInc.RequestsId,
                                    "#",
                                    reqInc.Title,
                                    " برای ",
                                    reqInc.FullName,
                                    "در برنامه حضور غیاب اعمال نشد!!"
                                    });
                                    return false;
                                }
                            }
                        }
                        break;
                    case 104:
                        {
                            short value = 0;
                            if (reqInc.StartHour == 0)
                            {
                                value = 7;
                            }
                            else if (reqInc.StartHour == 1)
                            {
                                value = 4;
                            }
                            if (!WorkFlowsDatabase.Instance.MorMam.Insert(reqInc.EmpNo, null, double.Parse(reqInc.Duration), null, new short?((short)57), reqInc.Description, null, UtilityManagers.ConvertXorToString(currentEmpNo.ToString()), UtilityManagers.ConvertXorToString(userPassword), null, UtilityManagers.ConvertXorToString(DateTime.Now.ToShortTimeString()), null, new short?(value), DateTime.Now.Date.ToOADate(), reqInc.StartDate.Date.ToOADate(), new DateTime(1899, 12, 30).ToOADate(), DateTime.Now.Date.ToOADate()))
                            {
                                msg = string.Concat(new object[]
                                {
                            reqInc.RequestsId,
                            "#",
                            reqInc.Title,
                            " برای ",
                            reqInc.FullName,
                            "در برنامه حضور غیاب اعمال نشد!!"
                                });
                                return false;
                            }
                            break;
                        }
                    case 105:
                        {
                            DataTable dataTable = WorkFlowsDatabase.Instance.DataFile.GetDataByDateAndTime(reqInc.EmpNo, reqInc.StartDate.ToOADate(), (short)reqInc.StartHour);
                            if (dataTable == null || dataTable.Rows.Count <= 0)
                            {
                                msg = string.Concat(new object[]
                                {
                            reqInc.RequestsId,
                            "#در تاریخ ",
                            ShDate.CompleteDateName(reqInc.StartDate),
                            " ترددی برای ",
                            reqInc.FullName,
                            " وجود ندارد"
                                });
                                return false;
                            }
                            WorkFlowsDatabase.Instance.DataFile.UpdateByModify(Convert.ToDateTime(dataTable.Rows[0]["DATE_"]).Date.ToOADate(), (short)reqInc.StartHour, (short)reqInc.StartHour, reqInc.EmpNo, (reqInc.Duration == "") ?(short) 0 : short.Parse(reqInc.Duration), false);
                            break;
                        }
                    default:
                        if (flag)
                        {
                            if (reqInc.EndDate.HasValue && WorkFlowsDatabase.Instance.MorMam.IsExist(reqInc.StartDate.ToOADate(), reqInc.EndDate.Value.ToOADate(), reqInc.EmpNo) > 0)
                            {
                                msg = string.Concat(new object[]
                                {
                                reqInc.RequestsId,
                                "#شما مجاز به تایید نمی باشید.\n در تاریخ ",
                                ShDate.CompleteDateName(reqInc.StartDate),
                                " درخواست روزانه برای ",
                                reqInc.FullName,
                                " وجود دارد."
                                });
                                return false;
                            }
                            if (reqInc.Type == 60 && reqInc.EndDate.HasValue && !WorkFlowsDatabase.Instance.Requests.CheckEsteh(reqInc.EmpNo, reqInc.StartDate.Date, reqInc.EndDate.Value.Date, ref text, ref msg))
                            {
                                msg = reqInc.RequestsId + "#" + msg;
                                return false;
                            }
                            int num = 0;
                            LeaveType operationsId = (LeaveType)reqInc.OperationsId;
                            if (operationsId != LeaveType.Duty)
                            {
                                if (operationsId == LeaveType.Leave)
                                {
                                    if (!WorkFlowsDatabase.Instance.MorMam.Insert(reqInc, UtilityManagers.ConvertXorToString(currentEmpNo.ToString()), UtilityManagers.ConvertXorToString(userPassword), ShDate.DateToStr(reqInc.StartDate, false), ShDate.IsLeapYear(ShDate.CurrentYear()), ref num))
                                    {
                                        msg = reqInc.RequestsId + "#مشکل ثبت در حضور و غیاب";
                                        return false;
                                    }
                                    if (num == -2)
                                    {
                                        msg = reqInc.RequestsId + "#گروه چرخشي براي گروه جاری تعريف نشده";
                                        return false;
                                    }
                                    if (num == -3)
                                    {
                                        msg = string.Concat(new object[]
                                        {
                                        reqInc.RequestsId,
                                        "#گروه چرخشي براي ",
                                        reqInc.FullName,
                                        " تعريف نشده"
                                        });
                                        return false;
                                    }
                                }
                            }
                            else if (!WorkFlowsDatabase.Instance.MorMam.Insert(reqInc, UtilityManagers.ConvertXorToString(currentEmpNo.ToString()), UtilityManagers.ConvertXorToString(userPassword), ShDate.DateToStr(reqInc.StartDate, false), ShDate.IsLeapYear(ShDate.CurrentYear()), ref num))
                            {
                                msg = reqInc.RequestsId + "#مشکل ثبت در حضور و غیاب";
                                return false;
                            }
                        }
                        else
                        {
                            short type = reqInc.Type;
                            switch (reqInc.OperationsId)
                            {
                                case 3:
                                case 5:
                                    if (DateTime.Now.Subtract(reqInc.StartDate).TotalMinutes >= (double)registerTolerance || registerTolerance == 0)
                                    {
                                        if (!WorkFlowsDatabase.Instance.DataFile.UpdateTimeStatus(reqInc.StartDate.Date.ToOADate(), (short)reqInc.StartHour, (short)reqInc.EndHour, reqInc.EmpNo, ref type, ref dataRow, reqInc.Duration))
                                        {
                                            if (type == -1)
                                            {
                                                acceptCodeEnum = AcceptCodeEnum.NoDateFile;
                                                msg = string.Concat(new object[]
                                                {
                                            reqInc.RequestsId,
                                            "#در تاریخ ",
                                            ShDate.CompleteDateName(reqInc.StartDate),
                                            " ترددی در بازه درخواستی برای ",
                                            reqInc.FullName,
                                            " وجود ندارد"
                                                });
                                            }
                                            else
                                            {
                                                acceptCodeEnum = AcceptCodeEnum.Other;
                                                msg = reqInc.RequestsId + "#در برنامه حضور غیاب ثبت نشد!!";
                                            }
                                        }
                                    }
                                    else
                                    {
                                        acceptCodeEnum = AcceptCodeEnum.RegisterTolerance;
                                        msg = reqInc.RequestsId + "#در برنامه حضور غیاب ثبت نشد!!مدت انتظار تایید";
                                    }
                                    break;
                                case 4:
                                    if (reqInc.Status == StatusEnum.ExitFromOrg)
                                    {
                                        if (!WorkFlowsDatabase.Instance.DataFile.UpdateTimeStatus(reqInc.StartDate.Date.ToOADate(), (short)reqInc.StartHour, (short)reqInc.EndHour, reqInc.EmpNo, ref type, ref dataRow, reqInc.Duration))
                                        {
                                            if (type == -1)
                                            {
                                                acceptCodeEnum = AcceptCodeEnum.NoDateFile;
                                                msg = string.Concat(new object[]
                                                {
                                            reqInc.RequestsId,
                                            "#در تاریخ ",
                                            ShDate.CompleteDateName(reqInc.StartDate),
                                            " ترددی در بازه درخواستی برای ",
                                            reqInc.FullName,
                                            " وجود ندارد"
                                                });
                                            }
                                            else
                                            {
                                                acceptCodeEnum = AcceptCodeEnum.Other;
                                                msg = reqInc.RequestsId + "#در برنامه حضور غیاب ثبت نشد!!";
                                            }
                                        }
                                        else
                                        {
                                            WorkFlowsDatabase.Instance.DataFile.Insert(0, reqInc.StartDate.ToOADate(), (short)reqInc.EndHour, reqInc.EmpNo, true, 1);
                                            WorkFlowsDatabase.Instance.DataFile.Insert(0, reqInc.StartDate.ToOADate(), (short)(reqInc.EndHour + 1), reqInc.EmpNo, true, 1);
                                        }
                                    }
                                    else
                                    {
                                        WorkFlowsDatabase.Instance.DataFile.Insert(0, reqInc.StartDate.ToOADate(), (short)reqInc.StartHour, reqInc.EmpNo, true, 1);
                                        WorkFlowsDatabase.Instance.DataFile.Insert(reqInc.Type, reqInc.StartDate.ToOADate(), (short)(reqInc.StartHour + 1), reqInc.EmpNo, true, 1);
                                        WorkFlowsDatabase.Instance.DataFile.Insert(0, reqInc.StartDate.ToOADate(), (short)reqInc.EndHour, reqInc.EmpNo, true, 1);
                                        WorkFlowsDatabase.Instance.DataFile.Insert(0, reqInc.StartDate.ToOADate(), (short)(reqInc.EndHour + 1), reqInc.EmpNo, true, 1);
                                    }
                                    break;
                            }
                        }
                        break;
                }
            }
            string requestedTime = reqInc.Duration;
            if (acceptCodeEnum == AcceptCodeEnum.ParallelApproval)
            {
                requestedTime = string.Empty;
            }
            if (dataRow != null)
            {
                return WorkFlowsDatabase.Instance.Requests.AcceptRequest(true, currentEmpNo, reqInc.RequestsId, "", (short)acceptCodeEnum, requestedTime, new int?(Convert.ToInt32(dataRow["TIME_"])), new int?(Convert.ToInt32(dataRow["Tolerance"])));
            }
            return WorkFlowsDatabase.Instance.Requests.AcceptRequest(true, currentEmpNo, reqInc.RequestsId, "", (short)acceptCodeEnum, requestedTime, null, null);
        }
        // Token: 0x060001CE RID: 462 RVA: 0x00013C30 File Offset: 0x00011E30
        public int RevokeRequest(RequestRow reqInc, bool isDay, ref DataRow dtTaradod, double currentEmpNo)
        {
            int num = 0;
            if (reqInc.Type < 100)
            {
                if (isDay)
                {
                    if ((reqInc.OperationsId == 5 || reqInc.OperationsId == 3) && reqInc.EndDate.HasValue)
                    {
                        DataRow dataRow = MorMamDelete(new double?(reqInc.EmpNo), new short?(reqInc.Type), UtilityManagers.ConvertXorToString(currentEmpNo.ToString()), reqInc.StartDate.ToOADate(), reqInc.EndDate.Value.ToOADate());
                        if (dataRow != null)
                        {
                            LogMorMamInsert(dataRow, currentEmpNo);
                        }
                        else
                        {
                            num++;
                        }
                    }
                }
                else if (reqInc.OperationsId == 5 || reqInc.OperationsId == 3)
                {
                    WorkFlowsDatabase.Instance.DataFile.UpdateTimeStatus(reqInc.StartDate.Date.ToOADate(), (short)reqInc.StartHour, (short)reqInc.EndHour, reqInc.EmpNo, reqInc.Duration);
                }
                else if (reqInc.OperationsId == 4)
                {
                    if (reqInc.Status == StatusEnum.ExitFromOrg)
                    {
                        short num2 = 0;
                        if (!WorkFlowsDatabase.Instance.DataFile.UpdateTimeStatus(reqInc.StartDate.Date.ToOADate(), (short)reqInc.StartHour, (short)reqInc.EndHour, reqInc.EmpNo, ref num2, ref dtTaradod, reqInc.Duration))
                        {
                            num++;
                        }
                        else
                        {
                            WorkFlowsDatabase.Instance.DataFile.Delete(reqInc.StartDate.ToOADate(), (short)reqInc.EndHour, reqInc.EmpNo);
                            WorkFlowsDatabase.Instance.DataFile.Delete(reqInc.StartDate.ToOADate(), (short)(reqInc.EndHour + 1), reqInc.EmpNo);
                        }
                    }
                    else
                    {
                        WorkFlowsDatabase.Instance.DataFile.Delete(reqInc.StartDate.ToOADate(), (short)reqInc.StartHour, reqInc.EmpNo);
                        WorkFlowsDatabase.Instance.DataFile.Delete(reqInc.StartDate.ToOADate(), (short)(reqInc.StartHour + 1), reqInc.EmpNo);
                        WorkFlowsDatabase.Instance.DataFile.Delete(reqInc.StartDate.ToOADate(), (short)reqInc.EndHour, reqInc.EmpNo);
                        WorkFlowsDatabase.Instance.DataFile.Delete(reqInc.StartDate.ToOADate(), (short)(reqInc.EndHour + 1), reqInc.EmpNo);
                    }
                }
            }
            else if (reqInc.Type == 101)
            {
                DataTable dataByDateAndTime = WorkFlowsDatabase.Instance.DataFile.GetDataByDateAndTime(reqInc.EmpNo, reqInc.StartDate.ToOADate(), (short)reqInc.EndHour);
                if (dataByDateAndTime != null && dataByDateAndTime.Rows.Count > 0)
                {
                    if (reqInc.EndHour < 1)
                    {
                        if (!WorkFlowsDatabase.Instance.DataFile.Insert(0, reqInc.StartDate.ToOADate(), (short)reqInc.StartHour, reqInc.EmpNo, true, 0))
                        {
                            num++;
                        }
                    }
                    else
                    {
                        WorkFlowsDatabase.Instance.DataFile.UpdateByModify(Convert.ToDateTime(dataByDateAndTime.Rows[0]["DATE_"]).Date.ToOADate(), (short)reqInc.EndHour, (short)reqInc.StartHour, reqInc.EmpNo, 0, true);
                    }
                }
                else
                {
                    num++;
                }
            }
            else if (reqInc.Type == 100)
            {
                if (!WorkFlowsDatabase.Instance.DataFile.Delete(reqInc.StartDate.ToOADate(), (short)reqInc.StartHour, reqInc.EmpNo))
                {
                    num++;
                }
            }
            else if (reqInc.Type == 103 && !WorkFlowsDatabase.Instance.Overs.Delete(reqInc.RequestsId, reqInc.EmpNo, reqInc.StartDate))
            {
                num++;
            }
            if (num == 0 && !WorkFlowsDatabase.Instance.Requests.AcceptRequest(true, reqInc.CurEmpNo, reqInc.RequestsId, "", 22))
            {
                num++;
            }
            return num;
        }
        // Token: 0x06000155 RID: 341 RVA: 0x0000A9F8 File Offset: 0x00008BF8
        public long LogMorMamInsert(DataRow dr, double currentEmpNo)
        {
            string text = UtilityManagers.ConvertXorToString(dr["EMP_No"].ToString());
            string text2 = UtilityManagers.ConvertXorToString(currentEmpNo.ToString());
            double num = 0.0;
            if (!(dr["ISU_DATE"] is DBNull) && dr["ISU_DATE"] != null)
            {
                num = Math.Round(Convert.ToDouble(dr["ISU_DATE"]), 0);
            }
            double num2 = Math.Round(Convert.ToDouble(dr["S_DATE"]), 0);
            double num3 = Math.Round(Convert.ToDouble(dr["E_DATE"]), 0);
            string text3 = UtilityManagers.ConvertXorToString(dr["Requested"].ToString());
            string text4 = UtilityManagers.ConvertXorToString(dr["TYP"].ToString());
            string text5 = UtilityManagers.ConvertXorToString(0.ToString());
            string text6 = dr["Babat"].ToString();
            long num4 = Convert.ToInt64(dr["RefNumber"]);
           
            object obj = base.ExecuteScalar("WF_LogMor_MamInsert", new object[]
            {
                text,
                text2,
                num,
                num2,
                num3,
                num,
                text3,
                text4,
                text5,
                text6,
                num4
            });
            if (obj == null)
            {
                return 0L;
            }
            return Convert.ToInt64(obj);
        }
        // Token: 0x06000166 RID: 358 RVA: 0x0000BA20 File Offset: 0x00009C20
        public DataRow MorMamDelete(double? empNo, short? typ, string p7, double sDate, double eDate)
        {
            sDate = Math.Round(sDate, 0);
            eDate = Math.Round(eDate, 0);
            //DataRow expr_55 = base.ExecuteDataRow("WF_MOR_MAMDelete", new object[]
            //{
            //    empNo.Value,
            //    sDate,
            //    eDate,
            //    typ,
            //    p7
            //});
            var constre = ConfigurationManager.ConnectionStrings["connectionStringProviderSystem"].ToString();
            var connection = new SqlConnection(constre);

            if (connection.State == ConnectionState.Open)
            {
                connection.Close();
            }
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = new DataTable();
            cmd = new SqlCommand("WF_MOR_MAMDelete", connection);
            cmd.Parameters.Add(new SqlParameter("@Emp_No", empNo??0));
            cmd.Parameters.Add(new SqlParameter("@S_DATE", sDate));
            cmd.Parameters.Add(new SqlParameter("@E_DATE", eDate));
            cmd.Parameters.Add(new SqlParameter("@Typ", typ));
            cmd.Parameters.Add(new SqlParameter("@P7", p7));
            cmd.CommandType = CommandType.StoredProcedure;
            da.SelectCommand = cmd;
            da.Fill(dt);
            connection.Close();
            connection.Dispose();
            DataRow expr_55 = dt.Rows[0];
            if (expr_55 != null)
            {
                UpdateKVCurrentMonth(empNo??0, sDate, eDate);
            }
            return expr_55;
        }
        // Token: 0x0600012C RID: 300 RVA: 0x000099E8 File Offset: 0x00007BE8
        internal void UpdateKVCurrentMonth(double empNo, double sDate, double eDate)
        {
            var mngFormFields = new RegisterRequests();
            ParmfileRow dataRow = mngFormFields.GetDataRow();
           DateTime now = DateTime.Now;
            DateTime dateTime;
            DateTime dateTime2;
            if (ShDate.DayOf(now) < dataRow.StartDay)
            {
                dateTime = ShDate.StrToDate(string.Format("{0}/{1}/{2}", UtilityManagers.ConvertToFourDigitNumber(ShDate.YearOf(now).ToString()), UtilityManagers.ConvertToTowDigitNumber((int)(ShDate.MonthOf(now) - 1)), UtilityManagers.ConvertToTowDigitNumber((int)dataRow.StartDay)));
                dateTime2 = ShDate.StrToDate(string.Format("{0}/{1}/{2}", UtilityManagers.ConvertToFourDigitNumber(ShDate.YearOf(now).ToString()), UtilityManagers.ConvertToTowDigitNumber((int)ShDate.MonthOf(now)), UtilityManagers.ConvertToTowDigitNumber((int)dataRow.StartDay))).AddDays(-1.0);
            }
            else
            {
                dateTime = ShDate.StrToDate(string.Format("{0}/{1}/{2}", UtilityManagers.ConvertToFourDigitNumber(ShDate.YearOf(now).ToString()), UtilityManagers.ConvertToTowDigitNumber((int)ShDate.MonthOf(now)), UtilityManagers.ConvertToTowDigitNumber((int)dataRow.StartDay)));
                dateTime2 = ShDate.StrToDate(string.Format("{0}/{1}/{2}", UtilityManagers.ConvertToFourDigitNumber(ShDate.YearOf(now).ToString()), UtilityManagers.ConvertToTowDigitNumber((int)(ShDate.MonthOf(now) + 1)), UtilityManagers.ConvertToTowDigitNumber((int)dataRow.StartDay))).AddDays(-1.0);
            }
            if (Math.Max(dateTime.ToOADate(), sDate) - Math.Min(dateTime2.ToOADate(), eDate) >= 0.0)
            {
                var constre = ConfigurationManager.ConnectionStrings["connectionStringProviderSystem"].ToString();
                var connection = new SqlConnection(constre);

                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }

                string query = @"UPDATE EMPLOYEE SET KVCurrentMonth = 0 WHERE EMP_NO=" + empNo;

                connection.Open();
                var command = new SqlCommand(query, connection);

                command.ExecuteNonQuery();
                
                connection.Close();
                connection.Dispose();



              //  base.ExecuteNonQuery(CommandType.Text, "UPDATE EMPLOYEE SET KVCurrentMonth = 0 WHERE EMP_NO=" + empNo);
            }
        }
        // Token: 0x060001CA RID: 458 RVA: 0x000134A4 File Offset: 0x000116A4
        private bool CheckAccess(RequestRow reqRow, double currentEmpNo, ref string erroAccess)
      {
          LimitedManagerRow limitedManagerRow = null;
            var registerRest=new RegisterRequests();
          DataRow dataByEmpNo = registerRest.GetDataByEmpNo(reqRow.EmpNo);
          if (dataByEmpNo != null && dataByEmpNo["EmpMngr"].ToString().Equals(currentEmpNo.ToString()))
          {
              limitedManagerRow = new LimitedManagerRow
              {
                  LimitDailyDuty = (int) ((dataByEmpNo["Duty"] is DBNull) ? 0 : Convert.ToInt16(dataByEmpNo["Duty"])),
                  LimitDailyLeave = (int) ((dataByEmpNo["Leave"] is DBNull) ? 0 : Convert.ToInt16(dataByEmpNo["Leave"])),
                  LimitDailyLeaveOut =
                      (int) ((dataByEmpNo["LeaveOut"] is DBNull) ? 0 : Convert.ToInt16(dataByEmpNo["LeaveOut"])),
                  LimitForgottenInOut = true,
                  LimitInOutCorrection = true,
                  LimitLeaveAddition = 9999,
                  LimitOverTime = 9999,
                  LimitShiftReplace = 999,
                  LimitTimeDuty = 2359,
                  LimitTimeLeave = 2359,
                  LimitTimeDutyNoReturn = true,
                  LimitDailyDutyPerMonth = 999,
                  LimitDailyLeavePerMonth = 999,
                  LimitDailyLeaveOutPerMonth = 999,
                  LimitForgottenInOutPerMonth = true,
                  LimitInOutCorrectionPerMonth = true,
                  LimitLeaveAdditionPerMonth = 9999,
                  LimitOverTimePerMonth = 9999,
                  LimitShiftReplacePerMonth = 999,
                  LimitTimeDutyPerMonth = 99959,
                  LimitTimeLeavePerMonth = 99959,
                  LimitTimeDutyNoReturnPerMonth = true
              };
          }
          if (limitedManagerRow == null)
          {
              limitedManagerRow = GetData(reqRow.CurEmpNo, reqRow.CurSection);
          }
          return this.HaveAccessLimitManagerPerRequest(limitedManagerRow, reqRow, ref erroAccess) &&
                 this.HaveAccessLimitManagerPerMonthAndSection(limitedManagerRow, reqRow, ref erroAccess);
      }
        // Token: 0x060001CB RID: 459 RVA: 0x00013668 File Offset: 0x00011868
        private bool HaveAccessLimitManagerPerMonthAndSection(LimitedManagerRow lmRow, RequestRow reqRow, ref string erroAccess)
        {
            ActionEnum actionIdByCard = GetActionIdByCard(reqRow.Type, (short)reqRow.OperationsId, reqRow.IsDay);
            int num = 0;
            switch (actionIdByCard)
            {
                case ActionEnum.DailyLeave:
                    if (lmRow.LimitDailyLeavePerMonth < 31)
                    {
                        DataTable dataTable = this.GetSumRequestInCurrentMonth(reqRow.EmpNo, (int)actionIdByCard, reqRow.IsDay, lmRow.LimitDailyLeavePerMonth);
                        if (dataTable != null && dataTable.Rows.Count > 0)
                        {
                            erroAccess = this.GetErroAccess(dataTable, "ماهیانه");
                            return false;
                        }
                    }
                    break;
                case ActionEnum.DailyDuty:
                    if (lmRow.LimitDailyDutyPerMonth < 31)
                    {
                        DataTable dataTable = this.GetSumRequestInCurrentMonth(reqRow.EmpNo, (int)actionIdByCard, reqRow.IsDay, lmRow.LimitDailyDutyPerMonth);
                        if (dataTable != null && dataTable.Rows.Count > 0)
                        {
                            erroAccess = this.GetErroAccess(dataTable, "ماهیانه");
                            return false;
                        }
                    }
                    break;
                case ActionEnum.TimeLeave:
                    if (lmRow.LimitTimeLeavePerMonth < 22000)
                    {
                        DataTable dataTable = this.GetSumRequestInCurrentMonth(reqRow.EmpNo, (int)actionIdByCard, reqRow.IsDay, lmRow.LimitTimeLeavePerMonth);
                        if (dataTable != null && dataTable.Rows.Count > 0)
                        {
                            erroAccess = this.GetErroAccess(dataTable, "ماهیانه");
                            return false;
                        }
                    }
                    break;
                case ActionEnum.InOutCorrection:
                case ActionEnum.InOutTypeCorrection:
                    if (!lmRow.LimitInOutCorrectionPerMonth)
                    {
                        return false;
                    }
                    break;
                case ActionEnum.OverTime:
                    {
                        DataTable dataTable;
                        if (lmRow.LimitOverTimePerMonth < 22000)
                        {
                            dataTable = this.GetSumRequestInCurrentMonth(reqRow.EmpNo, (int)actionIdByCard, reqRow.IsDay, lmRow.LimitOverTimePerMonth);
                            if (dataTable != null && dataTable.Rows.Count > 0)
                            {
                                erroAccess = this.GetErroAccess(dataTable, "ماهیانه");
                                return false;
                            }
                        }
                      
                        DataRow byEmpNo = GetByEmpNo(reqRow.EmpNo);
                        int limitValue = 0;
                        if (!(byEmpNo["WF_LimitOverTime"] is DBNull))
                        {
                            int.TryParse(byEmpNo["WF_LimitOverTime"].ToString(), out limitValue);
                        }
                        int.TryParse(byEmpNo["SEC_NO"].ToString(), out num);
                        dataTable = GetSectionOverTimeAccess((double)num, limitValue, DateTime.Now);
                        if (dataTable != null && dataTable.Rows.Count > 0)
                        {
                            erroAccess = this.GetErroAccess(dataTable, "بخش");
                            return false;
                        }
                        break;
                    }
                case ActionEnum.ShiftReplace:
                    if (lmRow.LimitShiftReplacePerMonth < 31)
                    {
                        DataTable dataTable = this.GetSumRequestInCurrentMonth(reqRow.EmpNo, (int)actionIdByCard, reqRow.IsDay, lmRow.LimitShiftReplacePerMonth);
                        if (dataTable != null && dataTable.Rows.Count > 0)
                        {
                            erroAccess = this.GetErroAccess(dataTable, "ماهیانه");
                            return false;
                        }
                    }
                    break;
                case ActionEnum.LeaveAddition:
                    if (lmRow.LimitLeaveAdditionPerMonth < 22000)
                    {
                        DataTable dataTable = this.GetSumRequestInCurrentMonth(reqRow.EmpNo, (int)actionIdByCard, reqRow.IsDay, lmRow.LimitLeaveAdditionPerMonth);
                        if (dataTable != null && dataTable.Rows.Count > 0)
                        {
                            erroAccess = this.GetErroAccess(dataTable, "ماهیانه");
                            return false;
                        }
                    }
                    break;
                case ActionEnum.ForgottenInOut:
                    if (!lmRow.LimitForgottenInOutPerMonth)
                    {
                        return false;
                    }
                    break;
                case ActionEnum.TimeDuty:
                    if (lmRow.LimitTimeDutyPerMonth < 22000)
                    {
                        DataTable dataTable = this.GetSumRequestInCurrentMonth(reqRow.EmpNo, (int)actionIdByCard, reqRow.IsDay, lmRow.LimitTimeDutyPerMonth);
                        if (dataTable != null && dataTable.Rows.Count > 0)
                        {
                            erroAccess = this.GetErroAccess(dataTable, "ماهیانه");
                            return false;
                        }
                    }
                    break;
                case ActionEnum.DailyLeaveOut:
                    if (lmRow.LimitDailyLeaveOutPerMonth < 31)
                    {
                        DataTable dataTable = this.GetSumRequestInCurrentMonth(reqRow.EmpNo, (int)actionIdByCard, reqRow.IsDay, lmRow.LimitDailyLeaveOutPerMonth);
                        if (dataTable != null && dataTable.Rows.Count > 0)
                        {
                            erroAccess = this.GetErroAccess(dataTable, "ماهیانه");
                            return false;
                        }
                    }
                    break;
                case ActionEnum.TimeDutyNoReturn:
                    if (!lmRow.LimitTimeDutyNoReturnPerMonth)
                    {
                        return false;
                    }
                    break;
            }
            return true;
        }
        // Token: 0x060001CD RID: 461 RVA: 0x00013BA4 File Offset: 0x00011DA4
        private string GetErroAccess(DataTable SumRequestInCurrentMonth, string title = "ماهیانه")
        {
            string result = string.Empty;
            foreach (DataRow dataRow in SumRequestInCurrentMonth.Rows)
            {
                result = string.Format("دوره {0} مصرفی {2} {1} \n", dataRow["Period"], UtilityManagers.ConvertToTimeFormat(UtilityManagers.ConvertToFiveDigitNumber(dataRow["sumreq"].ToString())), title);
            }
            return result;
        }
        // Token: 0x060001D1 RID: 465 RVA: 0x000140C6 File Offset: 0x000122C6
        public DataTable GetSumRequestInCurrentMonth(double empNo, int actionId, bool isDay, int limitValue)
        {
            //return base.ExecuteDataTable("WF_IsManagerAccess", new object[]
            //{
            //    empNo,
            //    actionId,
            //    isDay,
            //    limitValue
            //});
            var constre = ConfigurationManager.ConnectionStrings["connectionStringProviderSystem"].ToString();
            var connection = new SqlConnection(constre);

            if (connection.State == ConnectionState.Open)
            {
                connection.Close();
            }
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = new DataTable();
            cmd = new SqlCommand("WF_IsManagerAccess", connection);
            cmd.Parameters.Add(new SqlParameter("@EmpNo", empNo));
            cmd.Parameters.Add(new SqlParameter("@ActionId", actionId));
            cmd.Parameters.Add(new SqlParameter("@IsDay", isDay));
            cmd.Parameters.Add(new SqlParameter("@Limitvalue", limitValue));
            cmd.CommandType = CommandType.StoredProcedure;
            da.SelectCommand = cmd;
            da.Fill(dt);
            connection.Close();
            connection.Dispose();
            return dt;
        }
        // Token: 0x060001D2 RID: 466 RVA: 0x000140FE File Offset: 0x000122FE
        public DataTable GetSectionOverTimeAccess(double secNo, int limitValue, DateTime startDate)
        {
            //return base.ExecuteDataTable("WF_SectionOverTimeAccess", new object[]
            //{
            //    secNo,
            //    limitValue,
            //    startDate
            //});
            var constre = ConfigurationManager.ConnectionStrings["connectionStringProviderSystem"].ToString();
            var connection = new SqlConnection(constre);

            if (connection.State == ConnectionState.Open)
            {
                connection.Close();
            }
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = new DataTable();
            cmd = new SqlCommand("WF_SectionOverTimeAccess", connection);
            cmd.Parameters.Add(new SqlParameter("@secNo", secNo));
            cmd.Parameters.Add(new SqlParameter("@Limitvalue", limitValue));
            cmd.Parameters.Add(new SqlParameter("@startDate", startDate));
            
            cmd.CommandType = CommandType.StoredProcedure;
            da.SelectCommand = cmd;
            da.Fill(dt);
            connection.Close();
            connection.Dispose();
            return dt;
        }
        // Token: 0x060001E3 RID: 483 RVA: 0x0001445C File Offset: 0x0001265C
        public DataRow GetByEmpNo(double empNo)
        {
            var constre = ConfigurationManager.ConnectionStrings["connectionStringProviderSystem"].ToString();
            var connection = new SqlConnection(constre);

            if (connection.State == ConnectionState.Open)
            {
                connection.Close();
            }
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = new DataTable(); 
            string commandText = "SELECT s.* FROM SECTIONS s JOIN EMPLOYEE e ON e.SEC_NO = s.SEC_NO WHERE e.EMP_NO=" + empNo;
            cmd = new SqlCommand(commandText, connection);
           // cmd.Parameters.Add(new SqlParameter("@EMP_NO", empNo));
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            da.Fill(dt);
            connection.Close();
            connection.Dispose();
            return dt.Rows[0];
           
            //return base.ExecuteDataRow(CommandType.Text, commandText);
        }
        public ActionEnum GetActionIdByCard(short cardNo, short cardType, bool isDay)
        {
            switch (cardType)
            {
                case 3:
                    if (!isDay)
                    {
                        return ActionEnum.TimeDuty;
                    }
                    return ActionEnum.DailyDuty;
                case 4:
                    return ActionEnum.TimeDutyNoReturn;
                case 5:
                    if (!isDay)
                    {
                        return ActionEnum.TimeLeave;
                    }
                    if (cardNo == 59 || cardNo == 70)
                    {
                        return ActionEnum.DailyLeaveOut;
                    }
                    return ActionEnum.DailyLeave;
                default:
                    switch (cardNo)
                    {
                        case 100:
                            return ActionEnum.ForgottenInOut;
                        case 101:
                            return ActionEnum.InOutCorrection;
                        case 102:
                            return ActionEnum.ShiftReplace;
                        case 103:
                            return ActionEnum.OverTime;
                        case 104:
                            return ActionEnum.LeaveAddition;
                        case 105:
                            return ActionEnum.InOutTypeCorrection;
                        default:
                            return ActionEnum.DailyLeave;
                    }
                    break;
            }
        }
        // Token: 0x060001CC RID: 460 RVA: 0x000139F4 File Offset: 0x00011BF4
        private bool HaveAccessLimitManagerPerRequest(LimitedManagerRow lmRow, RequestRow reqRow, ref string erroAccess)
        {
            switch (reqRow.Type)
            {
                case 100:
                    return lmRow.LimitForgottenInOut && (!"9,10".Split(new char[]
                    {
                    ','
                    }).Contains(reqRow.Duration) || lmRow.LimitTimeDuty >= 1) && (!"17,26".Split(new char[]
                    {
                    ','
                    }).Contains(reqRow.Duration) || lmRow.LimitTimeLeave >= 1);
                case 101:
                case 105:
                    return lmRow.LimitInOutCorrection;
                case 102:
                    return lmRow.LimitShiftReplace >= Convert.ToInt32(reqRow.Duration);
                case 103:
                    return lmRow.LimitOverTime >= Convert.ToInt32(reqRow.Duration);
                case 104:
                    return lmRow.LimitLeaveAddition >= Convert.ToInt32(reqRow.Duration);
                default:
                    switch (reqRow.OperationsId)
                    {
                        case 3:
                            if (reqRow.IsDay)
                            {
                                return lmRow.LimitDailyDuty >= Convert.ToInt32(reqRow.Duration);
                            }
                            return lmRow.LimitTimeDuty >= Convert.ToInt32(reqRow.Duration);
                        case 4:
                            return Convert.ToBoolean(lmRow.LimitTimeDutyNoReturn);
                        case 5:
                            if (!reqRow.IsDay)
                            {
                                return lmRow.LimitTimeLeave >= Convert.ToInt32(reqRow.Duration);
                            }
                            if (reqRow.Type == 59 || reqRow.Type == 70)
                            {
                                return lmRow.LimitDailyLeaveOut >= Convert.ToInt32(reqRow.Duration);
                            }
                            return lmRow.LimitDailyLeave >= Convert.ToInt32(reqRow.Duration);
                        default:
                            return true;
                    }
                    break;
            }
        }
        // Token: 0x0600014F RID: 335 RVA: 0x0000A7D2 File Offset: 0x000089D2
        public LimitedManagerRow GetData(double managerEmpNo, int secNo)
        {

            var constre = ConfigurationManager.ConnectionStrings["connectionStringProviderSystem"].ToString();
            var connection = new SqlConnection(constre);

            if (connection.State == ConnectionState.Open)
            {
                connection.Close();
            }
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = new DataTable();
            cmd = new SqlCommand("WF_GetLimitManager", connection);
            cmd.Parameters.Add(new SqlParameter("@Manager_EmpNO", managerEmpNo));
            cmd.Parameters.Add(new SqlParameter("@Sec_No", secNo));
            cmd.CommandType = CommandType.StoredProcedure;
            da.SelectCommand = cmd;
            da.Fill(dt);
            connection.Close();
            connection.Dispose();
            return new LimitedManagerRow(dt);
        }
        // Token: 0x060001A4 RID: 420 RVA: 0x0001119C File Offset: 0x0000F39C
        public RequestRow GetDataById(int requestId)
        {
            //DataRow dataRow = base.ExecuteDataRow("WF_RequestsGetDataByID", new object[]
            //{
            //    requestId
            //});
            var constre = ConfigurationManager.ConnectionStrings["connectionStringProviderSystem"].ToString();
            var connection = new SqlConnection(constre);

            if (connection.State == ConnectionState.Open)
            {
                connection.Close();
            }
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = new DataTable();
            cmd = new SqlCommand("WF_RequestsGetDataByID", connection);
            cmd.Parameters.Add(new SqlParameter("@requestID", requestId));
            cmd.CommandType = CommandType.StoredProcedure;
            da.SelectCommand = cmd;
            da.Fill(dt);
            connection.Close();
            connection.Dispose();
            DataRow dataRow = dt.Rows[0];
            if (dataRow != null)
            {
                return new RequestRow(dataRow);
            }
            return new RequestRow();
        }
        public List<MobilePortfolioViewModel> GetByManagerEmpNo(double empManager, DateTime startDate, DateTime endDate, short? isFinalApprove)
        {
           // DataTable dataTable = new DataTable();
            var constre = ConfigurationManager.ConnectionStrings["connectionStringProviderSystem"].ToString();
            var connection = new SqlConnection(constre);
            var lstPortfolio = new List<MobilePortfolioViewModel>();
            if (connection.State == ConnectionState.Open)
            {
                connection.Close();
            }
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter da = new SqlDataAdapter();
            DataTable sqlDataReader = new DataTable();
            cmd = new SqlCommand("WF_RequestGetByManagerEmpNo", connection);
            cmd.Parameters.Add(new SqlParameter("@EmpManager", empManager));
            cmd.Parameters.Add(new SqlParameter("@StartDate", startDate));
            cmd.Parameters.Add(new SqlParameter("@EndDate", endDate));
            cmd.Parameters.Add(new SqlParameter("@IsFinalApproved", (isFinalApprove ?? (object)DBNull.Value)));
            cmd.CommandType = CommandType.StoredProcedure;
            da.SelectCommand = cmd;
            da.Fill(sqlDataReader);
            connection.Close();
            connection.Dispose();
           
            {
                if (sqlDataReader.Rows.Count>0)
                {
                   // var total = sqlDataReader.Rows.Count;
                    for (int i = 0; i < sqlDataReader.Rows.Count; i++)
                    {
                        DataRow dataRow = sqlDataReader.Rows[i];
                        var portfolioModel = new MobilePortfolioViewModel();
                        portfolioModel.Docid = dataRow["RequestsID"].ToString();
                        portfolioModel.RequestSDate = ShDate.DateToStr((DateTime)dataRow["StartDate"]);
                        portfolioModel.RequestEDate = ShDate.DateToStr((DateTime)dataRow["StartDate"]);
                        portfolioModel.RequesterName = dataRow["EmployeeName"].ToString();
                        portfolioModel.RequesterDescription = dataRow["Description"].ToString();
                        portfolioModel.ActionDate = ShDate.DateToStr((DateTime)dataRow["SubmittedDate"]);
                        portfolioModel.DocTypeId = dataRow["Type"].ToString();
                        portfolioModel.RequesterCode = dataRow["Emp_NO"].ToString();
                        portfolioModel.ViewState = 0;
                        portfolioModel.RowNumber = 0;
                        portfolioModel.Action = 0;
                        portfolioModel.PageNumber = 1;
                        portfolioModel.DocMemberId = 0;
                        portfolioModel.TotalPage = 0;
                        portfolioModel.DocDes = dataRow["TITLE"].ToString();
                        portfolioModel.DoctypeName = dataRow["ReqStatus"].ToString();

                    string value = dataRow["Duration"].ToString();

                      short typeNo = Convert.ToInt16(dataRow["Type"]);
                
                        bool isDay;
                        bool.TryParse(dataRow["IS_DAY"].ToString(), out isDay);
                        string value2 = dataRow["TITLE"].ToString();

                        bool? isFinalApproved = null;
                        if (isFinalApprove.HasValue)
                        {
                            isFinalApproved = new bool?(isFinalApprove.Value != 0);
                        }
                        string empty = string.Empty;
                        GetDuration(Convert.ToInt32(dataRow["OperationsID"]), (int)typeNo, isDay, isFinalApproved,
                            dataRow["StartHour"].ToString(), dataRow["EndHour"].ToString(), ref value, ref empty,
                            ref value2);
                  
                        portfolioModel.DocDes = portfolioModel.DocDes +
                                                              " مقدار " + value;
                        lstPortfolio.Add(portfolioModel);
                    }
                }
            }
            return lstPortfolio;
        }

      public ResultViewModel GetFlowList(string requestId)
      {
          try
          {
              var emp_no = "";
              var titleRequest = "";
                var constreing = ConfigurationManager.ConnectionStrings["connectionStringProviderSystem"].ToString();
                var connections = new SqlConnection(constreing);

                if (connections.State == ConnectionState.Open)
                {
                    connections.Close();
                }

                string query = @"select EMP_NO,SubmittedByEmployeeID,crd.TITLE from Requests as reqst
                                    join CARDS as crd on reqst.Type=crd.CARD_NO
                                    where RequestsID=" + requestId;

                connections.Open();
                var command = new SqlCommand(query, connections);

                var resultRequest = command.ExecuteReader();
                while (resultRequest.Read())
                {
                    emp_no = resultRequest["SubmittedByEmployeeID"].ToString();
                    titleRequest= resultRequest["TITLE"].ToString();

                }
                connections.Close();
                connections.Dispose();




              var docDescription = "";
              var docComment = "";
              //  var resultPortfolioGeneral = GetByManagerEmpNo(double.Parse(emp_no), new DateTime(2000, 01, 01),
              //    DateTime.MaxValue, null);
      
              //var firstRequest = resultPortfolioGeneral.FirstOrDefault(x => x.Docid == requestId);
              //if (firstRequest != null)
              //{
              //    titleRequest = firstRequest.DocDes;
                  docDescription = titleRequest;
              //}
                var resultPortfolio =
                    GetByManagerEmpNo(double.Parse(emp_no), new DateTime(2000, 01, 01),
                        DateTime.MaxValue, null).ToList();
              var requestInfo = resultPortfolio.FirstOrDefault(x => x.Docid == requestId);
                var formfieldMng=new RequestFormLoader();
              var resultFormEdit = formfieldMng.GetFormFieldForEdit(emp_no, requestId);
              if (requestInfo != null)
              {
                 // docDescription = requestInfo.DocDes;
                  docComment = requestInfo.RequesterDescription;
              }
              if (resultFormEdit.Validate)
              {
                  var modelData = JsonConvert.DeserializeObject <List<FormFieldsMobileViewModel>>(resultFormEdit.Message);
                   foreach (var formItems in modelData)
                   {

                       if (formItems.Type != "DateComponent" && formItems.Type != "SelectBox" &&
                           formItems.Type != "AutoCompleteField" && formItems.Type != "ComboBox")
                       {
                               docDescription = docDescription + " " + formItems.Title + " " + formItems.DefaultValue;
                       }
                       if (formItems.Type == "DateComponent")
                       {
                           var resultDate = JsonConvert.DeserializeObject<SelectedDateViewModel>(formItems.DefaultValue);
                            var selectedDate=new DateTime(resultDate.Y, resultDate.M, resultDate.D).ToFa();
                           docDescription = docDescription + " " + formItems.Title + " " + selectedDate;
                       }
                   
                   }
              }
             

                var lstFlows =new List<MobileFlowLogViewModel>();
                var constre = ConfigurationManager.ConnectionStrings["connectionStringProviderSystem"].ToString();
                var connection = new SqlConnection(constre);

                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }

              

                connection.Open();
               // var command = new SqlCommand(query, connection);
               var cmd = new SqlCommand("WF_RequestGetFlow", connection);
                cmd.Parameters.Add(new SqlParameter("@RequestID", requestId));
          
                cmd.CommandType = CommandType.StoredProcedure;
                var result = cmd.ExecuteReader();
              var counter = 1;
                while (result.Read())
                {
                    var modelFlow=new MobileFlowLogViewModel();
                    // modelFlow.StepNo = counter.ToString();
                    //modelFlow.Description = docComment;
                    modelFlow.Descr = docDescription;
                    modelFlow.ActionDate = result["DateMoveUpFa"].ToString(); 
                    modelFlow.RoleName = result["posTitle"].ToString();
                    modelFlow.State = result["fstate"].ToString();
                    modelFlow.PersonName = result["fromEmpName"].ToString();
                    // modelFlow.RPName = result["fromEmpName"].ToString();
                    modelFlow.RPName = "";
                    modelFlow.StateMobile = result["fstate"].ToString();
                    modelFlow.StateMobileTitle = result["fstate"].ToString()== "ثبت درخواست"?"فرستنده": result["fstate"].ToString() == "بررسي نشده" ?"ارسال به": result["fstate"].ToString();
                    modelFlow.StateTitle= result["fstate"].ToString();
                    modelFlow.ActionTime = result["TimeMoveUp"].ToString();
                   // result["TITLE"].ToString();
                    lstFlows.Add(modelFlow);
                    counter++;

                }
                connection.Close();
                connection.Dispose();





                return new ResultViewModel
                {
                    Validate = true,
                   Message = JsonConvert.SerializeObject(lstFlows)

                };

            }
            catch (Exception ex)
          {

                return new ResultViewModel
                {
                    Validate = false,
                    ValidateMessage = ex.Message

                };
            }
            
      }

      public ResultViewModel GetShowRequest(string SessionID, string OnlineUserID, string PersonCode, string WpID, string StartDate,
           string EndDate, string DocType, string DocState, string PageSize, string PageNumber)
      {
          try
          {
              var lstData=new List<GetDocInfoModel>();
              var resultDataRequest = GetdataSource(PersonCode);
                var selecteddate=new DateTime();
              foreach (DataRow row in resultDataRequest.Rows)
              {
                  var reQuester = row["SubmittedByEmployeeID"].ToString() != PersonCode
                      ? "درخواست دهنده:" + GetEmpName(row["SubmittedByEmployeeID"].ToString()):"";
                  var ownerName = row["EMP_NO"].ToString() != PersonCode
                      ? "مجوز مربوط به شخص :" + row["EmployeeName"].ToString()
                      : "";


                  var modelRow = new GetDocInfoModel
                  {
                      DocID = row["RequestsID"].ToString(),
                      DocTypeID = row["Type"].ToString(),
                      ActorDisplayName = row.IsNull("IsFinalApproved") ?  row["ManagerName"].ToString() : row["EmployeeName"].ToString(),
                      DocTypeNeme = row["TITLE"].ToString()+" "+(row["AcceptCode"].ToString()=="21"?" (مجوز ابطال شد)  ":" "),
                      DocTitle = " به مدت " + row["Duration"].ToString()+" "+ownerName+" "+ reQuester,
                      StatusID = row.IsNull("IsFinalApproved")?"201" : bool.Parse(row["IsFinalApproved"].ToString()) ?"203":"204",
                      TotalPage = 0,
                      DocDescription = row["Description"].ToString()+" "+(row.IsNull("IsFinalApproved") ? "" : bool.Parse(row["IsFinalApproved"].ToString()) ? "" :"نظر مدیر:"+ row["ManagerIdea"].ToString()),
                      //Description = formTrans.Any(x => x.FormInstanceId == i.FormInstanceId) ? formTrans.FirstOrDefault(x => x.FormInstanceId == i.FormInstanceId).DescriptionTranslate : i.Description,//i.Description,                                     
                      DateRecord = row["StartDate"].ToString()
                      // ,SubmiteSelectedDate = (DateTime.Parse(row["SubmittedDate"].ToString()).Year + "/" + DateTime.Parse(row["SubmittedDate"].ToString()).Month + "/" + DateTime.Parse(row["SubmittedDate"].ToString()).Day).ToEn() 
                      ,
                      //SubmiteSelectedDate = DateTime.TryParse(row["StartDate"].ToString(),out selecteddate) ? (DateTime.Parse(row["StartDate"].ToString()).Year + "/" + DateTime.Parse(row["StartDate"].ToString()).Month + "/" + DateTime.Parse(row["StartDate"].ToString()).Day).ToEn():DateTime.MaxValue
                      SubmiteSelectedDate = row["StartDate"].ToString().ToEn() 
                      ,
                      Accepter = row.IsNull("IsFinalApproved") ? row["EmployeeName"].ToString() : row["ManagerName"].ToString(),
                      Editable = row.IsNull("IsFinalApproved") ,
                      Deletable = row.IsNull("IsFinalApproved") || bool.Parse(row["IsFinalApproved"].ToString()) 
                      //row.IsNull("IsFinalApproved") ?"درجریان": bool.Parse(row["IsFinalApproved"].ToString()) ? "تایید شده":"عدم تایید"
                  };
                    lstData.Add(modelRow);

              }
              var sdate = DateTime.Now;
              var edate = DateTime.Now;
              var mngWebservice=new WebServiceMethods();
              mngWebservice.FindWorkPreiodDays(WpID, PersonCode, ref sdate, ref edate);
              lstData = lstData.Where(x =>x.SubmiteSelectedDate >= sdate && x.SubmiteSelectedDate <= edate).ToList();
              if (DocType != "\n      "&& DocType != "0")
              {
                    var splitedCodes = DocType.Split('-');
                    var actionId = splitedCodes[0];
                    var selectedCodeId = splitedCodes[1];
                    lstData = lstData.Where(x => x.DocTypeID == selectedCodeId).ToList();
              }
              if (DocState != "\n      " && DocState != "0")
              {
                  switch (DocState)
                  {
                        case "0":
                          DocState = "201";
                          break;
                        case "1":
                            DocState = "203";
                            break;
                        case "2":
                            DocState = "204";
                            break;
                    }
                    lstData = lstData.Where(x => x.StatusID == DocState).ToList();
                 

              }
              if (lstData.Count > 0)
              {
                    lstData = lstData.OrderByDescending(x => x.SubmiteSelectedDate).ToList();
                    return new ResultViewModel
                  {
                      Validate = true,
                      Message = JsonConvert.SerializeObject(lstData)

                  };
              }
              else
              {
                    return new ResultViewModel
                    {
                        Validate = true,
                        Message = ""

                    };
                }
          }
          catch (Exception ex)
          {

                return new ResultViewModel
                {
                    Validate = false,
                    ValidateMessage = ex.Message

                };
            }
      }

      //public void GetCardex()
      //{
      //    try
      //    {
      //          WorkFlowsDatabase.Instance.Kardex.GetDayAndTimestring();
      //    }
      //    catch (Exception)
      //    {
              
      //        throw;
      //    }
      //}
      public string GetEmpName(string empNo)
      {
          try
          {
                UserActiveRow userActiveRow = WorkFlowsDatabase.Instance.Employee.GetDataByEmpNoForLogin(double.Parse(empNo));
              return userActiveRow.FullName;
          }
          catch (Exception)
          {

              return "";
          }
      }
        private DataTable GetdataSource(string empNo)
        {
            UserActiveRow userActiveRow = WorkFlowsDatabase.Instance.Employee.GetDataByEmpNoForLogin(double.Parse(empNo));
            if (!userActiveRow.IsSupervisor && !userActiveRow.IsManager)
            {
                return WorkFlowsDatabase.Instance.Requests.GetDataByEmployeeNumber(double.Parse(empNo));
            }
            if (!userActiveRow.IsManager)
            {
                return WorkFlowsDatabase.Instance.Requests.GetDataBySupervisor(userActiveRow.EmpNo);
            }
            //if (this.EditEmpNo.SelectedIndex > 0)
            //{
            //    return WorkFlowsDatabase.Instance.Requests.GetDataBySupervisor(Convert.ToDouble(this.EditEmpNo.SelectedValue));
            //}
            return WorkFlowsDatabase.Instance.Requests.GetDataBySupervisor(userActiveRow.EmpNo);
        }

      public ResultViewModel DeleteRequests(string onlineUser, string requestId)
      {
          try
          {
                WorkFlowsDatabase.Instance.Requests.Delete(int.Parse(requestId));
                WorkFlowsDatabase.Instance.Log.Insert("حذف درخواست", "Mobile",double.Parse(onlineUser), string.Concat(new object[]
                {
                    "WF_RequestsDelete : ",
                    requestId,
                    ";ip:",
                    "Mobile",
                    ";host=",
                    "Mobile"
                }));
                return new ResultViewModel
                {
                    Validate = true,
                    ValidateMessage = "حذف با موفقیت انجام شد"
                };
            }
          catch (Exception ex)
          {
              
              return new ResultViewModel
              {
                  Validate = false,
                  ExceptionMessage = ex.Message
              };
          }
      }
        public ResultViewModel DeleteAcceptedRequests(string onlineUser, string requestId)
        {
            try
            {
                WorkFlowsDatabase.Instance.Requests.UpdateAcceptCode(int.Parse(requestId), 21);
                return new ResultViewModel
                {
                    Validate = true,
                    ValidateMessage = "حذف با موفقیت انجام شد"
                };
            }
            catch (Exception ex)
            {

                return new ResultViewModel
                {
                    Validate = false,
                    ExceptionMessage = ex.Message
                };
            }
        }
        public void GetDuration(int operationsId, int typeNo, bool isDay, bool? isFinalApproved, string startHour, string endHour, ref string duration, ref string requestedTime, ref string cardTitle)
        {
            if (string.IsNullOrEmpty(duration))
            {
                return;
            }
            switch (operationsId)
            {
                case 1:
                    duration = UtilityManagers.ConvertToTimeFormat(duration);
                    requestedTime = UtilityManagers.ConvertToTimeFormat(requestedTime);
                    return;
                case 2:
                    switch (typeNo)
                    {
                        case 100:
                          //  duration = string.Empty;
                            //if (isFinalApproved.HasValue && isFinalApproved.Value)
                            //{
                                requestedTime = UtilityManagers.ConvertToTimeFormat(startHour);
                            duration = UtilityManagers.ConvertToTimeFormat(startHour);
                                return;
                           // }
                           // break;
                        case 101:
                            requestedTime = ((isFinalApproved.HasValue && isFinalApproved.Value) ? duration : string.Empty);
                            if (string.IsNullOrEmpty(endHour) || endHour == "0" || endHour == "00:00")
                            {
                                duration = "حذف " + startHour;
                                return;
                            }
                            duration = "قدیم:" + startHour + " جدید:" + UtilityManagers.ConvertToTimeFormat(endHour);
                            return;
                        case 102:
                            duration += "روز";
                            if (isFinalApproved.HasValue && isFinalApproved.Value)
                            {
                                requestedTime = duration;
                                return;
                            }
                            break;
                        case 103:
                            break;
                        case 104:
                            duration = duration.Insert(duration.Length - 2, ":");
                            if (isFinalApproved.HasValue && isFinalApproved.Value)
                            {
                                requestedTime = duration;
                                return;
                            }
                            break;
                        case 105:
                            cardTitle = "اصلاح نوع ورودوخروج";
                            if (duration == "5")
                            {
                                cardTitle += "-اضافه کار نامجاز";
                            }
                            duration = UtilityManagers.ConvertToTimeFormat(endHour);
                            if (isFinalApproved.HasValue && isFinalApproved.Value)
                            {
                                requestedTime = duration;
                                return;
                            }
                            break;
                        default:
                            return;
                    }
                    break;
                case 3:
                    requestedTime = UtilityManagers.ConvertToTimeFormat(requestedTime);
                    if (isDay)
                    {
                        duration += "روز";
                        requestedTime += "روز";
                        return;
                    }
                    duration = UtilityManagers.MakeTimeFormat(duration);
                    requestedTime = UtilityManagers.ConvertToTimeFormat(requestedTime);
                    return;
                case 4:
                    duration = UtilityManagers.MakeTimeFormat(duration);
                    if (isFinalApproved.HasValue && isFinalApproved.Value)
                    {
                        requestedTime = duration;
                    }
                    cardTitle += " بدون بازگشت";
                    break;
                case 5:
                    if (isDay)
                    {
                        duration += "روز";
                        requestedTime += "روز";
                        return;
                    }
                    if (Convert.ToDouble(duration) > 1.0)
                    {
                        duration = UtilityManagers.ConvertToTimeFormat(duration);
                        requestedTime = UtilityManagers.ConvertToTimeFormat(requestedTime);
                        return;
                    }
                    break;
                default:
                    return;
            }
        }

        public ResultViewModel ChangePassword(string empNumber, string userName, string password, string newPassword)
        {
            try
            {
                var constre = ConfigurationManager.ConnectionStrings["connectionStringProviderSystem"].ToString();
                var connection = new SqlConnection(constre);

                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }

                string query = @"select emy.WFUserName from EMPLOYEE  as emy 
                                              
                                            where EMP_NO = @EmpNumber ";
             
                connection.Open();
                //var command = new SqlCommand(query, connection);
                //command.Parameters.Add("@UserName", SqlDbType.VarChar, 2000).Value = UserName;
                //command.Parameters.Add("@Password", SqlDbType.VarChar, 2000).Value = Password;
                var command = new SqlCommand(query, connection);
              
                command.Parameters.Add("@EmpNumber", SqlDbType.VarChar, 2000).Value = empNumber;
             

                var result = command.ExecuteReader();
                while (result.Read())
                {
                    userName = result["WFUserName"].ToString();
                   
                }
                connection.Close();
                connection.Dispose();
                var resultValidate = ValidParamChangePassword(userName, password, password);
                if (resultValidate.Validate &&
                    WorkFlowsDatabase.Instance.Employee.ChangeLogin(double.Parse(empNumber), userName, newPassword))
                {
                    WorkFlowsDatabase.Instance.Log.Insert("تغییر رمز کاربر", "Mobile", double.Parse(empNumber), string.Concat(new string[]
                {
                   userName,
                    ";ip:",
                    "Mobile",
                    ";host=",
                  "Mobile"
                }));
                    return new ResultViewModel
                    {
                        Validate = true,
                        ValidateMessage = "عملیات با موفقیت به اتمام رسید"
                    };
                }
               return new ResultViewModel
                {
                    Validate = false,
                    ValidateMessage = "ثبت با خطا روبرو شد"+resultValidate.Message,
                    ExceptionMessage = "ثبت با خطا روبرو شد" + resultValidate.Message
                };
                
            }
            catch (Exception ex)
            {
                var result = new ResultViewModel
                {
                    Validate = false,
                    ValidateMessage = ex.Message,
                    ExceptionMessage = ex.Message
                };
                return result;

            }
        }
        private ResultViewModel ValidParamChangePassword(string userName,string password,string rePassword)
        {
            var message = "";
            
            if (userName == "")
            {
                
                message = "کد کاربری را وارد نمایید";
                return new ResultViewModel
                {
                    Validate = false,
                    ValidateMessage = message
                };
            }
            if (password == "")
            {
                message = "رمز عبور را وارد نمایید";
            return new ResultViewModel
                {
                    Validate = false,
                    ValidateMessage = message
                };
}
             if (password != rePassword)
            {
                message = "رمز عبور با تکرار رمز عبور برابر نیست!";
                return new ResultViewModel
                {
                    Validate = false,
                    ValidateMessage = message
            };
        }

            return new ResultViewModel
            {
                Validate = true,
                ValidateMessage = message
        };
    }

        public PortfolioManager(WorkFlowsDatabase owner) : base(owner)
      {
      }
        public List<DayRequestInfoAndGeneralInfo> GetDataFile(int startWorkPeriodY,int startWorkPeriodM, double empNumber)
        {
            try
            {
                var alldaysInfo = new List<DayRequestInfoAndGeneralInfo>();
                DateTime dateTime;
            DateTime dateTime2;
            var empData = WorkFlowsDatabase.Instance.Employee.GetDataByEmpNoForLogin(empNumber);
            GetDates(startWorkPeriodM, startWorkPeriodY, out dateTime, out dateTime2);
            var maxcoulmn = 0;
            var resultDate= WorkFlowsDatabase.Instance.DataFile.GetDataByEmpNo(empNumber, dateTime2.ToOADate(), empData.GrpNo, dateTime.ToOADate(), ref maxcoulmn);
            foreach (DataRow dataRowView in resultDate.Rows)
            {
                    var dayInfo=new DayRequestInfoAndGeneralInfo();
                if (!string.IsNullOrEmpty(dataRowView["DayType"].ToString()))
                {
                    switch (Convert.ToInt32(dataRowView["DayType"]))
                    {
                        case 50:
                                dayInfo.EndDayNames = "تعطیل ";
                            
                            break;
                        case 51:
                      
                                dayInfo.EndDayNames = "تعطیل شیفت ";
                            break;
                        case 52:
                                dayInfo.EndDayNames = "تعطیل خاص ";
                            break;
                        default:
                                dayInfo.EndDayNames = dataRowView["DayType"].ToString();
                           break;
                    }
                        dayInfo.EndDayNames  = dataRowView["DayType"].ToString();
                }
                if (!string.IsNullOrEmpty(dataRowView["DayStatus"].ToString()))
                {
                    switch (Convert.ToInt32(dataRowView["CARD_TYPE"]))
                    {
                        case 1:
                                dayInfo.DayStatusName = dataRowView["DayStatus"].ToString();
                            break;
                        case 2:
                                dayInfo.DayStatusName = dataRowView["DayStatus"].ToString();
                            break;
                        case 3:
                                dayInfo.DayStatusName = dataRowView["DayStatus"].ToString();
                            break;
                        case 5:
                                dayInfo.DayStatusName = dataRowView["DayStatus"].ToString();
                            break;
                    }
                }
                dayInfo.DateOfRequest = dataRowView["ShamsiDate"].ToString().ToEn();
                    alldaysInfo.Add(dayInfo);
            }
            return alldaysInfo;

            }
            catch (Exception)
            {
                
               return new List<DayRequestInfoAndGeneralInfo>();
            }
            
        }
        public void GetDates(int monthNo, int yearNo, out DateTime toDate, out DateTime fromDate)
        {
            var mngFormFields = new RegisterRequests();
            ParmfileRow paramFileRows = mngFormFields.GetDataRow();
            if (paramFileRows.StartDay <= 1)
            {
                fromDate = ShDate.StrToDate(string.Concat(new object[]
                {
                    yearNo,
                    "/",
                    UtilityManagers.ConvertToTowDigitNumber(monthNo.ToString()),
                    "/",
                    UtilityManagers.ConvertToTowDigitNumber(paramFileRows.StartDay.ToString())
                }));
                toDate = ShDate.LastDayOfMoon(fromDate);
                return;
            }
            if (monthNo == 1)
            {
                fromDate = ShDate.StrToDate(yearNo - 1 + "/12/" + UtilityManagers.ConvertToTowDigitNumber(paramFileRows.StartDay.ToString()));
            }
            else
            {
                fromDate = ShDate.StrToDate(string.Concat(new object[]
                {
                    yearNo,
                    "/",
                    UtilityManagers.ConvertToTowDigitNumber((monthNo - 1).ToString()),
                    "/",
                    UtilityManagers.ConvertToTowDigitNumber(paramFileRows.StartDay.ToString())
                }));
            }
            toDate = ShDate.StrToDate(string.Concat(new object[]
            {
                yearNo,
                "/",
                UtilityManagers.ConvertToTowDigitNumber(monthNo.ToString()),
                "/",
                UtilityManagers.ConvertToTowDigitNumber(((int)(paramFileRows.StartDay - 1)).ToString())
            }));
        }

        public bool SentNotification(double personId)
        {
            try
            {
                //System.Threading.Thread.Sleep(2000);
                var notifymessage = "";
                DataTable notifyRows = WorkFlowsDatabase.Instance.Requests.GetNewRequest(personId,  DateTime.Now.AddDays(-120.0).Date);
                foreach (DataRow notifyRow in notifyRows.Rows)
                {
                    notifymessage = notifymessage+" "+ notifyRow["NAME"] +" "+ notifyRow["FAMILY"] + " درخواست مجوز" + notifyRow["TITLE"] + " به تعداد" +
                                    notifyRow["cnt"];
                }
                OneSignaleSender(personId.ToString(), notifymessage);
                return true;
            }
            catch (Exception ex)
            {
                return false;

            }
        }

        public bool OneSignaleSender(string empNumber,string message)
        {
            try
            {
               
                Int64 ToPersonID = Convert.ToInt64(empNumber);
                //StripHTML(
                string Message = message;
                string playerID = "";
                var constre = ConfigurationManager.ConnectionStrings["connectionStringGeneral"].ToString();
                var connection = new SqlConnection(constre);
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
                connection.Open();
                string sp = "SELECT * FROM OneSignalPushNotfication  WHERE personId = @PersonID";
                SqlDataAdapter da = new SqlDataAdapter(sp, connection);
                da.SelectCommand.Parameters.AddWithValue("@PersonID", ToPersonID);
                DataTable dt = new DataTable();
                da.Fill(dt);
                connection.Close();
                if (dt.Rows.Count >= 1)
                {
                    if (connection.State == ConnectionState.Open)
                    {
                        connection.Close();
                    }
                    playerID = dt.Rows[0]["playerId"].ToString();
                    var request = WebRequest.Create(WebConfigurationManager.AppSettings["oneSignalAddress"]) as HttpWebRequest;
                    request.KeepAlive = true;
                    request.Method = "POST";
                    request.ContentType = "application/json; charset=utf-8";
                    request.Headers.Add("authorization", "Basic MzE5Yjk4ZGItYjg1Ny00MTM3LTg4ZDAtMTc5ZjkyNTAwNGM3");
                    var serializer = new JavaScriptSerializer();

                    var obj = new
                    {
                        app_id = "e8c263b4-361c-4936-b633-d541b7dfb83f",
                        contents = new { en = Message },
                        include_player_ids = new string[] { playerID }
                    };
                    var param = serializer.Serialize(obj);
                    byte[] byteArray = Encoding.UTF8.GetBytes(param);



                    try
                    {
                        using (var writer = request.GetRequestStream())
                        {
                            writer.Write(byteArray, 0, byteArray.Length);
                        }

                        using (var response = request.GetResponse() as HttpWebResponse)
                        {
                            using (var reader = new StreamReader(response.GetResponseStream()))
                            {
                               var responseContent = reader.ReadToEnd();
                            }
                        }
                    }
                    catch (WebException ex)
                    {

                        System.Diagnostics.Debug.WriteLine(ex.Message);
                        System.Diagnostics.Debug.WriteLine(new StreamReader(ex.Response.GetResponseStream()).ReadToEnd());
                        return false;
                    }
                }
                else
                {
                    if (connection.State == ConnectionState.Open)
                    {
                        connection.Close();
                    }
                    //return "Player ID Not Found";
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {

                return false;
            }
        }


    }

    public class RequestRow: BaseCls
    {
        // Token: 0x060001AC RID: 428 RVA: 0x00002269 File Offset: 0x00000469
        public RequestRow()
        {
        }

        // Token: 0x060001AD RID: 429 RVA: 0x00003C30 File Offset: 0x00001E30
        public RequestRow(DataRow requestInfo)
        {
            if (requestInfo == null)
            {
                return;
            }
            this.RequestsId = CastInt(requestInfo["RequestsID"]);
            this.EmpNo = base.CastDouble(requestInfo["EMP_NO"]);
            this.SubmittedDate = base.CastDateTimeNullable(requestInfo["SubmittedDate"]);
            this.Type = base.CastShort(requestInfo["TYPE"]);
            this.StartDate = Convert.ToDateTime(requestInfo["StartDate"]);
            this.EndDate = base.CastDateTimeNullable(requestInfo["EndDate"]);
            this.StartHour = base.CastInt(requestInfo["StartHour"]);
            this.EndHour = base.CastInt(requestInfo["EndHour"]);
            this.Duration = Convert.ToString(requestInfo["Duration"]);
            this.SubmittedByEmployeeId = base.CastDouble(requestInfo["SubmittedByEmployeeID"]);
            this.IsFinalApproved = base.CastBoolNullable(requestInfo["IsFinalApproved"]);
            this.ApprovalByManagerEmpNo = base.CastDouble(requestInfo["ApprovalByManagerEmp_No"]);
            this.ApprovalDate = base.CastDateTimeNullable(requestInfo["ApprovalDate"]);
            this.OperationsId = base.CastInt(requestInfo["OperationsID"]);
            this.Description = Convert.ToString(requestInfo["DESCRIPTION"]);
            this.CurSection = base.CastInt(requestInfo["CurSection"]);
            this.CurEmpNo = base.CastDouble(requestInfo["CurEmp_NO"]);
            this.AcceptCode = base.CastInt(requestInfo["AcceptCode"]);
            this.IoStatus = (IoStatusEnum)base.CastInt(requestInfo["IOStatus"]);
            this.Status = (StatusEnum)base.CastInt(requestInfo["RequestStatus"]);
            this.TransferCost = base.CastDouble(requestInfo["TransferCost"]);
            try
            {
                this.DeviceBackStr = requestInfo["DeviceBackStr"].ToString();
            }
            catch
            {
                this.DeviceBackStr = string.Empty;
            }
            try
            {
                this.DeviceWentStr = requestInfo["DeviceWentStr"].ToString();
            }
            catch
            {
                this.DeviceWentStr = string.Empty;
            }
            try
            {
                this.Section = Convert.ToString(requestInfo["Section"]);
            }
            catch
            {
                this.Section = string.Empty;
            }
            try
            {
                this.ManagerName = Convert.ToString(requestInfo["ManagerName"]);
            }
            catch
            {
                this.ManagerName = string.Empty;
            }
            try
            {
                this.ApprovalManager = Convert.ToString(requestInfo["ApprovalManager"]);
            }
            catch
            {
                this.ApprovalManager = string.Empty;
            }
            try
            {
                this.Postion = Convert.ToString(requestInfo["postion"]);
            }
            catch
            {
                this.Postion = string.Empty;
            }
            try
            {
                this.Source = requestInfo["Source"].ToString();
            }
            catch
            {
                this.Source = string.Empty;
            }
            try
            {
                this.Distination = requestInfo["Distination"].ToString();
            }
            catch
            {
                this.Distination = string.Empty;
            }
            try
            {
                this.DeviceBack = base.CastShort(requestInfo["DeviceBack"]);
            }
            catch
            {
            }
            try
            {
                this.LocationStatus = base.CastBool(requestInfo["LocationStatus"]);
            }
            catch
            {
                this.LocationStatus = false;
            }
            try
            {
                this.DeviceWent = base.CastShort(requestInfo["DeviceWent"]);
            }
            catch
            {
            }
            try
            {
                this.MoveUpCnt = base.CastInt(requestInfo["MoveUpCnt"]);
            }
            catch
            {
            }
            try
            {
                this.IsDay = base.CastBool(requestInfo["IS_DAY"]);
            }
            catch
            {
                this.IsDay = false;
            }
            try
            {
                this.CardType = base.CastShort(requestInfo["CARD_TYPE"]);
            }
            catch
            {
                this.CardType = 0;
            }
            try
            {
                this.FullName = requestInfo["FullName"].ToString();
            }
            catch
            {
                this.FullName = string.Empty;
            }
            try
            {
                this.Title = requestInfo["TITLE"].ToString();
            }
            catch
            {
                this.Title = string.Empty;
            }
            this.IsFirstTimeShift = Convert.ToBoolean(requestInfo["IsFirstTimeShift"]);
            try
            {
                this.ParalelIsApprove = base.CastBool(requestInfo["IsApprove"]);
            }
            catch
            {
                this.ParalelIsApprove = false;
            }
            try
            {
                this.ParalelApproveDate = new DateTime?(Convert.ToDateTime(requestInfo["ChangeDate"]));
            }
            catch
            {
                this.ParalelApproveDate = null;
            }
            try
            {
                this.ParallelApprovalEmpNo = base.CastDouble(requestInfo["ParallelApprovalEmpNo"]);
            }
            catch
            {
                this.ParallelApprovalEmpNo = 0.0;
            }
            try
            {
                this.ParalelFullName = requestInfo["ParalelFullName"].ToString();
            }
            catch
            {
                this.ParalelFullName = string.Empty;
            }
        }
        // Token: 0x170000CA RID: 202
        // (get) Token: 0x060001AE RID: 430 RVA: 0x00004220 File Offset: 0x00002420
        // (set) Token: 0x060001AF RID: 431 RVA: 0x00004228 File Offset: 0x00002428
        public short CardType
        {
            get;
            set;
        }

        // Token: 0x170000CB RID: 203
        // (get) Token: 0x060001B0 RID: 432 RVA: 0x00004231 File Offset: 0x00002431
        // (set) Token: 0x060001B1 RID: 433 RVA: 0x00004239 File Offset: 0x00002439
        public string Postion
        {
            get;
            set;
        }

        // Token: 0x170000CC RID: 204
        // (get) Token: 0x060001B2 RID: 434 RVA: 0x00004242 File Offset: 0x00002442
        // (set) Token: 0x060001B3 RID: 435 RVA: 0x0000424A File Offset: 0x0000244A
        public int RequestsId
        {
            get;
            set;
        }

        // Token: 0x170000CD RID: 205
        // (get) Token: 0x060001B4 RID: 436 RVA: 0x00004253 File Offset: 0x00002453
        // (set) Token: 0x060001B5 RID: 437 RVA: 0x0000425B File Offset: 0x0000245B
        public double EmpNo
        {
            get;
            set;
        }

        // Token: 0x170000CE RID: 206
        // (get) Token: 0x060001B6 RID: 438 RVA: 0x00004264 File Offset: 0x00002464
        // (set) Token: 0x060001B7 RID: 439 RVA: 0x0000426C File Offset: 0x0000246C
        public DateTime? SubmittedDate
        {
            get;
            set;
        }

        // Token: 0x170000CF RID: 207
        // (get) Token: 0x060001B8 RID: 440 RVA: 0x00004275 File Offset: 0x00002475
        // (set) Token: 0x060001B9 RID: 441 RVA: 0x0000427D File Offset: 0x0000247D
        public short Type
        {
            get;
            set;
        }

        // Token: 0x170000D0 RID: 208
        // (get) Token: 0x060001BA RID: 442 RVA: 0x00004286 File Offset: 0x00002486
        // (set) Token: 0x060001BB RID: 443 RVA: 0x0000428E File Offset: 0x0000248E
        public DateTime StartDate
        {
            get;
            set;
        }

        // Token: 0x170000D1 RID: 209
        // (get) Token: 0x060001BC RID: 444 RVA: 0x00004297 File Offset: 0x00002497
        // (set) Token: 0x060001BD RID: 445 RVA: 0x0000429F File Offset: 0x0000249F
        public DateTime? EndDate
        {
            get;
            set;
        }

        // Token: 0x170000D2 RID: 210
        // (get) Token: 0x060001BE RID: 446 RVA: 0x000042A8 File Offset: 0x000024A8
        // (set) Token: 0x060001BF RID: 447 RVA: 0x000042B0 File Offset: 0x000024B0
        public int StartHour
        {
            get;
            set;
        }

        // Token: 0x170000D3 RID: 211
        // (get) Token: 0x060001C0 RID: 448 RVA: 0x000042B9 File Offset: 0x000024B9
        // (set) Token: 0x060001C1 RID: 449 RVA: 0x000042C1 File Offset: 0x000024C1
        public int EndHour
        {
            get;
            set;
        }

        // Token: 0x170000D4 RID: 212
        // (get) Token: 0x060001C2 RID: 450 RVA: 0x000042CA File Offset: 0x000024CA
        // (set) Token: 0x060001C3 RID: 451 RVA: 0x000042D2 File Offset: 0x000024D2
        public string Duration
        {
            get;
            set;
        }

        // Token: 0x170000D5 RID: 213
        // (get) Token: 0x060001C4 RID: 452 RVA: 0x000042DB File Offset: 0x000024DB
        // (set) Token: 0x060001C5 RID: 453 RVA: 0x000042E3 File Offset: 0x000024E3
        public double SubmittedByEmployeeId
        {
            get;
            set;
        }

        // Token: 0x170000D6 RID: 214
        // (get) Token: 0x060001C6 RID: 454 RVA: 0x000042EC File Offset: 0x000024EC
        // (set) Token: 0x060001C7 RID: 455 RVA: 0x000042F4 File Offset: 0x000024F4
        public bool? IsFinalApproved
        {
            get;
            set;
        }

        // Token: 0x170000D7 RID: 215
        // (get) Token: 0x060001C8 RID: 456 RVA: 0x000042FD File Offset: 0x000024FD
        // (set) Token: 0x060001C9 RID: 457 RVA: 0x00004305 File Offset: 0x00002505
        public double ApprovalByManagerEmpNo
        {
            get;
            set;
        }

        // Token: 0x170000D8 RID: 216
        // (get) Token: 0x060001CA RID: 458 RVA: 0x0000430E File Offset: 0x0000250E
        // (set) Token: 0x060001CB RID: 459 RVA: 0x00004316 File Offset: 0x00002516
        public DateTime? ApprovalDate
        {
            get;
            set;
        }

        // Token: 0x170000D9 RID: 217
        // (get) Token: 0x060001CC RID: 460 RVA: 0x0000431F File Offset: 0x0000251F
        // (set) Token: 0x060001CD RID: 461 RVA: 0x00004327 File Offset: 0x00002527
        public int OperationsId
        {
            get;
            set;
        }

        // Token: 0x170000DA RID: 218
        // (get) Token: 0x060001CE RID: 462 RVA: 0x00004330 File Offset: 0x00002530
        // (set) Token: 0x060001CF RID: 463 RVA: 0x00004338 File Offset: 0x00002538
        public string Description
        {
            get;
            set;
        }

        // Token: 0x170000DB RID: 219
        // (get) Token: 0x060001D0 RID: 464 RVA: 0x00004341 File Offset: 0x00002541
        // (set) Token: 0x060001D1 RID: 465 RVA: 0x00004349 File Offset: 0x00002549
        public int CurSection
        {
            get;
            set;
        }

        // Token: 0x170000DC RID: 220
        // (get) Token: 0x060001D2 RID: 466 RVA: 0x00004352 File Offset: 0x00002552
        // (set) Token: 0x060001D3 RID: 467 RVA: 0x0000435A File Offset: 0x0000255A
        public IoStatusEnum IoStatus
        {
            get;
            set;
        }

        // Token: 0x170000DD RID: 221
        // (get) Token: 0x060001D4 RID: 468 RVA: 0x00004363 File Offset: 0x00002563
        // (set) Token: 0x060001D5 RID: 469 RVA: 0x0000436B File Offset: 0x0000256B
        public StatusEnum Status
        {
            get;
            set;
        }

        // Token: 0x170000DE RID: 222
        // (get) Token: 0x060001D6 RID: 470 RVA: 0x00004374 File Offset: 0x00002574
        // (set) Token: 0x060001D7 RID: 471 RVA: 0x0000437C File Offset: 0x0000257C
        public string Title
        {
            get;
            set;
        }

        // Token: 0x170000DF RID: 223
        // (get) Token: 0x060001D8 RID: 472 RVA: 0x00004385 File Offset: 0x00002585
        // (set) Token: 0x060001D9 RID: 473 RVA: 0x0000438D File Offset: 0x0000258D
        public short DeviceWent
        {
            get;
            set;
        }

        // Token: 0x170000E0 RID: 224
        // (get) Token: 0x060001DA RID: 474 RVA: 0x00004396 File Offset: 0x00002596
        // (set) Token: 0x060001DB RID: 475 RVA: 0x0000439E File Offset: 0x0000259E
        public string DeviceWentStr
        {
            get;
            set;
        }

        // Token: 0x170000E1 RID: 225
        // (get) Token: 0x060001DC RID: 476 RVA: 0x000043A7 File Offset: 0x000025A7
        // (set) Token: 0x060001DD RID: 477 RVA: 0x000043AF File Offset: 0x000025AF
        public bool LocationStatus
        {
            get;
            set;
        }

        // Token: 0x170000E2 RID: 226
        // (get) Token: 0x060001DE RID: 478 RVA: 0x000043B8 File Offset: 0x000025B8
        // (set) Token: 0x060001DF RID: 479 RVA: 0x000043C0 File Offset: 0x000025C0
        public short DeviceBack
        {
            get;
            set;
        }

        // Token: 0x170000E3 RID: 227
        // (get) Token: 0x060001E0 RID: 480 RVA: 0x000043C9 File Offset: 0x000025C9
        // (set) Token: 0x060001E1 RID: 481 RVA: 0x000043D1 File Offset: 0x000025D1
        public string DeviceBackStr
        {
            get;
            set;
        }

        // Token: 0x170000E4 RID: 228
        // (get) Token: 0x060001E2 RID: 482 RVA: 0x000043DA File Offset: 0x000025DA
        // (set) Token: 0x060001E3 RID: 483 RVA: 0x000043E2 File Offset: 0x000025E2
        public string Distination
        {
            get;
            set;
        }

        // Token: 0x170000E5 RID: 229
        // (get) Token: 0x060001E4 RID: 484 RVA: 0x000043EB File Offset: 0x000025EB
        // (set) Token: 0x060001E5 RID: 485 RVA: 0x000043F3 File Offset: 0x000025F3
        public string Source
        {
            get;
            set;
        }

        // Token: 0x170000E6 RID: 230
        // (get) Token: 0x060001E6 RID: 486 RVA: 0x000043FC File Offset: 0x000025FC
        // (set) Token: 0x060001E7 RID: 487 RVA: 0x00004404 File Offset: 0x00002604
        public string FullName
        {
            get;
            set;
        }

        // Token: 0x170000E7 RID: 231
        // (get) Token: 0x060001E8 RID: 488 RVA: 0x0000440D File Offset: 0x0000260D
        // (set) Token: 0x060001E9 RID: 489 RVA: 0x00004415 File Offset: 0x00002615
        public bool IsDay
        {
            get;
            set;
        }

        // Token: 0x170000E8 RID: 232
        // (get) Token: 0x060001EA RID: 490 RVA: 0x0000441E File Offset: 0x0000261E
        // (set) Token: 0x060001EB RID: 491 RVA: 0x00004426 File Offset: 0x00002626
        public double CurEmpNo
        {
            get;
            set;
        }

        // Token: 0x170000E9 RID: 233
        // (get) Token: 0x060001EC RID: 492 RVA: 0x0000442F File Offset: 0x0000262F
        // (set) Token: 0x060001ED RID: 493 RVA: 0x00004437 File Offset: 0x00002637
        public int AcceptCode
        {
            get;
            set;
        }

        // Token: 0x170000EA RID: 234
        // (get) Token: 0x060001EE RID: 494 RVA: 0x00004440 File Offset: 0x00002640
        // (set) Token: 0x060001EF RID: 495 RVA: 0x00004448 File Offset: 0x00002648
        public int MoveUpCnt
        {
            get;
            set;
        }

        // Token: 0x170000EB RID: 235
        // (get) Token: 0x060001F0 RID: 496 RVA: 0x00004451 File Offset: 0x00002651
        // (set) Token: 0x060001F1 RID: 497 RVA: 0x00004459 File Offset: 0x00002659
        public string ManagerName
        {
            get;
            set;
        }

        // Token: 0x170000EC RID: 236
        // (get) Token: 0x060001F2 RID: 498 RVA: 0x00004462 File Offset: 0x00002662
        // (set) Token: 0x060001F3 RID: 499 RVA: 0x0000446A File Offset: 0x0000266A
        public string Section
        {
            get;
            set;
        }

        // Token: 0x170000ED RID: 237
        // (get) Token: 0x060001F4 RID: 500 RVA: 0x00004473 File Offset: 0x00002673
        // (set) Token: 0x060001F5 RID: 501 RVA: 0x0000447B File Offset: 0x0000267B
        public string ApprovalManager
        {
            get;
            set;
        }

        // Token: 0x170000EE RID: 238
        // (get) Token: 0x060001F6 RID: 502 RVA: 0x00004484 File Offset: 0x00002684
        // (set) Token: 0x060001F7 RID: 503 RVA: 0x0000448C File Offset: 0x0000268C
        public double TransferCost
        {
            get;
            set;
        }

        // Token: 0x170000EF RID: 239
        // (get) Token: 0x060001F8 RID: 504 RVA: 0x00004495 File Offset: 0x00002695
        // (set) Token: 0x060001F9 RID: 505 RVA: 0x0000449D File Offset: 0x0000269D
        public bool IsFirstTimeShift
        {
            get;
            set;
        }

        // Token: 0x170000F0 RID: 240
        // (get) Token: 0x060001FA RID: 506 RVA: 0x000044A6 File Offset: 0x000026A6
        // (set) Token: 0x060001FB RID: 507 RVA: 0x000044AE File Offset: 0x000026AE
        public DateTime? ParalelApproveDate
        {
            get;
            set;
        }

        // Token: 0x170000F1 RID: 241
        // (get) Token: 0x060001FC RID: 508 RVA: 0x000044B7 File Offset: 0x000026B7
        // (set) Token: 0x060001FD RID: 509 RVA: 0x000044BF File Offset: 0x000026BF
        public string ParalelFullName
        {
            get;
            set;
        }

        // Token: 0x170000F2 RID: 242
        // (get) Token: 0x060001FE RID: 510 RVA: 0x000044C8 File Offset: 0x000026C8
        // (set) Token: 0x060001FF RID: 511 RVA: 0x000044D0 File Offset: 0x000026D0
        public bool ParalelIsApprove
        {
            get;
            set;
        }

        // Token: 0x170000F3 RID: 243
        // (get) Token: 0x06000200 RID: 512 RVA: 0x000044D9 File Offset: 0x000026D9
        // (set) Token: 0x06000201 RID: 513 RVA: 0x000044E1 File Offset: 0x000026E1
        public double ParallelApprovalEmpNo
        {
            get;
            set;
        }
    }
    // Token: 0x02000002 RID: 2
    public enum AcceptCodeEnum
    {
        // Token: 0x04000002 RID: 2
        Accept,
        // Token: 0x04000003 RID: 3
        RegisterTolerance,
        // Token: 0x04000004 RID: 4
        MultiDateFile = 7,
        // Token: 0x04000005 RID: 5
        NoDateFile,
        // Token: 0x04000006 RID: 6
        Other,
        // Token: 0x04000007 RID: 7
        ParallelApproval = 19,
        // Token: 0x04000008 RID: 8
        OnRevoking = 21,
        // Token: 0x04000009 RID: 9
        Revoke,
        // Token: 0x0400000A RID: 10
        RejectRevoke
    }
    // Token: 0x02000016 RID: 22
    public enum LeaveType
    {
        // Token: 0x040000D3 RID: 211
        Leave = 5,
        // Token: 0x040000D4 RID: 212
        Duty = 3,
        // Token: 0x040000D5 RID: 213
        OverTime = 1,
        // Token: 0x040000D6 RID: 214
        InOut,
        // Token: 0x040000D7 RID: 215
        DutyNoReturn = 4
    }
    /// <summary>
    /// اطلاعات ایکس ام ال که منجر به مشکل می شود به صورت ثابت اورده شده تا در ایکس ام ال وارده از موبایل حذف گردد
    /// </summary>
    public class XmlStaticTexts
    {
        /// <summary>
        /// متن شروع ایکس ام ال
        /// </summary>
        public static readonly string BeginningCdata = "<![CDATA";
        /// <summary>
        /// متن پایان ایکس ام ال
        /// </summary>
        public static readonly string EndCdata = "]]";
    }
    /// <summary>
    /// مدل درخواست های تاییید یا عدم تایید شده در کارتابل
    /// </summary>
    public class Tb
    {
        /// <summary>
        /// شماره سطر
        /// </summary>
        public string RowNo { get; set; }
        /// <summary>
        /// شناسه سند
        /// </summary>
        public string DocID { get; set; }
        /// <summary>
        /// نوع سند
        /// </summary>
        public string DocTypeID { get; set; }
        /// <summary>
        /// شرح کاربر
        /// </summary>
        public string ActorDesc { get; set; }
        /// <summary>
        /// شماره صفحه
        /// </summary>
        public string PageNumber { get; set; }
    }
    /// <summary>
    /// عملیات انجام شده در کارتابل
    /// </summary>
    public class ActionTb
    {
        /// <summary>
        /// عملیات
        /// </summary>
        public string Action { get; set; }
        /// <summary>
        /// نوع عملیات غیر قابل استفاده در لئوپارد
        /// </summary>
        public string R_P_Type { get; set; }
    }
    /// <summary>
    /// مدل اصلی جی سان شده در کارتابل
    /// </summary>
    public class RootObject
    {
        /// <summary>
        /// لیست درخواست ها
        /// </summary>
        public List<Tb> Tb { get; set; }
        /// <summary>
        /// عملیات
        /// </summary>
        public ActionTb ActionTb { get; set; }
    }
    /// <summary>
    /// مدل اصلی جی سان شده در کارتابل
    /// </summary>
    public class RootObjectSingleRow
    {
        /// <summary>
        /// لیست درخواست ها
        /// </summary>
        public Tb Tb { get; set; }
        /// <summary>
        /// عملیات
        /// </summary>
        public ActionTb ActionTb { get; set; }
    }

    public class SelectedDateViewModel
    {
        public int Y { get; set; }
        public int M { get; set; }
        public int D { get; set; }
    }
}
