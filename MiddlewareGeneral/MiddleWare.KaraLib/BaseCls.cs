﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiddleWare.KaraLib
{
   public class BaseCls
    {
        // Token: 0x0600001A RID: 26 RVA: 0x00002124 File Offset: 0x00000324
        public DateTime? CastDateTimeNullable(object argDateTime)
        {
            if (argDateTime is DBNull || argDateTime.ToString() == "")
            {
                return null;
            }
            return new DateTime?(Convert.ToDateTime(argDateTime));
        }

        // Token: 0x0600001B RID: 27 RVA: 0x00002160 File Offset: 0x00000360
        public bool? CastBoolNullable(object argBool)
        {
            if (argBool is DBNull || argBool.ToString() == "")
            {
                return null;
            }
            return new bool?(Convert.ToBoolean(argBool));
        }

        // Token: 0x0600001C RID: 28 RVA: 0x0000219C File Offset: 0x0000039C
        public bool CastBool(object argBool)
        {
            return !(argBool is DBNull) && !(argBool.ToString() == "") && Convert.ToBoolean(argBool);
        }

        // Token: 0x0600001D RID: 29 RVA: 0x000021C0 File Offset: 0x000003C0
        public int CastInt(object arg)
        {
            if (arg is DBNull || arg.ToString() == "")
            {
                return 0;
            }
            return Convert.ToInt32(arg);
        }

        // Token: 0x0600001E RID: 30 RVA: 0x000021E4 File Offset: 0x000003E4
        public short CastShort(object arg)
        {
            if (arg is DBNull || arg.ToString() == "")
            {
                return 0;
            }
            return Convert.ToInt16(arg);
        }

        // Token: 0x0600001F RID: 31 RVA: 0x00002208 File Offset: 0x00000408
        public double CastDouble(object arg)
        {
            if (arg is DBNull || arg.ToString() == "")
            {
                return 0.0;
            }
            return Convert.ToDouble(arg);
        }
    }
}
