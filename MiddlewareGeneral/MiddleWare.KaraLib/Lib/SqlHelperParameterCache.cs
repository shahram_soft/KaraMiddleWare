﻿using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;

namespace Microsoft.ApplicationBlocks.Data
{
	// Token: 0x02000003 RID: 3
	public sealed class SqlHelperParameterCache
	{
		// Token: 0x0600004D RID: 77 RVA: 0x00002050 File Offset: 0x00000250
		private SqlHelperParameterCache()
		{
		}

		// Token: 0x0600004E RID: 78 RVA: 0x00003950 File Offset: 0x00001B50
		private static SqlParameter[] DiscoverSpParameterSet(SqlConnection connection, string spName, bool includeReturnValueParameter)
		{
			if (connection == null)
			{
				throw new ArgumentNullException("connection");
			}
			if (spName == null || spName.Length == 0)
			{
				throw new ArgumentNullException("spName");
			}
			SqlCommand sqlCommand = new SqlCommand(spName, connection);
			sqlCommand.CommandType = CommandType.StoredProcedure;
			connection.Open();
			SqlCommandBuilder.DeriveParameters(sqlCommand);
			connection.Close();
			if (!includeReturnValueParameter)
			{
				sqlCommand.Parameters.RemoveAt(0);
			}
			SqlParameter[] array = new SqlParameter[sqlCommand.Parameters.Count];
			sqlCommand.Parameters.CopyTo(array, 0);
			SqlParameter[] array2 = array;
			for (int i = 0; i < array2.Length; i++)
			{
				array2[i].Value = DBNull.Value;
			}
			return array;
		}

		// Token: 0x0600004F RID: 79 RVA: 0x000039F0 File Offset: 0x00001BF0
		private static SqlParameter[] CloneParameters(SqlParameter[] originalParameters)
		{
			SqlParameter[] array = new SqlParameter[originalParameters.Length];
			int i = 0;
			int num = originalParameters.Length;
			while (i < num)
			{
				array[i] = (SqlParameter)((ICloneable)originalParameters[i]).Clone();
				i++;
			}
			return array;
		}

		// Token: 0x06000050 RID: 80 RVA: 0x00003A28 File Offset: 0x00001C28
		public static void CacheParameterSet(string connectionString, string commandText, params SqlParameter[] commandParameters)
		{
			if (connectionString == null || connectionString.Length == 0)
			{
				throw new ArgumentNullException("connectionString");
			}
			if (commandText == null || commandText.Length == 0)
			{
				throw new ArgumentNullException("commandText");
			}
			string key = connectionString + ":" + commandText;
			SqlHelperParameterCache.paramCache[key] = commandParameters;
		}

		// Token: 0x06000051 RID: 81 RVA: 0x00003A7C File Offset: 0x00001C7C
		public static SqlParameter[] GetCachedParameterSet(string connectionString, string commandText)
		{
			if (connectionString == null || connectionString.Length == 0)
			{
				throw new ArgumentNullException("connectionString");
			}
			if (commandText == null || commandText.Length == 0)
			{
				throw new ArgumentNullException("commandText");
			}
			string key = connectionString + ":" + commandText;
			SqlParameter[] array = SqlHelperParameterCache.paramCache[key] as SqlParameter[];
			if (array == null)
			{
				return null;
			}
			return SqlHelperParameterCache.CloneParameters(array);
		}

		// Token: 0x06000052 RID: 82 RVA: 0x00003ADE File Offset: 0x00001CDE
		public static SqlParameter[] GetSpParameterSet(string connectionString, string spName)
		{
			return SqlHelperParameterCache.GetSpParameterSet(connectionString, spName, false);
		}

		// Token: 0x06000053 RID: 83 RVA: 0x00003AE8 File Offset: 0x00001CE8
		public static SqlParameter[] GetSpParameterSet(string connectionString, string spName, bool includeReturnValueParameter)
		{
			if (connectionString == null || connectionString.Length == 0)
			{
				throw new ArgumentNullException("connectionString");
			}
			if (spName == null || spName.Length == 0)
			{
				throw new ArgumentNullException("spName");
			}
			SqlParameter[] spParameterSetInternal;
			using (SqlConnection sqlConnection = new SqlConnection(connectionString))
			{
				spParameterSetInternal = SqlHelperParameterCache.GetSpParameterSetInternal(sqlConnection, spName, includeReturnValueParameter);
			}
			return spParameterSetInternal;
		}

		// Token: 0x06000054 RID: 84 RVA: 0x00003B50 File Offset: 0x00001D50
		internal static SqlParameter[] GetSpParameterSet(SqlConnection connection, string spName)
		{
			return SqlHelperParameterCache.GetSpParameterSet(connection, spName, false);
		}

		// Token: 0x06000055 RID: 85 RVA: 0x00003B5C File Offset: 0x00001D5C
		internal static SqlParameter[] GetSpParameterSet(SqlConnection connection, string spName, bool includeReturnValueParameter)
		{
			if (connection == null)
			{
				throw new ArgumentNullException("connection");
			}
			SqlParameter[] spParameterSetInternal;
			using (SqlConnection sqlConnection = (SqlConnection)((ICloneable)connection).Clone())
			{
				spParameterSetInternal = SqlHelperParameterCache.GetSpParameterSetInternal(sqlConnection, spName, includeReturnValueParameter);
			}
			return spParameterSetInternal;
		}

		// Token: 0x06000056 RID: 86 RVA: 0x00003BAC File Offset: 0x00001DAC
		private static SqlParameter[] GetSpParameterSetInternal(SqlConnection connection, string spName, bool includeReturnValueParameter)
		{
			if (connection == null)
			{
				throw new ArgumentNullException("connection");
			}
			if (spName == null || spName.Length == 0)
			{
				throw new ArgumentNullException("spName");
			}
			string key = connection.ConnectionString + ":" + spName + (includeReturnValueParameter ? ":include ReturnValue Parameter" : "");
			SqlParameter[] array = SqlHelperParameterCache.paramCache[key] as SqlParameter[];
			if (array == null)
			{
				SqlParameter[] array2 = SqlHelperParameterCache.DiscoverSpParameterSet(connection, spName, includeReturnValueParameter);
				SqlHelperParameterCache.paramCache[key] = array2;
				array = array2;
			}
			return SqlHelperParameterCache.CloneParameters(array);
		}

		// Token: 0x04000001 RID: 1
		private static Hashtable paramCache = Hashtable.Synchronized(new Hashtable());
	}
}
