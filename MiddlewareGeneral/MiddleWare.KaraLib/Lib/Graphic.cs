﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace MiddleWare.KaraLib.Lib
{
   public class Graphic
    {
        // Token: 0x06000063 RID: 99 RVA: 0x00003230 File Offset: 0x00001430
        public static Size ConstraintPictureSize(Size PicSize, Size ConstraintSize)
        {
            int num = PicSize.Width;
            int num2 = PicSize.Height;
            if (num > ConstraintSize.Width)
            {
                num2 = ConstraintSize.Width * num2 / num;
                num = ConstraintSize.Width;
            }
            if (num2 > ConstraintSize.Height)
            {
                num = ConstraintSize.Height * num / num2;
                num2 = ConstraintSize.Height;
            }
            return new Size(num, num2);
        }

        // Token: 0x06000064 RID: 100 RVA: 0x00003290 File Offset: 0x00001490
        public static string ConvertPictureToJpeg(string FileName)
        {
            Path.GetExtension(FileName).ToUpper().Trim();
            string str = Path.GetFileNameWithoutExtension(FileName).Trim().ToUpper();
            string expr_3C = Path.GetDirectoryName(FileName).Trim().ToUpper() + "\\";
            string text = expr_3C + "TemporaryPicture.JPG";
            string text2 = expr_3C + str + ".JPG";
            Image expr_5A = Image.FromFile(FileName);
            expr_5A.Save(text, ImageFormat.Jpeg);
            expr_5A.Dispose();
            File.Delete(FileName);
            Image expr_77 = Image.FromFile(text);
            expr_77.Save(text2, ImageFormat.Jpeg);
            expr_77.Dispose();
            File.Delete(text);
            return text2;
        }

        // Token: 0x06000065 RID: 101 RVA: 0x0000332C File Offset: 0x0000152C
        public static string MakeThumbnailFromPicture(string fileName, Size ConstraintSize)
        {
            return Graphic.MakeThumbnailFromPicture(fileName, "", ConstraintSize.Width, ConstraintSize.Height);
        }

        // Token: 0x06000066 RID: 102 RVA: 0x00003347 File Offset: 0x00001547
        public static string MakeThumbnailFromPicture(string fileName, int ConstraintWidth, int ConstraintHeight)
        {
            return Graphic.MakeThumbnailFromPicture(fileName, "", ConstraintWidth, ConstraintHeight);
        }

        // Token: 0x06000067 RID: 103 RVA: 0x00003356 File Offset: 0x00001556
        public static string MakeThumbnailFromPicture(string fileName, string thumbnailName, Size ConstraintSize)
        {
            return Graphic.MakeThumbnailFromPicture(fileName, thumbnailName, ConstraintSize.Width, ConstraintSize.Height);
        }

        // Token: 0x06000068 RID: 104 RVA: 0x00003370 File Offset: 0x00001570
        public static string MakeThumbnailFromPicture(string fileName, string thumbnailName, int constraintWidth, int constraintHeight)
        {
            if (fileName.Trim() == "" || !File.Exists(fileName.Trim()))
            {
                throw new Exception("فايل مورد نظر جهت ساختن تصوير کوچک معتبر نيست");
            }
            string text;
            if (thumbnailName.Trim() == "")
            {
                text = Graphic.GetThumbnailFilePath(fileName);
            }
            else
            {
                if (Path.GetExtension(thumbnailName).Trim().ToUpper() != ".JPG")
                {
                    throw new Exception("خروجي اين تابع فقط فايل از نوع jpg ميباشد ، لطفا نام خروجي را تغيير دهيد");
                }
                text = thumbnailName;
            }
            Image.GetThumbnailImageAbort callback = new Image.GetThumbnailImageAbort(Graphic.ThumbnailCallback);
            string result;
            using (Image image = Image.FromFile(fileName))
            {
                Size size = Graphic.ConstraintPictureSize(image.Size, new Size(constraintWidth, constraintHeight));
                using (Image thumbnailImage = image.GetThumbnailImage(size.Width, size.Height, callback, IntPtr.Zero))
                {
                    thumbnailImage.Save(text, image.RawFormat);
                    result = text;
                }
            }
            return result;
        }

        // Token: 0x06000069 RID: 105 RVA: 0x0000347C File Offset: 0x0000167C
        public static string GetThumbnailFilePath(string FileName)
        {
            string str = Path.GetFileNameWithoutExtension(FileName).Trim();
            string str2 = Path.GetExtension(FileName).Trim();
            return Path.GetDirectoryName(FileName) + Path.DirectorySeparatorChar.ToString() + str + "_Thu" + str2;
        }

        // Token: 0x0600006A RID: 106 RVA: 0x000034C5 File Offset: 0x000016C5
        public static Image MakeThumbnailFromPicture(Image pic)
        {
            return Graphic.MakeThumbnailFromPicture(pic, 160, 120);
        }

        // Token: 0x0600006B RID: 107 RVA: 0x000034D4 File Offset: 0x000016D4
        public static Image MakeThumbnailFromPicture(Image pic, int thumbWidth, int thumbHeight)
        {
            Image.GetThumbnailImageAbort callback = new Image.GetThumbnailImageAbort(Graphic.ThumbnailCallback);
            return pic.GetThumbnailImage(thumbWidth, thumbHeight, callback, IntPtr.Zero);
        }

        // Token: 0x0600006C RID: 108 RVA: 0x000034FC File Offset: 0x000016FC
        public static bool ThumbnailCallback()
        {
            return false;
        }

        // Token: 0x0600006D RID: 109 RVA: 0x00003500 File Offset: 0x00001700
        public static Image CreateThumbnail(int maxWidth, int maxHeight, string path)
        {
            Image image = Image.FromFile(path);
            double arg_1D_0 = (double)maxWidth / (double)image.Width;
            double val = (double)maxHeight / (double)image.Height;
            double num = Math.Min(arg_1D_0, val);
            int width = (int)((double)image.Width * num);
            int height = (int)((double)image.Height * num);
            Bitmap expr_42 = new Bitmap(width, height);
            Graphics.FromImage(expr_42).DrawImage(image, 0, 0, width, height);
            image.Dispose();
            return expr_42;
        }
    }
}
