﻿using System;
using System.Data;

namespace PW.WorkFlows.SqlDal
{
	// Token: 0x0200000F RID: 15
	public class Conferment : BaseDAL
	{
		// Token: 0x060000D1 RID: 209 RVA: 0x00003C40 File Offset: 0x00001E40
		public Conferment(WorkFlowsDatabase owner) : base(owner)
		{
		}

		// Token: 0x060000D2 RID: 210 RVA: 0x00005CD4 File Offset: 0x00003ED4
		public DataTable GetConferment(double empManager)
		{
			DataTable result;
			try
			{
				string commandText = "SELECT c.EmpManager, c.EMP_NO, c.SEC_NO, c.ActionID, c.PermitValue,ISNULL(s.TITLE,'حذف شده') TITLE,c.IsDelete , \ndbo.ShamsyDate(c.StartDate) StartDate,dbo.ShamsyDate(c.EndDate) EndDate,e.[NAME],e.FAMILY,a.Fdesc  \n FROM Conferment c  \nJOIN EMPLOYEE e ON e.EMP_NO = c.EMP_NO  \nLEFT OUTER JOIN SECTIONS s ON  s.SEC_NO = c.SEC_NO \nJOIN [Action] a  ON a.ActionID=c.ActionID  WHERE c.EmpManager=" + empManager;
				result = base.ExecuteDataTable(CommandType.Text, commandText);
			}
			catch
			{
				result = null;
			}
			return result;
		}

		// Token: 0x060000D3 RID: 211 RVA: 0x00005D14 File Offset: 0x00003F14
		public DataTable GetConferment()
		{
			DataTable result;
			try
			{
				result = base.ExecuteDataTable(CommandType.Text, "SELECT c.EmpManager, c.EMP_NO, c.SEC_NO, c.ActionID, c.PermitValue,ISNULL(s.TITLE,'حذف شده') TITLE,c.IsDelete , \ndbo.ShamsyDate(c.StartDate) StartDate,dbo.ShamsyDate(c.EndDate) EndDate,e.[NAME],e.FAMILY,a.Fdesc  \n FROM Conferment c  \nJOIN EMPLOYEE e ON e.EMP_NO = c.EMP_NO  \nLEFT OUTER JOIN SECTIONS s ON  s.SEC_NO = c.SEC_NO \nJOIN [Action] a  ON a.ActionID=c.ActionID ");
			}
			catch
			{
				result = null;
			}
			return result;
		}

		// Token: 0x060000D4 RID: 212 RVA: 0x00005D48 File Offset: 0x00003F48
		public DataRow GetConferment(double empManager, int actionId, int secNo)
		{
			DataRow result;
			try
			{
				string commandText = string.Concat(new object[]
				{
					"SELECT * FROM Conferment c WHERE GETDATE() BETWEEN c.StartDate AND c.EndDate AND c.EmpManager=",
					empManager,
					" AND c.ActionID=",
					actionId,
					" AND c.IsDelete=0 AND c.SEC_NO=",
					secNo
				});
				result = base.ExecuteDataRow(CommandType.Text, commandText);
			}
			catch
			{
				result = null;
			}
			return result;
		}

		// Token: 0x060000D5 RID: 213 RVA: 0x00005DB8 File Offset: 0x00003FB8
		public bool Insert(double empManager, double empNo, int secNo, int actionId, int permitValue, DateTime startDate, DateTime endDate)
		{
			bool result;
			try
			{
				string commandText = string.Concat(new object[]
				{
					"INSERT INTO Conferment(EmpManager,EMP_NO,SEC_NO,ActionID,PermitValue,StartDate,EndDate) VALUES(",
					empManager,
					",",
					empNo,
					",",
					secNo,
					",",
					actionId,
					",",
					permitValue,
					",'",
					startDate.ToString("yyyy-MM-dd"),
					"','",
					endDate.ToString("yyyy-MM-dd"),
					"')"
				});
				result = (base.ExecuteNonQuery(CommandType.Text, commandText) > 0);
			}
			catch
			{
				result = false;
			}
			return result;
		}

		// Token: 0x060000D6 RID: 214 RVA: 0x00005E8C File Offset: 0x0000408C
		public bool UpdatePermitValue(double empManager, double empNo, int secNo, int actionId, int permitValue, DateTime startDate, DateTime endDate)
		{
			bool result;
			try
			{
				string commandText = string.Concat(new object[]
				{
					"UPDATE Conferment SET PermitValue = ",
					permitValue,
					"\nWHERE EmpManager=",
					empManager,
					" AND EMP_NO=",
					empNo,
					" AND SEC_NO=",
					secNo,
					" AND ActionID= ",
					actionId
				});
				result = (base.ExecuteNonQuery(CommandType.Text, commandText) > 0);
			}
			catch
			{
				result = false;
			}
			return result;
		}

		// Token: 0x060000D7 RID: 215 RVA: 0x00005F24 File Offset: 0x00004124
		public bool Delete(double empManager, double empNo, int secNo, int actionId)
		{
			bool result;
			try
			{
				string commandText = string.Concat(new object[]
				{
					"UPDATE Conferment SET IsDelete = ~IsDelete  WHERE \nEmpManager=",
					empManager,
					" AND EMP_NO=",
					empNo,
					" AND SEC_NO=",
					secNo,
					" AND ActionID= ",
					actionId
				});
				result = (base.ExecuteNonQuery(CommandType.Text, commandText) > 0);
			}
			catch
			{
				result = false;
			}
			return result;
		}

		// Token: 0x060000D8 RID: 216 RVA: 0x00005FA8 File Offset: 0x000041A8
		public DataRow GetExist(double empManager, int actionId, int secNo)
		{
			DataRow result;
			try
			{
				string commandText = string.Concat(new object[]
				{
					"SELECT * FROM Conferment c WHERE c.EmpManager=",
					empManager,
					" AND c.IsDelete=0 AND c.ActionID=",
					actionId,
					" AND c.SEC_NO=",
					secNo
				});
				result = base.ExecuteDataRow(CommandType.Text, commandText);
			}
			catch
			{
				result = null;
			}
			return result;
		}
	}
}
