﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using MiddleWare.KaraLib.Lib.SQLData;


namespace PW.WorkFlows.SqlDal
{
	// Token: 0x02000018 RID: 24
	public class EmployeeManagers : BaseDAL
	{
		// Token: 0x0600012D RID: 301 RVA: 0x00003C40 File Offset: 0x00001E40
		public EmployeeManagers(WorkFlowsDatabase owner) : base(owner)
		{
		}

		// Token: 0x0600012E RID: 302 RVA: 0x00009B6F File Offset: 0x00007D6F
		public DataTable GetData()
		{
			return base.ExecuteDataTable("WF_EmployeeManagersGetData", null);
		}

		// Token: 0x0600012F RID: 303 RVA: 0x00009B7D File Offset: 0x00007D7D
		public DataTable GetDataByManager(double managerEmpNo, int operationsId)
		{
			return base.ExecuteDataTable("WF_EmployeeManagersGetDataByManager", new object[]
			{
				managerEmpNo,
				operationsId
			});
		}

		// Token: 0x06000130 RID: 304 RVA: 0x00009BA2 File Offset: 0x00007DA2
		public int Insert(double managerEmpNo, int operationsId, double empNo)
		{
			return base.ExecuteNonQuery("WF_EmployeeManagersInsert", new object[]
			{
				managerEmpNo,
				operationsId,
				empNo
			});
		}

		// Token: 0x06000131 RID: 305 RVA: 0x00009BD0 File Offset: 0x00007DD0
		public int Delete(double managerEmpNo, int operationsId, double empNo)
		{
			return base.ExecuteNonQuery("WF_EmployeeManagersDelete", new object[]
			{
				managerEmpNo,
				operationsId,
				empNo
			});
		}

		// Token: 0x06000132 RID: 306 RVA: 0x00009BFE File Offset: 0x00007DFE
		public int DeleteAll(double managerEmpNo, int operationsId)
		{
			return base.ExecuteNonQuery("WF_EmployeeManagersDeleteAll", new object[]
			{
				managerEmpNo,
				operationsId
			});
		}

		// Token: 0x06000133 RID: 307 RVA: 0x00009C23 File Offset: 0x00007E23
		public int InsertAll(double managerEmpNo, int operationsId)
		{
			return base.ExecuteNonQuery("WF_EmployeeManagersInsertAll", new object[]
			{
				managerEmpNo,
				operationsId
			});
		}

		// Token: 0x06000134 RID: 308 RVA: 0x00009C48 File Offset: 0x00007E48
		public DataTable GetPerson(double managerEmpNo)
		{
			return base.ExecuteDataTable("WF_EmployeeGetPerson", new object[]
			{
				managerEmpNo
			});
		}

		// Token: 0x06000135 RID: 309 RVA: 0x00009C64 File Offset: 0x00007E64
		public List<EmployeeRow> GetEmployee()
		{
			List<EmployeeRow> list = new List<EmployeeRow>();
			using (SqlDataReader sqlDataReader = base.ExecuteReader(CommandType.Text, "SELECT *,ltrim(Str(wgev.EMP_NO))+' '+ wgev.FullName EmpName  FROM WF_GetEmployee_VI wgev", null))
			{
				if (!sqlDataReader.HasRows)
				{
					return list;
				}
				while (sqlDataReader.Read())
				{
					list.Add(new EmployeeRow(sqlDataReader));
				}
			}
			return list;
		}

		// Token: 0x06000136 RID: 310 RVA: 0x00009CC4 File Offset: 0x00007EC4
		public List<EmployeeRow> GetEmployee(int pageSize, double skip)
		{
			string commandText = string.Concat(new object[]
			{
				"SELECT TOP ",
				pageSize,
				" * \nFROM   ( \n           SELECT ROW_NUMBER() OVER(ORDER BY wgev.EMP_NO) rn, \n                  *, \n                  STR(wgev.EMP_NO) + ' ' + wgev.FullName EmpName \n           FROM   WF_GetEmployee_VI wgev \n       )dt \nWHERE  dt.rn > ",
				skip
			});
			List<EmployeeRow> list = new List<EmployeeRow>();
			using (SqlDataReader sqlDataReader = base.ExecuteReader(CommandType.Text, commandText, null))
			{
				if (!sqlDataReader.HasRows)
				{
					return list;
				}
				while (sqlDataReader.Read())
				{
					list.Add(new EmployeeRow(sqlDataReader));
				}
			}
			return list;
		}
	}
}
