﻿using System;
using System.Data;

namespace PW.WorkFlows.SqlDal
{
	// Token: 0x02000005 RID: 5
	public class WardenCards : BaseDAL
	{
		// Token: 0x0600005B RID: 91 RVA: 0x00003C40 File Offset: 0x00001E40
		public WardenCards(WorkFlowsDatabase owner) : base(owner)
		{
		}

		// Token: 0x0600005C RID: 92 RVA: 0x00003CCC File Offset: 0x00001ECC
		public DataSet GetData(string wardenEmpNo)
		{
			string commandText = string.Format("SELECT c.TITLE, \n       LTRIM(STR(c.CARD_NO)) + '#' + CASE  \n                                          WHEN wc.WardenEmp_NO IS NULL THEN '0' \n                                          ELSE '1' \n                                     END cardNOVal \nFROM   CARDS c \n       LEFT OUTER JOIN WardenCards wc \n            ON  wc.CardNo = c.CARD_NO \n            AND wc.WardenEmp_NO = {0} \nWHERE  CARD_TYPE IN (2, 3, 5, 30,11) \n       AND c.CARD_NO > 0", wardenEmpNo);
			return base.ExecuteDataset(CommandType.Text, commandText);
		}

		// Token: 0x0600005D RID: 93 RVA: 0x00003CF0 File Offset: 0x00001EF0
		public bool Insert(string wardenEmpNo, string cardNo)
		{
			string commandText = string.Concat(new string[]
			{
				"INSERT INTO WardenCards(WardenEmp_NO,CardNo) \nVALUES(",
				wardenEmpNo,
				",",
				cardNo,
				")"
			});
			return base.ExecuteNonQuery(CommandType.Text, commandText) >= 0;
		}

		// Token: 0x0600005E RID: 94 RVA: 0x00003D38 File Offset: 0x00001F38
		public bool Delete(string wardenEmpNo)
		{
			string commandText = "DELETE FROM WardenCards WHERE WardenEmp_NO=" + wardenEmpNo;
			return base.ExecuteNonQuery(CommandType.Text, commandText) >= 0;
		}
	}
}
