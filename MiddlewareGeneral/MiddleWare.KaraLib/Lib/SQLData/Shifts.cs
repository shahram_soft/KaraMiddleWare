﻿using System;
using System.Data;

namespace PW.WorkFlows.SqlDal
{
	// Token: 0x0200002D RID: 45
	public class Shifts : BaseDAL
	{
		// Token: 0x060001F9 RID: 505 RVA: 0x00003C40 File Offset: 0x00001E40
		public Shifts(WorkFlowsDatabase owner) : base(owner)
		{
		}

		// Token: 0x060001FA RID: 506 RVA: 0x0001520B File Offset: 0x0001340B
		public DataTable GetData()
		{
			return base.ExecuteDataTable("WF_SHIFTSGetData", null);
		}

		// Token: 0x060001FB RID: 507 RVA: 0x00015219 File Offset: 0x00013419
		public DataRow GetDataByShiftNo(short shiftNo)
		{
			return base.ExecuteDataRow("WF_SHIFTSGetDataByShiftNo", new object[]
			{
				shiftNo
			});
		}

		// Token: 0x060001FC RID: 508 RVA: 0x00015238 File Offset: 0x00013438
		public DataTable FindShift(double empNo, double dateStart, int year, int month, int day)
		{
			return base.ExecuteDataTable("WF_FindShift", new object[]
			{
				empNo,
				dateStart,
				year,
				month,
				day
			});
		}

		// Token: 0x060001FD RID: 509 RVA: 0x00015288 File Offset: 0x00013488
		public DataRow GetSHiftData(double empNo, double dateStart, int year, int month, int day)
		{
			DataRow result;
			try
			{
				DataTable dataTable = this.FindShift(empNo, dateStart, year, month, day);
				if (dataTable != null && dataTable.Rows != null && dataTable.Rows.Count > 0)
				{
					result = this.GetDataByShiftNo(Convert.ToInt16(dataTable.Rows[0]["ShiftNo"]));
				}
				else
				{
					int? num = new int?(0);
					int? num2 = new int?(0);
					int? num3 = new int?(0);
					WorkFlowsDatabase.Instance.Groups.Sp_MixFindShift(ref num, new double?(empNo), new double?(dateStart), new int?(year), new int?(month), new int?(day), ref num2, ref num3);
					result = ((num2 > 0) ? this.GetDataByShiftNo(Convert.ToInt16(num2)) : null);
				}
			}
			catch
			{
				result = null;
			}
			return result;
		}
	}
}
