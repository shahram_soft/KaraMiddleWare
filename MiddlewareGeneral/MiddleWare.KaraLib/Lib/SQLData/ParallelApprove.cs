﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using MiddleWare.KaraLib;
using MiddleWare.KaraLib.ViewModels;


namespace PW.WorkFlows.SqlDal
{
	// Token: 0x02000025 RID: 37
	public class ParallelApprove : BaseDAL
	{
		// Token: 0x0600017A RID: 378 RVA: 0x00003C40 File Offset: 0x00001E40
		public ParallelApprove(WorkFlowsDatabase owner) : base(owner)
		{
		}

		// Token: 0x0600017B RID: 379 RVA: 0x0000BFC4 File Offset: 0x0000A1C4
		public DataTable GetData()
		{
			return base.ExecuteDataTable(CommandType.Text, "SELECT * FROM WFParallelApprove wa");
		}

		// Token: 0x0600017C RID: 380 RVA: 0x0000BFD4 File Offset: 0x0000A1D4
		public List<ParallelApproveRow> GetDataByCardNo(int requestId, int cardNo)
		{
			List<ParallelApproveRow> list = new List<ParallelApproveRow>();
			List<ParallelApproveRow> result;
			using (SqlDataReader sqlDataReader = base.ExecuteReader("WF_ParallelApproveGetData", new object[]
			{
				requestId,
				cardNo
			}))
			{
				if (sqlDataReader.HasRows)
				{
					while (sqlDataReader.Read())
					{
						ParallelApproveRow item = new ParallelApproveRow(sqlDataReader);
						list.Add(item);
					}
				}
				result = list;
			}
			return result;
		}

		// Token: 0x0600017D RID: 381 RVA: 0x0000C04C File Offset: 0x0000A24C
		public int GetTooltip(int requestId, int cardNo, ref string toolTipStr)
		{
			List<ParallelApproveRow> dataByCardNo = WorkFlowsDatabase.Instance.ParallelApprove.GetDataByCardNo(requestId, cardNo);
			if (dataByCardNo.Count > 0)
			{
				toolTipStr = "اطلاعات تایید کارشناس ::. \n";
				int num = 0;
				foreach (ParallelApproveRow current in dataByCardNo)
				{
					num++;
					if (current.RequestId > 0)
					{
						if (current.FromDate.HasValue)
						{
							if (current.IsApprove)
							{
								if (current.IsDaily)
								{
									toolTipStr += string.Format("{0}: تایید شده توسط {1}({2}) از تاریخ {3} به مدت {4}", new object[]
									{
										num,
										current.FullName,
										current.Section,
										ShDate.DateToStr(current.FromDate.Value),
										current.RequestDuration
									});
									if (current.CardNo != 100)
									{
										toolTipStr += " روز";
									}
								}
								else
								{
									toolTipStr += string.Format("{0}: تایید شده توسط {1}({2})", num, current.FullName, current.Section);
								}
							}
							else
							{
								toolTipStr += string.Format("<span style='color: red'>درخواست توسط {0} رد شده است</span>", current.FullName);
							}
						}
					}
					else
					{
						toolTipStr += string.Format("<span style='color: red'>{0}: نیاز به تایید {1}({2})</span>", num, current.FullName, current.Section);
					}
					toolTipStr += "\n";
				}
			}
			return dataByCardNo.Count + 1;
		}

		// Token: 0x0600017E RID: 382 RVA: 0x0000C1F4 File Offset: 0x0000A3F4
		public int GetDataByEmpNo(double empNo, bool isNull)
		{
			object obj = base.ExecuteScalar("WF_ParallelApproveCountAcceptState", new object[]
			{
				empNo,
				isNull
			});
			if (obj != null)
			{
				return Convert.ToInt32(obj);
			}
			return 0;
		}

		// Token: 0x0600017F RID: 383 RVA: 0x0000C230 File Offset: 0x0000A430
		public int Insert(int cardNo, double empNo, int secNo, int limitDuration)
		{
			string commandText = string.Concat(new object[]
			{
				"INSERT INTO WFParallelApprove(CardNo,SecNo,EmpNo,LimitDuration) \nVALUES(",
				cardNo,
				",",
				secNo,
				",",
				empNo,
				",",
				limitDuration,
				")"
			});
			return base.ExecuteNonQuery(CommandType.Text, commandText);
		}

		// Token: 0x06000180 RID: 384 RVA: 0x0000C2A0 File Offset: 0x0000A4A0
		public int Update(int oldcardNo, double oldempNo, int oldsecNo, int cardNo, double empNo, int secNo, int limitDuration)
		{
			string commandText = string.Format("UPDATE WFParallelApprove SET CardNo = {0},SecNo = {1},EmpNo = {2},LimitDuration = {3},DateModified = GETDATE() where CardNo = {4} AND SecNo = {5} AND EmpNo = {6} ", new object[]
			{
				cardNo,
				secNo,
				empNo,
				limitDuration,
				oldcardNo,
				oldsecNo,
				oldempNo
			});
			return base.ExecuteNonQuery(CommandType.Text, commandText);
		}

		// Token: 0x06000181 RID: 385 RVA: 0x0000C30C File Offset: 0x0000A50C
		public int Delete(int cardNo, double empNo, int secNo)
		{
			string commandText = string.Format("DELETE FROM WFParallelApprove WHERE CardNo = {0} and SecNo = {1} and EmpNo = {2}", cardNo, secNo, empNo);
			return base.ExecuteNonQuery(CommandType.Text, commandText);
		}

		// Token: 0x06000182 RID: 386 RVA: 0x0000C33E File Offset: 0x0000A53E
		public DataTable GetAcceptState(int requestId, int cardNo)
		{
			return base.ExecuteDataTable("WF_ParallelApproveGetAcceptState", new object[]
			{
				requestId,
				cardNo
			});
		}

		// Token: 0x06000183 RID: 387 RVA: 0x0000C364 File Offset: 0x0000A564
		public bool HaveNotAccept(int requestId, int cardNo)
		{
			DataTable acceptState = this.GetAcceptState(requestId, cardNo);
			return acceptState != null && acceptState.Rows != null && acceptState.Rows.Count > 0 && acceptState.Select("RequestID IS NULL").Any<DataRow>();
		}

		// Token: 0x06000184 RID: 388 RVA: 0x0000C3A8 File Offset: 0x0000A5A8
		public bool IsParallelApprove(double empNo)
		{
			object obj = base.ExecuteScalar(CommandType.Text, "SELECT COUNT(1) cnt FROM WFParallelApprove wa WHERE wa.EmpNo=" + empNo);
			return obj != null && Convert.ToInt32(obj) > 0;
		}

		// Token: 0x06000185 RID: 389 RVA: 0x0000C3DC File Offset: 0x0000A5DC
		public RequestCountRow CountNewRequestByApprovalEmpNo(double empNo)
		{
			DataRow dataRow = base.ExecuteDataRow("WF_CountNewRequestByApprovalEmpNo", new object[]
			{
				empNo
			});
			if (dataRow != null)
			{
				return new RequestCountRow(dataRow);
			}
			return new RequestCountRow();
		}

		// Token: 0x06000186 RID: 390 RVA: 0x0000C413 File Offset: 0x0000A613
		public DataTable GetNewRequest(double empNo)
		{
			return base.ExecuteDataTable("WF_GetNewRequestForParallel", new object[]
			{
				empNo
			});
		}
	}
}
