﻿using System;
using System.Data;
using MiddleWare.KaraLib;


namespace PW.WorkFlows.SqlDal
{
	// Token: 0x0200001E RID: 30
	public class LogMorMam : BaseDAL
	{
		// Token: 0x06000154 RID: 340 RVA: 0x00003C40 File Offset: 0x00001E40
		public LogMorMam(WorkFlowsDatabase owner) : base(owner)
		{
		}

		// Token: 0x06000155 RID: 341 RVA: 0x0000A9F8 File Offset: 0x00008BF8
		public long Insert(DataRow dr, double currentEmpNo)
		{
			string text = UtilityManagers.ConvertXorToString(dr["EMP_No"].ToString());
			string text2 = UtilityManagers.ConvertXorToString(currentEmpNo.ToString());
			double num = 0.0;
			if (!(dr["ISU_DATE"] is DBNull) && dr["ISU_DATE"] != null)
			{
				num = Math.Round(Convert.ToDouble(dr["ISU_DATE"]), 0);
			}
			double num2 = Math.Round(Convert.ToDouble(dr["S_DATE"]), 0);
			double num3 = Math.Round(Convert.ToDouble(dr["E_DATE"]), 0);
			string text3 = UtilityManagers.ConvertXorToString(dr["Requested"].ToString());
			string text4 = UtilityManagers.ConvertXorToString(dr["TYP"].ToString());
			string text5 = UtilityManagers.ConvertXorToString(0.ToString());
			string text6 = dr["Babat"].ToString();
			long num4 = Convert.ToInt64(dr["RefNumber"]);
			object obj = base.ExecuteScalar("WF_LogMor_MamInsert", new object[]
			{
				text,
				text2,
				num,
				num2,
				num3,
				num,
				text3,
				text4,
				text5,
				text6,
				num4
			});
			if (obj == null)
			{
				return 0L;
			}
			return Convert.ToInt64(obj);
		}

		// Token: 0x06000156 RID: 342 RVA: 0x0000AB6C File Offset: 0x00008D6C
		public long Insert(string empNo, double isuDate, double sDate, double eDate, string requested, string typ, string incTyp, string babat, long refNumber, string userId)
		{
			empNo = UtilityManagers.ConvertXorToString(empNo);
			requested = UtilityManagers.ConvertXorToString(requested);
			typ = UtilityManagers.ConvertXorToString(typ);
			incTyp = UtilityManagers.ConvertXorToString(incTyp);
			object obj = base.ExecuteScalar("WF_LogMor_MamInsert", new object[]
			{
				empNo,
				userId,
				isuDate,
				sDate,
				eDate,
				isuDate,
				requested,
				typ,
				incTyp,
				babat,
				refNumber
			});
			if (obj != null)
			{
				return Convert.ToInt64(obj);
			}
			return 0L;
		}
	}
}
