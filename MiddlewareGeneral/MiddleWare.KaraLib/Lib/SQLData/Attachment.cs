﻿//using System;
//using System.Collections.Generic;
//using System.Data;
//using System.Data.SqlClient;
//using System.IO;


//namespace PW.WorkFlows.SqlDal
//{
//	// Token: 0x02000010 RID: 16
//	public class Attachment : BaseDAL
//	{
//		// Token: 0x060000D9 RID: 217 RVA: 0x00003C40 File Offset: 0x00001E40
//		public Attachment(WorkFlowsDatabase owner) : base(owner)
//		{
//		}

//		// Token: 0x060000DA RID: 218 RVA: 0x00006018 File Offset: 0x00004218
//		public int Delete(int attachmentId)
//		{
//			return base.ExecuteNonQuery("WF_Attachment_Delete", new object[]
//			{
//				attachmentId
//			});
//		}

//		// Token: 0x060000DB RID: 219 RVA: 0x00006034 File Offset: 0x00004234
//		public int DeleteByRequestId(int requestId)
//		{
//			return base.ExecuteNonQuery("WF_Attachment_DeleteByRequestID", new object[]
//			{
//				requestId
//			});
//		}

//		// Token: 0x060000DC RID: 220 RVA: 0x00006050 File Offset: 0x00004250
//		private int Insert(int requestId, string name, byte[] attachment, byte[] attachmentThu, string attachmentType)
//		{
//			return base.ExecuteNonQuery("WF_Attachment_Insert", new object[]
//			{
//				requestId,
//				name,
//				attachment,
//				attachmentThu,
//				attachmentType
//			});
//		}

//		// Token: 0x060000DD RID: 221 RVA: 0x00006080 File Offset: 0x00004280
//		public int Insert(AttachmentRow attRow, string attachmentPath)
//		{
//			File.Move(Path.Combine(Path.Combine(attachmentPath, "Attachment_Temp"), attRow.Name), Path.Combine(attachmentPath, attRow.RequestId + "_" + attRow.Name));
//			return base.ExecuteNonQuery("WF_Attachment_Insert", new object[]
//			{
//				attRow.RequestId,
//				attRow.Name,
//				null,
//				null,
//				attRow.AttachmentType
//			});
//		}

//		// Token: 0x060000DE RID: 222 RVA: 0x000060FA File Offset: 0x000042FA
//		public DataTable Select()
//		{
//			return base.ExecuteDataTable("WF_Attachment_Select", null);
//		}

//		// Token: 0x060000DF RID: 223 RVA: 0x00006108 File Offset: 0x00004308
//		public DataTable SelectById(int attachmentId)
//		{
//			return base.ExecuteDataTable("WF_Attachment_SelectByID", new object[]
//			{
//				attachmentId
//			});
//		}

//		// Token: 0x060000E0 RID: 224 RVA: 0x00006124 File Offset: 0x00004324
//		public List<AttachmentRow> SelectByRequestId(int requestId)
//		{
//			List<AttachmentRow> list = new List<AttachmentRow>();
//			List<AttachmentRow> result;
//			using (SqlDataReader sqlDataReader = base.ExecuteReader("WF_Attachment_SelectByRequestID", new object[]
//			{
//				requestId
//			}))
//			{
//				if (sqlDataReader.HasRows)
//				{
//					while (sqlDataReader.Read())
//					{
//						AttachmentRow item = new AttachmentRow
//						{
//							AttachmentId = Convert.ToInt32((sqlDataReader["AttachmentID"] is DBNull || sqlDataReader["AttachmentID"] == null) ? 0 : sqlDataReader["AttachmentID"]),
//							RequestId = Convert.ToInt32((sqlDataReader["RequestID"] is DBNull || sqlDataReader["RequestID"] == null) ? 0 : sqlDataReader["RequestID"]),
//							Name = Convert.ToString(sqlDataReader["Name"]),
//							Attachment = (byte[])sqlDataReader["Attachment"],
//							AttachmentThu = (byte[])sqlDataReader["Attachment_thu"],
//							AttachmentType = Convert.ToString(sqlDataReader["Attachment_Type"])
//						};
//						list.Add(item);
//					}
//				}
//				result = list;
//			}
//			return result;
//		}
//	}
//}
