﻿using System;
using System.Data;

namespace PW.WorkFlows.SqlDal
{
	// Token: 0x02000032 RID: 50
	public class WardenSection : BaseDAL
	{
		// Token: 0x06000258 RID: 600 RVA: 0x00003C40 File Offset: 0x00001E40
		public WardenSection(WorkFlowsDatabase owner) : base(owner)
		{
		}

		// Token: 0x06000259 RID: 601 RVA: 0x00016E74 File Offset: 0x00015074
		public DataSet GetData(string wardenEmpNo)
		{
			string commandText = string.Concat(new string[]
			{
				"SELECT e.WardenDayCount,e.WardenJustInShift,e.WardenIsShowRequest,e.WardenIsShowReview FROM EMPLOYEE e WHERE e.EMP_NO=",
				wardenEmpNo,
				" \n SELECT *, LTRIM(STR(dd.SEC_NO)) + '#' + LTRIM(dd.ISAccess) SecValue FROM   (SELECT s.SEC_NO, s.TITLE,  \n(CASE ISNULL((SELECT top 1 ws.WardenEmp_NO FROM   WardenSection ws WHERE  ws.SecNo = s.SEC_NO \nAND ws.WardenEmp_NO = ",
				wardenEmpNo,
				"),0)WHEN 0 THEN 0 ELSE 1 END) ISAccess FROM   SECTIONS s)dd \n"
			});
			return base.ExecuteDataset(CommandType.Text, commandText);
		}

		// Token: 0x0600025A RID: 602 RVA: 0x00016EB8 File Offset: 0x000150B8
		public bool Insert(string wardenEmpNo, string secNo)
		{
			string commandText = string.Concat(new string[]
			{
				"INSERT INTO WardenSection(WardenEmp_NO,\tSecNo) \nVALUES(\t",
				wardenEmpNo,
				",",
				secNo,
				")"
			});
			return base.ExecuteNonQuery(CommandType.Text, commandText) >= 0;
		}

		// Token: 0x0600025B RID: 603 RVA: 0x00016F00 File Offset: 0x00015100
		public bool Delete(string wardenEmpNo)
		{
			string commandText = "DELETE FROM WardenSection WHERE WardenEmp_NO=" + wardenEmpNo;
			return base.ExecuteNonQuery(CommandType.Text, commandText) >= 0;
		}

		// Token: 0x0600025C RID: 604 RVA: 0x00016F28 File Offset: 0x00015128
		public bool UpdateEmployee_Warden(string wardenEmpNo, int wardenDayCount, bool wardenJustInShift, bool WardenIsShowRequest, bool WardenIsShowReview)
		{
			string commandText = string.Concat(new object[]
			{
				"UPDATE EMPLOYEE SET WardenDayCount =",
				wardenDayCount,
				" , WardenJustInShift = ",
				Convert.ToByte(wardenJustInShift),
				" , WardenIsShowRequest = ",
				Convert.ToByte(WardenIsShowRequest),
				" , WardenIsShowReview = ",
				Convert.ToByte(WardenIsShowReview),
				" WHERE EMP_NO=",
				wardenEmpNo
			}) ?? "";
			return base.ExecuteNonQuery(CommandType.Text, commandText) >= 0;
		}
	}
}
