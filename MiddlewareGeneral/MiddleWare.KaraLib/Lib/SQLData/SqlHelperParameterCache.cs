﻿using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;

namespace PW.WorkFlows.SqlDal
{
	// Token: 0x0200000C RID: 12
	[DebuggerStepThrough]
	internal sealed class SqlHelperParameterCache
	{
		// Token: 0x060000A1 RID: 161 RVA: 0x00002050 File Offset: 0x00000250
		private SqlHelperParameterCache()
		{
		}

		// Token: 0x060000A2 RID: 162 RVA: 0x00005048 File Offset: 0x00003248
		private static SqlParameter[] DiscoverSpParameterSet(SqlConnection connection, string spName, bool includeReturnValueParameter)
		{
			if (connection == null)
			{
				throw new ArgumentNullException("connection");
			}
			if (spName == null || spName.Length == 0)
			{
				throw new ArgumentNullException("spName");
			}
			SqlCommand sqlCommand = new SqlCommand(spName, connection);
			sqlCommand.CommandType = CommandType.StoredProcedure;
			connection.Open();
			SqlCommandBuilder.DeriveParameters(sqlCommand);
			connection.Close();
			if (!includeReturnValueParameter)
			{
				sqlCommand.Parameters.RemoveAt(0);
			}
			SqlParameter[] array = new SqlParameter[sqlCommand.Parameters.Count];
			sqlCommand.Parameters.CopyTo(array, 0);
			SqlParameter[] array2 = array;
			for (int i = 0; i < array2.Length; i++)
			{
				array2[i].Value = DBNull.Value;
			}
			return array;
		}

		// Token: 0x060000A3 RID: 163 RVA: 0x000050E8 File Offset: 0x000032E8
		private static SqlParameter[] CloneParameters(SqlParameter[] originalParameters)
		{
			SqlParameter[] array = new SqlParameter[originalParameters.Length];
			int i = 0;
			int num = originalParameters.Length;
			while (i < num)
			{
				array[i] = (SqlParameter)((ICloneable)originalParameters[i]).Clone();
				i++;
			}
			return array;
		}

		// Token: 0x060000A4 RID: 164 RVA: 0x00005120 File Offset: 0x00003320
		public static void CacheParameterSet(string connectionString, string commandText, params SqlParameter[] commandParameters)
		{
			if (string.IsNullOrEmpty(connectionString))
			{
				throw new ArgumentNullException("connectionString");
			}
			if (string.IsNullOrEmpty(commandText))
			{
				throw new ArgumentNullException("commandText");
			}
			string key = connectionString + ":" + commandText;
			SqlHelperParameterCache.paramCache[key] = commandParameters;
		}

		// Token: 0x060000A5 RID: 165 RVA: 0x0000516C File Offset: 0x0000336C
		public static SqlParameter[] GetCachedParameterSet(string connectionString, string commandText)
		{
			if (string.IsNullOrEmpty(connectionString))
			{
				throw new ArgumentNullException("connectionString");
			}
			if (string.IsNullOrEmpty(commandText))
			{
				throw new ArgumentNullException("commandText");
			}
			string key = connectionString + ":" + commandText;
			SqlParameter[] array = SqlHelperParameterCache.paramCache[key] as SqlParameter[];
			if (array != null)
			{
				return SqlHelperParameterCache.CloneParameters(array);
			}
			return null;
		}

		// Token: 0x060000A6 RID: 166 RVA: 0x000051C8 File Offset: 0x000033C8
		public static SqlParameter[] GetSpParameterSet(string connectionString, string spName)
		{
			return SqlHelperParameterCache.GetSpParameterSet(connectionString, spName, false);
		}

		// Token: 0x060000A7 RID: 167 RVA: 0x000051D4 File Offset: 0x000033D4
		public static SqlParameter[] GetSpParameterSet(string connectionString, string spName, bool includeReturnValueParameter)
		{
			if (connectionString == null || connectionString.Length == 0)
			{
				throw new ArgumentNullException("connectionString");
			}
			if (spName == null || spName.Length == 0)
			{
				throw new ArgumentNullException("spName");
			}
			SqlParameter[] spParameterSetInternal;
			using (SqlConnection sqlConnection = new SqlConnection(connectionString))
			{
				spParameterSetInternal = SqlHelperParameterCache.GetSpParameterSetInternal(sqlConnection, spName, includeReturnValueParameter);
			}
			return spParameterSetInternal;
		}

		// Token: 0x060000A8 RID: 168 RVA: 0x0000523C File Offset: 0x0000343C
		internal static SqlParameter[] GetSpParameterSet(SqlConnection connection, string spName)
		{
			return SqlHelperParameterCache.GetSpParameterSet(connection, spName, false);
		}

		// Token: 0x060000A9 RID: 169 RVA: 0x00005248 File Offset: 0x00003448
		internal static SqlParameter[] GetSpParameterSet(SqlConnection connection, string spName, bool includeReturnValueParameter)
		{
			if (connection == null)
			{
				throw new ArgumentNullException("connection");
			}
			SqlParameter[] spParameterSetInternal;
			using (SqlConnection sqlConnection = (SqlConnection)((ICloneable)connection).Clone())
			{
				spParameterSetInternal = SqlHelperParameterCache.GetSpParameterSetInternal(sqlConnection, spName, includeReturnValueParameter);
			}
			return spParameterSetInternal;
		}

		// Token: 0x060000AA RID: 170 RVA: 0x00005298 File Offset: 0x00003498
		private static SqlParameter[] GetSpParameterSetInternal(SqlConnection connection, string spName, bool includeReturnValueParameter)
		{
			if (connection == null)
			{
				throw new ArgumentNullException("connection");
			}
			if (spName == null || spName.Length == 0)
			{
				throw new ArgumentNullException("spName");
			}
			string key = connection.ConnectionString + ":" + spName + (includeReturnValueParameter ? ":include ReturnValue Parameter" : "");
			SqlParameter[] array = SqlHelperParameterCache.paramCache[key] as SqlParameter[];
			if (array == null)
			{
				SqlParameter[] array2 = SqlHelperParameterCache.DiscoverSpParameterSet(connection, spName, includeReturnValueParameter);
				SqlHelperParameterCache.paramCache[key] = array2;
				array = array2;
			}
			return SqlHelperParameterCache.CloneParameters(array);
		}

		// Token: 0x04000003 RID: 3
		private static Hashtable paramCache = Hashtable.Synchronized(new Hashtable());
	}
}
