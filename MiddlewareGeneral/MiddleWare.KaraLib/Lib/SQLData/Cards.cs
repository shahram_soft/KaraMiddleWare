﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using MiddleWare.KaraLib;


namespace PW.WorkFlows.SqlDal
{
	// Token: 0x0200000D RID: 13
	public class Cards : BaseDAL
	{
		// Token: 0x060000AC RID: 172 RVA: 0x00003C40 File Offset: 0x00001E40
		public Cards(WorkFlowsDatabase owner) : base(owner)
		{
		}

		// Token: 0x060000AD RID: 173 RVA: 0x0000532C File Offset: 0x0000352C
		public DataTable GetCardsData(bool isDay, int cardNo, double empNo)
		{
			DataTable dataTable = base.ExecuteDataTable("WF_CARDSGetCardsData", new object[]
			{
				isDay,
				empNo
			});
			if (dataTable != null && dataTable.Rows.Count > 0)
			{
				DataRow dataRow = dataTable.NewRow();
				dataRow["ID"] = Convert.ToInt32(dataTable.Compute("Max(ID)", "")) + 1;
				dataRow["CARD_NO"] = cardNo;
				dataRow["TITLE"] = "عادی";
				dataRow["REP_ACTIVE"] = 0;
				dataRow["SYS_ACTIVE"] = 1;
				dataRow["IS_DAY"] = 0;
				dataRow["SELECTED"] = 0;
				dataRow["ROUNDED"] = 0;
				dataRow["IsAmalkard"] = 0;
				dataRow["CARD_TYPE"] = 0;
				dataTable.Rows.InsertAt(dataRow, 0);
			}
			return dataTable;
		}

		// Token: 0x060000AE RID: 174 RVA: 0x00005448 File Offset: 0x00003648
		public DataTable GetData(short cardType, bool isDay)
		{
			if (cardType == 4)
			{
				cardType = 3;
			}
			return base.ExecuteDataTable("WF_CARDSGetData", new object[]
			{
				cardType,
				isDay
			});
		}

		// Token: 0x060000AF RID: 175 RVA: 0x00005474 File Offset: 0x00003674
		public DataTable GetAccessData(short cardType, bool isDay, double empNo)
		{
			return base.ExecuteDataTable("WF_CARDSGetAccessData", new object[]
			{
				cardType,
				isDay,
				empNo
			});
		}

		// Token: 0x060000B0 RID: 176 RVA: 0x000054A4 File Offset: 0x000036A4
		public DataTable GetAccessData(short cardType)
		{
			string commandText = "SELECT * FROM CARDS c WHERE c.CARD_TYPE=" + cardType + " AND c.SYS_ACTIVE=1 ";
			return base.ExecuteDataTable(CommandType.Text, commandText);
		}

		// Token: 0x060000B1 RID: 177 RVA: 0x000054CF File Offset: 0x000036CF
		public DataTable GetTimeType()
		{
			return base.ExecuteDataTable("WF_GetTimeType", null);
		}

		// Token: 0x060000B2 RID: 178 RVA: 0x000054DD File Offset: 0x000036DD
		public DataTable GetTimeTypeByEmpNo(double empNo)
		{
			return base.ExecuteDataTable("WF_GetTimeTypeByEmpNo", new object[]
			{
				empNo
			});
		}

		// Token: 0x060000B3 RID: 179 RVA: 0x000054FC File Offset: 0x000036FC
		public DataTable GetCardLimitetd(bool isDay)
		{
			string commandText = "SELECT c.CARD_NO,c.TITLE,ISNULL(c.LimitValue,0) LimitValue FROM CARDS c WHERE CARD_TYPE IN (2,3, 5,30) AND SYS_ACTIVE = 1 AND c.IS_DAY=" + Convert.ToByte(isDay);
			return base.ExecuteDataTable(CommandType.Text, commandText);
		}

		// Token: 0x060000B4 RID: 180 RVA: 0x00005527 File Offset: 0x00003727
		public DataTable GetCardLimitetd()
		{
			return base.ExecuteDataTable(CommandType.Text, "SELECT c.CARD_NO,c.TITLE,ISNULL(c.LimitValue,0) LimitValue,CASE c.IS_DAY WHEN 0 THEN N'ساعتي' ELSE N'روزانه' END Kind \n  FROM CARDS c WHERE CARD_TYPE IN (2,3, 5,30) AND SYS_ACTIVE = 1");
		}

		// Token: 0x060000B5 RID: 181 RVA: 0x00005535 File Offset: 0x00003735
		public DataTable GetCardsUseWf()
		{
			return base.ExecuteDataTable(CommandType.Text, "SELECT c.CARD_NO,c.TITLE,ISNULL(c.LimitValue,0) LimitValue,CASE c.IS_DAY WHEN 0 THEN N'ساعتي' ELSE N'روزانه' END Kind  \n           FROM  \nCARDS c WHERE ((CARD_TYPE IN (2,3, 5,30) AND SYS_ACTIVE = 1) OR c.CARD_TYPE=11) AND c.CARD_NO<>0 ");
		}

		// Token: 0x060000B6 RID: 182 RVA: 0x00005544 File Offset: 0x00003744
		public bool UpdateLimitValue(int limitValue, int cardNo)
		{
			string commandText = string.Concat(new object[]
			{
				"UPDATE CARDS SET LimitValue = ",
				limitValue,
				" WHERE CARD_NO=",
				cardNo
			});
			return base.ExecuteNonQuery(CommandType.Text, commandText) > 0;
		}

		// Token: 0x060000B7 RID: 183 RVA: 0x0000558A File Offset: 0x0000378A
		public bool IsDayByCardNo(short cardNo)
		{
			return Convert.ToBoolean(base.ExecuteScalar("WF_CARDSIsDayByCardNo", new object[]
			{
				cardNo
			}));
		}

		// Token: 0x060000B8 RID: 184 RVA: 0x000055AB File Offset: 0x000037AB
		public DataRow GetDataByCardNo(short? cardNo)
		{
			return base.ExecuteDataRow(CommandType.Text, "SELECT TOP 1 c.* FROM CARDS c WHERE c.CARD_NO=" + cardNo);
		}

		// Token: 0x060000B9 RID: 185 RVA: 0x000055C4 File Offset: 0x000037C4
		public string GetTitleByCardNo(string duration)
		{
			string result = "---";
			int num;
			if (int.TryParse(duration, out num))
			{
				object obj = base.ExecuteScalar(CommandType.Text, "SELECT TOP 1 c.TITLE FROM CARDS c WHERE c.CARD_NO=" + (num - 100));
				if (obj != null)
				{
					result = obj.ToString();
				}
			}
			return result;
		}

		// Token: 0x17000007 RID: 7
		// (get) Token: 0x060000BA RID: 186 RVA: 0x00005607 File Offset: 0x00003807
		// (set) Token: 0x060000BB RID: 187 RVA: 0x0000560F File Offset: 0x0000380F
		public bool AvalDoreFlg
		{
			get;
			set;
		}

		// Token: 0x17000008 RID: 8
		// (get) Token: 0x060000BC RID: 188 RVA: 0x00005618 File Offset: 0x00003818
		public bool CurIsMorDay
		{
			get
			{
				return WorkFlowsDatabase.Instance.Parmfile.GetDataRow().IsMorDay;
			}
		}

		// Token: 0x17000009 RID: 9
		// (get) Token: 0x060000BD RID: 189 RVA: 0x0000562E File Offset: 0x0000382E
		// (set) Token: 0x060000BE RID: 190 RVA: 0x00005636 File Offset: 0x00003836
		public double Inc
		{
			get;
			set;
		}

		// Token: 0x1700000A RID: 10
		// (get) Token: 0x060000BF RID: 191 RVA: 0x0000563F File Offset: 0x0000383F
		// (set) Token: 0x060000C0 RID: 192 RVA: 0x00005647 File Offset: 0x00003847
		public double Dec
		{
			get;
			set;
		}

		// Token: 0x1700000B RID: 11
		// (get) Token: 0x060000C1 RID: 193 RVA: 0x00005650 File Offset: 0x00003850
		// (set) Token: 0x060000C2 RID: 194 RVA: 0x00005658 File Offset: 0x00003858
		public bool IsForceLeaveInNextYear
		{
			get;
			set;
		}

		// Token: 0x060000C3 RID: 195 RVA: 0x00005664 File Offset: 0x00003864
		public int TimePlus(int time1, int time2)
		{
			int num = 0;
			int num2 = time1 % 100 + time2 % 100;
			int num3;
			if (time1 == 0)
			{
				num3 = time2;
			}
			else if (time2 == 0)
			{
				num3 = time1;
			}
			else
			{
				num3 = (time1 / 100 + time2 / 100 + num2 / 60) * 100 + num2 % 60;
			}
			int num4 = num3;
			if (num4 < 0)
			{
				num = num4;
				num4 *= -1;
			}
			if (time1 < 0 && time2 > 0 && num4 % 100 > 0)
			{
				time1 *= -1;
				num3 = ((time1 > time2) ? this.TimeSub(time1, time2) : this.TimeSub(time2, time1));
				if (num < 0)
				{
					num3 *= -1;
				}
			}
			else if (time1 > 0 && time2 < 0 && num4 % 100 > 0)
			{
				time2 *= -1;
				num3 = ((time1 > time2) ? this.TimeSub(time1, time2) : this.TimeSub(time2, time1));
				if (num < 0)
				{
					num3 *= -1;
				}
			}
			return num3;
		}

		// Token: 0x060000C4 RID: 196 RVA: 0x00005718 File Offset: 0x00003918
		public int TimeSub(int time1, int time2)
		{
			if (time1 < time2)
			{
				if (time1 == 0)
				{
					int arg_0B_0 = time1;
					time1 = time2;
					time2 = arg_0B_0;
				}
				else if (time2 != 0)
				{
					time1 += 2400;
				}
			}
			int num = time1 % 100;
			int num2 = time2 % 100;
			int time3;
			if (num < num2)
			{
				time3 = num + 60 - num2;
				time2 += 100;
			}
			else
			{
				time3 = num - num2;
			}
			int arg_46_0 = time1 / 100;
			int num3 = time2 / 100;
			int num4 = arg_46_0 - num3;
			return this.TimePlus(num4 * 100, time3);
		}

		// Token: 0x060000C5 RID: 197 RVA: 0x0000577C File Offset: 0x0000397C
		public int Mult_Time(double n, int time1)
		{
			bool flag = false;
			if (n < 0.0)
			{
				n *= -1.0;
				flag = true;
			}
			if (time1 < 0)
			{
				time1 *= -1;
				flag = true;
			}
			int expr_3A = (int)((double)(time1 / 100 * 60 + time1 % 100) * n);
			int num = expr_3A / 60;
			int num2 = expr_3A - num * 60;
			string s;
			if (num2 < 10)
			{
				s = num + "0" + num2;
			}
			else
			{
				s = num + num2.ToString();
			}
			int result;
			if (flag)
			{
				result = int.Parse(s) * -1;
			}
			else
			{
				result = int.Parse(s);
			}
			return result;
		}

		// Token: 0x060000C6 RID: 198 RVA: 0x00005818 File Offset: 0x00003A18
		public int DivTime(int time1, int time2)
		{
			int result;
			if (time1 != 0 & time2 != 0)
			{
				int num = time1 % 100;
				int num2 = time1 / 100;
				int num3 = time2 % 100;
				int arg_28_0 = time2 / 100;
				int num4 = num2 * 60 + num;
				int num5 = arg_28_0 * 60 + num3;
				result = num4 / num5;
			}
			else if (time1 == 0)
			{
				result = 0;
			}
			else
			{
				result = -1;
			}
			return result;
		}

		// Token: 0x060000C7 RID: 199 RVA: 0x00005864 File Offset: 0x00003A64
		public short FindGrpsByEmpAndDate(double empNo, DateTime date, short grpNo, ref int shiftno)
		{
			PersianCalendar expr_05 = new PersianCalendar();
			int year = expr_05.GetYear(date);
			int month = expr_05.GetMonth(date);
			int dayOfMonth = expr_05.GetDayOfMonth(date);
			int? num = new int?((int)grpNo);
			float num2 = Convert.ToSingle((int)date.ToOADate());
			int? num3 = new int?(0);
			int? num4 = new int?(0);
			WorkFlowsDatabase.Instance.Groups.Sp_MixFindShift(ref num, new double?(empNo), new double?((double)num2), new int?(year), new int?(month), new int?(dayOfMonth), ref num3, ref num4);
			if (num3.HasValue)
			{
				shiftno = num3.Value;
			}
			if (num.HasValue)
			{
				return (short)num.Value;
			}
			return 0;
		}

		// Token: 0x060000C8 RID: 200 RVA: 0x00005910 File Offset: 0x00003B10
		public DataTable GetApproval(bool isNullApproval)
		{
			return base.ExecuteDataTable("WF_CardsGetApproval", new object[]
			{
				isNullApproval
			});
		}

		// Token: 0x060000C9 RID: 201 RVA: 0x0000592C File Offset: 0x00003B2C
		public void GetCount(int cardNo, double empNo, int persianStartDay, out string text, out string textMonth, out string tooltips, out string tooltipsMonth)
		{
			string commandText = string.Concat(new object[]
			{
				"SELECT r.IsFinalApproved ,COUNT(1) cnt \n  FROM Requests r WHERE TYPE=",
				cardNo,
				" AND r.EMP_NO=",
				empNo,
				"   \nGROUP BY r.IsFinalApproved"
			});
			DataTable dataTable = base.ExecuteDataTable(CommandType.Text, commandText);
			int num;
			try
			{
				num = Convert.ToInt32(dataTable.Select("IsFinalApproved=1")[0]["cnt"]);
			}
			catch
			{
				num = 0;
			}
			int num2;
			try
			{
				num2 = Convert.ToInt32(dataTable.Select("IsFinalApproved=0")[0]["cnt"]);
			}
			catch
			{
				num2 = 0;
			}
			int num3;
			try
			{
				num3 = Convert.ToInt32(dataTable.Select("IsFinalApproved is null")[0]["cnt"]);
			}
			catch
			{
				num3 = 0;
			}
			text = "تعداد کل :" + (num + num2 + num3);
			tooltips = string.Format("تایید شده :{0} \n رد شده :{1}  \n بررسی نشده :{2}", num, num2, num3);
			List<DateTime> currentStartAndEndPeriod = ShDate.GetCurrentStartAndEndPeriod(persianStartDay);
			commandText = string.Concat(new object[]
			{
				"SELECT r.IsFinalApproved ,COUNT(1) cnt \n  FROM Requests r WHERE TYPE=",
				cardNo,
				" AND r.EMP_NO=",
				empNo,
				"  AND r.StartDate BETWEEN ",
				currentStartAndEndPeriod[0].ToOADate(),
				" AND ",
				currentStartAndEndPeriod[1].ToOADate(),
				" \nGROUP BY r.IsFinalApproved"
			});
			dataTable = base.ExecuteDataTable(CommandType.Text, commandText);
			try
			{
				num = Convert.ToInt32(dataTable.Select("IsFinalApproved=1")[0]["cnt"]);
			}
			catch
			{
				num = 0;
			}
			try
			{
				num2 = Convert.ToInt32(dataTable.Select("IsFinalApproved=0")[0]["cnt"]);
			}
			catch
			{
				num2 = 0;
			}
			try
			{
				num3 = Convert.ToInt32(dataTable.Select("IsFinalApproved is null")[0]["cnt"]);
			}
			catch
			{
				num3 = 0;
			}
			textMonth = "   تعداد ماه جاری :" + (num + num2 + num3);
			tooltipsMonth = string.Format("تایید شده :{0} \n رد شده :{1}  \n بررسی نشده :{2}", num, num2, num3);
		}
	}
}
