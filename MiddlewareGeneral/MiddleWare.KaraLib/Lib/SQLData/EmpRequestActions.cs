﻿using System;
using System.Data;

namespace PW.WorkFlows.SqlDal
{
	// Token: 0x02000019 RID: 25
	public class EmpRequestActions : BaseDAL
	{
		// Token: 0x06000137 RID: 311 RVA: 0x00003C40 File Offset: 0x00001E40
		public EmpRequestActions(WorkFlowsDatabase owner) : base(owner)
		{
		}

		// Token: 0x06000138 RID: 312 RVA: 0x00009D50 File Offset: 0x00007F50
		public DataTable GetFreeEmployee(int actionId)
		{
			return base.ExecuteDataTable(CommandType.Text, "SELECT * FROM WF_GetEmployee_VI wgev WHERE wgev.EMP_NO NOT IN (SELECT era.EmpNo FROM EmpRequestActions era WHERE era.ActionId=" + actionId + ")");
		}

		// Token: 0x06000139 RID: 313 RVA: 0x00009D6E File Offset: 0x00007F6E
		public DataTable GetUnFreeEmployee(int actionId)
		{
			return base.ExecuteDataTable(CommandType.Text, "SELECT * FROM WF_GetEmployee_VI wgev WHERE wgev.EMP_NO IN (SELECT era.EmpNo FROM EmpRequestActions era WHERE era.ActionId=" + actionId + ")");
		}

		// Token: 0x0600013A RID: 314 RVA: 0x00009D8C File Offset: 0x00007F8C
		public int Insert(int actionId, double empNo)
		{
			return base.ExecuteNonQuery(CommandType.Text, string.Concat(new object[]
			{
				"INSERT INTO EmpRequestActions(ActionId,EmpNo)VALUES(",
				actionId,
				",",
				empNo,
				")"
			}));
		}

		// Token: 0x0600013B RID: 315 RVA: 0x00009DCA File Offset: 0x00007FCA
		public int Delete(int actionId, double empNo)
		{
			return base.ExecuteNonQuery(CommandType.Text, string.Concat(new object[]
			{
				"DELETE FROM EmpRequestActions WHERE ActionId=",
				actionId,
				" AND EmpNo=",
				empNo
			}));
		}

		// Token: 0x0600013C RID: 316 RVA: 0x00009E00 File Offset: 0x00008000
		public int InsertAll(int actionId)
		{
			string commandText = string.Concat(new object[]
			{
				"INSERT INTO EmpRequestActions(ActionId,EmpNo) \n(SELECT ",
				actionId,
				",wgev.EMP_NO   FROM WF_GetEmployee_VI wgev WHERE wgev.EMP_NO NOT IN  \n(SELECT era.EmpNo FROM EmpRequestActions era WHERE era.ActionId=",
				actionId,
				"))"
			});
			return base.ExecuteNonQuery(CommandType.Text, commandText);
		}

		// Token: 0x0600013D RID: 317 RVA: 0x00009E4B File Offset: 0x0000804B
		public int DeleteAll(int actionId)
		{
			return base.ExecuteNonQuery(CommandType.Text, "DELETE FROM EmpRequestActions WHERE ActionId=" + actionId);
		}

		// Token: 0x0600013E RID: 318 RVA: 0x00009E64 File Offset: 0x00008064
		public DataTable GetRequestActionsByEmpNo(double empNo)
		{
			string commandText = "SELECT * FROM EmpRequestActions era \nJOIN [Action] a ON  a.ActionId = era.ActionId \nWHERE  era.EmpNo = " + empNo + " AND a.IsPermission = 1 AND a.IsShow = 1";
			return base.ExecuteDataTable(CommandType.Text, commandText);
		}
	}
}
