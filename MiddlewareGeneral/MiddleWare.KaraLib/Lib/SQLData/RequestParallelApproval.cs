﻿using System;
using System.Data;
using MiddleWare.KaraLib;


namespace PW.WorkFlows.SqlDal
{
	// Token: 0x02000029 RID: 41
	public class RequestParallelApproval : BaseDAL
	{
		// Token: 0x06000197 RID: 407 RVA: 0x00003C40 File Offset: 0x00001E40
		public RequestParallelApproval(WorkFlowsDatabase owner) : base(owner)
		{
		}

		// Token: 0x06000198 RID: 408 RVA: 0x0000E7CC File Offset: 0x0000C9CC
		public DataTable Get(int requestId)
		{
			string commandText = "SELECT * FROM WFRequestParallelApproval WHERE RequestID=" + requestId;
			return base.ExecuteDataTable(CommandType.Text, commandText);
		}

		// Token: 0x06000199 RID: 409 RVA: 0x0000E7F4 File Offset: 0x0000C9F4
		public DataRow Get(int requestId, double parallelApprovalEmpNo)
		{
			string commandText = string.Concat(new object[]
			{
				"SELECT * FROM WFRequestParallelApproval WHERE RequestID=",
				requestId,
				" AND ParallelApprovalEmpNo=",
				parallelApprovalEmpNo
			});
			return base.ExecuteDataRow(CommandType.Text, commandText);
		}

		// Token: 0x0600019A RID: 410 RVA: 0x0000E838 File Offset: 0x0000CA38
		public DataRow GetApprove(int requestId)
		{
			string commandText = "SELECT TOP 1 * FROM WFRequestParallelApproval wpa WHERE  wpa.IsApprove=1 AND wpa.RequestID=" + requestId + " ORDER BY wpa.ChangeDate DESC";
			return base.ExecuteDataRow(CommandType.Text, commandText);
		}

		// Token: 0x0600019B RID: 411 RVA: 0x0000E864 File Offset: 0x0000CA64
		public bool Insert(int requestId, double parallelApprovalEmpNo, bool isApprove, DateTime fromDate, int duration, bool isParallelForCard)
		{
			bool arg_AE_0 = base.ExecuteNonQuery("WF_RequestParallelApprovalInsert", new object[]
			{
				requestId,
				parallelApprovalEmpNo,
				isApprove,
				fromDate,
				duration,
				isParallelForCard
			}) > 0;
			if (isApprove)
			{
				var dataById = WorkFlowsDatabase.Instance.Requests.GetDataById(requestId);
				if (dataById.IsDay && dataById.IsFinalApproved.HasValue && dataById.IsFinalApproved.Value)
				{
					string empty = string.Empty;
					WorkFlowsDatabase.Instance.Requests.SaveToPwKara(dataById, dataById.ApprovalByManagerEmpNo, "RequestParallelApproval.Insert", ref empty, 0);
				}
			}
			return arg_AE_0;
		}
	}
}
