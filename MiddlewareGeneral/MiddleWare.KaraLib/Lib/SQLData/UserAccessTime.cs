﻿using System;
using System.Data;

namespace PW.WorkFlows.SqlDal
{
	// Token: 0x02000030 RID: 48
	public class UserAccessTime : BaseDAL
	{
		// Token: 0x06000245 RID: 581 RVA: 0x00003C40 File Offset: 0x00001E40
		public UserAccessTime(WorkFlowsDatabase Owner) : base(Owner)
		{
		}

		// Token: 0x06000246 RID: 582 RVA: 0x00015D84 File Offset: 0x00013F84
		public int Insert(int AccessTimeGroupID, byte DayNo, bool H1, bool H2, bool H3, bool H4, bool H5, bool H6, bool H7, bool H8, bool H9, bool H10, bool H11, bool H12, bool H13, bool H14, bool H15, bool H16, bool H17, bool H18, bool H19, bool H20, bool H21, bool H22, bool H23, bool H24)
		{
			return Convert.ToInt32(base.ExecuteScalar("WFUserAccessTime_Insert", new object[]
			{
				AccessTimeGroupID,
				DayNo,
				H1,
				H2,
				H3,
				H4,
				H5,
				H6,
				H7,
				H8,
				H9,
				H10,
				H11,
				H12,
				H13,
				H14,
				H15,
				H16,
				H17,
				H18,
				H19,
				H20,
				H21,
				H22,
				H23,
				H24
			}));
		}

		// Token: 0x06000247 RID: 583 RVA: 0x00015EBC File Offset: 0x000140BC
		public DataTable Select(int If_AccessTimeGroupID, out bool IsExist)
		{
			DataTable dataTable = base.ExecuteDataTable("WFUserAccessTime_Select", new object[]
			{
				If_AccessTimeGroupID
			});
			if (dataTable != null && dataTable.Rows != null && dataTable.Rows.Count > 0)
			{
				IsExist = true;
				return dataTable;
			}
			IsExist = false;
			return this.SelectVirtual(If_AccessTimeGroupID);
		}

		// Token: 0x06000248 RID: 584 RVA: 0x00015F0C File Offset: 0x0001410C
		public long Update(int AccessTimeGroupID, byte DayNo, string Hour, bool HourValue)
		{
			return (long)base.ExecuteNonQuery(CommandType.Text, string.Concat(new object[]
			{
				"UPDATE WFUserAccessTime SET ",
				Hour,
				" = ",
				HourValue.ToString(),
				" WHERE  AccessTimeGroupID =",
				AccessTimeGroupID,
				"  AND DayNo = ",
				DayNo
			}));
		}

		// Token: 0x06000249 RID: 585 RVA: 0x00015F6C File Offset: 0x0001416C
		public int Update(int If_AccessTimeGroupID, byte If_DayNo, bool H1, bool H2, bool H3, bool H4, bool H5, bool H6, bool H7, bool H8, bool H9, bool H10, bool H11, bool H12, bool H13, bool H14, bool H15, bool H16, bool H17, bool H18, bool H19, bool H20, bool H21, bool H22, bool H23, bool H24)
		{
			return base.ExecuteNonQuery("WFUserAccessTime_Update", new object[]
			{
				If_AccessTimeGroupID,
				If_DayNo,
				H1,
				H2,
				H3,
				H4,
				H5,
				H6,
				H7,
				H8,
				H9,
				H10,
				H11,
				H12,
				H13,
				H14,
				H15,
				H16,
				H17,
				H18,
				H19,
				H20,
				H21,
				H22,
				H23,
				H24
			});
		}

		// Token: 0x0600024A RID: 586 RVA: 0x000160A0 File Offset: 0x000142A0
		public DataTable SelectVirtual(int AccessTimeGroupID)
		{
			DataTable dataTable = new DataTable();
			dataTable.Columns.Add("AccessTimeGroupID", typeof(int));
			dataTable.Columns.Add("DayNo", typeof(byte));
			dataTable.Columns.Add("H1", typeof(bool));
			dataTable.Columns.Add("H2", typeof(bool));
			dataTable.Columns.Add("H3", typeof(bool));
			dataTable.Columns.Add("H4", typeof(bool));
			dataTable.Columns.Add("H5", typeof(bool));
			dataTable.Columns.Add("H6", typeof(bool));
			dataTable.Columns.Add("H7", typeof(bool));
			dataTable.Columns.Add("H8", typeof(bool));
			dataTable.Columns.Add("H9", typeof(bool));
			dataTable.Columns.Add("H10", typeof(bool));
			dataTable.Columns.Add("H11", typeof(bool));
			dataTable.Columns.Add("H12", typeof(bool));
			dataTable.Columns.Add("H13", typeof(bool));
			dataTable.Columns.Add("H14", typeof(bool));
			dataTable.Columns.Add("H15", typeof(bool));
			dataTable.Columns.Add("H16", typeof(bool));
			dataTable.Columns.Add("H17", typeof(bool));
			dataTable.Columns.Add("H18", typeof(bool));
			dataTable.Columns.Add("H19", typeof(bool));
			dataTable.Columns.Add("H20", typeof(bool));
			dataTable.Columns.Add("H21", typeof(bool));
			dataTable.Columns.Add("H22", typeof(bool));
			dataTable.Columns.Add("H23", typeof(bool));
			dataTable.Columns.Add("H24", typeof(bool));
			for (byte b = 1; b < 8; b += 1)
			{
				DataRow dataRow = dataTable.NewRow();
				dataRow["AccessTimeGroupID"] = AccessTimeGroupID;
				dataRow["DayNo"] = b;
				dataRow["H1"] = true;
				dataRow["H2"] = true;
				dataRow["H3"] = true;
				dataRow["H4"] = true;
				dataRow["H5"] = true;
				dataRow["H6"] = true;
				dataRow["H7"] = true;
				dataRow["H8"] = true;
				dataRow["H9"] = true;
				dataRow["H10"] = true;
				dataRow["H11"] = true;
				dataRow["H12"] = true;
				dataRow["H13"] = true;
				dataRow["H14"] = true;
				dataRow["H15"] = true;
				dataRow["H16"] = true;
				dataRow["H17"] = true;
				dataRow["H18"] = true;
				dataRow["H19"] = true;
				dataRow["H20"] = true;
				dataRow["H21"] = true;
				dataRow["H22"] = true;
				dataRow["H23"] = true;
				dataRow["H24"] = true;
				dataTable.Rows.Add(dataRow);
			}
			return dataTable;
		}
	}
}
