﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using MiddleWare.KaraLib;
using MiddleWare.KaraLib.Lib.SQLData;


namespace PW.WorkFlows.SqlDal
{
	// Token: 0x0200000A RID: 10
	public class Action : BaseDAL
	{
		// Token: 0x0600007A RID: 122 RVA: 0x00003C40 File Offset: 0x00001E40
		public Action(WorkFlowsDatabase owner) : base(owner)
		{
		}

		// Token: 0x0600007B RID: 123 RVA: 0x0000405B File Offset: 0x0000225B
		public DataTable GetAction()
		{
			return base.ExecuteDataTable(CommandType.Text, "SELECT * FROM [Action] a WHERE IsShow=1");
		}
        public List<ActionRow> GetValid(double empNo)
        {
            string commandText = "SELECT a.*  FROM EmpRequestActions AS era \nLEFT OUTER JOIN [Action] AS a ON a.ActionId = era.ActionId \n" + string.Format("WHERE era.EmpNo={0} AND a.IsPermission=1 ", empNo);
            List<ActionRow> list = new List<ActionRow>();
            using (SqlDataReader sqlDataReader = base.ExecuteReader(CommandType.Text, commandText))
            {
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        ActionRow item = new ActionRow(sqlDataReader);
                        list.Add(item);
                    }
                }
            }
            return list;
        }
        // Token: 0x0600007C RID: 124 RVA: 0x00004069 File Offset: 0x00002269
        public DataTable GetActiveAction()
		{
			return base.ExecuteDataTable(CommandType.Text, "SELECT * FROM [Action] a WHERE a.IsPermission=1 and IsShow=1");
		}

		// Token: 0x0600007D RID: 125 RVA: 0x00004078 File Offset: 0x00002278
		public bool IsPermission(ActionEnum actionStatus)
		{
			int num = Convert.ToInt32(actionStatus);
			object obj = base.ExecuteScalar(CommandType.Text, "SELECT TOP 1 a.IsPermission FROM [Action] a WHERE a.ActionId=" + num);
			return obj != null && Convert.ToBoolean(obj);
		}

		// Token: 0x0600007E RID: 126 RVA: 0x000040B4 File Offset: 0x000022B4
		public int GetRequestValidDays(ActionEnum actionStatus)
		{
			int num = Convert.ToInt32(actionStatus);
			object obj = base.ExecuteScalar(CommandType.Text, "SELECT a.RequestValidDays  FROM [Action] a WHERE a.ActionId=" + num);
			if (obj != null)
			{
				return Convert.ToInt32(obj);
			}
			return 31;
		}

		// Token: 0x0600007F RID: 127 RVA: 0x000040F4 File Offset: 0x000022F4
		public int GetLimitCount(ActionEnum actionStatus)
		{
			int result;
			try
			{
				int num = Convert.ToInt32(actionStatus);
				result = Convert.ToInt32(base.ExecuteScalar(CommandType.Text, "SELECT a.LimitCount  FROM [Action] a WHERE a.ActionId=" + num));
			}
			catch
			{
				result = 10;
			}
			return result;
		}

		// Token: 0x06000080 RID: 128 RVA: 0x00004144 File Offset: 0x00002344
		public bool UpdatePermission(int actionId, bool isPermission, int requestValidDays, int limitCount, bool printAccess)
		{
			return base.ExecuteNonQuery(CommandType.Text, string.Concat(new object[]
			{
				"UPDATE [Action] SET IsPermission = ",
				Convert.ToByte(isPermission),
				",RequestValidDays = ",
				requestValidDays,
				",LimitCount=",
				limitCount,
				",PrintAccess=",
				Convert.ToByte(printAccess),
				" WHERE ActionId=",
				actionId
			})) >= 0;
		}

		// Token: 0x06000081 RID: 129 RVA: 0x000041CC File Offset: 0x000023CC
		public bool GetIsPermission(ActionEnum actionStatus, double empNo)
		{
			int num = Convert.ToInt32(actionStatus);
			return Convert.ToBoolean(base.ExecuteScalar("WF_GetIsPermission", new object[]
			{
				num,
				empNo
			}));
		}

		// Token: 0x06000082 RID: 130 RVA: 0x00004210 File Offset: 0x00002410
		public ActionEnum GetActionIdByCard(short cardNo, short cardType, bool isDay)
		{
			switch (cardType)
			{
			case 3:
				if (!isDay)
				{
					return ActionEnum.TimeDuty;
				}
				return ActionEnum.DailyDuty;
			case 4:
				return ActionEnum.TimeDutyNoReturn;
			case 5:
				if (!isDay)
				{
					return ActionEnum.TimeLeave;
				}
				if (cardNo == 59 || cardNo == 70)
				{
					return ActionEnum.DailyLeaveOut;
				}
				return ActionEnum.DailyLeave;
			default:
				switch (cardNo)
				{
				case 100:
					return ActionEnum.ForgottenInOut;
				case 101:
					return ActionEnum.InOutCorrection;
				case 102:
					return ActionEnum.ShiftReplace;
				case 103:
					return ActionEnum.OverTime;
				case 104:
					return ActionEnum.LeaveAddition;
				case 105:
					return ActionEnum.InOutTypeCorrection;
				default:
					return ActionEnum.DailyLeave;
				}
				break;
			}
		}
	}
}
