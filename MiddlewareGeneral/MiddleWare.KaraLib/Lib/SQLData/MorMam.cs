﻿using System;
using System.Data;
using System.Data.SqlClient;
using MiddleWare.KaraLib;


namespace PW.WorkFlows.SqlDal
{
	// Token: 0x02000020 RID: 32
	public class MorMam : BaseDAL
	{
		// Token: 0x06000159 RID: 345 RVA: 0x00003C40 File Offset: 0x00001E40
		public MorMam(WorkFlowsDatabase owner) : base(owner)
		{
		}

		// Token: 0x0600015A RID: 346 RVA: 0x0000AC8C File Offset: 0x00008E8C
		public DataTable MamGetData()
		{
			DataTable dataTable = new DataTable();
			dataTable.Columns.Add("EMP_NO", typeof(double));
			dataTable.Columns.Add("SAVED", typeof(double));
			dataTable.Columns.Add("S_DATE", typeof(DateTime));
			dataTable.Columns.Add("E_DATE", typeof(DateTime));
			dataTable.Columns.Add("REQUESTED", typeof(double));
			dataTable.Columns.Add("S_NO", typeof(string));
			dataTable.Columns.Add("ISU_DATE", typeof(DateTime));
			dataTable.Columns.Add("TYP", typeof(short));
			dataTable.Columns.Add("BABAT", typeof(string));
			dataTable.Columns.Add("INC_TYP", typeof(short));
			dataTable.Columns.Add("PRINTED", typeof(bool));
			dataTable.Columns.Add("P6", typeof(string));
			dataTable.Columns.Add("P7", typeof(string));
			dataTable.Columns.Add("P8", typeof(string));
			dataTable.Columns.Add("MODI_DATE", typeof(DateTime));
			dataTable.Columns.Add("MODI_TIME", typeof(string));
			dataTable.Columns.Add("T_EMP_NO", typeof(double));
			using (SqlDataReader sqlDataReader = base.ExecuteReader("WF_MOR_MAMGetData", null))
			{
				if (sqlDataReader.HasRows)
				{
					while (sqlDataReader.Read())
					{
						DataRow dataRow = dataTable.NewRow();
						dataRow["EMP_NO"] = sqlDataReader["EMP_NO"];
						dataRow["SAVED"] = sqlDataReader["SAVED"];
						dataRow["S_DATE"] = sqlDataReader["S_DATE"];
						dataRow["E_DATE"] = sqlDataReader["E_DATE"];
						dataRow["REQUESTED"] = sqlDataReader["REQUESTED"];
						dataRow["S_NO"] = sqlDataReader["S_NO"];
						dataRow["ISU_DATE"] = sqlDataReader["ISU_DATE"];
						dataRow["TYP"] = sqlDataReader["TYP"];
						dataRow["BABAT"] = sqlDataReader["BABAT"];
						dataRow["INC_TYP"] = sqlDataReader["INC_TYP"];
						dataRow["PRINTED"] = sqlDataReader["PRINTED"];
						dataRow["P6"] = sqlDataReader["P6"];
						dataRow["P7"] = sqlDataReader["P7"];
						dataRow["P8"] = sqlDataReader["P8"];
						dataRow["MODI_DATE"] = sqlDataReader["MODI_DATE"];
						dataRow["MODI_TIME"] = sqlDataReader["MODI_TIME"];
						dataRow["T_EMP_NO"] = sqlDataReader["T_EMP_NO"];
						dataTable.Rows.Add(dataRow);
					}
				}
			}
			return dataTable;
		}

		// Token: 0x0600015B RID: 347 RVA: 0x0000B044 File Offset: 0x00009244
		public DataTable MamGetDataByEmpNo(double fromDate, double toDate, double empNo)
		{
			return base.ExecuteDataTable("WF_MOR_MAMGetDataByEmpNo", new object[]
			{
				fromDate,
				toDate,
				empNo
			});
		}

		// Token: 0x0600015C RID: 348 RVA: 0x0000B074 File Offset: 0x00009274
		public bool Insert(RequestRow reqInfo, string userName, string password, string shamsiDate, bool isLeapYear, ref int Duration)
		{
			DataRow approve = WorkFlowsDatabase.Instance.RequestParallelApproval.GetApprove(reqInfo.RequestsId);
			if (approve == null || approve["RequestDuration"] is DBNull || approve["FromDate"] is DBNull)
			{
				Duration = Convert.ToInt32(reqInfo.Duration);
			}
			else
			{
				int.TryParse(approve["RequestDuration"].ToString(), out Duration);
				reqInfo.StartDate = Convert.ToDateTime(approve["FromDate"]);
				if (Duration == 0)
				{
					if (reqInfo.EndDate.HasValue)
					{
						reqInfo.Duration = (reqInfo.EndDate.Value.Subtract(reqInfo.StartDate).Days + 1).ToString();
					}
				}
				else
				{
					reqInfo.EndDate = new DateTime?(reqInfo.StartDate.AddDays((double)(Duration - 1)));
					reqInfo.Duration = Duration.ToString();
				}
			}
			if (reqInfo.OperationsId == 5 && reqInfo.Type == 57)
			{
				short num = 0;
				if (!WorkFlowsDatabase.Instance.Parmfile.GetDataRow().IsMorDay)
				{
					Duration = WorkFlowsDatabase.Instance.MorTime.GetMorTime(reqInfo.EmpNo, reqInfo.StartDate.ToOADate(), reqInfo.EndDate.Value.ToOADate(), shamsiDate, isLeapYear, ref num);
				}
			}
			return this.Insert(reqInfo.EmpNo, null, (double)Duration, null, new short?(reqInfo.Type), reqInfo.Description, null, userName, password, null, UtilityManagers.ConvertXorToString(DateTime.Now.ToShortTimeString()), null, new short?(0), DateTime.Now.Date.ToOADate(), reqInfo.StartDate.ToOADate(), reqInfo.EndDate.Value.ToOADate(), DateTime.Now.Date.ToOADate());
		}

		// Token: 0x0600015D RID: 349 RVA: 0x0000B290 File Offset: 0x00009490
		public bool Insert(double empNo, double? saved, double requested, string sNo, short? typ, string babat, bool? printed, string p6, string p7, string p8, string modiTime, double? iEmpNo, short? incTyp, double modiDate, double sDate, double eDate, double isuDate)
		{
			if (requested < 0.0)
			{
				return false;
			}
			sDate = Math.Round(sDate, 0);
			eDate = Math.Round(eDate, 0);
			isuDate = Math.Round(isuDate, 0);
			object obj = base.ExecuteScalar("WF_MOR_MAMInsert", new object[]
			{
				empNo,
				saved,
				requested,
				sNo,
				typ,
				babat,
				printed,
				p6,
				p7,
				p8,
				modiTime,
				iEmpNo,
				incTyp,
				modiDate,
				sDate,
				eDate,
				isuDate
			});
			long refNumber;
			if (obj != null && long.TryParse(obj.ToString(), out refNumber))
			{
				WorkFlowsDatabase.Instance.LogMorMam.Insert(empNo.ToString(), isuDate, sDate, eDate, requested.ToString(), typ.ToString(), incTyp.ToString(), babat, refNumber, p6);
				WorkFlowsDatabase.Instance.Employee.UpdateKVCurrentMonth(empNo, sDate, eDate);
				return true;
			}
			return false;
		}

		// Token: 0x0600015E RID: 350 RVA: 0x0000B3D9 File Offset: 0x000095D9
		public DataTable GetRemedialDataByEmpNo(double empNo, DateTime startDate, DateTime endDate)
		{
			return base.ExecuteDataTable("WF_MorMamGetRemedialDataByEmpNo", new object[]
			{
				empNo,
				startDate,
				endDate
			});
		}

		// Token: 0x0600015F RID: 351 RVA: 0x0000B407 File Offset: 0x00009607
		public DataTable GetDataByM_M_Date(double empNo, double date)
		{
			return base.ExecuteDataTable("WF_MorMamGetDataByM_M_Date", new object[]
			{
				empNo,
				date
			});
		}

		// Token: 0x06000160 RID: 352 RVA: 0x0000B42C File Offset: 0x0000962C
		public DataTable GetDataByM_M_Date_Main(double empNo, double date)
		{
			return base.ExecuteDataTable("WF_MorMamGetDataByM_M_Date_Main", new object[]
			{
				empNo,
				date
			});
		}

		// Token: 0x06000161 RID: 353 RVA: 0x0000B454 File Offset: 0x00009654
		public double UsedMorMam(double empNo, int typ, double sDate, double eDate)
		{
			object obj = base.ExecuteScalar("UsedMorMam", new object[]
			{
				empNo,
				typ,
				sDate,
				eDate
			});
			if (!(obj.ToString() == ""))
			{
				return Convert.ToDouble(obj);
			}
			return 0.0;
		}

		// Token: 0x06000162 RID: 354 RVA: 0x0000B4BC File Offset: 0x000096BC
		public DataTable GetRemainKardex(double empNo)
		{
			DataTable dataTable = new DataTable();
			dataTable.Columns.Add("RemainKardex", typeof(int));
			dataTable.Columns.Add("IS_MOR_DAY", typeof(int));
			using (SqlDataReader sqlDataReader = base.ExecuteReader("GetRemainKardex", new object[]
			{
				empNo
			}))
			{
				if (sqlDataReader.HasRows)
				{
					while (sqlDataReader.Read())
					{
						DataRow dataRow = dataTable.NewRow();
						dataRow["RemainKardex"] = sqlDataReader["RemainKardex"];
						dataRow["IS_MOR_DAY"] = sqlDataReader["IS_MOR_DAY"];
						dataTable.Rows.Add(dataRow);
					}
				}
			}
			return dataTable;
		}

		// Token: 0x06000163 RID: 355 RVA: 0x0000B590 File Offset: 0x00009790
		public string GetRemainKardexFormat(double empNo, ParmfileRow pfr)
		{
			string text = "";
			DataTable remainKardex = WorkFlowsDatabase.Instance.MorMam.GetRemainKardex(empNo);
			if (remainKardex != null && remainKardex.Rows.Count > 0)
			{
				string text2 = remainKardex.Rows[0]["RemainKardex"].ToString();
				if (Convert.ToBoolean(remainKardex.Rows[0]["IS_MOR_DAY"]))
				{
					text = "مانده : " + text2 + " روز";
				}
				else
				{
					try
					{
						bool flag = false;
						if (text2.Contains("-"))
						{
							text2 = text2.Replace("-", "");
							flag = true;
						}
						text2 = UtilityManagers.ConvertToFourDigitNumber(text2);
						int num;
						int.TryParse(text2.Substring(0, text2.Length - 2), out num);
						int num2;
						int.TryParse(text2.Substring(text2.Length - 2, 2), out num2);
						int num3 = WorkFlowsDatabase.Instance.Groups.GetDayTime(empNo, DateTime.Now, pfr);
						string str = " (هر روز " + UtilityManagers.ConvertToTimeFormat(num3.ToString()) + ")";
						num3 = ShDate.Time2Min(num3);
						int num4 = num * 60 + num2;
						if (num3 == num4)
						{
							text += string.Format("مانده مرخص : {0} معادل {1} روز و {2}:{3}", new object[]
							{
								UtilityManagers.ConvertToTimeFormat(text2),
								1,
								num,
								num2
							});
						}
						else if (num3 > num4)
						{
							text = string.Concat(new object[]
							{
								text,
								"مانده مرخص : ",
								num,
								":",
								num2
							});
						}
						else if (num3 < num4)
						{
							int num5 = num4 / num3;
							int expr_1B3 = num4 % num3;
							num = expr_1B3 / 60;
							num2 = expr_1B3 % 60;
							text += string.Format("مانده مرخص : {0} معادل {1} روز و {2}:{3}", new object[]
							{
								UtilityManagers.ConvertToTimeFormat(text2),
								num5,
								num,
								num2
							});
						}
						if (flag)
						{
							text += " منفی ";
						}
						text += str;
					}
					catch
					{
						text = "مانده مرخص نامشخص می باشد";
					}
				}
			}
			return text;
		}

		// Token: 0x06000164 RID: 356 RVA: 0x0000B7DC File Offset: 0x000099DC
		public bool IsAccessWithReamin(double empNo, int requestDuration, bool isTimeLeave, ref string limitDay, ParmfileRow drParmfile, out string sumRequestDuration)
		{
			sumRequestDuration = string.Empty;
			if (!drParmfile.WfRequestMandeControl)
			{
				return true;
			}
			int num = ShDate.Time2Min(WorkFlowsDatabase.Instance.Groups.GetDayTime(empNo, DateTime.Now, drParmfile));
			bool result = false;
			int num2 = 0;
			int num3;
			if (isTimeLeave)
			{
				num3 = WorkFlowsDatabase.Instance.Requests.GetSumDailyLeaveDurationByEmpNo(empNo) * num;
				string text = UtilityManagers.ConvertToFourDigitNumber(requestDuration.ToString());
				int num4;
				int.TryParse(text.Substring(0, text.Length - 2), out num4);
				int num5;
				int.TryParse(text.Substring(text.Length - 2, 2), out num5);
				num3 += WorkFlowsDatabase.Instance.Requests.GetSumTimeLeaveDurationByEmpNo(empNo) + (num4 * 60 + num5);
			}
			else
			{
				num3 = (WorkFlowsDatabase.Instance.Requests.GetSumDailyLeaveDurationByEmpNo(empNo) + requestDuration) * num;
				num3 += WorkFlowsDatabase.Instance.Requests.GetSumTimeLeaveDurationByEmpNo(empNo);
			}
			sumRequestDuration = UtilityManagers.ConvertMinToTimeFormat(num3, num);
			DataTable remainKardex = WorkFlowsDatabase.Instance.MorMam.GetRemainKardex(empNo);
			if (remainKardex != null && remainKardex.Rows.Count > 0)
			{
				int num6 = Convert.ToInt32(remainKardex.Rows[0]["RemainKardex"].ToString().Replace(":", ""));
				long num7;
				if (Convert.ToBoolean(remainKardex.Rows[0]["IS_MOR_DAY"]))
				{
					num7 = (long)(Convert.ToInt32(num6) * num);
				}
				else
				{
					int num8 = num6 / 100;
					long num9 = (long)(num6 % 100);
					if (num8 < 0)
					{
						num9 *= -1L;
						num7 = (long)(num8 * 60) + num9;
					}
					else
					{
						num7 = (long)(num8 * 60) + num9;
					}
				}
				long num10 = num7 - (long)num3;
				long num11 = num10 % (long)num;
				limitDay = string.Concat(new object[]
				{
					num10 / (long)num,
					" روز ",
					num11 / 60L,
					" ساعت ",
					num11 % 60L,
					" دقیقه "
				});
				result = (num7 - (long)num3 >= (long)num2);
			}
			return result;
		}

		// Token: 0x06000165 RID: 357 RVA: 0x0000B9EA File Offset: 0x00009BEA
		public int IsExist(double startDate, double endDate, double empNo)
		{
			return Convert.ToInt32(base.ExecuteScalar("WF_MorMamIsExist", new object[]
			{
				startDate,
				endDate,
				empNo
			}));
		}

		// Token: 0x06000166 RID: 358 RVA: 0x0000BA20 File Offset: 0x00009C20
		public DataRow Delete(double? empNo, short? typ, string p7, double sDate, double eDate)
		{
			sDate = Math.Round(sDate, 0);
			eDate = Math.Round(eDate, 0);
			DataRow expr_55 = base.ExecuteDataRow("WF_MOR_MAMDelete", new object[]
			{
				empNo.Value,
				sDate,
				eDate,
				typ,
				p7
			});
			if (expr_55 != null)
			{
				WorkFlowsDatabase.Instance.Employee.UpdateKVCurrentMonth(empNo.Value, sDate, eDate);
			}
			return expr_55;
		}

		// Token: 0x06000167 RID: 359 RVA: 0x0000BA9F File Offset: 0x00009C9F
		public DataRow GetByRefNumber(long refNumber)
		{
			return base.ExecuteDataRow(CommandType.Text, "SELECT * FROM MOR_MAM mm WHERE mm.RefNumber=" + refNumber);
		}

		// Token: 0x06000168 RID: 360 RVA: 0x0000BAB8 File Offset: 0x00009CB8
		public DataRow SumTimeRequestedInYear(double empNo)
		{
			DataRow result;
			try
			{
				double num = ShDate.FirstDayOfYear(DateTime.Now).ToOADate();
				string commandText = string.Format("SELECT dbo.Min2Time(SUM(dbo.Time2Min(mm.REQUESTED))) sumTime,count(1) cnt FROM MOR_MAM mm WHERE mm.TYP=57 AND mm.INC_TYP=2 AND mm.EMP_NO={0} AND mm.S_DATE >{1}", empNo, num);
				result = base.ExecuteDataRow(CommandType.Text, commandText);
			}
			catch
			{
				result = null;
			}
			return result;
		}

		// Token: 0x06000169 RID: 361 RVA: 0x0000BB10 File Offset: 0x00009D10
		public DataRow SumOtherMorMamInYear(double empNo)
		{
			DataRow result;
			try
			{
				double num = ShDate.FirstDayOfYear(DateTime.Now).ToOADate();
				string commandText = string.Format("SELECT dbo.Min2Time(SUM(dbo.Time2Min(mm.REQUESTED))) sumOther,count(1) cnt FROM MOR_MAM mm WHERE  mm.INC_TYP<=5 AND mm.INC_TYP<>2 AND mm.E_DATE<>0 AND mm.EMP_NO={0} AND mm.S_DATE >{1}", empNo, num);
				result = base.ExecuteDataRow(CommandType.Text, commandText);
			}
			catch
			{
				result = null;
			}
			return result;
		}
	}
}
