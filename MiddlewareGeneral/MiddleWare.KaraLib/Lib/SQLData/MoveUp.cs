﻿using System;
using System.Data;

namespace PW.WorkFlows.SqlDal
{
	// Token: 0x02000022 RID: 34
	public class MoveUp : BaseDAL
	{
		// Token: 0x0600016D RID: 365 RVA: 0x00003C40 File Offset: 0x00001E40
		public MoveUp(WorkFlowsDatabase owner) : base(owner)
		{
		}

		// Token: 0x0600016E RID: 366 RVA: 0x0000BC4C File Offset: 0x00009E4C
		public bool Insert(int requestId, double fromMgrNo, double toMgrNo, int cardNo, double fromSecNo, double toSecNo)
		{
			string commandText = string.Concat(new object[]
			{
				"INSERT INTO MoveUp(RequestID,FromMgrNo,ToMgrNo,CardNo,FromSecNo,ToSecNo) VALUES(",
				requestId,
				",",
				fromMgrNo,
				",",
				toMgrNo,
				",",
				cardNo,
				",",
				fromSecNo,
				",",
				toSecNo,
				")"
			});
			return base.ExecuteNonQuery(CommandType.Text, commandText) >= 0;
		}

		// Token: 0x0600016F RID: 367 RVA: 0x0000BCEC File Offset: 0x00009EEC
		public DataTable GetByManager(double fromMgrNo)
		{
			string commandText = "SELECT * FROM MoveUp mu WHERE mu.FromMgrNo=" + fromMgrNo;
			return base.ExecuteDataTable(CommandType.Text, commandText);
		}

		// Token: 0x06000170 RID: 368 RVA: 0x0000BD12 File Offset: 0x00009F12
		public DataTable GetFlows(int requestID)
		{
			return base.ExecuteDataTable("WF_RequestGetFlow", new object[]
			{
				requestID
			});
		}
	}
}
