﻿using System;
using System.Data;
using MiddleWare.KaraLib;


namespace PW.WorkFlows.SqlDal
{
	// Token: 0x02000021 RID: 33
	public class MorTime : BaseDAL
	{
		// Token: 0x0600016A RID: 362 RVA: 0x00003C40 File Offset: 0x00001E40
		public MorTime(WorkFlowsDatabase owner) : base(owner)
		{
		}

		// Token: 0x0600016B RID: 363 RVA: 0x0000BB68 File Offset: 0x00009D68
		public int GetMorTime(double empNo, double dateS, double dateE, string shamsiDate, bool isKabise, ref short grp_no)
		{
			DataRow dataRow = base.ExecuteDataRow("MorTime", new object[]
			{
				empNo,
				dateS,
				dateE,
				shamsiDate,
				isKabise
			});
			if (dataRow != null && dataRow["SumMorTime"].ToString() != "")
			{
				grp_no = Convert.ToInt16(dataRow["Grp_No"]);
				return Convert.ToInt32(dataRow["SumMorTime"]);
			}
			grp_no = -1;
			return -1;
		}

		// Token: 0x0600016C RID: 364 RVA: 0x0000BBFC File Offset: 0x00009DFC
		public DataTable GetShiftIsHoliday(double empNo, double dateS, double dateE, string shamsiDate, bool isKabise)
		{
			return base.ExecuteDataTable("WF_GetShiftIsHoliday", new object[]
			{
				empNo,
				dateS,
				dateE,
				ShDate.RemoveDateSeparator(shamsiDate),
				isKabise
			});
		}
	}
}
