﻿using System;
using System.Data.SqlClient;
using MiddleWare.KaraLib;


namespace PW.WorkFlows.SqlDal
{
	// Token: 0x02000026 RID: 38
	public class Parmfile : BaseDAL
	{
		// Token: 0x06000187 RID: 391 RVA: 0x00003C40 File Offset: 0x00001E40
		public Parmfile(WorkFlowsDatabase owner) : base(owner)
		{
		}

		// Token: 0x06000188 RID: 392 RVA: 0x0000C430 File Offset: 0x0000A630
		public ParmfileRow GetDataRow()
		{
			ParmfileRow result = null;
			using (SqlDataReader sqlDataReader = base.ExecuteReader("WF_PARMFILEGetData", null))
			{
				if (sqlDataReader.Read())
				{
					result = new ParmfileRow
					{
						MyEste = Convert.ToInt32((sqlDataReader["M_Y_ESTE"] is DBNull || sqlDataReader["M_Y_ESTE"] == null) ? 0 : sqlDataReader["M_Y_ESTE"]),
						EmpEste = Convert.ToInt16((sqlDataReader["EMP_ESTE"] is DBNull || sqlDataReader["EMP_ESTE"] == null) ? 0 : sqlDataReader["EMP_ESTE"]),
						IsMorDay = Convert.ToBoolean(sqlDataReader["IS_MOR_DAY"]),
						CodePlace = Convert.ToInt64((sqlDataReader["CodePlace"] is DBNull || sqlDataReader["CodePlace"] == null) ? 0 : sqlDataReader["CodePlace"]),
						StartDay = Convert.ToInt16((sqlDataReader["START_DAY"] is DBNull || sqlDataReader["START_DAY"] == null) ? 0 : sqlDataReader["START_DAY"]),
						Domain = Convert.ToString(sqlDataReader["Domain"]),
						DomainFieldAdmin = Convert.ToString(sqlDataReader["DomainFieldAdmin"]),
						DomainFieldEmpNo = Convert.ToString(sqlDataReader["DomainFieldEmpNO"]),
						Tolerance = Convert.ToInt32((sqlDataReader["Tolerance"] is DBNull || sqlDataReader["Tolerance"] == null) ? 0 : sqlDataReader["Tolerance"]),
						WfRequestMandeControl = Convert.ToBoolean(sqlDataReader["WFRequestMandeControl"]),
						KasrEsth = Convert.ToInt16((sqlDataReader["KASR_ESTH"] is DBNull || sqlDataReader["KASR_ESTH"] == null) ? 0 : sqlDataReader["KASR_ESTH"]),
						EndDateRamedan = Convert.ToSingle((sqlDataReader["EndDateRamedan"] is DBNull || sqlDataReader["EndDateRamedan"] == null) ? 0 : sqlDataReader["EndDateRamedan"]),
						StartDateRamedan = Convert.ToSingle((sqlDataReader["StartDateRamedan"] is DBNull || sqlDataReader["StartDateRamedan"] == null) ? 0 : sqlDataReader["StartDateRamedan"]),
						MorDebt = Convert.ToInt16((sqlDataReader["MOR_DEBT"] is DBNull || sqlDataReader["MOR_DEBT"] == null) ? 0 : sqlDataReader["MOR_DEBT"]),
						DifKasrEsth = Convert.ToBoolean(sqlDataReader["difKasrEsth"]),
						DayArchive = Convert.ToInt32((sqlDataReader["DayArchive"] is DBNull || sqlDataReader["DayArchive"] == null) ? 0 : sqlDataReader["DayArchive"]),
						WfIsAttachment = Convert.ToBoolean(sqlDataReader["WFIsAttachment"]),
						DayArchiveKind = Convert.ToBoolean(sqlDataReader["DayArchiveKind"]),
						WfParallelApprove = Convert.ToBoolean((sqlDataReader["WF_ParallelApprove"] is DBNull || sqlDataReader["WF_ParallelApprove"] == null) ? 0 : sqlDataReader["WF_ParallelApprove"]),
						InvalidMonthToEdit = Convert.ToInt32((sqlDataReader["InvalidMonthToEdit"] is DBNull || sqlDataReader["InvalidMonthToEdit"] == null) ? 0 : sqlDataReader["InvalidMonthToEdit"]),
						MmDate = (double)Convert.ToInt32((sqlDataReader["M_M_DATE"] is DBNull || sqlDataReader["M_M_DATE"] == null) ? 0 : sqlDataReader["M_M_DATE"]),
						WfLimitMonthShowDaily = Convert.ToInt16((sqlDataReader["WFLimitMonthShowDaily"] is DBNull || sqlDataReader["WFLimitMonthShowDaily"] == null) ? 0 : sqlDataReader["WFLimitMonthShowDaily"]),
						WfIsChangeUserName = Convert.ToBoolean(sqlDataReader["WFIsChangeUserName"]),
						WfDifrenceRequest = Convert.ToBoolean(sqlDataReader["WFDifrenceRequest"]),
						CnoEqPno = Convert.ToBoolean(sqlDataReader["CNO_EQ_PNO"]),
						WfNeedDesc = Convert.ToBoolean(sqlDataReader["WFNeedDesc"]),
						RegisterTolerance = Convert.ToInt16((sqlDataReader["RegisterTolerance"] is DBNull || sqlDataReader["RegisterTolerance"] == null) ? 0 : sqlDataReader["RegisterTolerance"]),
						ShowRemainKardex = Convert.ToBoolean(sqlDataReader["ShowRemainKardex"])
					};
				}
			}
			return result;
		}

		// Token: 0x06000189 RID: 393 RVA: 0x0000C938 File Offset: 0x0000AB38
		public int Update(string domain, string domainFieldEmpNo, string domainFieldAdmin, int dayArchive, int tolerance, bool wfRequestMandeControl, bool wfIsAttachment, bool dayArchiveKind, bool wfParallelApprove, short wfLimitMonthShowDaily, bool wfIsChangeUserName, bool wfDifrenceRequest, bool wfNeedDesc, short registerTolerance, bool showRemainKardex)
		{
			return base.ExecuteNonQuery("WF_UpdatePARMFILE", new object[]
			{
				domain,
				domainFieldEmpNo,
				domainFieldAdmin,
				dayArchive,
				tolerance,
				wfRequestMandeControl,
				wfIsAttachment,
				dayArchiveKind,
				wfParallelApprove,
				wfLimitMonthShowDaily,
				wfIsChangeUserName,
				wfDifrenceRequest,
				wfNeedDesc,
				registerTolerance,
				showRemainKardex
			});
		}
	}
}
