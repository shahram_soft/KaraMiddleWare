﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using MiddleWare.KaraLib;
using MiddleWare.KaraLib.ViewModels;


namespace PW.WorkFlows.SqlDal
{
    // Token: 0x02000028 RID: 40
    public class Reports : BaseDAL
    {
        // Token: 0x0600018C RID: 396 RVA: 0x00003C40 File Offset: 0x00001E40
        public Reports(WorkFlowsDatabase owner) : base(owner)
        {
        }

        // Token: 0x0600018D RID: 397 RVA: 0x0000C9F0 File Offset: 0x0000ABF0
        public List<TurnoverManagerRow> TurnoverRequestInMonth(string fdate, string tdate, string employee,
            int[] operations)
        {
            List<TurnoverManagerRow> list = new List<TurnoverManagerRow>();
            using (SqlDataReader sqlDataReader = base.ExecuteReader("WF_Rep_TurnoverRequestInMonth", new object[]
            {
                fdate,
                tdate,
                string.IsNullOrEmpty(employee) ? "" : employee
            }))
            {
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        list.Add(new TurnoverManagerRow
                        {
                            EmpNO =
                                Convert.ToInt32((sqlDataReader["EmpNO"] is DBNull || sqlDataReader["EmpNO"] == null)
                                    ? 0
                                    : sqlDataReader["EmpNO"]),
                            EmpName = Convert.ToString(sqlDataReader["EmpName"]),
                            YD =
                                Convert.ToInt32((sqlDataReader["YD"] is DBNull || sqlDataReader["YD"] == null)
                                    ? 0
                                    : sqlDataReader["YD"]),
                            MD =
                                Convert.ToInt16((sqlDataReader["MD"] is DBNull || sqlDataReader["MD"] == null)
                                    ? 0
                                    : sqlDataReader["MD"]),
                            CountReq =
                                Convert.ToInt32((sqlDataReader["CountReq"] is DBNull ||
                                                 sqlDataReader["CountReq"] == null)
                                    ? 0
                                    : sqlDataReader["CountReq"]),
                            OperationsID =
                                (int)
                                    Convert.ToInt16((sqlDataReader["OperationsID"] is DBNull ||
                                                     sqlDataReader["OperationsID"] == null)
                                        ? 0
                                        : sqlDataReader["OperationsID"])
                        });
                    }
                }
            }
            return (from dt1 in list
                where operations.Contains(dt1.OperationsID)
                select dt1).ToList<TurnoverManagerRow>();
        }

        // Token: 0x0600018E RID: 398 RVA: 0x0000CBEC File Offset: 0x0000ADEC
        public List<TurnoverManagerRow> TurnoverRequestInWeek(DateTime fdate, DateTime tdate, string employee,
            int[] operations)
        {
            List<TurnoverManagerRow> list = new List<TurnoverManagerRow>();
            using (SqlDataReader sqlDataReader = base.ExecuteReader("WF_Rep_TurnoverRequestInWeek", new object[]
            {
                fdate,
                tdate,
                string.IsNullOrEmpty(employee) ? "" : employee
            }))
            {
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        TurnoverManagerRow turnoverManagerRow = new TurnoverManagerRow();
                        turnoverManagerRow.EmpNO =
                            Convert.ToInt32((sqlDataReader["EmpNO"] is DBNull || sqlDataReader["EmpNO"] == null)
                                ? 0
                                : sqlDataReader["EmpNO"]);
                        turnoverManagerRow.EmpName = Convert.ToString(sqlDataReader["EmpName"]);
                        turnoverManagerRow.YD =
                            Convert.ToInt32((sqlDataReader["YD"] is DBNull || sqlDataReader["YD"] == null)
                                ? 0
                                : sqlDataReader["YD"]);
                        turnoverManagerRow.WD =
                            Convert.ToInt16((sqlDataReader["WD"] is DBNull || sqlDataReader["WD"] == null)
                                ? 0
                                : sqlDataReader["WD"]);
                        turnoverManagerRow.CountReq =
                            Convert.ToInt32((sqlDataReader["CountReq"] is DBNull || sqlDataReader["CountReq"] == null)
                                ? 0
                                : sqlDataReader["CountReq"]);
                        turnoverManagerRow.OperationsID =
                            (int)
                                Convert.ToInt16((sqlDataReader["OperationsID"] is DBNull ||
                                                 sqlDataReader["OperationsID"] == null)
                                    ? 0
                                    : sqlDataReader["OperationsID"]);
                        DateTime vDateTime =
                            new DateTime(turnoverManagerRow.YD, 1, 1).AddDays((double) (turnoverManagerRow.WD*7));
                        turnoverManagerRow.WD = (short) ShDate.DateToWeekNo(vDateTime);
                        turnoverManagerRow.WDName = string.Concat(new object[]
                        {
                            "هفته ",
                            (short) ShDate.DateToWeekNo(vDateTime),
                            " سال ",
                            ShDate.YearOf(vDateTime),
                            "\n  ",
                            ShDate.MonthName(ShDate.MonthOf(vDateTime)),
                            " ماه"
                        });
                        list.Add(turnoverManagerRow);
                    }
                }
            }
            return (from dt1 in list
                where operations.Contains(dt1.OperationsID)
                select dt1).ToList<TurnoverManagerRow>();
        }

        // Token: 0x0600018F RID: 399 RVA: 0x0000CE80 File Offset: 0x0000B080
        public List<TurnoverEmgrRow> GetTurnoverEmgr(DateTime fromDate, DateTime toDate, string operationsId,
            string managerNo)
        {
            List<TurnoverEmgrRow> list = new List<TurnoverEmgrRow>();
            List<TurnoverEmgrRow> result;
            using (SqlDataReader sqlDataReader = base.ExecuteReader("WF_GetTurnoverEmgr", new object[]
            {
                fromDate,
                toDate,
                managerNo,
                operationsId
            }))
            {
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        TurnoverEmgrRow turnoverEmgrRow = new TurnoverEmgrRow();
                        turnoverEmgrRow.CurEmp_NO =
                            Convert.ToDouble((sqlDataReader["CurEmp_NO"] is DBNull || sqlDataReader["CurEmp_NO"] == null)
                                ? 0
                                : sqlDataReader["CurEmp_NO"]);
                        turnoverEmgrRow.ManagerName = Convert.ToString(sqlDataReader["ManagerName"]);
                        turnoverEmgrRow.InKartablCount =
                            Convert.ToInt32((sqlDataReader["InKartablCount"] is DBNull ||
                                             sqlDataReader["InKartablCount"] == null)
                                ? 0
                                : sqlDataReader["InKartablCount"]);
                        turnoverEmgrRow.AcceptCount =
                            Convert.ToInt32((sqlDataReader["AcceptCount"] is DBNull ||
                                             sqlDataReader["AcceptCount"] == null)
                                ? 0
                                : sqlDataReader["AcceptCount"]);
                        turnoverEmgrRow.RejectCount =
                            Convert.ToInt32((sqlDataReader["RejectCount"] is DBNull ||
                                             sqlDataReader["RejectCount"] == null)
                                ? 0
                                : sqlDataReader["RejectCount"]);
                        turnoverEmgrRow.InKartablCountRev =
                            Convert.ToInt32((sqlDataReader["InKartablCountRev"] is DBNull ||
                                             sqlDataReader["InKartablCountRev"] == null)
                                ? 0
                                : sqlDataReader["InKartablCountRev"]);
                        turnoverEmgrRow.AcceptCountRev =
                            Convert.ToInt32((sqlDataReader["AcceptCountRev"] is DBNull ||
                                             sqlDataReader["AcceptCountRev"] == null)
                                ? 0
                                : sqlDataReader["AcceptCountRev"]);
                        turnoverEmgrRow.RejectCountRev =
                            Convert.ToInt32((sqlDataReader["RejectCountRev"] is DBNull ||
                                             sqlDataReader["RejectCountRev"] == null)
                                ? 0
                                : sqlDataReader["RejectCountRev"]);
                        turnoverEmgrRow.MoveUpCount =
                            Convert.ToInt32((sqlDataReader["MoveUpCount"] is DBNull ||
                                             sqlDataReader["MoveUpCount"] == null)
                                ? 0
                                : sqlDataReader["MoveUpCount"]);
                        turnoverEmgrRow.TotalRequest = turnoverEmgrRow.InKartablCount + turnoverEmgrRow.AcceptCount +
                                                       turnoverEmgrRow.RejectCount + turnoverEmgrRow.MoveUpCount;
                        list.Add(turnoverEmgrRow);
                    }
                }
                result = list;
            }
            return result;
        }

        // Token: 0x06000190 RID: 400 RVA: 0x0000D134 File Offset: 0x0000B334
        public List<TurnoverEmgrRow> GetTurnoverManager(DateTime fromDate, DateTime toDate, string operationsId,
            string managerNo, string managersName)
        {
            DataSet dataSet = base.ExecuteDataset("WF_GetTurnoverManager", new object[]
            {
                fromDate,
                toDate,
                managerNo,
                operationsId
            });
            DataTable arg_60_0 = dataSet.Tables[0].DefaultView.ToTable(true, new string[]
            {
                "CurEmp_NO",
                "fullName"
            });
            List<TurnoverEmgrRow> list = new List<TurnoverEmgrRow>();
            foreach (DataRow dataRow in arg_60_0.Rows)
            {
                TurnoverEmgrRow turnoverEmgrRow = new TurnoverEmgrRow
                {
                    CurEmp_NO = Convert.ToDouble(dataRow["CurEmp_NO"]),
                    ManagerName = dataRow["fullName"].ToString()
                };
                DataRow[] array =
                    dataSet.Tables[0].Select(string.Format("CurEmp_NO={0} and IsFinalApproved Is null",
                        turnoverEmgrRow.CurEmp_NO));
                turnoverEmgrRow.InKartablCount = ((array.Length != 0) ? Convert.ToInt32(array[0]["cnt"]) : 0);
                array =
                    dataSet.Tables[0].Select(string.Format("CurEmp_NO={0} and IsFinalApproved = True",
                        turnoverEmgrRow.CurEmp_NO));
                turnoverEmgrRow.AcceptCount = ((array.Length != 0) ? Convert.ToInt32(array[0]["cnt"]) : 0);
                array =
                    dataSet.Tables[0].Select(string.Format("CurEmp_NO={0} and IsFinalApproved = False",
                        turnoverEmgrRow.CurEmp_NO));
                turnoverEmgrRow.RejectCount = ((array.Length != 0) ? Convert.ToInt32(array[0]["cnt"]) : 0);
                array =
                    dataSet.Tables[0].Select(string.Format("CurEmp_NO={0} and IsFinalApproved Is null",
                        turnoverEmgrRow.CurEmp_NO));
                turnoverEmgrRow.InKartablCountRev = ((array.Length != 0) ? Convert.ToInt32(array[0]["cntrev"]) : 0);
                array =
                    dataSet.Tables[0].Select(string.Format("CurEmp_NO={0} and IsFinalApproved = True",
                        turnoverEmgrRow.CurEmp_NO));
                turnoverEmgrRow.AcceptCountRev = ((array.Length != 0) ? Convert.ToInt32(array[0]["cntrev"]) : 0);
                array =
                    dataSet.Tables[0].Select(string.Format("CurEmp_NO={0} and IsFinalApproved = False",
                        turnoverEmgrRow.CurEmp_NO));
                turnoverEmgrRow.RejectCountRev = ((array.Length != 0) ? Convert.ToInt32(array[0]["cntrev"]) : 0);
                array = dataSet.Tables[1].Select(string.Format("CurEmp_NO={0}", turnoverEmgrRow.CurEmp_NO));
                turnoverEmgrRow.MoveUpCount = ((array.Length != 0) ? Convert.ToInt32(array[0]["cnt"]) : 0);
                turnoverEmgrRow.TotalRequest = turnoverEmgrRow.InKartablCount + turnoverEmgrRow.AcceptCount +
                                               turnoverEmgrRow.RejectCount + turnoverEmgrRow.MoveUpCount;
                list.Add(turnoverEmgrRow);
            }
            return list;
        }

        // Token: 0x06000191 RID: 401 RVA: 0x0000D46C File Offset: 0x0000B66C
        public List<TurnoverEmgrRow> GetTurnoverEmgr()
        {
            return this.GetTurnoverEmgr(DateTime.Now.AddMonths(-6), DateTime.Now, "1,2,3,4,5,6", "1");
        }

        // Token: 0x06000192 RID: 402 RVA: 0x0000D4A0 File Offset: 0x0000B6A0
        public List<RequestByEmpRow> GetRequestByEmp(DateTime fromDate, DateTime toDate, List<double> employee)
        {
            List<RequestByEmpRow> list = new List<RequestByEmpRow>();
            List<RequestByEmpRow> result;
            using (SqlDataReader sqlDataReader = base.ExecuteReader("WF_Rep_RequestByEmp", new object[]
            {
                fromDate,
                toDate
            }))
            {
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        RequestByEmpRow requestByEmpRow = new RequestByEmpRow();
                        requestByEmpRow.EmpNo =
                            Convert.ToDouble((sqlDataReader["EMP_NO"] is DBNull || sqlDataReader["EMP_NO"] == null)
                                ? 0
                                : sqlDataReader["EMP_NO"]);
                        requestByEmpRow.OverTimr =
                            Convert.ToInt32((sqlDataReader["OverTimr"] is DBNull || sqlDataReader["OverTimr"] == null)
                                ? 0
                                : sqlDataReader["OverTimr"]);
                        requestByEmpRow.Taradod =
                            Convert.ToInt32((sqlDataReader["Taradod"] is DBNull || sqlDataReader["Taradod"] == null)
                                ? 0
                                : sqlDataReader["Taradod"]);
                        requestByEmpRow.Duty =
                            Convert.ToInt32((sqlDataReader["Duty"] is DBNull || sqlDataReader["Duty"] == null)
                                ? 0
                                : sqlDataReader["Duty"]);
                        requestByEmpRow.Leave =
                            Convert.ToInt32((sqlDataReader["Leave"] is DBNull || sqlDataReader["Leave"] == null)
                                ? 0
                                : sqlDataReader["Leave"]);
                        requestByEmpRow.Name = Convert.ToString(sqlDataReader["NAME"]);
                        requestByEmpRow.Family = Convert.ToString(sqlDataReader["FAMILY"]);
                        requestByEmpRow.SecNo =
                            Convert.ToDouble((sqlDataReader["SEC_NO"] is DBNull || sqlDataReader["SEC_NO"] == null)
                                ? 0
                                : sqlDataReader["SEC_NO"]);
                        requestByEmpRow.Title = Convert.ToString(sqlDataReader["TITLE"]);
                        requestByEmpRow.TotalCount = requestByEmpRow.Taradod + requestByEmpRow.Duty +
                                                     requestByEmpRow.Leave + requestByEmpRow.OverTimr;
                        list.Add(requestByEmpRow);
                    }
                }
                result = ((employee.Count > 0)
                    ? (from q2 in list
                        where employee.Contains(q2.EmpNo)
                        select q2).ToList<RequestByEmpRow>()
                    : list);
            }
            return result;
        }

        // Token: 0x06000193 RID: 403 RVA: 0x0000D734 File Offset: 0x0000B934
        public List<RequestReport> GetRequest(DateTime fromDate, DateTime toDate, int type)
        {
            List<RequestReport> list = new List<RequestReport>();
            List<RequestReport> result;
            using (SqlDataReader sqlDataReader = base.ExecuteReader("WF_Rep_Request", new object[]
            {
                fromDate,
                toDate,
                type
            }))
            {
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        RequestReport requestReport = new RequestReport();
                        requestReport.OperationsId =
                            Convert.ToInt32((sqlDataReader["OperationsId"] is DBNull ||
                                             sqlDataReader["OperationsId"] == null)
                                ? 0
                                : sqlDataReader["OperationsId"]);
                        requestReport.StartDate = Convert.ToDateTime(sqlDataReader["StartDate"]);
                        requestReport.EmpNo =
                            Convert.ToDouble((sqlDataReader["EMP_NO"] is DBNull || sqlDataReader["EMP_NO"] == null)
                                ? 0
                                : sqlDataReader["EMP_NO"]);
                        requestReport.PersNo =
                            Convert.ToDouble((sqlDataReader["PERS_NO"] is DBNull || sqlDataReader["PERS_NO"] == null)
                                ? 0
                                : sqlDataReader["PERS_NO"]);
                        requestReport.Name = Convert.ToString(sqlDataReader["NAME"]);
                        requestReport.Family = Convert.ToString(sqlDataReader["FAMILY"]);
                        if (sqlDataReader["IsFinalApproved"] is DBNull || sqlDataReader["IsFinalApproved"] == null)
                        {
                            requestReport.IsFinalApproved = null;
                        }
                        else
                        {
                            requestReport.IsFinalApproved =
                                new bool?(Convert.ToBoolean(sqlDataReader["IsFinalApproved"]));
                        }
                        requestReport.SecNo =
                            Convert.ToDouble((sqlDataReader["SEC_NO"] is DBNull || sqlDataReader["SEC_NO"] == null)
                                ? 0
                                : sqlDataReader["SEC_NO"]);
                        requestReport.RequestsId =
                            Convert.ToInt32((sqlDataReader["RequestsID"] is DBNull ||
                                             sqlDataReader["RequestsID"] == null)
                                ? 0
                                : sqlDataReader["RequestsID"]);
                        requestReport.Type =
                            Convert.ToInt16((sqlDataReader["Type"] is DBNull || sqlDataReader["Type"] == null)
                                ? 0
                                : sqlDataReader["Type"]);
                        requestReport.CardTitle = Convert.ToString(sqlDataReader["CardTITLE"]);
                        requestReport.Distination = Convert.ToString(sqlDataReader["Distination"]);
                        requestReport.IsDay = Convert.ToBoolean(sqlDataReader["IS_DAY"]);
                        string text = Convert.ToString(sqlDataReader["Duration"]);
                        requestReport.Duration = text;
                        string title = Convert.ToString(sqlDataReader["TITLE"]);
                        string a = sqlDataReader["Requested_Time"].ToString();
                        if (a == "")
                        {
                            a = "0";
                        }
                        base.GetDuration(requestReport.OperationsId, (int) requestReport.Type, requestReport.IsDay,
                            requestReport.IsFinalApproved,
                            UtilityManagers.ConvertToTimeFormat(sqlDataReader["StartHour"].ToString()),
                            sqlDataReader["EndHour"].ToString(), ref text, ref a, ref title);
                        requestReport.Title = title;
                        requestReport.DurationStr = text;
                        list.Add(requestReport);
                    }
                }
                result = list;
            }
            return result;
        }

        // Token: 0x06000194 RID: 404 RVA: 0x0000DAB0 File Offset: 0x0000BCB0
        public List<TurnoverSectionRow> GetCountRequestInMonthForSections(string fdate, string tdate,
            DataTable dTsections, DataTable dTcards, bool isAllSections, bool isAllCards)
        {
            List<TurnoverSectionRow> list = new List<TurnoverSectionRow>();
            string text = fdate;
            string[] array = tdate.Split(new char[]
            {
                '/'
            });
            string text2 = string.Empty;
            string text3 = string.Empty;
            bool flag = false;
            TurnoverSectionRow modelClassInstance = null;
            int num = Convert.ToInt32(array[0]);
            while (ShDate.DifDays(text, tdate) >= 0)
            {
                array = text.Split(new char[]
                {
                    '/'
                });
                foreach (DataRow dataRow in dTsections.Rows)
                {
                    if (!(dataRow["SEC_NO"].ToString() == "-1"))
                    {
                        foreach (DataRow dataRow2 in dTcards.Rows)
                        {
                            modelClassInstance = new TurnoverSectionRow();
                            modelClassInstance.MD = Convert.ToInt32(array[1]);
                            modelClassInstance.YD = Convert.ToInt32(array[0]);
                            modelClassInstance.MonthName = ShDate.MonthName((short) modelClassInstance.MD);
                            if (num > modelClassInstance.YD)
                            {
                                TurnoverSectionRow expr_11C = modelClassInstance;
                                expr_11C.MonthName = expr_11C.MonthName + " سال" + modelClassInstance.YD;
                            }
                            modelClassInstance.SEC_NO = Convert.ToInt32(dataRow["SEC_NO"]);
                            modelClassInstance.CARD_NO = Convert.ToInt32(dataRow2["CARD_NO"]);
                            modelClassInstance.secTitle = dataRow["TITLE"].ToString();
                            modelClassInstance.cardTitle = dataRow2["TITLE"].ToString();
                            list.Add(modelClassInstance);
                            if (string.IsNullOrEmpty(text2))
                            {
                                text3 = text3 + "," + modelClassInstance.CARD_NO;
                            }
                        }
                        if (!flag && modelClassInstance != null)
                        {
                            text2 = text2 + "," + modelClassInstance.SEC_NO;
                        }
                    }
                }
                flag = true;
                text = ShDate.AddMonths(text, 1);
            }
            text2 = ((text2 != "" && !isAllSections) ? text2.Remove(0, 1) : "");
            text3 = ((text3 != "" && !isAllCards) ? text3.Remove(0, 1) : "");
            using (SqlDataReader sqlDataReader = base.ExecuteReader("WF_GetCountRequestInMonthForSections", new object[]
            {
                ShDate.StrToDate(fdate),
                ShDate.StrToDate(tdate),
                string.IsNullOrEmpty(text2) ? "" : text2,
                string.IsNullOrEmpty(text3) ? "" : text3,
                3
            }))
            {
                if (sqlDataReader.HasRows)
                {
                    //System.Func<TurnoverSectionRow, bool><>
                    //9
                    //__0;
                    while (sqlDataReader.Read())
                    {
                        modelClassInstance = new TurnoverSectionRow();
                        modelClassInstance.SEC_NO =
                            Convert.ToInt32((sqlDataReader["SEC_NO"] is DBNull || sqlDataReader["SEC_NO"] == null)
                                ? 0
                                : sqlDataReader["SEC_NO"]);
                        modelClassInstance.secTitle = Convert.ToString(sqlDataReader["secTitle"]);
                        modelClassInstance.CARD_NO =
                            Convert.ToInt32((sqlDataReader["CARD_NO"] is DBNull || sqlDataReader["CARD_NO"] == null)
                                ? 0
                                : sqlDataReader["CARD_NO"]);
                        modelClassInstance.cardTitle = Convert.ToString(sqlDataReader["secTitle"]);
                        modelClassInstance.YD =
                            Convert.ToInt32((sqlDataReader["YD"] is DBNull || sqlDataReader["YD"] == null)
                                ? 0
                                : sqlDataReader["YD"]);
                        modelClassInstance.MD =
                            (int)
                                Convert.ToInt16((sqlDataReader["MD"] is DBNull || sqlDataReader["MD"] == null)
                                    ? 0
                                    : sqlDataReader["MD"]);
                        IEnumerable<TurnoverSectionRow> arg_4A0_0 = list;
                        Func<TurnoverSectionRow, bool> arg_4A0_1;
                        //if ((arg_4A0_1 = <>
                        //9
                        //__0) ==
                        //null)
                        //{
                        //    arg_4A0_1 = (<>
                        //    9
                        //    __0 =
                        //        ((TurnoverSectionRow c) =>
                        //            c.SEC_NO == modelClassInstance.SEC_NO && c.CARD_NO == modelClassInstance.CARD_NO &&
                        //            c.YD == modelClassInstance.YD && c.MD == modelClassInstance.MD))
                        //    ;
                        //}
                        //arg_4A0_0.FirstOrDefault(arg_4A0_1).CountReq =
                            Convert.ToInt32((sqlDataReader["CountReq"] is DBNull || sqlDataReader["CountReq"] == null)
                                ? 0
                                : sqlDataReader["CountReq"]);
                    }
                }
            }
            return list;
        }

        // Token: 0x06000195 RID: 405 RVA: 0x0000E008 File Offset: 0x0000C208
        public List<TurnoverSectionRow> GetCountRequestInWeekForSections(string fdate, string tdate,
            DataTable dTsections, DataTable dTcards, bool isAllSections, bool isAllCards)
        {
            List<TurnoverSectionRow> list = new List<TurnoverSectionRow>();
            string text = fdate;
            string[] array = tdate.Split(new char[]
            {
                '/'
            });
            string text2 = string.Empty;
            string text3 = string.Empty;
            bool flag = false;
            TurnoverSectionRow mci = null;
            int num = Convert.ToInt32(array[0]);
            while (ShDate.DifDays(text, tdate) > 0)
            {
                array = text.Split(new char[]
                {
                    '/'
                });
                foreach (DataRow dataRow in dTsections.Rows)
                {
                    foreach (DataRow dataRow2 in dTcards.Rows)
                    {
                        mci = new TurnoverSectionRow();
                        mci.YD = Convert.ToInt32(array[0]);
                        mci.MD = Convert.ToInt32(array[1]);
                        mci.WD = Convert.ToInt32(array[2])/7 + 1;
                        mci.MonthName = ShDate.MonthName((short) mci.MD);
                        if (num > mci.YD)
                        {
                            TurnoverSectionRow expr_113 = mci;
                            expr_113.MonthName = expr_113.MonthName + " سال" + mci.YD;
                        }
                        mci.WeekName = string.Format("هفته {0} ", ShDate.DayStrNum[mci.WD - 1]);
                        if (num > mci.YD)
                        {
                            TurnoverSectionRow expr_175 = mci;
                            expr_175.WeekName += mci.MonthName;
                        }
                        mci.SEC_NO = Convert.ToInt32(dataRow["SEC_NO"]);
                        mci.CARD_NO = Convert.ToInt32(dataRow2["CARD_NO"]);
                        mci.secTitle = dataRow["TITLE"].ToString();
                        mci.cardTitle = dataRow2["TITLE"].ToString();
                        list.Add(mci);
                        if (string.IsNullOrEmpty(text2))
                        {
                            text3 = text3 + mci.CARD_NO + ",";
                        }
                    }
                    if (!flag && mci != null)
                    {
                        text2 = text2 + mci.SEC_NO + ",";
                    }
                }
                flag = true;
                text = ShDate.AddDays(text, 7.0);
            }
            text2 = ((text2 != "" && !isAllSections) ? text2.Remove(text2.Length - 1) : "");
            text3 = ((text3 != "" && !isAllCards) ? text3.Remove(text3.Length - 1) : "");
            using (SqlDataReader sqlDataReader = base.ExecuteReader("WF_GetCountRequestInWeekForSections", new object[]
            {
                ShDate.StrToDate(fdate),
                ShDate.StrToDate(tdate),
                string.IsNullOrEmpty(text2) ? "" : text2,
                string.IsNullOrEmpty(text3) ? "" : text3,
                3
            }))
            {
                if (sqlDataReader.HasRows)
                {
                    //Func<TurnoverSectionRow, bool><>
                    //9
                    //__0;
                    while (sqlDataReader.Read())
                    {
                        mci = new TurnoverSectionRow();
                        mci.SEC_NO =
                            Convert.ToInt32((sqlDataReader["SEC_NO"] is DBNull || sqlDataReader["SEC_NO"] == null)
                                ? 0
                                : sqlDataReader["SEC_NO"]);
                        mci.secTitle = Convert.ToString(sqlDataReader["secTitle"]);
                        mci.CARD_NO =
                            Convert.ToInt32((sqlDataReader["CARD_NO"] is DBNull || sqlDataReader["CARD_NO"] == null)
                                ? 0
                                : sqlDataReader["CARD_NO"]);
                        mci.cardTitle = Convert.ToString(sqlDataReader["secTitle"]);
                        mci.YD =
                            Convert.ToInt32((sqlDataReader["YD"] is DBNull || sqlDataReader["YD"] == null)
                                ? 0
                                : sqlDataReader["YD"]);
                        mci.MD =
                            (int)
                                Convert.ToInt16((sqlDataReader["MD"] is DBNull || sqlDataReader["MD"] == null)
                                    ? 0
                                    : sqlDataReader["MD"]);
                        mci.WD =
                            (int)
                                Convert.ToInt16((sqlDataReader["WD"] is DBNull || sqlDataReader["WD"] == null)
                                    ? 0
                                    : sqlDataReader["WD"]);
                        IEnumerable<TurnoverSectionRow> arg_54A_0 = list;
                        Func<TurnoverSectionRow, bool> arg_54A_1;
                        //if ((arg_54A_1 = <>
                        //9
                        //__0) ==
                        //null)
                        //{
                        //    arg_54A_1 = (<>
                        //    9
                        //    __0 =
                        //        ((TurnoverSectionRow c) =>
                        //            c.SEC_NO == mci.SEC_NO && c.CARD_NO == mci.CARD_NO && c.YD == mci.YD &&
                        //            c.MD == mci.MD && c.WD == mci.WD))
                        //    ;
                        //}
                        //TurnoverSectionRow turnoverSectionRow = arg_54A_0.FirstOrDefault(arg_54A_1);
                        //if (turnoverSectionRow != null)
                        //{
                        //    turnoverSectionRow.CountReq =
                        //        Convert.ToInt32((sqlDataReader["CountReq"] is DBNull ||
                        //                         sqlDataReader["CountReq"] == null)
                        //            ? 0
                        //            : sqlDataReader["CountReq"]);
                        //}
                    }
                }
            }
            return list;
        }

        // Token: 0x06000196 RID: 406 RVA: 0x0000E614 File Offset: 0x0000C814
        public List<LogReport> GetLog(DateTime fromDate, DateTime toDate)
        {
            List<LogReport> list = new List<LogReport>();
            List<LogReport> result;
            using (SqlDataReader sqlDataReader = base.ExecuteReader("WF_Rep_Log", new object[]
            {
                fromDate,
                toDate.AddDays(1.0)
            }))
            {
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        LogReport logReport = new LogReport();
                        logReport.LogID =
                            Convert.ToInt32((sqlDataReader["LogID"] is DBNull || sqlDataReader["LogID"] == null)
                                ? 0
                                : sqlDataReader["LogID"]);
                        logReport.LogDate = Convert.ToDateTime(sqlDataReader["LogDate"]);
                        logReport.EMP_NO =
                            Convert.ToDouble((sqlDataReader["EMP_NO"] is DBNull || sqlDataReader["EMP_NO"] == null)
                                ? 0
                                : sqlDataReader["EMP_NO"]);
                        logReport.EmployeeName = Convert.ToString(sqlDataReader["EmployeeName"]);
                        logReport.FullName = ((sqlDataReader["FullName"] is DBNull)
                            ? string.Empty
                            : Convert.ToString(sqlDataReader["FullName"]));
                        logReport.LogMsg = Convert.ToString(sqlDataReader["LogMsg"]);
                        logReport.CurrentPage = Convert.ToString(sqlDataReader["CurrentPage"]);
                        LogReport expr_151 = logReport;
                        expr_151.FullName = expr_151.FullName + " " + logReport.EmployeeName;
                        list.Add(logReport);
                    }
                }
                result = list;
            }
            return result;
        }
    }

    // Token: 0x02000012 RID: 18
    public class RequestReport
    {
        // Token: 0x060000C8 RID: 200 RVA: 0x0000211C File Offset: 0x0000031C
        public RequestReport()
        {
        }

        // Token: 0x060000C9 RID: 201 RVA: 0x00002F38 File Offset: 0x00001138
        public RequestReport(DateTime startDate, double empNo, double persNo, string name, string family, string title,
            bool isFinalApproved, double secNo, int requestsId, short type, string cardTitle, string distination,
            string source, string description)
        {
            this.StartDate = startDate;
            this.EmpNo = empNo;
            this.PersNo = persNo;
            this.Name = name;
            this.Family = family;
            this.Title = title;
            this.IsFinalApproved = new bool?(isFinalApproved);
            this.SecNo = secNo;
            this.RequestsId = requestsId;
            this.Type = type;
            this.CardTitle = cardTitle;
            this.Distination = distination;
            this.Source = source;
            this.Description = description;
        }

        // Token: 0x17000059 RID: 89
        // (get) Token: 0x060000CA RID: 202 RVA: 0x00002FBD File Offset: 0x000011BD
        // (set) Token: 0x060000CB RID: 203 RVA: 0x00002FC5 File Offset: 0x000011C5
        public string Source { get; set; }

        // Token: 0x1700005A RID: 90
        // (get) Token: 0x060000CC RID: 204 RVA: 0x00002FCE File Offset: 0x000011CE
        // (set) Token: 0x060000CD RID: 205 RVA: 0x00002FD6 File Offset: 0x000011D6
        public string Distination { get; set; }

        // Token: 0x1700005B RID: 91
        // (get) Token: 0x060000CE RID: 206 RVA: 0x00002FDF File Offset: 0x000011DF
        // (set) Token: 0x060000CF RID: 207 RVA: 0x00002FE7 File Offset: 0x000011E7
        public DateTime StartDate { get; set; }

        // Token: 0x1700005C RID: 92
        // (get) Token: 0x060000D0 RID: 208 RVA: 0x00002FF0 File Offset: 0x000011F0
        public string ShamsyStartDate
        {
            get { return ShDate.DateToStr(this.StartDate, true) + " " + ((!this.IsDay) ? this.StartHourStr : ""); }
        }

        // Token: 0x1700005D RID: 93
        // (get) Token: 0x060000D1 RID: 209 RVA: 0x0000301D File Offset: 0x0000121D
        // (set) Token: 0x060000D2 RID: 210 RVA: 0x00003025 File Offset: 0x00001225
        public double EmpNo { get; set; }

        // Token: 0x1700005E RID: 94
        // (get) Token: 0x060000D3 RID: 211 RVA: 0x0000302E File Offset: 0x0000122E
        // (set) Token: 0x060000D4 RID: 212 RVA: 0x00003036 File Offset: 0x00001236
        public double PersNo { get; set; }

        // Token: 0x1700005F RID: 95
        // (get) Token: 0x060000D5 RID: 213 RVA: 0x0000303F File Offset: 0x0000123F
        // (set) Token: 0x060000D6 RID: 214 RVA: 0x00003047 File Offset: 0x00001247
        public string Name { get; set; }

        // Token: 0x17000060 RID: 96
        // (get) Token: 0x060000D7 RID: 215 RVA: 0x00003050 File Offset: 0x00001250
        // (set) Token: 0x060000D8 RID: 216 RVA: 0x00003058 File Offset: 0x00001258
        public string Family { get; set; }

        // Token: 0x17000061 RID: 97
        // (get) Token: 0x060000D9 RID: 217 RVA: 0x00003061 File Offset: 0x00001261
        public string FullName
        {
            get { return this.Name + " " + this.Family; }
        }

        // Token: 0x17000062 RID: 98
        // (get) Token: 0x060000DA RID: 218 RVA: 0x00003079 File Offset: 0x00001279
        // (set) Token: 0x060000DB RID: 219 RVA: 0x00003081 File Offset: 0x00001281
        public string Title { get; set; }

        // Token: 0x17000063 RID: 99
        // (get) Token: 0x060000DC RID: 220 RVA: 0x0000308A File Offset: 0x0000128A
        // (set) Token: 0x060000DD RID: 221 RVA: 0x00003092 File Offset: 0x00001292
        public bool? IsFinalApproved { get; set; }

        // Token: 0x17000064 RID: 100
        // (get) Token: 0x060000DE RID: 222 RVA: 0x0000309C File Offset: 0x0000129C
        public string Status
        {
            get
            {
                if (!this.IsFinalApproved.HasValue)
                {
                    return "ارسالی";
                }
                if (!this.IsFinalApproved.Value)
                {
                    return "رد";
                }
                return "تایید";
            }
        }

        // Token: 0x17000065 RID: 101
        // (get) Token: 0x060000DF RID: 223 RVA: 0x000030DA File Offset: 0x000012DA
        // (set) Token: 0x060000E0 RID: 224 RVA: 0x000030E2 File Offset: 0x000012E2
        public double SecNo { get; set; }

        // Token: 0x17000066 RID: 102
        // (get) Token: 0x060000E1 RID: 225 RVA: 0x000030EB File Offset: 0x000012EB
        // (set) Token: 0x060000E2 RID: 226 RVA: 0x000030F3 File Offset: 0x000012F3
        public int RequestsId { get; set; }

        // Token: 0x17000067 RID: 103
        // (get) Token: 0x060000E3 RID: 227 RVA: 0x000030FC File Offset: 0x000012FC
        // (set) Token: 0x060000E4 RID: 228 RVA: 0x00003104 File Offset: 0x00001304
        public short Type { get; set; }

        // Token: 0x17000068 RID: 104
        // (get) Token: 0x060000E5 RID: 229 RVA: 0x0000310D File Offset: 0x0000130D
        // (set) Token: 0x060000E6 RID: 230 RVA: 0x00003115 File Offset: 0x00001315
        public string CardTitle { get; set; }

        // Token: 0x17000069 RID: 105
        // (get) Token: 0x060000E7 RID: 231 RVA: 0x0000311E File Offset: 0x0000131E
        // (set) Token: 0x060000E8 RID: 232 RVA: 0x00003126 File Offset: 0x00001326
        public int StartHour { get; set; }

        // Token: 0x1700006A RID: 106
        // (get) Token: 0x060000E9 RID: 233 RVA: 0x0000312F File Offset: 0x0000132F
        // (set) Token: 0x060000EA RID: 234 RVA: 0x00003137 File Offset: 0x00001337
        public int EndHour { get; set; }

        // Token: 0x1700006B RID: 107
        // (get) Token: 0x060000EB RID: 235 RVA: 0x00003140 File Offset: 0x00001340
        // (set) Token: 0x060000EC RID: 236 RVA: 0x00003148 File Offset: 0x00001348
        public int OperationsId { get; set; }

        // Token: 0x1700006C RID: 108
        // (get) Token: 0x060000ED RID: 237 RVA: 0x00003151 File Offset: 0x00001351
        // (set) Token: 0x060000EE RID: 238 RVA: 0x00003159 File Offset: 0x00001359
        public string Duration { get; set; }

        // Token: 0x1700006D RID: 109
        // (get) Token: 0x060000EF RID: 239 RVA: 0x00003162 File Offset: 0x00001362
        // (set) Token: 0x060000F0 RID: 240 RVA: 0x0000316A File Offset: 0x0000136A
        public bool IsDay { get; set; }

        // Token: 0x1700006E RID: 110
        // (get) Token: 0x060000F1 RID: 241 RVA: 0x00003173 File Offset: 0x00001373
        // (set) Token: 0x060000F2 RID: 242 RVA: 0x0000317B File Offset: 0x0000137B
        public string DurationStr { get; set; }

        // Token: 0x1700006F RID: 111
        // (get) Token: 0x060000F3 RID: 243 RVA: 0x00003184 File Offset: 0x00001384
        public string StartHourStr
        {
            get { return UtilityManagers.ConvertToTimeFormat(this.StartHour.ToString()); }
        }

        // Token: 0x17000070 RID: 112
        // (get) Token: 0x060000F4 RID: 244 RVA: 0x000031A4 File Offset: 0x000013A4
        public string EndHourStr
        {
            get { return UtilityManagers.ConvertToTimeFormat(this.EndHour.ToString()); }
        }

        // Token: 0x17000071 RID: 113
        // (get) Token: 0x060000F5 RID: 245 RVA: 0x000031C4 File Offset: 0x000013C4
        // (set) Token: 0x060000F6 RID: 246 RVA: 0x000031CC File Offset: 0x000013CC
        public string Description { get; set; }
    }

    // Token: 0x02000011 RID: 17
    public class TurnoverSectionRow : BaseRow
    {
        // Token: 0x1700004F RID: 79
        // (get) Token: 0x060000B4 RID: 180 RVA: 0x00002E8B File Offset: 0x0000108B
        // (set) Token: 0x060000B5 RID: 181 RVA: 0x00002E93 File Offset: 0x00001093
        public int SEC_NO { get; set; }

        // Token: 0x17000050 RID: 80
        // (get) Token: 0x060000B6 RID: 182 RVA: 0x00002E9C File Offset: 0x0000109C
        // (set) Token: 0x060000B7 RID: 183 RVA: 0x00002EA4 File Offset: 0x000010A4
        public string secTitle { get; set; }

        // Token: 0x17000051 RID: 81
        // (get) Token: 0x060000B8 RID: 184 RVA: 0x00002EAD File Offset: 0x000010AD
        // (set) Token: 0x060000B9 RID: 185 RVA: 0x00002EB5 File Offset: 0x000010B5
        public int CARD_NO { get; set; }

        // Token: 0x17000052 RID: 82
        // (get) Token: 0x060000BA RID: 186 RVA: 0x00002EBE File Offset: 0x000010BE
        // (set) Token: 0x060000BB RID: 187 RVA: 0x00002EC6 File Offset: 0x000010C6
        public string cardTitle { get; set; }

        // Token: 0x17000053 RID: 83
        // (get) Token: 0x060000BC RID: 188 RVA: 0x00002ECF File Offset: 0x000010CF
        // (set) Token: 0x060000BD RID: 189 RVA: 0x00002ED7 File Offset: 0x000010D7
        public int YD { get; set; }

        // Token: 0x17000054 RID: 84
        // (get) Token: 0x060000BE RID: 190 RVA: 0x00002EE0 File Offset: 0x000010E0
        // (set) Token: 0x060000BF RID: 191 RVA: 0x00002EE8 File Offset: 0x000010E8
        public int MD { get; set; }

        // Token: 0x17000055 RID: 85
        // (get) Token: 0x060000C0 RID: 192 RVA: 0x00002EF1 File Offset: 0x000010F1
        // (set) Token: 0x060000C1 RID: 193 RVA: 0x00002EF9 File Offset: 0x000010F9
        public int WD { get; set; }

        // Token: 0x17000056 RID: 86
        // (get) Token: 0x060000C2 RID: 194 RVA: 0x00002F02 File Offset: 0x00001102
        // (set) Token: 0x060000C3 RID: 195 RVA: 0x00002F0A File Offset: 0x0000110A
        public string WeekName { get; set; }

        // Token: 0x17000057 RID: 87
        // (get) Token: 0x060000C4 RID: 196 RVA: 0x00002F13 File Offset: 0x00001113
        // (set) Token: 0x060000C5 RID: 197 RVA: 0x00002F1B File Offset: 0x0000111B
        public string MonthName { get; set; }

        // Token: 0x17000058 RID: 88
        // (get) Token: 0x060000C6 RID: 198 RVA: 0x00002F24 File Offset: 0x00001124
        // (set) Token: 0x060000C7 RID: 199 RVA: 0x00002F2C File Offset: 0x0000112C
        public int CountReq { get; set; }
    }

    // Token: 0x0200000C RID: 12
    public class LogReport
    {
        // Token: 0x1700003B RID: 59
        // (get) Token: 0x06000088 RID: 136 RVA: 0x00002C89 File Offset: 0x00000E89
        // (set) Token: 0x06000089 RID: 137 RVA: 0x00002C91 File Offset: 0x00000E91
        public int LogID { get; set; }

        // Token: 0x1700003C RID: 60
        // (get) Token: 0x0600008A RID: 138 RVA: 0x00002C9A File Offset: 0x00000E9A
        // (set) Token: 0x0600008B RID: 139 RVA: 0x00002CA2 File Offset: 0x00000EA2
        public string LogMsg { get; set; }

        // Token: 0x1700003D RID: 61
        // (get) Token: 0x0600008C RID: 140 RVA: 0x00002CAB File Offset: 0x00000EAB
        // (set) Token: 0x0600008D RID: 141 RVA: 0x00002CB3 File Offset: 0x00000EB3
        public string CurrentPage { get; set; }

        // Token: 0x1700003E RID: 62
        // (get) Token: 0x0600008E RID: 142 RVA: 0x00002CBC File Offset: 0x00000EBC
        // (set) Token: 0x0600008F RID: 143 RVA: 0x00002CC4 File Offset: 0x00000EC4
        public double EMP_NO { get; set; }

        // Token: 0x1700003F RID: 63
        // (get) Token: 0x06000090 RID: 144 RVA: 0x00002CCD File Offset: 0x00000ECD
        // (set) Token: 0x06000091 RID: 145 RVA: 0x00002CD5 File Offset: 0x00000ED5
        public string FullName { get; set; }

        // Token: 0x17000040 RID: 64
        // (get) Token: 0x06000092 RID: 146 RVA: 0x00002CDE File Offset: 0x00000EDE
        public string LogDateStr
        {
            get { return ShDate.DateToStr(this.LogDate); }
        }

        // Token: 0x17000041 RID: 65
        // (get) Token: 0x06000093 RID: 147 RVA: 0x00002CEB File Offset: 0x00000EEB
        // (set) Token: 0x06000094 RID: 148 RVA: 0x00002CF3 File Offset: 0x00000EF3
        public DateTime LogDate { get; set; }

        // Token: 0x17000042 RID: 66
        // (get) Token: 0x06000095 RID: 149 RVA: 0x00002CFC File Offset: 0x00000EFC
        // (set) Token: 0x06000096 RID: 150 RVA: 0x00002D04 File Offset: 0x00000F04
        public string EmployeeName { get; set; }
    }
}
