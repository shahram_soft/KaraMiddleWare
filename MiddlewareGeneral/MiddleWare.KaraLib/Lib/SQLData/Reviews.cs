﻿using System;
using System.Data;

namespace PW.WorkFlows.SqlDal
{
	// Token: 0x0200002B RID: 43
	public class Reviews : BaseDAL
	{
		// Token: 0x060001D9 RID: 473 RVA: 0x00003C40 File Offset: 0x00001E40
		public Reviews(WorkFlowsDatabase owner) : base(owner)
		{
		}

		// Token: 0x060001DA RID: 474 RVA: 0x00014308 File Offset: 0x00012508
		public DataTable GetData()
		{
			return base.ExecuteDataTable("WF_ReviewsGetData", null);
		}

		// Token: 0x060001DB RID: 475 RVA: 0x00014316 File Offset: 0x00012516
		public DataTable GetDataById(int id)
		{
			return base.ExecuteDataTable("WF_ReviewsGetDataByID", new object[]
			{
				id
			});
		}

		// Token: 0x060001DC RID: 476 RVA: 0x00014334 File Offset: 0x00012534
		public int Insert(int requestId, double reviewedByEmployeeId, string description, short reviewStatus, DateTime reviewDate, bool isShowPersonal)
		{
			return base.ExecuteNonQuery("WF_ReviewsInsert", new object[]
			{
				requestId,
				reviewedByEmployeeId,
				description,
				reviewStatus,
				reviewDate,
				isShowPersonal
			});
		}

		// Token: 0x060001DD RID: 477 RVA: 0x00014388 File Offset: 0x00012588
		public int Update(int id, double reviewedByEmployeeId, string description, short reviewStatus, DateTime reviewDate, bool isShowPersonal)
		{
			return base.ExecuteNonQuery("WF_ReviewsUpdate", new object[]
			{
				id,
				reviewedByEmployeeId,
				description,
				reviewStatus,
				reviewDate,
				isShowPersonal
			});
		}

		// Token: 0x060001DE RID: 478 RVA: 0x000143DA File Offset: 0x000125DA
		public DataTable GetManagerReviewsByRequestId(int requestsId, bool isShowPersonFilter)
		{
			return base.ExecuteDataTable("WF_GetManagerReviewsByRequestID", new object[]
			{
				requestsId,
				isShowPersonFilter
			});
		}

		// Token: 0x060001DF RID: 479 RVA: 0x00014400 File Offset: 0x00012600
		public int CountWithRequestId(string requestId)
		{
			object obj = base.ExecuteScalar(CommandType.Text, "SELECT * FROM Reviews r WHERE r.RequestID=" + requestId);
			if (obj != null)
			{
				return Convert.ToInt32(obj);
			}
			return -1;
		}
	}
}
