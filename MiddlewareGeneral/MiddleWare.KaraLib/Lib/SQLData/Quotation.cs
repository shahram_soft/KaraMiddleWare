﻿using System;
using System.Data;

namespace PW.WorkFlows.SqlDal
{
	// Token: 0x02000027 RID: 39
	public class Quotation : BaseDAL
	{
		// Token: 0x0600018A RID: 394 RVA: 0x00003C40 File Offset: 0x00001E40
		public Quotation(WorkFlowsDatabase owner) : base(owner)
		{
		}

		// Token: 0x0600018B RID: 395 RVA: 0x0000C9E1 File Offset: 0x0000ABE1
		public DataRow GetData()
		{
			return base.ExecuteDataRow(CommandType.Text, "SELECT TOP 1 * FROM Quotation q WHERE q.Serial=((CAST(getdate() AS INT)-41631)%2000)+17");
		}
	}
}
