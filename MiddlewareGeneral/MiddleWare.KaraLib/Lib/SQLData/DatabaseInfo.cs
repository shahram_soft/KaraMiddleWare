﻿using System;
using System.Data;
using Microsoft.ApplicationBlocks.Data;

namespace PW.WorkFlows.SqlDal
{
	// Token: 0x02000011 RID: 17
	public class DatabaseInfo : BaseDAL
	{
		// Token: 0x060000E1 RID: 225 RVA: 0x00003C40 File Offset: 0x00001E40
		public DatabaseInfo(WorkFlowsDatabase owner) : base(owner)
		{
		}

		// Token: 0x060000E2 RID: 226 RVA: 0x0000626C File Offset: 0x0000446C
		public DataTable GetDatabases(string masterConStr)
		{
			DataTable result;
			try
			{
				DataSet dataSet = SqlHelper.ExecuteDataset(masterConStr, CommandType.Text, "SELECT name, database_id FROM sys.databases WHERE [name] LIKE 'pwkara%' ORDER BY [name]");
				if (dataSet != null && dataSet.Tables.Count > 0)
				{
					result = dataSet.Tables[0];
				}
				else
				{
					result = null;
				}
			}
			catch
			{
				result = null;
			}
			return result;
		}
	}
}
