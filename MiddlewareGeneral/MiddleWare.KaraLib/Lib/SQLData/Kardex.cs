﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using MiddleWare.KaraLib;
using MiddleWare.KaraLib.ViewModels;


namespace PW.WorkFlows.SqlDal
{
	// Token: 0x02000013 RID: 19
	public class Kardex : BaseDAL
	{
		// Token: 0x060000E9 RID: 233 RVA: 0x00003C40 File Offset: 0x00001E40
		public Kardex(WorkFlowsDatabase owner) : base(owner)
		{
		}

		// Token: 0x060000EA RID: 234 RVA: 0x00006387 File Offset: 0x00004587
		public DataTable GetMorMamData(double empNo, double date, int state)
		{
			return base.ExecuteDataTable("WF_GetMorMamData", new object[]
			{
				empNo,
				date,
				state
			});
		}

		// Token: 0x060000EB RID: 235 RVA: 0x000063B8 File Offset: 0x000045B8
		public void GetDayAndTimestring(double empNo, double time, out int timeDay, bool isMorDay, out int hur)
		{
			timeDay = (int)time;
			hur = 0;
			int num = this.kasr_Esteh(empNo, DateTime.Now.Date);
			if (!isMorDay)
			{
				timeDay = Kardex.DivTime((int)time, num);
				if (timeDay < 0 & time < 0.0)
				{
					hur = Kardex.TimeSub((int)time * -1, Kardex.Mult_Time((double)(timeDay * -1), num));
				}
				else if (timeDay < 0)
				{
					hur = Kardex.TimeSub((int)time, Kardex.Mult_Time((double)(timeDay * -1), num));
				}
				else if (time < 0.0)
				{
					hur = Kardex.TimeSub((int)time * -1, Kardex.Mult_Time((double)timeDay, num));
				}
				else
				{
					hur = Kardex.TimeSub((int)time, Kardex.Mult_Time((double)timeDay, num));
				}
			}
			if (timeDay == 0 && time < 0.0)
			{
				hur *= -1;
			}
		}

		// Token: 0x060000EC RID: 236 RVA: 0x0000648C File Offset: 0x0000468C
		public static int Mult_Time(double n, int time1)
		{
			bool flag = false;
			if (n < 0.0)
			{
				n *= -1.0;
				flag = true;
			}
			if (time1 < 0)
			{
				time1 *= -1;
				flag = true;
			}
			int expr_3A = (int)((double)(time1 / 100 * 60 + time1 % 100) * n);
			int num = expr_3A / 60;
			int num2 = expr_3A - num * 60;
			string s;
			if (num2 < 10)
			{
				s = num + "0" + num2;
			}
			else
			{
				s = num + num2.ToString();
			}
			int result;
			if (flag)
			{
				result = int.Parse(s) * -1;
			}
			else
			{
				result = int.Parse(s);
			}
			return result;
		}

		// Token: 0x060000ED RID: 237 RVA: 0x00006528 File Offset: 0x00004728
		public string DayTimeStr(int remainedDays, int remainedHours)
		{
			string result = "00:00";
			if (remainedDays != 0 && remainedHours != 0)
			{
				result = string.Format("{1} روز {0}", remainedDays, UtilityManagers.MakeTimeFormat(remainedHours.ToString()));
			}
			else if (remainedDays != 0 && remainedHours == 0)
			{
				result = remainedDays + " روز ";
			}
			else if (remainedDays == 0 && remainedHours != 0)
			{
				result = UtilityManagers.MakeTimeFormat(remainedHours.ToString());
			}
			return result;
		}

		// Token: 0x060000EE RID: 238 RVA: 0x00006590 File Offset: 0x00004790
		public static int TimeSub(int time1, int time2)
		{
			if (time1 < time2)
			{
				if (time1 == 0)
				{
					int arg_0B_0 = time1;
					time1 = time2;
					time2 = arg_0B_0;
				}
				else if (time2 != 0)
				{
					time1 += 2400;
				}
			}
			int num = time1 % 100;
			int num2 = time2 % 100;
			int time3;
			if (num < num2)
			{
				time3 = num + 60 - num2;
				time2 += 100;
			}
			else
			{
				time3 = num - num2;
			}
			int arg_46_0 = time1 / 100;
			int num3 = time2 / 100;
			return ShDate.TimePlus((arg_46_0 - num3) * 100, time3);
		}

		// Token: 0x060000EF RID: 239 RVA: 0x000065F0 File Offset: 0x000047F0
		public DataTable GetMorMamData(double empNo, double fromDate, double toDate)
		{
			string commandText = string.Format("SELECT * FROM MOR_MAM mm  \n\tWHERE  \n\t\t\tmm.EMP_NO={0} AND  \n\t\t\tmm.TYP=57 AND  \n\t\t\tmm.S_DATE >= {1} AND  \n\t\t\tmm.S_DATE <= {2}", empNo, toDate, fromDate);
			return base.ExecuteDataTable(CommandType.Text, commandText);
		}

		// Token: 0x060000F0 RID: 240 RVA: 0x00006624 File Offset: 0x00004824
		public double KardexRemTime(double empNo, double fromDate, out int remainedDays, out int remainedHours, bool avalDoreFlg, bool repYearly, double toDate, bool isForceLeaveInNextYear, out List<MyTempKardex> outMyTempKardex, ref double Dec_, ref double Inc_, ParmfileRow p)
		{
			double num = 0.0;
			Dec_ = 0.0;
			Inc_ = 0.0;
			DataRow dataByEmpNo = WorkFlowsDatabase.Instance.Employee.GetDataByEmpNo(empNo);
			if (dataByEmpNo == null)
			{
				remainedDays = 0;
				remainedHours = 0;
				outMyTempKardex = null;
				return 0.0;
			}
			double num2 = avalDoreFlg ? Convert.ToDouble((dataByEmpNo["Esteh_Save_Main"] is DBNull || dataByEmpNo["Esteh_Save_Main"] == null) ? 0 : dataByEmpNo["Esteh_Save_Main"]) : Convert.ToDouble((dataByEmpNo["ESTEH_SAVE"] is DBNull || dataByEmpNo["ESTEH_SAVE"] == null) ? 0 : dataByEmpNo["ESTEH_SAVE"]);
			DataTable dataTable;
			if (avalDoreFlg)
			{
				dataTable = this.GetMorMamData(empNo, fromDate, 0);
			}
			else if (repYearly)
			{
				dataTable = ((toDate >= p.MmDate) ? this.GetMorMamData(empNo, fromDate, toDate) : this.GetMorMamData(empNo, fromDate, 1));
			}
			else
			{
				dataTable = this.GetMorMamData(empNo, fromDate, 1);
			}
			List<MyTempKardex> list = new List<MyTempKardex>();
			int num3 = 0;
			if (p.IsMorDay)
			{
				if (dataTable.Rows.Count <= 0)
				{
					goto IL_E9E;
				}
				DataRow dataRow = dataTable.Rows[0];
				int remainedDays2;
				int remainedHours2;
				this.GetDayAndTimestring(empNo, num2, out remainedDays2, p.IsMorDay, out remainedHours2);
				long value = Convert.ToInt64(dataRow["RefNumber"]);
				double num4 = Convert.ToDouble(dataRow["REQUESTED"]);
				double value2 = Convert.ToDouble(dataRow["S_DATE"]);
				double value3 = Convert.ToDouble(dataRow["E_DATE"]);
				double value4 = Convert.ToDouble(dataRow["ISU_DATE"]);
				short num5 = Convert.ToInt16((dataRow["INC_TYP"] is DBNull || dataRow["INC_TYP"] == null) ? 0 : dataRow["INC_TYP"]);
				list.Add(new MyTempKardex(new long?(value), new double?(empNo), new double?(num2), new double?(value2), new double?(value3), new double?(num4), new double?(0.0), new double?(value4), new short?(num5), (num5 == 0) ? "" : num5.ToString(), dataRow["BABAT"].ToString(), new bool?(false)));
				if (num5 >= 6)
				{
					list[num3].Remtime = new double?(num4 + num2);
					this.GetDayAndTimestring(empNo, list[num3].Remtime.Value, out remainedDays2, p.IsMorDay, out remainedHours2);
					list[num3].RemTimestr = this.DayTimeStr(remainedDays2, remainedHours2);
					Inc_ += num4;
				}
				else
				{
					if (num2 > num4)
					{
						list[num3].Remtime = new double?(num2 - num4);
					}
					else if (num2 < 0.0)
					{
						list[num3].Remtime = new double?((num4 + num2 * -1.0) * -1.0);
					}
					else
					{
						list[num3].Remtime = new double?((num4 - num2) * -1.0);
					}
					this.GetDayAndTimestring(empNo, list[num3].Remtime.Value, out remainedDays2, p.IsMorDay, out remainedHours2);
					list[num3].RemTimestr = this.DayTimeStr(remainedDays2, remainedHours2);
					Dec_ += num4;
				}
				double value5 = list[num3].Remtime.Value;
				int num6 = 0;
			    IEnumerator enumerator = dataTable.Rows.GetEnumerator();
				{
					while (enumerator.MoveNext())
					{
						DataRow dataRow2 = (DataRow)enumerator.Current;
						num6++;
						if (num6 != 1)
						{
							num3++;
							value = Convert.ToInt64((dataRow2["RefNumber"] is DBNull || dataRow2["RefNumber"] == null) ? 0 : dataRow2["RefNumber"]);
							num4 = Convert.ToDouble((dataRow2["REQUESTED"] is DBNull || dataRow2["REQUESTED"] == null) ? 0 : dataRow2["REQUESTED"]);
							value2 = Convert.ToDouble((dataRow2["S_DATE"] is DBNull || dataRow2["S_DATE"] == null) ? 0 : dataRow2["S_DATE"]);
							value3 = Convert.ToDouble((dataRow2["E_DATE"] is DBNull || dataRow2["E_DATE"] == null) ? 0 : dataRow2["E_DATE"]);
							value4 = Convert.ToDouble((dataRow2["ISU_DATE"] is DBNull || dataRow2["ISU_DATE"] == null) ? 0 : dataRow2["ISU_DATE"]);
							num5 = Convert.ToInt16((dataRow2["INC_TYP"] is DBNull || dataRow2["INC_TYP"] == null) ? 0 : dataRow2["INC_TYP"]);
							this.GetDayAndTimestring(empNo, value5, out remainedDays2, p.IsMorDay, out remainedHours2);
							list.Add(new MyTempKardex(new long?(value), new double?(empNo), new double?(value5), new double?(value2), new double?(value3), new double?(num4), new double?(0.0), new double?(value4), new short?(num5), (num5 == 0) ? "" : num5.ToString(), dataRow2["BABAT"].ToString(), new bool?(false)));
							if (num5 >= 6)
							{
								list[num3].Remtime = new double?(num4 + value5);
								this.GetDayAndTimestring(empNo, list[num3].Remtime.Value, out remainedDays2, p.IsMorDay, out remainedHours2);
								list[num3].RemTimestr = this.DayTimeStr(remainedDays2, remainedHours2);
								Inc_ += num4;
							}
							else
							{
								if (value5 > num4)
								{
									list[num3].Remtime = new double?(value5 - num4);
								}
								else if (value5 < 0.0)
								{
									list[num3].Remtime = new double?((num4 + value5 * -1.0) * -1.0);
								}
								else
								{
									list[num3].Remtime = new double?((num4 - value5) * -1.0);
								}
								this.GetDayAndTimestring(empNo, list[num3].Remtime.Value, out remainedDays2, p.IsMorDay, out remainedHours2);
								list[num3].RemTimestr = this.DayTimeStr(remainedDays2, remainedHours2);
								Dec_ += num4;
							}
							value5 = list[num3].Remtime.Value;
						}
					}
					goto IL_E9E;
				}
			}
			num3 = 0;
			if (dataTable.Rows.Count > 0)
			{
				DataRow dataRow = dataTable.Rows[0];
				long value = Convert.ToInt64(dataRow["RefNumber"]);
				double num4 = Convert.ToDouble(dataRow["REQUESTED"]);
				double value2 = Convert.ToDouble(dataRow["S_DATE"]);
				double value3 = Convert.ToDouble(dataRow["E_DATE"]);
				double value4 = Convert.ToDouble(dataRow["ISU_DATE"]);
				short num5 = Convert.ToInt16((dataRow["INC_TYP"] is DBNull || dataRow["INC_TYP"] == null) ? 0 : dataRow["INC_TYP"]);
				list.Add(new MyTempKardex(new long?(value), new double?(empNo), new double?(num2), new double?(value2), new double?(value3), new double?(num4), new double?(0.0), new double?(value4), new short?(num5), num5.ToString(), dataRow["BABAT"].ToString(), new bool?(false)));
				if (num5 >= 6)
				{
					list[num3].Remtime = new double?((double)ShDate.TimePlus((int)num4, (int)num2));
					int remainedDays2;
					int remainedHours2;
					this.GetDayAndTimestring(empNo, list[num3].Remtime.Value, out remainedDays2, p.IsMorDay, out remainedHours2);
					list[num3].RemTimestr = this.DayTimeStr(remainedDays2, remainedHours2);
					Inc_ = (double)ShDate.TimePlus((int)Inc_, (int)num4);
				}
				else
				{
					if (num2 > num4)
					{
						list[num3].Remtime = new double?((double)Kardex.TimeSub((int)num2, (int)num4));
					}
					else if (num2 < 0.0)
					{
						list[num3].Remtime = new double?((double)(ShDate.TimePlus((int)num4, (int)num2 * -1) * -1));
					}
					else
					{
						list[num3].Remtime = new double?((double)(Kardex.TimeSub((int)num4, (int)num2) * -1));
					}
					int remainedDays2;
					int remainedHours2;
					this.GetDayAndTimestring(empNo, list[num3].Remtime.Value, out remainedDays2, p.IsMorDay, out remainedHours2);
					list[num3].RemTimestr = this.DayTimeStr(remainedDays2, remainedHours2);
					Dec_ = (double)ShDate.TimePlus((int)Dec_, (int)num4);
				}
				double value5 = list[num3].Remtime.Value;
				int num7 = 0;
				foreach (DataRow dataRow3 in dataTable.Rows)
				{
					num7++;
					if (num7 != 1)
					{
						num3++;
						value = Convert.ToInt64((dataRow3["RefNumber"] is DBNull || dataRow3["RefNumber"] == null) ? 0 : dataRow3["RefNumber"]);
						num4 = Convert.ToDouble((dataRow3["REQUESTED"] is DBNull || dataRow3["REQUESTED"] == null) ? 0 : dataRow3["REQUESTED"]);
						value2 = Convert.ToDouble((dataRow3["S_DATE"] is DBNull || dataRow3["S_DATE"] == null) ? 0 : dataRow3["S_DATE"]);
						value3 = Convert.ToDouble((dataRow3["E_DATE"] is DBNull || dataRow3["E_DATE"] == null) ? 0 : dataRow3["E_DATE"]);
						value4 = Convert.ToDouble((dataRow3["ISU_DATE"] is DBNull || dataRow3["ISU_DATE"] == null) ? 0 : dataRow3["ISU_DATE"]);
						num5 = Convert.ToInt16((dataRow3["INC_TYP"] is DBNull || dataRow3["INC_TYP"] == null) ? 0 : dataRow3["INC_TYP"]);
						list.Add(new MyTempKardex(new long?(value), new double?(empNo), new double?(value5), new double?(value2), new double?(value3), new double?(num4), new double?(0.0), new double?(value4), new short?(num5), num5.ToString(), dataRow3["BABAT"].ToString(), new bool?(false)));
						if (num5 >= 6)
						{
							list[num3].Remtime = new double?((double)ShDate.TimePlus((int)num4, (int)value5));
							int remainedDays2;
							int remainedHours2;
							this.GetDayAndTimestring(empNo, list[num3].Remtime.Value, out remainedDays2, p.IsMorDay, out remainedHours2);
							list[num3].RemTimestr = this.DayTimeStr(remainedDays2, remainedHours2);
							Inc_ = (double)ShDate.TimePlus((int)Inc_, (int)num4);
						}
						else
						{
							if (value5 > num4)
							{
								list[num3].Remtime = new double?((double)Kardex.TimeSub((int)value5, (int)num4));
							}
							else if (value5 < 0.0)
							{
								list[num3].Remtime = new double?((double)(ShDate.TimePlus((int)num4, (int)value5 * -1) * -1));
							}
							else
							{
								list[num3].Remtime = new double?((double)(Kardex.TimeSub((int)num4, (int)value5) * -1));
							}
							int remainedDays2;
							int remainedHours2;
							this.GetDayAndTimestring(empNo, list[num3].Remtime.Value, out remainedDays2, p.IsMorDay, out remainedHours2);
							list[num3].RemTimestr = this.DayTimeStr(remainedDays2, remainedHours2);
							Dec_ = (double)ShDate.TimePlus((int)Dec_, (int)num4);
						}
						value5 = list[num3].Remtime.Value;
					}
				}
			}
			if (dataTable.Rows.Count <= 0)
			{
				num = num2;
			}
			else
			{
				List<MyTempKardex> list2 = (from c in list
				where c.SDate <= fromDate
				select c).ToList<MyTempKardex>();
				num = (isForceLeaveInNextYear ? list2[0].Saved.Value : list2[0].Remtime.Value);
				double value6 = list2[0].SDate.Value;
				int num8 = 0;
				while (num8 < list2.Count && (list2[num8].SDate == fromDate || list2[0].SDate == value6))
				{
					num = (isForceLeaveInNextYear ? list2[num8].Saved.Value : list2[num8].Remtime.Value);
					num8++;
				}
			}
			IL_E9E:
			this.GetDayAndTimestring(empNo, num, out remainedDays, p.IsMorDay, out remainedHours);
			outMyTempKardex = list;
			return num;
		}

		// Token: 0x060000F1 RID: 241 RVA: 0x0000751C File Offset: 0x0000571C
		public int kasr_Esteh(double empNo, DateTime date)
		{
			int value = 0;
			int num = 0;
			DataRow dataByEmpNo = WorkFlowsDatabase.Instance.Employee.GetDataByEmpNo(empNo);
			if (Convert.ToBoolean(dataByEmpNo["IS_UNIQ"]))
			{
				switch (Convert.ToInt32(dataByEmpNo["Mor_Kasr"]))
				{
				case 1:
					WorkFlowsDatabase.Instance.Groups.FindGrpsByEmpAndDate(empNo, date, Convert.ToInt16(dataByEmpNo["GRP_NO"]), ref value);
					num = Convert.ToInt32(WorkFlowsDatabase.Instance.Shifts.GetDataByShiftNo(Convert.ToInt16(value))["SHIF_TIME"]);
					break;
				case 2:
					WorkFlowsDatabase.Instance.Groups.FindGrpsByEmpAndDate(empNo, date, Convert.ToInt16(dataByEmpNo["GRP_NO"]), ref value);
					num = Convert.ToInt32(WorkFlowsDatabase.Instance.Shifts.GetDataByShiftNo(Convert.ToInt16(value))["KASR_GH"]);
					break;
				case 3:
					num = Convert.ToInt32(dataByEmpNo["KASR_ESTH"]);
					break;
				}
			}
			else
			{
				value = 0;
				short grpNo = WorkFlowsDatabase.Instance.Groups.FindGrpsByEmpAndDate(empNo, date, Convert.ToInt16(dataByEmpNo["GRP_NO"]), ref value);
				DataRow dataByGrpNo = WorkFlowsDatabase.Instance.Groups.GetDataByGrpNo(grpNo);
				switch (Convert.ToInt32(dataByGrpNo["Mor_Kasr"]))
				{
				case 1:
					num = Convert.ToInt32(WorkFlowsDatabase.Instance.Shifts.GetDataByShiftNo(Convert.ToInt16(value))["SHIF_TIME"]);
					break;
				case 2:
					num = Convert.ToInt32(WorkFlowsDatabase.Instance.Shifts.GetDataByShiftNo(Convert.ToInt16(value))["KASR_GH"]);
					break;
				case 3:
					num = Convert.ToInt32(dataByGrpNo["KASR_ESTH"]);
					break;
				}
			}
			if (num == 0)
			{
				num = 720;
			}
			return num;
		}

		// Token: 0x060000F2 RID: 242 RVA: 0x000076FC File Offset: 0x000058FC
		public static int DivTime(int time1, int time2)
		{
			int result;
			if (time1 != 0 & time2 != 0)
			{
				int num = time1 % 100;
				int num2 = time1 / 100;
				int num3 = time2 % 100;
				int arg_28_0 = time2 / 100;
				int num4 = num2 * 60 + num;
				int num5 = arg_28_0 * 60 + num3;
				result = num4 / num5;
			}
			else if (time1 == 0)
			{
				result = 0;
			}
			else
			{
				result = -1;
			}
			return result;
		}
	}
}
