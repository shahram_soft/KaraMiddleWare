﻿using System;

namespace PW.WorkFlows.SqlDal
{
	// Token: 0x0200001F RID: 31
	public class ManagerReplace : BaseDAL
	{
		// Token: 0x06000157 RID: 343 RVA: 0x00003C40 File Offset: 0x00001E40
		public ManagerReplace(WorkFlowsDatabase owner) : base(owner)
		{
		}

		// Token: 0x06000158 RID: 344 RVA: 0x0000AC0C File Offset: 0x00008E0C
		public int Insert(double oldManagerEmpNo, double newManagerEmpNo, double secNo, string userName, bool transferNewManager, double otherManagerEmpNo, bool revokeConferment, bool revokeSubstitute)
		{
			int result;
			try
			{
				result = base.ExecuteNonQuery("WF_ManagerReplaceInsert", new object[]
				{
					oldManagerEmpNo,
					newManagerEmpNo,
					secNo,
					userName,
					transferNewManager,
					otherManagerEmpNo,
					revokeConferment,
					revokeSubstitute
				});
			}
			catch
			{
				result = 0;
			}
			return result;
		}
	}
}
