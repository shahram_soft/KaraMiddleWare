﻿using System;
using System.Data;

namespace PW.WorkFlows.SqlDal
{
	// Token: 0x02000015 RID: 21
	public class Help : BaseDAL
	{
		// Token: 0x060000F5 RID: 245 RVA: 0x00003C40 File Offset: 0x00001E40
		public Help(WorkFlowsDatabase owner) : base(owner)
		{
		}

		// Token: 0x060000F6 RID: 246 RVA: 0x000077B1 File Offset: 0x000059B1
		public DataTable GetHelp()
		{
			return base.ExecuteDataTable(CommandType.Text, "SELECT * FROM WF_Help wh");
		}

		// Token: 0x060000F7 RID: 247 RVA: 0x000077C0 File Offset: 0x000059C0
		public string GetHelpPageName(string pageName)
		{
			string result;
			try
			{
				object obj = base.ExecuteScalar(CommandType.Text, "SELECT wh.HelpPageName FROM WF_Help wh WHERE wh.PageName='" + pageName + "'");
				result = ((obj != null) ? obj.ToString() : "");
			}
			catch
			{
				result = pageName;
			}
			return result;
		}
	}
}
