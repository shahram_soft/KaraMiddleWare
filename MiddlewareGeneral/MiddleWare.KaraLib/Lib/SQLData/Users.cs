﻿using System;
using System.Data;
using System.Data.SqlClient;
using MiddleWare.KaraLib;
using MiddleWare.KaraLib.ViewModels;


namespace PW.WorkFlows.SqlDal
{
	// Token: 0x02000031 RID: 49
	public class Users : BaseDAL
	{
		// Token: 0x0600024B RID: 587 RVA: 0x00003C40 File Offset: 0x00001E40
		public Users(WorkFlowsDatabase owner) : base(owner)
		{
		}

		// Token: 0x0600024C RID: 588 RVA: 0x00016552 File Offset: 0x00014752
		public DataTable UsersGetData()
		{
			return base.ExecuteDataTable("WF_UsersGetData", null);
		}

		// Token: 0x0600024D RID: 589 RVA: 0x00016560 File Offset: 0x00014760
		public UserActiveRow GetByIdAndPassword(string userId, string password)
		{
			string text = "";
			UserActiveRow userActiveRow = null;
			using (SqlDataReader sqlDataReader = base.ExecuteReader("WF_UsersGetDataByIdAndPassword", new object[]
			{
				userId,
				UtilityManagers.EncryptPassword(password, userId, 12)
			}))
			{
				if (sqlDataReader.Read())
				{
					text = sqlDataReader["USERS_NAME"].ToString();
					userActiveRow = new UserActiveRow
					{
						EmpNo = Convert.ToDouble((sqlDataReader["USERS_EmpNo"] is DBNull || sqlDataReader["USERS_EmpNo"] == null) ? 0 : sqlDataReader["USERS_EmpNo"]),
						Section = "",
						IsSupervisor = false,
						IsManager = false,
						IsSubstitute = false,
						IsEmpMngr = false,
						Name = "",
						Family = "",
						GrpNo = 0,
						EndDate = DateTime.Now,
						IsUniq = false,
						IsWarden = true,
						WardenDayCount = 9999,
						WardenJustInShift = false,
						WardenIsShowRequest = new bool?(true),
						WardenIsShowReview = new bool?(true),
						IsParallelApprove = false
					};
				}
			}
			if (!userId.ToLower().Trim().Equals("admin") && !text.ToUpper().Trim().Equals("WF"))
			{
				return null;
			}
			if (text.ToUpper().Trim().Equals("WF"))
			{
				userActiveRow.Family = userId;
			}
			return userActiveRow;
		}

		// Token: 0x0600024E RID: 590 RVA: 0x000166F0 File Offset: 0x000148F0
		public UserActiveRow GetById(string userId)
		{
			UserActiveRow result = null;
			using (SqlDataReader sqlDataReader = base.ExecuteReader("WF_UsersGetById", new object[]
			{
				userId
			}))
			{
				if (sqlDataReader.Read())
				{
					result = new UserActiveRow
					{
						EmpNo = Convert.ToDouble((sqlDataReader["USERS_EmpNo"] is DBNull || sqlDataReader["USERS_EmpNo"] == null) ? 0 : sqlDataReader["USERS_EmpNo"]),
						Section = "",
						IsSupervisor = false,
						IsManager = false,
						IsSubstitute = false,
						IsEmpMngr = false,
						Name = "",
						Family = "",
						GrpNo = 0,
						EndDate = DateTime.Now,
						IsUniq = false,
						IsWarden = true,
						WardenDayCount = 9999,
						WardenJustInShift = false,
						WardenIsShowRequest = new bool?(true),
						WardenIsShowReview = new bool?(true),
						IsParallelApprove = false
					};
				}
			}
			return result;
		}

		// Token: 0x0600024F RID: 591 RVA: 0x00016810 File Offset: 0x00014A10
		public bool GetUserAccessTimeGroup(double empNo)
		{
			bool result = false;
			string commandText = "SELECT e.EMP_NO,wtg.*  FROM EMPLOYEE e \nJOIN WFAccessTimeGroup wtg ON  e.AccessTimeGroupID = wtg.AccessTimeGroupID \nWHERE  e.AccessTimeGroupID IS NOT NULL AND e.EMP_NO=" + empNo;
			DataRow dataRow = base.ExecuteDataRow(CommandType.Text, commandText);
			if (dataRow == null)
			{
				return true;
			}
			string a = dataRow["GroupStatus"].ToString();
			if (!(a == "0"))
			{
				if (!(a == "1"))
				{
					if (!(a == "2"))
					{
						if (a == "3")
						{
							string text = dataRow["IncTolerance"].ToString();
							text = UtilityManagers.ConvertToFourDigitNumber(text);
							TimeSpan t = new TimeSpan(Convert.ToInt32(text.Substring(0, 2)), Convert.ToInt32(text.Substring(2, 2)), 0);
							text = dataRow["DecTolerance"].ToString();
							text = UtilityManagers.ConvertToFourDigitNumber(text);
							TimeSpan t2 = new TimeSpan(Convert.ToInt32(text.Substring(0, 2)), Convert.ToInt32(text.Substring(2, 2)), 0);
							result = (DateTime.Now.TimeOfDay >= t2 && DateTime.Now.TimeOfDay <= t);
						}
					}
					else
					{
						object obj = base.ExecuteScalar(CommandType.Text, string.Concat(new object[]
						{
							"SELECT H",
							DateTime.Now.Hour,
							" FROM WFUserAccessTime wat WHERE  wat.AccessTimeGroupID = ",
							dataRow["AccessTimeGroupID"],
							" AND wat.H",
							DateTime.Now.Hour,
							" = 1 AND wat.DayNo=",
							ShDate.CurrentDayOfWeek()
						}));
						if (obj != null)
						{
							result = Convert.ToBoolean(obj);
						}
					}
				}
				else
				{
					TimeSpan[] startTimeAndEndTimeByEmp = this.GetStartTimeAndEndTimeByEmp(empNo);
					if (startTimeAndEndTimeByEmp != null)
					{
						TimeSpan timeSpan = (TimeSpan)startTimeAndEndTimeByEmp.GetValue(0);
						TimeSpan timeSpan2 = (TimeSpan)startTimeAndEndTimeByEmp.GetValue(1);
						string text = dataRow["IncTolerance"].ToString();
						text = UtilityManagers.ConvertToFourDigitNumber(text);
						TimeSpan ts = new TimeSpan(Convert.ToInt32(text.Substring(0, 2)), Convert.ToInt32(text.Substring(2, 2)), 0);
						text = dataRow["DecTolerance"].ToString();
						text = UtilityManagers.ConvertToFourDigitNumber(text);
						TimeSpan ts2 = new TimeSpan(Convert.ToInt32(text.Substring(0, 2)), Convert.ToInt32(text.Substring(2, 2)), 0);
						result = (DateTime.Now.TimeOfDay >= timeSpan.Subtract(ts2) && DateTime.Now.TimeOfDay <= timeSpan2.Add(ts));
					}
				}
			}
			else
			{
				result = true;
			}
			return result;
		}

		// Token: 0x06000250 RID: 592 RVA: 0x00016AB3 File Offset: 0x00014CB3
		public TimeSpan[] GetStartTimeAndEndTimeByEmp(double empNo)
		{
			return this.GetStartTimeAndEndTimeByDate(empNo, DateTime.Now);
		}

		// Token: 0x06000251 RID: 593 RVA: 0x00016AC4 File Offset: 0x00014CC4
		public TimeSpan[] GetStartTimeAndEndTimeByDate(double empNo, DateTime shiftDate)
		{
			DataRow sHiftData = WorkFlowsDatabase.Instance.Shifts.GetSHiftData(empNo, shiftDate.ToOADate(), (int)ShDate.YearOf(shiftDate), (int)ShDate.MonthOf(shiftDate), (int)ShDate.DayOf(shiftDate));
			if (sHiftData == null)
			{
				return null;
			}
			short shiftNo;
			if (short.TryParse(sHiftData["SHIFT_NO"].ToString().Trim(), out shiftNo))
			{
				return this.GetStartTimeAndEndTimeByShiftNo(shiftNo, sHiftData, shiftDate.ToOADate());
			}
			return null;
		}

		// Token: 0x06000252 RID: 594 RVA: 0x00016B30 File Offset: 0x00014D30
		public TimeSpan[] GetStartTimeAndEndTimeByShiftNo(short shiftNo, DataRow drShift, double CurrDate)
		{
			if (shiftNo == 50 || shiftNo == 51 || shiftNo == 52)
			{
				return null;
			}
			if (drShift == null)
			{
				drShift = WorkFlowsDatabase.Instance.Shifts.GetDataByShiftNo(shiftNo);
			}
			ParmfileRow expr_35 = WorkFlowsDatabase.Instance.Parmfile.GetDataRow();
			double num = (double)expr_35.EndDateRamedan;
			double num2 = (double)expr_35.StartDateRamedan;
			TimeSpan timeSpan;
			TimeSpan timeSpan2;
			if (CurrDate >= num2 && CurrDate <= num)
			{
				string text = UtilityManagers.ConvertToFourDigitNumber(drShift["RAMEZAN_S"].ToString());
				timeSpan = new TimeSpan(Convert.ToInt32(text.Substring(0, 2)), Convert.ToInt32(text.Substring(2, 2)), 0);
				if (DateTime.Now.DayOfWeek == DayOfWeek.Thursday)
				{
					text = UtilityManagers.ConvertToFourDigitNumber(drShift["RAMEZAN_E5"].ToString());
					timeSpan2 = new TimeSpan(Convert.ToInt32(text.Substring(0, 2)), Convert.ToInt32(text.Substring(2, 2)), 0);
				}
				else
				{
					text = UtilityManagers.ConvertToFourDigitNumber(drShift["RAMEZAN_T"].ToString());
					TimeSpan ts = new TimeSpan(Convert.ToInt32(text.Substring(0, 2)), Convert.ToInt32(text.Substring(2, 2)), 0);
					timeSpan2 = timeSpan.Add(ts);
				}
			}
			else if (DateTime.Now.DayOfWeek == DayOfWeek.Thursday)
			{
				string text = UtilityManagers.ConvertToFourDigitNumber(drShift["END5_TIME"].ToString());
				timeSpan2 = new TimeSpan(Convert.ToInt32(text.Substring(0, 2)), Convert.ToInt32(text.Substring(2, 2)), 0);
				text = UtilityManagers.ConvertToFourDigitNumber(drShift["START_TIME"].ToString());
				timeSpan = new TimeSpan(Convert.ToInt32(text.Substring(0, 2)), Convert.ToInt32(text.Substring(2, 2)), 0);
			}
			else
			{
				string text = UtilityManagers.ConvertToFourDigitNumber(drShift["End_time"].ToString());
				timeSpan2 = new TimeSpan(Convert.ToInt32(text.Substring(0, 2)), Convert.ToInt32(text.Substring(2, 2)), 0);
				text = UtilityManagers.ConvertToFourDigitNumber(drShift["START_TIME"].ToString());
				timeSpan = new TimeSpan(Convert.ToInt32(text.Substring(0, 2)), Convert.ToInt32(text.Substring(2, 2)), 0);
			}
			return new TimeSpan[]
			{
				timeSpan,
				timeSpan2
			};
		}

		// Token: 0x06000253 RID: 595 RVA: 0x00016D68 File Offset: 0x00014F68
		public int Insert(string userId, string userPass, DateTime sDate, DateTime edate)
		{
			int result;
			try
			{
				result = base.ExecuteNonQuery("WF_InsertUsers", new object[]
				{
					userId,
					userPass,
					sDate,
					edate
				});
			}
			catch
			{
				result = 0;
			}
			return result;
		}

		// Token: 0x06000254 RID: 596 RVA: 0x00016DBC File Offset: 0x00014FBC
		public int Update(string userId, string userPass, DateTime sDate, DateTime edate)
		{
			int result;
			try
			{
				result = base.ExecuteNonQuery("WF_UpdateUsers", new object[]
				{
					userId,
					userPass,
					sDate,
					edate
				});
			}
			catch
			{
				result = 0;
			}
			return result;
		}

		// Token: 0x06000255 RID: 597 RVA: 0x00016E10 File Offset: 0x00015010
		public int Delete(string userId)
		{
			int result;
			try
			{
				result = base.ExecuteNonQuery("WF_DeleteUsers", new object[]
				{
					userId
				});
			}
			catch
			{
				result = 0;
			}
			return result;
		}

		// Token: 0x06000256 RID: 598 RVA: 0x00016E4C File Offset: 0x0001504C
		public DataTable GetUsers(string usersName)
		{
			return base.ExecuteDataTable(CommandType.Text, "SELECT * FROM Users u WHERE u.USERS_NAME='" + usersName + "'");
		}

		// Token: 0x06000257 RID: 599 RVA: 0x00016E65 File Offset: 0x00015065
		public DataTable GetUsers()
		{
			return this.GetUsers("WF");
		}
	}
}
