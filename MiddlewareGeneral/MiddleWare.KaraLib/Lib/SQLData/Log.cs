﻿using System;
using System.Data;

namespace PW.WorkFlows.SqlDal
{
	// Token: 0x02000014 RID: 20
	public class Log : BaseDAL
	{
		// Token: 0x060000F3 RID: 243 RVA: 0x00003C40 File Offset: 0x00001E40
		public Log(WorkFlowsDatabase owner) : base(owner)
		{
		}

		// Token: 0x060000F4 RID: 244 RVA: 0x00007748 File Offset: 0x00005948
		public void Insert(string logMsg, string currentPage, double empNo, string fullName)
		{
			if (!BaseDAL.IsLoged)
			{
				return;
			}
			string commandText = string.Concat(new object[]
			{
				"INSERT INTO WF_Log(LogMsg,CurrentPage,EMP_NO,FullName) \nVALUES('",
				logMsg,
				"','",
				currentPage,
				"',",
				empNo,
				",'",
				fullName,
				"')"
			});
			base.ExecuteNonQuery(CommandType.Text, commandText);
		}
	}
}
