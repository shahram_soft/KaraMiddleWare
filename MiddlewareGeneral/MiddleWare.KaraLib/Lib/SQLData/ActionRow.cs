﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiddleWare.KaraLib.Lib.SQLData
{
   public class ActionRow : BaseCls
    {
        public ActionRow()
        {
        }

        // Token: 0x0600001B RID: 27 RVA: 0x0000212C File Offset: 0x0000032C
        public ActionRow(DataRow dr)
        {
            if (dr == null)
            {
                return;
            }
            this.ActionId = base.CastInt(dr["ActionId"]);
            this.Fdesc = base.CastString(dr["Fdesc"]);
            this.IsPermission = base.CastBool(dr["IsPermission"]);
            this.IsShow = base.CastBool(dr["IsShow"]);
            this.RequestValidDays = base.CastInt(dr["RequestValidDays"]);
            this.LimitCount = base.CastInt(dr["LimitCount"]);
            this.PrintAccess = base.CastBool(dr["PrintAccess"]);
            this.NeedDesc = base.CastBool(dr["NeedDesc"]);
        }

        // Token: 0x0600001C RID: 28 RVA: 0x000021FC File Offset: 0x000003FC
        public ActionRow(IDataRecord dr)
        {
            if (dr == null)
            {
                return;
            }
            this.ActionId = base.CastInt(dr["ActionId"]);
            this.Fdesc = base.CastString(dr["Fdesc"]);
            this.IsPermission = base.CastBool(dr["IsPermission"]);
            this.IsShow = base.CastBool(dr["IsShow"]);
            this.RequestValidDays = base.CastInt(dr["RequestValidDays"]);
            this.LimitCount = base.CastInt(dr["LimitCount"]);
            this.PrintAccess = base.CastBool(dr["PrintAccess"]);
            this.NeedDesc = base.CastBool(dr["NeedDesc"]);
        }

        // Token: 0x1700000D RID: 13
        // (get) Token: 0x0600001D RID: 29 RVA: 0x000022CB File Offset: 0x000004CB
        // (set) Token: 0x0600001E RID: 30 RVA: 0x000022D3 File Offset: 0x000004D3
        public int ActionId
        {
            get;
            set;
        }

        // Token: 0x1700000E RID: 14
        // (get) Token: 0x0600001F RID: 31 RVA: 0x000022DC File Offset: 0x000004DC
        // (set) Token: 0x06000020 RID: 32 RVA: 0x000022E4 File Offset: 0x000004E4
        public string Fdesc
        {
            get;
            set;
        }

        // Token: 0x1700000F RID: 15
        // (get) Token: 0x06000021 RID: 33 RVA: 0x000022ED File Offset: 0x000004ED
        // (set) Token: 0x06000022 RID: 34 RVA: 0x000022F5 File Offset: 0x000004F5
        public bool IsPermission
        {
            get;
            set;
        }

        // Token: 0x17000010 RID: 16
        // (get) Token: 0x06000023 RID: 35 RVA: 0x000022FE File Offset: 0x000004FE
        // (set) Token: 0x06000024 RID: 36 RVA: 0x00002306 File Offset: 0x00000506
        public bool IsShow
        {
            get;
            set;
        }

        // Token: 0x17000011 RID: 17
        // (get) Token: 0x06000025 RID: 37 RVA: 0x0000230F File Offset: 0x0000050F
        // (set) Token: 0x06000026 RID: 38 RVA: 0x00002317 File Offset: 0x00000517
        public int RequestValidDays
        {
            get;
            set;
        }

        // Token: 0x17000012 RID: 18
        // (get) Token: 0x06000027 RID: 39 RVA: 0x00002320 File Offset: 0x00000520
        // (set) Token: 0x06000028 RID: 40 RVA: 0x00002328 File Offset: 0x00000528
        public int LimitCount
        {
            get;
            set;
        }

        // Token: 0x17000013 RID: 19
        // (get) Token: 0x06000029 RID: 41 RVA: 0x00002331 File Offset: 0x00000531
        // (set) Token: 0x0600002A RID: 42 RVA: 0x00002339 File Offset: 0x00000539
        public bool PrintAccess
        {
            get;
            set;
        }

        // Token: 0x17000014 RID: 20
        // (get) Token: 0x0600002B RID: 43 RVA: 0x00002342 File Offset: 0x00000542
        // (set) Token: 0x0600002C RID: 44 RVA: 0x0000234A File Offset: 0x0000054A
        public bool NeedDesc
        {
            get;
            set;
        }
    }
}
