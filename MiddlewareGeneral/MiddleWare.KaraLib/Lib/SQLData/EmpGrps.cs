﻿using System;
using System.Collections;
using System.Data;

namespace PW.WorkFlows.SqlDal
{
	// Token: 0x0200001B RID: 27
	public class EmpGrps : BaseDAL
	{
		// Token: 0x06000140 RID: 320 RVA: 0x00003C40 File Offset: 0x00001E40
		public EmpGrps(WorkFlowsDatabase owner) : base(owner)
		{
		}

		// Token: 0x06000141 RID: 321 RVA: 0x00009E8F File Offset: 0x0000808F
		public DataTable GrpsGetData()
		{
			return base.ExecuteDataTable("WF_Emp_GrpsGetData", null);
		}

		// Token: 0x06000142 RID: 322 RVA: 0x00009EA0 File Offset: 0x000080A0
		public bool Insert(double empNo, double date, int grpNo, int? prevGrp)
		{
			return prevGrp.HasValue && base.ExecuteNonQuery("WF_Emp_GrpsInsert", new object[]
			{
				empNo,
				date,
				(short)grpNo,
				(short)prevGrp.Value
			}) >= 0;
		}

		// Token: 0x06000143 RID: 323 RVA: 0x00009EFC File Offset: 0x000080FC
		public bool ExistAnotherRequestByDate(double empNo, DateTime startDate, int duration, int type)
		{
			bool result = false;
			int num = 0;
			if (type == 102)
			{
				num = duration;
			}
			for (int i = 0; i <= num; i++)
			{
				DataTable dataByDate = WorkFlowsDatabase.Instance.Requests.GetDataByDate(empNo, startDate.Date.AddDays((double)i));
				if (dataByDate != null && dataByDate.Rows.Count > 0)
				{
				    var enumerator = dataByDate.Rows.GetEnumerator();
					{
						while (enumerator.MoveNext())
						{
							if (Convert.ToInt32(((DataRow)enumerator.Current)["Type"]) == type)
							{
								result = true;
							}
						}
					}
				}
			}
			return result;
		}
	}
}
