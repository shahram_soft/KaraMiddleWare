﻿using System;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Net.Mail;
using Microsoft.ApplicationBlocks.Data;

namespace PW.WorkFlows.SqlDal
{
	// Token: 0x0200002F RID: 47
	[DebuggerStepThrough]
	public class WorkFlowsDatabase : IDisposable
	{
		// Token: 0x1700000C RID: 12
		// (get) Token: 0x0600020A RID: 522 RVA: 0x00015564 File Offset: 0x00013764
		public BaseDAL BaseDAL
		{
			get
			{
				BaseDAL arg_19_0;
				if ((arg_19_0 = this._baseDAL) == null)
				{
					arg_19_0 = (this._baseDAL = new BaseDAL(this));
				}
				return arg_19_0;
			}
		}

		// Token: 0x1700000D RID: 13
		// (get) Token: 0x0600020B RID: 523 RVA: 0x0001558C File Offset: 0x0001378C
		public Condition Condition
		{
			get
			{
				Condition arg_19_0;
				if ((arg_19_0 = this._condition) == null)
				{
					arg_19_0 = (this._condition = new Condition(this));
				}
				return arg_19_0;
			}
		}

		// Token: 0x1700000E RID: 14
		// (get) Token: 0x0600020C RID: 524 RVA: 0x000155B4 File Offset: 0x000137B4
		public WFUserSections WFUserSections
		{
			get
			{
				WFUserSections arg_19_0;
				if ((arg_19_0 = this._wfUserSections) == null)
				{
					arg_19_0 = (this._wfUserSections = new WFUserSections(this));
				}
				return arg_19_0;
			}
		}

		// Token: 0x1700000F RID: 15
		// (get) Token: 0x0600020D RID: 525 RVA: 0x000155DC File Offset: 0x000137DC
		public DatabaseInfo DatabaseInfo
		{
			get
			{
				DatabaseInfo arg_19_0;
				if ((arg_19_0 = this._DatabaseInfo) == null)
				{
					arg_19_0 = (this._DatabaseInfo = new DatabaseInfo(this));
				}
				return arg_19_0;
			}
		}

		// Token: 0x17000010 RID: 16
		// (get) Token: 0x0600020E RID: 526 RVA: 0x00015604 File Offset: 0x00013804
		public WFPrintSeting PrintSeting
		{
			get
			{
				WFPrintSeting arg_19_0;
				if ((arg_19_0 = this._wfPrintSeting) == null)
				{
					arg_19_0 = (this._wfPrintSeting = new WFPrintSeting(this));
				}
				return arg_19_0;
			}
		}

		// Token: 0x17000011 RID: 17
		// (get) Token: 0x0600020F RID: 527 RVA: 0x0001562C File Offset: 0x0001382C
		public News News
		{
			get
			{
				News arg_19_0;
				if ((arg_19_0 = this._news) == null)
				{
					arg_19_0 = (this._news = new News(this));
				}
				return arg_19_0;
			}
		}

		// Token: 0x17000012 RID: 18
		// (get) Token: 0x06000210 RID: 528 RVA: 0x00015654 File Offset: 0x00013854
		public BaseInfo BaseInfo
		{
			get
			{
				BaseInfo arg_19_0;
				if ((arg_19_0 = this._baseInfo) == null)
				{
					arg_19_0 = (this._baseInfo = new BaseInfo(this));
				}
				return arg_19_0;
			}
		}

		// Token: 0x17000013 RID: 19
		// (get) Token: 0x06000211 RID: 529 RVA: 0x0001567C File Offset: 0x0001387C
		public Quotation Quotation
		{
			get
			{
				Quotation arg_19_0;
				if ((arg_19_0 = this._quotation) == null)
				{
					arg_19_0 = (this._quotation = new Quotation(this));
				}
				return arg_19_0;
			}
		}

		// Token: 0x17000014 RID: 20
		// (get) Token: 0x06000212 RID: 530 RVA: 0x000156A4 File Offset: 0x000138A4
		public RequestParallelApproval RequestParallelApproval
		{
			get
			{
				RequestParallelApproval arg_19_0;
				if ((arg_19_0 = this._requestParallelApproval) == null)
				{
					arg_19_0 = (this._requestParallelApproval = new RequestParallelApproval(this));
				}
				return arg_19_0;
			}
		}

		// Token: 0x17000015 RID: 21
		// (get) Token: 0x06000213 RID: 531 RVA: 0x000156CC File Offset: 0x000138CC
		public ParallelApprove ParallelApprove
		{
			get
			{
				ParallelApprove arg_19_0;
				if ((arg_19_0 = this._parallelApprove) == null)
				{
					arg_19_0 = (this._parallelApprove = new ParallelApprove(this));
				}
				return arg_19_0;
			}
		}

		// Token: 0x17000016 RID: 22
		// (get) Token: 0x06000214 RID: 532 RVA: 0x000156F4 File Offset: 0x000138F4
		public Kardex Kardex
		{
			get
			{
				Kardex arg_19_0;
				if ((arg_19_0 = this._kardex) == null)
				{
					arg_19_0 = (this._kardex = new Kardex(this));
				}
				return arg_19_0;
			}
		}

		// Token: 0x17000017 RID: 23
		// (get) Token: 0x06000215 RID: 533 RVA: 0x0001571C File Offset: 0x0001391C
		public ManagerReplace ManagerReplace
		{
			get
			{
				ManagerReplace arg_19_0;
				if ((arg_19_0 = this._managerReplace) == null)
				{
					arg_19_0 = (this._managerReplace = new ManagerReplace(this));
				}
				return arg_19_0;
			}
		}

		// Token: 0x17000018 RID: 24
		// (get) Token: 0x06000216 RID: 534 RVA: 0x00015744 File Offset: 0x00013944
		public LogMorMam LogMorMam
		{
			get
			{
				LogMorMam arg_19_0;
				if ((arg_19_0 = this._logMorMam) == null)
				{
					arg_19_0 = (this._logMorMam = new LogMorMam(this));
				}
				return arg_19_0;
			}
		}

		// Token: 0x17000019 RID: 25
		// (get) Token: 0x06000217 RID: 535 RVA: 0x0001576C File Offset: 0x0001396C
		public Log Log
		{
			get
			{
				Log arg_19_0;
				if ((arg_19_0 = this._log) == null)
				{
					arg_19_0 = (this._log = new Log(this));
				}
				return arg_19_0;
			}
		}

		// Token: 0x1700001A RID: 26
		// (get) Token: 0x06000218 RID: 536 RVA: 0x00015794 File Offset: 0x00013994
		public Attachment Attachment
		{
			get
			{
				Attachment arg_19_0;
				if ((arg_19_0 = this._attachment) == null)
				{
					arg_19_0 = (this._attachment = new Attachment(this.ToString()));
				}
				return arg_19_0;
			}
		}

		// Token: 0x1700001B RID: 27
		// (get) Token: 0x06000219 RID: 537 RVA: 0x000157BC File Offset: 0x000139BC
		public Help Help
		{
			get
			{
				Help arg_19_0;
				if ((arg_19_0 = this._help) == null)
				{
					arg_19_0 = (this._help = new Help(this));
				}
				return arg_19_0;
			}
		}

		// Token: 0x1700001C RID: 28
		// (get) Token: 0x0600021A RID: 538 RVA: 0x000157E4 File Offset: 0x000139E4
		public WfEmpCard WfEmpCard
		{
			get
			{
				WfEmpCard arg_19_0;
				if ((arg_19_0 = this._wfEmpCard) == null)
				{
					arg_19_0 = (this._wfEmpCard = new WfEmpCard(this));
				}
				return arg_19_0;
			}
		}

		// Token: 0x1700001D RID: 29
		// (get) Token: 0x0600021B RID: 539 RVA: 0x0001580C File Offset: 0x00013A0C
		public UserAccessTime UserAccessTime
		{
			get
			{
				UserAccessTime arg_19_0;
				if ((arg_19_0 = this._userAccessTime) == null)
				{
					arg_19_0 = (this._userAccessTime = new UserAccessTime(this));
				}
				return arg_19_0;
			}
		}

		// Token: 0x1700001E RID: 30
		// (get) Token: 0x0600021C RID: 540 RVA: 0x00015834 File Offset: 0x00013A34
		public AccessTimeGroup AccessTimeGroup
		{
			get
			{
				AccessTimeGroup arg_19_0;
				if ((arg_19_0 = this._accessTimeGroup) == null)
				{
					arg_19_0 = (this._accessTimeGroup = new AccessTimeGroup(this));
				}
				return arg_19_0;
			}
		}

		// Token: 0x1700001F RID: 31
		// (get) Token: 0x0600021D RID: 541 RVA: 0x0001585C File Offset: 0x00013A5C
		public LimitedManager LimitedManager
		{
			get
			{
				LimitedManager arg_19_0;
				if ((arg_19_0 = this._limitedManager) == null)
				{
					arg_19_0 = (this._limitedManager = new LimitedManager(this));
				}
				return arg_19_0;
			}
		}

		// Token: 0x17000020 RID: 32
		// (get) Token: 0x0600021E RID: 542 RVA: 0x00015884 File Offset: 0x00013A84
		public Conferment Conferment
		{
			get
			{
				Conferment arg_19_0;
				if ((arg_19_0 = this._conferment) == null)
				{
					arg_19_0 = (this._conferment = new Conferment(this));
				}
				return arg_19_0;
			}
		}

		// Token: 0x17000021 RID: 33
		// (get) Token: 0x0600021F RID: 543 RVA: 0x000158AC File Offset: 0x00013AAC
		public ActionSection ActionSection
		{
			get
			{
				ActionSection arg_19_0;
				if ((arg_19_0 = this._actionSection) == null)
				{
					arg_19_0 = (this._actionSection = new ActionSection(this));
				}
				return arg_19_0;
			}
		}

		// Token: 0x17000022 RID: 34
		// (get) Token: 0x06000220 RID: 544 RVA: 0x000158D4 File Offset: 0x00013AD4
		public Reports Report
		{
			get
			{
				Reports arg_19_0;
				if ((arg_19_0 = this._report) == null)
				{
					arg_19_0 = (this._report = new Reports(this));
				}
				return arg_19_0;
			}
		}

		// Token: 0x17000023 RID: 35
		// (get) Token: 0x06000221 RID: 545 RVA: 0x000158FC File Offset: 0x00013AFC
		public EmpRequestActions EmpRequestActions
		{
			get
			{
				EmpRequestActions arg_19_0;
				if ((arg_19_0 = this._empRequestActions) == null)
				{
					arg_19_0 = (this._empRequestActions = new EmpRequestActions(this));
				}
				return arg_19_0;
			}
		}

		// Token: 0x17000024 RID: 36
		// (get) Token: 0x06000222 RID: 546 RVA: 0x00015924 File Offset: 0x00013B24
		public Action Action
		{
			get
			{
				Action arg_19_0;
				if ((arg_19_0 = this._action) == null)
				{
					arg_19_0 = (this._action = new Action(this));
				}
				return arg_19_0;
			}
		}

		// Token: 0x17000025 RID: 37
		// (get) Token: 0x06000223 RID: 547 RVA: 0x0001594C File Offset: 0x00013B4C
		public Cards Cards
		{
			get
			{
				Cards arg_19_0;
				if ((arg_19_0 = this._cards) == null)
				{
					arg_19_0 = (this._cards = new Cards(this));
				}
				return arg_19_0;
			}
		}

		// Token: 0x17000026 RID: 38
		// (get) Token: 0x06000224 RID: 548 RVA: 0x00015974 File Offset: 0x00013B74
		public DataFile DataFile
		{
			get
			{
				DataFile arg_19_0;
				if ((arg_19_0 = this._dataFile) == null)
				{
					arg_19_0 = (this._dataFile = new DataFile(this));
				}
				return arg_19_0;
			}
		}

		// Token: 0x17000027 RID: 39
		// (get) Token: 0x06000225 RID: 549 RVA: 0x0001599C File Offset: 0x00013B9C
		public EmpGrps EmpGrps
		{
			get
			{
				EmpGrps arg_19_0;
				if ((arg_19_0 = this._empGrps) == null)
				{
					arg_19_0 = (this._empGrps = new EmpGrps(this));
				}
				return arg_19_0;
			}
		}

		// Token: 0x17000028 RID: 40
		// (get) Token: 0x06000226 RID: 550 RVA: 0x000159C4 File Offset: 0x00013BC4
		public Employee Employee
		{
			get
			{
				Employee arg_19_0;
				if ((arg_19_0 = this._employee) == null)
				{
					arg_19_0 = (this._employee = new Employee(this));
				}
				return arg_19_0;
			}
		}

		// Token: 0x17000029 RID: 41
		// (get) Token: 0x06000227 RID: 551 RVA: 0x000159EC File Offset: 0x00013BEC
		public EmployeeManagers EmployeeManagers
		{
			get
			{
				EmployeeManagers arg_19_0;
				if ((arg_19_0 = this._employeeManagers) == null)
				{
					arg_19_0 = (this._employeeManagers = new EmployeeManagers(this));
				}
				return arg_19_0;
			}
		}

		// Token: 0x1700002A RID: 42
		// (get) Token: 0x06000228 RID: 552 RVA: 0x00015A14 File Offset: 0x00013C14
		public EmpTypes EmpTypes
		{
			get
			{
				EmpTypes arg_19_0;
				if ((arg_19_0 = this._empTypes) == null)
				{
					arg_19_0 = (this._empTypes = new EmpTypes(this));
				}
				return arg_19_0;
			}
		}

		// Token: 0x1700002B RID: 43
		// (get) Token: 0x06000229 RID: 553 RVA: 0x00015A3C File Offset: 0x00013C3C
		public Groups Groups
		{
			get
			{
				Groups arg_19_0;
				if ((arg_19_0 = this._groups) == null)
				{
					arg_19_0 = (this._groups = new Groups(this));
				}
				return arg_19_0;
			}
		}

		// Token: 0x1700002C RID: 44
		// (get) Token: 0x0600022A RID: 554 RVA: 0x00015A64 File Offset: 0x00013C64
		public MorMam MorMam
		{
			get
			{
				MorMam arg_19_0;
				if ((arg_19_0 = this._morMam) == null)
				{
					arg_19_0 = (this._morMam = new MorMam(this));
				}
				return arg_19_0;
			}
		}

		// Token: 0x1700002D RID: 45
		// (get) Token: 0x0600022B RID: 555 RVA: 0x00015A8C File Offset: 0x00013C8C
		public OperationTypes OperationTypes
		{
			get
			{
				OperationTypes arg_19_0;
				if ((arg_19_0 = this._operationTypes) == null)
				{
					arg_19_0 = (this._operationTypes = new OperationTypes(this));
				}
				return arg_19_0;
			}
		}

		// Token: 0x1700002E RID: 46
		// (get) Token: 0x0600022C RID: 556 RVA: 0x00015AB4 File Offset: 0x00013CB4
		public Overs Overs
		{
			get
			{
				Overs arg_19_0;
				if ((arg_19_0 = this._overs) == null)
				{
					arg_19_0 = (this._overs = new Overs(this));
				}
				return arg_19_0;
			}
		}

		// Token: 0x1700002F RID: 47
		// (get) Token: 0x0600022D RID: 557 RVA: 0x00015ADC File Offset: 0x00013CDC
		public Parmfile Parmfile
		{
			get
			{
				Parmfile arg_19_0;
				if ((arg_19_0 = this._parmfile) == null)
				{
					arg_19_0 = (this._parmfile = new Parmfile(this));
				}
				return arg_19_0;
			}
		}

		// Token: 0x17000030 RID: 48
		// (get) Token: 0x0600022E RID: 558 RVA: 0x00015B04 File Offset: 0x00013D04
		public Requests Requests
		{
			get
			{
				Requests arg_19_0;
				if ((arg_19_0 = this._requests) == null)
				{
					arg_19_0 = (this._requests = new Requests(this));
				}
				return arg_19_0;
			}
		}

		// Token: 0x17000031 RID: 49
		// (get) Token: 0x0600022F RID: 559 RVA: 0x00015B2C File Offset: 0x00013D2C
		public Reviews Reviews
		{
			get
			{
				Reviews arg_19_0;
				if ((arg_19_0 = this._reviews) == null)
				{
					arg_19_0 = (this._reviews = new Reviews(this));
				}
				return arg_19_0;
			}
		}

		// Token: 0x17000032 RID: 50
		// (get) Token: 0x06000230 RID: 560 RVA: 0x00015B54 File Offset: 0x00013D54
		public Sections Sections
		{
			get
			{
				Sections arg_19_0;
				if ((arg_19_0 = this._sections) == null)
				{
					arg_19_0 = (this._sections = new Sections(this));
				}
				return arg_19_0;
			}
		}

		// Token: 0x17000033 RID: 51
		// (get) Token: 0x06000231 RID: 561 RVA: 0x00015B7C File Offset: 0x00013D7C
		public Shifts Shifts
		{
			get
			{
				Shifts arg_19_0;
				if ((arg_19_0 = this._shifts) == null)
				{
					arg_19_0 = (this._shifts = new Shifts(this));
				}
				return arg_19_0;
			}
		}

		// Token: 0x17000034 RID: 52
		// (get) Token: 0x06000232 RID: 562 RVA: 0x00015BA4 File Offset: 0x00013DA4
		public Substitute Substitute
		{
			get
			{
				Substitute arg_19_0;
				if ((arg_19_0 = this._substitute) == null)
				{
					arg_19_0 = (this._substitute = new Substitute(this));
				}
				return arg_19_0;
			}
		}

		// Token: 0x17000035 RID: 53
		// (get) Token: 0x06000233 RID: 563 RVA: 0x00015BCC File Offset: 0x00013DCC
		public Users Users
		{
			get
			{
				Users arg_19_0;
				if ((arg_19_0 = this._users) == null)
				{
					arg_19_0 = (this._users = new Users(this));
				}
				return arg_19_0;
			}
		}

		// Token: 0x17000036 RID: 54
		// (get) Token: 0x06000234 RID: 564 RVA: 0x00015BF4 File Offset: 0x00013DF4
		public MorTime MorTime
		{
			get
			{
				MorTime arg_19_0;
				if ((arg_19_0 = this._morTime) == null)
				{
					arg_19_0 = (this._morTime = new MorTime(this));
				}
				return arg_19_0;
			}
		}

		// Token: 0x17000037 RID: 55
		// (get) Token: 0x06000235 RID: 565 RVA: 0x00015C1C File Offset: 0x00013E1C
		public WardenSection WardenSection
		{
			get
			{
				WardenSection arg_19_0;
				if ((arg_19_0 = this._wardenSection) == null)
				{
					arg_19_0 = (this._wardenSection = new WardenSection(this));
				}
				return arg_19_0;
			}
		}

		// Token: 0x17000038 RID: 56
		// (get) Token: 0x06000236 RID: 566 RVA: 0x00015C44 File Offset: 0x00013E44
		public WardenCards WardenCards
		{
			get
			{
				WardenCards arg_19_0;
				if ((arg_19_0 = this._wardenCards) == null)
				{
					arg_19_0 = (this._wardenCards = new WardenCards(this));
				}
				return arg_19_0;
			}
		}

		// Token: 0x17000039 RID: 57
		// (get) Token: 0x06000237 RID: 567 RVA: 0x00015C6C File Offset: 0x00013E6C
		public MoveUp MoveUp
		{
			get
			{
				MoveUp arg_19_0;
				if ((arg_19_0 = this._moveUp) == null)
				{
					arg_19_0 = (this._moveUp = new MoveUp(this));
				}
				return arg_19_0;
			}
		}

		// Token: 0x1700003A RID: 58
		// (get) Token: 0x06000238 RID: 568 RVA: 0x00015C92 File Offset: 0x00013E92
		// (set) Token: 0x06000239 RID: 569 RVA: 0x00015C9A File Offset: 0x00013E9A
		public bool ThrowException
		{
			get
			{
				return this._throwException;
			}
			set
			{
				this._throwException = value;
			}
		}

		// Token: 0x1700003B RID: 59
		// (get) Token: 0x0600023A RID: 570 RVA: 0x00015CA3 File Offset: 0x00013EA3
		// (set) Token: 0x0600023B RID: 571 RVA: 0x00015CAB File Offset: 0x00013EAB
		public SqlTransaction Transaction
		{
			get
			{
				return this._transaction;
			}
			set
			{
				this._transaction = value;
			}
		}

		// Token: 0x0600023C RID: 572 RVA: 0x00015CB4 File Offset: 0x00013EB4
		public void BeginTransaction(string connectionString)
		{
			SqlConnection sqlConnection = new SqlConnection(connectionString);
			sqlConnection.Open();
			this._transaction = sqlConnection.BeginTransaction();
		}

		// Token: 0x0600023D RID: 573 RVA: 0x00015CDA File Offset: 0x00013EDA
		public void BeginTransaction()
		{
			this.BeginTransaction(SqlHelper.ConStr.ConnectionString);
		}

		// Token: 0x0600023E RID: 574 RVA: 0x00015CEC File Offset: 0x00013EEC
		public void CommitTransaction()
		{
			this._transaction.Commit();
			this._transaction.Dispose();
			this._transaction = null;
		}

		// Token: 0x0600023F RID: 575 RVA: 0x00015D0B File Offset: 0x00013F0B
		public void RollBackTransaction()
		{
			this._transaction.Rollback();
			this._transaction.Dispose();
			this._transaction = null;
		}

		// Token: 0x1700003C RID: 60
		// (get) Token: 0x06000240 RID: 576 RVA: 0x00015D2A File Offset: 0x00013F2A
		// (set) Token: 0x06000241 RID: 577 RVA: 0x00015D36 File Offset: 0x00013F36
		public string ConnectionString
		{
			get
			{
				return SqlHelper.ConStr.ConnectionString;
			}
			set
			{
				SqlHelper.ConStr.ConnectionString = value;
			}
		}

		// Token: 0x1700003D RID: 61
		// (get) Token: 0x06000242 RID: 578 RVA: 0x00015D43 File Offset: 0x00013F43
		public bool IsInTransaction
		{
			get
			{
				return this.Transaction != null;
			}
		}

		// Token: 0x06000243 RID: 579 RVA: 0x00015D4E File Offset: 0x00013F4E
		public void Dispose()
		{
			if (this._transaction != null)
			{
				this._transaction.Dispose();
				this._transaction = null;
				throw new Exception("یک ترنزکشن باز میباشد ، لطفا وضعیت آن را مشخص نمایید");
			}
		}

		// Token: 0x0400000A RID: 10
		public static WorkFlowsDatabase Instance = new WorkFlowsDatabase();

		// Token: 0x0400000B RID: 11
		private bool _throwException;

		// Token: 0x0400000C RID: 12
		private SqlTransaction _transaction;

		// Token: 0x0400000D RID: 13
		private BaseDAL _baseDAL;

		// Token: 0x0400000E RID: 14
		private Cards _cards;

		// Token: 0x0400000F RID: 15
		private DataFile _dataFile;

		// Token: 0x04000010 RID: 16
		private EmpGrps _empGrps;

		// Token: 0x04000011 RID: 17
		private Employee _employee;

		// Token: 0x04000012 RID: 18
		private EmployeeManagers _employeeManagers;

		// Token: 0x04000013 RID: 19
		private EmpTypes _empTypes;

		// Token: 0x04000014 RID: 20
		private Groups _groups;

		// Token: 0x04000015 RID: 21
		private MorMam _morMam;

		// Token: 0x04000016 RID: 22
		private MorTime _morTime;

		// Token: 0x04000017 RID: 23
		private OperationTypes _operationTypes;

		// Token: 0x04000018 RID: 24
		private Overs _overs;

		// Token: 0x04000019 RID: 25
		private Parmfile _parmfile;

		// Token: 0x0400001A RID: 26
		private Requests _requests;

		// Token: 0x0400001B RID: 27
		private Reviews _reviews;

		// Token: 0x0400001C RID: 28
		private Sections _sections;

		// Token: 0x0400001D RID: 29
		private Shifts _shifts;

		// Token: 0x0400001E RID: 30
		private Substitute _substitute;

		// Token: 0x0400001F RID: 31
		private Users _users;

		// Token: 0x04000020 RID: 32
		private Action _action;

		// Token: 0x04000021 RID: 33
		private EmpRequestActions _empRequestActions;

		// Token: 0x04000022 RID: 34
		private WardenSection _wardenSection;

		// Token: 0x04000023 RID: 35
		private WardenCards _wardenCards;

		// Token: 0x04000024 RID: 36
		private MoveUp _moveUp;

		// Token: 0x04000025 RID: 37
		private Reports _report;

		// Token: 0x04000026 RID: 38
		private ActionSection _actionSection;

		// Token: 0x04000027 RID: 39
		private Conferment _conferment;

		// Token: 0x04000028 RID: 40
		private LimitedManager _limitedManager;

		// Token: 0x04000029 RID: 41
		private AccessTimeGroup _accessTimeGroup;

		// Token: 0x0400002A RID: 42
		private UserAccessTime _userAccessTime;

		// Token: 0x0400002B RID: 43
		private WfEmpCard _wfEmpCard;

		// Token: 0x0400002C RID: 44
		private Attachment _attachment;

		// Token: 0x0400002D RID: 45
		private Help _help;

		// Token: 0x0400002E RID: 46
		private Log _log;

		// Token: 0x0400002F RID: 47
		private LogMorMam _logMorMam;

		// Token: 0x04000030 RID: 48
		private ManagerReplace _managerReplace;

		// Token: 0x04000031 RID: 49
		private Kardex _kardex;

		// Token: 0x04000032 RID: 50
		private ParallelApprove _parallelApprove;

		// Token: 0x04000033 RID: 51
		private RequestParallelApproval _requestParallelApproval;

		// Token: 0x04000034 RID: 52
		private Quotation _quotation;

		// Token: 0x04000035 RID: 53
		private News _news;

		// Token: 0x04000036 RID: 54
		private BaseInfo _baseInfo;

		// Token: 0x04000037 RID: 55
		private DatabaseInfo _DatabaseInfo;

		// Token: 0x04000038 RID: 56
		private WFPrintSeting _wfPrintSeting;

		// Token: 0x04000039 RID: 57
		private WFUserSections _wfUserSections;

		// Token: 0x0400003A RID: 58
		private Condition _condition;
	}
}
