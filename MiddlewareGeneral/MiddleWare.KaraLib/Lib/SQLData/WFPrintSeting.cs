﻿using System;
using System.Data;

namespace PW.WorkFlows.SqlDal
{
	// Token: 0x02000007 RID: 7
	public class WFPrintSeting : BaseDAL
	{
		// Token: 0x06000065 RID: 101 RVA: 0x00003C40 File Offset: 0x00001E40
		public WFPrintSeting(WorkFlowsDatabase owner) : base(owner)
		{
		}

		// Token: 0x06000066 RID: 102 RVA: 0x00003DFB File Offset: 0x00001FFB
		public DataTable Get()
		{
			return base.ExecuteDataTable(CommandType.Text, "SELECT ws.*,a.Fdesc FROM WFPrintSeting ws JOIN [Action] a ON a.ActionId = ws.ActionId");
		}

		// Token: 0x06000067 RID: 103 RVA: 0x00003E09 File Offset: 0x00002009
		public DataRow Get(int id)
		{
			return base.ExecuteDataRow(CommandType.Text, "SELECT ws.*,a.Fdesc FROM WFPrintSeting ws JOIN [Action] a ON a.ActionId = ws.ActionId Where ws.ID=" + id);
		}

		// Token: 0x06000068 RID: 104 RVA: 0x00003E22 File Offset: 0x00002022
		public DataTable GetByActioId(int actionId)
		{
			return base.ExecuteDataTable(CommandType.Text, "SELECT ws.*,a.Fdesc FROM WFPrintSeting ws JOIN [Action] a ON a.ActionId = ws.ActionId Where ws.ActionId=" + actionId);
		}

		// Token: 0x06000069 RID: 105 RVA: 0x00003E3B File Offset: 0x0000203B
		public bool Update(int actionId, string headerName, string context, int id)
		{
			return base.ExecuteNonQuery(CommandType.Text, string.Format("UPDATE WFPrintSeting SET ActionId = {0},HeaderName = N'{1}',Context = N'{2}' WHERE ID={3}", new object[]
			{
				actionId,
				headerName,
				context,
				id
			})) >= 0;
		}

		// Token: 0x0600006A RID: 106 RVA: 0x00003E75 File Offset: 0x00002075
		public bool Insert(int actionId, string headerName, string context)
		{
			return base.ExecuteNonQuery(CommandType.Text, string.Format("INSERT INTO WFPrintSeting(ActionId,HeaderName,Context)VALUES({0},N'{1}',N'{2}')", actionId, headerName, context)) >= 0;
		}

		// Token: 0x0600006B RID: 107 RVA: 0x00003E96 File Offset: 0x00002096
		public bool Delete(int id)
		{
			return base.ExecuteNonQuery(CommandType.Text, "DELETE FROM WFPrintSeting WHERE ID=" + id) >= 0;
		}
	}
}
