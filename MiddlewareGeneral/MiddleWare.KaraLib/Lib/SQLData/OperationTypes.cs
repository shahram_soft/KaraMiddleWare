﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace PW.WorkFlows.SqlDal
{
	// Token: 0x02000023 RID: 35
	public class OperationTypes : BaseDAL
	{
		// Token: 0x06000171 RID: 369 RVA: 0x00003C40 File Offset: 0x00001E40
		public OperationTypes(WorkFlowsDatabase owner) : base(owner)
		{
		}

		// Token: 0x06000172 RID: 370 RVA: 0x0000BD30 File Offset: 0x00009F30
		public DataTable OperationTypesGetData()
		{
			DataTable dataTable = new DataTable();
			dataTable.Columns.Add("OperationsID", typeof(int));
			dataTable.Columns.Add("NAME", typeof(string));
			dataTable.Columns.Add("FullNAME", typeof(string));
			using (SqlDataReader sqlDataReader = base.ExecuteReader("WF_OperationTypesGetData", null))
			{
				if (sqlDataReader.HasRows)
				{
					while (sqlDataReader.Read())
					{
						DataRow dataRow = dataTable.NewRow();
						dataRow["OperationsID"] = sqlDataReader["OperationsID"];
						dataRow["NAME"] = sqlDataReader["NAME"];
						dataRow["FullNAME"] = sqlDataReader["OperationsID"] + "_" + sqlDataReader["NAME"];
						dataTable.Rows.Add(dataRow);
					}
				}
			}
			return dataTable;
		}

		// Token: 0x06000173 RID: 371 RVA: 0x0000BE3C File Offset: 0x0000A03C
		public int OperationTypesInsert(int operationsId, string name)
		{
			return base.ExecuteNonQuery("WF_OperationTypesInsert", new object[]
			{
				operationsId,
				name
			});
		}
	}
}
