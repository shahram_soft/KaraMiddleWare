﻿using System;
using System.Data;

namespace PW.WorkFlows.SqlDal
{
	// Token: 0x02000009 RID: 9
	public class AccessTimeGroup : BaseDAL
	{
		// Token: 0x06000073 RID: 115 RVA: 0x00003C40 File Offset: 0x00001E40
		public AccessTimeGroup(WorkFlowsDatabase owner) : base(owner)
		{
		}

		// Token: 0x06000074 RID: 116 RVA: 0x00003F84 File Offset: 0x00002184
		public int Delete(int ifAccessTimeGroupId)
		{
			return base.ExecuteNonQuery("WFAccessTimeGroup_Delete", new object[]
			{
				ifAccessTimeGroupId
			});
		}

		// Token: 0x06000075 RID: 117 RVA: 0x00003FA0 File Offset: 0x000021A0
		public int Insert(string groupName, byte groupStatus, short incTolerance, short decTolerance)
		{
			return Convert.ToInt32(base.ExecuteScalar("WFAccessTimeGroup_Insert", new object[]
			{
				groupName,
				groupStatus,
				incTolerance,
				decTolerance
			}));
		}

		// Token: 0x06000076 RID: 118 RVA: 0x00003FD8 File Offset: 0x000021D8
		public DataTable Select()
		{
			return base.ExecuteDataTable("WFAccessTimeGroup_Select", null);
		}

		// Token: 0x06000077 RID: 119 RVA: 0x00003FE6 File Offset: 0x000021E6
		public DataTable SelectByStatus(byte groupStatus)
		{
			return base.ExecuteDataTable("WFAccessTimeGroup_SelectByStatus", new object[]
			{
				groupStatus
			});
		}

		// Token: 0x06000078 RID: 120 RVA: 0x00004002 File Offset: 0x00002202
		public int Update(int ifAccessTimeGroupId, string groupName, byte groupStatus, short incTolerance, short decTolerance)
		{
			return base.ExecuteNonQuery("WFAccessTimeGroup_Update", new object[]
			{
				ifAccessTimeGroupId,
				groupName,
				groupStatus,
				incTolerance,
				decTolerance
			});
		}

		// Token: 0x06000079 RID: 121 RVA: 0x0000403F File Offset: 0x0000223F
		public DataRow SelectById(int ifAccessTimeGroupId)
		{
			return base.ExecuteDataRow("WFAccessTimeGroup_SelectByID", new object[]
			{
				ifAccessTimeGroupId
			});
		}
	}
}
