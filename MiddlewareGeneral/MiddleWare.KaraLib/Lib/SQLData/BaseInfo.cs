﻿using System;
using System.Data;

namespace PW.WorkFlows.SqlDal
{
	// Token: 0x02000008 RID: 8
	public class BaseInfo : BaseDAL
	{
		// Token: 0x0600006C RID: 108 RVA: 0x00003C40 File Offset: 0x00001E40
		public BaseInfo(WorkFlowsDatabase owner) : base(owner)
		{
		}

		// Token: 0x0600006D RID: 109 RVA: 0x00003EB5 File Offset: 0x000020B5
		public int Delete(int ifBaseId)
		{
			return base.ExecuteNonQuery(CommandType.Text, "DELETE FROM WFBase WHERE BaseId=" + ifBaseId);
		}

		// Token: 0x0600006E RID: 110 RVA: 0x00003ED0 File Offset: 0x000020D0
		public int Insert(short baseCode, string baseValue)
		{
			string commandText = string.Format("INSERT INTO WFBase(BaseCode,BaseValue,BaseGroup) VALUES({0},N'{1}','DIO')", baseCode, baseValue);
			return base.ExecuteNonQuery(CommandType.Text, commandText);
		}

		// Token: 0x0600006F RID: 111 RVA: 0x00003EF7 File Offset: 0x000020F7
		public DataTable GetData()
		{
			return base.ExecuteDataTable(CommandType.Text, "SELECT * FROM WFBase w WHERE w.BaseGroup='DIO'");
		}

		// Token: 0x06000070 RID: 112 RVA: 0x00003F05 File Offset: 0x00002105
		public bool IsExist(short ifBaseCode)
		{
			return Convert.ToInt32(base.ExecuteScalar(CommandType.Text, "SELECT COUNT(1) cnt  FROM WFBase w WHERE w.BaseCode=" + ifBaseCode)) > 0;
		}

		// Token: 0x06000071 RID: 113 RVA: 0x00003F26 File Offset: 0x00002126
		public bool IsExistInRequest(short ifBaseCode)
		{
			return Convert.ToInt32(base.ExecuteScalar(CommandType.Text, string.Concat(new object[]
			{
				"SELECT COUNT(1) cnt FROM Requests WHERE DeviceWent=",
				ifBaseCode,
				" OR DeviceBack=",
				ifBaseCode
			}))) > 0;
		}

		// Token: 0x06000072 RID: 114 RVA: 0x00003F64 File Offset: 0x00002164
		public int Update(int ifBaseId, short baseCode, string baseValue)
		{
			return base.ExecuteNonQuery(CommandType.Text, string.Format("UPDATE WFBase SET BaseValue = N'{0}',BaseCode={1} WHERE BaseId={2}", baseValue, baseCode, ifBaseId));
		}
	}
}
