﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using MiddleWare.KaraLib;
using MiddleWare.KaraLib.ViewModels;


namespace PW.WorkFlows.SqlDal
{
	// Token: 0x0200002A RID: 42
	public class Requests : BaseDAL
	{
		// Token: 0x0600019C RID: 412 RVA: 0x00003C40 File Offset: 0x00001E40
		public Requests(WorkFlowsDatabase owner) : base(owner)
		{
		}

		// Token: 0x0600019D RID: 413 RVA: 0x0000E920 File Offset: 0x0000CB20
		public int Delete(int requestsId)
		{
			int result;
			try
			{
				result = base.ExecuteNonQuery("WF_RequestsDelete", new object[]
				{
					requestsId
				});
			}
			catch
			{
				result = 0;
			}
			return result;
		}

		// Token: 0x0600019E RID: 414 RVA: 0x0000E960 File Offset: 0x0000CB60
		public DataTable GetDataByDate(double empNo, DateTime startDate)
		{
			return base.ExecuteDataTable("WF_RequestsGetDataByDate", new object[]
			{
				empNo,
				startDate
			});
		}

		// Token: 0x0600019F RID: 415 RVA: 0x0000E988 File Offset: 0x0000CB88
		public DataTable GetDataByEmployeeNumber(double empNo)
		{
			DataTable dataTable = new DataTable();
			dataTable.Columns.Add("ApprovalByManagerEmp_No", typeof(double));
			dataTable.Columns.Add("ApprovalDate", typeof(string));
			dataTable.Columns.Add("Duration", typeof(string));
			dataTable.Columns.Add("EMP_NO", typeof(double));
			dataTable.Columns.Add("EndDate", typeof(string));
			dataTable.Columns.Add("EndHour", typeof(int));
			dataTable.Columns.Add("IsFinalApproved", typeof(bool));
			dataTable.Columns.Add("IsApproved", typeof(string));
			dataTable.Columns.Add("OperationsID", typeof(int));
			dataTable.Columns.Add("RequestsID", typeof(int));
			dataTable.Columns.Add("StartDate", typeof(string));
			dataTable.Columns.Add("StartHour", typeof(string));
			dataTable.Columns.Add("SubmittedByEmployeeID", typeof(double));
			dataTable.Columns.Add("SubmittedDate", typeof(string));
			dataTable.Columns.Add("Type", typeof(short));
			dataTable.Columns.Add("EmployeeName", typeof(string));
			dataTable.Columns.Add("Description", typeof(string));
			dataTable.Columns.Add("TITLE", typeof(string));
			dataTable.Columns.Add("ReviewsCount", typeof(int));
			dataTable.Columns.Add("ApprovalName", typeof(string));
			dataTable.Columns.Add("CurEmp_NO", typeof(double));
			dataTable.Columns.Add("Requested_Time", typeof(string));
			dataTable.Columns.Add("ManagerName", typeof(string));
			dataTable.Columns.Add("AcceptCode", typeof(short));
            dataTable.Columns.Add("ManagerIdea", typeof(string));
            using (SqlDataReader sqlDataReader = base.ExecuteReader("WF_RequestsGetDataByEmployeeNumber", new object[]
			{
				empNo
			}))
			{
				if (sqlDataReader.HasRows)
				{
					while (sqlDataReader.Read())
					{
						DataRow dataRow = dataTable.NewRow();
						dataRow["ApprovalByManagerEmp_No"] = sqlDataReader["ApprovalByManagerEmp_No"];
                        dataRow["ManagerIdea"] = sqlDataReader["ManagerIdea"];
                        dataRow["ApprovalDate"] = sqlDataReader["ApprovalDate"];
						string value = sqlDataReader["Duration"].ToString();
						string text = sqlDataReader["Requested_Time"].ToString();
						if (text == "")
						{
							text = "0";
						}
						dataRow["EMP_NO"] = sqlDataReader["EMP_NO"];
						dataRow["AcceptCode"] = sqlDataReader["AcceptCode"];
						dataRow["EndDate"] = ((sqlDataReader["EndDate"].ToString() != "") ? ShDate.DateToStr((DateTime)sqlDataReader["EndDate"]) : "");
						dataRow["EndHour"] = sqlDataReader["EndHour"];
						dataRow["IsFinalApproved"] = sqlDataReader["IsFinalApproved"];
						bool? isFinalApproved = null;
						if (!(sqlDataReader["IsFinalApproved"] is DBNull) && !string.IsNullOrEmpty(sqlDataReader["IsFinalApproved"].ToString()))
						{
							isFinalApproved = new bool?(Convert.ToBoolean(sqlDataReader["IsFinalApproved"]));
						}
						dataRow["OperationsID"] = sqlDataReader["OperationsID"];
						dataRow["RequestsID"] = sqlDataReader["RequestsID"];
						dataRow["StartDate"] = ((sqlDataReader["StartDate"].ToString() != "") ? ShDate.DateToStr((DateTime)sqlDataReader["StartDate"]) : "");
						dataRow["StartHour"] = UtilityManagers.ConvertToTimeFormat(sqlDataReader["StartHour"].ToString());
						dataRow["SubmittedByEmployeeID"] = sqlDataReader["SubmittedByEmployeeID"];
						dataRow["SubmittedDate"] = ((sqlDataReader["SubmittedDate"].ToString() != "") ? (ShDate.DateToStr((DateTime)sqlDataReader["SubmittedDate"]) + " " + ((DateTime)sqlDataReader["SubmittedDate"]).ToString("hh:mm")) : "");
						bool isDay;
						bool.TryParse(sqlDataReader["IS_DAY"].ToString(), out isDay);
						dataRow["Type"] = sqlDataReader["Type"];
						short typeNo = Convert.ToInt16(sqlDataReader["Type"]);
						dataRow["EmployeeName"] = sqlDataReader["EmployeeName"];
						dataRow["Description"] = sqlDataReader["Description"];
						string value2 = sqlDataReader["TITLE"].ToString();
						dataRow["ReviewsCount"] = sqlDataReader["ReviewsCount"];
						dataRow["ApprovalName"] = sqlDataReader["ApprovalName"];
						dataRow["CurEmp_NO"] = sqlDataReader["CurEmp_NO"];
						dataRow["ManagerName"] = sqlDataReader["ManagerName"];
						if (!isFinalApproved.HasValue)
						{
							dataRow["IsApproved"] = "ارسالی";
						}
						else
						{
							dataRow["IsApproved"] = (isFinalApproved.Value ? "تایید" : "رد");
						}
						base.GetDuration(Convert.ToInt32(dataRow["OperationsID"]), (int)typeNo, isDay, isFinalApproved, dataRow["StartHour"].ToString(), dataRow["EndHour"].ToString(), ref value, ref text, ref value2);
						dataRow["TITLE"] = value2;
						dataRow["Duration"] = value;
						dataRow["Requested_Time"] = text;
						dataTable.Rows.Add(dataRow);
					}
				}
			}
			return dataTable;
		}

		// Token: 0x060001A0 RID: 416 RVA: 0x0000F068 File Offset: 0x0000D268
		public DataTable GetDataBySupervisor(double empNo)
		{
			DataTable dataTable = new DataTable();
			dataTable.Columns.Add("ApprovalByManagerEmp_No", typeof(double));
			dataTable.Columns.Add("ApprovalDate", typeof(string));
			dataTable.Columns.Add("Duration", typeof(string));
			dataTable.Columns.Add("EMP_NO", typeof(double));
			dataTable.Columns.Add("EndDate", typeof(string));
			dataTable.Columns.Add("EndHour", typeof(int));
			dataTable.Columns.Add("IsFinalApproved", typeof(bool));
			dataTable.Columns.Add("IsApproved", typeof(string));
			dataTable.Columns.Add("OperationsID", typeof(int));
			dataTable.Columns.Add("RequestsID", typeof(int));
			dataTable.Columns.Add("StartDate", typeof(string));
			dataTable.Columns.Add("StartHour", typeof(string));
			dataTable.Columns.Add("SubmittedByEmployeeID", typeof(double));
			dataTable.Columns.Add("SubmittedDate", typeof(string));
			dataTable.Columns.Add("Type", typeof(short));
			dataTable.Columns.Add("EmployeeName", typeof(string));
			dataTable.Columns.Add("Description", typeof(string));
			dataTable.Columns.Add("TITLE", typeof(string));
			dataTable.Columns.Add("ReviewsCount", typeof(int));
			dataTable.Columns.Add("ApprovalName", typeof(string));
			dataTable.Columns.Add("CurEmp_NO", typeof(double));
			dataTable.Columns.Add("Requested_Time", typeof(string));
			dataTable.Columns.Add("ManagerName", typeof(string));
			dataTable.Columns.Add("AcceptCode", typeof(short));
            dataTable.Columns.Add("ManagerIdea", typeof(string));
            using (SqlDataReader sqlDataReader = base.ExecuteReader("WF_RequestsGetDataBySupervisor", new object[]
			{
				empNo
			}))
			{
				if (sqlDataReader.HasRows)
				{
					while (sqlDataReader.Read())
					{
						DataRow dataRow = dataTable.NewRow();
						dataRow["ApprovalByManagerEmp_No"] = sqlDataReader["ApprovalByManagerEmp_No"];
                        dataRow["ManagerIdea"] = sqlDataReader["ManagerIdea"];
                        dataRow["ApprovalDate"] = sqlDataReader["ApprovalDate"];
						string value = sqlDataReader["Duration"].ToString();
						dataRow["EMP_NO"] = sqlDataReader["EMP_NO"];
						dataRow["EndDate"] = ((sqlDataReader["EndDate"].ToString() != "") ? ShDate.DateToStr((DateTime)sqlDataReader["EndDate"]) : "");
						dataRow["EndHour"] = sqlDataReader["EndHour"];
						dataRow["IsFinalApproved"] = sqlDataReader["IsFinalApproved"];
						bool? isFinalApproved = null;
						if (!(sqlDataReader["IsFinalApproved"] is DBNull) && !string.IsNullOrEmpty(sqlDataReader["IsFinalApproved"].ToString()))
						{
							isFinalApproved = new bool?(Convert.ToBoolean(sqlDataReader["IsFinalApproved"]));
						}
						dataRow["OperationsID"] = sqlDataReader["OperationsID"];
						dataRow["RequestsID"] = sqlDataReader["RequestsID"];
						dataRow["StartDate"] = ((sqlDataReader["StartDate"].ToString() != "") ? ShDate.DateToStr((DateTime)sqlDataReader["StartDate"]) : "");
						dataRow["StartHour"] = UtilityManagers.ConvertToTimeFormat(sqlDataReader["StartHour"].ToString());
						dataRow["SubmittedByEmployeeID"] = sqlDataReader["SubmittedByEmployeeID"];
						dataRow["SubmittedDate"] = ((sqlDataReader["SubmittedDate"].ToString() != "") ? (ShDate.DateToStr((DateTime)sqlDataReader["SubmittedDate"]) + " " + ((DateTime)sqlDataReader["SubmittedDate"]).ToString("hh:mm")) : "");
						dataRow["Type"] = sqlDataReader["Type"];
						short typeNo = Convert.ToInt16(sqlDataReader["Type"]);
						dataRow["EmployeeName"] = sqlDataReader["EmployeeName"];
						dataRow["Description"] = sqlDataReader["Description"];
						string value2 = sqlDataReader["TITLE"].ToString();
						dataRow["ReviewsCount"] = sqlDataReader["ReviewsCount"];
						dataRow["ApprovalName"] = sqlDataReader["ApprovalName"];
						dataRow["CurEmp_NO"] = sqlDataReader["CurEmp_NO"];
						dataRow["ManagerName"] = sqlDataReader["ManagerName"];
						dataRow["AcceptCode"] = sqlDataReader["AcceptCode"];
						bool isDay;
						bool.TryParse(sqlDataReader["IS_DAY"].ToString(), out isDay);
						if (!isFinalApproved.HasValue)
						{
							dataRow["IsApproved"] = "ارسالی";
						}
						else if (isFinalApproved.Value)
						{
							dataRow["IsApproved"] = "تایید";
						}
						else
						{
							dataRow["IsApproved"] = "رد";
						}
						string text = sqlDataReader["Requested_Time"].ToString();
						if (text == "")
						{
							text = "0";
						}
						base.GetDuration(Convert.ToInt32(dataRow["OperationsID"]), (int)typeNo, isDay, isFinalApproved, dataRow["StartHour"].ToString(), dataRow["EndHour"].ToString(), ref value, ref text, ref value2);
						dataRow["TITLE"] = value2;
						dataRow["Duration"] = value;
						dataRow["Requested_Time"] = text;
						dataTable.Rows.Add(dataRow);
					}
				}
			}
			return dataTable;
		}

		// Token: 0x060001A1 RID: 417 RVA: 0x0000F750 File Offset: 0x0000D950
		public DataTable GetByManagerEmpNo(double empManager, DateTime startDate, DateTime endDate, short? isFinalApprove)
		{
			DataTable dataTable = new DataTable();
			dataTable.Columns.Add("ApprovalByManagerEmp_No", typeof(double));
			dataTable.Columns.Add("ApprovalDate", typeof(string));
			dataTable.Columns.Add("Duration", typeof(string));
			dataTable.Columns.Add("DurationOrg", typeof(string));
			dataTable.Columns.Add("EMP_NO", typeof(double));
			dataTable.Columns.Add("EndDate", typeof(string));
			dataTable.Columns.Add("EndHour", typeof(int));
			dataTable.Columns.Add("IsFinalApproved", typeof(bool));
			dataTable.Columns.Add("OperationsID", typeof(int));
			dataTable.Columns.Add("RequestsID", typeof(int));
			dataTable.Columns.Add("StartDate", typeof(string));
			dataTable.Columns.Add("StartHour", typeof(string));
			dataTable.Columns.Add("SubmittedByEmployeeID", typeof(double));
			dataTable.Columns.Add("SubmittedDate", typeof(string));
			dataTable.Columns.Add("Type", typeof(short));
			dataTable.Columns.Add("EmployeeName", typeof(string));
			dataTable.Columns.Add("Description", typeof(string));
			dataTable.Columns.Add("TITLE", typeof(string));
			dataTable.Columns.Add("ManagerName", typeof(string));
			dataTable.Columns.Add("EmpManager", typeof(double));
			dataTable.Columns.Add("SEC_NO", typeof(double));
			dataTable.Columns.Add("SECTIONS", typeof(string));
			dataTable.Columns.Add("CurEmp_NO", typeof(double));
			dataTable.Columns.Add("SEmp_No", typeof(double));
			dataTable.Columns.Add("S_FDate", typeof(string));
			dataTable.Columns.Add("S_TDate", typeof(string));
			dataTable.Columns.Add("AcceptCode", typeof(short));
			dataTable.Columns.Add("CurSection", typeof(int));
			dataTable.Columns.Add("MoveUpCnt", typeof(int));
			dataTable.Columns.Add("ReqStatus", typeof(string));
			dataTable.Columns.Add("ParallelCount", typeof(int));
			dataTable.Columns.Add("EmpSecTitle", typeof(string));
			dataTable.Columns.Add("EmpSecNo", typeof(double));
			dataTable.Columns.Add("NeedParallelCount", typeof(int));
			dataTable.Columns.Add("SubmittedFullName", typeof(string));
			using (SqlDataReader sqlDataReader = base.ExecuteReader("WF_RequestGetByManagerEmpNo", new object[]
			{
				empManager,
				startDate,
				endDate,
				isFinalApprove
			}))
			{
				if (sqlDataReader.HasRows)
				{
					while (sqlDataReader.Read())
					{
						DataRow dataRow = dataTable.NewRow();
						dataRow["ApprovalByManagerEmp_No"] = sqlDataReader["ApprovalByManagerEmp_No"];
						dataRow["ApprovalDate"] = sqlDataReader["ApprovalDate"];
						dataRow["DurationOrg"] = sqlDataReader["Duration"];
						string value = sqlDataReader["Duration"].ToString();
						dataRow["EMP_NO"] = sqlDataReader["EMP_NO"];
						dataRow["AcceptCode"] = sqlDataReader["AcceptCode"];
						dataRow["EndDate"] = ((sqlDataReader["EndDate"].ToString() != "") ? ShDate.DateToStr((DateTime)sqlDataReader["EndDate"]) : "");
						dataRow["EndHour"] = sqlDataReader["EndHour"];
						dataRow["IsFinalApproved"] = sqlDataReader["IsFinalApproved"];
						dataRow["OperationsID"] = sqlDataReader["OperationsID"];
						dataRow["RequestsID"] = sqlDataReader["RequestsID"];
						dataRow["StartDate"] = ((sqlDataReader["StartDate"].ToString() != "") ? ShDate.DateToStr((DateTime)sqlDataReader["StartDate"]) : "");
						dataRow["StartHour"] = UtilityManagers.ConvertToTimeFormat(sqlDataReader["StartHour"].ToString());
						dataRow["SubmittedByEmployeeID"] = sqlDataReader["SubmittedByEmployeeID"];
						dataRow["SubmittedDate"] = ((sqlDataReader["SubmittedDate"].ToString() != "") ? (ShDate.DateToStr((DateTime)sqlDataReader["SubmittedDate"]) + " " + ((DateTime)sqlDataReader["SubmittedDate"]).ToString("hh:mm")) : "");
						dataRow["Type"] = sqlDataReader["Type"];
						short typeNo = Convert.ToInt16(sqlDataReader["Type"]);
						dataRow["EmployeeName"] = sqlDataReader["EmployeeName"];
						dataRow["Description"] = sqlDataReader["Description"];
						bool isDay;
						bool.TryParse(sqlDataReader["IS_DAY"].ToString(), out isDay);
						string value2 = sqlDataReader["TITLE"].ToString();
						dataRow["ManagerName"] = sqlDataReader["ManagerName"];
						dataRow["EmpManager"] = sqlDataReader["EmpManager"];
						dataRow["SEC_NO"] = sqlDataReader["SEC_NO"];
						dataRow["SECTIONS"] = sqlDataReader["SECTIONS"];
						dataRow["CurEmp_NO"] = sqlDataReader["CurEmp_NO"];
						dataRow["SEmp_No"] = sqlDataReader["SEmp_No"];
						dataRow["S_FDate"] = sqlDataReader["S_FDate"];
						dataRow["S_TDate"] = sqlDataReader["S_TDate"];
						dataRow["CurSection"] = sqlDataReader["CurSection"];
						dataRow["MoveUpCnt"] = sqlDataReader["MoveUpCnt"];
						dataRow["ReqStatus"] = sqlDataReader["ReqStatus"];
						dataRow["ParallelCount"] = sqlDataReader["ParallelCount"];
						dataRow["EmpSecNo"] = sqlDataReader["EmpSecNo"];
						dataRow["EmpSecTitle"] = sqlDataReader["EmpSecTitle"];
						dataRow["NeedParallelCount"] = sqlDataReader["NeedParallelCount"];
						dataRow["SubmittedFullName"] = sqlDataReader["SubmittedFullName"];
						bool? isFinalApproved = null;
						if (isFinalApprove.HasValue)
						{
							isFinalApproved = new bool?(isFinalApprove.Value != 0);
						}
						string empty = string.Empty;
						base.GetDuration(Convert.ToInt32(dataRow["OperationsID"]), (int)typeNo, isDay, isFinalApproved, dataRow["StartHour"].ToString(), dataRow["EndHour"].ToString(), ref value, ref empty, ref value2);
						dataRow["Duration"] = value;
						dataRow["TITLE"] = value2;
						dataTable.Rows.Add(dataRow);
					}
				}
			}
			return dataTable;
		}

		// Token: 0x060001A2 RID: 418 RVA: 0x00010004 File Offset: 0x0000E204
		public DataTable GetMoveUpByManagerEmpNo(double empManager, DateTime? startDate)
		{
			DataTable dataTable = new DataTable();
			dataTable.Columns.Add("ApprovalByManagerEmp_No", typeof(double));
			dataTable.Columns.Add("ApprovalDate", typeof(string));
			dataTable.Columns.Add("Duration", typeof(string));
			dataTable.Columns.Add("DurationOrg", typeof(string));
			dataTable.Columns.Add("EMP_NO", typeof(double));
			dataTable.Columns.Add("EndDate", typeof(string));
			dataTable.Columns.Add("EndHour", typeof(int));
			dataTable.Columns.Add("IsFinalApproved", typeof(bool));
			dataTable.Columns.Add("OperationsID", typeof(int));
			dataTable.Columns.Add("RequestsID", typeof(int));
			dataTable.Columns.Add("StartDate", typeof(string));
			dataTable.Columns.Add("StartHour", typeof(string));
			dataTable.Columns.Add("SubmittedByEmployeeID", typeof(double));
			dataTable.Columns.Add("SubmittedDate", typeof(string));
			dataTable.Columns.Add("Type", typeof(short));
			dataTable.Columns.Add("EmployeeName", typeof(string));
			dataTable.Columns.Add("Description", typeof(string));
			dataTable.Columns.Add("TITLE", typeof(string));
			dataTable.Columns.Add("ManagerName", typeof(string));
			dataTable.Columns.Add("EmpManager", typeof(double));
			dataTable.Columns.Add("SEC_NO", typeof(double));
			dataTable.Columns.Add("SECTIONS", typeof(string));
			dataTable.Columns.Add("CurEmp_NO", typeof(double));
			dataTable.Columns.Add("SEmp_No", typeof(double));
			dataTable.Columns.Add("S_FDate", typeof(string));
			dataTable.Columns.Add("S_TDate", typeof(string));
			dataTable.Columns.Add("AcceptCode", typeof(short));
			dataTable.Columns.Add("CurSection", typeof(int));
			dataTable.Columns.Add("MoveUpCnt", typeof(int));
			dataTable.Columns.Add("ReqStatus", typeof(string));
			dataTable.Columns.Add("ParallelCount", typeof(int));
			dataTable.Columns.Add("NeedParallelCount", typeof(int));
			dataTable.Columns.Add("EmpSecTitle", typeof(string));
			dataTable.Columns.Add("EmpSecNo", typeof(double));
			dataTable.Columns.Add("SubmittedFullName", typeof(string));
			using (SqlDataReader sqlDataReader = base.ExecuteReader("WF_RequestGetMoveUpByManagerEmpNo", new object[]
			{
				empManager,
				startDate
			}))
			{
				if (sqlDataReader.HasRows)
				{
					while (sqlDataReader.Read())
					{
						DataRow dataRow = dataTable.NewRow();
						dataRow["ApprovalByManagerEmp_No"] = sqlDataReader["ApprovalByManagerEmp_No"];
						dataRow["ApprovalDate"] = sqlDataReader["ApprovalDate"];
						dataRow["DurationOrg"] = sqlDataReader["Duration"];
						string value = sqlDataReader["Duration"].ToString();
						dataRow["EMP_NO"] = sqlDataReader["EMP_NO"];
						dataRow["AcceptCode"] = sqlDataReader["AcceptCode"];
						dataRow["EndDate"] = ((sqlDataReader["EndDate"].ToString() != "") ? ShDate.DateToStr((DateTime)sqlDataReader["EndDate"]) : "");
						dataRow["EndHour"] = sqlDataReader["EndHour"];
						dataRow["IsFinalApproved"] = sqlDataReader["IsFinalApproved"];
						bool? isFinalApproved = null;
						if (!(sqlDataReader["IsFinalApproved"] is DBNull) && !string.IsNullOrEmpty(sqlDataReader["IsFinalApproved"].ToString()))
						{
							isFinalApproved = new bool?(Convert.ToBoolean(sqlDataReader["IsFinalApproved"]));
						}
						dataRow["OperationsID"] = sqlDataReader["OperationsID"];
						dataRow["RequestsID"] = sqlDataReader["RequestsID"];
						dataRow["StartDate"] = ((sqlDataReader["StartDate"].ToString() != "") ? ShDate.DateToStr((DateTime)sqlDataReader["StartDate"]) : "");
						dataRow["StartHour"] = UtilityManagers.ConvertToTimeFormat(sqlDataReader["StartHour"].ToString());
						dataRow["SubmittedByEmployeeID"] = sqlDataReader["SubmittedByEmployeeID"];
						dataRow["SubmittedDate"] = ((sqlDataReader["SubmittedDate"].ToString() != "") ? (ShDate.DateToStr((DateTime)sqlDataReader["SubmittedDate"]) + " " + ((DateTime)sqlDataReader["SubmittedDate"]).ToString("hh:mm")) : "");
						dataRow["Type"] = sqlDataReader["Type"];
						short typeNo = Convert.ToInt16(sqlDataReader["Type"]);
						dataRow["EmployeeName"] = sqlDataReader["EmployeeName"];
						dataRow["Description"] = sqlDataReader["Description"];
						bool isDay;
						bool.TryParse(sqlDataReader["IS_DAY"].ToString(), out isDay);
						string value2 = sqlDataReader["TITLE"].ToString();
						dataRow["ManagerName"] = sqlDataReader["ManagerName"];
						dataRow["EmpManager"] = sqlDataReader["EmpManager"];
						dataRow["SEC_NO"] = sqlDataReader["SEC_NO"];
						dataRow["SECTIONS"] = sqlDataReader["SECTIONS"];
						dataRow["CurEmp_NO"] = sqlDataReader["CurEmp_NO"];
						dataRow["SEmp_No"] = sqlDataReader["SEmp_No"];
						dataRow["S_FDate"] = sqlDataReader["S_FDate"];
						dataRow["S_TDate"] = sqlDataReader["S_TDate"];
						dataRow["CurSection"] = sqlDataReader["CurSection"];
						dataRow["MoveUpCnt"] = sqlDataReader["MoveUpCnt"];
						dataRow["ReqStatus"] = sqlDataReader["ReqStatus"];
						dataRow["ParallelCount"] = sqlDataReader["ParallelCount"];
						dataRow["EmpSecNo"] = sqlDataReader["EmpSecNo"];
						dataRow["EmpSecTitle"] = sqlDataReader["EmpSecTitle"];
						dataRow["NeedParallelCount"] = sqlDataReader["NeedParallelCount"];
						dataRow["SubmittedFullName"] = sqlDataReader["SubmittedFullName"];
						string empty = string.Empty;
						base.GetDuration(Convert.ToInt32(dataRow["OperationsID"]), (int)typeNo, isDay, isFinalApproved, dataRow["StartHour"].ToString(), dataRow["EndHour"].ToString(), ref value, ref empty, ref value2);
						dataRow["TITLE"] = value2;
						dataRow["Duration"] = value;
						dataTable.Rows.Add(dataRow);
					}
				}
			}
			return dataTable;
		}

		// Token: 0x060001A3 RID: 419 RVA: 0x000108CC File Offset: 0x0000EACC
		public DataTable GetReviews(double empManager, DateTime? startDate, DateTime endDate)
		{
			DataTable dataTable = new DataTable();
			dataTable.Columns.Add("ApprovalByManagerEmp_No", typeof(double));
			dataTable.Columns.Add("ApprovalDate", typeof(string));
			dataTable.Columns.Add("Duration", typeof(string));
			dataTable.Columns.Add("DurationOrg", typeof(string));
			dataTable.Columns.Add("EMP_NO", typeof(double));
			dataTable.Columns.Add("EndDate", typeof(string));
			dataTable.Columns.Add("EndHour", typeof(int));
			dataTable.Columns.Add("IsFinalApproved", typeof(bool));
			dataTable.Columns.Add("OperationsID", typeof(int));
			dataTable.Columns.Add("RequestsID", typeof(int));
			dataTable.Columns.Add("StartDate", typeof(string));
			dataTable.Columns.Add("StartHour", typeof(string));
			dataTable.Columns.Add("SubmittedByEmployeeID", typeof(double));
			dataTable.Columns.Add("SubmittedDate", typeof(string));
			dataTable.Columns.Add("Type", typeof(short));
			dataTable.Columns.Add("EmployeeName", typeof(string));
			dataTable.Columns.Add("Description", typeof(string));
			dataTable.Columns.Add("TITLE", typeof(string));
			dataTable.Columns.Add("ManagerName", typeof(string));
			dataTable.Columns.Add("EmpManager", typeof(double));
			dataTable.Columns.Add("SEC_NO", typeof(double));
			dataTable.Columns.Add("SECTIONS", typeof(string));
			dataTable.Columns.Add("CurEmp_NO", typeof(double));
			dataTable.Columns.Add("SEmp_No", typeof(double));
			dataTable.Columns.Add("S_FDate", typeof(string));
			dataTable.Columns.Add("S_TDate", typeof(string));
			dataTable.Columns.Add("AcceptCode", typeof(short));
			dataTable.Columns.Add("CurSection", typeof(int));
			dataTable.Columns.Add("MoveUpCnt", typeof(int));
			dataTable.Columns.Add("ReqStatus", typeof(string));
			dataTable.Columns.Add("ParallelCount", typeof(int));
			dataTable.Columns.Add("NeedParallelCount", typeof(int));
			dataTable.Columns.Add("EmpSecTitle", typeof(string));
			dataTable.Columns.Add("EmpSecNo", typeof(double));
			dataTable.Columns.Add("SubmittedFullName", typeof(string));
			using (SqlDataReader sqlDataReader = base.ExecuteReader("WF_RequestGetReviews", new object[]
			{
				empManager,
				startDate,
				endDate
			}))
			{
				if (sqlDataReader.HasRows)
				{
					while (sqlDataReader.Read())
					{
						DataRow dataRow = dataTable.NewRow();
						dataRow["ApprovalByManagerEmp_No"] = sqlDataReader["ApprovalByManagerEmp_No"];
						dataRow["ApprovalDate"] = sqlDataReader["ApprovalDate"];
						dataRow["DurationOrg"] = sqlDataReader["Duration"];
						string value = sqlDataReader["Duration"].ToString();
						dataRow["EMP_NO"] = sqlDataReader["EMP_NO"];
						dataRow["AcceptCode"] = sqlDataReader["AcceptCode"];
						dataRow["EndDate"] = ((sqlDataReader["EndDate"].ToString() != "") ? ShDate.DateToStr((DateTime)sqlDataReader["EndDate"]) : "");
						dataRow["EndHour"] = sqlDataReader["EndHour"];
						dataRow["IsFinalApproved"] = sqlDataReader["IsFinalApproved"];
						bool? isFinalApproved = null;
						if (!(sqlDataReader["IsFinalApproved"] is DBNull) && !string.IsNullOrEmpty(sqlDataReader["IsFinalApproved"].ToString()))
						{
							isFinalApproved = new bool?(Convert.ToBoolean(sqlDataReader["IsFinalApproved"]));
						}
						dataRow["OperationsID"] = sqlDataReader["OperationsID"];
						dataRow["RequestsID"] = sqlDataReader["RequestsID"];
						dataRow["StartDate"] = ((sqlDataReader["StartDate"].ToString() != "") ? ShDate.DateToStr((DateTime)sqlDataReader["StartDate"]) : "");
						dataRow["StartHour"] = UtilityManagers.ConvertToTimeFormat(sqlDataReader["StartHour"].ToString());
						dataRow["SubmittedByEmployeeID"] = sqlDataReader["SubmittedByEmployeeID"];
						dataRow["SubmittedDate"] = ((sqlDataReader["SubmittedDate"].ToString() != "") ? (ShDate.DateToStr((DateTime)sqlDataReader["SubmittedDate"]) + " " + ((DateTime)sqlDataReader["SubmittedDate"]).ToString("hh:mm")) : "");
						dataRow["Type"] = sqlDataReader["Type"];
						short typeNo = Convert.ToInt16(sqlDataReader["Type"]);
						dataRow["EmployeeName"] = sqlDataReader["EmployeeName"];
						dataRow["Description"] = sqlDataReader["Description"];
						bool isDay;
						bool.TryParse(sqlDataReader["IS_DAY"].ToString(), out isDay);
						string value2 = sqlDataReader["TITLE"].ToString();
						dataRow["ManagerName"] = sqlDataReader["ManagerName"];
						dataRow["EmpManager"] = sqlDataReader["EmpManager"];
						dataRow["SEC_NO"] = sqlDataReader["SEC_NO"];
						dataRow["SECTIONS"] = sqlDataReader["SECTIONS"];
						dataRow["CurEmp_NO"] = sqlDataReader["CurEmp_NO"];
						dataRow["SEmp_No"] = sqlDataReader["SEmp_No"];
						dataRow["S_FDate"] = sqlDataReader["S_FDate"];
						dataRow["S_TDate"] = sqlDataReader["S_TDate"];
						dataRow["CurSection"] = sqlDataReader["CurSection"];
						dataRow["MoveUpCnt"] = sqlDataReader["MoveUpCnt"];
						dataRow["ReqStatus"] = sqlDataReader["ReqStatus"];
						dataRow["ParallelCount"] = sqlDataReader["ParallelCount"];
						dataRow["EmpSecNo"] = sqlDataReader["EmpSecNo"];
						dataRow["EmpSecTitle"] = sqlDataReader["EmpSecTitle"];
						dataRow["NeedParallelCount"] = sqlDataReader["NeedParallelCount"];
						dataRow["SubmittedFullName"] = sqlDataReader["SubmittedFullName"];
						string empty = string.Empty;
						base.GetDuration(Convert.ToInt32(dataRow["OperationsID"]), (int)typeNo, isDay, isFinalApproved, dataRow["StartHour"].ToString(), dataRow["EndHour"].ToString(), ref value, ref empty, ref value2);
						dataRow["TITLE"] = value2;
						dataRow["Duration"] = value;
						dataTable.Rows.Add(dataRow);
					}
				}
			}
			return dataTable;
		}

		// Token: 0x060001A4 RID: 420 RVA: 0x0001119C File Offset: 0x0000F39C
		public RequestRow GetDataById(int requestId)
		{
			DataRow dataRow = base.ExecuteDataRow("WF_RequestsGetDataByID", new object[]
			{
				requestId
			});
			if (dataRow != null)
			{
				return new RequestRow(dataRow);
			}
			return new RequestRow();
		}

		// Token: 0x060001A5 RID: 421 RVA: 0x000111D4 File Offset: 0x0000F3D4
		public int Insert(double empNo, short type, DateTime startDate, DateTime? endDate, int? startHour, int? endHour, string duration, double submittedByEmployeeId, int operationsId, string description, ActionEnum actionE, StatusEnum status, IoStatusEnum ioStatus, string source, string distination, short? deviceWent, short? deviceBack, bool? locationStatus, double? transferCost, bool isFirstTimeShift = false)
		{
			int num = "100,101,102,103,104,105".Split(new char[]
			{
				','
			}).Contains(type.ToString()) ? 0 : this.EnoughLimitValue(empNo, startDate, new short?(type), duration);
			if (num < 0)
			{
				return num;
			}
			DateTime date = DateTime.Now.AddDays((double)(-1 * WorkFlowsDatabase.Instance.Action.GetRequestValidDays(actionE))).Date;
			if (startDate >= date || duration.Contains('-'))
			{
				if (WorkFlowsDatabase.Instance.Action.GetLimitCount(actionE) >= this.GetRequestsCount(empNo, actionE))
				{
					string value = WorkFlowsDatabase.Instance.Employee.GetDataByEmpNo(empNo)["SEC_NO"].ToString();
					double num2 = WorkFlowsDatabase.Instance.Sections.GetManagerOfSection(empNo, ref value);
					if (num2 > 0.0)
					{
						DataRow conferment = WorkFlowsDatabase.Instance.Conferment.GetConferment(num2, (int)actionE, Convert.ToInt32(value));
						if (conferment != null)
						{
							num2 = Convert.ToDouble(conferment["EMP_NO"]);
						}
						object obj = base.ExecuteScalar("WF_RequestsInsert", new object[]
						{
							empNo,
							DateTime.Now,
							type,
							startDate,
							endDate,
							startHour,
							endHour,
							duration,
							submittedByEmployeeId,
							null,
							null,
							null,
							operationsId,
							description,
							num2,
							Convert.ToInt32(value),
							(short)status,
							(short)ioStatus,
							source,
							distination,
							deviceWent,
							deviceBack,
							locationStatus,
							transferCost,
							isFirstTimeShift
						});
						if (obj != null)
						{
							num = Convert.ToInt32(obj);
						}
					}
					else
					{
						num = -2;
					}
				}
				else
				{
					num = -5;
				}
			}
			else
			{
				num = -3;
			}
			return num;
		}

		// Token: 0x060001A6 RID: 422 RVA: 0x00011400 File Offset: 0x0000F600
		public int Insert(double empNo, short type, DateTime startDate, DateTime? endDate, int? startHour, int? endHour, string duration, double submittedByEmployeeId, int operationsId, string description, ActionEnum actionE, StatusEnum status, IoStatusEnum ioStatus, bool isFirstTimeShift = false)
		{
			return this.Insert(empNo, type, startDate, endDate, startHour, endHour, duration, submittedByEmployeeId, operationsId, description, actionE, status, ioStatus, string.Empty, string.Empty, null, null, null, null, isFirstTimeShift);
		}

		// Token: 0x060001A7 RID: 423 RVA: 0x0001145C File Offset: 0x0000F65C
		public int Insert(double empNo, short type, DateTime startDate, DateTime? endDate, int? startHour, int? endHour, string duration, double submittedByEmployeeId, int operationsId, string description, ActionEnum actionE, double? transferCost, bool isFirstTimeShift = false)
		{
			return this.Insert(empNo, type, startDate, endDate, startHour, endHour, duration, submittedByEmployeeId, operationsId, description, actionE, StatusEnum.Unknown, IoStatusEnum.Unknown, string.Empty, string.Empty, null, null, null, transferCost, isFirstTimeShift);
		}

		// Token: 0x060001A8 RID: 424 RVA: 0x000114B0 File Offset: 0x0000F6B0
		public int Insert(double empNo, short type, DateTime startDate, DateTime? endDate, int? startHour, int? endHour, string duration, double submittedByEmployeeId, int operationsId, string description, ActionEnum actionE, bool isFirstTimeShift = false)
		{
			return this.Insert(empNo, type, startDate, endDate, startHour, endHour, duration, submittedByEmployeeId, operationsId, description, actionE, StatusEnum.Unknown, IoStatusEnum.Unknown, string.Empty, string.Empty, null, null, null, null, isFirstTimeShift);
		}

		// Token: 0x060001A9 RID: 425 RVA: 0x00011508 File Offset: 0x0000F708
		public int Insert(double empNo, short type, DateTime startDate, DateTime? endDate, int? startHour, int? endHour, string duration, double submittedByEmployeeId, int operationsId, string description, ActionEnum actionE, string source, string distination, short? deviceWent, short? deviceBack, bool? locationStatus, double? transferCost, bool isFirstTimeShift = false)
		{
			return this.Insert(empNo, type, startDate, endDate, startHour, endHour, duration, submittedByEmployeeId, operationsId, description, actionE, StatusEnum.Unknown, IoStatusEnum.Unknown, source, distination, deviceWent, deviceBack, locationStatus, transferCost, isFirstTimeShift);
		}

		// Token: 0x060001AA RID: 426 RVA: 0x0001153E File Offset: 0x0000F73E
		public DataRow GetRequestDuration(double empNo, DateTime sdate, short? cType)
		{
			return base.ExecuteDataRow("WF_GetRequestDuration", new object[]
			{
				empNo,
				sdate,
				cType
			});
		}

		// Token: 0x060001AB RID: 427 RVA: 0x0001156C File Offset: 0x0000F76C
		public int Update(int requestsId, double empNo, DateTime submittedDate, short type, DateTime startDate, DateTime? endDate, int? startHour, int? endHour, string duration, double submittedByEmployeeId, int operationsId, string description, ActionEnum actionE, StatusEnum status, IoStatusEnum ioStatus, string source, string distination, short? deviceWent, short? deviceBack, bool? locationStatus, double? transferCost, bool isFirstTimeShift)
		{
			int num = "100,101,102,103,104,105".Split(new char[]
			{
				','
			}).Contains(type.ToString()) ? 0 : this.EnoughLimitValue(empNo, startDate, new short?(type), duration);
			if (num < 0)
			{
				return num;
			}
			DateTime date = DateTime.Now.AddDays((double)(-1 * WorkFlowsDatabase.Instance.Action.GetRequestValidDays(actionE))).Date;
			if (startDate >= date || duration.Contains('-'))
			{
				string value = WorkFlowsDatabase.Instance.Employee.GetDataByEmpNo(empNo)["SEC_NO"].ToString();
				double num2 = WorkFlowsDatabase.Instance.Sections.GetManagerOfSection(empNo, ref value);
				DataRow conferment = WorkFlowsDatabase.Instance.Conferment.GetConferment(num2, Convert.ToInt32(actionE), Convert.ToInt32(value));
				if (conferment != null)
				{
					num2 = Convert.ToDouble(conferment["EMP_NO"]);
				}
				if (num2 > 0.0)
				{
					num = base.ExecuteNonQuery("WF_RequestsUpdate", new object[]
					{
						requestsId,
						empNo,
						submittedDate,
						type,
						startDate,
						endDate,
						startHour,
						endHour,
						duration,
						submittedByEmployeeId,
						operationsId,
						description,
						(short)status,
						(short)ioStatus,
						source,
						distination,
						deviceWent,
						deviceBack,
						locationStatus,
						num2,
						Convert.ToInt32(value),
						transferCost,
						isFirstTimeShift
					});
				}
				else
				{
					num = -2;
				}
			}
			else
			{
				num = -3;
			}
			return num;
		}

		// Token: 0x060001AC RID: 428 RVA: 0x0001177C File Offset: 0x0000F97C
		public int Update(int requestsId, double empNo, DateTime submittedDate, short type, DateTime startDate, DateTime? endDate, int? startHour, int? endHour, string duration, double submittedByEmployeeId, int operationsId, string description, ActionEnum actionE, StatusEnum status, IoStatusEnum ioStatus, bool isFirstTimeShift = false)
		{
			return this.Update(requestsId, empNo, submittedDate, type, startDate, endDate, startHour, endHour, duration, submittedByEmployeeId, operationsId, description, actionE, status, ioStatus, string.Empty, string.Empty, null, null, null, null, isFirstTimeShift);
		}

		// Token: 0x060001AD RID: 429 RVA: 0x000117DC File Offset: 0x0000F9DC
		public int Update(int requestsId, double empNo, DateTime submittedDate, short type, DateTime startDate, DateTime? endDate, int? startHour, int? endHour, string duration, double submittedByEmployeeId, int operationsId, string description, ActionEnum actionE, bool isFirstTimeShift = false)
		{
			return this.Update(requestsId, empNo, submittedDate, type, startDate, endDate, startHour, endHour, duration, submittedByEmployeeId, operationsId, description, actionE, StatusEnum.Unknown, IoStatusEnum.Unknown, string.Empty, string.Empty, null, null, null, null, isFirstTimeShift);
		}

		// Token: 0x060001AE RID: 430 RVA: 0x00011838 File Offset: 0x0000FA38
		public int Update(int requestsId, double empNo, DateTime submittedDate, short type, DateTime startDate, DateTime? endDate, int? startHour, int? endHour, string duration, double submittedByEmployeeId, int operationsId, string description, ActionEnum actionE, double? transferCost, bool isFirstTimeShift = false)
		{
			return this.Update(requestsId, empNo, submittedDate, type, startDate, endDate, startHour, endHour, duration, submittedByEmployeeId, operationsId, description, actionE, StatusEnum.Unknown, IoStatusEnum.Unknown, string.Empty, string.Empty, null, null, null, transferCost, isFirstTimeShift);
		}

		// Token: 0x060001AF RID: 431 RVA: 0x00011890 File Offset: 0x0000FA90
		public int Update(int requestsId, double empNo, DateTime submittedDate, short type, DateTime startDate, DateTime? endDate, int? startHour, int? endHour, string duration, double submittedByEmployeeId, int operationsId, string description, ActionEnum actionE, string source, string distination, short? deviceWent, short? deviceBack, bool? locationStatus, double? transferCost, bool isFirstTimeShift = false)
		{
			return this.Update(requestsId, empNo, submittedDate, type, startDate, endDate, startHour, endHour, duration, submittedByEmployeeId, operationsId, description, actionE, StatusEnum.Unknown, IoStatusEnum.Unknown, source, distination, deviceWent, deviceBack, locationStatus, transferCost, isFirstTimeShift);
		}

		// Token: 0x060001B0 RID: 432 RVA: 0x000118CC File Offset: 0x0000FACC
		public int Update(int requestsId, short startHour)
		{
			string commandText = string.Concat(new object[]
			{
				"UPDATE Requests SET StartHour = ",
				startHour,
				" WHERE RequestsID=",
				requestsId
			});
			return base.ExecuteNonQuery(CommandType.Text, commandText);
		}

		// Token: 0x060001B1 RID: 433 RVA: 0x00011910 File Offset: 0x0000FB10
		public int UpdateIsWardenCheck(int requestsId)
		{
			string commandText = "UPDATE Requests SET IsWardenCheck = ~IsWardenCheck WHERE RequestsID=" + requestsId;
			return base.ExecuteNonQuery(CommandType.Text, commandText);
		}

		// Token: 0x060001B2 RID: 434 RVA: 0x00011938 File Offset: 0x0000FB38
		public int UpdateAcceptCode(int requestsId, short acceptCode)
		{
			string commandText = string.Concat(new object[]
			{
				"UPDATE Requests SET AcceptCode = ",
				acceptCode,
				" WHERE RequestsID=",
				requestsId
			});
			return base.ExecuteNonQuery(CommandType.Text, commandText);
		}

		// Token: 0x060001B3 RID: 435 RVA: 0x0001197C File Offset: 0x0000FB7C
		public bool AcceptRequest(bool isFinalApproved, double approvalByManagerEmpNo, int requestsId, string managerIdea, short acceptCode)
		{
			return this.AcceptRequest(isFinalApproved, approvalByManagerEmpNo, requestsId, managerIdea, acceptCode, "", null, null);
		}

		// Token: 0x060001B4 RID: 436 RVA: 0x000119B0 File Offset: 0x0000FBB0
		public bool AcceptRequest(bool isFinalApproved, double approvalByManagerEmpNo, int requestsId, string managerIdea, short acceptCode, string requestedTime, int? IOTime, int? Tolerance)
		{
			return base.ExecuteNonQuery("WF_RequestsAcceptRequest", new object[]
			{
				isFinalApproved,
				approvalByManagerEmpNo,
				requestsId,
				managerIdea,
				acceptCode,
				requestedTime,
				IOTime,
				Tolerance
			}) >= 0;
		}

		// Token: 0x060001B5 RID: 437 RVA: 0x00011A17 File Offset: 0x0000FC17
		public int SetManagerKartabl()
		{
			return base.ExecuteNonQuery("WF_SetManagerKartabl", null);
		}

		// Token: 0x060001B6 RID: 438 RVA: 0x00011A25 File Offset: 0x0000FC25
		public int ChangeCurEmp_NO(int secNo, double empManager, double oldEmpManager)
		{
			return base.ExecuteNonQuery("WF_RequestChangeCurEmp_NO", new object[]
			{
				secNo,
				empManager,
				oldEmpManager
			});
		}

		// Token: 0x060001B7 RID: 439 RVA: 0x00011A54 File Offset: 0x0000FC54
		public bool MoveUp(double curEmpNo, string curSection, int requestId)
		{
			return base.ExecuteNonQuery(CommandType.Text, string.Concat(new object[]
			{
				"UPDATE Requests SET CurEmp_NO = ",
				curEmpNo,
				", MoveUpCnt =ISNULL(MoveUpCnt,0)+1 , CurSection = ",
				curSection,
				" WHERE RequestsID=",
				requestId
			})) >= 0;
		}

		// Token: 0x060001B8 RID: 440 RVA: 0x00011AA7 File Offset: 0x0000FCA7
		public DataRow GetRequestInfo(double empNo)
		{
			return base.ExecuteDataRow("WF_GetRequestInfo", new object[]
			{
				empNo
			});
		}

		// Token: 0x060001B9 RID: 441 RVA: 0x00011AC4 File Offset: 0x0000FCC4
		public int EnoughLimitValue(double empNo, DateTime sdate, short? type, string duration)
		{
			int result = 0;
			DataRow expr_12 = WorkFlowsDatabase.Instance.Cards.GetDataByCardNo(type);
			bool flag = Convert.ToBoolean(expr_12["IS_DAY"]);
			int num = Convert.ToInt32(expr_12["LimitValue"]);
			if (flag)
			{
				int num2;
				int.TryParse(duration, out num2);
				if (Convert.ToInt32(this.GetRequestDuration(empNo, sdate, type)["TotalValues"]) + num2 > num)
				{
					result = -4;
				}
			}
			else
			{
				string text = UtilityManagers.ConvertToFourDigitNumber(duration);
				if (new TimeSpan(Convert.ToInt32(text.Substring(0, 2)), Convert.ToInt32(text.Substring(2, 2)), 0).TotalHours > (double)num)
				{
					result = -4;
				}
			}
			return result;
		}

		// Token: 0x060001BA RID: 442 RVA: 0x00011B70 File Offset: 0x0000FD70
		public bool CheckEsteh(double empNo, DateTime startDate, DateTime endDate, ref string remain, ref string msg)
		{
			ParmfileRow dataRow = WorkFlowsDatabase.Instance.Parmfile.GetDataRow();
			if (dataRow.MyEste.ToString() == "" || dataRow.EmpEste.ToString() == "")
			{
				msg = "سقف مرخصی استعلاجی نا مشخص می باشد";
				return false;
			}
			bool result = true;
			double sDate = ShDate.StrToDate(ShDate.YearOf(startDate) + "/01/01").Date.ToOADate();
			double eDate = ShDate.StrToDate(ShDate.YearOf(endDate) + "/12/29").Date.ToOADate();
			double num = WorkFlowsDatabase.Instance.MorMam.UsedMorMam(empNo, 60, sDate, eDate);
			int num2 = ShDate.DifDays(endDate, startDate) + 1;
			if ((double)dataRow.MyEste >= num + (double)num2)
			{
				DateTime startDate2;
				DateTime dateTime;
				if (ShDate.DayOf(startDate) < dataRow.StartDay)
				{
					startDate2 = ShDate.StrToDate(string.Format("{0}/{1}/{2}", UtilityManagers.ConvertToFourDigitNumber(ShDate.YearOf(startDate).ToString()), UtilityManagers.ConvertToTowDigitNumber((int)(ShDate.MonthOf(startDate) - 1)), UtilityManagers.ConvertToTowDigitNumber((int)dataRow.StartDay)));
					dateTime = ShDate.StrToDate(string.Format("{0}/{1}/{2}", UtilityManagers.ConvertToFourDigitNumber(ShDate.YearOf(startDate).ToString()), UtilityManagers.ConvertToTowDigitNumber((int)ShDate.MonthOf(startDate)), UtilityManagers.ConvertToTowDigitNumber((int)dataRow.StartDay))).AddDays(-1.0);
				}
				else
				{
					startDate2 = ShDate.StrToDate(string.Format("{0}/{1}/{2}", UtilityManagers.ConvertToFourDigitNumber(ShDate.YearOf(startDate).ToString()), UtilityManagers.ConvertToTowDigitNumber((int)ShDate.MonthOf(startDate)), UtilityManagers.ConvertToTowDigitNumber((int)dataRow.StartDay)));
					dateTime = ShDate.StrToDate(string.Format("{0}/{1}/{2}", UtilityManagers.ConvertToFourDigitNumber(ShDate.YearOf(startDate).ToString()), UtilityManagers.ConvertToTowDigitNumber((int)(ShDate.MonthOf(startDate) + 1)), UtilityManagers.ConvertToTowDigitNumber((int)dataRow.StartDay))).AddDays(-1.0);
				}
				if (endDate <= dateTime)
				{
					double num3 = WorkFlowsDatabase.Instance.MorMam.UsedMorMam(empNo, 60, startDate2.Date.ToOADate(), dateTime.Date.ToOADate());
					if ((double)dataRow.EmpEste < num3 + (double)num2)
					{
						msg = "مدت در خواست استعلاجی بیشتر از سقف مجاز است";
						result = false;
					}
				}
				else
				{
					int num4 = ShDate.DifDays(startDate, dateTime);
					double num3 = WorkFlowsDatabase.Instance.MorMam.UsedMorMam(empNo, 60, startDate.ToOADate(), dateTime.ToOADate());
					if ((double)dataRow.EmpEste >= num3 + (double)num4)
					{
						int num5 = ShDate.DifDays(startDate2, endDate);
						num3 = WorkFlowsDatabase.Instance.MorMam.UsedMorMam(empNo, 60, startDate2.AddMonths(1).ToOADate(), dateTime.AddMonths(1).ToOADate());
						if ((double)dataRow.EmpEste < num3 + (double)num5)
						{
							msg = "مدت در خواست استعلاجی بیشتر از سقف مجاز است";
							result = false;
						}
					}
					else
					{
						msg = "مدت در خواست استعلاجی بیشتر از سقف مجاز است";
						result = false;
					}
				}
			}
			else
			{
				msg = "مدت در خواست استعلاجی بیشتر از سقف مجاز است";
				result = false;
			}
			remain = string.Format("مانده مرخصی استعلاجی برابر {0} روز می باشد.", (double)dataRow.EmpEste - (num + (double)num2));
			return result;
		}

		// Token: 0x060001BB RID: 443 RVA: 0x00011EB4 File Offset: 0x000100B4
		public RequestExistCountRow RequestsIsExist(DateTime startDate, DateTime endDate, double empNo)
		{
			DateTime dateTime = new DateTime(startDate.Year, startDate.Month, startDate.Day, 23, 59, 59);
			DateTime dateTime2 = new DateTime(endDate.Year, endDate.Month, endDate.Day, 23, 59, 59);
			DataTable dataTable = base.ExecuteDataTable("WF_RequestsIsExist", new object[]
			{
				dateTime.Date,
				dateTime2.Date,
				empNo
			});
			RequestExistCountRow requestExistCountRow = new RequestExistCountRow();
			try
			{
				requestExistCountRow.NullCount = Convert.ToInt32(dataTable.Select("IsFinalApproved is null")[0]["cnt"]);
			}
			catch
			{
				requestExistCountRow.NullCount = 0;
			}
			try
			{
				requestExistCountRow.TrueCount = Convert.ToInt32(dataTable.Select("IsFinalApproved=1")[0]["cnt"]);
			}
			catch
			{
				requestExistCountRow.TrueCount = 0;
			}
			try
			{
				requestExistCountRow.FalseCount = Convert.ToInt32(dataTable.Select("IsFinalApproved=0")[0]["cnt"]);
			}
			catch
			{
				requestExistCountRow.FalseCount = 0;
			}
			try
			{
				requestExistCountRow.NullTrueCount = Convert.ToInt32(dataTable.Select("IsFinalApproved is null or IsFinalApproved=1")[0]["cnt"]);
			}
			catch
			{
				requestExistCountRow.NullTrueCount = 0;
			}
			return requestExistCountRow;
		}

		// Token: 0x060001BC RID: 444 RVA: 0x00012034 File Offset: 0x00010234
		public RequestExistCountRow RequestsIsExistByType(DateTime startDate, double empNo, short rType, int operationsId, int startTime, int endTime)
		{
			DataTable dataTable = base.ExecuteDataTable("WF_RequestsIsExistByType", new object[]
			{
				startDate,
				empNo,
				rType,
				operationsId,
				startTime,
				endTime
			});
			RequestExistCountRow requestExistCountRow = new RequestExistCountRow();
			try
			{
				requestExistCountRow.NullCount = Convert.ToInt32(dataTable.Select("IsFinalApproved is null")[0]["cnt"]);
			}
			catch
			{
				requestExistCountRow.NullCount = 0;
			}
			try
			{
				requestExistCountRow.TrueCount = Convert.ToInt32(dataTable.Select("IsFinalApproved=1")[0]["cnt"]);
			}
			catch
			{
				requestExistCountRow.TrueCount = 0;
			}
			try
			{
				requestExistCountRow.FalseCount = Convert.ToInt32(dataTable.Select("IsFinalApproved=0")[0]["cnt"]);
			}
			catch
			{
				requestExistCountRow.FalseCount = 0;
			}
			try
			{
				requestExistCountRow.NullTrueCount = Convert.ToInt32(dataTable.Select("IsFinalApproved is null or IsFinalApproved=1")[0]["cnt"]);
			}
			catch
			{
				requestExistCountRow.NullTrueCount = 0;
			}
			return requestExistCountRow;
		}

		// Token: 0x060001BD RID: 445 RVA: 0x00012184 File Offset: 0x00010384
		public RequestExistCountRow RequestsHourlyIsExist(DateTime startDate, double empNo, int startTime, int endTime, int requestsID = 0)
		{
			DataTable dataTable = base.ExecuteDataTable("WF_RequestsHourlyIsExist", new object[]
			{
				startDate,
				empNo,
				startTime,
				endTime,
				requestsID
			});
			RequestExistCountRow requestExistCountRow = new RequestExistCountRow();
			try
			{
				requestExistCountRow.NullCount = Convert.ToInt32(dataTable.Select("IsFinalApproved is null")[0]["cnt"]);
			}
			catch
			{
				requestExistCountRow.NullCount = 0;
			}
			try
			{
				requestExistCountRow.TrueCount = Convert.ToInt32(dataTable.Select("IsFinalApproved=1")[0]["cnt"]);
			}
			catch
			{
				requestExistCountRow.TrueCount = 0;
			}
			try
			{
				requestExistCountRow.FalseCount = Convert.ToInt32(dataTable.Select("IsFinalApproved=0")[0]["cnt"]);
			}
			catch
			{
				requestExistCountRow.FalseCount = 0;
			}
			try
			{
				requestExistCountRow.NullTrueCount = Convert.ToInt32(dataTable.Select("IsFinalApproved is null or IsFinalApproved=1")[0]["cnt"]);
			}
			catch
			{
				requestExistCountRow.NullTrueCount = 0;
			}
			return requestExistCountRow;
		}

		// Token: 0x060001BE RID: 446 RVA: 0x000122C8 File Offset: 0x000104C8
		public RequestCountRow GetSubmitCount(string curEmpNo, string startDate)
		{
			DataRow dataRow = base.ExecuteDataRow("WF_GetSubmitCountRequest", new object[]
			{
				Convert.ToDouble(curEmpNo),
				DateTime.FromOADate(Convert.ToDouble(startDate)).Date
			});
			if (dataRow != null)
			{
				return new RequestCountRow(dataRow);
			}
			return new RequestCountRow();
		}

		// Token: 0x060001BF RID: 447 RVA: 0x00012320 File Offset: 0x00010520
		public int GetRequestsCount(double curEmpNo, ActionEnum actionStatus)
		{
			int result;
			try
			{
				int num = Convert.ToInt32(actionStatus);
				object obj = base.ExecuteScalar("WF_RequestsCountByEmpNO", new object[]
				{
					curEmpNo,
					num
				});
				result = ((obj == null) ? -1 : Convert.ToInt32(obj));
			}
			catch
			{
				result = 0;
			}
			return result;
		}

		// Token: 0x060001C0 RID: 448 RVA: 0x00012384 File Offset: 0x00010584
		public DataTable GetNewRequest(double curEmpNo, DateTime startDate)
		{
			return base.ExecuteDataTable("WF_GetNewRequest", new object[]
			{
				curEmpNo,
				startDate
			});
		}

		// Token: 0x060001C1 RID: 449 RVA: 0x000123AC File Offset: 0x000105AC
		public DataTable GetNewRequestWithRequestId(double curEmpNo)
		{
			string commandText = "SELECT r.*,c.TITLE,e.[NAME],e.FAMILY FROM Requests r \nJOIN CARDS c ON  r.[Type] = c.CARD_NO \nJOIN EMPLOYEE e ON  e.EMP_NO = r.EMP_NO \nWHERE  r.IsFinalApproved IS NULL AND r.SubmittedDate >= DATEADD(second,-20, GETDATE()) AND  \n       r.CurEmp_NO = " + curEmpNo;
			return base.ExecuteDataTable(CommandType.Text, commandText);
		}

		// Token: 0x060001C2 RID: 450 RVA: 0x000123D2 File Offset: 0x000105D2
		public DataTable GetNotExecute(DateTime startDate, double empNo)
		{
			return base.ExecuteDataTable("WF_RequestGetNotExecute", new object[]
			{
				startDate,
				empNo
			});
		}

		// Token: 0x060001C3 RID: 451 RVA: 0x000123F7 File Offset: 0x000105F7
		public DataTable GetAllNotExecute(DateTime startDate)
		{
			return base.ExecuteDataTable("WF_RequestGetAllNotExecute", new object[]
			{
				startDate
			});
		}

		// Token: 0x060001C4 RID: 452 RVA: 0x00012414 File Offset: 0x00010614
		public DataTable GetNeedRepairRequest()
		{
			string commandText = "SELECT r.RequestsID,r.EMP_NO,e.SEC_NO,s.EmpManager  FROM Requests r  \n JOIN EMPLOYEE e ON e.EMP_NO = r.EMP_NO \n JOIN SECTIONS s ON s.SEC_NO = e.SEC_NO \nWHERE r.IsFinalApproved IS NULL AND r.CurSection NOT IN(SELECT s.SEC_NO FROM SECTIONS s)";
			return base.ExecuteDataTable(CommandType.Text, commandText);
		}

		// Token: 0x060001C5 RID: 453 RVA: 0x00012430 File Offset: 0x00010630
		public void ManageNotExecuteRequest(DateTime startDate, double currentEmpNo, short registerTolerance)
		{
		    IEnumerator enumerator =
		        WorkFlowsDatabase.Instance.Requests.GetNotExecute(startDate, currentEmpNo).Rows.GetEnumerator();
			{
				while (enumerator.MoveNext())
				{
					RequestRow requestRow = new RequestRow((DataRow)enumerator.Current);
					string text = "";
					WorkFlowsDatabase.Instance.Requests.SaveToPwKara(requestRow, requestRow.ApprovalByManagerEmpNo, "WF", ref text, registerTolerance);
				}
			}
		}

		// Token: 0x060001C6 RID: 454 RVA: 0x000124BC File Offset: 0x000106BC
		public string ManageAllNotExecuteRequest(DateTime startDate, short registerTolerance)
		{
			string text = string.Empty;
		    IEnumerator enumerator = WorkFlowsDatabase.Instance.Requests.GetAllNotExecute(startDate).Rows.GetEnumerator();
			{
				while (enumerator.MoveNext())
				{
					RequestRow requestRow = new RequestRow((DataRow)enumerator.Current);
					string str = "";
					WorkFlowsDatabase.Instance.Requests.SaveToPwKara(requestRow, requestRow.ApprovalByManagerEmpNo, "WF", ref str, registerTolerance);
					text = text + str + "\n";
				}
			}
			return text;
		}

		// Token: 0x060001C7 RID: 455 RVA: 0x00012560 File Offset: 0x00010760
		public int RepairRequest(double curEmpNo, string curSection, int requestsId)
		{
			string commandText = string.Concat(new object[]
			{
				"UPDATE Requests SET CurEmp_NO = ",
				curEmpNo,
				" , CurSection = ",
				curSection,
				" WHERE RequestsID=",
				requestsId
			});
			return base.ExecuteNonQuery(CommandType.Text, commandText);
		}

		// Token: 0x060001C8 RID: 456 RVA: 0x000125B0 File Offset: 0x000107B0
		public void RepairRequest()
		{
			foreach (DataRow dataRow in this.GetNeedRepairRequest().Rows)
			{
				string curSection = dataRow["SEC_NO"].ToString();
				int requestsId = Convert.ToInt32(dataRow["RequestsID"]);
				double curEmpNo;
				if (dataRow["EmpManager"] is DBNull || Convert.ToDouble(dataRow["EmpManager"]) < 1.0)
				{
					double empNo = Convert.ToDouble(dataRow["EMP_NO"]);
					curEmpNo = WorkFlowsDatabase.Instance.Sections.GetManagerOfSection(empNo, ref curSection);
				}
				else
				{
					curEmpNo = Convert.ToDouble(dataRow["EmpManager"]);
				}
				WorkFlowsDatabase.Instance.Requests.RepairRequest(curEmpNo, curSection, requestsId);
			}
		}

		// Token: 0x060001C9 RID: 457 RVA: 0x000126B0 File Offset: 0x000108B0
		public bool SaveToPwKara(RequestRow reqInc, double currentEmpNo, string userPassword, ref string msg, short registerTolerance)
		{
			string empty = string.Empty;
			if (!this.CheckAccess(reqInc, currentEmpNo, ref empty))
			{
				msg = string.Concat(new object[]
				{
					reqInc.RequestsId,
					"#",
					string.Format("شما مجاز به تایید مجوز " + reqInc.Title + " نمی باشید.", new object[0]),
					empty
				});
				return false;
			}
			string text = "";
			DataRow dataRow = null;
			AcceptCodeEnum acceptCodeEnum = AcceptCodeEnum.Accept;
			bool flag = Convert.ToBoolean(reqInc.IsDay);
			if (reqInc.AcceptCode == 21)
			{
				return WorkFlowsDatabase.Instance.Requests.RevokeRequest(reqInc, flag, ref dataRow, currentEmpNo) < 1;
			}
			if (WorkFlowsDatabase.Instance.ParallelApprove.HaveNotAccept(reqInc.RequestsId, (int)reqInc.Type))
			{
				acceptCodeEnum = AcceptCodeEnum.ParallelApproval;
				msg = reqInc.RequestsId + "#نیاز به تایید کارشناس دارد";
			}
			else
			{
				switch (reqInc.Type)
				{
				case 100:
					if (!WorkFlowsDatabase.Instance.DataFile.Insert((reqInc.Duration == "") ?(short) 0 : short.Parse(reqInc.Duration), reqInc.StartDate.ToOADate(), (short)reqInc.StartHour, reqInc.EmpNo, true, 0))
					{
						msg = reqInc.RequestsId + "#خطا در ثبت تردد وجود دارد";
						return false;
					}
					break;
				case 101:
				{
					DataTable dataTable = WorkFlowsDatabase.Instance.DataFile.GetDataByDateAndTime(reqInc.EmpNo, reqInc.StartDate.ToOADate(), (short)reqInc.StartHour);
					if (dataTable == null || dataTable.Rows.Count <= 0)
					{
						msg = string.Concat(new object[]
						{
							reqInc.RequestsId,
							"#در تاریخ ",
							ShDate.CompleteDateName(reqInc.StartDate),
							" ترددی برای ",
							reqInc.FullName,
							" وجود ندارد"
						});
						return false;
					}
					if (reqInc.EndHour < 1)
					{
						WorkFlowsDatabase.Instance.DataFile.Delete(reqInc.StartDate.ToOADate(), (short)reqInc.StartHour, reqInc.EmpNo);
					}
					else
					{
						WorkFlowsDatabase.Instance.DataFile.UpdateByModify(Convert.ToDateTime(dataTable.Rows[0]["DATE_"]).Date.ToOADate(), (short)reqInc.StartHour, (short)reqInc.EndHour, reqInc.EmpNo, (reqInc.Duration == "") ? (short)0 : short.Parse(reqInc.Duration), true);
					}
					break;
				}
				case 102:
					if (WorkFlowsDatabase.Instance.EmpGrps.Insert(reqInc.EmpNo, reqInc.StartDate.Date.ToOADate(), reqInc.EndHour, new int?(reqInc.StartHour)) && reqInc.Duration != "0" && !WorkFlowsDatabase.Instance.EmpGrps.Insert(reqInc.EmpNo, reqInc.StartDate.AddDays(double.Parse(reqInc.Duration)).ToOADate(), reqInc.StartHour, new int?(reqInc.EndHour)))
					{
						msg = reqInc.RequestsId + "#در برنامه حضور غیاب اعمال نشد!!";
						return false;
					}
					break;
				case 103:
					if (reqInc.EndDate.HasValue)
					{
						TimeSpan timeSpan = reqInc.EndDate.Value.Date.Subtract(reqInc.StartDate.Date);
						for (int i = 0; i <= timeSpan.Days; i++)
						{
							DataTable dataTable = WorkFlowsDatabase.Instance.Overs.GetDataByEmpNo(reqInc.EmpNo, reqInc.StartDate.AddDays((double)i).ToOADate());
							if (dataTable != null && dataTable.Rows.Count > 0)
							{
								if (!WorkFlowsDatabase.Instance.Overs.Update(reqInc.EmpNo, Convert.ToDateTime(dataTable.Rows[0]["DATE_"]).ToOADate(), short.Parse(reqInc.Duration), DateTime.Now.Date.ToOADate(), currentEmpNo.ToString(), Convert.ToInt16(dataTable.Rows[0]["TIME_"])))
								{
									msg = string.Concat(new object[]
									{
										reqInc.RequestsId,
										"#",
										reqInc.Title,
										" برای ",
										reqInc.FullName,
										"در برنامه حضور غیاب اعمال نشد!!"
									});
									return false;
								}
							}
							else if (!WorkFlowsDatabase.Instance.Overs.Insert(reqInc.EmpNo, reqInc.StartDate.AddDays((double)i).ToOADate(), short.Parse(reqInc.Duration), DateTime.Now.Date.ToOADate(), currentEmpNo.ToString()))
							{
								msg = string.Concat(new object[]
								{
									reqInc.RequestsId,
									"#",
									reqInc.Title,
									" برای ",
									reqInc.FullName,
									"در برنامه حضور غیاب اعمال نشد!!"
								});
								return false;
							}
						}
					}
					break;
				case 104:
				{
					short value = 0;
					if (reqInc.StartHour == 0)
					{
						value = 7;
					}
					else if (reqInc.StartHour == 1)
					{
						value = 4;
					}
					if (!WorkFlowsDatabase.Instance.MorMam.Insert(reqInc.EmpNo, null, double.Parse(reqInc.Duration), null, new short?((short)57), reqInc.Description, null, UtilityManagers.ConvertXorToString(currentEmpNo.ToString()), UtilityManagers.ConvertXorToString(userPassword), null, UtilityManagers.ConvertXorToString(DateTime.Now.ToShortTimeString()), null, new short?(value), DateTime.Now.Date.ToOADate(), reqInc.StartDate.Date.ToOADate(), new DateTime(1899, 12, 30).ToOADate(), DateTime.Now.Date.ToOADate()))
					{
						msg = string.Concat(new object[]
						{
							reqInc.RequestsId,
							"#",
							reqInc.Title,
							" برای ",
							reqInc.FullName,
							"در برنامه حضور غیاب اعمال نشد!!"
						});
						return false;
					}
					break;
				}
				case 105:
				{
					DataTable dataTable = WorkFlowsDatabase.Instance.DataFile.GetDataByDateAndTime(reqInc.EmpNo, reqInc.StartDate.ToOADate(), (short)reqInc.StartHour);
					if (dataTable == null || dataTable.Rows.Count <= 0)
					{
						msg = string.Concat(new object[]
						{
							reqInc.RequestsId,
							"#در تاریخ ",
							ShDate.CompleteDateName(reqInc.StartDate),
							" ترددی برای ",
							reqInc.FullName,
							" وجود ندارد"
						});
						return false;
					}
					WorkFlowsDatabase.Instance.DataFile.UpdateByModify(Convert.ToDateTime(dataTable.Rows[0]["DATE_"]).Date.ToOADate(), (short)reqInc.StartHour, (short)reqInc.StartHour, reqInc.EmpNo, (reqInc.Duration == "") ?(short) 0 : short.Parse(reqInc.Duration), false);
					break;
				}
				default:
					if (flag)
					{
						if (reqInc.EndDate.HasValue && WorkFlowsDatabase.Instance.MorMam.IsExist(reqInc.StartDate.ToOADate(), reqInc.EndDate.Value.ToOADate(), reqInc.EmpNo) > 0)
						{
							msg = string.Concat(new object[]
							{
								reqInc.RequestsId,
								"#شما مجاز به تایید نمی باشید.\n در تاریخ ",
								ShDate.CompleteDateName(reqInc.StartDate),
								" درخواست روزانه برای ",
								reqInc.FullName,
								" وجود دارد."
							});
							return false;
						}
						if (reqInc.Type == 60 && reqInc.EndDate.HasValue && !WorkFlowsDatabase.Instance.Requests.CheckEsteh(reqInc.EmpNo, reqInc.StartDate.Date, reqInc.EndDate.Value.Date, ref text, ref msg))
						{
							msg = reqInc.RequestsId + "#" + msg;
							return false;
						}
						int num = 0;
						LeaveType operationsId = (LeaveType)reqInc.OperationsId;
						if (operationsId != LeaveType.Duty)
						{
							if (operationsId == LeaveType.Leave)
							{
								if (!WorkFlowsDatabase.Instance.MorMam.Insert(reqInc, UtilityManagers.ConvertXorToString(currentEmpNo.ToString()), UtilityManagers.ConvertXorToString(userPassword), ShDate.DateToStr(reqInc.StartDate, false), ShDate.IsLeapYear(ShDate.CurrentYear()), ref num))
								{
									msg = reqInc.RequestsId + "#مشکل ثبت در حضور و غیاب";
									return false;
								}
								if (num == -2)
								{
									msg = reqInc.RequestsId + "#گروه چرخشي براي گروه جاری تعريف نشده";
									return false;
								}
								if (num == -3)
								{
									msg = string.Concat(new object[]
									{
										reqInc.RequestsId,
										"#گروه چرخشي براي ",
										reqInc.FullName,
										" تعريف نشده"
									});
									return false;
								}
							}
						}
						else if (!WorkFlowsDatabase.Instance.MorMam.Insert(reqInc, UtilityManagers.ConvertXorToString(currentEmpNo.ToString()), UtilityManagers.ConvertXorToString(userPassword), ShDate.DateToStr(reqInc.StartDate, false), ShDate.IsLeapYear(ShDate.CurrentYear()), ref num))
						{
							msg = reqInc.RequestsId + "#مشکل ثبت در حضور و غیاب";
							return false;
						}
					}
					else
					{
						short type = reqInc.Type;
						switch (reqInc.OperationsId)
						{
						case 3:
						case 5:
							if (DateTime.Now.Subtract(reqInc.StartDate).TotalMinutes >= (double)registerTolerance || registerTolerance == 0)
							{
								if (!WorkFlowsDatabase.Instance.DataFile.UpdateTimeStatus(reqInc.StartDate.Date.ToOADate(), (short)reqInc.StartHour, (short)reqInc.EndHour, reqInc.EmpNo, ref type, ref dataRow, reqInc.Duration))
								{
									if (type == -1)
									{
										acceptCodeEnum = AcceptCodeEnum.NoDateFile;
										msg = string.Concat(new object[]
										{
											reqInc.RequestsId,
											"#در تاریخ ",
											ShDate.CompleteDateName(reqInc.StartDate),
											" ترددی در بازه درخواستی برای ",
											reqInc.FullName,
											" وجود ندارد"
										});
									}
									else
									{
										acceptCodeEnum = AcceptCodeEnum.Other;
										msg = reqInc.RequestsId + "#در برنامه حضور غیاب ثبت نشد!!";
									}
								}
							}
							else
							{
								acceptCodeEnum = AcceptCodeEnum.RegisterTolerance;
								msg = reqInc.RequestsId + "#در برنامه حضور غیاب ثبت نشد!!مدت انتظار تایید";
							}
							break;
						case 4:
							if (reqInc.Status == StatusEnum.ExitFromOrg)
							{
								if (!WorkFlowsDatabase.Instance.DataFile.UpdateTimeStatus(reqInc.StartDate.Date.ToOADate(), (short)reqInc.StartHour, (short)reqInc.EndHour, reqInc.EmpNo, ref type, ref dataRow, reqInc.Duration))
								{
									if (type == -1)
									{
										acceptCodeEnum = AcceptCodeEnum.NoDateFile;
										msg = string.Concat(new object[]
										{
											reqInc.RequestsId,
											"#در تاریخ ",
											ShDate.CompleteDateName(reqInc.StartDate),
											" ترددی در بازه درخواستی برای ",
											reqInc.FullName,
											" وجود ندارد"
										});
									}
									else
									{
										acceptCodeEnum = AcceptCodeEnum.Other;
										msg = reqInc.RequestsId + "#در برنامه حضور غیاب ثبت نشد!!";
									}
								}
								else
								{
									WorkFlowsDatabase.Instance.DataFile.Insert(0, reqInc.StartDate.ToOADate(), (short)reqInc.EndHour, reqInc.EmpNo, true, 1);
									WorkFlowsDatabase.Instance.DataFile.Insert(0, reqInc.StartDate.ToOADate(), (short)(reqInc.EndHour + 1), reqInc.EmpNo, true, 1);
								}
							}
							else
							{
								WorkFlowsDatabase.Instance.DataFile.Insert(0, reqInc.StartDate.ToOADate(), (short)reqInc.StartHour, reqInc.EmpNo, true, 1);
								WorkFlowsDatabase.Instance.DataFile.Insert(reqInc.Type, reqInc.StartDate.ToOADate(), (short)(reqInc.StartHour + 1), reqInc.EmpNo, true, 1);
								WorkFlowsDatabase.Instance.DataFile.Insert(0, reqInc.StartDate.ToOADate(), (short)reqInc.EndHour, reqInc.EmpNo, true, 1);
								WorkFlowsDatabase.Instance.DataFile.Insert(0, reqInc.StartDate.ToOADate(), (short)(reqInc.EndHour + 1), reqInc.EmpNo, true, 1);
							}
							break;
						}
					}
					break;
				}
			}
			string requestedTime = reqInc.Duration;
			if (acceptCodeEnum == AcceptCodeEnum.ParallelApproval)
			{
				requestedTime = string.Empty;
			}
			if (dataRow != null)
			{
				return WorkFlowsDatabase.Instance.Requests.AcceptRequest(true, currentEmpNo, reqInc.RequestsId, "", (short)acceptCodeEnum, requestedTime, new int?(Convert.ToInt32(dataRow["TIME_"])), new int?(Convert.ToInt32(dataRow["Tolerance"])));
			}
			return WorkFlowsDatabase.Instance.Requests.AcceptRequest(true, currentEmpNo, reqInc.RequestsId, "", (short)acceptCodeEnum, requestedTime, null, null);
		}

		// Token: 0x060001CA RID: 458 RVA: 0x000134A4 File Offset: 0x000116A4
		private bool CheckAccess(RequestRow reqRow, double currentEmpNo, ref string erroAccess)
		{
			LimitedManagerRow limitedManagerRow = null;
			DataRow dataByEmpNo = WorkFlowsDatabase.Instance.Employee.GetDataByEmpNo(reqRow.EmpNo);
			if (dataByEmpNo != null && dataByEmpNo["EmpMngr"].ToString().Equals(currentEmpNo.ToString()))
			{
				limitedManagerRow = new LimitedManagerRow
				{
					LimitDailyDuty = (int)((dataByEmpNo["Duty"] is DBNull) ? 0 : Convert.ToInt16(dataByEmpNo["Duty"])),
					LimitDailyLeave = (int)((dataByEmpNo["Leave"] is DBNull) ? 0 : Convert.ToInt16(dataByEmpNo["Leave"])),
					LimitDailyLeaveOut = (int)((dataByEmpNo["LeaveOut"] is DBNull) ? 0 : Convert.ToInt16(dataByEmpNo["LeaveOut"])),
					LimitForgottenInOut = true,
					LimitInOutCorrection = true,
					LimitLeaveAddition = 9999,
					LimitOverTime = 9999,
					LimitShiftReplace = 999,
					LimitTimeDuty = 2359,
					LimitTimeLeave = 2359,
					LimitTimeDutyNoReturn = true,
					LimitDailyDutyPerMonth = 999,
					LimitDailyLeavePerMonth = 999,
					LimitDailyLeaveOutPerMonth = 999,
					LimitForgottenInOutPerMonth = true,
					LimitInOutCorrectionPerMonth = true,
					LimitLeaveAdditionPerMonth = 9999,
					LimitOverTimePerMonth = 9999,
					LimitShiftReplacePerMonth = 999,
					LimitTimeDutyPerMonth = 99959,
					LimitTimeLeavePerMonth = 99959,
					LimitTimeDutyNoReturnPerMonth = true
				};
			}
			if (limitedManagerRow == null)
			{
				limitedManagerRow = WorkFlowsDatabase.Instance.LimitedManager.GetData(reqRow.CurEmpNo, reqRow.CurSection);
			}
			return this.HaveAccessLimitManagerPerRequest(limitedManagerRow, reqRow, ref erroAccess) && this.HaveAccessLimitManagerPerMonthAndSection(limitedManagerRow, reqRow, ref erroAccess);
		}

		// Token: 0x060001CB RID: 459 RVA: 0x00013668 File Offset: 0x00011868
		private bool HaveAccessLimitManagerPerMonthAndSection(LimitedManagerRow lmRow, RequestRow reqRow, ref string erroAccess)
		{
			ActionEnum actionIdByCard = WorkFlowsDatabase.Instance.Action.GetActionIdByCard(reqRow.Type, (short)reqRow.OperationsId, reqRow.IsDay);
			int num = 0;
			switch (actionIdByCard)
			{
			case ActionEnum.DailyLeave:
				if (lmRow.LimitDailyLeavePerMonth < 31)
				{
					DataTable dataTable = this.GetSumRequestInCurrentMonth(reqRow.EmpNo, (int)actionIdByCard, reqRow.IsDay, lmRow.LimitDailyLeavePerMonth);
					if (dataTable != null && dataTable.Rows.Count > 0)
					{
						erroAccess = this.GetErroAccess(dataTable, "ماهیانه");
						return false;
					}
				}
				break;
			case ActionEnum.DailyDuty:
				if (lmRow.LimitDailyDutyPerMonth < 31)
				{
					DataTable dataTable = this.GetSumRequestInCurrentMonth(reqRow.EmpNo, (int)actionIdByCard, reqRow.IsDay, lmRow.LimitDailyDutyPerMonth);
					if (dataTable != null && dataTable.Rows.Count > 0)
					{
						erroAccess = this.GetErroAccess(dataTable, "ماهیانه");
						return false;
					}
				}
				break;
			case ActionEnum.TimeLeave:
				if (lmRow.LimitTimeLeavePerMonth < 22000)
				{
					DataTable dataTable = this.GetSumRequestInCurrentMonth(reqRow.EmpNo, (int)actionIdByCard, reqRow.IsDay, lmRow.LimitTimeLeavePerMonth);
					if (dataTable != null && dataTable.Rows.Count > 0)
					{
						erroAccess = this.GetErroAccess(dataTable, "ماهیانه");
						return false;
					}
				}
				break;
			case ActionEnum.InOutCorrection:
			case ActionEnum.InOutTypeCorrection:
				if (!lmRow.LimitInOutCorrectionPerMonth)
				{
					return false;
				}
				break;
			case ActionEnum.OverTime:
			{
				DataTable dataTable;
				if (lmRow.LimitOverTimePerMonth < 22000)
				{
					dataTable = this.GetSumRequestInCurrentMonth(reqRow.EmpNo, (int)actionIdByCard, reqRow.IsDay, lmRow.LimitOverTimePerMonth);
					if (dataTable != null && dataTable.Rows.Count > 0)
					{
						erroAccess = this.GetErroAccess(dataTable, "ماهیانه");
						return false;
					}
				}
				DataRow byEmpNo = WorkFlowsDatabase.Instance.Sections.GetByEmpNo(reqRow.EmpNo);
				int limitValue = 0;
				if (!(byEmpNo["WF_LimitOverTime"] is DBNull))
				{
					int.TryParse(byEmpNo["WF_LimitOverTime"].ToString(), out limitValue);
				}
				int.TryParse(byEmpNo["SEC_NO"].ToString(), out num);
				dataTable = this.GetSectionOverTimeAccess((double)num, limitValue, DateTime.Now);
				if (dataTable != null && dataTable.Rows.Count > 0)
				{
					erroAccess = this.GetErroAccess(dataTable, "بخش");
					return false;
				}
				break;
			}
			case ActionEnum.ShiftReplace:
				if (lmRow.LimitShiftReplacePerMonth < 31)
				{
					DataTable dataTable = this.GetSumRequestInCurrentMonth(reqRow.EmpNo, (int)actionIdByCard, reqRow.IsDay, lmRow.LimitShiftReplacePerMonth);
					if (dataTable != null && dataTable.Rows.Count > 0)
					{
						erroAccess = this.GetErroAccess(dataTable, "ماهیانه");
						return false;
					}
				}
				break;
			case ActionEnum.LeaveAddition:
				if (lmRow.LimitLeaveAdditionPerMonth < 22000)
				{
					DataTable dataTable = this.GetSumRequestInCurrentMonth(reqRow.EmpNo, (int)actionIdByCard, reqRow.IsDay, lmRow.LimitLeaveAdditionPerMonth);
					if (dataTable != null && dataTable.Rows.Count > 0)
					{
						erroAccess = this.GetErroAccess(dataTable, "ماهیانه");
						return false;
					}
				}
				break;
			case ActionEnum.ForgottenInOut:
				if (!lmRow.LimitForgottenInOutPerMonth)
				{
					return false;
				}
				break;
			case ActionEnum.TimeDuty:
				if (lmRow.LimitTimeDutyPerMonth < 22000)
				{
					DataTable dataTable = this.GetSumRequestInCurrentMonth(reqRow.EmpNo, (int)actionIdByCard, reqRow.IsDay, lmRow.LimitTimeDutyPerMonth);
					if (dataTable != null && dataTable.Rows.Count > 0)
					{
						erroAccess = this.GetErroAccess(dataTable, "ماهیانه");
						return false;
					}
				}
				break;
			case ActionEnum.DailyLeaveOut:
				if (lmRow.LimitDailyLeaveOutPerMonth < 31)
				{
					DataTable dataTable = this.GetSumRequestInCurrentMonth(reqRow.EmpNo, (int)actionIdByCard, reqRow.IsDay, lmRow.LimitDailyLeaveOutPerMonth);
					if (dataTable != null && dataTable.Rows.Count > 0)
					{
						erroAccess = this.GetErroAccess(dataTable, "ماهیانه");
						return false;
					}
				}
				break;
			case ActionEnum.TimeDutyNoReturn:
				if (!lmRow.LimitTimeDutyNoReturnPerMonth)
				{
					return false;
				}
				break;
			}
			return true;
		}

		// Token: 0x060001CC RID: 460 RVA: 0x000139F4 File Offset: 0x00011BF4
		private bool HaveAccessLimitManagerPerRequest(LimitedManagerRow lmRow, RequestRow reqRow, ref string erroAccess)
		{
			switch (reqRow.Type)
			{
			case 100:
				return lmRow.LimitForgottenInOut && (!"9,10".Split(new char[]
				{
					','
				}).Contains(reqRow.Duration) || lmRow.LimitTimeDuty >= 1) && (!"17,26".Split(new char[]
				{
					','
				}).Contains(reqRow.Duration) || lmRow.LimitTimeLeave >= 1);
			case 101:
			case 105:
				return lmRow.LimitInOutCorrection;
			case 102:
				return lmRow.LimitShiftReplace >= Convert.ToInt32(reqRow.Duration);
			case 103:
				return lmRow.LimitOverTime >= Convert.ToInt32(reqRow.Duration);
			case 104:
				return lmRow.LimitLeaveAddition >= Convert.ToInt32(reqRow.Duration);
			default:
				switch (reqRow.OperationsId)
				{
				case 3:
					if (reqRow.IsDay)
					{
						return lmRow.LimitDailyDuty >= Convert.ToInt32(reqRow.Duration);
					}
					return lmRow.LimitTimeDuty >= Convert.ToInt32(reqRow.Duration);
				case 4:
					return Convert.ToBoolean(lmRow.LimitTimeDutyNoReturn);
				case 5:
					if (!reqRow.IsDay)
					{
						return lmRow.LimitTimeLeave >= Convert.ToInt32(reqRow.Duration);
					}
					if (reqRow.Type == 59 || reqRow.Type == 70)
					{
						return lmRow.LimitDailyLeaveOut >= Convert.ToInt32(reqRow.Duration);
					}
					return lmRow.LimitDailyLeave >= Convert.ToInt32(reqRow.Duration);
				default:
					return true;
				}
				break;
			}
		}

		// Token: 0x060001CD RID: 461 RVA: 0x00013BA4 File Offset: 0x00011DA4
		private string GetErroAccess(DataTable SumRequestInCurrentMonth, string title = "ماهیانه")
		{
			string result = string.Empty;
			foreach (DataRow dataRow in SumRequestInCurrentMonth.Rows)
			{
				result = string.Format("دوره {0} مصرفی {2} {1} \n", dataRow["Period"], UtilityManagers.ConvertToTimeFormat(UtilityManagers.ConvertToFiveDigitNumber(dataRow["sumreq"].ToString())), title);
			}
			return result;
		}

		// Token: 0x060001CE RID: 462 RVA: 0x00013C30 File Offset: 0x00011E30
		public int RevokeRequest(RequestRow reqInc, bool isDay, ref DataRow dtTaradod, double currentEmpNo)
		{
			int num = 0;
			if (reqInc.Type < 100)
			{
				if (isDay)
				{
					if ((reqInc.OperationsId == 5 || reqInc.OperationsId == 3) && reqInc.EndDate.HasValue)
					{
						DataRow dataRow = WorkFlowsDatabase.Instance.MorMam.Delete(new double?(reqInc.EmpNo), new short?(reqInc.Type), UtilityManagers.ConvertXorToString(currentEmpNo.ToString()), reqInc.StartDate.ToOADate(), reqInc.EndDate.Value.ToOADate());
						if (dataRow != null)
						{
							WorkFlowsDatabase.Instance.LogMorMam.Insert(dataRow, currentEmpNo);
						}
						else
						{
							num++;
						}
					}
				}
				else if (reqInc.OperationsId == 5 || reqInc.OperationsId == 3)
				{
					WorkFlowsDatabase.Instance.DataFile.UpdateTimeStatus(reqInc.StartDate.Date.ToOADate(), (short)reqInc.StartHour, (short)reqInc.EndHour, reqInc.EmpNo, reqInc.Duration);
				}
				else if (reqInc.OperationsId == 4)
				{
					if (reqInc.Status == StatusEnum.ExitFromOrg)
					{
						short num2 = 0;
						if (!WorkFlowsDatabase.Instance.DataFile.UpdateTimeStatus(reqInc.StartDate.Date.ToOADate(), (short)reqInc.StartHour, (short)reqInc.EndHour, reqInc.EmpNo, ref num2, ref dtTaradod, reqInc.Duration))
						{
							num++;
						}
						else
						{
							WorkFlowsDatabase.Instance.DataFile.Delete(reqInc.StartDate.ToOADate(), (short)reqInc.EndHour, reqInc.EmpNo);
							WorkFlowsDatabase.Instance.DataFile.Delete(reqInc.StartDate.ToOADate(), (short)(reqInc.EndHour + 1), reqInc.EmpNo);
						}
					}
					else
					{
						WorkFlowsDatabase.Instance.DataFile.Delete(reqInc.StartDate.ToOADate(), (short)reqInc.StartHour, reqInc.EmpNo);
						WorkFlowsDatabase.Instance.DataFile.Delete(reqInc.StartDate.ToOADate(), (short)(reqInc.StartHour + 1), reqInc.EmpNo);
						WorkFlowsDatabase.Instance.DataFile.Delete(reqInc.StartDate.ToOADate(), (short)reqInc.EndHour, reqInc.EmpNo);
						WorkFlowsDatabase.Instance.DataFile.Delete(reqInc.StartDate.ToOADate(), (short)(reqInc.EndHour + 1), reqInc.EmpNo);
					}
				}
			}
			else if (reqInc.Type == 101)
			{
				DataTable dataByDateAndTime = WorkFlowsDatabase.Instance.DataFile.GetDataByDateAndTime(reqInc.EmpNo, reqInc.StartDate.ToOADate(), (short)reqInc.EndHour);
				if (dataByDateAndTime != null && dataByDateAndTime.Rows.Count > 0)
				{
					if (reqInc.EndHour < 1)
					{
						if (!WorkFlowsDatabase.Instance.DataFile.Insert(0, reqInc.StartDate.ToOADate(), (short)reqInc.StartHour, reqInc.EmpNo, true, 0))
						{
							num++;
						}
					}
					else
					{
						WorkFlowsDatabase.Instance.DataFile.UpdateByModify(Convert.ToDateTime(dataByDateAndTime.Rows[0]["DATE_"]).Date.ToOADate(), (short)reqInc.EndHour, (short)reqInc.StartHour, reqInc.EmpNo, 0, true);
					}
				}
				else
				{
					num++;
				}
			}
			else if (reqInc.Type == 100)
			{
				if (!WorkFlowsDatabase.Instance.DataFile.Delete(reqInc.StartDate.ToOADate(), (short)reqInc.StartHour, reqInc.EmpNo))
				{
					num++;
				}
			}
			else if (reqInc.Type == 103 && !WorkFlowsDatabase.Instance.Overs.Delete(reqInc.RequestsId, reqInc.EmpNo, reqInc.StartDate))
			{
				num++;
			}
			if (num == 0 && !WorkFlowsDatabase.Instance.Requests.AcceptRequest(true, reqInc.CurEmpNo, reqInc.RequestsId, "", 22))
			{
				num++;
			}
			return num;
		}

		// Token: 0x060001CF RID: 463 RVA: 0x00014050 File Offset: 0x00012250
		public int GetSumDailyLeaveDurationByEmpNo(double empNo)
		{
			string commandText = "SELECT SUM(CAST(r.Duration AS INT)) SumDuration FROM Requests r WHERE r.IsFinalApproved IS NULL AND r.[Type]=57 AND r.EMP_NO=" + empNo;
			object obj = base.ExecuteScalar(CommandType.Text, commandText);
			if (!(obj is DBNull) && obj != null)
			{
				return Convert.ToInt32(obj);
			}
			return 0;
		}

		// Token: 0x060001D0 RID: 464 RVA: 0x0001408C File Offset: 0x0001228C
		public int GetSumTimeLeaveDurationByEmpNo(double empNo)
		{
			string commandText = "SELECT SUM(dbo.Time2Min(CAST(r.Duration AS INT))) SumDuration FROM Requests r WHERE r.IsFinalApproved IS NULL AND r.[Type]=17 AND r.EMP_NO=" + empNo;
			object obj = base.ExecuteScalar(CommandType.Text, commandText);
			if (!(obj is DBNull) && obj != null)
			{
				return Convert.ToInt32(obj);
			}
			return 0;
		}

		// Token: 0x060001D1 RID: 465 RVA: 0x000140C6 File Offset: 0x000122C6
		public DataTable GetSumRequestInCurrentMonth(double empNo, int actionId, bool isDay, int limitValue)
		{
			return base.ExecuteDataTable("WF_IsManagerAccess", new object[]
			{
				empNo,
				actionId,
				isDay,
				limitValue
			});
		}

		// Token: 0x060001D2 RID: 466 RVA: 0x000140FE File Offset: 0x000122FE
		public DataTable GetSectionOverTimeAccess(double secNo, int limitValue, DateTime startDate)
		{
			return base.ExecuteDataTable("WF_SectionOverTimeAccess", new object[]
			{
				secNo,
				limitValue,
				startDate
			});
		}

		// Token: 0x060001D3 RID: 467 RVA: 0x0001412C File Offset: 0x0001232C
		public DataTable GetStatistic(double managerEmpNo, DateTime startDate, DateTime endDate, int secNo)
		{
			return base.ExecuteDataTable("WF_RequestGetStatistic", new object[]
			{
				managerEmpNo,
				startDate,
				endDate,
				secNo
			});
		}

		// Token: 0x060001D4 RID: 468 RVA: 0x00014164 File Offset: 0x00012364
		public DataTable GetRemoveByPwkara(double curEmpNo)
		{
			return base.ExecuteDataTable("WF_RequestGetRemoveByPwkara", new object[]
			{
				curEmpNo
			});
		}

		// Token: 0x060001D5 RID: 469 RVA: 0x00014180 File Offset: 0x00012380
		public DataTable GetDifrenceDuration(double curEmpNo)
		{
			return base.ExecuteDataTable("WF_RequestsGetDifrenceDuration", new object[]
			{
				curEmpNo
			});
		}

		// Token: 0x060001D6 RID: 470 RVA: 0x0001419C File Offset: 0x0001239C
		public DataTable GetPrintViewer(double managerEmpNo)
		{
			return base.ExecuteDataTable("WF_RequestsGetPrintViewer", new object[]
			{
				managerEmpNo
			});
		}

		// Token: 0x060001D7 RID: 471 RVA: 0x000141B8 File Offset: 0x000123B8
		public DataTable GetPagingRequests(int pageNumber, int pageSize, ref int TotalRecords)
		{
			DataTable result;
			try
			{
				DataSet dataSet = base.ExecuteDataset("Paging_Request", new object[]
				{
					pageNumber,
					pageSize
				});
				TotalRecords = ((dataSet.Tables[1].Rows[0]["TotalRecords"] is DBNull) ? 0 : Convert.ToInt32(dataSet.Tables[1].Rows[0]["TotalRecords"]));
				result = dataSet.Tables[0];
			}
			catch
			{
				TotalRecords = 0;
				result = null;
			}
			return result;
		}

		// Token: 0x060001D8 RID: 472 RVA: 0x00014264 File Offset: 0x00012464
		public List<int> GetSummaryOverTime(double empNo, DateTime startDate, DateTime endDate)
		{
			string commandText = string.Format("SELECT dbo.Min2Time(ISNULL(SUM(dbo.Time2Min(ISNULL(r.Duration, 0))),0)) OverTime, \ndbo.Min2Time(ISNULL(SUM(dbo.Time2Min(ISNULL(r.StartHour, 0))),0)) BeforOverTime, \ndbo.Min2Time(ISNULL(SUM(dbo.Time2Min(ISNULL(r.EndHour, 0))),0)) AfterOverTime  \nFROM   Requests r \nWHERE  r.[Type] = {0} \n       AND r.EMP_NO = {1}        AND r.StartDate BETWEEN '{2}' AND '{3}'         AND r.IsFinalApproved = 1", new object[]
			{
				103,
				empNo,
				startDate.ToString("yyyy-MM-dd"),
				endDate.ToString("yyyy-MM-dd")
			});
			DataRow dataRow = base.ExecuteDataRow(CommandType.Text, commandText);
			List<int> list = new List<int>();
			if (dataRow != null)
			{
				list.Add(Convert.ToInt32(dataRow["OverTime"]));
				list.Add(Convert.ToInt32(dataRow["BeforOverTime"]));
				list.Add(Convert.ToInt32(dataRow["AfterOverTime"]));
			}
			return list;
		}
	}
}
