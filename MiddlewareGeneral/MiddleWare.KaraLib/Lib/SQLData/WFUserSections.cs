﻿using System;
using System.Data;

namespace PW.WorkFlows.SqlDal
{
	// Token: 0x02000006 RID: 6
	public class WFUserSections : BaseDAL
	{
		// Token: 0x0600005F RID: 95 RVA: 0x00003C40 File Offset: 0x00001E40
		public WFUserSections(WorkFlowsDatabase owner) : base(owner)
		{
		}

		// Token: 0x06000060 RID: 96 RVA: 0x00003D60 File Offset: 0x00001F60
		public DataTable GetIn(string userId)
		{
			string commandText = string.Format("SELECT *  FROM SECTIONS s WHERE \ns.TMFather IN(SELECT s.SEC_NO FROM SECTIONS s WHERE s.TMFather = 0) \nAND s.SEC_NO IN (SELECT ws.Sec_No FROM WFUserSections ws WHERE ws.UserId=N'{0}')", userId);
			return base.ExecuteDataTable(CommandType.Text, commandText);
		}

		// Token: 0x06000061 RID: 97 RVA: 0x00003D84 File Offset: 0x00001F84
		public DataTable Get(string userId)
		{
			string commandText = string.Format("SELECT *  FROM SECTIONS s WHERE \ns.SEC_NO IN (SELECT ws.Sec_No FROM WFUserSections ws WHERE ws.UserId=N'{0}')", userId);
			return base.ExecuteDataTable(CommandType.Text, commandText);
		}

		// Token: 0x06000062 RID: 98 RVA: 0x00003DA5 File Offset: 0x00001FA5
		public DataTable GetNotIn()
		{
			return base.ExecuteDataTable(CommandType.Text, "SELECT * FROM SECTIONS s WHERE \ns.TMFather IN(SELECT s.SEC_NO FROM SECTIONS s WHERE s.TMFather = 0) \nAND s.SEC_NO NOT IN (SELECT ws.Sec_No FROM WFUserSections ws)");
		}

		// Token: 0x06000063 RID: 99 RVA: 0x00003DB4 File Offset: 0x00001FB4
		public int Insert(string userId, int secNo, int? parentSecNo)
		{
			string commandText = string.Format("INSERT INTO WFUserSections(UserId,Sec_No,ParentSec_No)VALUES(N'{0}',{1},{2})", userId, secNo, parentSecNo);
			return base.ExecuteNonQuery(CommandType.Text, commandText);
		}

		// Token: 0x06000064 RID: 100 RVA: 0x00003DE1 File Offset: 0x00001FE1
		public int Delete(string userId, int secNo)
		{
			return base.ExecuteNonQuery(CommandType.Text, string.Format("DELETE FROM WFUserSections WHERE UserId=N'{0}' AND (Sec_No={1} OR ParentSec_No={1})", userId, secNo));
		}
	}
}
