﻿using System;
using System.Data;

namespace PW.WorkFlows.SqlDal
{
	// Token: 0x0200000E RID: 14
	public class ActionSection : BaseDAL
	{
		// Token: 0x060000CA RID: 202 RVA: 0x00003C40 File Offset: 0x00001E40
		public ActionSection(WorkFlowsDatabase owner) : base(owner)
		{
		}

		// Token: 0x060000CB RID: 203 RVA: 0x00005BA0 File Offset: 0x00003DA0
		public DataTable GetFreeEmployee(int actionId)
		{
			return base.ExecuteDataTable(CommandType.Text, "SELECT * FROM SECTIONS s WHERE s.SEC_NO NOT IN(SELECT era.SecNo  FROM ActionSection era WHERE era.ActionNo=" + actionId + ")");
		}

		// Token: 0x060000CC RID: 204 RVA: 0x00005BBE File Offset: 0x00003DBE
		public DataTable GetUnFreeEmployee(int actionId)
		{
			return base.ExecuteDataTable(CommandType.Text, "SELECT * FROM SECTIONS s WHERE s.SEC_NO IN(SELECT era.SecNo  FROM ActionSection era WHERE era.ActionNo=" + actionId + ")");
		}

		// Token: 0x060000CD RID: 205 RVA: 0x00005BDC File Offset: 0x00003DDC
		public int Insert(int secNo, int actionId, string duration)
		{
			return base.ExecuteNonQuery(CommandType.Text, string.Concat(new object[]
			{
				"INSERT INTO ActionSection(SecNo,ActionNo,Duration)VALUES(",
				secNo,
				",",
				actionId,
				",",
				duration,
				")"
			}));
		}

		// Token: 0x060000CE RID: 206 RVA: 0x00005C31 File Offset: 0x00003E31
		public int Delete(int secNo, int actionId)
		{
			return base.ExecuteNonQuery(CommandType.Text, string.Concat(new object[]
			{
				"DELETE FROM ActionSection WHERE SecNo=",
				secNo,
				" AND ActionNo=",
				actionId
			}));
		}

		// Token: 0x060000CF RID: 207 RVA: 0x00005C68 File Offset: 0x00003E68
		public int InsertAll(int actionId, string duration)
		{
			string commandText = string.Concat(new object[]
			{
				"INSERT INTO ActionSection(SecNo,ActionNo,Duration) \n(SELECT s.SEC_NO,",
				actionId,
				",",
				duration,
				"   FROM SECTIONS s WHERE s.SEC_NO NOT IN \n(SELECT as1.SecNo FROM ActionSection as1 WHERE as1.ActionNo=",
				duration,
				")) "
			});
			return base.ExecuteNonQuery(CommandType.Text, commandText);
		}

		// Token: 0x060000D0 RID: 208 RVA: 0x00005CBA File Offset: 0x00003EBA
		public int DeleteAll(int actionId)
		{
			return base.ExecuteNonQuery(CommandType.Text, "DELETE FROM ActionSection WHERE ActionNo=" + actionId);
		}
	}
}
