﻿using System;
using System.Data;

namespace PW.WorkFlows.SqlDal
{
	// Token: 0x0200002E RID: 46
	public class Substitute : BaseDAL
	{
		// Token: 0x060001FE RID: 510 RVA: 0x00003C40 File Offset: 0x00001E40
		public Substitute(WorkFlowsDatabase owner) : base(owner)
		{
		}

		// Token: 0x060001FF RID: 511 RVA: 0x00015380 File Offset: 0x00013580
		public bool IsSubstitute(double sEmpNo)
		{
			return Convert.ToInt32(base.ExecuteScalar("WF_EMPLOYEE_IsSubstitute", new object[]
			{
				sEmpNo
			})) > 0;
		}

		// Token: 0x06000200 RID: 512 RVA: 0x000153A4 File Offset: 0x000135A4
		public DataTable GetSubstitute(double sEmpNo)
		{
			return base.ExecuteDataTable("WF_EMPLOYEE_GetSubstitute", new object[]
			{
				sEmpNo
			});
		}

		// Token: 0x06000201 RID: 513 RVA: 0x000153C0 File Offset: 0x000135C0
		public DataTable GetAllByManager(double managerEmpNo)
		{
			string commandText = "SELECT s.*, dbo.ShamsyDate(s.StartDate) FStartDate,dbo.ShamsyDate(s.EndDate) FEndDate, \n  e.[NAME],e.FAMILY,CASE WHEN s.IsActive=1 THEN N'فعال' ELSE N'غيرفعال' END FActive, \n  CASE WHEN s.IsActive=0 THEN N'فعال' ELSE N'غيرفعال' END OFActive , \n  CASE WHEN s.IsInsertByAdmin=1 THEN N'Admin' WHEN s.IsInsertByAdmin IS NULL THEN '---' ELSE N'مدير' END OFIsInsertByAdmin \nFROM Substitute s \n  JOIN EMPLOYEE e ON e.EMP_NO=s.SubstituteEmpNo  \n WHERE s.ManagerEmpNo=" + managerEmpNo + "  ORDER BY s.StartDate";
			return base.ExecuteDataTable(CommandType.Text, commandText);
		}

		// Token: 0x06000202 RID: 514 RVA: 0x000153EB File Offset: 0x000135EB
		public DataTable GetAll()
		{
			return base.ExecuteDataTable(CommandType.Text, "SELECT s.*, dbo.ShamsyDate(s.StartDate) FStartDate,dbo.ShamsyDate(s.EndDate) FEndDate \n,e.[NAME],e.FAMILY,CASE WHEN s.IsActive=1 THEN N'فعال' ELSE N'غيرفعال' END FActive FROM Substitute s \nJOIN EMPLOYEE e ON e.EMP_NO=s.SubstituteEmpNo  \nORDER BY s.StartDate");
		}

		// Token: 0x06000203 RID: 515 RVA: 0x000153FC File Offset: 0x000135FC
		public DataRow GetCurrentSubstitute(double managerEmpNo)
		{
			string commandText = "SELECT top 1 * FROM Substitute s WHERE IsActive=1 AND GETDATE() BETWEEN s.StartDate AND s.EndDate AND s.ManagerEmpNo=" + managerEmpNo;
			return base.ExecuteDataRow(CommandType.Text, commandText);
		}

		// Token: 0x06000204 RID: 516 RVA: 0x00015424 File Offset: 0x00013624
		public DataRow GetSubstituteById(int substituteId)
		{
			string commandText = "SELECT * FROM Substitute s WHERE s.SubstituteID=" + substituteId;
			return base.ExecuteDataRow(CommandType.Text, commandText);
		}

		// Token: 0x06000205 RID: 517 RVA: 0x0001544C File Offset: 0x0001364C
		public bool IsExistActiveSubstitute(double managerEmpNo)
		{
			string commandText = "SELECT * FROM Substitute s WHERE IsActive=1 AND s.ManagerEmpNo=" + managerEmpNo;
			DataTable dataTable = base.ExecuteDataTable(CommandType.Text, commandText);
			return dataTable != null && dataTable.Rows.Count > 0;
		}

		// Token: 0x06000206 RID: 518 RVA: 0x00015488 File Offset: 0x00013688
		public bool Insert(double managerEmpNo, double substituteEmpNo, DateTime startDate, DateTime endDate, bool isActive, bool IsInsertByAdmin)
		{
			return base.ExecuteNonQuery("WF_SubstituteInsert", new object[]
			{
				managerEmpNo,
				substituteEmpNo,
				startDate,
				endDate,
				isActive,
				IsInsertByAdmin
			}) > 0;
		}

		// Token: 0x06000207 RID: 519 RVA: 0x000154E4 File Offset: 0x000136E4
		public bool UpdateIsActive(int substituteId, bool isActive)
		{
			string commandText = string.Concat(new object[]
			{
				"UPDATE Substitute SET IsActive =",
				Convert.ToInt32(isActive),
				"  WHERE SubstituteID=",
				substituteId
			});
			return base.ExecuteNonQuery(CommandType.Text, commandText) > 0;
		}

		// Token: 0x06000208 RID: 520 RVA: 0x00015530 File Offset: 0x00013730
		public bool UpdateIsActive(double managerEmpNo, bool isActive)
		{
			string commandText = string.Format("UPDATE Substitute SET IsActive = {0} WHERE ManagerEmpNo={1}", Convert.ToInt32(isActive), managerEmpNo);
			return base.ExecuteNonQuery(CommandType.Text, commandText) > 0;
		}
	}
}
