﻿using System;
using System.Data;

namespace PW.WorkFlows.SqlDal
{
	// Token: 0x02000033 RID: 51
	public class WfEmpCard : BaseDAL
	{
		// Token: 0x0600025D RID: 605 RVA: 0x00003C40 File Offset: 0x00001E40
		public WfEmpCard(WorkFlowsDatabase owner) : base(owner)
		{
		}

		// Token: 0x0600025E RID: 606 RVA: 0x00016FBB File Offset: 0x000151BB
		public DataTable GetFreeEmployee(int cardNo)
		{
			return base.ExecuteDataTable(CommandType.Text, "SELECT * FROM WF_GetEmployee_VI wgev WHERE wgev.EMP_NO IN (SELECT era.Emp_NO FROM WF_EmpCard era WHERE era.Card_NO=" + cardNo + ")");
		}

		// Token: 0x0600025F RID: 607 RVA: 0x00016FD9 File Offset: 0x000151D9
		public DataTable GetUnFreeEmployee(int cardNo)
		{
			return base.ExecuteDataTable(CommandType.Text, "SELECT * FROM WF_GetEmployee_VI wgev WHERE wgev.EMP_NO NOT IN (SELECT era.Emp_NO FROM WF_EmpCard era WHERE era.Card_NO=" + cardNo + ")");
		}

		// Token: 0x06000260 RID: 608 RVA: 0x00016FF7 File Offset: 0x000151F7
		public int Insert(int cardNo, double empNo)
		{
			return base.ExecuteNonQuery(CommandType.Text, string.Concat(new object[]
			{
				"DELETE FROM WF_EmpCard WHERE Card_NO=",
				cardNo,
				" AND Emp_NO=",
				empNo
			}));
		}

		// Token: 0x06000261 RID: 609 RVA: 0x0001702D File Offset: 0x0001522D
		public int Delete(int cardNo, double empNo)
		{
			return base.ExecuteNonQuery(CommandType.Text, string.Concat(new object[]
			{
				"INSERT INTO WF_EmpCard(Card_NO,Emp_NO)VALUES(",
				cardNo,
				",",
				empNo,
				")"
			}));
		}

		// Token: 0x06000262 RID: 610 RVA: 0x0001706B File Offset: 0x0001526B
		public int InsertAll(int cardNo)
		{
			return base.ExecuteNonQuery(CommandType.Text, "DELETE FROM WF_EmpCard WHERE Card_NO=" + cardNo);
		}

		// Token: 0x06000263 RID: 611 RVA: 0x00017084 File Offset: 0x00015284
		public int DeleteAll(int cardNo)
		{
			string commandText = string.Concat(new object[]
			{
				"INSERT INTO WF_EmpCard(Card_NO,Emp_NO) \n(SELECT ",
				cardNo,
				",wgev.EMP_NO   FROM WF_GetEmployee_VI wgev WHERE wgev.EMP_NO NOT IN  \n(SELECT era.Emp_NO FROM WF_EmpCard era WHERE era.Card_NO=",
				cardNo,
				"))"
			});
			return base.ExecuteNonQuery(CommandType.Text, commandText);
		}
	}
}
