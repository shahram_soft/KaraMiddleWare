﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using MiddleWare.KaraLib;
using MiddleWare.KaraLib.ViewModels;


namespace PW.WorkFlows.SqlDal
{
	// Token: 0x02000017 RID: 23
	public class Employee : BaseDAL
	{
		// Token: 0x06000108 RID: 264 RVA: 0x00003C40 File Offset: 0x00001E40
		public Employee(WorkFlowsDatabase owner) : base(owner)
		{
		}

		// Token: 0x06000109 RID: 265 RVA: 0x00008512 File Offset: 0x00006712
		public DataTable GetData()
		{
			return base.ExecuteDataTable("WF_EMPLOYEEGetData", null);
		}

		// Token: 0x0600010A RID: 266 RVA: 0x00008520 File Offset: 0x00006720
		public DataRow GetDataByEmpNo(double empNo)
		{
			return base.ExecuteDataRow("WF_EMPLOYEEGetDataByEmpNO", new object[]
			{
				empNo
			});
		}

		// Token: 0x0600010B RID: 267 RVA: 0x0000853C File Offset: 0x0000673C
		public DataTable GetPersonelData(short isKarmand)
		{
			return base.ExecuteDataTable("WF_EMPLOYEEGetPersonelData", new object[]
			{
				isKarmand
			});
		}

		// Token: 0x0600010C RID: 268 RVA: 0x00008558 File Offset: 0x00006758
		public DataTable GetManagersData()
		{
			return base.ExecuteDataTable("WF_EMPLOYEEGetManagersData", null);
		}

		// Token: 0x0600010D RID: 269 RVA: 0x00008568 File Offset: 0x00006768
		public bool ExistUserNameAndPassword(double EMP_NO)
		{
			string commandText = string.Format("SELECT Count(1) cnt \nFROM   EMPLOYEE e \nWHERE  e.EMP_NO = {0} \n       AND ( \n               e.WFUserName IS NOT NULL \n               OR e.WFUserName <> '' \n               OR e.WFUserPass IS NOT NULL \n               OR e.WFUserPass <> '' \n           )", EMP_NO);
			object obj = base.ExecuteScalar(CommandType.Text, commandText);
			return obj != null && Convert.ToInt32(obj) > 0;
		}

		// Token: 0x0600010E RID: 270 RVA: 0x0000859D File Offset: 0x0000679D
		public DataTable GetUnSelect(double managerEmpNo, int operationsId)
		{
			return base.ExecuteDataTable("WF_EMPLOYEEGetUnSelect", new object[]
			{
				managerEmpNo,
				operationsId
			});
		}

		// Token: 0x0600010F RID: 271 RVA: 0x000085C4 File Offset: 0x000067C4
		public bool CheckValidUserCode(string userCode, double empNo)
		{
			object obj = base.ExecuteScalar(CommandType.Text, string.Format("SELECT COUNT(1) Cnt FROM EMPLOYEE e WHERE e.WFUserName='{0}' AND EMP_NO<>{1}", userCode, empNo));
			return obj != null && Convert.ToUInt32(obj) == 0u;
		}

		// Token: 0x06000110 RID: 272 RVA: 0x000085F8 File Offset: 0x000067F8
		public DataTable GetUnWarden()
		{
			return base.ExecuteDataTable(CommandType.Text, "SELECT * FROM WF_GetEmployee_VI wgev WHERE wgev.IsWarden IS NULL OR wgev.IsWarden=0");
		}

		// Token: 0x06000111 RID: 273 RVA: 0x00008606 File Offset: 0x00006806
		public DataTable GetWarden()
		{
			return base.ExecuteDataTable(CommandType.Text, "SELECT * FROM WF_GetEmployee_VI wgev WHERE wgev.IsWarden=1");
		}

		// Token: 0x06000112 RID: 274 RVA: 0x00008614 File Offset: 0x00006814
		public DataTable GetEmployee_VI()
		{
			DataTable dataTable = new DataTable();
			dataTable.Columns.Add("EMP_NO", typeof(double));
			dataTable.Columns.Add("SEC_NO", typeof(double));
			dataTable.Columns.Add("FullName", typeof(string));
			dataTable.Columns.Add("NAME", typeof(string));
			dataTable.Columns.Add("FAMILY", typeof(string));
			dataTable.Columns.Add("IS_KARMAND", typeof(string));
			dataTable.Columns.Add("TITLE", typeof(string));
			using (SqlDataReader sqlDataReader = base.ExecuteReader(CommandType.Text, "SELECT * FROM WF_GetEmployee_VI ORDER BY FullName"))
			{
				if (sqlDataReader.HasRows)
				{
					while (sqlDataReader.Read())
					{
						DataRow dataRow = dataTable.NewRow();
						dataRow["EMP_NO"] = sqlDataReader["EMP_NO"].ToString();
						dataRow["FullName"] = sqlDataReader["FullName"];
						dataRow["NAME"] = sqlDataReader["NAME"];
						dataRow["FAMILY"] = sqlDataReader["FAMILY"];
						dataRow["SEC_NO"] = sqlDataReader["SEC_NO"].ToString();
						dataRow["IS_KARMAND"] = sqlDataReader["IS_KARMAND"];
						dataRow["TITLE"] = sqlDataReader["TITLE"];
						dataTable.Rows.Add(dataRow);
					}
				}
			}
			return dataTable;
		}

		// Token: 0x06000113 RID: 275 RVA: 0x000087E0 File Offset: 0x000069E0
		public DataTable GetUnPrintViewer()
		{
			return base.ExecuteDataTable(CommandType.Text, "SELECT * FROM WF_GetEmployee_VI wgev WHERE wgev.IsPrintViewer IS NULL OR wgev.IsPrintViewer=0");
		}

		// Token: 0x06000114 RID: 276 RVA: 0x000087EE File Offset: 0x000069EE
		public DataTable GetPrintViewer()
		{
			return base.ExecuteDataTable(CommandType.Text, "SELECT * FROM WF_GetEmployee_VI wgev WHERE wgev.IsPrintViewer=1");
		}

		// Token: 0x06000115 RID: 277 RVA: 0x000087FC File Offset: 0x000069FC
		public DataTable BySupervisor(bool isSupervisor)
		{
			return base.ExecuteDataTable("WF_EmployeeBySupervisor", new object[]
			{
				isSupervisor
			});
		}

		// Token: 0x06000116 RID: 278 RVA: 0x00008818 File Offset: 0x00006A18
		public DataTable ByEmpMngr(double empMngr)
		{
			return base.ExecuteDataTable(CommandType.Text, "SELECT * FROM WF_GetEmployee_VI wgev ORDER BY wgev.EmpMngr DESC");
		}

		// Token: 0x06000117 RID: 279 RVA: 0x00008826 File Offset: 0x00006A26
		public DataTable GetEmpMngr()
		{
			return base.ExecuteDataTable(CommandType.Text, "SELECT * FROM (SELECT 0 EMP_NO,'' [NAME],'' FAMILY,'همه' FullName UNION SELECT e.EMP_NO,e.[NAME],e.FAMILY, \ne.FAMILY+' - '+e.[NAME]+' - '+LTRIM(str(e.EMP_NO)) FullName FROM EMPLOYEE e WHERE e.EMP_NO IN  \n(SELECT e1.EmpMngr FROM EMPLOYEE e1 WHERE e1.EmpMngr IS NOT NULL))dt ORDER BY dt.FAMILY");
		}

		// Token: 0x06000118 RID: 280 RVA: 0x00008834 File Offset: 0x00006A34
		public DataTable GetDataForSignature()
		{
			return base.ExecuteDataTable(CommandType.Text, "SELECT e.EMP_NO, e.NAME, e.FAMILY,e.SignatureImg FROM EMPLOYEE e WHERE e.IsCut=0 AND e.END_DATE>=GETDATE() ORDER BY e.EMP_NO");
		}

		// Token: 0x06000119 RID: 281 RVA: 0x00008844 File Offset: 0x00006A44
		public UserActiveRow GetDataByEmpNoForLogin(double empNo)
		{
			UserActiveRow userActiveRow = null;
			using (SqlDataReader sqlDataReader = base.ExecuteReader("WF_EMPLOYEEGetDataByEmpNoForLogin", new object[]
			{
				empNo
			}))
			{
				if (sqlDataReader.Read())
				{
					userActiveRow = new UserActiveRow();
					userActiveRow.Section = Convert.ToString(sqlDataReader["TITLE"]);
					userActiveRow.EmpNo = Convert.ToDouble((sqlDataReader["EMP_NO"] is DBNull || sqlDataReader["EMP_NO"] == null) ? 0 : sqlDataReader["EMP_NO"]);
					userActiveRow.SecNo = Convert.ToInt32((sqlDataReader["SEC_NO"] is DBNull || sqlDataReader["SEC_NO"] == null) ? 0 : sqlDataReader["SEC_NO"]);
					userActiveRow.IsSupervisor = Convert.ToBoolean(sqlDataReader["Supervisor"]);
					userActiveRow.IsManager = Convert.ToBoolean(sqlDataReader["Manager"]);
					userActiveRow.IsConferment = Convert.ToBoolean(sqlDataReader["IsConferment"]);
					userActiveRow.IsSubstitute = Convert.ToBoolean(sqlDataReader["Substitute"]);
					userActiveRow.IsEmpMngr = Convert.ToBoolean(sqlDataReader["IsEmpMngr"]);
					userActiveRow.Name = Convert.ToString(sqlDataReader["NAME"]);
					userActiveRow.Family = Convert.ToString(sqlDataReader["FAMILY"]);
					userActiveRow.GrpNo = Convert.ToInt16((sqlDataReader["GRP_NO"] is DBNull || sqlDataReader["GRP_NO"] == null) ? 0 : sqlDataReader["GRP_NO"]);
					userActiveRow.EndDate = ((sqlDataReader["END_DATE"] is DBNull || sqlDataReader["END_DATE"] == null) ? DateTime.Now : Convert.ToDateTime(sqlDataReader["END_DATE"]));
					userActiveRow.IsUniq = Convert.ToBoolean(sqlDataReader["IS_UNIQ"]);
					userActiveRow.IsWarden = Convert.ToBoolean((sqlDataReader["IsWarden"] is DBNull || sqlDataReader["IsWarden"] == null) ? false : sqlDataReader["IsWarden"]);
					userActiveRow.WardenDayCount = Convert.ToInt32((sqlDataReader["WardenDayCount"] is DBNull || sqlDataReader["WardenDayCount"] == null) ? 0 : sqlDataReader["WardenDayCount"]);
					userActiveRow.WardenJustInShift = Convert.ToBoolean((sqlDataReader["WardenJustInShift"] is DBNull || sqlDataReader["WardenJustInShift"] == null) ? false : sqlDataReader["WardenJustInShift"]);
					userActiveRow.WardenIsShowRequest = new bool?(Convert.ToBoolean((sqlDataReader["WardenIsShowRequest"] is DBNull || sqlDataReader["WardenJustInShift"] == null) ? false : sqlDataReader["WardenIsShowRequest"]));
					userActiveRow.WardenIsShowReview = new bool?(Convert.ToBoolean((sqlDataReader["WardenIsShowReview"] is DBNull || sqlDataReader["WardenIsShowReview"] == null) ? false : sqlDataReader["WardenIsShowReview"]));
					userActiveRow.EmpImg = ((sqlDataReader["Img"] is DBNull) ? null : ((byte[])sqlDataReader["Img"]));
					userActiveRow.IsPrintViewer = Convert.ToBoolean((sqlDataReader["IsPrintViewer"] is DBNull || sqlDataReader["IsPrintViewer"] == null) ? false : sqlDataReader["IsPrintViewer"]);
					userActiveRow.IsParallelApprove = Convert.ToBoolean(sqlDataReader["IsParallelApprove"]);
					userActiveRow.SysActive = Convert.ToBoolean((sqlDataReader["SYS_ACTIVE"] is DBNull || sqlDataReader["SYS_ACTIVE"] == null) ? false : sqlDataReader["SYS_ACTIVE"]);
					userActiveRow.IsCut = Convert.ToBoolean((sqlDataReader["IsCut"] is DBNull || sqlDataReader["IsCut"] == null) ? false : sqlDataReader["IsCut"]);
					userActiveRow.IsParallelSectionKartabl = Convert.ToBoolean(sqlDataReader["IsParallelSectionKartabl"]);
				}
			}
			return userActiveRow;
		}

		// Token: 0x0600011A RID: 282 RVA: 0x00008CAC File Offset: 0x00006EAC
		public UserActiveRow GetByIdAndPassword(string WFUserName, string WFUserPass)
		{
			WFUserPass = UtilityManagers.EncryptPassword(WFUserPass, WFUserName, 12);
			UserActiveRow userActiveRow = null;
			using (SqlDataReader sqlDataReader = base.ExecuteReader("WF_EMPLOYEEGetDataByIdAndPassword", new object[]
			{
				WFUserName,
				WFUserPass
			}))
			{
				if (sqlDataReader.Read())
				{
					userActiveRow = new UserActiveRow();
					userActiveRow.Section = Convert.ToString(sqlDataReader["TITLE"]);
					userActiveRow.EmpNo = Convert.ToDouble((sqlDataReader["EMP_NO"] is DBNull || sqlDataReader["EMP_NO"] == null) ? 0 : sqlDataReader["EMP_NO"]);
					userActiveRow.SecNo = Convert.ToInt32((sqlDataReader["SEC_NO"] is DBNull || sqlDataReader["SEC_NO"] == null) ? 0 : sqlDataReader["SEC_NO"]);
					userActiveRow.IsSupervisor = Convert.ToBoolean(sqlDataReader["Supervisor"]);
					userActiveRow.IsManager = Convert.ToBoolean(sqlDataReader["Manager"]);
					userActiveRow.IsConferment = Convert.ToBoolean(sqlDataReader["IsConferment"]);
					userActiveRow.IsSubstitute = Convert.ToBoolean(sqlDataReader["Substitute"]);
					userActiveRow.IsEmpMngr = Convert.ToBoolean(sqlDataReader["IsEmpMngr"]);
					userActiveRow.Name = Convert.ToString(sqlDataReader["NAME"]);
					userActiveRow.Family = Convert.ToString(sqlDataReader["FAMILY"]);
					userActiveRow.GrpNo = Convert.ToInt16((sqlDataReader["GRP_NO"] is DBNull || sqlDataReader["GRP_NO"] == null) ? 0 : sqlDataReader["GRP_NO"]);
					userActiveRow.EndDate = ((sqlDataReader["END_DATE"] is DBNull || sqlDataReader["END_DATE"] == null) ? DateTime.Now : Convert.ToDateTime(sqlDataReader["END_DATE"]));
					userActiveRow.IsUniq = Convert.ToBoolean(sqlDataReader["IS_UNIQ"]);
					userActiveRow.IsWarden = Convert.ToBoolean((sqlDataReader["IsWarden"] is DBNull || sqlDataReader["IsWarden"] == null) ? false : sqlDataReader["IsWarden"]);
					userActiveRow.WardenDayCount = Convert.ToInt32((sqlDataReader["WardenDayCount"] is DBNull || sqlDataReader["WardenDayCount"] == null) ? 0 : sqlDataReader["WardenDayCount"]);
					userActiveRow.WardenJustInShift = Convert.ToBoolean((sqlDataReader["WardenJustInShift"] is DBNull || sqlDataReader["WardenJustInShift"] == null) ? false : sqlDataReader["WardenJustInShift"]);
					userActiveRow.WardenIsShowRequest = new bool?(Convert.ToBoolean((sqlDataReader["WardenIsShowRequest"] is DBNull || sqlDataReader["WardenJustInShift"] == null) ? false : sqlDataReader["WardenIsShowRequest"]));
					userActiveRow.WardenIsShowReview = new bool?(Convert.ToBoolean((sqlDataReader["WardenIsShowReview"] is DBNull || sqlDataReader["WardenIsShowReview"] == null) ? false : sqlDataReader["WardenIsShowReview"]));
					userActiveRow.EmpImg = ((sqlDataReader["Img"] is DBNull) ? null : ((byte[])sqlDataReader["Img"]));
					userActiveRow.IsParallelApprove = Convert.ToBoolean(sqlDataReader["IsParallelApprove"]);
					userActiveRow.IsPrintViewer = Convert.ToBoolean((sqlDataReader["IsPrintViewer"] is DBNull || sqlDataReader["IsPrintViewer"] == null) ? false : sqlDataReader["IsPrintViewer"]);
					userActiveRow.SysActive = Convert.ToBoolean((sqlDataReader["SYS_ACTIVE"] is DBNull || sqlDataReader["SYS_ACTIVE"] == null) ? false : sqlDataReader["SYS_ACTIVE"]);
					userActiveRow.IsCut = Convert.ToBoolean((sqlDataReader["IsCut"] is DBNull || sqlDataReader["IsCut"] == null) ? false : sqlDataReader["IsCut"]);
					userActiveRow.IsParallelSectionKartabl = Convert.ToBoolean(sqlDataReader["IsParallelSectionKartabl"]);
				}
			}
			return userActiveRow;
		}

		// Token: 0x0600011B RID: 283 RVA: 0x0000911C File Offset: 0x0000731C
		public UserActiveRow GetPwkaraUsers(string usersId, string usersPassword)
		{
			usersPassword = UtilityManagers.EncryptPassword(usersPassword, usersId, 12);
			UserActiveRow result = null;
			using (SqlDataReader sqlDataReader = base.ExecuteReader("WF_GetPwkaraUsers", new object[]
			{
				usersId,
				usersPassword
			}))
			{
				if (sqlDataReader.Read())
				{
					result = new UserActiveRow
					{
						Section = Convert.ToString(sqlDataReader["TITLE"]),
						EmpNo = Convert.ToDouble((sqlDataReader["EMP_NO"] is DBNull || sqlDataReader["EMP_NO"] == null) ? 0 : sqlDataReader["EMP_NO"]),
						SecNo = Convert.ToInt32((sqlDataReader["SEC_NO"] is DBNull || sqlDataReader["SEC_NO"] == null) ? 0 : sqlDataReader["SEC_NO"]),
						IsSupervisor = Convert.ToBoolean(sqlDataReader["Supervisor"]),
						IsManager = Convert.ToBoolean(sqlDataReader["Manager"]),
						IsSubstitute = Convert.ToBoolean(sqlDataReader["Substitute"]),
						IsEmpMngr = Convert.ToBoolean(sqlDataReader["IsEmpMngr"]),
						Name = Convert.ToString(sqlDataReader["NAME"]),
						Family = Convert.ToString(sqlDataReader["FAMILY"]),
						GrpNo = Convert.ToInt16((sqlDataReader["GRP_NO"] is DBNull || sqlDataReader["GRP_NO"] == null) ? 0 : sqlDataReader["GRP_NO"]),
						EndDate = ((sqlDataReader["END_DATE"] is DBNull || sqlDataReader["END_DATE"] == null) ? DateTime.Now : Convert.ToDateTime(sqlDataReader["END_DATE"])),
						IsUniq = Convert.ToBoolean(sqlDataReader["IS_UNIQ"]),
						IsWarden = Convert.ToBoolean((sqlDataReader["IsWarden"] is DBNull || sqlDataReader["IsWarden"] == null) ? false : sqlDataReader["IsWarden"]),
						WardenDayCount = Convert.ToInt32((sqlDataReader["WardenDayCount"] is DBNull || sqlDataReader["WardenDayCount"] == null) ? 0 : sqlDataReader["WardenDayCount"]),
						WardenJustInShift = Convert.ToBoolean((sqlDataReader["WardenJustInShift"] is DBNull || sqlDataReader["WardenJustInShift"] == null) ? false : sqlDataReader["WardenJustInShift"]),
						WardenIsShowRequest = new bool?(Convert.ToBoolean((sqlDataReader["WardenIsShowRequest"] is DBNull || sqlDataReader["WardenJustInShift"] == null) ? false : sqlDataReader["WardenIsShowRequest"])),
						WardenIsShowReview = new bool?(Convert.ToBoolean((sqlDataReader["WardenIsShowReview"] is DBNull || sqlDataReader["WardenIsShowReview"] == null) ? false : sqlDataReader["WardenIsShowReview"])),
						EmpImg = ((sqlDataReader["Img"] is DBNull) ? null : ((byte[])sqlDataReader["Img"])),
						IsParallelApprove = Convert.ToBoolean(sqlDataReader["IsParallelApprove"]),
						IsPrintViewer = Convert.ToBoolean((sqlDataReader["IsPrintViewer"] is DBNull || sqlDataReader["IsPrintViewer"] == null) ? false : sqlDataReader["IsPrintViewer"]),
						SysActive = Convert.ToBoolean((sqlDataReader["SYS_ACTIVE"] is DBNull || sqlDataReader["SYS_ACTIVE"] == null) ? false : sqlDataReader["SYS_ACTIVE"]),
						IsCut = Convert.ToBoolean((sqlDataReader["IsCut"] is DBNull || sqlDataReader["IsCut"] == null) ? false : sqlDataReader["IsCut"]),
						IsParallelSectionKartabl = Convert.ToBoolean(sqlDataReader["IsParallelSectionKartabl"])
					};
				}
			}
			return result;
		}

		// Token: 0x0600011C RID: 284 RVA: 0x00009578 File Offset: 0x00007778
		public DataTable GetAccessManager(double empNo, int actionId)
		{
			return base.ExecuteDataTable("WF_GetAccessManager", new object[]
			{
				empNo,
				actionId
			});
		}

		// Token: 0x0600011D RID: 285 RVA: 0x0000959D File Offset: 0x0000779D
		public int UpdateSignatureImg(double empNo, byte[] signatureImg)
		{
			return base.ExecuteNonQuery("WF_UpdateSignatureImg", new object[]
			{
				empNo,
				signatureImg
			});
		}

		// Token: 0x0600011E RID: 286 RVA: 0x000095C0 File Offset: 0x000077C0
		public int UpdateUserData(double empNo)
		{
			string text = empNo.ToString(CultureInfo.InvariantCulture);
			string text2 = UtilityManagers.EncryptPassword(empNo.ToString(CultureInfo.InvariantCulture), empNo.ToString(CultureInfo.InvariantCulture), 12);
			return base.ExecuteNonQuery("WF_EMPLOYEEUpdateEmployeeUserData", new object[]
			{
				empNo,
				text,
				text2
			});
		}

		// Token: 0x0600011F RID: 287 RVA: 0x0000961C File Offset: 0x0000781C
		public int UpdateUserData(double empNo, double persNo)
		{
			string text = persNo.ToString(CultureInfo.InvariantCulture);
			string text2 = UtilityManagers.EncryptPassword(persNo.ToString(CultureInfo.InvariantCulture), persNo.ToString(CultureInfo.InvariantCulture), 12);
			return base.ExecuteNonQuery("WF_EMPLOYEEUpdateEmployeeUserData", new object[]
			{
				empNo,
				text,
				text2
			});
		}

		// Token: 0x06000120 RID: 288 RVA: 0x00009678 File Offset: 0x00007878
		public int RemoveUsernameAndPassword()
		{
			return base.ExecuteNonQuery(CommandType.Text, "UPDATE EMPLOYEE SET WFUserName = NULL,WFUserPass = NULL");
		}

		// Token: 0x06000121 RID: 289 RVA: 0x00009686 File Offset: 0x00007886
		public int SetAllPasword()
		{
			return base.ExecuteNonQuery("WF_EMPLOYEESetPasword", null);
		}

		// Token: 0x06000122 RID: 290 RVA: 0x00009694 File Offset: 0x00007894
		public bool ChangeLogin(double empNo, string userCode, string pass)
		{
			pass = UtilityManagers.EncryptPassword(pass, userCode, 12);
			return base.ExecuteNonQuery("WF_EMPLOYEE_ChangeLogin", new object[]
			{
				empNo,
				userCode,
				pass
			}) >= 0;
		}

		// Token: 0x06000123 RID: 291 RVA: 0x000096C9 File Offset: 0x000078C9
		public int SetIskarmand(double ifEmpNo, short isKarmand)
		{
			return base.ExecuteNonQuery("WF_EMPLOYEE_SetISKARMAND", new object[]
			{
				ifEmpNo,
				isKarmand
			});
		}

		// Token: 0x06000124 RID: 292 RVA: 0x000096EE File Offset: 0x000078EE
		public int SetSEmp(double empNo, double sEmpNo, string sFDate, string sTDate)
		{
			return base.ExecuteNonQuery("WF_EMPLOYEESetSEmp", new object[]
			{
				empNo,
				sEmpNo,
				sFDate,
				sTDate
			});
		}

		// Token: 0x06000125 RID: 293 RVA: 0x0000971C File Offset: 0x0000791C
		public int SetWarden(double ifEmpNo, bool isWarden)
		{
			return base.ExecuteNonQuery(CommandType.Text, string.Concat(new object[]
			{
				"UPDATE EMPLOYEE SET IsWarden = ",
				Convert.ToByte(isWarden),
				" WHERE EMP_NO=",
				ifEmpNo
			}));
		}

		// Token: 0x06000126 RID: 294 RVA: 0x00009757 File Offset: 0x00007957
		public int SetPrintViewer(double ifEmpNo, bool isPrintViewer)
		{
			return base.ExecuteNonQuery(CommandType.Text, string.Concat(new object[]
			{
				"UPDATE EMPLOYEE SET IsPrintViewer = ",
				Convert.ToByte(isPrintViewer),
				" WHERE EMP_NO=",
				ifEmpNo
			}));
		}

		// Token: 0x06000127 RID: 295 RVA: 0x00009794 File Offset: 0x00007994
		public int UpdateEmpMngr(double? empMngr, double empNo, int leave, int leaveOut, int duty)
		{
			return base.ExecuteNonQuery(CommandType.Text, string.Concat(new object[]
			{
				"UPDATE EMPLOYEE SET EmpMngr=",
				empMngr.HasValue ? empMngr.ToString() : "null",
				",LEAVE = ",
				(leave < 1) ? "null" : leave.ToString(CultureInfo.InvariantCulture),
				",LeaveOut = ",
				(leaveOut < 1) ? "null" : leaveOut.ToString(CultureInfo.InvariantCulture),
				",DUTY = ",
				(duty < 1) ? "null" : duty.ToString(CultureInfo.InvariantCulture),
				" WHERE EMP_NO=",
				empNo
			}));
		}

		// Token: 0x06000128 RID: 296 RVA: 0x00009858 File Offset: 0x00007A58
		public int UpdateAllEmpMngr(double? empMngr, bool isReset, int leave, int leaveOut, int duty)
		{
			string text = "UPDATE EMPLOYEE SET EmpMngr=" + (isReset ? "null" : empMngr.ToString());
			if (isReset)
			{
				text += ",LEAVE = null,LeaveOut = null,DUTY = null";
			}
			else
			{
				text = string.Concat(new object[]
				{
					text,
					",LEAVE = ",
					leave,
					",LeaveOut = ",
					leaveOut,
					",DUTY = ",
					duty
				});
			}
			text = string.Concat(new object[]
			{
				text,
				"\n WHERE EMP_NO IN (SELECT e.EMP_NO FROM EMPLOYEE e  WHERE e.EmpMngr IS NULL OR e.EmpMngr=",
				empMngr,
				" )"
			});
			return base.ExecuteNonQuery(CommandType.Text, text);
		}

		// Token: 0x06000129 RID: 297 RVA: 0x00009910 File Offset: 0x00007B10
		public int Update_AccessTimeGroupID(int? accessTimeGroupId, double empNo)
		{
			if (!accessTimeGroupId.HasValue)
			{
				return base.ExecuteNonQuery(CommandType.Text, "UPDATE EMPLOYEE SET AccessTimeGroupID =null WHERE EMP_NO=" + empNo);
			}
			return base.ExecuteNonQuery(CommandType.Text, string.Concat(new object[]
			{
				"UPDATE EMPLOYEE SET AccessTimeGroupID =",
				accessTimeGroupId,
				" WHERE EMP_NO=",
				empNo
			}));
		}

		// Token: 0x0600012A RID: 298 RVA: 0x00009972 File Offset: 0x00007B72
		public int Update_AccessTimeGroupID(int? accessTimeGroupId)
		{
			if (!accessTimeGroupId.HasValue)
			{
				return base.ExecuteNonQuery(CommandType.Text, "UPDATE EMPLOYEE SET AccessTimeGroupID =null");
			}
			return base.ExecuteNonQuery(CommandType.Text, "UPDATE EMPLOYEE SET AccessTimeGroupID =" + accessTimeGroupId);
		}

		// Token: 0x0600012B RID: 299 RVA: 0x000099A4 File Offset: 0x00007BA4
		public DataTable SelectByAccessTimeGroupId(int accessTimeGroupId, bool free)
		{
			string commandText;
			if (free)
			{
				commandText = "SELECT e.EMP_NO,e.[NAME],e.FAMILY,e.SEC_NO,s.TITLE,wtg.GroupName \n  FROM EMPLOYEE e \nJOIN SECTIONS s ON  s.SEC_NO = e.SEC_NO \nLEFT JOIN WFAccessTimeGroup wtg ON e.AccessTimeGroupID=wtg.AccessTimeGroupID \nWHERE  (e.IsCut = 0) AND (e.END_DATE >= GETDATE()) AND (e.AccessTimeGroupID IS NULL OR e.AccessTimeGroupID <> " + accessTimeGroupId + ")";
			}
			else
			{
				commandText = "SELECT e.EMP_NO,e.[NAME],e.FAMILY,e.SEC_NO,s.TITLE,wtg.GroupName \n  FROM EMPLOYEE e \nJOIN SECTIONS s ON  s.SEC_NO = e.SEC_NO \nLEFT JOIN WFAccessTimeGroup wtg ON e.AccessTimeGroupID=wtg.AccessTimeGroupID \nWHERE  (e.IsCut = 0) AND (e.END_DATE >= GETDATE()) AND e.AccessTimeGroupID = " + accessTimeGroupId;
			}
			return base.ExecuteDataTable(CommandType.Text, commandText);
		}

		// Token: 0x0600012C RID: 300 RVA: 0x000099E8 File Offset: 0x00007BE8
		internal void UpdateKVCurrentMonth(double empNo, double sDate, double eDate)
		{
			ParmfileRow dataRow = WorkFlowsDatabase.Instance.Parmfile.GetDataRow();
			DateTime now = DateTime.Now;
			DateTime dateTime;
			DateTime dateTime2;
			if (ShDate.DayOf(now) < dataRow.StartDay)
			{
				dateTime = ShDate.StrToDate(string.Format("{0}/{1}/{2}", UtilityManagers.ConvertToFourDigitNumber(ShDate.YearOf(now).ToString()), UtilityManagers.ConvertToTowDigitNumber((int)(ShDate.MonthOf(now) - 1)), UtilityManagers.ConvertToTowDigitNumber((int)dataRow.StartDay)));
				dateTime2 = ShDate.StrToDate(string.Format("{0}/{1}/{2}", UtilityManagers.ConvertToFourDigitNumber(ShDate.YearOf(now).ToString()), UtilityManagers.ConvertToTowDigitNumber((int)ShDate.MonthOf(now)), UtilityManagers.ConvertToTowDigitNumber((int)dataRow.StartDay))).AddDays(-1.0);
			}
			else
			{
				dateTime = ShDate.StrToDate(string.Format("{0}/{1}/{2}", UtilityManagers.ConvertToFourDigitNumber(ShDate.YearOf(now).ToString()), UtilityManagers.ConvertToTowDigitNumber((int)ShDate.MonthOf(now)), UtilityManagers.ConvertToTowDigitNumber((int)dataRow.StartDay)));
				dateTime2 = ShDate.StrToDate(string.Format("{0}/{1}/{2}", UtilityManagers.ConvertToFourDigitNumber(ShDate.YearOf(now).ToString()), UtilityManagers.ConvertToTowDigitNumber((int)(ShDate.MonthOf(now) + 1)), UtilityManagers.ConvertToTowDigitNumber((int)dataRow.StartDay))).AddDays(-1.0);
			}
			if (Math.Max(dateTime.ToOADate(), sDate) - Math.Min(dateTime2.ToOADate(), eDate) >= 0.0)
			{
				base.ExecuteNonQuery(CommandType.Text, "UPDATE EMPLOYEE SET KVCurrentMonth = 0 WHERE EMP_NO=" + empNo);
			}
		}
	}
}
