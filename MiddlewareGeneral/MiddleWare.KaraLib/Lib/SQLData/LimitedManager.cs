﻿using System;
using System.Data;
using MiddleWare.KaraLib.ViewModels;


namespace PW.WorkFlows.SqlDal
{
	// Token: 0x0200001D RID: 29
	public class LimitedManager : BaseDAL
	{
		// Token: 0x0600014C RID: 332 RVA: 0x00003C40 File Offset: 0x00001E40
		public LimitedManager(WorkFlowsDatabase owner) : base(owner)
		{
		}

		// Token: 0x0600014D RID: 333 RVA: 0x0000A79D File Offset: 0x0000899D
		public DataTable GetData()
		{
			return base.ExecuteDataTable(CommandType.Text, "SELECT * FROM LimitedManager la");
		}

		// Token: 0x0600014E RID: 334 RVA: 0x0000A7AC File Offset: 0x000089AC
		public DataTable GetData(double managerEmpNo)
		{
			string commandText = "SELECT la.*,a.Fdesc FROM LimitedManager la  \nJOIN [Action] a ON a.ActionID = la.ActionID \nWHERE la.Manager_EmpNO=" + managerEmpNo;
			return base.ExecuteDataTable(CommandType.Text, commandText);
		}

		// Token: 0x0600014F RID: 335 RVA: 0x0000A7D2 File Offset: 0x000089D2
		public LimitedManagerRow GetData(double managerEmpNo, int secNo)
		{
			return new LimitedManagerRow(base.ExecuteDataTable("WF_GetLimitManager", new object[]
			{
				managerEmpNo,
				secNo
			}));
		}

		// Token: 0x06000150 RID: 336 RVA: 0x0000A7FC File Offset: 0x000089FC
		public DataTable GetFreeData(double managerEmpNo, int secNo)
		{
			string commandText = string.Concat(new object[]
			{
				"SELECT la.*,a.Fdesc FROM LimitedManager la  \nJOIN [Action] a ON a.ActionID = la.ActionID \nWHERE la.Manager_EmpNO=",
				managerEmpNo,
				" AND la.Sec_No=",
				secNo
			});
			return base.ExecuteDataTable(CommandType.Text, commandText);
		}

		// Token: 0x06000151 RID: 337 RVA: 0x0000A840 File Offset: 0x00008A40
		public bool Insert(double managerEmpNo, int secNo, int actionId, object limitDuration, object limitDurationPerMonth)
		{
			bool result;
			try
			{
				string commandText = string.Concat(new object[]
				{
					"INSERT INTO LimitedManager(Manager_EmpNO,Sec_No,ActionID,LimitDuration,LimitDurationPerMonth) \nVALUES(",
					managerEmpNo,
					",",
					secNo,
					",",
					actionId,
					",",
					Convert.ToInt32(limitDuration),
					",",
					Convert.ToInt32(limitDurationPerMonth),
					")"
				});
				result = (base.ExecuteNonQuery(CommandType.Text, commandText) > 0);
			}
			catch
			{
				result = false;
			}
			return result;
		}

		// Token: 0x06000152 RID: 338 RVA: 0x0000A8EC File Offset: 0x00008AEC
		public bool Update(double managerEmpNo, int actionId, string limitDuration)
		{
			bool result;
			try
			{
				string commandText = string.Concat(new object[]
				{
					"UPDATE LimitedManager SET\tLimitDuration = '",
					limitDuration,
					"' \nWHERE Manager_EmpNO=",
					managerEmpNo,
					" AND ActionID=",
					actionId
				});
				result = (base.ExecuteNonQuery(CommandType.Text, commandText) > 0);
			}
			catch
			{
				result = false;
			}
			return result;
		}

		// Token: 0x06000153 RID: 339 RVA: 0x0000A958 File Offset: 0x00008B58
		public bool Update(double managerEmpNo, int secNo, int actionId, object limitDuration, object LimitDurationPerMonth)
		{
			bool result;
			try
			{
				string commandText = string.Concat(new object[]
				{
					"UPDATE LimitedManager SET LimitDuration = ",
					Convert.ToInt32(limitDuration),
					",LimitDurationPerMonth = ",
					Convert.ToInt32(LimitDurationPerMonth),
					" \nWHERE Manager_EmpNO=",
					managerEmpNo,
					" AND ActionID=",
					actionId,
					" AND Sec_No=",
					secNo
				});
				result = (base.ExecuteNonQuery(CommandType.Text, commandText) > 0);
			}
			catch
			{
				result = false;
			}
			return result;
		}
	}
}
