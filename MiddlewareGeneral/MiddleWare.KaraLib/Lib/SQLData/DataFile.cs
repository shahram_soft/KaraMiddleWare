﻿using System;
using System.Data;
using System.Data.SqlClient;
using MiddleWare.KaraLib;


namespace PW.WorkFlows.SqlDal
{
	// Token: 0x02000016 RID: 22
	public class DataFile : BaseDAL
	{
		// Token: 0x060000F8 RID: 248 RVA: 0x00003C40 File Offset: 0x00001E40
		public DataFile(WorkFlowsDatabase owner) : base(owner)
		{
		}

		// Token: 0x060000F9 RID: 249 RVA: 0x00007810 File Offset: 0x00005A10
		public DataTable GetData()
		{
			return base.ExecuteDataTable("WF_DataFileGetData", null);
		}

		// Token: 0x060000FA RID: 250 RVA: 0x0000781E File Offset: 0x00005A1E
		public DataTable GetDataByDateAndAroundTime(double empno, double date, short startTime, short endTime)
		{
			return base.ExecuteDataTable("WF_DataFileGetDataByDateAndAroundTime", new object[]
			{
				empno,
				date,
				startTime,
				endTime
			});
		}

		// Token: 0x060000FB RID: 251 RVA: 0x00007856 File Offset: 0x00005A56
		public DataRow FindNearDataFile(double empNo, double date, int startTime, int endTime)
		{
			return base.ExecuteDataRow("WF_FindNearDataFile", new object[]
			{
				empNo,
				date,
				startTime,
				endTime
			});
		}

		// Token: 0x060000FC RID: 252 RVA: 0x0000788E File Offset: 0x00005A8E
		public DataTable GetDataByDateAndTime(double empNo, double date, short time)
		{
			return base.ExecuteDataTable("WF_DataFileGetDataByDateAndTime", new object[]
			{
				empNo,
				date,
				time
			});
		}

		// Token: 0x060000FD RID: 253 RVA: 0x000078BC File Offset: 0x00005ABC
		public DataTable GetDataByEmpNo(double empNo, double date)
		{
			return base.ExecuteDataTable("WF_DataFileGetDataByEmpNO", new object[]
			{
				empNo,
				date
			});
		}

		// Token: 0x060000FE RID: 254 RVA: 0x000078E4 File Offset: 0x00005AE4
		private bool Update(double date_, object time_, double empNo, short type_)
		{
			if (base.ExecuteNonQuery("WF_DataFileUpdate", new object[]
			{
				date_,
				time_,
				empNo,
				type_,
				true
			}) > 0)
			{
				WorkFlowsDatabase.Instance.Employee.UpdateKVCurrentMonth(empNo, date_, date_);
				return true;
			}
			return false;
		}

		// Token: 0x060000FF RID: 255 RVA: 0x00007944 File Offset: 0x00005B44
		public bool UpdateTimeStatus(double startDate, short startHour, short endTime, double empNo, ref short type, ref DataRow drTaradod, string duration)
		{
			int time;
			int.TryParse(duration.Replace("_", ""), out time);
			endTime = ((endTime > startHour) ? endTime : ((short)ShDate.TimePlus((int)startHour, time)));
			drTaradod = this.FindNearDataFile(empNo, startDate, (int)startHour, (int)endTime);
			if (drTaradod == null)
			{
				type = -1;
				return false;
			}
			return this.Update(startDate, drTaradod["Time_"], empNo, type);
		}

		// Token: 0x06000100 RID: 256 RVA: 0x000079B0 File Offset: 0x00005BB0
		public bool UpdateTimeStatus(double startDate, short startHour, short endTime, double empNo, string duration)
		{
			int time;
			int.TryParse(duration.Replace("_", ""), out time);
			endTime = ((endTime > startHour) ? endTime : ((short)ShDate.TimePlus((int)startHour, time)));
			DataRow dataRow = this.FindNearDataFile(empNo, startDate, (int)startHour, (int)endTime);
			return dataRow != null && (dataRow["Status"].ToString() == "0" || this.Update(startDate, dataRow["Time_"], empNo, 0));
		}

		// Token: 0x06000101 RID: 257 RVA: 0x00007A2B File Offset: 0x00005C2B
		public bool UpdateTimeStatus(double startDate, short time, double empNo, short type)
		{
			return this.Update(startDate, time, empNo, type);
		}

		// Token: 0x06000102 RID: 258 RVA: 0x00007A40 File Offset: 0x00005C40
		public bool UpdateByModify(double date, short oldTime, short newTime, double empNo, short status, bool modify)
		{
			bool result;
			try
			{
				if (base.ExecuteNonQuery("WF_DataFileUpdateByModify", new object[]
				{
					date,
					oldTime,
					newTime,
					empNo,
					status,
					modify
				}) > 0)
				{
					WorkFlowsDatabase.Instance.Employee.UpdateKVCurrentMonth(empNo, date, date);
					result = true;
				}
				else
				{
					result = false;
				}
			}
			catch (Exception innerException)
			{
				while (innerException != null && !(innerException is SqlException))
				{
					innerException = innerException.InnerException;
				}
				if (innerException != null)
				{
					SqlException ex = innerException as SqlException;
					if (ex != null)
					{
						int number = ex.Number;
						if (number == 2601 || number == 2627)
						{
							result = true;
							return result;
						}
						result = false;
						return result;
					}
				}
				result = false;
			}
			return result;
		}

		// Token: 0x06000103 RID: 259 RVA: 0x00007B0C File Offset: 0x00005D0C
		public bool Insert(short status, double date, short time, double empNo, bool modify, short clockNo)
		{
			bool result;
			try
			{
				string text = UtilityManagers.ConvertToFourDigitNumber(time.ToString());
				if (text.Substring(2, 2) == "60")
				{
					time = Convert.ToInt16(Convert.ToInt32(text.Substring(0, 2)) + 1 + "00");
				}
				if (base.ExecuteNonQuery("WF_DataFileInsert", new object[]
				{
					status,
					date,
					time,
					empNo,
					modify,
					clockNo
				}) > -1)
				{
					WorkFlowsDatabase.Instance.Employee.UpdateKVCurrentMonth(empNo, date, date);
					result = true;
				}
				else
				{
					result = false;
				}
			}
			catch (Exception innerException)
			{
				while (innerException != null && !(innerException is SqlException))
				{
					innerException = innerException.InnerException;
				}
				if (innerException != null)
				{
					SqlException ex = innerException as SqlException;
					if (ex != null)
					{
						int number = ex.Number;
						if (number == 2601 || number == 2627)
						{
							result = true;
							return result;
						}
						result = false;
						return result;
					}
				}
				result = false;
			}
			return result;
		}

		// Token: 0x06000104 RID: 260 RVA: 0x00007C20 File Offset: 0x00005E20
		public bool Delete(double date, short time, double empNo)
		{
			if (base.ExecuteNonQuery("WF_DataFileDelete", new object[]
			{
				date,
				time,
				empNo
			}) > 0)
			{
				WorkFlowsDatabase.Instance.Employee.UpdateKVCurrentMonth(empNo, date, date);
				return true;
			}
			return false;
		}

		// Token: 0x06000105 RID: 261 RVA: 0x00007C74 File Offset: 0x00005E74
		public DataTable GetDataByEmpNo(double empNo, double fromDate, short grpNo, double toDate, ref int MaxCol)
		{
			DataSet dataSet = base.ExecuteDataset("WF_ShowDataFile", new object[]
			{
				fromDate,
				toDate,
				empNo
			});
			MaxCol = Convert.ToInt32(dataSet.Tables[2].Rows[0]["MaxCol"]);
			if (MaxCol < 12)
			{
				MaxCol = 12;
			}
			DataTable dataTable = new DataTable("TblDataFile");
			dataTable.Columns.Add("Emp_NO", typeof(double));
			dataTable.Columns.Add("DayName", typeof(string));
			dataTable.Columns.Add("ShamsiDate", typeof(string));
			dataTable.Columns.Add("ShamsiDayNo", typeof(string));
			dataTable.Columns.Add("DayStatus", typeof(string));
			dataTable.Columns.Add("CARD_TYPE", typeof(int));
			for (int i = 1; i <= MaxCol; i++)
			{
				dataTable.Columns.Add("CARD_TYPE" + i, typeof(int));
				dataTable.Columns.Add("DF" + i, typeof(string));
				dataTable.Columns.Add("CardTitle" + i, typeof(string));
			}
			dataTable.Columns.Add("DayType", typeof(int));
			int num = 0;
			for (double num2 = fromDate; num2 <= toDate; num2 += 1.0)
			{
				DataRow dataRow = dataTable.NewRow();
				dataRow["Emp_NO"] = empNo;
				string text = ShDate.DateToStr(DateTime.FromOADate(num2), true);
				dataRow["ShamsiDate"] = text;
				dataRow["ShamsiDayNo"] = text.Remove(0, 2);
				WorkFlowsDatabase.Instance.Cards.FindGrpsByEmpAndDate(empNo, DateTime.FromOADate(num2), grpNo, ref num);
				dataRow["DayType"] = num;
				dataRow["DayName"] = ShDate.DayName(ShDate.DayOfWeek(DateTime.FromOADate(num2)));
				try
				{
					if (dataSet.Tables[1] != null && dataSet.Tables[1].Rows.Count > 0)
					{
						DataRow[] array = dataSet.Tables[1].Select(string.Concat(new object[]
						{
							"S_DATE <= ",
							num2,
							" AND E_DATE >= ",
							num2
						}));
						if (array.Length != 0)
						{
							dataRow["DayStatus"] = array[0]["TITLE"];
							dataRow["CARD_TYPE"] = array[0]["CARD_TYPE"];
						}
					}
					else
					{
						dataRow["DayStatus"] = "";
						dataRow["CARD_TYPE"] = -1;
					}
				}
				catch
				{
					dataRow["DayStatus"] = "";
					dataRow["CARD_TYPE"] = -1;
				}
				int num3 = 0;
				DataRow[] array2 = dataSet.Tables[0].Select("DATE_=" + num2);
				for (int j = 0; j < array2.Length; j++)
				{
					DataRow dataRow2 = array2[j];
					num3++;
					if (num3 > MaxCol)
					{
						break;
					}
					dataRow["CARD_TYPE" + num3] = dataRow2["CARD_TYPE"];
					dataRow["DF" + num3] = UtilityManagers.MakeTimeFormat(dataRow2["TIME_"].ToString());
					dataRow["CardTitle" + num3] = dataRow2["TITLE"];
				}
				dataTable.Rows.Add(dataRow);
			}
			return dataTable;
		}

		// Token: 0x06000106 RID: 262 RVA: 0x000080C0 File Offset: 0x000062C0
		public DataTable GetDataByDate(double date, string empNo, string secNo, ref int MaxCol)
		{
			MaxCol = 0;
			string commandText = string.Concat(new object[]
			{
				"SELECT DISTINCT df.[STATUS], df.DATE_, df.TIME_, df.EMP_NO,c.TITLE,\ne.[NAME],e.FAMILY,c.CARD_TYPE FROM DataFile df  \nJOIN CARDS c ON df.[STATUS]=c.CARD_NO \nJOIN EMPLOYEE e ON df.EMP_NO=e.EMP_NO \n    WHERE df.DATE_=",
				date,
				" \n",
				(!string.IsNullOrEmpty(secNo)) ? ("AND (e.SEC_NO IN(" + secNo + ")") : "(",
				" \n OR e.EMP_NO=",
				empNo,
				") \n ORDER BY df.EMP_NO,df.TIME_     \n\n /******************************************/ \nSELECT MAX(df.cnt) MaxCol FROM  \n(SELECT COUNT(1) cnt FROM DataFile df  \nJOIN EMPLOYEE e ON df.EMP_NO=e.EMP_NO \n    WHERE df.DATE_=",
				date,
				" \n",
				(!string.IsNullOrEmpty(secNo)) ? ("AND ( e.SEC_NO IN(" + secNo + ")") : "(",
				" \n OR e.EMP_NO=",
				empNo,
				") \n GROUP BY df.EMP_NO)df "
			});
			DataSet dataSet = base.ExecuteDataset(CommandType.Text, commandText);
			if (dataSet.Tables[1].Rows[0]["MaxCol"] is DBNull || dataSet.Tables[1].Rows[0]["MaxCol"] == null)
			{
				return new DataTable();
			}
			MaxCol = Convert.ToInt32(dataSet.Tables[1].Rows[0]["MaxCol"]);
			DataTable dataTable = new DataTable();
			dataTable.Columns.Add("EmpNo", typeof(double));
			dataTable.Columns.Add("EmployeeName", typeof(string));
			for (int i = 1; i <= MaxCol; i++)
			{
				dataTable.Columns.Add("DF" + i, typeof(string)).Caption = i.ToString();
				dataTable.Columns.Add("DFTitle" + i, typeof(string));
				dataTable.Columns.Add("DFStatus" + i, typeof(short));
				dataTable.Columns.Add("CardType" + i, typeof(short));
			}
			foreach (DataRow dataRow in dataSet.Tables[0].DefaultView.ToTable(true, new string[]
			{
				"EMP_NO",
				"NAME",
				"FAMILY"
			}).Rows)
			{
				int num = 1;
				DataRow dataRow2 = dataTable.NewRow();
				dataRow2["EmpNo"] = dataRow["EMP_NO"];
				dataRow2["EmployeeName"] = string.Format("{0} {1}", dataRow["NAME"].ToString().Trim(), dataRow["FAMILY"].ToString().Trim());
				DataRow[] array = dataSet.Tables[0].Select("EMP_NO=" + dataRow["EMP_NO"]);
				for (int j = 0; j < array.Length; j++)
				{
					DataRow dataRow3 = array[j];
					dataRow2["DF" + num] = UtilityManagers.MakeTimeFormat(dataRow3["TIME_"].ToString());
					dataRow2["DFTitle" + num] = dataRow3["TITLE"].ToString();
					dataRow2["DFStatus" + num] = dataRow3["STATUS"];
					dataRow2["CardType" + num] = dataRow3["CARD_TYPE"];
					num++;
				}
				dataTable.Rows.Add(dataRow2);
			}
			return dataTable;
		}

		// Token: 0x06000107 RID: 263 RVA: 0x000084E4 File Offset: 0x000066E4
		public DataSet DataFileManage(double empNo, double sdate, double edate)
		{
			return base.ExecuteDataset("WF_DataFileManage", new object[]
			{
				empNo,
				sdate,
				edate
			});
		}
	}
}
