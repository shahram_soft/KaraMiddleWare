﻿using System;
using System.Data;

namespace PW.WorkFlows.SqlDal
{
	// Token: 0x02000024 RID: 36
	public class Overs : BaseDAL
	{
		// Token: 0x06000174 RID: 372 RVA: 0x00003C40 File Offset: 0x00001E40
		public Overs(WorkFlowsDatabase owner) : base(owner)
		{
		}

		// Token: 0x06000175 RID: 373 RVA: 0x0000BE5C File Offset: 0x0000A05C
		public DataTable GetData()
		{
			return base.ExecuteDataTable("WF_OVERSGetData", null);
		}

		// Token: 0x06000176 RID: 374 RVA: 0x0000BE6A File Offset: 0x0000A06A
		public DataTable GetDataByEmpNo(double empNo, double date)
		{
			return base.ExecuteDataTable("WF_OVERSGetDataByEmpNo", new object[]
			{
				empNo,
				date
			});
		}

		// Token: 0x06000177 RID: 375 RVA: 0x0000BE90 File Offset: 0x0000A090
		public bool Insert(double empNo, double date, short time, double issuDate, string usersId)
		{
			if (base.ExecuteNonQuery("WF_OVERSInsert", new object[]
			{
				empNo,
				date,
				time,
				issuDate,
				usersId
			}) >= 0)
			{
				WorkFlowsDatabase.Instance.Employee.UpdateKVCurrentMonth(empNo, date, date);
				return true;
			}
			return false;
		}

		// Token: 0x06000178 RID: 376 RVA: 0x0000BEF0 File Offset: 0x0000A0F0
		public bool Update(double empNo, double date, short time, double issuDate, string usersId, short oldTime)
		{
			if (base.ExecuteNonQuery("WF_OVERSUpdate", new object[]
			{
				empNo,
				date,
				time,
				issuDate,
				usersId,
				oldTime
			}) >= 0)
			{
				WorkFlowsDatabase.Instance.Employee.UpdateKVCurrentMonth(empNo, date, date);
				return true;
			}
			return false;
		}

		// Token: 0x06000179 RID: 377 RVA: 0x0000BF5C File Offset: 0x0000A15C
		public bool Delete(int requestsID, double empNo, DateTime date)
		{
			bool result;
			try
			{
				if (base.ExecuteNonQuery("WF_OVERS_Delete", new object[]
				{
					requestsID
				}) >= 0)
				{
					WorkFlowsDatabase.Instance.Employee.UpdateKVCurrentMonth(empNo, date.ToOADate(), date.ToOADate());
					result = true;
				}
				else
				{
					result = false;
				}
			}
			catch
			{
				result = false;
			}
			return result;
		}
	}
}
