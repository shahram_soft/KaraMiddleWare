﻿using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using MiddleWare.KaraLib;


namespace PW.WorkFlows.SqlDal
{
	// Token: 0x0200001C RID: 28
	public class Groups : BaseDAL
	{
		// Token: 0x06000144 RID: 324 RVA: 0x00003C40 File Offset: 0x00001E40
		public Groups(WorkFlowsDatabase owner) : base(owner)
		{
		}

		// Token: 0x06000145 RID: 325 RVA: 0x00009FBC File Offset: 0x000081BC
		public DataTable GetData()
		{
			return base.ExecuteDataTable("WF_GROUPSGetData", null);
		}

		// Token: 0x06000146 RID: 326 RVA: 0x00009FCA File Offset: 0x000081CA
		public DataRow GetDataByGrpNo(short grpNo)
		{
			return base.ExecuteDataRow("WF_GROUPSGetDataByGrpNo", new object[]
			{
				grpNo
			});
		}

		// Token: 0x06000147 RID: 327 RVA: 0x00009FE8 File Offset: 0x000081E8
		public object Sp_MixFindShift(ref int? groupFirst, double? empNo, double? dateStart, int? year, int? month, int? day, ref int? shiftNew, ref int? calenT)
		{
			SqlCommand sqlCommand = new SqlCommand();
			sqlCommand.Connection = SqlHelper.ConStr;
			sqlCommand.CommandText = "dbo.Sp_MixFindShift";
			sqlCommand.CommandType = CommandType.StoredProcedure;
			sqlCommand.Parameters.Add(new SqlParameter("@RETURN_VALUE", SqlDbType.Int, 4, ParameterDirection.ReturnValue, 10, 0, null, DataRowVersion.Current, false, null, "", "", ""));
			sqlCommand.Parameters.Add(new SqlParameter("@GroupFirst", SqlDbType.Int, 4, ParameterDirection.InputOutput, 10, 0, null, DataRowVersion.Current, false, null, "", "", ""));
			sqlCommand.Parameters.Add(new SqlParameter("@Emp_No", SqlDbType.Float, 8, ParameterDirection.Input, 53, 0, null, DataRowVersion.Current, false, null, "", "", ""));
			sqlCommand.Parameters.Add(new SqlParameter("@DateStart", SqlDbType.Float, 8, ParameterDirection.Input, 53, 0, null, DataRowVersion.Current, false, null, "", "", ""));
			sqlCommand.Parameters.Add(new SqlParameter("@Year_", SqlDbType.Int, 4, ParameterDirection.Input, 10, 0, null, DataRowVersion.Current, false, null, "", "", ""));
			sqlCommand.Parameters.Add(new SqlParameter("@Month_", SqlDbType.Int, 4, ParameterDirection.Input, 10, 0, null, DataRowVersion.Current, false, null, "", "", ""));
			sqlCommand.Parameters.Add(new SqlParameter("@Day_", SqlDbType.Int, 4, ParameterDirection.Input, 10, 0, null, DataRowVersion.Current, false, null, "", "", ""));
			sqlCommand.Parameters.Add(new SqlParameter("@ShiftNew", SqlDbType.Int, 4, ParameterDirection.InputOutput, 10, 0, null, DataRowVersion.Current, false, null, "", "", ""));
			sqlCommand.Parameters.Add(new SqlParameter("@Calen_T", SqlDbType.Int, 4, ParameterDirection.InputOutput, 10, 0, null, DataRowVersion.Current, false, null, "", "", ""));
			sqlCommand.Parameters[1].Value = (groupFirst.HasValue ? (object) groupFirst.Value : DBNull.Value);
			sqlCommand.Parameters[2].Value = (empNo.HasValue ? (object) empNo.Value : DBNull.Value);
			sqlCommand.Parameters[3].Value = (dateStart.HasValue ? (object) dateStart.Value : DBNull.Value);
			sqlCommand.Parameters[4].Value = (year.HasValue ? (object) year.Value : DBNull.Value);
			sqlCommand.Parameters[5].Value = (month.HasValue ? (object) month.Value : DBNull.Value);
			sqlCommand.Parameters[6].Value = (day.HasValue ? (object) day.Value : DBNull.Value);
			sqlCommand.Parameters[7].Value = (shiftNew.HasValue ? (object) shiftNew.Value : DBNull.Value);
			sqlCommand.Parameters[8].Value = (calenT.HasValue ? (object) calenT.Value : DBNull.Value);
			ConnectionState state = sqlCommand.Connection.State;
			if ((sqlCommand.Connection.State & ConnectionState.Open) != ConnectionState.Open)
			{
				sqlCommand.Connection.Open();
			}
			object obj;
			try
			{
				obj = sqlCommand.ExecuteScalar();
			}
			finally
			{
				if (state == ConnectionState.Closed)
				{
					sqlCommand.Connection.Close();
				}
			}
			groupFirst = ((sqlCommand.Parameters[1].Value == null || sqlCommand.Parameters[1].Value is DBNull) ? null : new int?((int)sqlCommand.Parameters[1].Value));
			shiftNew = ((sqlCommand.Parameters[7].Value == null || sqlCommand.Parameters[7].Value is DBNull) ? null : new int?((int)sqlCommand.Parameters[7].Value));
			calenT = ((sqlCommand.Parameters[8].Value == null || sqlCommand.Parameters[8].Value is DBNull) ? null : new int?((int)sqlCommand.Parameters[8].Value));
			if (obj == null || obj is DBNull)
			{
				return null;
			}
			return obj;
		}

		// Token: 0x06000148 RID: 328 RVA: 0x0000A4B0 File Offset: 0x000086B0
		public short FindGrpsByEmpAndDate(double empNo, DateTime date, short grpNo, ref int shiftNo)
		{
			short value = ShDate.YearOf(date);
			short value2 = ShDate.MonthOf(date);
			short value3 = ShDate.DayOf(date);
			float num = Convert.ToSingle(date.ToOADate());
			int? num2 = new int?(0);
			int? num3 = new int?(0);
			int? num4 = new int?((int)grpNo);
			WorkFlowsDatabase.Instance.Groups.Sp_MixFindShift(ref num4, new double?(empNo), new double?((double)num), new int?((int)value), new int?((int)value2), new int?((int)value3), ref num2, ref num3);
			if (num2.HasValue)
			{
				shiftNo = num2.Value;
			}
			if (num2 < 0)
			{
				throw new Exception("تعریف شیفتها دارای اشکال می باشد.شیفت " + num2);
			}
			if (num4.HasValue)
			{
				return (short)num4.Value;
			}
			return -1;
		}

		// Token: 0x06000149 RID: 329 RVA: 0x0000A588 File Offset: 0x00008788
		public short FindGrpsByEmpAndDate(double empNo, DateTime date, ref int shiftNo)
		{
			short value = ShDate.YearOf(date);
			short value2 = ShDate.MonthOf(date);
			short value3 = ShDate.DayOf(date);
			float num = Convert.ToSingle(date.ToOADate());
			int? num2 = new int?(0);
			int? num3 = new int?(0);
			int? num4 = new int?(0);
			WorkFlowsDatabase.Instance.Groups.Sp_MixFindShift(ref num4, new double?(empNo), new double?((double)num), new int?((int)value), new int?((int)value2), new int?((int)value3), ref num2, ref num3);
			if (num2.HasValue)
			{
				shiftNo = num2.Value;
			}
			if (num4.HasValue)
			{
				return (short)num4.Value;
			}
			return -1;
		}

		// Token: 0x0600014A RID: 330 RVA: 0x0000A62C File Offset: 0x0000882C
		public short GetCurrentGroup(double empNo)
		{
			DateTime now = DateTime.Now;
			short value = ShDate.YearOf(now);
			short value2 = ShDate.MonthOf(now);
			short value3 = ShDate.DayOf(now);
			float num = Convert.ToSingle(now.ToOADate());
			int? num2 = new int?(0);
			int? num3 = new int?(0);
			int? num4 = new int?(0);
			WorkFlowsDatabase.Instance.Groups.Sp_MixFindShift(ref num4, new double?(empNo), new double?((double)num), new int?((int)value), new int?((int)value2), new int?((int)value3), ref num2, ref num3);
			if (num4.HasValue)
			{
				return (short)num4.Value;
			}
			return -1;
		}

		// Token: 0x0600014B RID: 331 RVA: 0x0000A6C4 File Offset: 0x000088C4
		public int GetDayTime(double empNo, DateTime date, ParmfileRow pr)
		{
			int num = (int)pr.KasrEsth;
			DataRow dataByEmpNo = WorkFlowsDatabase.Instance.Employee.GetDataByEmpNo(empNo);
			if (Convert.ToBoolean(dataByEmpNo["IS_UNIQ"]))
			{
				if (Convert.ToInt32(dataByEmpNo["Mor_Kasr"]) == 3)
				{
					num = Convert.ToInt32(dataByEmpNo["KASR_ESTH"]);
				}
			}
			else if (pr.DifKasrEsth)
			{
				int num2 = 0;
				short grpNo = WorkFlowsDatabase.Instance.Groups.FindGrpsByEmpAndDate(empNo, date, Convert.ToInt16(dataByEmpNo["GRP_NO"]), ref num2);
				DataRow dataByGrpNo = WorkFlowsDatabase.Instance.Groups.GetDataByGrpNo(grpNo);
				if (Convert.ToInt32(dataByGrpNo["Mor_Kasr"]) == 3)
				{
					num = Convert.ToInt32(dataByGrpNo["KASR_ESTH"]);
				}
			}
			else
			{
				num = (int)pr.KasrEsth;
			}
			if (num == 0)
			{
				num = 720;
			}
			return num;
		}
	}
}
