﻿using System;
using System.Data;

namespace PW.WorkFlows.SqlDal
{
	// Token: 0x02000012 RID: 18
	public class News : BaseDAL
	{
		// Token: 0x060000E3 RID: 227 RVA: 0x00003C40 File Offset: 0x00001E40
		public News(WorkFlowsDatabase owner) : base(owner)
		{
		}

		// Token: 0x060000E4 RID: 228 RVA: 0x000062C0 File Offset: 0x000044C0
		public DataTable GetNews()
		{
			return base.ExecuteDataTable(CommandType.Text, "SELECT *,dbo.ShamsyDate(n.StartDate) StartDateFa,dbo.ShamsyDate(n.EndDate) EndDateFa,CASE WHEN n.IsActive=1 THEN N'فعال' ELSE N'غيرفعال' END Active FROM News n");
		}

		// Token: 0x060000E5 RID: 229 RVA: 0x000062CE File Offset: 0x000044CE
		public DataTable GetOpenNews()
		{
			return base.ExecuteDataTable(CommandType.Text, "SELECT *,dbo.ShamsyDate(n.StartDate) StartDateFa FROM News n WHERE n.IsActive=1 AND (CAST(FLOOR(CAST(GETDATE() as FLOAT)) AS DateTime)) BETWEEN n.StartDate AND n.EndDate");
		}

		// Token: 0x060000E6 RID: 230 RVA: 0x000062DC File Offset: 0x000044DC
		public bool Delete(int newsId)
		{
			return base.ExecuteNonQuery("WF_DeleteNews", new object[]
			{
				newsId
			}) > 0;
		}

		// Token: 0x060000E7 RID: 231 RVA: 0x000062FC File Offset: 0x000044FC
		public bool Update(int newsId, string lead, string newsValue, DateTime startDate, DateTime endDate, bool isActive)
		{
			return base.ExecuteNonQuery("WF_UpdateNews", new object[]
			{
				newsId,
				lead,
				newsValue,
				startDate,
				endDate,
				isActive
			}) > 0;
		}

		// Token: 0x060000E8 RID: 232 RVA: 0x0000634C File Offset: 0x0000454C
		public bool Insert(string lead, string newsValue, DateTime startDate, DateTime endDate, bool isActive)
		{
			return base.ExecuteNonQuery("WF_InsertNews", new object[]
			{
				lead,
				newsValue,
				startDate,
				endDate,
				isActive
			}) > 0;
		}
	}
}
