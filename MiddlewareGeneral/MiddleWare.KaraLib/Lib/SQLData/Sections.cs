﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using MiddleWare.KaraLib.ViewModels;


namespace PW.WorkFlows.SqlDal
{
	// Token: 0x0200002C RID: 44
	public class Sections : BaseDAL
	{
		// Token: 0x060001E0 RID: 480 RVA: 0x0001442B File Offset: 0x0001262B
		public Sections(WorkFlowsDatabase owner) : base(owner)
		{
		}

		// Token: 0x060001E1 RID: 481 RVA: 0x0001443F File Offset: 0x0001263F
		public DataTable GetData()
		{
			return base.ExecuteDataTable("WF_SECTIONSGetData", null);
		}

		// Token: 0x060001E2 RID: 482 RVA: 0x0001444D File Offset: 0x0001264D
		public DataTable GetDataForOrgChart()
		{
			return base.ExecuteDataTable(CommandType.Text, "SELECT RTRIM(LTRIM(s.TITLE)) AS TITLE, s.SEC_NO, s.LOCAL_TEL, s.NEW_SEC, s.SELECTED,  \n       s.MODI_DATE, s.TFather, s.TMFather, s.EmpManager, s.SEmp_No, s.S_FDate, s.S_TDate,  \n       EMPLOYEE.NAME+' '+ EMPLOYEE.FAMILY AS FullName \nFROM   SECTIONS s LEFT OUTER JOIN EMPLOYEE ON  s.EmpManager = EMPLOYEE.EMP_NO");
		}

		// Token: 0x060001E3 RID: 483 RVA: 0x0001445C File Offset: 0x0001265C
		public DataRow GetByEmpNo(double empNo)
		{
			string commandText = "SELECT s.* FROM SECTIONS s JOIN EMPLOYEE e ON e.SEC_NO = s.SEC_NO WHERE e.EMP_NO=" + empNo;
			return base.ExecuteDataRow(CommandType.Text, commandText);
		}

		// Token: 0x060001E4 RID: 484 RVA: 0x00014484 File Offset: 0x00012684
		public int GetLimitOverTimeBySecNo(double secNo)
		{
			int result;
			try
			{
				string commandText = "SELECT s.WF_LimitOverTime FROM SECTIONS s WHERE s.SEC_NO=" + secNo;
				object obj = base.ExecuteScalar(CommandType.Text, commandText);
				if (obj != null)
				{
					result = Convert.ToInt32(obj);
				}
				else
				{
					result = 0;
				}
			}
			catch
			{
				result = 0;
			}
			return result;
		}

		// Token: 0x060001E5 RID: 485 RVA: 0x000144D4 File Offset: 0x000126D4
		public SectionsRow GetManagerNo(string empNo)
		{
			string commandText = string.Concat(new string[]
			{
				"SELECT top 1 * FROM SECTIONS AS s WHERE (SEC_NO =(SELECT TFather FROM SECTIONS AS s1 WHERE \n(SEC_NO =(SELECT SEC_NO FROM EMPLOYEE AS e WHERE (EMP_NO = ",
				empNo,
				"))))) OR \n(SEC_NO =(SELECT SEC_NO FROM EMPLOYEE AS e WHERE (EMP_NO = ",
				empNo,
				"))) AND  \n(EmpManager <> (CASE WHEN s.TFather <> 0 THEN ",
				empNo,
				" ELSE 0 END))"
			});
			SectionsRow sectionsRow = null;
			using (SqlDataReader sqlDataReader = base.ExecuteReader(CommandType.Text, commandText))
			{
				if (sqlDataReader.Read())
				{
					sectionsRow = new SectionsRow();
					sectionsRow.Title = Convert.ToString(sqlDataReader["TITLE"]);
					sectionsRow.SecNo = Convert.ToDouble((sqlDataReader["SEC_NO"] is DBNull || sqlDataReader["SEC_NO"] == null) ? 0 : sqlDataReader["SEC_NO"]);
					sectionsRow.LocalTel = Convert.ToString(sqlDataReader["LOCAL_TEL"]);
					sectionsRow.NewSec = Convert.ToDouble((sqlDataReader["NEW_SEC"] is DBNull || sqlDataReader["NEW_SEC"] == null) ? 0 : sqlDataReader["NEW_SEC"]);
					sectionsRow.Selected = Convert.ToBoolean(sqlDataReader["SELECTED"]);
					sectionsRow.ModiDate = Convert.ToDateTime(sqlDataReader["MODI_DATE"]);
					sectionsRow.Father = Convert.ToDouble((sqlDataReader["TFather"] is DBNull || sqlDataReader["TFather"] == null) ? 0 : sqlDataReader["TFather"]);
					sectionsRow.TmFather = Convert.ToDouble((sqlDataReader["TMFather"] is DBNull || sqlDataReader["TMFather"] == null) ? 0 : sqlDataReader["TMFather"]);
					sectionsRow.EmpManager = Convert.ToDouble((sqlDataReader["EmpManager"] is DBNull || sqlDataReader["EmpManager"] == null) ? 0 : sqlDataReader["EmpManager"]);
					sectionsRow.SEmpNo = Convert.ToDouble((sqlDataReader["SEmp_No"] is DBNull || sqlDataReader["SEmp_No"] == null) ? 0 : sqlDataReader["SEmp_No"]);
					sectionsRow.SfDate = Convert.ToString(sqlDataReader["S_FDate"]);
					sectionsRow.StDate = Convert.ToString(sqlDataReader["S_TDate"]);
					sectionsRow.Duty = Convert.ToInt32((sqlDataReader["Duty"] is DBNull || sqlDataReader["Duty"] == null) ? 0 : sqlDataReader["Duty"]);
					sectionsRow.Leave = Convert.ToInt32((sqlDataReader["Leave"] is DBNull || sqlDataReader["Leave"] == null) ? 0 : sqlDataReader["Leave"]);
					sectionsRow.LeaveOut = Convert.ToInt32((sqlDataReader["LeaveOut"] is DBNull || sqlDataReader["LeaveOut"] == null) ? 0 : sqlDataReader["LeaveOut"]);
				}
			}
			return sectionsRow;
		}

		// Token: 0x060001E6 RID: 486 RVA: 0x00014808 File Offset: 0x00012A08
		public double GetManagerOfSection(double empNo, ref string secNo)
		{
			DataTable data = this.GetData();
			if (data.Rows != null && data.Rows.Count > 0)
			{
				this.GetEmpManager(data, secNo, empNo);
				secNo = this._curSec;
				return this._mgrEmpNo;
			}
			return empNo;
		}

		// Token: 0x060001E7 RID: 487 RVA: 0x0001484C File Offset: 0x00012A4C
		private void GetEmpManager(DataTable dt, string secNo, double empNo)
		{
			DataRow[] array = dt.Select("SEC_NO=" + secNo);
			if (array.Length != 0 && !(array[0]["EmpManager"] is DBNull))
			{
				DataRow dataRow = array[0];
				if (Convert.ToDouble(dataRow["EmpManager"]) <= 0.0 || Convert.ToDouble(dataRow["EmpManager"]) == empNo)
				{
					if (Convert.ToDouble(dataRow["EmpManager"]) == empNo)
					{
						this._mgrEmpNo = Convert.ToDouble(dataRow["EmpManager"]);
						this._curSec = secNo;
					}
					this.GetEmpManager(dt, dataRow["TMFather"].ToString(), empNo);
					return;
				}
				this._mgrEmpNo = Convert.ToDouble(dataRow["EmpManager"]);
				this._curSec = secNo;
			}
		}

		// Token: 0x060001E8 RID: 488 RVA: 0x00014924 File Offset: 0x00012B24
		public int Update(double secNo, double empManager, bool setManager, bool setConferment, int limitOverTime)
		{
			return base.ExecuteNonQuery("WF_SECTIONS_Update", new object[]
			{
				secNo,
				empManager,
				setManager,
				setConferment,
				limitOverTime
			});
		}

		// Token: 0x060001E9 RID: 489 RVA: 0x00014974 File Offset: 0x00012B74
		public string GetSecByManager(double empManager)
		{
			DataTable dataTable = base.ExecuteDataTable(CommandType.Text, "SELECT s.SEC_NO FROM SECTIONS s WHERE s.EmpManager=" + empManager);
			string result = "";
			for (int i = 0; i < dataTable.Rows.Count; i++)
			{
				if (i + 1 == dataTable.Rows.Count)
				{
					result = dataTable.Rows[i]["SEC_NO"].ToString();
				}
				else
				{
					result = dataTable.Rows[i]["SEC_NO"] + " , ";
				}
			}
			return result;
		}

		// Token: 0x060001EA RID: 490 RVA: 0x00014A05 File Offset: 0x00012C05
		public DataTable GetSectionByManager(double empManager)
		{
			return base.ExecuteDataTable(CommandType.Text, "SELECT *,LTRIM(s.SEC_NO)+' - '+s.TITLE SectionInfo FROM SECTIONS s WHERE s.EmpManager=" + empManager);
		}

		// Token: 0x060001EB RID: 491 RVA: 0x00014A1E File Offset: 0x00012C1E
		public DataTable GetManagerBySubstitute(double sEmpNo, int sTDate, int sFDate)
		{
			return base.ExecuteDataTable("WF_SectionsGetManagerBySubstitute", new object[]
			{
				sEmpNo,
				sTDate,
				sFDate
			});
		}

		// Token: 0x060001EC RID: 492 RVA: 0x00014A4C File Offset: 0x00012C4C
		public List<SectionsRow> SectionsGetManagerBySubstitute(double sEmpNo, int sTDate, int sFDate)
		{
			List<SectionsRow> list = new List<SectionsRow>();
			List<SectionsRow> result;
			using (SqlDataReader sqlDataReader = base.ExecuteReader("WF_SectionsGetManagerBySubstitute", new object[]
			{
				sEmpNo,
				sTDate,
				sFDate
			}))
			{
				if (sqlDataReader.HasRows)
				{
					while (sqlDataReader.Read())
					{
						list.Add(new SectionsRow
						{
							Title = Convert.ToString(sqlDataReader["TITLE"]),
							SecNo = Convert.ToDouble((sqlDataReader["SEC_NO"] is DBNull || sqlDataReader["SEC_NO"] == null) ? 0 : sqlDataReader["SEC_NO"]),
							LocalTel = Convert.ToString(sqlDataReader["LOCAL_TEL"]),
							NewSec = Convert.ToDouble((sqlDataReader["NEW_SEC"] is DBNull || sqlDataReader["NEW_SEC"] == null) ? 0 : sqlDataReader["NEW_SEC"]),
							Selected = Convert.ToBoolean(sqlDataReader["SELECTED"]),
							ModiDate = Convert.ToDateTime(sqlDataReader["MODI_DATE"]),
							Father = Convert.ToDouble((sqlDataReader["TFather"] is DBNull || sqlDataReader["TFather"] == null) ? 0 : sqlDataReader["TFather"]),
							TmFather = Convert.ToDouble((sqlDataReader["TMFather"] is DBNull || sqlDataReader["TMFather"] == null) ? 0 : sqlDataReader["TMFather"]),
							EmpManager = Convert.ToDouble((sqlDataReader["EmpManager"] is DBNull || sqlDataReader["EmpManager"] == null) ? 0 : sqlDataReader["EmpManager"]),
							SEmpNo = Convert.ToDouble((sqlDataReader["SEmp_No"] is DBNull || sqlDataReader["SEmp_No"] == null) ? 0 : sqlDataReader["SEmp_No"]),
							SfDate = Convert.ToString(sqlDataReader["S_FDate"]),
							StDate = Convert.ToString(sqlDataReader["S_TDate"]),
							Duty = Convert.ToInt32((sqlDataReader["Duty"] is DBNull || sqlDataReader["Duty"] == null) ? 0 : sqlDataReader["Duty"]),
							Leave = Convert.ToInt32((sqlDataReader["Leave"] is DBNull || sqlDataReader["Leave"] == null) ? 0 : sqlDataReader["Leave"]),
							LeaveOut = Convert.ToInt32((sqlDataReader["LeaveOut"] is DBNull || sqlDataReader["LeaveOut"] == null) ? 0 : sqlDataReader["LeaveOut"])
						});
					}
				}
				result = list;
			}
			return result;
		}

		// Token: 0x060001ED RID: 493 RVA: 0x00014D88 File Offset: 0x00012F88
		public int GetFreeSection()
		{
			object obj = base.ExecuteScalar(CommandType.Text, "SELECT COUNT(1) FreeNode FROM SECTIONS s WHERE (SELECT COUNT(1) FROM SECTIONS s1 WHERE s1.SEC_NO=s.TMFather)<1");
			if (obj.ToString() != "")
			{
				return Convert.ToInt32(obj);
			}
			return -1;
		}

		// Token: 0x060001EE RID: 494 RVA: 0x00014DBC File Offset: 0x00012FBC
		public DataTable GetManager()
		{
			string commandText = "SELECT s.EmpManager,e.[NAME]+' '+ e.FAMILY FullName  FROM SECTIONS s  \nJOIN EMPLOYEE e ON s.EmpManager=e.EMP_NO WHERE s.EmpManager>0";
			return base.ExecuteDataTable(CommandType.Text, commandText);
		}

		// Token: 0x060001EF RID: 495 RVA: 0x00014DD8 File Offset: 0x00012FD8
		public DataTable GetSectionByWarden(double wardenEmpNo)
		{
			string commandText;
			if (wardenEmpNo == 0.0)
			{
				commandText = "SELECT DISTINCT s.TITLE,s.SEC_NO FROM SECTIONS s";
			}
			else
			{
				commandText = "SELECT DISTINCT s.TITLE,s.SEC_NO FROM SECTIONS s JOIN WardenSection ws ON s.SEC_NO=ws.SecNo WHERE ws.WardenEmp_NO=" + wardenEmpNo;
			}
			return base.ExecuteDataTable(CommandType.Text, commandText);
		}

		// Token: 0x060001F0 RID: 496 RVA: 0x00014E18 File Offset: 0x00013018
		public DataRow GetParentSectionManager(int secNo)
		{
			string commandText = "SELECT * FROM SECTIONS s WHERE s.SEC_NO=(SELECT s.TFather FROM SECTIONS s WHERE s.SEC_NO=" + secNo + ")";
			return base.ExecuteDataRow(CommandType.Text, commandText);
		}

		// Token: 0x060001F1 RID: 497 RVA: 0x00014E44 File Offset: 0x00013044
		private void GetSubsetSection(double secno, ref DataTable subsetSections)
		{
			foreach (DataRow dataRow in base.ExecuteDataTable(CommandType.Text, "SELECT s.*,e.[NAME]+' '+e.FAMILY ManagerName FROM SECTIONS s JOIN EMPLOYEE e ON s.EmpManager=e.EMP_NO WHERE s.TMFather=" + secno).Rows)
			{
				DataRow dataRow2 = subsetSections.NewRow();
				dataRow2["SEC_NO"] = dataRow["SEC_NO"];
				dataRow2["EmpManager"] = dataRow["EmpManager"];
				dataRow2["ManagerName"] = dataRow["ManagerName"].ToString();
				subsetSections.Rows.Add(dataRow2);
				this.GetSubsetSection(Convert.ToDouble(dataRow["SEC_NO"]), ref subsetSections);
			}
		}

		// Token: 0x060001F2 RID: 498 RVA: 0x00014F24 File Offset: 0x00013124
		public DataTable GetSubsetSection(double managerEmpno)
		{
			DataTable dataTable = new DataTable();
			dataTable.Columns.Add("SEC_NO", typeof(double));
			dataTable.Columns.Add("EmpManager", typeof(double));
			dataTable.Columns.Add("ManagerName", typeof(string));
			foreach (DataRow dataRow in this.GetSubsetbyManager(managerEmpno).Rows)
			{
				DataRow dataRow2 = dataTable.NewRow();
				dataRow2["SEC_NO"] = dataRow["SEC_NO"];
				dataRow2["EmpManager"] = dataRow["EmpManager"];
				dataRow2["ManagerName"] = dataRow["ManagerName"].ToString();
				dataTable.Rows.Add(dataRow2);
				this.GetSubsetSection(Convert.ToDouble(dataRow["SEC_NO"]), ref dataTable);
			}
			return dataTable;
		}

		// Token: 0x060001F3 RID: 499 RVA: 0x0001504C File Offset: 0x0001324C
		public DataTable GetSubsetEmployee(double managerEmpno)
		{
			DataTable arg_0D_0 = this.GetSubsetSection(managerEmpno);
			string text = "";
			foreach (DataRow dataRow in arg_0D_0.Rows)
			{
				text = text + dataRow["SEC_NO"] + ",";
			}
			if (string.IsNullOrEmpty(text))
			{
				return null;
			}
			text = text.Remove(text.Length - 1);
			return base.ExecuteDataTable("WF_EmployeeGetSubsetPerson", new object[]
			{
				managerEmpno,
				text
			});
		}

		// Token: 0x060001F4 RID: 500 RVA: 0x000150F8 File Offset: 0x000132F8
		public DataTable GetSubsetbyManager(double managerEmpno)
		{
			return base.ExecuteDataTable("WF_SectionsGetSubset", new object[]
			{
				managerEmpno
			});
		}

		// Token: 0x060001F5 RID: 501 RVA: 0x00015114 File Offset: 0x00013314
		public DataTable GetSubset(double secNo)
		{
			DataTable dataTable = base.ExecuteDataTable(CommandType.Text, "SELECT s.* FROM SECTIONS s");
			DataTable dataTable2 = dataTable.Clone();
			dataTable2.ImportRow(dataTable.Select("SEC_NO=" + secNo)[0]);
			Sections.GetSubset(secNo, dataTable, ref dataTable2);
			return dataTable2;
		}

		// Token: 0x060001F6 RID: 502 RVA: 0x00015160 File Offset: 0x00013360
		private static void GetSubset(double secNo, DataTable dtSections, ref DataTable dtSubset)
		{
			DataRow[] array = dtSections.Select("TMFather=" + secNo);
			for (int i = 0; i < array.Length; i++)
			{
				DataRow dataRow = array[i];
				dtSubset.ImportRow(dataRow);
				secNo = Convert.ToDouble(dataRow["SEC_NO"]);
				Sections.GetSubset(secNo, dtSections, ref dtSubset);
			}
		}

		// Token: 0x060001F7 RID: 503 RVA: 0x000151B8 File Offset: 0x000133B8
		public int UpdateParallelApprove(int? wf_ParallelApproveEmp_No, int secNo)
		{
			string commandText = string.Format("UPDATE Sections SET WF_ParallelApproveEmp_No = {0},WF_ParallelApproveStartDate=GetDate()-1 WHERE Sec_No={1}", wf_ParallelApproveEmp_No.HasValue ? wf_ParallelApproveEmp_No.Value.ToString() : "NULL", secNo);
			return base.ExecuteNonQuery(CommandType.Text, commandText);
		}

		// Token: 0x060001F8 RID: 504 RVA: 0x000151FD File Offset: 0x000133FD
		public DataTable GetParallelApprove()
		{
			return base.ExecuteDataTable(CommandType.Text, "SELECT s.*,e.[Name]+' '+e.Family FullName FROM Sections s JOIN Employee e ON e.Emp_No = s.WF_ParallelApproveEmp_No WHERE s.WF_ParallelApproveEmp_No IS NOT NULL");
		}

		// Token: 0x04000008 RID: 8
		private double _mgrEmpNo;

		// Token: 0x04000009 RID: 9
		private string _curSec = "";
	}
    // Token: 0x02000020 RID: 32
    public class SectionsRow : BaseRow
    {
        // Token: 0x06000223 RID: 547 RVA: 0x00002D48 File Offset: 0x00000F48
        public SectionsRow()
        {
        }

        // Token: 0x06000224 RID: 548 RVA: 0x00004A5C File Offset: 0x00002C5C
        public SectionsRow(string title, double secNo)
        {
            this.Title = title;
            this.SecNo = secNo;
        }

        // Token: 0x06000225 RID: 549 RVA: 0x00004A74 File Offset: 0x00002C74
        public SectionsRow(string title, double secNo, string localTel, double newSec, bool selected, DateTime modiDate, double father, double tmFather, double empManager, double sEmpNo, string sfDate, string stDate, int duty, int leave, int leaveOut)
        {
            this.Title = title;
            this.SecNo = secNo;
            this.LocalTel = localTel;
            this.NewSec = newSec;
            this.Selected = selected;
            this.ModiDate = modiDate;
            this.Father = father;
            this.TmFather = tmFather;
            this.EmpManager = empManager;
            this.SEmpNo = sEmpNo;
            this.SfDate = sfDate;
            this.StDate = stDate;
            this.Duty = duty;
            this.Leave = leave;
            this.LeaveOut = leaveOut;
        }

        // Token: 0x17000102 RID: 258
        // (get) Token: 0x06000226 RID: 550 RVA: 0x00004AFC File Offset: 0x00002CFC
        // (set) Token: 0x06000227 RID: 551 RVA: 0x00004B04 File Offset: 0x00002D04
        public string Title
        {
            get;
            set;
        }

        // Token: 0x17000103 RID: 259
        // (get) Token: 0x06000228 RID: 552 RVA: 0x00004B0D File Offset: 0x00002D0D
        // (set) Token: 0x06000229 RID: 553 RVA: 0x00004B15 File Offset: 0x00002D15
        public double SecNo
        {
            get;
            set;
        }

        // Token: 0x17000104 RID: 260
        // (get) Token: 0x0600022A RID: 554 RVA: 0x00004B1E File Offset: 0x00002D1E
        // (set) Token: 0x0600022B RID: 555 RVA: 0x00004B26 File Offset: 0x00002D26
        public string LocalTel
        {
            get;
            set;
        }

        // Token: 0x17000105 RID: 261
        // (get) Token: 0x0600022C RID: 556 RVA: 0x00004B2F File Offset: 0x00002D2F
        // (set) Token: 0x0600022D RID: 557 RVA: 0x00004B37 File Offset: 0x00002D37
        public double NewSec
        {
            get;
            set;
        }

        // Token: 0x17000106 RID: 262
        // (get) Token: 0x0600022E RID: 558 RVA: 0x00004B40 File Offset: 0x00002D40
        // (set) Token: 0x0600022F RID: 559 RVA: 0x00004B48 File Offset: 0x00002D48
        public bool Selected
        {
            get;
            set;
        }

        // Token: 0x17000107 RID: 263
        // (get) Token: 0x06000230 RID: 560 RVA: 0x00004B51 File Offset: 0x00002D51
        // (set) Token: 0x06000231 RID: 561 RVA: 0x00004B59 File Offset: 0x00002D59
        public DateTime ModiDate
        {
            get;
            set;
        }

        // Token: 0x17000108 RID: 264
        // (get) Token: 0x06000232 RID: 562 RVA: 0x00004B62 File Offset: 0x00002D62
        // (set) Token: 0x06000233 RID: 563 RVA: 0x00004B6A File Offset: 0x00002D6A
        public double Father
        {
            get;
            set;
        }

        // Token: 0x17000109 RID: 265
        // (get) Token: 0x06000234 RID: 564 RVA: 0x00004B73 File Offset: 0x00002D73
        // (set) Token: 0x06000235 RID: 565 RVA: 0x00004B7B File Offset: 0x00002D7B
        public double TmFather
        {
            get;
            set;
        }

        // Token: 0x1700010A RID: 266
        // (get) Token: 0x06000236 RID: 566 RVA: 0x00004B84 File Offset: 0x00002D84
        // (set) Token: 0x06000237 RID: 567 RVA: 0x00004B8C File Offset: 0x00002D8C
        public double EmpManager
        {
            get;
            set;
        }

        // Token: 0x1700010B RID: 267
        // (get) Token: 0x06000238 RID: 568 RVA: 0x00004B95 File Offset: 0x00002D95
        // (set) Token: 0x06000239 RID: 569 RVA: 0x00004B9D File Offset: 0x00002D9D
        public double SEmpNo
        {
            get;
            set;
        }

        // Token: 0x1700010C RID: 268
        // (get) Token: 0x0600023A RID: 570 RVA: 0x00004BA6 File Offset: 0x00002DA6
        // (set) Token: 0x0600023B RID: 571 RVA: 0x00004BAE File Offset: 0x00002DAE
        public string SfDate
        {
            get;
            set;
        }

        // Token: 0x1700010D RID: 269
        // (get) Token: 0x0600023C RID: 572 RVA: 0x00004BB7 File Offset: 0x00002DB7
        // (set) Token: 0x0600023D RID: 573 RVA: 0x00004BBF File Offset: 0x00002DBF
        public string StDate
        {
            get;
            set;
        }

        // Token: 0x1700010E RID: 270
        // (get) Token: 0x0600023E RID: 574 RVA: 0x00004BC8 File Offset: 0x00002DC8
        // (set) Token: 0x0600023F RID: 575 RVA: 0x00004BD0 File Offset: 0x00002DD0
        public int Duty
        {
            get;
            set;
        }

        // Token: 0x1700010F RID: 271
        // (get) Token: 0x06000240 RID: 576 RVA: 0x00004BD9 File Offset: 0x00002DD9
        // (set) Token: 0x06000241 RID: 577 RVA: 0x00004BE1 File Offset: 0x00002DE1
        public int Leave
        {
            get;
            set;
        }

        // Token: 0x17000110 RID: 272
        // (get) Token: 0x06000242 RID: 578 RVA: 0x00004BEA File Offset: 0x00002DEA
        // (set) Token: 0x06000243 RID: 579 RVA: 0x00004BF2 File Offset: 0x00002DF2
        public int LeaveOut
        {
            get;
            set;
        }
    }
}
