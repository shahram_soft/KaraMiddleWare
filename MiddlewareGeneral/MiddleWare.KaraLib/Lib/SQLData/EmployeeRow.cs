﻿using System.Data;
using System.Data.SqlClient;

namespace MiddleWare.KaraLib.Lib.SQLData
{
  public  class EmployeeRow : BaseCls
    {
        // Token: 0x06000026 RID: 38 RVA: 0x00002269 File Offset: 0x00000469
        public EmployeeRow()
        {
        }

        // Token: 0x06000027 RID: 39 RVA: 0x00002274 File Offset: 0x00000474
        public EmployeeRow(DataRow dr)
        {
            this.EmpNo = base.CastDouble(dr["EMP_NO"]);
            this.SecNo = base.CastInt(dr["SEC_NO"]);
            this.EmpName = dr["EmpName"].ToString();
        }

        // Token: 0x06000028 RID: 40 RVA: 0x000022CC File Offset: 0x000004CC
        public EmployeeRow(SqlDataReader dr)
        {
            this.EmpNo = base.CastDouble(dr["EMP_NO"]);
            this.SecNo = base.CastInt(dr["SEC_NO"]);
            this.EmpName = dr["EmpName"].ToString();
        }

        // Token: 0x1700000E RID: 14
        // (get) Token: 0x06000029 RID: 41 RVA: 0x00002323 File Offset: 0x00000523
        // (set) Token: 0x0600002A RID: 42 RVA: 0x0000232B File Offset: 0x0000052B
        public double EmpNo
        {
            get;
            set;
        }

        // Token: 0x1700000F RID: 15
        // (get) Token: 0x0600002B RID: 43 RVA: 0x00002334 File Offset: 0x00000534
        // (set) Token: 0x0600002C RID: 44 RVA: 0x0000233C File Offset: 0x0000053C
        public int SecNo
        {
            get;
            set;
        }

        // Token: 0x17000010 RID: 16
        // (get) Token: 0x0600002D RID: 45 RVA: 0x00002345 File Offset: 0x00000545
        // (set) Token: 0x0600002E RID: 46 RVA: 0x0000234D File Offset: 0x0000054D
        public string EmpName
        {
            get;
            set;
        }

        // Token: 0x17000011 RID: 17
        // (get) Token: 0x0600002F RID: 47 RVA: 0x00002356 File Offset: 0x00000556
        public string EmpVal
        {
            get
            {
                return this.EmpNo + "#" + this.SecNo;
            }
        }
    }
}
