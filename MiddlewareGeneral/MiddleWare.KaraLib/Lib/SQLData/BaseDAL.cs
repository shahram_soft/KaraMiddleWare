﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using MiddleWare.KaraLib;


namespace PW.WorkFlows.SqlDal
{
	// Token: 0x0200000B RID: 11
	public class BaseDAL
	{
		// Token: 0x17000003 RID: 3
		// (get) Token: 0x06000083 RID: 131 RVA: 0x00004288 File Offset: 0x00002488
		public static bool IsLoged
		{
			get
			{
				bool result;
				try
				{
					result = Convert.ToBoolean(ConfigurationManager.AppSettings["IsLoegd"]);
				}
				catch
				{
					result = false;
				}
				return result;
			}
		}

		// Token: 0x06000084 RID: 132 RVA: 0x000042C4 File Offset: 0x000024C4
		public void GetDuration(int operationsId, int typeNo, bool isDay, bool? isFinalApproved, string startHour, string endHour, ref string duration, ref string requestedTime, ref string cardTitle)
		{
			if (string.IsNullOrEmpty(duration))
			{
				return;
			}
			switch (operationsId)
			{
			case 1:
				duration = UtilityManagers.ConvertToTimeFormat(duration);
				requestedTime = UtilityManagers.ConvertToTimeFormat(requestedTime);
				return;
			case 2:
				switch (typeNo)
				{
				case 100:
					duration = string.Empty;
					if (isFinalApproved.HasValue && isFinalApproved.Value)
					{
						requestedTime = startHour;
						return;
					}
					break;
				case 101:
					requestedTime = ((isFinalApproved.HasValue && isFinalApproved.Value) ? duration : string.Empty);
					if (string.IsNullOrEmpty(endHour) || endHour == "0" || endHour == "00:00")
					{
						duration = "حذف " + startHour;
						return;
					}
					duration = "قدیم:" + startHour + " جدید:" + UtilityManagers.ConvertToTimeFormat(endHour);
					return;
				case 102:
					duration += "روز";
					if (isFinalApproved.HasValue && isFinalApproved.Value)
					{
						requestedTime = duration;
						return;
					}
					break;
				case 103:
					break;
				case 104:
					duration = duration.Insert(duration.Length - 2, ":");
					if (isFinalApproved.HasValue && isFinalApproved.Value)
					{
						requestedTime = duration;
						return;
					}
					break;
				case 105:
					cardTitle = "اصلاح نوع ورودوخروج";
					if (duration == "5")
					{
						cardTitle += "-اضافه کار نامجاز";
					}
					duration = UtilityManagers.ConvertToTimeFormat(endHour);
					if (isFinalApproved.HasValue && isFinalApproved.Value)
					{
						requestedTime = duration;
						return;
					}
					break;
				default:
					return;
				}
				break;
			case 3:
				requestedTime = UtilityManagers.ConvertToTimeFormat(requestedTime);
				if (isDay)
				{
					duration += "روز";
					requestedTime += "روز";
					return;
				}
				duration = UtilityManagers.MakeTimeFormat(duration);
				requestedTime = UtilityManagers.ConvertToTimeFormat(requestedTime);
				return;
			case 4:
				duration = UtilityManagers.MakeTimeFormat(duration);
				if (isFinalApproved.HasValue && isFinalApproved.Value)
				{
					requestedTime = duration;
				}
				cardTitle += " بدون بازگشت";
				break;
			case 5:
				if (isDay)
				{
					duration += "روز";
					requestedTime += "روز";
					return;
				}
				if (Convert.ToDouble(duration) > 1.0)
				{
					duration = UtilityManagers.ConvertToTimeFormat(duration);
					requestedTime = UtilityManagers.ConvertToTimeFormat(requestedTime);
					return;
				}
				break;
			default:
				return;
			}
		}

		// Token: 0x06000085 RID: 133 RVA: 0x00004540 File Offset: 0x00002740
		public BaseDAL(WorkFlowsDatabase owner)
		{
			this._parentDatabase = owner;
		}

		// Token: 0x17000004 RID: 4
		// (get) Token: 0x06000086 RID: 134 RVA: 0x0000454F File Offset: 0x0000274F
		public WorkFlowsDatabase Database
		{
			get
			{
				return this._parentDatabase;
			}
		}

		// Token: 0x06000087 RID: 135 RVA: 0x00004558 File Offset: 0x00002758
		private static void AttachParameters(SqlCommand command, IEnumerable<SqlParameter> commandParameters)
		{
			if (command == null)
			{
				throw new ArgumentNullException("command");
			}
			if (commandParameters != null)
			{
				foreach (SqlParameter current in commandParameters)
				{
					if (current != null)
					{
						if ((current.Direction == ParameterDirection.InputOutput || current.Direction == ParameterDirection.Input) && current.Value == null)
						{
							current.Value = DBNull.Value;
						}
						command.Parameters.Add(current);
					}
				}
			}
		}

		// Token: 0x06000088 RID: 136 RVA: 0x000045E0 File Offset: 0x000027E0
		private static void AssignParameterValues(IEnumerable<SqlParameter> commandParameters, DataRow dataRow)
		{
			if (commandParameters == null || dataRow == null)
			{
				return;
			}
			int num = 0;
			foreach (SqlParameter current in commandParameters)
			{
				if (current.ParameterName == null || current.ParameterName.Length <= 1)
				{
					throw new Exception(string.Format("Please provide a valid parameter name on the parameter #{0}, the ParameterName property has the following value: '{1}'.", num, current.ParameterName));
				}
				if (dataRow.Table.Columns.IndexOf(current.ParameterName.Substring(1)) != -1)
				{
					current.Value = dataRow[current.ParameterName.Substring(1)];
				}
				num++;
			}
		}

		// Token: 0x06000089 RID: 137 RVA: 0x00004698 File Offset: 0x00002898
		private static void AssignParameterValues(IList<SqlParameter> commandParameters, object[] parameterValues)
		{
			if (commandParameters == null || parameterValues == null)
			{
				return;
			}
			if (commandParameters.Count != parameterValues.Length)
			{
				throw new ArgumentException("Parameter count does not match Parameter Value count.");
			}
			int i = 0;
			int count = commandParameters.Count;
			while (i < count)
			{
				if (parameterValues[i] is IDbDataParameter)
				{
					IDbDataParameter dbDataParameter = (IDbDataParameter)parameterValues[i];
					if (dbDataParameter.Value == null)
					{
						commandParameters[i].Value = DBNull.Value;
					}
					else
					{
						commandParameters[i].Value = dbDataParameter.Value;
					}
				}
				else if (parameterValues[i] == null)
				{
					commandParameters[i].Value = DBNull.Value;
				}
				else
				{
					commandParameters[i].Value = parameterValues[i];
				}
				i++;
			}
		}

		// Token: 0x0600008A RID: 138 RVA: 0x00004740 File Offset: 0x00002940
		private static void PrepareCommand(SqlCommand command, SqlConnection connection, SqlTransaction transaction, CommandType commandType, string commandText, SqlParameter[] commandParameters, out bool mustCloseConnection)
		{
			if (command == null)
			{
				throw new ArgumentNullException("command");
			}
			if (commandText == null || commandText.Length == 0)
			{
				throw new ArgumentNullException("commandText");
			}
			if (connection.State != ConnectionState.Open)
			{
				mustCloseConnection = true;
				connection.Open();
			}
			else
			{
				mustCloseConnection = false;
			}
			command.Connection = connection;
			command.CommandText = commandText;
			if (transaction != null)
			{
				if (transaction.Connection == null)
				{
					throw new ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", "transaction");
				}
				command.Transaction = transaction;
			}
			command.CommandType = commandType;
			if (commandParameters != null)
			{
				BaseDAL.AttachParameters(command, commandParameters);
			}
		}

		// Token: 0x17000005 RID: 5
		// (get) Token: 0x0600008B RID: 139 RVA: 0x000047D0 File Offset: 0x000029D0
		private SqlTransaction Transaction
		{
			get
			{
				return this._parentDatabase.Transaction;
			}
		}

		// Token: 0x17000006 RID: 6
		// (get) Token: 0x0600008C RID: 140 RVA: 0x000047DD File Offset: 0x000029DD
		private string ConnectionString
		{
			get
			{
				return this._parentDatabase.ConnectionString;
			}
		}

		// Token: 0x0600008D RID: 141 RVA: 0x000047EA File Offset: 0x000029EA
		public int ExecuteNonQuery(string spName, params object[] parameterValues)
		{
			if (this.Transaction != null)
			{
				return SqlHelper.ExecuteNonQuery(this.Transaction, spName, parameterValues);
			}
			return SqlHelper.ExecuteNonQuery(this.ConnectionString, spName, parameterValues);
		}

		// Token: 0x0600008E RID: 142 RVA: 0x0000480F File Offset: 0x00002A0F
		public int ExecuteNonQuery(CommandType commandType, string commandText)
		{
			if (this.Transaction != null)
			{
				return SqlHelper.ExecuteNonQuery(this.Transaction, commandType, commandText);
			}
			return SqlHelper.ExecuteNonQuery(this.ConnectionString, commandType, commandText);
		}

		// Token: 0x0600008F RID: 143 RVA: 0x00004834 File Offset: 0x00002A34
		public int ExecuteNonQuery(CommandType commandType, string commandText, params SqlParameter[] commandParameters)
		{
			if (this.Transaction != null)
			{
				return SqlHelper.ExecuteNonQuery(this.Transaction, commandType, commandText, commandParameters);
			}
			return SqlHelper.ExecuteNonQuery(this.ConnectionString, commandType, commandText, commandParameters);
		}

		// Token: 0x06000090 RID: 144 RVA: 0x0000485B File Offset: 0x00002A5B
		public SqlDataReader ExecuteReader(string spName, params object[] parameterValues)
		{
			if (this.Transaction != null)
			{
				return SqlHelper.ExecuteReader(this.Transaction, spName, parameterValues);
			}
			return SqlHelper.ExecuteReader(this.ConnectionString, spName, parameterValues);
		}

		// Token: 0x06000091 RID: 145 RVA: 0x00004880 File Offset: 0x00002A80
		public SqlDataReader ExecuteReader(CommandType commandType, string commandText, params SqlParameter[] commandParameters)
		{
			if (this.Transaction != null)
			{
				return SqlHelper.ExecuteReader(this.Transaction, commandType, commandText, commandParameters);
			}
			return SqlHelper.ExecuteReader(this.ConnectionString, commandType, commandText, commandParameters);
		}

		// Token: 0x06000092 RID: 146 RVA: 0x000048A7 File Offset: 0x00002AA7
		public SqlDataReader ExecuteReader(CommandType commandType, string commandText)
		{
			if (this.Transaction != null)
			{
				return SqlHelper.ExecuteReader(this.Transaction, commandType, commandText);
			}
			return SqlHelper.ExecuteReader(this.ConnectionString, commandType, commandText);
		}

		// Token: 0x06000093 RID: 147 RVA: 0x000048CC File Offset: 0x00002ACC
		public DataTable ExecuteDataTable(string spName, params object[] parameterValues)
		{
			if (this.Transaction != null)
			{
				DataSet dataSet = SqlHelper.ExecuteDataset(this.Transaction, spName, parameterValues);
				if (dataSet == null || dataSet.Tables.Count <= 0)
				{
					return new DataTable();
				}
				if (string.IsNullOrEmpty(Condition.ExperesionSection) || dataSet.Tables[0].Rows.Count <= 0 || dataSet.Tables[0].Columns.IndexOf("SEC_NO") <= 0)
				{
					return dataSet.Tables[0];
				}
				DataRow[] array = dataSet.Tables[0].Select(Condition.ExperesionSection);
				if (array.Length == 0)
				{
					return new DataTable();
				}
				return array.CopyToDataTable<DataRow>();
			}
			else
			{
				DataSet dataSet = SqlHelper.ExecuteDataset(this.ConnectionString, spName, parameterValues);
				if (dataSet == null || dataSet.Tables.Count <= 0)
				{
					return new DataTable();
				}
				if (string.IsNullOrEmpty(Condition.ExperesionSection) || dataSet.Tables[0].Rows.Count <= 0 || dataSet.Tables[0].Columns.IndexOf("SEC_NO") <= 0)
				{
					return dataSet.Tables[0];
				}
				DataRow[] array2 = dataSet.Tables[0].Select(Condition.ExperesionSection);
				if (array2.Length == 0)
				{
					return new DataTable();
				}
				return array2.CopyToDataTable<DataRow>();
			}
		}

		// Token: 0x06000094 RID: 148 RVA: 0x00004A24 File Offset: 0x00002C24
		public DataTable ExecuteDataTable(CommandType commandType, string commandText, params SqlParameter[] commandParameters)
		{
			if (this.Transaction != null)
			{
				DataSet dataSet = SqlHelper.ExecuteDataset(this.Transaction, commandType, commandText, commandParameters);
				if (dataSet != null && dataSet.Tables.Count > 0)
				{
					return dataSet.Tables[0];
				}
				return null;
			}
			else
			{
				DataSet dataSet = SqlHelper.ExecuteDataset(this.ConnectionString, commandType, commandText, commandParameters);
				if (dataSet != null && dataSet.Tables.Count > 0)
				{
					return dataSet.Tables[0];
				}
				return null;
			}
		}

		// Token: 0x06000095 RID: 149 RVA: 0x00004A98 File Offset: 0x00002C98
		public DataTable ExecuteDataTable(CommandType commandType, string commandText)
		{
			if (this.Transaction != null)
			{
				DataSet dataSet = SqlHelper.ExecuteDataset(this.Transaction, commandType, commandText);
				if (dataSet == null || dataSet.Tables.Count <= 0)
				{
					return new DataTable();
				}
				if (string.IsNullOrEmpty(Condition.ExperesionSection) || dataSet.Tables[0].Rows.Count <= 0 || dataSet.Tables[0].Columns.IndexOf("SEC_NO") <= 0)
				{
					return dataSet.Tables[0];
				}
				DataRow[] array = dataSet.Tables[0].Select(Condition.ExperesionSection);
				if (array.Length == 0)
				{
					return new DataTable();
				}
				return array.CopyToDataTable<DataRow>();
			}
			else
			{
				DataSet dataSet = SqlHelper.ExecuteDataset(this.ConnectionString, commandType, commandText);
				if (dataSet == null || dataSet.Tables.Count <= 0)
				{
					return new DataTable();
				}
				if (string.IsNullOrEmpty(Condition.ExperesionSection) || dataSet.Tables[0].Rows.Count <= 0 || dataSet.Tables[0].Columns.IndexOf("SEC_NO") <= 0)
				{
					return dataSet.Tables[0];
				}
				DataRow[] array2 = dataSet.Tables[0].Select(Condition.ExperesionSection);
				if (array2.Length == 0)
				{
					return new DataTable();
				}
				return array2.CopyToDataTable<DataRow>();
			}
		}

		// Token: 0x06000096 RID: 150 RVA: 0x00004BF0 File Offset: 0x00002DF0
		public DataRow ExecuteDataRow(string spName, params object[] parameterValues)
		{
			if (this.Transaction != null)
			{
				DataSet dataSet = SqlHelper.ExecuteDataset(this.Transaction, spName, parameterValues);
				if (dataSet != null && dataSet.Tables.Count > 0 && dataSet.Tables[0].Rows.Count > 0)
				{
					return dataSet.Tables[0].Rows[0];
				}
				return null;
			}
			else
			{
				DataSet dataSet = SqlHelper.ExecuteDataset(this.ConnectionString, spName, parameterValues);
				if (dataSet != null && dataSet.Tables.Count > 0 && dataSet.Tables[0].Rows.Count > 0)
				{
					return dataSet.Tables[0].Rows[0];
				}
				return null;
			}
		}

		// Token: 0x06000097 RID: 151 RVA: 0x00004CA8 File Offset: 0x00002EA8
		public DataRow ExecuteDataRow(CommandType commandType, string commandText, params SqlParameter[] commandParameters)
		{
			if (this.Transaction != null)
			{
				DataSet dataSet = SqlHelper.ExecuteDataset(this.Transaction, commandType, commandText, commandParameters);
				if (dataSet != null && dataSet.Tables.Count > 0 && dataSet.Tables[0].Rows.Count > 0)
				{
					return dataSet.Tables[0].Rows[0];
				}
				return null;
			}
			else
			{
				DataSet dataSet = SqlHelper.ExecuteDataset(this.ConnectionString, commandType, commandText, commandParameters);
				if (dataSet != null && dataSet.Tables.Count > 0 && dataSet.Tables[0].Rows.Count > 0)
				{
					return dataSet.Tables[0].Rows[0];
				}
				return null;
			}
		}

		// Token: 0x06000098 RID: 152 RVA: 0x00004D64 File Offset: 0x00002F64
		public DataRow ExecuteDataRow(CommandType commandType, string commandText)
		{
			if (this.Transaction != null)
			{
				DataSet dataSet = SqlHelper.ExecuteDataset(this.Transaction, commandType, commandText);
				if (dataSet != null && dataSet.Tables.Count > 0 && dataSet.Tables[0].Rows.Count > 0)
				{
					return dataSet.Tables[0].Rows[0];
				}
				return null;
			}
			else
			{
				DataSet dataSet = SqlHelper.ExecuteDataset(this.ConnectionString, commandType, commandText);
				if (dataSet != null && dataSet.Tables.Count > 0 && dataSet.Tables[0].Rows.Count > 0)
				{
					return dataSet.Tables[0].Rows[0];
				}
				return null;
			}
		}

		// Token: 0x06000099 RID: 153 RVA: 0x00004E1C File Offset: 0x0000301C
		public DataSet ExecuteDataset(string spName, params object[] parameterValues)
		{
			if (this.Transaction != null)
			{
				return SqlHelper.ExecuteDataset(this.Transaction, spName, parameterValues);
			}
			return SqlHelper.ExecuteDataset(this.ConnectionString, spName, parameterValues);
		}

		// Token: 0x0600009A RID: 154 RVA: 0x00004E41 File Offset: 0x00003041
		public DataSet ExecuteDataset(CommandType commandType, string commandText, params SqlParameter[] commandParameters)
		{
			if (this.Transaction != null)
			{
				return SqlHelper.ExecuteDataset(this.Transaction, commandType, commandText, commandParameters);
			}
			return SqlHelper.ExecuteDataset(this.ConnectionString, commandType, commandText, commandParameters);
		}

		// Token: 0x0600009B RID: 155 RVA: 0x00004E68 File Offset: 0x00003068
		public DataSet ExecuteDataset(CommandType commandType, string commandText)
		{
			if (this.Transaction != null)
			{
				return SqlHelper.ExecuteDataset(this.Transaction, commandType, commandText);
			}
			return SqlHelper.ExecuteDataset(this.ConnectionString, commandType, commandText);
		}

		// Token: 0x0600009C RID: 156 RVA: 0x00004E8D File Offset: 0x0000308D
		public object ExecuteScalar(string spName, params object[] parameterValues)
		{
			if (this.Transaction != null)
			{
				return SqlHelper.ExecuteScalar(this.Transaction, spName, parameterValues);
			}
			return SqlHelper.ExecuteScalar(this.ConnectionString, spName, parameterValues);
		}

		// Token: 0x0600009D RID: 157 RVA: 0x00004EB2 File Offset: 0x000030B2
		public object ExecuteScalar(CommandType commandType, string commandText)
		{
			if (this.Transaction != null)
			{
				return SqlHelper.ExecuteScalar(this.Transaction, commandType, commandText);
			}
			return SqlHelper.ExecuteScalar(this.ConnectionString, commandType, commandText);
		}

		// Token: 0x0600009E RID: 158 RVA: 0x00004ED7 File Offset: 0x000030D7
		public object ExecuteScalar(CommandType commandType, string commandText, params SqlParameter[] commandParameters)
		{
			if (this.Transaction != null)
			{
				return SqlHelper.ExecuteScalar(this.Transaction, commandText, commandParameters);
			}
			return SqlHelper.ExecuteScalar(this.ConnectionString, commandText, commandParameters);
		}

		// Token: 0x0600009F RID: 159 RVA: 0x00004EFC File Offset: 0x000030FC
		public string GetDesc(string fldDesc, string tableName, string fldCode, string fldCodeValue)
		{
			string commandText = string.Concat(new string[]
			{
				"select ltrim(rtrim([",
				fldDesc,
				"])) from [",
				tableName,
				"] where [",
				fldCode,
				"]='",
				fldCodeValue,
				"'"
			});
			return Convert.ToString(this.ExecuteScalar(CommandType.Text, commandText));
		}

		// Token: 0x060000A0 RID: 160 RVA: 0x00004F5C File Offset: 0x0000315C
		public DataTable GetPagingData(string tables, string pk, string sort, int pageNumber, int pageSize, string fields, string filter, ref int totalPages)
		{
			DataTable result;
			try
			{
				if (!string.IsNullOrEmpty(Condition.ExperesionSection))
				{
					filter = filter + " AND " + Condition.ExperesionSection;
				}
				if (string.IsNullOrEmpty(sort))
				{
					sort = pk;
				}
				DataSet dataSet = this.ExecuteDataset("Paging_Cursor", new object[]
				{
					tables,
					sort,
					pageNumber,
					pageSize,
					fields,
					filter
				});
				totalPages = ((dataSet.Tables[1].Rows[0]["TotalPages"] is DBNull) ? 0 : Convert.ToInt32(dataSet.Tables[1].Rows[0]["TotalPages"]));
				result = dataSet.Tables[0];
			}
			catch
			{
				totalPages = 0;
				result = null;
			}
			return result;
		}

		// Token: 0x04000002 RID: 2
		private readonly WorkFlowsDatabase _parentDatabase;

		// Token: 0x02000035 RID: 53
		private enum SqlConnectionOwnership
		{
			// Token: 0x0400003F RID: 63
			Internal,
			// Token: 0x04000040 RID: 64
			External
		}
	}
}
