﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Xml;
using MiddleWare.KaraLib;


namespace Microsoft.ApplicationBlocks.Data
{
	// Token: 0x02000002 RID: 2
	public sealed class SqlHelper
	{
		// Token: 0x06000001 RID: 1 RVA: 0x00002050 File Offset: 0x00000250
		private SqlHelper()
		{
		}

		// Token: 0x06000002 RID: 2 RVA: 0x00002058 File Offset: 0x00000258
		private static void AttachParameters(SqlCommand command, SqlParameter[] commandParameters)
		{
			if (command == null)
			{
				throw new ArgumentNullException("command");
			}
			if (commandParameters != null)
			{
				for (int i = 0; i < commandParameters.Length; i++)
				{
					SqlParameter sqlParameter = commandParameters[i];
					if (sqlParameter != null)
					{
						if ((sqlParameter.Direction == ParameterDirection.InputOutput || sqlParameter.Direction == ParameterDirection.Input) && sqlParameter.Value == null)
						{
							sqlParameter.Value = DBNull.Value;
						}
						command.Parameters.Add(sqlParameter);
					}
				}
			}
		}

		// Token: 0x06000003 RID: 3 RVA: 0x000020C0 File Offset: 0x000002C0
		private static void AssignParameterValues(SqlParameter[] commandParameters, DataRow dataRow)
		{
			if (commandParameters == null || dataRow == null)
			{
				return;
			}
			int num = 0;
			for (int i = 0; i < commandParameters.Length; i++)
			{
				SqlParameter sqlParameter = commandParameters[i];
				if (sqlParameter.ParameterName == null || sqlParameter.ParameterName.Length <= 1)
				{
					throw new Exception(string.Format("Please provide a valid parameter name on the parameter #{0}, the ParameterName property has the following value: '{1}'.", num, sqlParameter.ParameterName));
				}
				if (dataRow.Table.Columns.IndexOf(sqlParameter.ParameterName.Substring(1)) != -1)
				{
					sqlParameter.Value = dataRow[sqlParameter.ParameterName.Substring(1)];
				}
				num++;
			}
		}

		// Token: 0x06000004 RID: 4 RVA: 0x00002158 File Offset: 0x00000358
		private static void AssignParameterValues(SqlParameter[] commandParameters, object[] parameterValues)
		{
			if (commandParameters == null || parameterValues == null)
			{
				return;
			}
			if (commandParameters.Length != parameterValues.Length)
			{
				throw new ArgumentException("Parameter count does not match Parameter Value count.");
			}
			int i = 0;
			int num = commandParameters.Length;
			while (i < num)
			{
				if (parameterValues[i] is IDbDataParameter)
				{
					IDbDataParameter dbDataParameter = (IDbDataParameter)parameterValues[i];
					if (dbDataParameter.Value == null)
					{
						commandParameters[i].Value = DBNull.Value;
					}
					else
					{
						commandParameters[i].Value = dbDataParameter.Value;
					}
				}
				else if (parameterValues[i] == null)
				{
					commandParameters[i].Value = DBNull.Value;
				}
				else
				{
					commandParameters[i].Value = parameterValues[i];
				}
				i++;
			}
		}

		// Token: 0x06000005 RID: 5 RVA: 0x000021E8 File Offset: 0x000003E8
		private static void PrepareCommand(SqlCommand command, SqlConnection connection, SqlTransaction transaction, CommandType commandType, string commandText, SqlParameter[] commandParameters, out bool mustCloseConnection)
		{
			if (command == null)
			{
				throw new ArgumentNullException("command");
			}
			if (commandText == null || commandText.Length == 0)
			{
				throw new ArgumentNullException("commandText");
			}
			if (connection.State != ConnectionState.Open)
			{
				mustCloseConnection = true;
				connection.Open();
			}
			else
			{
				mustCloseConnection = false;
			}
			command.Connection = connection;
			command.CommandText = commandText;
			if (transaction != null)
			{
				if (transaction.Connection == null)
				{
					throw new ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", "transaction");
				}
				command.Transaction = transaction;
			}
			command.CommandType = commandType;
			if (commandParameters != null)
			{
				SqlHelper.AttachParameters(command, commandParameters);
			}
		}

		// Token: 0x17000001 RID: 1
		// (get) Token: 0x06000006 RID: 6 RVA: 0x00002278 File Offset: 0x00000478
		public static SqlConnection ConStr
		{
			get
			{
				string text = null;
				try
				{
					text = ConfigurationManager.ConnectionStrings["connectionStringProviderSystem"].ConnectionString;
					if (!text.StartsWith("Data Source="))
					{
						text = UtilityManagers.Decrypt(text);
					}
				}
				catch
				{
				}
				if (text != null)
				{
					return new SqlConnection
					{
						ConnectionString = text
					};
				}
				return null;
			}
		}

		// Token: 0x06000007 RID: 7 RVA: 0x000022D8 File Offset: 0x000004D8
		public static int ExecuteNonQuery(CommandType commandType, string commandText)
		{
			return SqlHelper.ExecuteNonQuery(SqlHelper.ConStr, commandType, commandText, null);
		}

		// Token: 0x06000008 RID: 8 RVA: 0x000022E7 File Offset: 0x000004E7
		public static int ExecuteNonQuery(string connectionString, CommandType commandType, string commandText)
		{
			return SqlHelper.ExecuteNonQuery(connectionString, commandType, commandText, null);
		}

		// Token: 0x06000009 RID: 9 RVA: 0x000022F4 File Offset: 0x000004F4
		public static int ExecuteNonQuery(string connectionString, CommandType commandType, string commandText, params SqlParameter[] commandParameters)
		{
			if (connectionString == null || connectionString.Length == 0)
			{
				throw new ArgumentNullException("connectionString");
			}
			int result;
			using (SqlConnection sqlConnection = new SqlConnection(connectionString))
			{
				sqlConnection.Open();
				result = SqlHelper.ExecuteNonQuery(sqlConnection, commandType, commandText, commandParameters);
			}
			return result;
		}

		// Token: 0x0600000A RID: 10 RVA: 0x0000234C File Offset: 0x0000054C
		public static int ExecuteNonQuery(string connectionString, string spName, params object[] parameterValues)
		{
			if (connectionString == null || connectionString.Length == 0)
			{
				throw new ArgumentNullException("connectionString");
			}
			if (spName == null || spName.Length == 0)
			{
				throw new ArgumentNullException("spName");
			}
			if (parameterValues != null && parameterValues.Length != 0)
			{
				SqlParameter[] spParameterSet = SqlHelperParameterCache.GetSpParameterSet(connectionString, spName);
				SqlHelper.AssignParameterValues(spParameterSet, parameterValues);
				return SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure, spName, spParameterSet);
			}
			return SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure, spName);
		}

		// Token: 0x0600000B RID: 11 RVA: 0x000023AD File Offset: 0x000005AD
		public static int ExecuteNonQuery(SqlConnection connection, CommandType commandType, string commandText)
		{
			return SqlHelper.ExecuteNonQuery(connection, commandType, commandText, null);
		}

		// Token: 0x0600000C RID: 12 RVA: 0x000023B8 File Offset: 0x000005B8
		public static int ExecuteNonQuery(SqlConnection connection, CommandType commandType, string commandText, params SqlParameter[] commandParameters)
		{
			if (connection == null)
			{
				throw new ArgumentNullException("connection");
			}
			SqlCommand arg_15_0 = new SqlCommand();
			bool flag = false;
			SqlHelper.PrepareCommand(arg_15_0, connection, null, commandType, commandText, commandParameters, out flag);
			int result = arg_15_0.ExecuteNonQuery();
			arg_15_0.Parameters.Clear();
			if (flag)
			{
				connection.Close();
			}
			return result;
		}

		// Token: 0x0600000D RID: 13 RVA: 0x00002404 File Offset: 0x00000604
		public static int ExecuteNonQuery(SqlConnection connection, string spName, params object[] parameterValues)
		{
			if (connection == null)
			{
				throw new ArgumentNullException("connection");
			}
			if (spName == null || spName.Length == 0)
			{
				throw new ArgumentNullException("spName");
			}
			if (parameterValues != null && parameterValues.Length != 0)
			{
				SqlParameter[] spParameterSet = SqlHelperParameterCache.GetSpParameterSet(connection, spName);
				SqlHelper.AssignParameterValues(spParameterSet, parameterValues);
				return SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, spName, spParameterSet);
			}
			return SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, spName);
		}

		// Token: 0x0600000E RID: 14 RVA: 0x0000245D File Offset: 0x0000065D
		public static int ExecuteNonQuery(SqlTransaction transaction, CommandType commandType, string commandText)
		{
			return SqlHelper.ExecuteNonQuery(transaction, commandType, commandText, null);
		}

		// Token: 0x0600000F RID: 15 RVA: 0x00002468 File Offset: 0x00000668
		public static int ExecuteNonQuery(SqlTransaction transaction, CommandType commandType, string commandText, params SqlParameter[] commandParameters)
		{
			if (transaction == null)
			{
				throw new ArgumentNullException("transaction");
			}
			if (transaction != null && transaction.Connection == null)
			{
				throw new ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", "transaction");
			}
			SqlCommand arg_30_0 = new SqlCommand();
			bool flag = false;
			SqlHelper.PrepareCommand(arg_30_0, transaction.Connection, transaction, commandType, commandText, commandParameters, out flag);
			int result = arg_30_0.ExecuteNonQuery();
			arg_30_0.Parameters.Clear();
			return result;
		}

		// Token: 0x06000010 RID: 16 RVA: 0x000024CC File Offset: 0x000006CC
		public static int ExecuteNonQuery(SqlTransaction transaction, string spName, params object[] parameterValues)
		{
			if (transaction == null)
			{
				throw new ArgumentNullException("transaction");
			}
			if (transaction != null && transaction.Connection == null)
			{
				throw new ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", "transaction");
			}
			if (spName == null || spName.Length == 0)
			{
				throw new ArgumentNullException("spName");
			}
			if (parameterValues != null && parameterValues.Length != 0)
			{
				SqlParameter[] spParameterSet = SqlHelperParameterCache.GetSpParameterSet(transaction.Connection, spName);
				SqlHelper.AssignParameterValues(spParameterSet, parameterValues);
				return SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spName, spParameterSet);
			}
			return SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spName);
		}

		// Token: 0x06000011 RID: 17 RVA: 0x00002545 File Offset: 0x00000745
		public static DataSet ExecuteDataset(string connectionString, CommandType commandType, string commandText)
		{
			return SqlHelper.ExecuteDataset(connectionString, commandType, commandText, null);
		}

		// Token: 0x06000012 RID: 18 RVA: 0x00002550 File Offset: 0x00000750
		public static DataSet ExecuteDataset(string connectionString, CommandType commandType, string commandText, params SqlParameter[] commandParameters)
		{
			if (connectionString == null || connectionString.Length == 0)
			{
				throw new ArgumentNullException("connectionString");
			}
			DataSet result;
			using (SqlConnection sqlConnection = new SqlConnection(connectionString))
			{
				sqlConnection.Open();
				result = SqlHelper.ExecuteDataset(sqlConnection, commandType, commandText, commandParameters);
			}
			return result;
		}

		// Token: 0x06000013 RID: 19 RVA: 0x000025A8 File Offset: 0x000007A8
		public static DataSet ExecuteDataset(string connectionString, string spName, params object[] parameterValues)
		{
			if (connectionString == null || connectionString.Length == 0)
			{
				throw new ArgumentNullException("connectionString");
			}
			if (spName == null || spName.Length == 0)
			{
				throw new ArgumentNullException("spName");
			}
			if (parameterValues != null && parameterValues.Length != 0)
			{
				SqlParameter[] spParameterSet = SqlHelperParameterCache.GetSpParameterSet(connectionString, spName);
				SqlHelper.AssignParameterValues(spParameterSet, parameterValues);
				return SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, spName, spParameterSet);
			}
			return SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, spName);
		}

		// Token: 0x06000014 RID: 20 RVA: 0x00002609 File Offset: 0x00000809
		public static DataSet ExecuteDataset(SqlConnection connection, CommandType commandType, string commandText)
		{
			return SqlHelper.ExecuteDataset(connection, commandType, commandText, null);
		}

		// Token: 0x06000015 RID: 21 RVA: 0x00002614 File Offset: 0x00000814
		public static DataSet ExecuteDataset(SqlConnection connection, CommandType commandType, string commandText, params SqlParameter[] commandParameters)
		{
			if (connection == null)
			{
				throw new ArgumentNullException("connection");
			}
			SqlCommand sqlCommand = new SqlCommand();
			bool flag = false;
			SqlHelper.PrepareCommand(sqlCommand, connection, null, commandType, commandText, commandParameters, out flag);
			DataSet result;
			using (SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand))
			{
				DataSet dataSet = new DataSet();
				sqlDataAdapter.Fill(dataSet);
				sqlCommand.Parameters.Clear();
				if (flag)
				{
					connection.Close();
				}
				result = dataSet;
			}
			return result;
		}

		// Token: 0x06000016 RID: 22 RVA: 0x00002690 File Offset: 0x00000890
		public static DataSet ExecuteDataset(SqlConnection connection, string spName, params object[] parameterValues)
		{
			if (connection == null)
			{
				throw new ArgumentNullException("connection");
			}
			if (spName == null || spName.Length == 0)
			{
				throw new ArgumentNullException("spName");
			}
			if (parameterValues != null && parameterValues.Length != 0)
			{
				SqlParameter[] spParameterSet = SqlHelperParameterCache.GetSpParameterSet(connection, spName);
				SqlHelper.AssignParameterValues(spParameterSet, parameterValues);
				return SqlHelper.ExecuteDataset(connection, CommandType.StoredProcedure, spName, spParameterSet);
			}
			return SqlHelper.ExecuteDataset(connection, CommandType.StoredProcedure, spName);
		}

		// Token: 0x06000017 RID: 23 RVA: 0x000026E9 File Offset: 0x000008E9
		public static DataSet ExecuteDataset(SqlTransaction transaction, CommandType commandType, string commandText)
		{
			return SqlHelper.ExecuteDataset(transaction, commandType, commandText, null);
		}

		// Token: 0x06000018 RID: 24 RVA: 0x000026F4 File Offset: 0x000008F4
		public static DataSet ExecuteDataset(SqlTransaction transaction, CommandType commandType, string commandText, params SqlParameter[] commandParameters)
		{
			if (transaction == null)
			{
				throw new ArgumentNullException("transaction");
			}
			if (transaction != null && transaction.Connection == null)
			{
				throw new ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", "transaction");
			}
			SqlCommand sqlCommand = new SqlCommand();
			bool flag = false;
			SqlHelper.PrepareCommand(sqlCommand, transaction.Connection, transaction, commandType, commandText, commandParameters, out flag);
			DataSet result;
			using (SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand))
			{
				DataSet dataSet = new DataSet();
				sqlDataAdapter.Fill(dataSet);
				sqlCommand.Parameters.Clear();
				result = dataSet;
			}
			return result;
		}

		// Token: 0x06000019 RID: 25 RVA: 0x00002788 File Offset: 0x00000988
		public static DataSet ExecuteDataset(SqlTransaction transaction, string spName, params object[] parameterValues)
		{
			if (transaction == null)
			{
				throw new ArgumentNullException("transaction");
			}
			if (transaction != null && transaction.Connection == null)
			{
				throw new ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", "transaction");
			}
			if (spName == null || spName.Length == 0)
			{
				throw new ArgumentNullException("spName");
			}
			if (parameterValues != null && parameterValues.Length != 0)
			{
				SqlParameter[] spParameterSet = SqlHelperParameterCache.GetSpParameterSet(transaction.Connection, spName);
				SqlHelper.AssignParameterValues(spParameterSet, parameterValues);
				return SqlHelper.ExecuteDataset(transaction, CommandType.StoredProcedure, spName, spParameterSet);
			}
			return SqlHelper.ExecuteDataset(transaction, CommandType.StoredProcedure, spName);
		}

		// Token: 0x0600001A RID: 26 RVA: 0x00002804 File Offset: 0x00000A04
		private static SqlDataReader ExecuteReader(SqlConnection connection, SqlTransaction transaction, CommandType commandType, string commandText, SqlParameter[] commandParameters, SqlHelper.SqlConnectionOwnership connectionOwnership)
		{
			if (connection == null)
			{
				throw new ArgumentNullException("connection");
			}
			bool flag = false;
			SqlCommand sqlCommand = new SqlCommand();
			SqlDataReader result;
			try
			{
				SqlHelper.PrepareCommand(sqlCommand, connection, transaction, commandType, commandText, commandParameters, out flag);
				SqlDataReader sqlDataReader;
				if (connectionOwnership == SqlHelper.SqlConnectionOwnership.External)
				{
					sqlDataReader = sqlCommand.ExecuteReader();
				}
				else
				{
					sqlDataReader = sqlCommand.ExecuteReader(CommandBehavior.CloseConnection);
				}
				bool flag2 = true;
			    IEnumerator enumerator = sqlCommand.Parameters.GetEnumerator();
				{
					while (enumerator.MoveNext())
					{
						if (((SqlParameter)enumerator.Current).Direction != ParameterDirection.Input)
						{
							flag2 = false;
						}
					}
				}
				if (flag2)
				{
					sqlCommand.Parameters.Clear();
				}
				result = sqlDataReader;
			}
			catch
			{
				if (flag)
				{
					connection.Close();
				}
				throw;
			}
			return result;
		}

		// Token: 0x0600001B RID: 27 RVA: 0x000028D0 File Offset: 0x00000AD0
		public static SqlDataReader ExecuteReader(string connectionString, CommandType commandType, string commandText)
		{
			return SqlHelper.ExecuteReader(connectionString, commandType, commandText, null);
		}

		// Token: 0x0600001C RID: 28 RVA: 0x000028DC File Offset: 0x00000ADC
		public static SqlDataReader ExecuteReader(string connectionString, CommandType commandType, string commandText, params SqlParameter[] commandParameters)
		{
			if (connectionString == null || connectionString.Length == 0)
			{
				throw new ArgumentNullException("connectionString");
			}
			SqlConnection sqlConnection = null;
			SqlDataReader result;
			try
			{
				sqlConnection = new SqlConnection(connectionString);
				sqlConnection.Open();
				result = SqlHelper.ExecuteReader(sqlConnection, null, commandType, commandText, commandParameters, SqlHelper.SqlConnectionOwnership.Internal);
			}
			catch
			{
				if (sqlConnection != null)
				{
					sqlConnection.Close();
				}
				throw;
			}
			return result;
		}

		// Token: 0x0600001D RID: 29 RVA: 0x0000293C File Offset: 0x00000B3C
		public static SqlDataReader ExecuteReader(string connectionString, string spName, params object[] parameterValues)
		{
			if (connectionString == null || connectionString.Length == 0)
			{
				throw new ArgumentNullException("connectionString");
			}
			if (spName == null || spName.Length == 0)
			{
				throw new ArgumentNullException("spName");
			}
			if (parameterValues != null && parameterValues.Length != 0)
			{
				SqlParameter[] spParameterSet = SqlHelperParameterCache.GetSpParameterSet(connectionString, spName);
				SqlHelper.AssignParameterValues(spParameterSet, parameterValues);
				return SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, spName, spParameterSet);
			}
			return SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, spName);
		}

		// Token: 0x0600001E RID: 30 RVA: 0x0000299D File Offset: 0x00000B9D
		public static SqlDataReader ExecuteReader(SqlConnection connection, CommandType commandType, string commandText)
		{
			return SqlHelper.ExecuteReader(connection, commandType, commandText, null);
		}

		// Token: 0x0600001F RID: 31 RVA: 0x000029A8 File Offset: 0x00000BA8
		public static SqlDataReader ExecuteReader(SqlConnection connection, CommandType commandType, string commandText, params SqlParameter[] commandParameters)
		{
			return SqlHelper.ExecuteReader(connection, null, commandType, commandText, commandParameters, SqlHelper.SqlConnectionOwnership.External);
		}

		// Token: 0x06000020 RID: 32 RVA: 0x000029B8 File Offset: 0x00000BB8
		public static SqlDataReader ExecuteReader(SqlConnection connection, string spName, params object[] parameterValues)
		{
			if (connection == null)
			{
				throw new ArgumentNullException("connection");
			}
			if (spName == null || spName.Length == 0)
			{
				throw new ArgumentNullException("spName");
			}
			if (parameterValues != null && parameterValues.Length != 0)
			{
				SqlParameter[] spParameterSet = SqlHelperParameterCache.GetSpParameterSet(connection, spName);
				SqlHelper.AssignParameterValues(spParameterSet, parameterValues);
				return SqlHelper.ExecuteReader(connection, CommandType.StoredProcedure, spName, spParameterSet);
			}
			return SqlHelper.ExecuteReader(connection, CommandType.StoredProcedure, spName);
		}

		// Token: 0x06000021 RID: 33 RVA: 0x00002A11 File Offset: 0x00000C11
		public static SqlDataReader ExecuteReader(SqlTransaction transaction, CommandType commandType, string commandText)
		{
			return SqlHelper.ExecuteReader(transaction, commandType, commandText, null);
		}

		// Token: 0x06000022 RID: 34 RVA: 0x00002A1C File Offset: 0x00000C1C
		public static SqlDataReader ExecuteReader(SqlTransaction transaction, CommandType commandType, string commandText, params SqlParameter[] commandParameters)
		{
			if (transaction == null)
			{
				throw new ArgumentNullException("transaction");
			}
			if (transaction != null && transaction.Connection == null)
			{
				throw new ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", "transaction");
			}
			return SqlHelper.ExecuteReader(transaction.Connection, transaction, commandType, commandText, commandParameters, SqlHelper.SqlConnectionOwnership.External);
		}

		// Token: 0x06000023 RID: 35 RVA: 0x00002A58 File Offset: 0x00000C58
		public static SqlDataReader ExecuteReader(SqlTransaction transaction, string spName, params object[] parameterValues)
		{
			if (transaction == null)
			{
				throw new ArgumentNullException("transaction");
			}
			if (transaction != null && transaction.Connection == null)
			{
				throw new ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", "transaction");
			}
			if (spName == null || spName.Length == 0)
			{
				throw new ArgumentNullException("spName");
			}
			if (parameterValues != null && parameterValues.Length != 0)
			{
				SqlParameter[] spParameterSet = SqlHelperParameterCache.GetSpParameterSet(transaction.Connection, spName);
				SqlHelper.AssignParameterValues(spParameterSet, parameterValues);
				return SqlHelper.ExecuteReader(transaction, CommandType.StoredProcedure, spName, spParameterSet);
			}
			return SqlHelper.ExecuteReader(transaction, CommandType.StoredProcedure, spName);
		}

		// Token: 0x06000024 RID: 36 RVA: 0x00002AD1 File Offset: 0x00000CD1
		public static object ExecuteScalar(string connectionString, CommandType commandType, string commandText)
		{
			return SqlHelper.ExecuteScalar(connectionString, commandType, commandText, null);
		}

		// Token: 0x06000025 RID: 37 RVA: 0x00002ADC File Offset: 0x00000CDC
		public static object ExecuteScalar(string connectionString, CommandType commandType, string commandText, params SqlParameter[] commandParameters)
		{
			if (connectionString == null || connectionString.Length == 0)
			{
				throw new ArgumentNullException("connectionString");
			}
			object result;
			using (SqlConnection sqlConnection = new SqlConnection(connectionString))
			{
				sqlConnection.Open();
				result = SqlHelper.ExecuteScalar(sqlConnection, commandType, commandText, commandParameters);
			}
			return result;
		}

		// Token: 0x06000026 RID: 38 RVA: 0x00002B34 File Offset: 0x00000D34
		public static object ExecuteScalar(string connectionString, string spName, params object[] parameterValues)
		{
			if (connectionString == null || connectionString.Length == 0)
			{
				throw new ArgumentNullException("connectionString");
			}
			if (spName == null || spName.Length == 0)
			{
				throw new ArgumentNullException("spName");
			}
			if (parameterValues != null && parameterValues.Length != 0)
			{
				SqlParameter[] spParameterSet = SqlHelperParameterCache.GetSpParameterSet(connectionString, spName);
				SqlHelper.AssignParameterValues(spParameterSet, parameterValues);
				return SqlHelper.ExecuteScalar(connectionString, CommandType.StoredProcedure, spName, spParameterSet);
			}
			return SqlHelper.ExecuteScalar(connectionString, CommandType.StoredProcedure, spName);
		}

		// Token: 0x06000027 RID: 39 RVA: 0x00002B95 File Offset: 0x00000D95
		public static object ExecuteScalar(SqlConnection connection, CommandType commandType, string commandText)
		{
			return SqlHelper.ExecuteScalar(connection, commandType, commandText, null);
		}

		// Token: 0x06000028 RID: 40 RVA: 0x00002BA0 File Offset: 0x00000DA0
		public static object ExecuteScalar(SqlConnection connection, CommandType commandType, string commandText, params SqlParameter[] commandParameters)
		{
			if (connection == null)
			{
				throw new ArgumentNullException("connection");
			}
			SqlCommand arg_15_0 = new SqlCommand();
			bool flag = false;
			SqlHelper.PrepareCommand(arg_15_0, connection, null, commandType, commandText, commandParameters, out flag);
			object result = arg_15_0.ExecuteScalar();
			arg_15_0.Parameters.Clear();
			if (flag)
			{
				connection.Close();
			}
			return result;
		}

		// Token: 0x06000029 RID: 41 RVA: 0x00002BEC File Offset: 0x00000DEC
		public static object ExecuteScalar(SqlConnection connection, string spName, params object[] parameterValues)
		{
			if (connection == null)
			{
				throw new ArgumentNullException("connection");
			}
			if (spName == null || spName.Length == 0)
			{
				throw new ArgumentNullException("spName");
			}
			if (parameterValues != null && parameterValues.Length != 0)
			{
				SqlParameter[] spParameterSet = SqlHelperParameterCache.GetSpParameterSet(connection, spName);
				SqlHelper.AssignParameterValues(spParameterSet, parameterValues);
				return SqlHelper.ExecuteScalar(connection, CommandType.StoredProcedure, spName, spParameterSet);
			}
			return SqlHelper.ExecuteScalar(connection, CommandType.StoredProcedure, spName);
		}

		// Token: 0x0600002A RID: 42 RVA: 0x00002C45 File Offset: 0x00000E45
		public static object ExecuteScalar(SqlTransaction transaction, CommandType commandType, string commandText)
		{
			return SqlHelper.ExecuteScalar(transaction, commandType, commandText, null);
		}

		// Token: 0x0600002B RID: 43 RVA: 0x00002C50 File Offset: 0x00000E50
		public static object ExecuteScalar(SqlTransaction transaction, CommandType commandType, string commandText, params SqlParameter[] commandParameters)
		{
			if (transaction == null)
			{
				throw new ArgumentNullException("transaction");
			}
			if (transaction != null && transaction.Connection == null)
			{
				throw new ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", "transaction");
			}
			SqlCommand arg_30_0 = new SqlCommand();
			bool flag = false;
			SqlHelper.PrepareCommand(arg_30_0, transaction.Connection, transaction, commandType, commandText, commandParameters, out flag);
			object result = arg_30_0.ExecuteScalar();
			arg_30_0.Parameters.Clear();
			return result;
		}

		// Token: 0x0600002C RID: 44 RVA: 0x00002CB4 File Offset: 0x00000EB4
		public static object ExecuteScalar(SqlTransaction transaction, string spName, params object[] parameterValues)
		{
			if (transaction == null)
			{
				throw new ArgumentNullException("transaction");
			}
			if (transaction != null && transaction.Connection == null)
			{
				throw new ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", "transaction");
			}
			if (spName == null || spName.Length == 0)
			{
				throw new ArgumentNullException("spName");
			}
			if (parameterValues != null && parameterValues.Length != 0)
			{
				SqlParameter[] spParameterSet = SqlHelperParameterCache.GetSpParameterSet(transaction.Connection, spName);
				SqlHelper.AssignParameterValues(spParameterSet, parameterValues);
				return SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, spName, spParameterSet);
			}
			return SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, spName);
		}

		// Token: 0x0600002D RID: 45 RVA: 0x00002D2D File Offset: 0x00000F2D
		public static XmlReader ExecuteXmlReader(SqlConnection connection, CommandType commandType, string commandText)
		{
			return SqlHelper.ExecuteXmlReader(connection, commandType, commandText, null);
		}

		// Token: 0x0600002E RID: 46 RVA: 0x00002D38 File Offset: 0x00000F38
		public static XmlReader ExecuteXmlReader(SqlConnection connection, CommandType commandType, string commandText, params SqlParameter[] commandParameters)
		{
			if (connection == null)
			{
				throw new ArgumentNullException("connection");
			}
			bool flag = false;
			SqlCommand sqlCommand = new SqlCommand();
			XmlReader result;
			try
			{
				SqlHelper.PrepareCommand(sqlCommand, connection, null, commandType, commandText, commandParameters, out flag);
				XmlReader xmlReader = sqlCommand.ExecuteXmlReader();
				sqlCommand.Parameters.Clear();
				result = xmlReader;
			}
			catch
			{
				if (flag)
				{
					connection.Close();
				}
				throw;
			}
			return result;
		}

		// Token: 0x0600002F RID: 47 RVA: 0x00002D9C File Offset: 0x00000F9C
		public static XmlReader ExecuteXmlReader(SqlConnection connection, string spName, params object[] parameterValues)
		{
			if (connection == null)
			{
				throw new ArgumentNullException("connection");
			}
			if (spName == null || spName.Length == 0)
			{
				throw new ArgumentNullException("spName");
			}
			if (parameterValues != null && parameterValues.Length != 0)
			{
				SqlParameter[] spParameterSet = SqlHelperParameterCache.GetSpParameterSet(connection, spName);
				SqlHelper.AssignParameterValues(spParameterSet, parameterValues);
				return SqlHelper.ExecuteXmlReader(connection, CommandType.StoredProcedure, spName, spParameterSet);
			}
			return SqlHelper.ExecuteXmlReader(connection, CommandType.StoredProcedure, spName);
		}

		// Token: 0x06000030 RID: 48 RVA: 0x00002DF5 File Offset: 0x00000FF5
		public static XmlReader ExecuteXmlReader(SqlTransaction transaction, CommandType commandType, string commandText)
		{
			return SqlHelper.ExecuteXmlReader(transaction, commandType, commandText, null);
		}

		// Token: 0x06000031 RID: 49 RVA: 0x00002E00 File Offset: 0x00001000
		public static XmlReader ExecuteXmlReader(SqlTransaction transaction, CommandType commandType, string commandText, params SqlParameter[] commandParameters)
		{
			if (transaction == null)
			{
				throw new ArgumentNullException("transaction");
			}
			if (transaction != null && transaction.Connection == null)
			{
				throw new ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", "transaction");
			}
			SqlCommand arg_30_0 = new SqlCommand();
			bool flag = false;
			SqlHelper.PrepareCommand(arg_30_0, transaction.Connection, transaction, commandType, commandText, commandParameters, out flag);
			XmlReader result = arg_30_0.ExecuteXmlReader();
			arg_30_0.Parameters.Clear();
			return result;
		}

		// Token: 0x06000032 RID: 50 RVA: 0x00002E64 File Offset: 0x00001064
		public static XmlReader ExecuteXmlReader(SqlTransaction transaction, string spName, params object[] parameterValues)
		{
			if (transaction == null)
			{
				throw new ArgumentNullException("transaction");
			}
			if (transaction != null && transaction.Connection == null)
			{
				throw new ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", "transaction");
			}
			if (spName == null || spName.Length == 0)
			{
				throw new ArgumentNullException("spName");
			}
			if (parameterValues != null && parameterValues.Length != 0)
			{
				SqlParameter[] spParameterSet = SqlHelperParameterCache.GetSpParameterSet(transaction.Connection, spName);
				SqlHelper.AssignParameterValues(spParameterSet, parameterValues);
				return SqlHelper.ExecuteXmlReader(transaction, CommandType.StoredProcedure, spName, spParameterSet);
			}
			return SqlHelper.ExecuteXmlReader(transaction, CommandType.StoredProcedure, spName);
		}

		// Token: 0x06000033 RID: 51 RVA: 0x00002EE0 File Offset: 0x000010E0
		public static void FillDataset(string connectionString, CommandType commandType, string commandText, DataSet dataSet, string[] tableNames)
		{
			if (connectionString == null || connectionString.Length == 0)
			{
				throw new ArgumentNullException("connectionString");
			}
			if (dataSet == null)
			{
				throw new ArgumentNullException("dataSet");
			}
			using (SqlConnection sqlConnection = new SqlConnection(connectionString))
			{
				sqlConnection.Open();
				SqlHelper.FillDataset(sqlConnection, commandType, commandText, dataSet, tableNames);
			}
		}

		// Token: 0x06000034 RID: 52 RVA: 0x00002F48 File Offset: 0x00001148
		public static void FillDataset(string connectionString, CommandType commandType, string commandText, DataSet dataSet, string[] tableNames, params SqlParameter[] commandParameters)
		{
			if (connectionString == null || connectionString.Length == 0)
			{
				throw new ArgumentNullException("connectionString");
			}
			if (dataSet == null)
			{
				throw new ArgumentNullException("dataSet");
			}
			using (SqlConnection sqlConnection = new SqlConnection(connectionString))
			{
				sqlConnection.Open();
				SqlHelper.FillDataset(sqlConnection, commandType, commandText, dataSet, tableNames, commandParameters);
			}
		}

		// Token: 0x06000035 RID: 53 RVA: 0x00002FB0 File Offset: 0x000011B0
		public static void FillDataset(string connectionString, string spName, DataSet dataSet, string[] tableNames, params object[] parameterValues)
		{
			if (connectionString == null || connectionString.Length == 0)
			{
				throw new ArgumentNullException("connectionString");
			}
			if (dataSet == null)
			{
				throw new ArgumentNullException("dataSet");
			}
			using (SqlConnection sqlConnection = new SqlConnection(connectionString))
			{
				sqlConnection.Open();
				SqlHelper.FillDataset(sqlConnection, spName, dataSet, tableNames, parameterValues);
			}
		}

		// Token: 0x06000036 RID: 54 RVA: 0x00003018 File Offset: 0x00001218
		public static void FillDataset(SqlConnection connection, CommandType commandType, string commandText, DataSet dataSet, string[] tableNames)
		{
			SqlHelper.FillDataset(connection, commandType, commandText, dataSet, tableNames, null);
		}

		// Token: 0x06000037 RID: 55 RVA: 0x00003026 File Offset: 0x00001226
		public static void FillDataset(SqlConnection connection, CommandType commandType, string commandText, DataSet dataSet, string[] tableNames, params SqlParameter[] commandParameters)
		{
			SqlHelper.FillDataset(connection, null, commandType, commandText, dataSet, tableNames, commandParameters);
		}

		// Token: 0x06000038 RID: 56 RVA: 0x00003038 File Offset: 0x00001238
		public static void FillDataset(SqlConnection connection, string spName, DataSet dataSet, string[] tableNames, params object[] parameterValues)
		{
			if (connection == null)
			{
				throw new ArgumentNullException("connection");
			}
			if (dataSet == null)
			{
				throw new ArgumentNullException("dataSet");
			}
			if (spName == null || spName.Length == 0)
			{
				throw new ArgumentNullException("spName");
			}
			if (parameterValues != null && parameterValues.Length != 0)
			{
				SqlParameter[] spParameterSet = SqlHelperParameterCache.GetSpParameterSet(connection, spName);
				SqlHelper.AssignParameterValues(spParameterSet, parameterValues);
				SqlHelper.FillDataset(connection, CommandType.StoredProcedure, spName, dataSet, tableNames, spParameterSet);
				return;
			}
			SqlHelper.FillDataset(connection, CommandType.StoredProcedure, spName, dataSet, tableNames);
		}

		// Token: 0x06000039 RID: 57 RVA: 0x000030A6 File Offset: 0x000012A6
		public static void FillDataset(SqlTransaction transaction, CommandType commandType, string commandText, DataSet dataSet, string[] tableNames)
		{
			SqlHelper.FillDataset(transaction, commandType, commandText, dataSet, tableNames, null);
		}

		// Token: 0x0600003A RID: 58 RVA: 0x000030B4 File Offset: 0x000012B4
		public static void FillDataset(SqlTransaction transaction, CommandType commandType, string commandText, DataSet dataSet, string[] tableNames, params SqlParameter[] commandParameters)
		{
			SqlHelper.FillDataset(transaction.Connection, transaction, commandType, commandText, dataSet, tableNames, commandParameters);
		}

		// Token: 0x0600003B RID: 59 RVA: 0x000030CC File Offset: 0x000012CC
		public static void FillDataset(SqlTransaction transaction, string spName, DataSet dataSet, string[] tableNames, params object[] parameterValues)
		{
			if (transaction == null)
			{
				throw new ArgumentNullException("transaction");
			}
			if (transaction != null && transaction.Connection == null)
			{
				throw new ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", "transaction");
			}
			if (dataSet == null)
			{
				throw new ArgumentNullException("dataSet");
			}
			if (spName == null || spName.Length == 0)
			{
				throw new ArgumentNullException("spName");
			}
			if (parameterValues != null && parameterValues.Length != 0)
			{
				SqlParameter[] spParameterSet = SqlHelperParameterCache.GetSpParameterSet(transaction.Connection, spName);
				SqlHelper.AssignParameterValues(spParameterSet, parameterValues);
				SqlHelper.FillDataset(transaction, CommandType.StoredProcedure, spName, dataSet, tableNames, spParameterSet);
				return;
			}
			SqlHelper.FillDataset(transaction, CommandType.StoredProcedure, spName, dataSet, tableNames);
		}

		// Token: 0x0600003C RID: 60 RVA: 0x0000315C File Offset: 0x0000135C
		private static void FillDataset(SqlConnection connection, SqlTransaction transaction, CommandType commandType, string commandText, DataSet dataSet, string[] tableNames, params SqlParameter[] commandParameters)
		{
			if (connection == null)
			{
				throw new ArgumentNullException("connection");
			}
			if (dataSet == null)
			{
				throw new ArgumentNullException("dataSet");
			}
			SqlCommand sqlCommand = new SqlCommand();
			bool flag = false;
			SqlHelper.PrepareCommand(sqlCommand, connection, transaction, commandType, commandText, commandParameters, out flag);
			using (SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand))
			{
				if (tableNames != null && tableNames.Length != 0)
				{
					string text = "Table";
					for (int i = 0; i < tableNames.Length; i++)
					{
						if (tableNames[i] == null || tableNames[i].Length == 0)
						{
							throw new ArgumentException("The tableNames parameter must contain a list of tables, a value was provided as null or empty string.", "tableNames");
						}
						sqlDataAdapter.TableMappings.Add(text, tableNames[i]);
						text += (i + 1).ToString();
					}
				}
				sqlDataAdapter.Fill(dataSet);
				sqlCommand.Parameters.Clear();
			}
			if (flag)
			{
				connection.Close();
			}
		}

		// Token: 0x0600003D RID: 61 RVA: 0x00003248 File Offset: 0x00001448
		public static void UpdateDataset(SqlCommand insertCommand, SqlCommand deleteCommand, SqlCommand updateCommand, DataSet dataSet, string tableName)
		{
			if (insertCommand == null)
			{
				throw new ArgumentNullException("insertCommand");
			}
			if (deleteCommand == null)
			{
				throw new ArgumentNullException("deleteCommand");
			}
			if (updateCommand == null)
			{
				throw new ArgumentNullException("updateCommand");
			}
			if (tableName == null || tableName.Length == 0)
			{
				throw new ArgumentNullException("tableName");
			}
			using (SqlDataAdapter sqlDataAdapter = new SqlDataAdapter())
			{
				sqlDataAdapter.UpdateCommand = updateCommand;
				sqlDataAdapter.InsertCommand = insertCommand;
				sqlDataAdapter.DeleteCommand = deleteCommand;
				sqlDataAdapter.Update(dataSet, tableName);
				dataSet.AcceptChanges();
			}
		}

		// Token: 0x0600003E RID: 62 RVA: 0x000032E0 File Offset: 0x000014E0
		public static SqlCommand CreateCommand(SqlConnection connection, string spName, params string[] sourceColumns)
		{
			if (connection == null)
			{
				throw new ArgumentNullException("connection");
			}
			if (spName == null || spName.Length == 0)
			{
				throw new ArgumentNullException("spName");
			}
			SqlCommand sqlCommand = new SqlCommand(spName, connection);
			sqlCommand.CommandType = CommandType.StoredProcedure;
			if (sourceColumns != null && sourceColumns.Length != 0)
			{
				SqlParameter[] spParameterSet = SqlHelperParameterCache.GetSpParameterSet(connection, spName);
				for (int i = 0; i < sourceColumns.Length; i++)
				{
					spParameterSet[i].SourceColumn = sourceColumns[i];
				}
				SqlHelper.AttachParameters(sqlCommand, spParameterSet);
			}
			return sqlCommand;
		}

		// Token: 0x0600003F RID: 63 RVA: 0x00003350 File Offset: 0x00001550
		public static int ExecuteNonQueryTypedParams(string connectionString, string spName, DataRow dataRow)
		{
			if (connectionString == null || connectionString.Length == 0)
			{
				throw new ArgumentNullException("connectionString");
			}
			if (spName == null || spName.Length == 0)
			{
				throw new ArgumentNullException("spName");
			}
			if (dataRow != null && dataRow.ItemArray.Length != 0)
			{
				SqlParameter[] spParameterSet = SqlHelperParameterCache.GetSpParameterSet(connectionString, spName);
				SqlHelper.AssignParameterValues(spParameterSet, dataRow);
				return SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure, spName, spParameterSet);
			}
			return SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure, spName);
		}

		// Token: 0x06000040 RID: 64 RVA: 0x000033B8 File Offset: 0x000015B8
		public static int ExecuteNonQueryTypedParams(SqlConnection connection, string spName, DataRow dataRow)
		{
			if (connection == null)
			{
				throw new ArgumentNullException("connection");
			}
			if (spName == null || spName.Length == 0)
			{
				throw new ArgumentNullException("spName");
			}
			if (dataRow != null && dataRow.ItemArray.Length != 0)
			{
				SqlParameter[] spParameterSet = SqlHelperParameterCache.GetSpParameterSet(connection, spName);
				SqlHelper.AssignParameterValues(spParameterSet, dataRow);
				return SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, spName, spParameterSet);
			}
			return SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, spName);
		}

		// Token: 0x06000041 RID: 65 RVA: 0x00003418 File Offset: 0x00001618
		public static int ExecuteNonQueryTypedParams(SqlTransaction transaction, string spName, DataRow dataRow)
		{
			if (transaction == null)
			{
				throw new ArgumentNullException("transaction");
			}
			if (transaction != null && transaction.Connection == null)
			{
				throw new ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", "transaction");
			}
			if (spName == null || spName.Length == 0)
			{
				throw new ArgumentNullException("spName");
			}
			if (dataRow != null && dataRow.ItemArray.Length != 0)
			{
				SqlParameter[] spParameterSet = SqlHelperParameterCache.GetSpParameterSet(transaction.Connection, spName);
				SqlHelper.AssignParameterValues(spParameterSet, dataRow);
				return SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spName, spParameterSet);
			}
			return SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spName);
		}

		// Token: 0x06000042 RID: 66 RVA: 0x00003498 File Offset: 0x00001698
		public static DataSet ExecuteDatasetTypedParams(string connectionString, string spName, DataRow dataRow)
		{
			if (connectionString == null || connectionString.Length == 0)
			{
				throw new ArgumentNullException("connectionString");
			}
			if (spName == null || spName.Length == 0)
			{
				throw new ArgumentNullException("spName");
			}
			if (dataRow != null && dataRow.ItemArray.Length != 0)
			{
				SqlParameter[] spParameterSet = SqlHelperParameterCache.GetSpParameterSet(connectionString, spName);
				SqlHelper.AssignParameterValues(spParameterSet, dataRow);
				return SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, spName, spParameterSet);
			}
			return SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, spName);
		}

		// Token: 0x06000043 RID: 67 RVA: 0x00003500 File Offset: 0x00001700
		public static DataSet ExecuteDatasetTypedParams(SqlConnection connection, string spName, DataRow dataRow)
		{
			if (connection == null)
			{
				throw new ArgumentNullException("connection");
			}
			if (spName == null || spName.Length == 0)
			{
				throw new ArgumentNullException("spName");
			}
			if (dataRow != null && dataRow.ItemArray.Length != 0)
			{
				SqlParameter[] spParameterSet = SqlHelperParameterCache.GetSpParameterSet(connection, spName);
				SqlHelper.AssignParameterValues(spParameterSet, dataRow);
				return SqlHelper.ExecuteDataset(connection, CommandType.StoredProcedure, spName, spParameterSet);
			}
			return SqlHelper.ExecuteDataset(connection, CommandType.StoredProcedure, spName);
		}

		// Token: 0x06000044 RID: 68 RVA: 0x00003560 File Offset: 0x00001760
		public static DataSet ExecuteDatasetTypedParams(SqlTransaction transaction, string spName, DataRow dataRow)
		{
			if (transaction == null)
			{
				throw new ArgumentNullException("transaction");
			}
			if (transaction != null && transaction.Connection == null)
			{
				throw new ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", "transaction");
			}
			if (spName == null || spName.Length == 0)
			{
				throw new ArgumentNullException("spName");
			}
			if (dataRow != null && dataRow.ItemArray.Length != 0)
			{
				SqlParameter[] spParameterSet = SqlHelperParameterCache.GetSpParameterSet(transaction.Connection, spName);
				SqlHelper.AssignParameterValues(spParameterSet, dataRow);
				return SqlHelper.ExecuteDataset(transaction, CommandType.StoredProcedure, spName, spParameterSet);
			}
			return SqlHelper.ExecuteDataset(transaction, CommandType.StoredProcedure, spName);
		}

		// Token: 0x06000045 RID: 69 RVA: 0x000035E0 File Offset: 0x000017E0
		public static SqlDataReader ExecuteReaderTypedParams(string connectionString, string spName, DataRow dataRow)
		{
			if (connectionString == null || connectionString.Length == 0)
			{
				throw new ArgumentNullException("connectionString");
			}
			if (spName == null || spName.Length == 0)
			{
				throw new ArgumentNullException("spName");
			}
			if (dataRow != null && dataRow.ItemArray.Length != 0)
			{
				SqlParameter[] spParameterSet = SqlHelperParameterCache.GetSpParameterSet(connectionString, spName);
				SqlHelper.AssignParameterValues(spParameterSet, dataRow);
				return SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, spName, spParameterSet);
			}
			return SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, spName);
		}

		// Token: 0x06000046 RID: 70 RVA: 0x00003648 File Offset: 0x00001848
		public static SqlDataReader ExecuteReaderTypedParams(SqlConnection connection, string spName, DataRow dataRow)
		{
			if (connection == null)
			{
				throw new ArgumentNullException("connection");
			}
			if (spName == null || spName.Length == 0)
			{
				throw new ArgumentNullException("spName");
			}
			if (dataRow != null && dataRow.ItemArray.Length != 0)
			{
				SqlParameter[] spParameterSet = SqlHelperParameterCache.GetSpParameterSet(connection, spName);
				SqlHelper.AssignParameterValues(spParameterSet, dataRow);
				return SqlHelper.ExecuteReader(connection, CommandType.StoredProcedure, spName, spParameterSet);
			}
			return SqlHelper.ExecuteReader(connection, CommandType.StoredProcedure, spName);
		}

		// Token: 0x06000047 RID: 71 RVA: 0x000036A8 File Offset: 0x000018A8
		public static SqlDataReader ExecuteReaderTypedParams(SqlTransaction transaction, string spName, DataRow dataRow)
		{
			if (transaction == null)
			{
				throw new ArgumentNullException("transaction");
			}
			if (transaction != null && transaction.Connection == null)
			{
				throw new ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", "transaction");
			}
			if (spName == null || spName.Length == 0)
			{
				throw new ArgumentNullException("spName");
			}
			if (dataRow != null && dataRow.ItemArray.Length != 0)
			{
				SqlParameter[] spParameterSet = SqlHelperParameterCache.GetSpParameterSet(transaction.Connection, spName);
				SqlHelper.AssignParameterValues(spParameterSet, dataRow);
				return SqlHelper.ExecuteReader(transaction, CommandType.StoredProcedure, spName, spParameterSet);
			}
			return SqlHelper.ExecuteReader(transaction, CommandType.StoredProcedure, spName);
		}

		// Token: 0x06000048 RID: 72 RVA: 0x00003728 File Offset: 0x00001928
		public static object ExecuteScalarTypedParams(string connectionString, string spName, DataRow dataRow)
		{
			if (connectionString == null || connectionString.Length == 0)
			{
				throw new ArgumentNullException("connectionString");
			}
			if (spName == null || spName.Length == 0)
			{
				throw new ArgumentNullException("spName");
			}
			if (dataRow != null && dataRow.ItemArray.Length != 0)
			{
				SqlParameter[] spParameterSet = SqlHelperParameterCache.GetSpParameterSet(connectionString, spName);
				SqlHelper.AssignParameterValues(spParameterSet, dataRow);
				return SqlHelper.ExecuteScalar(connectionString, CommandType.StoredProcedure, spName, spParameterSet);
			}
			return SqlHelper.ExecuteScalar(connectionString, CommandType.StoredProcedure, spName);
		}

		// Token: 0x06000049 RID: 73 RVA: 0x00003790 File Offset: 0x00001990
		public static object ExecuteScalarTypedParams(SqlConnection connection, string spName, DataRow dataRow)
		{
			if (connection == null)
			{
				throw new ArgumentNullException("connection");
			}
			if (spName == null || spName.Length == 0)
			{
				throw new ArgumentNullException("spName");
			}
			if (dataRow != null && dataRow.ItemArray.Length != 0)
			{
				SqlParameter[] spParameterSet = SqlHelperParameterCache.GetSpParameterSet(connection, spName);
				SqlHelper.AssignParameterValues(spParameterSet, dataRow);
				return SqlHelper.ExecuteScalar(connection, CommandType.StoredProcedure, spName, spParameterSet);
			}
			return SqlHelper.ExecuteScalar(connection, CommandType.StoredProcedure, spName);
		}

		// Token: 0x0600004A RID: 74 RVA: 0x000037F0 File Offset: 0x000019F0
		public static object ExecuteScalarTypedParams(SqlTransaction transaction, string spName, DataRow dataRow)
		{
			if (transaction == null)
			{
				throw new ArgumentNullException("transaction");
			}
			if (transaction != null && transaction.Connection == null)
			{
				throw new ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", "transaction");
			}
			if (spName == null || spName.Length == 0)
			{
				throw new ArgumentNullException("spName");
			}
			if (dataRow != null && dataRow.ItemArray.Length != 0)
			{
				SqlParameter[] spParameterSet = SqlHelperParameterCache.GetSpParameterSet(transaction.Connection, spName);
				SqlHelper.AssignParameterValues(spParameterSet, dataRow);
				return SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, spName, spParameterSet);
			}
			return SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, spName);
		}

		// Token: 0x0600004B RID: 75 RVA: 0x00003870 File Offset: 0x00001A70
		public static XmlReader ExecuteXmlReaderTypedParams(SqlConnection connection, string spName, DataRow dataRow)
		{
			if (connection == null)
			{
				throw new ArgumentNullException("connection");
			}
			if (spName == null || spName.Length == 0)
			{
				throw new ArgumentNullException("spName");
			}
			if (dataRow != null && dataRow.ItemArray.Length != 0)
			{
				SqlParameter[] spParameterSet = SqlHelperParameterCache.GetSpParameterSet(connection, spName);
				SqlHelper.AssignParameterValues(spParameterSet, dataRow);
				return SqlHelper.ExecuteXmlReader(connection, CommandType.StoredProcedure, spName, spParameterSet);
			}
			return SqlHelper.ExecuteXmlReader(connection, CommandType.StoredProcedure, spName);
		}

		// Token: 0x0600004C RID: 76 RVA: 0x000038D0 File Offset: 0x00001AD0
		public static XmlReader ExecuteXmlReaderTypedParams(SqlTransaction transaction, string spName, DataRow dataRow)
		{
			if (transaction == null)
			{
				throw new ArgumentNullException("transaction");
			}
			if (transaction != null && transaction.Connection == null)
			{
				throw new ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", "transaction");
			}
			if (spName == null || spName.Length == 0)
			{
				throw new ArgumentNullException("spName");
			}
			if (dataRow != null && dataRow.ItemArray.Length != 0)
			{
				SqlParameter[] spParameterSet = SqlHelperParameterCache.GetSpParameterSet(transaction.Connection, spName);
				SqlHelper.AssignParameterValues(spParameterSet, dataRow);
				return SqlHelper.ExecuteXmlReader(transaction, CommandType.StoredProcedure, spName, spParameterSet);
			}
			return SqlHelper.ExecuteXmlReader(transaction, CommandType.StoredProcedure, spName);
		}

		// Token: 0x02000034 RID: 52
		private enum SqlConnectionOwnership
		{
			// Token: 0x0400003C RID: 60
			Internal,
			// Token: 0x0400003D RID: 61
			External
		}
	}
}
