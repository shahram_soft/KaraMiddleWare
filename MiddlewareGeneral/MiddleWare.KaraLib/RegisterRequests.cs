﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using Interface.ViewModel;
using Interface.ViewModel.General;
using Newtonsoft.Json;
using PersianDate;
using PW.WorkFlows.SqlDal;
using System.Drawing;
using MiddleWare.KaraLib.Lib;

namespace MiddleWare.KaraLib
{
    public enum ActionEnum : short
    {
        // Token: 0x04000022 RID: 34
        DailyLeave = 1,
        // Token: 0x04000023 RID: 35
        DailyDuty,
        // Token: 0x04000024 RID: 36
        TimeLeave,
        // Token: 0x04000025 RID: 37
        InOutCorrection,
        // Token: 0x04000026 RID: 38
        OverTime,
        // Token: 0x04000027 RID: 39
        ShiftReplace,
        // Token: 0x04000028 RID: 40
        LeaveAddition,
        // Token: 0x04000029 RID: 41
        ForgottenInOut,
        // Token: 0x0400002A RID: 42
        TimeDuty,
        // Token: 0x0400002B RID: 43
        DailyLeaveOut,
        // Token: 0x0400002C RID: 44
        ShowDailyWork,
        // Token: 0x0400002D RID: 45
        TimeDutyNoReturn,
        // Token: 0x0400002E RID: 46
        InOutTypeCorrection
    }
    public enum StatusEnum
    {
        // Token: 0x04000064 RID: 100
        Unknown,
        // Token: 0x04000065 RID: 101
        ExitFromOrg,
        // Token: 0x04000066 RID: 102
        ExitFromHome
    }
    public enum IoStatusEnum
    {
        // Token: 0x04000068 RID: 104
        Unknown,
        // Token: 0x04000069 RID: 105
        EnterAndExit,
        // Token: 0x0400006A RID: 106
        Enter,
        // Token: 0x0400006B RID: 107
        Exit
    }
    public   class RegisterRequests
    {
        // Token: 0x04000008 RID: 8
        private double _mgrEmpNo;

        // Token: 0x04000009 RID: 9
        private string _curSec = "";
        public static int DifDays(DateTime StartDate, DateTime EndDate)
        {
            return (StartDate - EndDate).Days;
        }
        public bool SaveFiles(int reqId,string onlineUser)
        {
            try
            {
              IEnumerable<string> arg_4D_0 = from c in Directory.GetFiles(HttpContext.Current.Server.MapPath(("~/App_Data")))
                                                       where c.Contains(onlineUser + "@")
                                                       select c;
                        var path = WebConfigurationManager.AppSettings["FileAttachAddress"];
                        foreach (string current in arg_4D_0)
                        {
                            string fileName = Path.GetFileName(current);
                            if (fileName != null)
                            {
                                string destFileName = Path.Combine(path, fileName.Replace(onlineUser + "@", reqId + "@"));
                                File.Move(current, destFileName);
                        var newdesTeniction = Path.Combine(path, destFileName.Replace(".jpg", "_Thu.jpg"));
                        Image.GetThumbnailImageAbort callback = Graphic.ThumbnailCallback;
                        using (Image image = Image.FromFile(destFileName))
                        {
                            Size size = Graphic.ConstraintPictureSize(image.Size, new Size(100, 100));
                            using (Image thumbnailImage = image.GetThumbnailImage(size.Width, size.Height, callback, IntPtr.Zero))
                            {
                                thumbnailImage.Save(newdesTeniction, image.RawFormat);

                            }
                        }
                        
                    }
                        }
                        return true;
            }
            catch (Exception)
            {

                return false;
            }
          
        }
        public ResultViewModel RegisterDailyRequest(double empNo,short type, DateTime startDate, DateTime endDate, int? startHour, int? endHour, string duration, double submittedByEmployeeId, int operationsId, string description, ActionEnum actionE, bool isFirstTimeShift = false, bool isInsert = true, int requestId = 0)
     {
         try
         {
             var isValid = false;
             string result;
                    string text = (DifDays(endDate.Date, startDate.Date) + 1).ToString(CultureInfo.InvariantCulture);
                if (int.Parse(text) > 0)
                {
                    int num = 0;
                    if (isInsert)
                    {
                        num = InsertRequest(empNo, type, startDate, endDate, startHour, endHour, duration,
                            submittedByEmployeeId, operationsId, description, actionE, StatusEnum.Unknown,
                            IoStatusEnum.Unknown, string.Empty, string.Empty, null, null, null, null, isFirstTimeShift);
                    }
                    else
                    {
                        num = UpdateRequest(requestId,empNo, type, startDate, endDate, startHour, endHour, duration,
                          submittedByEmployeeId, operationsId, description, actionE, StatusEnum.Unknown,
                          IoStatusEnum.Unknown, string.Empty, string.Empty, null, null, null, null, isFirstTimeShift);
                    }
                    //if (num > 0)
                        //{
                        //    this.SaveAttachments(num);
                        //}
                    
                   
                    switch (num)
                    {
                        case -5:
                            result="تعداد درخواستهای شما از سقف مجاز بیشتر می باشد.";
                            break;
                        case -4:
                            result = "مدت مجوز درخواستی مجاز نمی باشد";
                            break;
                        case -3:
                            result = "درخواستهای " + (DateTime.Now - endDate.Date).Days + " روز قبل را نمی توانید ثبت کنید\nبا مدیر سیستم صحبت کنید";
                            break;
                        case -2:
                            result = "اطلاعات ثبت نشد\n مدیری برای شما وجود ندارد";
                            break;
                        case -1:
                            result = "اطلاعات ثبت نشد";
                            break;
                        default:
                            SaveFiles(num, submittedByEmployeeId.ToString());
                            isValid = true;
                            result = "ثبت با موفقیت انجام شد";
                            break;
                    }
                }
                else
                {
                    result = "تاریخ شروع و پایان را صحیح وارد نمایید.";
                }
                return new ResultViewModel
                {
                    Validate = isValid,
                    ValidateMessage = result
                };
        }
         catch (Exception ex)
         {
             
            return new ResultViewModel
            {
                Validate = false,
                ExceptionMessage = ex.Message
            };
         }
     }
        public ResultViewModel RegisterForgetIoRequest(double empNo, short type,DateTime sDat,string sTime,string description,string typeRequest,string onlineUser, bool isFirstTimeShift = false, bool isInsert = true, int requestId = 0)
        {
            try
            {
                int num = 0;
                int value = Convert.ToInt32(sTime.Replace(":", ""));

                //   num = InsertRequest(this.CurrentEmpNo, 100, this.EditForgetDate.Date, null, new int?(value), null, this.EditCards.SelectedValue, base.Mc.UserData.EmpNo, 2, this.Edit_Description.Text, ActionEnum.ForgottenInOut, false);
                if (isInsert)
                {
                    num = InsertRequest(empNo, type, sDat, null, new int?(value), null, typeRequest,
                        double.Parse(onlineUser), 2, description, ActionEnum.ForgottenInOut, StatusEnum.Unknown,
                        IoStatusEnum.Unknown, string.Empty, string.Empty, null, null, null, null, isFirstTimeShift);

                }
                else
                {
                    num = UpdateRequest(requestId,empNo, type, sDat, null, new int?(value), null, typeRequest,
                        double.Parse(onlineUser), 2, description, ActionEnum.ForgottenInOut, StatusEnum.Unknown,
                        IoStatusEnum.Unknown, string.Empty, string.Empty, null, null, null, null, isFirstTimeShift);

                }



                switch (num)
                {
                    case -5:
                        //this.ShowMessage("تعداد درخواستهای شما از سقف مجاز بیشتر می باشد.");
                        return new ResultViewModel
                        {
                            Validate = false,
                            ValidateMessage = "تعداد درخواستهای شما از سقف مجاز بیشتر می باشد."
                        };
                    case -4:
                       // this.ShowMessage("مدت مجوز درخواستی مجاز نمی باشد");
                        return new ResultViewModel
                        {
                            Validate = false,
                            ValidateMessage = "مدت مجوز درخواستی مجاز نمی باشد"
                        };
                    case -3:
                       // this.ShowMessage("درخواستهای " + (DateTime.Now - this.EditForgetDate.Date).Days + " روز قبل را نمی توانید ثبت کنید\nبا مدیر سیستم صحبت کنید");
                        return new ResultViewModel
                        {
                            Validate = false,
                            ValidateMessage = "درخواستهای " + (DateTime.Now - sDat.Date).Days + " روز قبل را نمی توانید ثبت کنید\nبا مدیر سیستم صحبت کنید"
                        };
                    case -2:
                        //this.ShowMessage("اطلاعات ثبت نشد\n مدیری برای شما وجود ندارد");
                        return new ResultViewModel
                        {
                            Validate = false,
                            ValidateMessage = "اطلاعات ثبت نشد\n مدیری برای شما وجود ندارد"
                        };
                    case -1:
                      //  this.ShowMessage("اطلاعات ثبت نشد");
                        return new ResultViewModel
                        {
                            Validate = false,
                            ValidateMessage = "اطلاعات ثبت نشد"
                        };
                    default:
                        return new ResultViewModel
                        {
                            Validate = true,
                            ValidateMessage = "ثبت با موفقیت انجام شد"
                        };

                       // break;
                }
            }
            catch (Exception ex)
            {

                return new ResultViewModel
                {
                    Validate = false,
                    ExceptionMessage = ex.Message
                };
            }
        }
        public ResultViewModel RegisterCorrectionTypeIoRequest(double empNo, short type, DateTime sDat, string sTime, string description, string typeRequest,string onlineUser, bool isFirstTimeShift = false, bool isInsert = true, int requestId = 0)
        {
            try
            {
                ParmfileRow paramFileRows = GetDataRow();
                var validateOfRequest = WorkFlowsDatabase.Instance.Action.GetValid(empNo);
                var timeRequest = validateOfRequest.FirstOrDefault(x => x.ActionId == (int)ActionEnum.InOutTypeCorrection);
                var requiredDesc = false;
                if (timeRequest != null)
                {
                    requiredDesc = timeRequest.NeedDesc;
                }
                if (requiredDesc && string.IsNullOrEmpty(description))
                {
                   // this.lblErr.Text = "توضیحات درخواست را وارد نمایید";
                    return new ResultViewModel
                    {
                        Validate = false,
                        ValidateMessage = "توضیحات درخواست را وارد نمایید"
                    };
                }
                int num = 0;
                string selectedValue = typeRequest;
                int num2 = Convert.ToInt32(sTime.Replace(":", ""));
                if (num2 < 1 || num2 >= 2400)
                {
                   // this.lblErr.Text = "ساعت قدیم را وارد نمایید.";
                    return new ResultViewModel
                    {
                        Validate = false,
                        ValidateMessage = "ساعت قدیم را وارد نمایید."
                    };
                }
                if (isInsert)
                {
                    //num = InsertRequest(this.CurrentEmpNo, 105, this.EditRequestDate.Date, null, new int?(num2), new int?(num2), selectedValue, base.Mc.UserData.EmpNo, 2, this.Edit_Description.Text, ActionEnum.InOutTypeCorrection, false);
                    num = InsertRequest(empNo, type, sDat, null, new int?(num2), new int?(num2), selectedValue,
                        double.Parse(onlineUser), 2, description, ActionEnum.InOutTypeCorrection, StatusEnum.Unknown,
                        IoStatusEnum.Unknown, string.Empty, string.Empty, null, null, null, null, isFirstTimeShift);

                }
                else
                {
                    num = UpdateRequest(requestId,empNo, type, sDat, null, new int?(num2), new int?(num2), selectedValue,
                      double.Parse(onlineUser), 2, description, ActionEnum.InOutTypeCorrection, StatusEnum.Unknown,
                      IoStatusEnum.Unknown, string.Empty, string.Empty, null, null, null, null, isFirstTimeShift);

                }

                switch (num)
                {
                    case -5:
                        //this.ShowMessage("تعداد درخواستهای شما از سقف مجاز بیشتر می باشد.");
                        return new ResultViewModel
                        {
                            Validate = false,
                            ValidateMessage = "تعداد درخواستهای شما از سقف مجاز بیشتر می باشد."
                        };
                    case -4:
                        //this.ShowMessage("مدت مجوز درخواستی مجاز نمی باشد");
                        return new ResultViewModel
                        {
                            Validate = false,
                            ValidateMessage = "مدت مجوز درخواستی مجاز نمی باشد"
                        };
                    case -3:
                       // this.ShowMessage("درخواستهای " + (DateTime.Now - this.EditRequestDate.Date).Days + " روز قبل را نمی توانید ثبت کنید\nبا مدیر سیستم صحبت کنید");
                        return new ResultViewModel
                        {
                            Validate = false,
                            ValidateMessage = "درخواستهای " + (DateTime.Now - sDat.Date).Days + " روز قبل را نمی توانید ثبت کنید\nبا مدیر سیستم صحبت کنید"
                        };
                    case -2:
                        //this.ShowMessage("اطلاعات ثبت نشد\n مدیری برای شما وجود ندارد");
                        return new ResultViewModel
                        {
                            Validate = false,
                            ValidateMessage = "اطلاعات ثبت نشد\n مدیری برای شما وجود ندارد"
                        };
                    case -1:
                        //this.ShowMessage("اطلاعات ثبت نشد");
                        return new ResultViewModel
                        {
                            Validate = false,
                            ValidateMessage = "اطلاعات ثبت نشد"
                        };
                    default:
                        return new ResultViewModel
                        {
                            Validate = true,
                            ValidateMessage = "ثبت با موفقیت انجام شد"
                        };
                }
            }
            catch (Exception ex)
            {

                return new ResultViewModel
                {
                    Validate = false,
                    ExceptionMessage = ex.Message
                };
            }
        }
        public ResultViewModel RegisterForCorectIoRequest(double empNo, short type, DateTime sDat, string sTime,string newTime, string description, string typeRequest,string onlineUser, bool isFirstTimeShift = false, bool isInsert=true,int requestId=0)
        {
            try
            {
                ParmfileRow paramFileRows = GetDataRow();


                var validateOfRequest = WorkFlowsDatabase.Instance.Action.GetValid(empNo);
                var timeRequest = validateOfRequest.FirstOrDefault(x => x.ActionId == (int)ActionEnum.InOutCorrection);
                var requiredDesc = false;
                if (timeRequest != null)
                {
                    requiredDesc = timeRequest.NeedDesc;
                }
                if (requiredDesc && string.IsNullOrEmpty(description))
                {
                    //this.lblErr.Text = "توضیحات درخواست را وارد نمایید";
                    return new ResultViewModel
                    {
                        Validate = false,
                        ValidateMessage = "توضیحات درخواست را وارد نمایید"
                    };
                }
                int num = 0;
                string selectedValue = typeRequest;
                int num2 = Convert.ToInt32(sTime.Replace(":", ""));
                int num3 = Convert.ToInt32(newTime.Replace(":", ""));
                if (num2 < 1 || num2 >= 2400)
                {
                  //  this.lblErr.Text = "ساعت قدیم را وارد نمایید.";
                    return new ResultViewModel
                    {
                        Validate = false,
                        ValidateMessage = "ساعت قدیم را وارد نمایید."
                    };
                }
                if (num3 < 0 || num3 >= 2400)
                {
                   // this.lblErr.Text = "ساعت جدید را صحیح وارد نمایید";
                    return new ResultViewModel
                    {
                        Validate = false,
                        ValidateMessage = "ساعت جدید را صحیح وارد نمایید"
                    };
                }
                if (!UtilityManagers.ValidateTime(sTime))
                {
                   // this.lblErr.Text = "ساعت قدیم را صحیح وارد نمایید.";
                    return new ResultViewModel
                    {
                        Validate = false,
                        ValidateMessage = "ساعت قدیم را صحیح وارد نمایید."
                    };
                }
                if (!UtilityManagers.ValidateTime(newTime))
                {
                    //this.lblErr.Text = "ساعت قدیم را صحیح وارد نمایید.";
                    return new ResultViewModel
                    {
                        Validate = false,
                        ValidateMessage = "ساعت قدیم را صحیح وارد نمایید."
                    };
                }
                //if (num3 < 1)
                //{
                //    this.lblErr.Text = "ساعت جدید شروع را صفر وارد کردید !! <br \\> این درخواست به عنوان درخواست حذف در نظر گرفته شد!!<br \\>";
                //}

                //num = InsertRequest(this.CurrentEmpNo, 101, this.EditRequestDate.Date, null, new int?(num2), new int?(num3), selectedValue, base.Mc.UserData.EmpNo, 2, this.Edit_Description.Text, ActionEnum.InOutCorrection, false);
                if (isInsert)
                {
                    num = InsertRequest(empNo, type, sDat, null, new int?(num2), new int?(num3), typeRequest,
                        double.Parse(onlineUser), 2, description, ActionEnum.InOutCorrection, StatusEnum.Unknown,
                        IoStatusEnum.Unknown, string.Empty, string.Empty, null, null, null, null, isFirstTimeShift);
                }
                else
                {
                    num = UpdateRequest(requestId,empNo, type, sDat, null, new int?(num2), new int?(num3), typeRequest,
                       double.Parse(onlineUser), 2, description, ActionEnum.InOutCorrection, StatusEnum.Unknown,
                       IoStatusEnum.Unknown, string.Empty, string.Empty, null, null, null, null, isFirstTimeShift);
                }

                switch (num)
                {
                    case -5:
                       // this.ShowMessage("تعداد درخواستهای شما از سقف مجاز بیشتر می باشد.");
                        return new ResultViewModel
                        {
                            Validate = false,
                            ValidateMessage = "تعداد درخواستهای شما از سقف مجاز بیشتر می باشد."
                        };
                    case -4:
                       // this.ShowMessage("مدت مجوز درخواستی مجاز نمی باشد");
                        return new ResultViewModel
                        {
                            Validate = false,
                            ValidateMessage = "مدت مجوز درخواستی مجاز نمی باشد"
                        };
                    case -3:
                        //this.ShowMessage("درخواستهای " + (DateTime.Now - this.EditRequestDate.Date).Days + " روز قبل را نمی توانید ثبت کنید\nبا مدیر سیستم صحبت کنید");
                        return new ResultViewModel
                        {
                            Validate = false,
                            ValidateMessage = "درخواستهای " + (DateTime.Now - sDat.Date).Days + " روز قبل را نمی توانید ثبت کنید\nبا مدیر سیستم صحبت کنید"
                        };
                    case -2:
                        //this.ShowMessage("اطلاعات ثبت نشد\n مدیری برای شما وجود ندارد");
                        return new ResultViewModel
                        {
                            Validate = false,
                            ValidateMessage = "اطلاعات ثبت نشد\n مدیری برای شما وجود ندارد"
                        };
                    case -1:
                        //this.ShowMessage("اطلاعات ثبت نشد");
                        return new ResultViewModel
                        {
                            Validate = false,
                            ValidateMessage = "اطلاعات ثبت نشد"
                        };
                    default:
                        return new ResultViewModel
                        {
                            Validate = true,
                            ValidateMessage = "ثبت با موفقیت انجام شد"
                        };
                }
            }
            catch (Exception ex)
            {

                return new ResultViewModel
                {
                    Validate = false,
                    ExceptionMessage = ex.Message
                };
            }
        }
        public ResultViewModel RegisterMissonDailyRequest(double empNo, short type, DateTime startDate, DateTime endDate,bool locationCompany,string transferPriceMissDl,string toVehicelMissDl,string returnVehicelMissDl,  double submittedByEmployeeId, int operationsId, string description, ActionEnum actionE,string source,string destinition, bool isFirstTimeShift = false, bool isInsert = true, int requestId = 0)
        {
            try
            {
                var resultRequest = new ResultViewModel();
                int num =
                    ShDate.DifDays(endDate.Date,
                        startDate.Date) + 1;
                if (num > 0)
                {
                    double? transferCost = null;
                    if (!string.IsNullOrEmpty(transferPriceMissDl))
                    {
                        transferPriceMissDl = ToEnglishNumber(transferPriceMissDl);
                        transferCost = new double?(Convert.ToDouble(transferPriceMissDl));
                    }
                    short value;
                    short.TryParse(toVehicelMissDl, out value);
                    short value2;
                    short.TryParse(returnVehicelMissDl, out value2);
                    int num2 = -1;
                    if (isInsert)
                    {
                        num2 = InsertRequest(empNo, type,
                            startDate.Date,
                            new DateTime?(endDate.Date), null, null,
                            num.ToString(CultureInfo.InvariantCulture), submittedByEmployeeId, 3,
                            description, ActionEnum.DailyDuty, StatusEnum.Unknown, IoStatusEnum.Unknown, source,
                            destinition, new short?(value), new short?(value2),
                            locationCompany, transferCost, false);
                    }
                    else
                    {
                        num2 = UpdateRequest(requestId,empNo, type,
                      startDate.Date,
                      new DateTime?(endDate.Date), null, null,
                      num.ToString(CultureInfo.InvariantCulture), submittedByEmployeeId, 3,
                      description, ActionEnum.DailyDuty, StatusEnum.Unknown, IoStatusEnum.Unknown, source,
                      destinition, new short?(value), new short?(value2),
                      locationCompany, transferCost, false);
                    }
                    //if (num2 > 0)
                    //{
                    //    this.SaveAttachments(num2);
                    //}

            
                    switch (num2)
                    {
                        case -5:
                           // this.ShowMessage("تعداد درخواستهای شما از سقف مجاز بیشتر می باشد.");
                            resultRequest.Validate = false;
                            resultRequest.ValidateMessage = "تعداد درخواستهای شما از سقف مجاز بیشتر می باشد.";
                            break;
                        case -4:
                            //this.ShowMessage("مدت مجوز درخواستی مجاز نمی باشد");
                            resultRequest.Validate = false;
                            resultRequest.ValidateMessage = "مدت مجوز درخواستی مجاز نمی باشد";
                            break;
                        case -3:
                            //this.ShowMessage("درخواستهای " + (DateTime.Now - this.EditStartDate.Date).Days +
                            //                 " روز قبل را نمی توانید ثبت کنید\nبا مدیر سیستم صحبت کنید");
                            resultRequest.Validate = false;
                            resultRequest.ValidateMessage = "درخواستهای " + (DateTime.Now - startDate.Date).Days +
                                             " روز قبل را نمی توانید ثبت کنید\nبا مدیر سیستم صحبت کنید";
                            break;
                        case -2:
                            //this.ShowMessage("اطلاعات ثبت نشد\n مدیری برای شما وجود ندارد");
                            resultRequest.Validate = false;
                            resultRequest.ValidateMessage = "اطلاعات ثبت نشد\n مدیری برای شما وجود ندارد";
                            break;
                        case -1:
                           // this.ShowMessage("اطلاعات ثبت نشد");
                            resultRequest.Validate = false;
                            resultRequest.ValidateMessage = "اطلاعات ثبت نشد";
                            break;
                        default:
                            SaveFiles(num2, submittedByEmployeeId.ToString());
                            resultRequest.Validate = true;
                            resultRequest.ValidateMessage = "با موفقیت ثبت شد";
                            break;
                    }
                }
                else
                {
                   // this.lblErr.Text = "مقدار تاریخ شروع و پایان را صحیح وارد نمایید.";
                    resultRequest.Validate = false;
                    resultRequest.ValidateMessage = "مقدار تاریخ شروع و پایان را صحیح وارد نمایید.";
                }
                return resultRequest;
            }
            catch (Exception ex)
            {

                return new ResultViewModel
                {
                    Validate = false,
                    ExceptionMessage = ex.Message
                };
            }

        }

        public int UpdateRequest(int requestId,double empNo, short type, DateTime startDate, DateTime? endDate, int? startHour,
            int? endHour, string duration, double submittedByEmployeeId, int operationsId, string description,
            ActionEnum actionE, StatusEnum status, IoStatusEnum ioStatus, string source, string distination,
            short? deviceWent, short? deviceBack, bool? locationStatus, double? transferCost,
            bool isFirstTimeShift = false)
        {
            return WorkFlowsDatabase.Instance.Requests.Update(requestId, empNo,DateTime.Now, type,startDate,endDate,startHour,endHour,duration,
                submittedByEmployeeId,operationsId,description,actionE,  status,  ioStatus,  source,  distination,
           deviceWent,  deviceBack,  locationStatus,  transferCost,isFirstTimeShift);
        }

        public int InsertRequest(double empNo, short type, DateTime startDate, DateTime? endDate, int? startHour, int? endHour, string duration, double submittedByEmployeeId, int operationsId, string description, ActionEnum actionE, StatusEnum status, IoStatusEnum ioStatus, string source, string distination, short? deviceWent, short? deviceBack, bool? locationStatus, double? transferCost, bool isFirstTimeShift = false)
        {
            double managerCode = 0;
            var resultService = 0;
            int num = "100,101,102,103,104,105".Split(new char[]
            {
                ','
            }).Contains(type.ToString()) ? 0 : this.EnoughLimitValue(empNo, startDate, new short?(type), duration);
            if (num < 0)
            {
                return num;
            }
            DateTime date = DateTime.Now.AddDays((double)(-1 * GetRequestValidDays(actionE))).Date;
            if (startDate >= date || duration.Contains('-'))
            {
                if (GetLimitCount(actionE) >= this.GetRequestsCount(empNo, actionE))
                {
                    string value =GetDataByEmpNo(empNo)["SEC_NO"].ToString();
                    double num2 = GetManagerOfSection(empNo, ref value);
                    if (num2 > 0.0)
                    {
                        DataRow conferment = GetConferment(num2, (int)actionE, Convert.ToInt32(value));
                        if (conferment != null)
                        {
                            num2 = Convert.ToDouble(conferment["EMP_NO"]);
                        }
                        var constre = ConfigurationManager.ConnectionStrings["connectionStringProviderSystem"].ToString();
                        var connection = new SqlConnection(constre);
                      if (connection.State == ConnectionState.Open)
                        {
                            connection.Close();
                        }
                        connection.Open();
                        managerCode = num2;
                        var command = new SqlCommand("WF_RequestsInsert", connection);
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add("@EMP_NO", SqlDbType.Float, 2000).Value = empNo;
                        command.Parameters.Add("@SubmittedDate", SqlDbType.DateTime, 2000).Value = DateTime.Now;
                        command.Parameters.Add("@Type", SqlDbType.SmallInt, 2000).Value = type;
                        command.Parameters.Add("@StartDate", SqlDbType.DateTime, 2000).Value = startDate;
                        command.Parameters.Add("@EndDate", SqlDbType.DateTime, 2000).Value = (endDate ?? (object)DBNull.Value);
                        command.Parameters.Add("@StartHour", SqlDbType.Int, 2000).Value =( startHour ?? (object) DBNull.Value);
                        command.Parameters.Add("@EndHour", SqlDbType.Int, 2000).Value = (endHour ?? (object)DBNull.Value);
                        command.Parameters.Add("@Duration", SqlDbType.VarChar, 2000).Value = duration;
                        command.Parameters.Add("@SubmittedByEmployeeID", SqlDbType.Float, 2000).Value = submittedByEmployeeId;
                        command.Parameters.Add("@IsFinalApproved", SqlDbType.Bit, 2000).Value = DBNull.Value;
                        command.Parameters.Add("@ApprovalByManagerEmp_No", SqlDbType.Float, 2000).Value = DBNull.Value;
                        command.Parameters.Add("@ApprovalDate", SqlDbType.DateTime, 2000).Value = DBNull.Value;
                        command.Parameters.Add("@OperationsID", SqlDbType.Int, 2000).Value = operationsId;
                        command.Parameters.Add("@Description", SqlDbType.VarChar, 2000).Value = description;
                        command.Parameters.Add("@CurEmp_NO", SqlDbType.Float, 2000).Value = num2;
                        command.Parameters.Add("@CurSection", SqlDbType.Int, 2000).Value = Convert.ToInt32(value);
                        command.Parameters.Add("@RequestStatus", SqlDbType.TinyInt, 2000).Value = (short)status;
                        command.Parameters.Add("@IOStatus", SqlDbType.TinyInt, 2000).Value = (short)ioStatus;
                        command.Parameters.Add("@Source", SqlDbType.VarChar, 2000).Value = source ?? (object)DBNull.Value;
                        command.Parameters.Add("@Distination", SqlDbType.VarChar, 2000).Value = (distination ?? (object)DBNull.Value);
                        command.Parameters.Add("@DeviceWent", SqlDbType.TinyInt, 2000).Value = (deviceWent ?? (object)DBNull.Value);
                        command.Parameters.Add("@DeviceBack", SqlDbType.TinyInt, 2000).Value = (deviceBack ?? (object)DBNull.Value);
                        command.Parameters.Add("@LocationStatus", SqlDbType.Bit, 2000).Value = (locationStatus ?? (object)DBNull.Value);
                        command.Parameters.Add("@TransferCost", SqlDbType.Float, 2000).Value = (transferCost ?? (object)DBNull.Value);
                        command.Parameters.Add("@IsFirstTimeShift", SqlDbType.Bit, 2000).Value = isFirstTimeShift;


                        var result = command.ExecuteReader();
                        while (result.Read())
                        {
                            resultService = int.Parse(result[0].ToString());
                        }
                        connection.Close();
                        connection.Dispose();
                        //object obj = base.ExecuteScalar("WF_RequestsInsert", new object[]
                        //{
                        //    empNo,
                        //    DateTime.Now,
                        //    type,
                        //    startDate,
                        //    endDate,
                        //    startHour,
                        //    endHour,
                        //    duration,
                        //    submittedByEmployeeId,
                        //    null,
                        //    null,
                        //    null,
                        //    operationsId,
                        //    description,
                        //    num2,
                        //    Convert.ToInt32(value),
                        //    (short)status,
                        //    (short)ioStatus,
                        //    source,
                        //    distination,
                        //    deviceWent,
                        //    deviceBack,
                        //    locationStatus,
                        //    transferCost,
                        //    isFirstTimeShift
                        //});
                        //if (obj != null)
                        //{
                        //    num = Convert.ToInt32(obj);
                        //}
                        num = resultService;
                    }
                    else
                    {
                        num = -2;
                    }
                }
                else
                {
                    num = -5;
                }
            }
            else
            {
                num = -3;
            }
            if (num > 0)
            {
                var portfolioMng=new PortfolioManager(new WorkFlowsDatabase());
               
                    var result = portfolioMng.SentNotification(managerCode);
               
              
            }
            return num;
        }
        public ResultViewModel ValidParam(DateTime sDate,DateTime eDate,string cardId,string description,string empNumber,bool isInsert=true)
        {
            bool flag = true;
            var resultGeneral=new ResultViewModel();
            //this.lblErr.ToolTip = "";
            ParmfileRow paramFileRows = GetDataRow();
            if (eDate.Date < sDate.Date)
            {
                return new ResultViewModel
                {
                    Validate = false,
                    ValidateMessage = "زمان پایان نمیتواند کوچکتر از زمان شروع میباشد."
                };
             
            }
            var validateOfRequest = WorkFlowsDatabase.Instance.Action.GetValid(double.Parse(empNumber));
            var timeRequest = validateOfRequest.FirstOrDefault(x => x.ActionId == (int)ActionEnum.DailyLeave);
            var requiredDesc = false;
            if (timeRequest != null)
            {
                requiredDesc = timeRequest.NeedDesc;
            }
            if (string.IsNullOrEmpty(description) && requiredDesc)
            {
                //this.lblErr.Text = "توضیحات درخواست را وارد نمایید";
                return new ResultViewModel
                {
                    Validate = false,
                    ValidateMessage = "توضیحات درخواست را وارد نمایید"
                };
            }
            if (cardId == "60")
            {
                string text = "";
                string text2 = "";
                if (!CheckEsteh(double.Parse(empNumber), sDate.Date, eDate.Date, ref text, ref text2))
                {
                    return new ResultViewModel
                    {
                        Validate = false,
                        ValidateMessage = text2
                    };
                }
            }
            if (cardId == "57")
            {
                string str = "";
                int requestDuration = DifDays(eDate.Date, sDate.Date) + 1;
                string text3;
                if (!IsAccessWithReamin(double.Parse(empNumber), requestDuration, false, ref str, paramFileRows, out text3)&&isInsert)
                {
                    //this.lblErr.Text = "مانده منفی می باشد ،شما مجاز به ثبت درخواست نمی باشد";
                    //this.lblErr.ToolTip = "مرخصی استفاده شده و درخواست شده :" + str;
                    //flag = false;
                    return new ResultViewModel
                    {
                        Validate = false,
                        ValidateMessage = "مانده منفی می باشد ،شما مجاز به ثبت درخواست نمی باشد"
                    };
                }
                //if (!string.IsNullOrEmpty(text3))
                //{
                //   // this.lblSumRequest.Text = "مجموع مرخصی بررسی نشده معادل " + text3;
                //}
            }
            if (true)
            {
                RequestExistCountRow requestExistCountRow = RequestsIsExist(sDate.Date, eDate.Date,double.Parse(empNumber));
                if (MamGetDataByEmpNo(eDate.Date.ToOADate(), sDate.Date.ToOADate(), double.Parse(cardId)).Rows.Count > 0 || requestExistCountRow.NullCount > 0)
                {
                    return new ResultViewModel
                    {
                        Validate = false,
                        ValidateMessage = "در بازه انتخابی ،شما یک درخواست مجوز دارید "
                    };
                }
                else
                {
                    DataTable shiftIsHoliday = GetShiftIsHoliday(double.Parse(empNumber), sDate.Date.ToOADate(), eDate.Date.ToOADate(), sDate.ToFa().Replace("/", ""), ShDate.IsLeapYear((short)sDate.Date.Year));
                    if (shiftIsHoliday != null && shiftIsHoliday.Rows != null && shiftIsHoliday.Rows.Count > 0)
                    {
                        //this.TdHoliday.InnerText = "";
                        foreach (DataRow dataRow in shiftIsHoliday.Rows)
                        {
                            return new ResultViewModel
                            {
                                Validate = true,
                                ValidateMessage = "تاریخ "+ ShDate.FormatDate(dataRow["CurDate"].ToString()) + " "+ dataRow["TITLE"].ToString()+ " میباشد"
                            };
                        }
                        //this.TBL_Show.Visible = true;
                    }
                }
            }
            return new ResultViewModel
            {
                Validate = true
            };
        }
        // Token: 0x0600016C RID: 364 RVA: 0x0000BBFC File Offset: 0x00009DFC
        public DataTable GetShiftIsHoliday(double empNo, double dateS, double dateE, string shamsiDate, bool isKabise)
        {
            //return base.ExecuteDataTable("WF_GetShiftIsHoliday", new object[]
            //{
            //    empNo,
            //    dateS,
            //    dateE,
            //    ShDate.RemoveDateSeparator(shamsiDate),
            //    isKabise
            //});
            var constre = ConfigurationManager.ConnectionStrings["connectionStringProviderSystem"].ToString();
            var connection = new SqlConnection(constre);

            if (connection.State == ConnectionState.Open)
            {
                connection.Close();
            }
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = new DataTable();
            cmd = new SqlCommand("WF_GetShiftIsHoliday", connection);
            cmd.Parameters.Add(new SqlParameter("@Emp_no", empNo));
            cmd.Parameters.Add(new SqlParameter("@DateS", dateS));
            cmd.Parameters.Add(new SqlParameter("@DateE", dateE));
            cmd.Parameters.Add(new SqlParameter("@ShamsiDate", shamsiDate));
            cmd.Parameters.Add(new SqlParameter("@ISKabise", isKabise));
            cmd.CommandType = CommandType.StoredProcedure;
            da.SelectCommand = cmd;
            da.Fill(dt);
            connection.Close();
            connection.Dispose();
            return dt;
        }
        // Token: 0x0600015B RID: 347 RVA: 0x0000B044 File Offset: 0x00009244
        public DataTable MamGetDataByEmpNo(double fromDate, double toDate, double empNo)
        {
            //return base.ExecuteDataTable("WF_MOR_MAMGetDataByEmpNo", new object[]
            //{
            //    fromDate,
            //    toDate,
            //    empNo
            //});
            var constre = ConfigurationManager.ConnectionStrings["connectionStringProviderSystem"].ToString();
            var connection = new SqlConnection(constre);

            if (connection.State == ConnectionState.Open)
            {
                connection.Close();
            }
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = new DataTable();
            cmd = new SqlCommand("WF_MOR_MAMGetDataByEmpNo", connection);
            cmd.Parameters.Add(new SqlParameter("@From", fromDate));
            cmd.Parameters.Add(new SqlParameter("@TO", toDate));
            cmd.Parameters.Add(new SqlParameter("@Emp_No", empNo));
            cmd.CommandType = CommandType.StoredProcedure;
            da.SelectCommand = cmd;
            da.Fill(dt);
            connection.Close();
            connection.Dispose();
            return dt;
        }
        // Token: 0x060001BB RID: 443 RVA: 0x00011EB4 File Offset: 0x000100B4
        public RequestExistCountRow RequestsIsExist(DateTime startDate, DateTime endDate, double empNo)
        {
            DateTime dateTime = new DateTime(startDate.Year, startDate.Month, startDate.Day, 23, 59, 59);
            DateTime dateTime2 = new DateTime(endDate.Year, endDate.Month, endDate.Day, 23, 59, 59);
            //DataTable dataTable = base.ExecuteDataTable("WF_RequestsIsExist", new object[]
            //{
            //    dateTime.Date,
            //    dateTime2.Date,
            //    empNo
            //});
            var constre = ConfigurationManager.ConnectionStrings["connectionStringProviderSystem"].ToString();
            var connection = new SqlConnection(constre);

            if (connection.State == ConnectionState.Open)
            {
                connection.Close();
            }
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dataTable = new DataTable();
            cmd = new SqlCommand("WF_RequestsIsExist", connection);
            cmd.Parameters.Add(new SqlParameter("@StartDate", dateTime.Date));
            cmd.Parameters.Add(new SqlParameter("@EndDate", dateTime2.Date));
            cmd.Parameters.Add(new SqlParameter("@Emp_No", empNo));
            cmd.CommandType = CommandType.StoredProcedure;
            da.SelectCommand = cmd;
            da.Fill(dataTable);
            connection.Close();
            connection.Dispose();
            RequestExistCountRow requestExistCountRow = new RequestExistCountRow();
            try
            {
                requestExistCountRow.NullCount = Convert.ToInt32(dataTable.Select("IsFinalApproved is null")[0]["cnt"]);
            }
            catch
            {
                requestExistCountRow.NullCount = 0;
            }
            try
            {
                requestExistCountRow.TrueCount = Convert.ToInt32(dataTable.Select("IsFinalApproved=1")[0]["cnt"]);
            }
            catch
            {
                requestExistCountRow.TrueCount = 0;
            }
            try
            {
                requestExistCountRow.FalseCount = Convert.ToInt32(dataTable.Select("IsFinalApproved=0")[0]["cnt"]);
            }
            catch
            {
                requestExistCountRow.FalseCount = 0;
            }
            try
            {
                requestExistCountRow.NullTrueCount = Convert.ToInt32(dataTable.Select("IsFinalApproved is null or IsFinalApproved=1")[0]["cnt"]);
            }
            catch
            {
                requestExistCountRow.NullTrueCount = 0;
            }
            return requestExistCountRow;
        }
        // Token: 0x06000164 RID: 356 RVA: 0x0000B7DC File Offset: 0x000099DC
        public bool IsAccessWithReamin(double empNo, int requestDuration, bool isTimeLeave, ref string limitDay, ParmfileRow drParmfile, out string sumRequestDuration)
        {
            sumRequestDuration = string.Empty;
            if (!drParmfile.WfRequestMandeControl)
            {
                return true;
            }
            int num = ShDate.Time2Min(GetDayTime(empNo, DateTime.Now, drParmfile));
            bool result = false;
            int num2 = 0;
            int num3;
            if (isTimeLeave)
            {
                num3 = GetSumDailyLeaveDurationByEmpNo(empNo) * num;
                string text = UtilityManagers.ConvertToFourDigitNumber(requestDuration.ToString());
                int num4;
                int.TryParse(text.Substring(0, text.Length - 2), out num4);
                int num5;
                int.TryParse(text.Substring(text.Length - 2, 2), out num5);
                num3 += GetSumTimeLeaveDurationByEmpNo(empNo) + (num4 * 60 + num5);
            }
            else
            {
                num3 = (GetSumDailyLeaveDurationByEmpNo(empNo) + requestDuration) * num;
                num3 += GetSumTimeLeaveDurationByEmpNo(empNo);
            }
            sumRequestDuration = UtilityManagers.ConvertMinToTimeFormat(num3, num);
            DataTable remainKardex = GetRemainKardex(empNo);
            if (remainKardex != null && remainKardex.Rows.Count > 0)
            {
                int num6 = Convert.ToInt32(remainKardex.Rows[0]["RemainKardex"].ToString().Replace(":", ""));
                long num7;
                if (Convert.ToBoolean(remainKardex.Rows[0]["IS_MOR_DAY"]))
                {
                    num7 = (long)(Convert.ToInt32(num6) * num);
                }
                else
                {
                    int num8 = num6 / 100;
                    long num9 = (long)(num6 % 100);
                    if (num8 < 0)
                    {
                        num9 *= -1L;
                        num7 = (long)(num8 * 60) + num9;
                    }
                    else
                    {
                        num7 = (long)(num8 * 60) + num9;
                    }
                }
                long num10 = num7 - (long)num3;
                long num11 = num10 % (long)num;
                limitDay = string.Concat(new object[]
                {
                    num10 / (long)num,
                    " روز ",
                    num11 / 60L,
                    " ساعت ",
                    num11 % 60L,
                    " دقیقه "
                });
                result = (num7 - (long)num3 >= (long)num2);
            }
            return result;
        }
        // Token: 0x06000162 RID: 354 RVA: 0x0000B4BC File Offset: 0x000096BC
        public DataTable GetRemainKardex(double empNo)
        {
            DataTable dataTable = new DataTable();
            dataTable.Columns.Add("RemainKardex", typeof(int));
            dataTable.Columns.Add("IS_MOR_DAY", typeof(int));
            //using (SqlDataReader sqlDataReader = base.ExecuteReader("GetRemainKardex", new object[]
            //{
            //    empNo
            //}))
            //{
            //    if (sqlDataReader.HasRows)
            //    {
            //        while (sqlDataReader.Read())
            //        {
            //            DataRow dataRow = dataTable.NewRow();
            //            dataRow["RemainKardex"] = sqlDataReader["RemainKardex"];
            //            dataRow["IS_MOR_DAY"] = sqlDataReader["IS_MOR_DAY"];
            //            dataTable.Rows.Add(dataRow);
            //        }
            //    }
            //}
            var constre = ConfigurationManager.ConnectionStrings["connectionStringProviderSystem"].ToString();
            var connection = new SqlConnection(constre);
            if (connection.State == ConnectionState.Open)
            {
                connection.Close();
            }
            connection.Open();
            var command = new SqlCommand("GetRemainKardex", connection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add("@Emp_No", SqlDbType.Float, 2000).Value = empNo;
          

            var result = command.ExecuteReader();
            while (result.Read())
            {
                DataRow dataRow = dataTable.NewRow();
                dataRow["RemainKardex"] = result["RemainKardex"];
                dataRow["IS_MOR_DAY"] = result["IS_MOR_DAY"];
                dataTable.Rows.Add(dataRow);
            }
            connection.Close();
            connection.Dispose();

            return dataTable;
        }
        // Token: 0x060001D0 RID: 464 RVA: 0x0001408C File Offset: 0x0001228C
        public int GetSumTimeLeaveDurationByEmpNo(double empNo)
        {
            var outPut = 0;
            var constre = ConfigurationManager.ConnectionStrings["connectionStringProviderSystem"].ToString();
            var connection = new SqlConnection(constre);

            if (connection.State == ConnectionState.Open)
            {
                connection.Close();
            }

            string query = "SELECT SUM(dbo.Time2Min(CAST(r.Duration AS INT))) SumDuration FROM Requests r WHERE r.IsFinalApproved IS NULL AND r.[Type]=17 AND r.EMP_NO=" + empNo;

            connection.Open();
            var command = new SqlCommand(query, connection);

            var result = command.ExecuteReader();
            while (result.Read())
            {
                  int.TryParse(result[0].ToString(),out outPut);


            }
            connection.Close();
            connection.Dispose();


            //object obj = base.ExecuteScalar(CommandType.Text, commandText);
            //if (!(obj is DBNull) && obj != null)
            //{
            //    return Convert.ToInt32(obj);
            //}
            return outPut;
            //string commandText = "SELECT SUM(dbo.Time2Min(CAST(r.Duration AS INT))) SumDuration FROM Requests r WHERE r.IsFinalApproved IS NULL AND r.[Type]=17 AND r.EMP_NO=" + empNo;
            //object obj = base.ExecuteScalar(CommandType.Text, commandText);
            //if (!(obj is DBNull) && obj != null)
            //{
            //    return Convert.ToInt32(obj);
            //}
            //return 0;
        }
        public int GetSumDailyLeaveDurationByEmpNo(double empNo)
        {
            var outPut = 0;
            var constre = ConfigurationManager.ConnectionStrings["connectionStringProviderSystem"].ToString();
            var connection = new SqlConnection(constre);

            if (connection.State == ConnectionState.Open)
            {
                connection.Close();
            }

            string query = "SELECT SUM(CAST(r.Duration AS INT)) SumDuration FROM Requests r WHERE r.IsFinalApproved IS NULL AND r.[Type]=57 AND r.EMP_NO=" + empNo;

            connection.Open();
            var command = new SqlCommand(query, connection);

            var result = command.ExecuteReader();
            while (result.Read())
            {
                int.TryParse(result[0].ToString(),out outPut);


            }
            connection.Close();
            connection.Dispose();


            //object obj = base.ExecuteScalar(CommandType.Text, commandText);
            //if (!(obj is DBNull) && obj != null)
            //{
            //    return Convert.ToInt32(obj);
            //}
            return outPut;
        }
        // Token: 0x0600014B RID: 331 RVA: 0x0000A6C4 File Offset: 0x000088C4
        public int GetDayTime(double empNo, DateTime date, ParmfileRow pr)
        {
            int num = (int)pr.KasrEsth;
            DataRow dataByEmpNo = GetDataByEmpNo(empNo);
            if (Convert.ToBoolean(dataByEmpNo["IS_UNIQ"]))
            {
                if (Convert.ToInt32(dataByEmpNo["Mor_Kasr"]) == 3)
                {
                    num = Convert.ToInt32(dataByEmpNo["KASR_ESTH"]);
                }
            }
            else if (pr.DifKasrEsth)
            {
                int num2 = 0;
                short grpNo = FindGrpsByEmpAndDate(empNo, date, Convert.ToInt16(dataByEmpNo["GRP_NO"]), ref num2);
                DataRow dataByGrpNo = GetDataByGrpNo(grpNo);
                if (Convert.ToInt32(dataByGrpNo["Mor_Kasr"]) == 3)
                {
                    num = Convert.ToInt32(dataByGrpNo["KASR_ESTH"]);
                }
            }
            else
            {
                num = (int)pr.KasrEsth;
            }
            if (num == 0)
            {
                num = 720;
            }
            return num;
        }
        public DataRow GetDataByGrpNo(short grpNo)
        {
            var constre = ConfigurationManager.ConnectionStrings["connectionStringProviderSystem"].ToString();
            var connection = new SqlConnection(constre);

            if (connection.State == ConnectionState.Open)
            {
                connection.Close();
            }
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = new DataTable();
            cmd = new SqlCommand("WF_GROUPSGetDataByGrpNo", connection);
            cmd.Parameters.Add(new SqlParameter("@Grp_no", grpNo));
            cmd.CommandType = CommandType.StoredProcedure;
            da.SelectCommand = cmd;
            da.Fill(dt);
            return dt.Rows[0];
            //return base.ExecuteDataRow("WF_GROUPSGetDataByGrpNo", new object[]
            //{
            //    grpNo
            //});
        }
        // Token: 0x06000148 RID: 328 RVA: 0x0000A4B0 File Offset: 0x000086B0
        public short FindGrpsByEmpAndDate(double empNo, DateTime date, short grpNo, ref int shiftNo)
        {
            short value = ShDate.YearOf(date);
            short value2 = ShDate.MonthOf(date);
            short value3 = ShDate.DayOf(date);
            float num = Convert.ToSingle(date.ToOADate());
            int? num2 = new int?(0);
            int? num3 = new int?(0);
            int? num4 = new int?((int)grpNo);
            Sp_MixFindShift(ref num4, new double?(empNo), new double?((double)num), new int?((int)value), new int?((int)value2), new int?((int)value3), ref num2, ref num3);
            if (num2.HasValue)
            {
                shiftNo = num2.Value;
            }
            if (num2 < 0)
            {
                throw new Exception("تعریف شیفتها دارای اشکال می باشد.شیفت " + num2);
            }
            if (num4.HasValue)
            {
                return (short)num4.Value;
            }
            return -1;
        }
        // Token: 0x06000147 RID: 327 RVA: 0x00009FE8 File Offset: 0x000081E8
        public object Sp_MixFindShift(ref int? groupFirst, double? empNo, double? dateStart, int? year, int? month, int? day, ref int? shiftNew, ref int? calenT)
        {
            var constre = ConfigurationManager.ConnectionStrings["connectionStringProviderSystem"].ToString();
            var connection = new SqlConnection(constre);

            if (connection.State == ConnectionState.Open)
            {
                connection.Close();
            }
            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.Connection = connection;
            sqlCommand.CommandText = "dbo.Sp_MixFindShift";
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.Parameters.Add(new SqlParameter("@RETURN_VALUE", SqlDbType.Int, 4, ParameterDirection.ReturnValue, 10, 0, null, DataRowVersion.Current, false, null, "", "", ""));
            sqlCommand.Parameters.Add(new SqlParameter("@GroupFirst", SqlDbType.Int, 4, ParameterDirection.InputOutput, 10, 0, null, DataRowVersion.Current, false, null, "", "", ""));
            sqlCommand.Parameters.Add(new SqlParameter("@Emp_No", SqlDbType.Float, 8, ParameterDirection.Input, 53, 0, null, DataRowVersion.Current, false, null, "", "", ""));
            sqlCommand.Parameters.Add(new SqlParameter("@DateStart", SqlDbType.Float, 8, ParameterDirection.Input, 53, 0, null, DataRowVersion.Current, false, null, "", "", ""));
            sqlCommand.Parameters.Add(new SqlParameter("@Year_", SqlDbType.Int, 4, ParameterDirection.Input, 10, 0, null, DataRowVersion.Current, false, null, "", "", ""));
            sqlCommand.Parameters.Add(new SqlParameter("@Month_", SqlDbType.Int, 4, ParameterDirection.Input, 10, 0, null, DataRowVersion.Current, false, null, "", "", ""));
            sqlCommand.Parameters.Add(new SqlParameter("@Day_", SqlDbType.Int, 4, ParameterDirection.Input, 10, 0, null, DataRowVersion.Current, false, null, "", "", ""));
            sqlCommand.Parameters.Add(new SqlParameter("@ShiftNew", SqlDbType.Int, 4, ParameterDirection.InputOutput, 10, 0, null, DataRowVersion.Current, false, null, "", "", ""));
            sqlCommand.Parameters.Add(new SqlParameter("@Calen_T", SqlDbType.Int, 4, ParameterDirection.InputOutput, 10, 0, null, DataRowVersion.Current, false, null, "", "", ""));
            sqlCommand.Parameters[1].Value = (groupFirst.HasValue ? (object) groupFirst.Value : DBNull.Value);
            sqlCommand.Parameters[2].Value = (empNo.HasValue ? (object) empNo.Value : DBNull.Value);
            sqlCommand.Parameters[3].Value = (dateStart.HasValue ? (object) dateStart.Value : DBNull.Value);
            sqlCommand.Parameters[4].Value = (year.HasValue ? (object) year.Value : DBNull.Value);
            sqlCommand.Parameters[5].Value = (month.HasValue ? (object) month.Value : DBNull.Value);
            sqlCommand.Parameters[6].Value = (day.HasValue ? (object) day.Value : DBNull.Value);
            sqlCommand.Parameters[7].Value = (shiftNew.HasValue ? (object) shiftNew.Value : DBNull.Value);
            sqlCommand.Parameters[8].Value = (calenT.HasValue ? (object) calenT.Value : DBNull.Value);
            ConnectionState state = sqlCommand.Connection.State;
            if ((sqlCommand.Connection.State & ConnectionState.Open) != ConnectionState.Open)
            {
                sqlCommand.Connection.Open();
            }
            object obj;
            try
            {
                obj = sqlCommand.ExecuteScalar();
            }
            finally
            {
                if (state == ConnectionState.Closed)
                {
                    sqlCommand.Connection.Close();
                }
            }
            groupFirst = ((sqlCommand.Parameters[1].Value == null || sqlCommand.Parameters[1].Value is DBNull) ? null : new int?((int)sqlCommand.Parameters[1].Value));
            shiftNew = ((sqlCommand.Parameters[7].Value == null || sqlCommand.Parameters[7].Value is DBNull) ? null : new int?((int)sqlCommand.Parameters[7].Value));
            calenT = ((sqlCommand.Parameters[8].Value == null || sqlCommand.Parameters[8].Value is DBNull) ? null : new int?((int)sqlCommand.Parameters[8].Value));
            if (obj == null || obj is DBNull)
            {
                return null;
            }
            return obj;
        }
        // Token: 0x0600010A RID: 266 RVA: 0x00008520 File Offset: 0x00006720
        //public DataRow GetDataByEmpNo(double empNo)
        //{
        //    var constre = ConfigurationManager.ConnectionStrings["connectionStringProviderSystem"].ToString();
        //    var connection = new SqlConnection(constre);

        //    if (connection.State == ConnectionState.Open)
        //    {
        //        connection.Close();
        //    }
        //    SqlCommand cmd = new SqlCommand();
        //    SqlDataAdapter da = new SqlDataAdapter();
        //    DataTable dt = new DataTable();
        //    cmd = new SqlCommand("WF_EMPLOYEEGetDataByEmpNO", connection);
        //    cmd.Parameters.Add(new SqlParameter("@EMP_NO", empNo));
        //    cmd.CommandType = CommandType.StoredProcedure;
        //    da.SelectCommand = cmd;
        //    da.Fill(dt);
        //    return dt.Rows[0];
        //    //return base.ExecuteDataRow("WF_EMPLOYEEGetDataByEmpNO", new object[]
        //    //{
        //    //    empNo
        //    //});
        //}
        public ParmfileRow GetDataRow()
        {
            ParmfileRow result = null;
            var constre = ConfigurationManager.ConnectionStrings["connectionStringProviderSystem"].ToString();
            var connection = new SqlConnection(constre);

            if (connection.State == ConnectionState.Open)
            {
                connection.Close();
            }

            string query = @"WF_PARMFILEGetData";

            connection.Open();
            var command = new SqlCommand(query, connection);

            //var result = command.ExecuteReader();
            using (SqlDataReader sqlDataReader = command.ExecuteReader())
            {
                if (sqlDataReader.Read())
                {
                    result = new ParmfileRow
                    {
                        MyEste = Convert.ToInt32((sqlDataReader["M_Y_ESTE"] is DBNull || sqlDataReader["M_Y_ESTE"] == null) ? 0 : sqlDataReader["M_Y_ESTE"]),
                        EmpEste = Convert.ToInt16((sqlDataReader["EMP_ESTE"] is DBNull || sqlDataReader["EMP_ESTE"] == null) ? 0 : sqlDataReader["EMP_ESTE"]),
                        IsMorDay = Convert.ToBoolean(sqlDataReader["IS_MOR_DAY"]),
                        CodePlace = Convert.ToInt64((sqlDataReader["CodePlace"] is DBNull || sqlDataReader["CodePlace"] == null) ? 0 : sqlDataReader["CodePlace"]),
                        StartDay = Convert.ToInt16((sqlDataReader["START_DAY"] is DBNull || sqlDataReader["START_DAY"] == null) ? 0 : sqlDataReader["START_DAY"]),
                        Domain = Convert.ToString(sqlDataReader["Domain"]),
                        DomainFieldAdmin = Convert.ToString(sqlDataReader["DomainFieldAdmin"]),
                        DomainFieldEmpNo = Convert.ToString(sqlDataReader["DomainFieldEmpNO"]),
                        Tolerance = Convert.ToInt32((sqlDataReader["Tolerance"] is DBNull || sqlDataReader["Tolerance"] == null) ? 0 : sqlDataReader["Tolerance"]),
                        WfRequestMandeControl = Convert.ToBoolean(sqlDataReader["WFRequestMandeControl"]),
                        KasrEsth = Convert.ToInt16((sqlDataReader["KASR_ESTH"] is DBNull || sqlDataReader["KASR_ESTH"] == null) ? 0 : sqlDataReader["KASR_ESTH"]),
                        EndDateRamedan = Convert.ToSingle((sqlDataReader["EndDateRamedan"] is DBNull || sqlDataReader["EndDateRamedan"] == null) ? 0 : sqlDataReader["EndDateRamedan"]),
                        StartDateRamedan = Convert.ToSingle((sqlDataReader["StartDateRamedan"] is DBNull || sqlDataReader["StartDateRamedan"] == null) ? 0 : sqlDataReader["StartDateRamedan"]),
                        MorDebt = Convert.ToInt16((sqlDataReader["MOR_DEBT"] is DBNull || sqlDataReader["MOR_DEBT"] == null) ? 0 : sqlDataReader["MOR_DEBT"]),
                        DifKasrEsth = Convert.ToBoolean(sqlDataReader["difKasrEsth"]),
                        DayArchive = Convert.ToInt32((sqlDataReader["DayArchive"] is DBNull || sqlDataReader["DayArchive"] == null) ? 0 : sqlDataReader["DayArchive"]),
                        WfIsAttachment = Convert.ToBoolean(sqlDataReader["WFIsAttachment"]),
                        DayArchiveKind = Convert.ToBoolean(sqlDataReader["DayArchiveKind"]),
                        WfParallelApprove = Convert.ToBoolean((sqlDataReader["WF_ParallelApprove"] is DBNull || sqlDataReader["WF_ParallelApprove"] == null) ? 0 : sqlDataReader["WF_ParallelApprove"]),
                        InvalidMonthToEdit = Convert.ToInt32((sqlDataReader["InvalidMonthToEdit"] is DBNull || sqlDataReader["InvalidMonthToEdit"] == null) ? 0 : sqlDataReader["InvalidMonthToEdit"]),
                        MmDate = (double)Convert.ToInt32((sqlDataReader["M_M_DATE"] is DBNull || sqlDataReader["M_M_DATE"] == null) ? 0 : sqlDataReader["M_M_DATE"]),
                        WfLimitMonthShowDaily = Convert.ToInt16((sqlDataReader["WFLimitMonthShowDaily"] is DBNull || sqlDataReader["WFLimitMonthShowDaily"] == null) ? 0 : sqlDataReader["WFLimitMonthShowDaily"]),
                        WfIsChangeUserName = Convert.ToBoolean(sqlDataReader["WFIsChangeUserName"]),
                        WfDifrenceRequest = Convert.ToBoolean(sqlDataReader["WFDifrenceRequest"]),
                        CnoEqPno = Convert.ToBoolean(sqlDataReader["CNO_EQ_PNO"]),
                        WfNeedDesc = Convert.ToBoolean(sqlDataReader["WFNeedDesc"]),
                        RegisterTolerance = Convert.ToInt16((sqlDataReader["RegisterTolerance"] is DBNull || sqlDataReader["RegisterTolerance"] == null) ? 0 : sqlDataReader["RegisterTolerance"]),
                        ShowRemainKardex = Convert.ToBoolean(sqlDataReader["ShowRemainKardex"])
                    };
                }
            }
            connection.Close();
            connection.Dispose();
            return result;
        }
        // Token: 0x06000161 RID: 353 RVA: 0x0000B454 File Offset: 0x00009654
        public double UsedMorMam(double empNo, int typ, double sDate, double eDate)
        {
            var resultService = 0.0;
            var constre = ConfigurationManager.ConnectionStrings["connectionStringProviderSystem"].ToString();
            var connection = new SqlConnection(constre);
            if (connection.State == ConnectionState.Open)
            {
                connection.Close();
            }
            connection.Open();
            var command = new SqlCommand("UsedMorMam", connection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add("@Emp_No", SqlDbType.Float, 2000).Value = empNo;
            command.Parameters.Add("@S_date", SqlDbType.Float, 2000).Value = sDate;
            command.Parameters.Add("@typ", SqlDbType.Int, 2000).Value = typ;
            command.Parameters.Add("@E_date", SqlDbType.Float, 2000).Value = eDate;
        
            var result = command.ExecuteReader();
            while (result.Read())
            {
                resultService = double.Parse(result[0].ToString());
            }
            connection.Close();
            connection.Dispose();
            //object obj = base.ExecuteScalar("UsedMorMam", new object[]
            //{
            //    empNo,
            //    typ,
            //    sDate,
            //    eDate
            //});
            //if (!(obj.ToString() == ""))
            //{
            //    return Convert.ToDouble(obj);
            //}
            return resultService;
        }
        public bool CheckEsteh(double empNo, DateTime startDate, DateTime endDate, ref string remain, ref string msg)
        {
            ParmfileRow dataRow = GetDataRow();
            if (dataRow.MyEste.ToString() == "" || dataRow.EmpEste.ToString() == "")
            {
                msg = "سقف مرخصی استعلاجی نا مشخص می باشد";
                return false;
            }
            bool result = true;
            double sDate = ShDate.StrToDate(ShDate.YearOf(startDate) + "/01/01").Date.ToOADate();
            double eDate = ShDate.StrToDate(ShDate.YearOf(endDate) + "/12/29").Date.ToOADate();
            double num = UsedMorMam(empNo, 60, sDate, eDate);
            int num2 = ShDate.DifDays(endDate, startDate) + 1;
            if ((double)dataRow.MyEste >= num + (double)num2)
            {
                DateTime startDate2;
                DateTime dateTime;
                if (ShDate.DayOf(startDate) < dataRow.StartDay)
                {
                    startDate2 = ShDate.StrToDate(string.Format("{0}/{1}/{2}", UtilityManagers.ConvertToFourDigitNumber(ShDate.YearOf(startDate).ToString()), UtilityManagers.ConvertToTowDigitNumber((int)(ShDate.MonthOf(startDate) - 1)), UtilityManagers.ConvertToTowDigitNumber((int)dataRow.StartDay)));
                    dateTime = ShDate.StrToDate(string.Format("{0}/{1}/{2}", UtilityManagers.ConvertToFourDigitNumber(ShDate.YearOf(startDate).ToString()), UtilityManagers.ConvertToTowDigitNumber((int)ShDate.MonthOf(startDate)), UtilityManagers.ConvertToTowDigitNumber((int)dataRow.StartDay))).AddDays(-1.0);
                }
                else
                {
                    startDate2 = ShDate.StrToDate(string.Format("{0}/{1}/{2}", UtilityManagers.ConvertToFourDigitNumber(ShDate.YearOf(startDate).ToString()), UtilityManagers.ConvertToTowDigitNumber((int)ShDate.MonthOf(startDate)), UtilityManagers.ConvertToTowDigitNumber((int)dataRow.StartDay)));
                    dateTime = ShDate.StrToDate(string.Format("{0}/{1}/{2}", UtilityManagers.ConvertToFourDigitNumber(ShDate.YearOf(startDate).ToString()), UtilityManagers.ConvertToTowDigitNumber((int)(ShDate.MonthOf(startDate) + 1)), UtilityManagers.ConvertToTowDigitNumber((int)dataRow.StartDay))).AddDays(-1.0);
                }
                if (endDate <= dateTime)
                {
                    double num3 = UsedMorMam(empNo, 60, startDate2.Date.ToOADate(), dateTime.Date.ToOADate());
                    if ((double)dataRow.EmpEste < num3 + (double)num2)
                    {
                        msg = "مدت در خواست استعلاجی بیشتر از سقف مجاز است";
                        result = false;
                    }
                }
                else
                {
                    int num4 = ShDate.DifDays(startDate, dateTime);
                    double num3 = UsedMorMam(empNo, 60, startDate.ToOADate(), dateTime.ToOADate());
                    if ((double)dataRow.EmpEste >= num3 + (double)num4)
                    {
                        int num5 = ShDate.DifDays(startDate2, endDate);
                        num3 = UsedMorMam(empNo, 60, startDate2.AddMonths(1).ToOADate(), dateTime.AddMonths(1).ToOADate());
                        if ((double)dataRow.EmpEste < num3 + (double)num5)
                        {
                            msg = "مدت در خواست استعلاجی بیشتر از سقف مجاز است";
                            result = false;
                        }
                    }
                    else
                    {
                        msg = "مدت در خواست استعلاجی بیشتر از سقف مجاز است";
                        result = false;
                    }
                }
            }
            else
            {
                msg = "مدت در خواست استعلاجی بیشتر از سقف مجاز است";
                result = false;
            }
            remain = string.Format("مانده مرخصی استعلاجی برابر {0} روز می باشد.", (double)dataRow.EmpEste - (num + (double)num2));
            return result;
        }
        public int EnoughLimitValue(double empNo, DateTime sdate, short? type, string duration)
        {
            int result = 0;
            DataRow expr_12 = GetDataByCardNo(type);
            bool flag = Convert.ToBoolean(expr_12["IS_DAY"]);
            int num = Convert.ToInt32(expr_12["LimitValue"]);
            if (flag)
            {
                int num2;
                int.TryParse(duration, out num2);
                if (Convert.ToInt32(this.GetRequestDuration(empNo, sdate, type)["TotalValues"]) + num2 > num)
                {
                    result = -4;
                }
            }
            else
            {
                if (duration.Contains(":"))
                {
                    duration = duration.Replace(":", string.Empty);
                }
                string text = UtilityManagers.ConvertToFourDigitNumber(duration);
                if (new TimeSpan(Convert.ToInt32(text.Substring(0, 2)), Convert.ToInt32(text.Substring(2, 2)), 0).TotalHours > (double)num)
                {
                    result = -4;
                }
            }
            return result;
        }

        // Token: 0x060001AA RID: 426 RVA: 0x0001153E File Offset: 0x0000F73E
        public DataRow GetRequestDuration(double empNo, DateTime sdate, short? cType)
        {
            var constre = ConfigurationManager.ConnectionStrings["connectionStringProviderSystem"].ToString();
            var connection = new SqlConnection(constre);

            if (connection.State == ConnectionState.Open)
            {
                connection.Close();
            }
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = new DataTable();
         
                cmd = new SqlCommand("WF_GetRequestDuration", connection);
                cmd.Parameters.Add(new SqlParameter("@EMP_NO", empNo));
            cmd.Parameters.Add(new SqlParameter("@Sdate", sdate));
            cmd.Parameters.Add(new SqlParameter("@Type", cType));
            cmd.CommandType = CommandType.StoredProcedure;
                da.SelectCommand = cmd;
                da.Fill(dt);
                return dt.Rows[0];
            //return base.ExecuteDataRow("WF_GetRequestDuration", new object[]
            //{
            //    empNo,
            //    sdate,
            //    cType
            //});
        }
        public int GetRequestValidDays(ActionEnum actionStatus)
        {
            int num = Convert.ToInt32(actionStatus);
            var foundData = false;
            var constre = ConfigurationManager.ConnectionStrings["connectionStringProviderSystem"].ToString();
            var connection = new SqlConnection(constre);

            if (connection.State == ConnectionState.Open)
            {
                connection.Close();
            }

            string query = @"SELECT a.RequestValidDays  FROM [Action] a WHERE a.ActionId=" + num;

            connection.Open();
            var command = new SqlCommand(query, connection);

            var result = command.ExecuteReader();
            while (result.Read())
            {
                num = int.Parse(result["RequestValidDays"].ToString());
                foundData = true;

            }
            connection.Close();
            connection.Dispose();

            //object obj = base.ExecuteScalar(CommandType.Text, "SELECT a.RequestValidDays  FROM [Action] a WHERE a.ActionId=" + num);
            if (foundData)
            {
                return num;
            }
            return 31;
        }
        public int GetLimitCount(ActionEnum actionStatus)
        {
            int resultdata=0;
            try
            {
                int num = Convert.ToInt32(actionStatus);
                // result = Convert.ToInt32(base.ExecuteScalar(CommandType.Text, "SELECT a.LimitCount  FROM [Action] a WHERE a.ActionId=" + num));
                var constre = ConfigurationManager.ConnectionStrings["connectionStringProviderSystem"].ToString();
                var connection = new SqlConnection(constre);

                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }

                string query = @"SELECT a.LimitCount  FROM [Action] a WHERE a.ActionId=" + num;

                connection.Open();
                var command = new SqlCommand(query, connection);

                var result = command.ExecuteReader();
                while (result.Read())
                {
                    resultdata = int.Parse(result["LimitCount"].ToString());


                }
                connection.Close();
                connection.Dispose();

            }
            catch
            {
                resultdata = 10;
            }
            return resultdata;
        }
        public int GetRequestsCount(double curEmpNo, ActionEnum actionStatus)
        {
            int resultData=0;
            try
            {
                int num = Convert.ToInt32(actionStatus);
                //object obj = base.ExecuteScalar("WF_RequestsCountByEmpNO", new object[]
                //{
                //    curEmpNo,
                //    num
                //});
                var constre = ConfigurationManager.ConnectionStrings["connectionStringProviderSystem"].ToString();
                var connection = new SqlConnection(constre);
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
                connection.Open();
                var command = new SqlCommand("WF_RequestsCountByEmpNO", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@EMP_NO", SqlDbType.VarChar, 2000).Value = curEmpNo;
                command.Parameters.Add("@ActionID", SqlDbType.VarChar, 2000).Value = num;

                var result = command.ExecuteReader();
                while (result.Read())
                {
                    resultData = int.Parse(result[0].ToString());
                }
                connection.Close();
                connection.Dispose();
               
            }
            catch
            {
                resultData = 0;
            }
            return resultData;
        }
        public DataRow GetDataByEmpNo(double empNo)
        {
            var constre = ConfigurationManager.ConnectionStrings["connectionStringProviderSystem"].ToString();
            var connection = new SqlConnection(constre);

            if (connection.State == ConnectionState.Open)
            {
                connection.Close();
            }
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = new DataTable();
            cmd = new SqlCommand("WF_EMPLOYEEGetDataByEmpNO", connection);
            cmd.Parameters.Add(new SqlParameter("@EMP_NO", empNo));
            cmd.CommandType = CommandType.StoredProcedure;
            da.SelectCommand = cmd;
            da.Fill(dt);
            return dt.Rows[0];
            //return base.ExecuteDataRow("WF_EMPLOYEEGetDataByEmpNO", new object[]
            //{
            //    empNo
            //});
        }
        public double GetManagerOfSection(double empNo, ref string secNo)
        {
            DataTable data = this.GetData();
            if (data.Rows != null && data.Rows.Count > 0)
            {
                GetEmpManager(data, secNo, empNo);
                secNo = this._curSec;
                return this._mgrEmpNo;
            }
            return empNo;
        }

        public DataRow GetDataByCardNo(short? cardNo)
        {
            var constre = ConfigurationManager.ConnectionStrings["connectionStringProviderSystem"].ToString();
            var connection = new SqlConnection(constre);

            if (connection.State == ConnectionState.Open)
            {
                connection.Close();
            }
            DataRow outputResult;
            string query = @"SELECT TOP 1 c.* FROM CARDS c WHERE c.CARD_NO = " + cardNo;

            connection.Open();
            //var command = new SqlCommand(query, connection);
            DataTable table = new DataTable();
            using (var da = new SqlDataAdapter(query, connection))
            {
                da.Fill(table);
                outputResult = table.Rows[0];
            }

            //var result = command.ExecuteReader();

            //while (result.Read())
            //{
            //    orgName = result["TITLE"].ToString();


            //}
            connection.Close();
            connection.Dispose();
          
            return outputResult;
        }

        public DataTable GetData()
        {
            var constre = ConfigurationManager.ConnectionStrings["connectionStringProviderSystem"].ToString();
            var connection = new SqlConnection(constre);

            if (connection.State == ConnectionState.Open)
            {
                connection.Close();
            }
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = new DataTable();
            cmd = new SqlCommand("WF_SECTIONSGetData", connection);
           // cmd.Parameters.Add(new SqlParameter("@EMP_NO", empNo));
            cmd.CommandType = CommandType.StoredProcedure;
            da.SelectCommand = cmd;
            da.Fill(dt);
            return dt;
            //return base.ExecuteDataTable("WF_SECTIONSGetData", null);
        }

        // Token: 0x060001E7 RID: 487 RVA: 0x0001484C File Offset: 0x00012A4C
        private void GetEmpManager(DataTable dt, string secNo, double empNo)
        {
            DataRow[] array = dt.Select("SEC_NO=" + secNo);
            if (array.Length != 0 && !(array[0]["EmpManager"] is DBNull))
            {
                DataRow dataRow = array[0];
                if (Convert.ToDouble(dataRow["EmpManager"]) <= 0.0 || Convert.ToDouble(dataRow["EmpManager"]) == empNo)
                {
                    if (Convert.ToDouble(dataRow["EmpManager"]) == empNo)
                    {
                        this._mgrEmpNo = Convert.ToDouble(dataRow["EmpManager"]);
                        this._curSec = secNo;
                    }
                    this.GetEmpManager(dt, dataRow["TMFather"].ToString(), empNo);
                    return;
                }
                this._mgrEmpNo = Convert.ToDouble(dataRow["EmpManager"]);
                this._curSec = secNo;
            }
        }


        // Token: 0x060000D4 RID: 212 RVA: 0x00005D48 File Offset: 0x00003F48
        public DataRow GetConferment(double empManager, int actionId, int secNo)
        {
            DataRow result;
            try
            {
                var constre = ConfigurationManager.ConnectionStrings["connectionStringProviderSystem"].ToString();
                var connection = new SqlConnection(constre);

                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
                DataRow outputResult;
                string commandText = string.Concat(new object[]
                {
                    "SELECT * FROM Conferment c WHERE GETDATE() BETWEEN c.StartDate AND c.EndDate AND c.EmpManager=",
                    empManager,
                    " AND c.ActionID=",
                    actionId,
                    " AND c.IsDelete=0 AND c.SEC_NO=",
                    secNo
                });

                connection.Open();
                //var command = new SqlCommand(query, connection);
                DataTable table = new DataTable();
                using (var da = new SqlDataAdapter(commandText, connection))
                {
                    da.Fill(table);
                    outputResult = table.Rows[0];
                }

                //var result = command.ExecuteReader();

                //while (result.Read())
                //{
                //    orgName = result["TITLE"].ToString();


                //}
                connection.Close();
                connection.Dispose();

                return outputResult;
                //string commandText = string.Concat(new object[]
                //{
                //    "SELECT * FROM Conferment c WHERE GETDATE() BETWEEN c.StartDate AND c.EndDate AND c.EmpManager=",
                //    empManager,
                //    " AND c.ActionID=",
                //    actionId,
                //    " AND c.IsDelete=0 AND c.SEC_NO=",
                //    secNo
                //});
                //result = base.ExecuteDataRow(CommandType.Text, commandText);
            }
            catch
            {
                result = null;
            }
            return result;
        }
        public string SetStartTimeShift(double empNumb,DateTime requestDate)
        {
           
                TimeSpan[] startTimeAndEndTimeByDate = WorkFlowsDatabase.Instance.Users.GetStartTimeAndEndTimeByDate(empNumb, requestDate);
                if (startTimeAndEndTimeByDate != null && startTimeAndEndTimeByDate.Length != 0)
                {
                    //this.chbIsFirstTimeShift.Text = string.Concat(new object[]
                    //{
                    //    "مرخصی اول وقت می باشد , شروع شیفت ",
                    //    startTimeAndEndTimeByDate[0].Hours,
                    //    ":",
                    //    startTimeAndEndTimeByDate[0].Minutes
                    //});
                    return  startTimeAndEndTimeByDate[0].Hours.ToString().PadLeft(2, '0') + ":" + startTimeAndEndTimeByDate[0].Minutes.ToString().PadLeft(2, '0');
                  
                }
            return "00:00";
        }

        public ResultViewModel RegisterTimeLeaveRequest(string stratTime,string duration,DateTime requestDate,string empNumber,string description,string cardId,string onlineUser, bool isFirstTimeShift=false, bool isInsert = true, int requestId = 0)
        {
            try
            {
                int num = 0;
                int num2 = Convert.ToInt32(stratTime.Replace(":", ""));
                if (num2 < 1 || num2 >= 2400)
                {
                  
                    return new ResultViewModel
                    {
                        Validate = false,
                        ValidateMessage = "ساعت شروع را صحیح وارد نمایید"
                    };
                }
                int num3 = Convert.ToInt32(duration.Replace(":", ""));
                if (num3 < 1 || num3 >= 2400)
                {
                   return new ResultViewModel
                    {
                        Validate = false,
                        ValidateMessage = "مدت را صحیح وارد نمایید"
                    };
                }
                TimeSpan timeSpan;
                if (!TimeSpan.TryParse(duration, out timeSpan) || timeSpan.TotalMinutes <= 0.0)
                {
                   return new ResultViewModel
                    {
                        Validate = false,
                        ValidateMessage = "مدت را صحیح وارد نمایید"
                    };
                }
                TimeSpan ts;
                if (!TimeSpan.TryParse(stratTime, out ts) || ts.TotalMinutes <= 0.0)
                {
                   return new ResultViewModel
                    {
                        Validate = false,
                        ValidateMessage = "ساعت شروع را صحیح وارد نمایید"
                    };
                }
                string text = Convert.ToInt32(timeSpan.Hours + UtilityManagers.ConvertToTowDigitNumber(timeSpan.Minutes)).ToString();
                if (Convert.ToInt32(text) <= 0)
                {
                    return new ResultViewModel
                    {
                        Validate = false,
                        ValidateMessage = "مدت نمی تواند صفر باشد"
                    };
                }
                TimeSpan timeSpan2 = timeSpan.Add(ts);
                //if (timeSpan2.Days > 0)
                //{
                //    if (timeSpan2.Days == 1)
                //    {
                //        this.lblEndTime.Text = "روز بعد";
                //    }
                //    else
                //    {
                //        this.lblEndTime.Text = timeSpan2.Days + " روز بعد";
                //    }
                //}
                //else
                //{
                //    this.lblEndTime.Text = "";
                //}
                int num4 = Convert.ToInt32(timeSpan2.Hours + UtilityManagers.ConvertToTowDigitNumber(timeSpan2.Minutes));
               
                    int num5 = this.OverLapTime(num3,stratTime,requestDate,empNumber);
                    if (num5 > 0 &&isInsert)
                    {
                       return new ResultViewModel
                        {
                            Validate = false,
                            ValidateMessage = "در بازه درخواست شده،" + num5 + " مجوز ساعتی ثبت شده است!"
                        };
                }
                duration = duration.Replace(":", string.Empty);
                if (isInsert)
                {
                    num = InsertRequest(double.Parse(empNumber), Convert.ToInt16(cardId), requestDate.Date,
                        requestDate.Date, new int?(num2), new int?(num4), duration, double.Parse(onlineUser), 5,
                        description, ActionEnum.TimeLeave, StatusEnum.Unknown, IoStatusEnum.Unknown, string.Empty,
                        string.Empty, null, null, null, null, isFirstTimeShift);
                }
                else
                {
                    num = UpdateRequest(requestId,double.Parse(empNumber), Convert.ToInt16(cardId), requestDate.Date,
                        requestDate.Date, new int?(num2), new int?(num4), duration, double.Parse(onlineUser), 5,
                        description, ActionEnum.TimeLeave, StatusEnum.Unknown, IoStatusEnum.Unknown, string.Empty,
                        string.Empty, null, null, null, null, isFirstTimeShift);
                }
                   

                switch (num)
                {
                    case -5:
                       return new ResultViewModel
                        {
                            Validate = false,
                            ValidateMessage = "تعداد درخواستهای شما از سقف مجاز بیشتر می باشد."
                        };
                    case -4:
                        return new ResultViewModel
                        {
                            Validate = false,
                            ValidateMessage = "مدت مجوز درخواستی مجاز نمی باشد"
                        };
                    case -3:
                        
                        return new ResultViewModel
                        {
                            Validate = false,
                            ValidateMessage = "درخواستهای " + (DateTime.Now - requestDate.Date).Days + " روز قبل را نمی توانید ثبت کنید\nبا مدیر سیستم صحبت کنید"
                        };
                    case -2:
                        return new ResultViewModel
                        {
                            Validate = false,
                            ValidateMessage = "اطلاعات ثبت نشد\n مدیری برای شما وجود ندارد"
                        };
                    case -1:
                        return new ResultViewModel
                        {
                            Validate = false,
                            ValidateMessage = "اطلاعات ثبت نشد"
                        };
                    default:
                        return new ResultViewModel
                        {
                            Validate = true,
                            ValidateMessage = "ثبت با موفقیت انجام شد"
                        };
                      
                }
            
            }
            catch (Exception ex)
            {
                
               return new ResultViewModel
               {
                   Validate = false,
                   ExceptionMessage = ex.Message
               };
            }
        }

        public ResultViewModel RegisterHrMissonRequest(string empNumber,string codeId,DateTime sdates,string sTime,string duration,string priceMisson,string toVehicle,string returnVehicle,string description,string onlineUser,bool isFirstTimeShift=false, bool isInsert = true, int requestId = 0)
        {
            try
            {
                int num = 0;
                int num2 = Convert.ToInt32(sTime.Replace(":", ""));
                if (num2 < 1 || num2 >= 2400)
                {
                    //this.lblErr.Text = "ساعت شروع را صحیح وارد نمایید";
                    return new ResultViewModel
                    {
                        Validate = false,
                        ValidateMessage = "ساعت شروع را صحیح وارد نمایید"
                    };
                }
                int num3 = Convert.ToInt32(duration.Replace(":", ""));
                if (num3 < 1 || num3 >= 2400)
                {
                    //this.lblErr.Text = "مدت را صحیح وارد نمایید";
                    return new ResultViewModel
                    {
                        Validate = false,
                        ValidateMessage = "مدت را صحیح وارد نمایید"
                    };
                }
                TimeSpan timeSpan;
                if (!TimeSpan.TryParse(duration, out timeSpan) || timeSpan.TotalMinutes <= 0.0)
                {
                   // this.lblErr.Text = "مدت را صحیح وارد نمایید";
                    return new ResultViewModel
                    {
                        Validate = false,
                        ValidateMessage = "مدت را صحیح وارد نمایید"
                    };
                }
                TimeSpan ts;
                if (!TimeSpan.TryParse(sTime, out ts) || ts.TotalMinutes <= 0.0)
                {
                    //this.lblErr.Text = "ساعت شروع را صحیح وارد نمایید";
                    return new ResultViewModel
                    {
                        Validate = false,
                        ValidateMessage = "ساعت شروع را صحیح وارد نمایید"
                    };
                }
                string text = Convert.ToInt32(timeSpan.Hours + UtilityManagers.ConvertToTowDigitNumber(timeSpan.Minutes)).ToString();
                if (Convert.ToInt32(text) <= 0)
                {
                    //this.lblErr.Text = "مدت نمی تواند صفر باشد";
                    return new ResultViewModel
                    {
                        Validate = false,
                        ValidateMessage = "مدت نمی تواند صفر باشد"
                    };
                }
                TimeSpan timeSpan2 = timeSpan.Add(ts);
                //if (timeSpan2.Days > 0)
                //{
                //    if (timeSpan2.Days == 1)
                //    {
                //        this.lblEndTime.Text = "روز بعد";
                //    }
                //    else
                //    {
                //        this.lblEndTime.Text = timeSpan2.Days + " روز بعد";
                //    }
                //}
                //else
                //{
                //    this.lblEndTime.Text = "";
                //}
                int num4 = Convert.ToInt32(timeSpan2.Hours + UtilityManagers.ConvertToTowDigitNumber(timeSpan2.Minutes));
                short value;
                short.TryParse(toVehicle, out value);
                short value2;
                short.TryParse(returnVehicle, out value2);
                double? transferCost = null;
                if (!string.IsNullOrEmpty(priceMisson))
                {
                    priceMisson = ToEnglishNumber(priceMisson);
                    transferCost = new double?(Convert.ToDouble(priceMisson));
                }
               
                    int num5 = OverLapTime(num3, sTime,sdates, empNumber);
                    if (num5 > 0&& isInsert)
                    {
                       // this.lblErr.Text = "در بازه درخواست شده،" + num5 + " مجوز ساعتی ثبت شده است!";
                    return new ResultViewModel
                    {
                        Validate = false,
                        ValidateMessage = "در بازه درخواست شده،" + num5 + " مجوز ساعتی ثبت شده است!"
                    };
                }

                if (isInsert)
                {
                    // num = InsertRequest(this.CurrentEmpNo, Convert.ToInt16(this.EditCards.SelectedValue), this.EditRequestDate.Date, null, new int?(num2), new int?(num4), text, base.Mc.UserData.EmpNo, 3, this.Edit_Description.Text, ActionEnum.TimeDuty, null, null, new short?(value), new short?(value2), null, transferCost, this.chbIsFirstTimeShift.Checked);
                    num = InsertRequest(double.Parse(empNumber), Int16.Parse(codeId),
                        sdates.Date,
                        null, num2, null,
                        num3.ToString(CultureInfo.InvariantCulture), double.Parse(onlineUser), 3,
                        description, ActionEnum.TimeDuty, StatusEnum.Unknown, IoStatusEnum.Unknown, toVehicle,
                        returnVehicle, new short?(value), new short?(value2),
                        null, transferCost, false);

                }
                else
                {
                    num = UpdateRequest(requestId,double.Parse(empNumber), Int16.Parse(codeId),
                      sdates.Date,
                      null, num2, null,
                      num3.ToString(CultureInfo.InvariantCulture), double.Parse(onlineUser), 3,
                      description, ActionEnum.TimeDuty, StatusEnum.Unknown, IoStatusEnum.Unknown, toVehicle,
                      returnVehicle, new short?(value), new short?(value2),
                      null, transferCost, false);
                }

                switch (num)
                {
                    case -5:
                       // this.ShowMessage("تعداد درخواستهای شما از سقف مجاز بیشتر می باشد.");
                        return new ResultViewModel
                        {
                            Validate = false,
                            ValidateMessage = "تعداد درخواستهای شما از سقف مجاز بیشتر می باشد."
                        };
                    case -4:
                       // this.ShowMessage("مدت مجوز درخواستی مجاز نمی باشد");
                        return new ResultViewModel
                        {
                            Validate = false,
                            ValidateMessage = "مدت مجوز درخواستی مجاز نمی باشد"
                        };
                    case -3:
                       // this.ShowMessage("درخواستهای " + (DateTime.Now - this.EditRequestDate.Date).Days + " روز قبل را نمی توانید ثبت کنید\nبا مدیر سیستم صحبت کنید");
                        return new ResultViewModel
                        {
                            Validate = false,
                            ValidateMessage = "درخواستهای " + (DateTime.Now - sdates.Date).Days + " روز قبل را نمی توانید ثبت کنید\nبا مدیر سیستم صحبت کنید"
                        };
                    case -2:
                       // this.ShowMessage("اطلاعات ثبت نشد\n مدیری برای شما وجود ندارد");
                        return new ResultViewModel
                        {
                            Validate = false,
                            ValidateMessage = "اطلاعات ثبت نشد\n مدیری برای شما وجود ندارد"
                        };
                    case -1:
                       // this.ShowMessage("اطلاعات ثبت نشد");
                        return new ResultViewModel
                        {
                            Validate = false,
                            ValidateMessage = "اطلاعات ثبت نشد"
                        };
                    default:
                        return new ResultViewModel
                        {
                            Validate = true,
                            ValidateMessage = "اطلاعات با موفقیت ثبت شد"
                        };
                        
                }
            
            }
            catch (Exception ex)
            {

                return new ResultViewModel
                {
                    Validate = false,
                    ExceptionMessage = ex.Message
                };
            }
        }

        public ResultViewModel RegisterAddReduceHolidays(string empNumber, string codeId, DateTime sdates,  string duration, string typeRequest, string description,string onlineUser, bool isFirstTimeShift = false, bool isInsert = true, int requestId = 0)
        {
            try
            {
                ParmfileRow paramFileRows = GetDataRow();
                var validateOfRequest = WorkFlowsDatabase.Instance.Action.GetValid(double.Parse(empNumber));
                var timeRequest = validateOfRequest.FirstOrDefault(x => x.ActionId == (int)ActionEnum.LeaveAddition);
                var requiredDesc = false;
                if (timeRequest != null)
                {
                    requiredDesc = timeRequest.NeedDesc;
                }
                if (requiredDesc && string.IsNullOrEmpty(description))
                {
                    //this.lblErr.Text = "توضیحات درخواست را وارد نمایید";
                    return new ResultViewModel
                    {
                        Validate = false,
                        ValidateMessage = "توضیحات درخواست را وارد نمایید"
                    };
                }
                int num = 0;
                string text = duration;
                try
                {
                    string[] array = text.Split(new char[]
                    {
                    ':'
                    });
                    if (array.Length != 0 && Convert.ToInt32(array[1]) > 59)
                    {
                       // this.lblErr.Text = "مدت دخواست را صحیح وارد نمایید";
                        return new ResultViewModel
                        {
                            Validate = false,
                            ValidateMessage = "مدت دخواست را صحیح وارد نمایید"
                        };
                    }
                }
                catch
                {
                    return new ResultViewModel
                    {
                        Validate = false,
                        ValidateMessage = "مدت دخواست را صحیح وارد نمایید"
                    };
                }
                text = text.Replace(":", "");
                int value = int.Parse(typeRequest);
                if (Convert.ToDouble(text) > 0.0)
                {

                    //  num = InsertRequest(this.CurrentEmpNo, 104, this.EditRequestDate.Date, null, new int?(value), null, text, base.Mc.UserData.EmpNo, 5, this.EditDescription.Text, ActionEnum.LeaveAddition, false);
                    if (isInsert)
                    {
                            num = InsertRequest(double.Parse(empNumber), Int16.Parse(codeId),
                        sdates.Date,
                        null, new int?(value), null,
                        text, double.Parse(onlineUser), 5,
                        description, ActionEnum.LeaveAddition, StatusEnum.Unknown, IoStatusEnum.Unknown, null,
                        null, null, null,
                        null, null, false);
                    }
                    else
                    {
                        num = UpdateRequest(requestId,double.Parse(empNumber), Int16.Parse(codeId),
                    sdates.Date,
                    null, new int?(value), null,
                    text, double.Parse(onlineUser), 5,
                    description, ActionEnum.LeaveAddition, StatusEnum.Unknown, IoStatusEnum.Unknown, null,
                    null, null, null,
                    null, null, false);
                    }
                


                    switch (num)
                    {
                        case -5:
                            //this.ShowMessage("تعداد درخواستهای شما از سقف مجاز بیشتر می باشد.");
                            return new ResultViewModel
                            {
                                Validate = false,
                                ValidateMessage = "تعداد درخواستهای شما از سقف مجاز بیشتر می باشد."
                            };
                        case -4:
                            //this.ShowMessage("مدت مجوز درخواستی مجاز نمی باشد");
                            return new ResultViewModel
                            {
                                Validate = false,
                                ValidateMessage = "مدت مجوز درخواستی مجاز نمی باشد"
                            };
                        case -3:
                            //this.ShowMessage("درخواستهای " + (DateTime.Now - this.EditRequestDate.Date).Days + " روز قبل را نمی توانید ثبت کنید\nبا مدیر سیستم صحبت کنید");
                            return new ResultViewModel
                            {
                                Validate = false,
                                ValidateMessage = "درخواستهای " + (DateTime.Now - sdates.Date).Days + " روز قبل را نمی توانید ثبت کنید\nبا مدیر سیستم صحبت کنید"
                            };
                        case -2:
                           // this.ShowMessage("اطلاعات ثبت نشد\n مدیری برای شما وجود ندارد");
                            return new ResultViewModel
                            {
                                Validate = false,
                                ValidateMessage = "اطلاعات ثبت نشد\n مدیری برای شما وجود ندارد"
                            };
                        case -1:
                           // this.ShowMessage("اطلاعات ثبت نشد");
                            return new ResultViewModel
                            {
                                Validate = false,
                                ValidateMessage = "اطلاعات ثبت نشد"
                            };
                        default:
                            return new ResultViewModel
                            {
                                Validate = true,
                                ValidateMessage = "اطلاعات با موفقیت ثبت شد"
                            };
                    }
                }
                //this.lblErr.Text = "مدت درخواست را وارد نمایید!";
                return new ResultViewModel
                {
                    Validate = false,
                    ValidateMessage = "مدت درخواست را وارد نمایید!"
                };
            }
            catch (Exception ex)
            {

                return new ResultViewModel
                {
                    Validate = false,
                    ExceptionMessage = ex.Message
                };
            }
        }
        public ResultViewModel RegisterOverTimeRequest(string empNumber, string codeId, DateTime sdates, DateTime eDate, string duration, string description, string onlineUser,bool isFirstTimeShift = false, bool isInsert = true, int requestId = 0)
        {
            try
            {
                int num = 0;
                int num2 = Convert.ToInt32(duration.Replace(":", ""));
                if (num2 < 1 || num2 >= 2400)
                {
                   // this.lblErr.Text = "مدت درخواست را وارد نمایید.";
                    return new ResultViewModel
                    {
                        Validate = false,
                        ValidateMessage = "مدت درخواست را وارد نمایید."
                    };
                }
                if (!UtilityManagers.ValidateTime(duration))
                {
                   // this.lblErr.Text = "مدت درخواست را وارد نمایید.";
                    return new ResultViewModel
                    {
                        Validate = false,
                        ValidateMessage = "مدت درخواست را وارد نمایید."
                    };
                }

                //num = InsertRequest(this.CurrentEmpNo, 103, this.EditStartDate.Date, new DateTime?(this.EditEndDate.Date), null, null, num2.ToString(), base.Mc.UserData.EmpNo, 1, this.EditDescription.Text, ActionEnum.OverTime, false);
                if (isInsert)
                {
                    num = InsertRequest(double.Parse(empNumber), Int16.Parse(codeId),
                        sdates.Date,
                        eDate.Date, null, null,
                        num2.ToString(), double.Parse(onlineUser), 1,
                        description, ActionEnum.OverTime, StatusEnum.Unknown, IoStatusEnum.Unknown, null,
                        null, null, null,
                        null, null, false);
                }
                else
                {
                    num = UpdateRequest(requestId,double.Parse(empNumber), Int16.Parse(codeId),
                      sdates.Date,
                      eDate.Date, null, null,
                      num2.ToString(), double.Parse(onlineUser), 1,
                      description, ActionEnum.OverTime, StatusEnum.Unknown, IoStatusEnum.Unknown, null,
                      null, null, null,
                      null, null, false);
                }
                switch (num)
                {
                    case -5:
                       // this.ShowMessage("تعداد درخواستهای شما از سقف مجاز بیشتر می باشد.");
                        return new ResultViewModel
                        {
                            Validate = false,
                            ValidateMessage = "تعداد درخواستهای شما از سقف مجاز بیشتر می باشد."
                        };
                    case -4:
                       // this.ShowMessage("مدت مجوز درخواستی مجاز نمی باشد");
                        return new ResultViewModel
                        {
                            Validate = false,
                            ValidateMessage = "مدت مجوز درخواستی مجاز نمی باشد"
                        };
                    case -3:
                       // this.ShowMessage("درخواستهای " + (DateTime.Now - this.EditStartDate.Date).Days + " روز قبل را نمی توانید ثبت کنید\nبا مدیر سیستم صحبت کنید");
                        return new ResultViewModel
                        {
                            Validate = false,
                            ValidateMessage = "درخواستهای " + (DateTime.Now - sdates.Date).Days + " روز قبل را نمی توانید ثبت کنید\nبا مدیر سیستم صحبت کنید"
                        };
                    case -2:
                      //  this.ShowMessage("اطلاعات ثبت نشد\n مدیری برای شما وجود ندارد");
                        return new ResultViewModel
                        {
                            Validate = false,
                            ValidateMessage = "اطلاعات ثبت نشد\n مدیری برای شما وجود ندارد"
                        };
                    case -1:
                       // this.ShowMessage("اطلاعات ثبت نشد");
                        return new ResultViewModel
                        {
                            Validate = false,
                            ValidateMessage = "اطلاعات ثبت نشد"
                        };
                    default:
                        return new ResultViewModel
                        {
                            Validate = true,
                            ValidateMessage = "اطلاعات با موفقیت ثبت شد"
                        };
                }
            
                //this.lblErr.Text = "مدت درخواست را وارد نمایید!";
                //return new ResultViewModel
                //{
                //    Validate = false,
                //    ValidateMessage = "مدت درخواست را وارد نمایید!"
                //};
            }
            catch (Exception ex)
            {

                return new ResultViewModel
                {
                    Validate = false,
                    ExceptionMessage = ex.Message
                };
            }
        }
        public ResultViewModel RegisterHrMissonRequestWithoutResturn(string empNumber, string codeId, DateTime sdates, string sTime, string eTime,string enteredFromPlace, string description,string onlineUser, bool isFirstTimeShift = false, bool isInsert = true, int requestId = 0)
        {
            try
            {
                int num;
                int.TryParse(sTime.Replace(":", ""), out num);
                if (num < 1 || num >= 2400)
                {
                    //this.lblErr.Text = "ساعت شروع ماموریت را صحیح وارد نمایید";
                    return new ResultViewModel
                    {
                        Validate = false,
                        ValidateMessage = "ساعت شروع ماموریت را صحیح وارد نمایید"
                    };
                }
                int num2;
                int.TryParse(eTime.Replace(":", ""), out num2);
                if (num2 < 1 || num2 >= 2400)
                {
                    //this.lblErr.Text = "ساعت پایان ماموریت را صحیح وارد نمایید";
                    return new ResultViewModel
                    {
                        Validate = false,
                        ValidateMessage = "ساعت پایان ماموریت را صحیح وارد نمایید"
                    };
                }
                string text;
                IoStatusEnum ioStatus = IoStatus(num, num2,empNumber,sdates, out text);
                int num3 = 0;
                string duration = GetDuration(num.ToString(), num2.ToString());

                if (isInsert)
                {
                    //num3 = InsertRequest(this.CurrentEmpNo, Convert.ToInt16(this.EditCards.SelectedValue), this.EditRequestDate.Date, null, new int?(num), new int?(num2), duration, base.Mc.UserData.EmpNo, 4, this.Edit_Description.Text, ActionEnum.TimeDutyNoReturn, this.Status, ioStatus, false);
                    num3 = InsertRequest(double.Parse(empNumber), Int16.Parse(codeId),
                        sdates.Date,
                        null, num, num2,
                        duration.ToString(CultureInfo.InvariantCulture), double.Parse(onlineUser), 4,
                        description, ActionEnum.TimeDutyNoReturn,
                        enteredFromPlace == "1" ? StatusEnum.ExitFromOrg : StatusEnum.ExitFromHome, ioStatus, null,
                        null, null, null,
                        null, null, false);
                }
                else
                {
                    num3 = UpdateRequest(requestId,double.Parse(empNumber), Int16.Parse(codeId),
                       sdates.Date,
                       null, num, num2,
                       duration.ToString(CultureInfo.InvariantCulture), double.Parse(onlineUser), 4,
                       description, ActionEnum.TimeDutyNoReturn,
                       enteredFromPlace == "1" ? StatusEnum.ExitFromOrg : StatusEnum.ExitFromHome, ioStatus, null,
                       null, null, null,
                       null, null, false);
                }
               
                switch (num3)
                {
                    case -5:
                        //this.ShowMessage("تعداد درخواستهای شما از سقف مجاز بیشتر می باشد.");
                        return new ResultViewModel
                        {
                            Validate = false,
                            ValidateMessage = "تعداد درخواستهای شما از سقف مجاز بیشتر می باشد."
                        };
                    case -4:
                       // this.ShowMessage("مدت مجوز درخواستی مجاز نمی باشد");
                        return new ResultViewModel
                        {
                            Validate = false,
                            ValidateMessage = "مدت مجوز درخواستی مجاز نمی باشد"
                        };
                    case -3:
                       // this.ShowMessage("درخواستهای " + (DateTime.Now - this.EditRequestDate.Date).Days + " روز قبل را نمی توانید ثبت کنید\nبا مدیر سیستم صحبت کنید");
                        return new ResultViewModel
                        {
                            Validate = false,
                            ValidateMessage = "درخواستهای " + (DateTime.Now - sdates.Date).Days + " روز قبل را نمی توانید ثبت کنید\nبا مدیر سیستم صحبت کنید"
                        };
                    case -2:
                        //this.ShowMessage("اطلاعات ثبت نشد\n مدیری برای شما وجود ندارد");
                        return new ResultViewModel
                        {
                            Validate = false,
                            ValidateMessage = "اطلاعات ثبت نشد\n مدیری برای شما وجود ندارد"
                        };
                    case -1:
                        //this.ShowMessage(string.IsNullOrEmpty(text) ? "اطلاعات ثبت نشد" : text);
                        return new ResultViewModel
                        {
                            Validate = false,
                            ValidateMessage = string.IsNullOrEmpty(text) ? "اطلاعات ثبت نشد" : text
                        };
                    default:
                        return new ResultViewModel
                        {
                            Validate = true,
                            ValidateMessage = "اطلاعات با موفقیتثبت شد"
                        };
                }

            }
            catch (Exception ex)
            {

                return new ResultViewModel
                {
                    Validate = false,
                    ExceptionMessage = ex.Message
                };
            }
        }
        // Token: 0x06000219 RID: 537 RVA: 0x00014064 File Offset: 0x00012264
        private string GetDuration(string startTime, string endTime)
        {
            startTime = UtilityManagers.ConvertToFourDigitNumber(startTime);
            endTime = UtilityManagers.ConvertToFourDigitNumber(endTime);
            int hours = Convert.ToInt32(startTime.Substring(0, 2));
            int minutes = Convert.ToInt32(startTime.Substring(2, 2));
            int hours2 = Convert.ToInt32(endTime.Substring(0, 2));
            int minutes2 = Convert.ToInt32(endTime.Substring(2, 2));
            TimeSpan ts = new TimeSpan(hours, minutes, 0);
            TimeSpan timeSpan = new TimeSpan(hours2, minutes2, 0);
            TimeSpan timeSpan2 = timeSpan.Subtract(ts);
            return UtilityManagers.ConvertToTowDigitNumber(timeSpan2.Hours.ToString()) + UtilityManagers.ConvertToTowDigitNumber(timeSpan2.Minutes.ToString());
        }
        // Token: 0x060000FB RID: 251 RVA: 0x00007856 File Offset: 0x00005A56
        public DataRow FindNearDataFile(double empNo, double date, int startTime, int endTime)
        {
            //return base.ExecuteDataRow("WF_FindNearDataFile", new object[]
            //{
            //    empNo,
            //    date,
            //    startTime,
            //    endTime
            //});
            var constre = ConfigurationManager.ConnectionStrings["connectionStringProviderSystem"].ToString();
            var connection = new SqlConnection(constre);

            if (connection.State == ConnectionState.Open)
            {
                connection.Close();
            }
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = new DataTable();
            cmd = new SqlCommand("WF_FindNearDataFile", connection);
            cmd.Parameters.Add(new SqlParameter("@EMP_NO", empNo));
            cmd.Parameters.Add(new SqlParameter("@Date", date));
            cmd.Parameters.Add(new SqlParameter("@StartTime", startTime));
            cmd.Parameters.Add(new SqlParameter("@EndTime", endTime));
            cmd.CommandType = CommandType.StoredProcedure;
            da.SelectCommand = cmd;
            da.Fill(dt);
            connection.Close();
            connection.Dispose();
            if (dt.Rows.Count == 0)
            {
                return null;
            }
            return dt.Rows[0];
        }
        private IoStatusEnum IoStatus(int startTime, int endTime,string empNumber,DateTime requestDate, out string msg)
        {
            IoStatusEnum result = IoStatusEnum.Unknown;
            DataRow dataRow = null;
            DataRow dataRow2 = null;
            if (startTime > 0)
            {
                dataRow = FindNearDataFile(double.Parse(empNumber), requestDate.Date.ToOADate(), startTime, startTime);
            }
            if (startTime != endTime && endTime > 0)
            {
                dataRow2 = FindNearDataFile(double.Parse(empNumber), requestDate.Date.ToOADate(), endTime, endTime);
            }
            msg = "";
            if (dataRow != null && dataRow2 != null)
            {
                result = IoStatusEnum.EnterAndExit;
                string arg = UtilityManagers.ConvertToTimeFormat(dataRow["TIME_"].ToString());
                string text = UtilityManagers.ConvertToTimeFormat(dataRow2["TIME_"].ToString());
                msg = string.Format("ساعت {0} به عنوان شروع و ساعت {1} به عنوان پایان ماموریت در سیستم ثبت می گردد", arg, text);
            }
            else if (dataRow != null)
            {
                result = IoStatusEnum.Enter;
                string arg = UtilityManagers.ConvertToTimeFormat(dataRow["TIME_"].ToString());
                msg = string.Format("ساعت {0} به عنوان شروع ماموریت در سیستم ثبت می گردد", arg);
            }
            else if (dataRow2 != null)
            {
                result = IoStatusEnum.Exit;
                string text = UtilityManagers.ConvertToTimeFormat(dataRow2["TIME_"].ToString());
                msg = string.Format("ساعت {0} به عنوان پایان ماموریت در سیستم ثبت می گردد", text);
            }
            return result;
        }
        // Token: 0x060003CD RID: 973 RVA: 0x0002B4E8 File Offset: 0x000296E8
        private int OverLapTime(int duration,string startTime,DateTime requestDate,string empNumb)
        {
            short num = Convert.ToInt16(startTime.Replace(":", ""));
            short num2 = 0;
            num2 = ((num2 > num) ? num2 : ((short)ShDate.TimePlus((int)num, duration)));
            int nullTrueCount = RequestsHourlyIsExist(requestDate.Date, double.Parse(empNumb), (int)num, (int)num2, 0).NullTrueCount;
            if (nullTrueCount > 0)
            {
                return nullTrueCount;
            }
            return GetDataByDateAndAroundTime(double.Parse(empNumb), requestDate.Date.ToOADate(), num, num2).Select("STATUS>0 and STATUS<100").Length;
        }
        public RequestExistCountRow RequestsHourlyIsExist(DateTime startDate, double empNo, int startTime, int endTime, int requestsID = 0)
        {
            //DataTable dataTable = base.ExecuteDataTable("WF_RequestsHourlyIsExist", new object[]
            //{
            //    startDate,
            //    empNo,
            //    startTime,
            //    endTime,
            //    requestsID
            //});
            var constre = ConfigurationManager.ConnectionStrings["connectionStringProviderSystem"].ToString();
            var connection = new SqlConnection(constre);

            if (connection.State == ConnectionState.Open)
            {
                connection.Close();
            }
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dataTable = new DataTable();
            cmd = new SqlCommand("WF_RequestsHourlyIsExist", connection);
            cmd.Parameters.Add(new SqlParameter("@StartDate", startDate));
            cmd.Parameters.Add(new SqlParameter("@Emp_No", empNo));
            cmd.Parameters.Add(new SqlParameter("@startTime", startTime));
            cmd.Parameters.Add(new SqlParameter("@endTime", endTime));
            cmd.Parameters.Add(new SqlParameter("@RequestsID", requestsID));
            cmd.CommandType = CommandType.StoredProcedure;
            da.SelectCommand = cmd;
            da.Fill(dataTable);
            connection.Close();
            connection.Dispose();
            RequestExistCountRow requestExistCountRow = new RequestExistCountRow();
            try
            {
                requestExistCountRow.NullCount = Convert.ToInt32(dataTable.Select("IsFinalApproved is null")[0]["cnt"]);
            }
            catch
            {
                requestExistCountRow.NullCount = 0;
            }
            try
            {
                requestExistCountRow.TrueCount = Convert.ToInt32(dataTable.Select("IsFinalApproved=1")[0]["cnt"]);
            }
            catch
            {
                requestExistCountRow.TrueCount = 0;
            }
            try
            {
                requestExistCountRow.FalseCount = Convert.ToInt32(dataTable.Select("IsFinalApproved=0")[0]["cnt"]);
            }
            catch
            {
                requestExistCountRow.FalseCount = 0;
            }
            try
            {
                requestExistCountRow.NullTrueCount = Convert.ToInt32(dataTable.Select("IsFinalApproved is null or IsFinalApproved=1")[0]["cnt"]);
            }
            catch
            {
                requestExistCountRow.NullTrueCount = 0;
            }
            return requestExistCountRow;
        }
        // Token: 0x060000FA RID: 250 RVA: 0x0000781E File Offset: 0x00005A1E
        public DataTable GetDataByDateAndAroundTime(double empno, double date, short startTime, short endTime)
        {
            //return base.ExecuteDataTable("WF_DataFileGetDataByDateAndAroundTime", new object[]
            //{
            //    empno,
            //    date,
            //    startTime,
            //    endTime
            //});
            var constre = ConfigurationManager.ConnectionStrings["connectionStringProviderSystem"].ToString();
            var connection = new SqlConnection(constre);

            if (connection.State == ConnectionState.Open)
            {
                connection.Close();
            }
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dataTable = new DataTable();
            cmd = new SqlCommand("WF_DataFileGetDataByDateAndAroundTime", connection);
            cmd.Parameters.Add(new SqlParameter("@Emp_No", empno));
            cmd.Parameters.Add(new SqlParameter("@Date_", date));
            cmd.Parameters.Add(new SqlParameter("@StartTime", startTime));
            cmd.Parameters.Add(new SqlParameter("@EndTime", endTime));

            cmd.CommandType = CommandType.StoredProcedure;
            da.SelectCommand = cmd;
            da.Fill(dataTable);
            connection.Close();
            connection.Dispose();
            return dataTable;
        }
        public ResultViewModel ValidParamTimeLeave(string duration,string cardId,string description,string empNumb)
        {
            bool result = true;
            ParmfileRow paramFileRows = GetDataRow();

          var validateOfRequest=  WorkFlowsDatabase.Instance.Action.GetValid(double.Parse(empNumb));
            var timeRequest=validateOfRequest.FirstOrDefault(x => x.ActionId == (int) ActionEnum.TimeLeave);
            var requiredDesc = false;
            if (timeRequest != null)
            {
                requiredDesc = timeRequest.NeedDesc;
            }
            if (duration.Replace(":", "").Trim() == "")
            {
               return new ResultViewModel
                {
                    Validate = false,
                    ValidateMessage = "مدت را وارد نمایید."
                };
            }
            if (!UtilityManagers.ValidateTime(duration))
            {
               
                return new ResultViewModel
                {
                    Validate = false,
                    ValidateMessage = "مدت را صحیح وارد نمایید."
                };
            }
            if (cardId == "")
            {
              return new ResultViewModel
                {
                    Validate = false,
                    ValidateMessage = "نوع مجوز را مشخص کنید"
                };
            }
             if (requiredDesc && string.IsNullOrEmpty(description))
            {
               
                return new ResultViewModel
                {
                    Validate = false,
                    ValidateMessage = "توضیحات درخواست را وارد نمایید"
                };
            }
            
                string str = "";
                int requestDuration;
                if (int.TryParse(duration.Replace("_", "0").Replace(":", ""), out requestDuration))
                {
                    string empty = string.Empty;
                    if (cardId == "17" && !IsAccessWithReamin(double.Parse(empNumb), requestDuration, true, ref str, paramFileRows, out empty))
                    {
                        return new ResultViewModel
                        {
                            Validate = false,
                            ValidateMessage = "مانده منفی می باشد ،شما مجاز به ثبت درخواست نمی باشد"+ " "+ "مرخصی استفاده شده و درخواست شده :" + str
                        };
                }
                    //if (!string.IsNullOrEmpty(empty))
                    //{
                    //    this.lblSumRequest.Text = "مجموع مرخصی بررسی نشده معادل " + empty;
                    //}
                }
                else
                {
                return new ResultViewModel
                {
                    Validate = false,
                    ValidateMessage = "مدت زمان را صحیح وارد نمایید"
                };
            }

            return new ResultViewModel
            {
                Validate = true,
                ValidateMessage = "توضیحات درخواست را وارد نمایید"
            }; 
        }
        public ResultViewModel ValidParamDailyMisson(DateTime startTime,DateTime endTime,string empNumber,string desCription,bool isInsert=true)
        {
           
            ParmfileRow paramFileRows = GetDataRow();
            var validateOfRequest = WorkFlowsDatabase.Instance.Action.GetValid(double.Parse(empNumber));
            var timeRequest = validateOfRequest.FirstOrDefault(x => x.ActionId == (int)ActionEnum.DailyDuty);
            var requiredDesc = false;
            if (timeRequest != null)
            {
                requiredDesc = timeRequest.NeedDesc;
            }
            if (requiredDesc && string.IsNullOrEmpty(desCription))
            {
               return new ResultViewModel
                {
                    Validate = false,
                    ValidateMessage = "توضیحات درخواست را وارد نمایید"
                };
            }
            
                RequestExistCountRow requestExistCountRow = RequestsIsExist(startTime.Date, endTime.Date, double.Parse(empNumber));
                if (MamGetDataByEmpNo(endTime.Date.ToOADate(), startTime.Date.Date.ToOADate(), double.Parse(empNumber)).Rows.Count > 0 && isInsert)
                {
                      return new ResultViewModel
                        {
                            Validate = false,
                            ValidateMessage = "شما در این روز یک درخواست مجوز دارید "
                        };
            }
           
                    DataTable shiftIsHoliday =GetShiftIsHoliday(double.Parse(empNumber), startTime.Date.Date.ToOADate(), endTime.Date.Date.ToOADate(), startTime.ToFa().Replace("/", ""), ShDate.IsLeapYear((short)startTime.Date.Year));
                    if (shiftIsHoliday != null && shiftIsHoliday.Rows.Count > 0)
                    {
                        var tdHoliday = "";
                        foreach (DataRow dataRow in shiftIsHoliday.Rows)
                        {

                    tdHoliday += "تاریخ ";

                    tdHoliday = tdHoliday + dataRow["CurDate"] + " ";

                    tdHoliday += dataRow["TITLE"].ToString();

                    tdHoliday += " میباشد";
                        }
                //this.TBL_Show.Visible = true;
                return new ResultViewModel
                {
                    Validate = true,
                    ValidateMessage = tdHoliday
                };
            }
               
            
            return new ResultViewModel
            {
                Validate = true
            };
        }
        public ResultViewModel ValidHrMissonParam(double empNumb,string duration,string description)
        {
            ParmfileRow paramFileRows = GetDataRow();

            if (!UtilityManagers.ValidateTime(duration))
            {
              //  this.lblErr.Text = "مدت را صحیح وارد نمایید.";
                return new ResultViewModel
                {
                    Validate = false,
                    ValidateMessage = "مدت را صحیح وارد نمایید."
                };
            }
            var validateOfRequest = WorkFlowsDatabase.Instance.Action.GetValid(empNumb);
            var timeRequest = validateOfRequest.FirstOrDefault(x => x.ActionId == (int)ActionEnum.TimeDuty);
            var requiredDesc = false;
            if (timeRequest != null)
            {
                requiredDesc = timeRequest.NeedDesc;
            }
            if (requiredDesc && string.IsNullOrEmpty(description))
            {
                //this.lblErr.Text = "توضیحات درخواست را وارد نمایید";
                return new ResultViewModel
                {
                    Validate = false,
                    ValidateMessage = "توضیحات درخواست را وارد نمایید"
                };
            }
            return new ResultViewModel
            {
                Validate = true
            };
        }
        public ResultViewModel ValidParamForgetIo(string empNum,DateTime forgetDate,string forgetTime,string description, bool isInsert=true)
        {
            int startTime = Convert.ToInt32(forgetTime.Replace(":", ""));
            if (RequestsIsExistByType(forgetDate.Date, double.Parse(empNum), 100, 2, startTime, 0).NullTrueCount > 0&& isInsert)
            {
                return new ResultViewModel
                {
                    Validate = false,
                    ValidateMessage = "درخواست شما تکراری می باشد."
                };
                //this.lblErr.Text = "درخواست شما تکراری می باشد.";
            }
            var validateOfRequest = WorkFlowsDatabase.Instance.Action.GetValid(double.Parse(empNum));
            var timeRequest = validateOfRequest.FirstOrDefault(x => x.ActionId == (int)ActionEnum.ForgottenInOut);
            var requiredDesc = false;
            if (timeRequest != null)
            {
                requiredDesc = timeRequest.NeedDesc;
            }
            if (requiredDesc && string.IsNullOrEmpty(description))
            {
                //this.lblErr.Text = "توضیحات درخواست را وارد نمایید";
                return new ResultViewModel
                {
                    Validate = false,
                    ValidateMessage = "توضیحات درخواست را وارد نمایید"
                };
            }
            return new ResultViewModel
            {
                Validate = true
            };
        }
        public RequestExistCountRow RequestsIsExistByType(DateTime startDate, double empNo, short rType, int operationsId, int startTime, int endTime)
        {
            //DataTable dataTable = base.ExecuteDataTable("WF_RequestsIsExistByType", new object[]
            //{
            //    startDate,
            //    empNo,
            //    rType,
            //    operationsId,
            //    startTime,
            //    endTime
            //});
            var constre = ConfigurationManager.ConnectionStrings["connectionStringProviderSystem"].ToString();
            var connection = new SqlConnection(constre);

            if (connection.State == ConnectionState.Open)
            {
                connection.Close();
            }
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dataTable = new DataTable();
            cmd = new SqlCommand("WF_RequestsIsExistByType", connection);
            cmd.Parameters.Add(new SqlParameter("@StartDate", startDate));
            cmd.Parameters.Add(new SqlParameter("@Emp_No", empNo));
            cmd.Parameters.Add(new SqlParameter("@Type", rType));
            cmd.Parameters.Add(new SqlParameter("@OperationsID", operationsId));
            cmd.Parameters.Add(new SqlParameter("@StartTime", startTime));
            cmd.Parameters.Add(new SqlParameter("@EndTime", endTime));
            cmd.CommandType = CommandType.StoredProcedure;
            da.SelectCommand = cmd;
            da.Fill(dataTable);
            connection.Close();
            connection.Dispose();
        
            RequestExistCountRow requestExistCountRow = new RequestExistCountRow();
            try
            {
                requestExistCountRow.NullCount = Convert.ToInt32(dataTable.Select("IsFinalApproved is null")[0]["cnt"]);
            }
            catch
            {
                requestExistCountRow.NullCount = 0;
            }
            try
            {
                requestExistCountRow.TrueCount = Convert.ToInt32(dataTable.Select("IsFinalApproved=1")[0]["cnt"]);
            }
            catch
            {
                requestExistCountRow.TrueCount = 0;
            }
            try
            {
                requestExistCountRow.FalseCount = Convert.ToInt32(dataTable.Select("IsFinalApproved=0")[0]["cnt"]);
            }
            catch
            {
                requestExistCountRow.FalseCount = 0;
            }
            try
            {
                requestExistCountRow.NullTrueCount = Convert.ToInt32(dataTable.Select("IsFinalApproved is null or IsFinalApproved=1")[0]["cnt"]);
            }
            catch
            {
                requestExistCountRow.NullTrueCount = 0;
            }
            return requestExistCountRow;
        }
        public ResultViewModel ValidParamOverTimeRequest(double empNo,DateTime sDate,DateTime eDate,string description)
        {
            ParmfileRow paramFileRows = GetDataRow();

            if (ShDate.DifDays(eDate.Date, sDate.Date) < 0)
                {
                   // this.lblErr.Text = "مقدار تاریخ شروع و پایان را صحیح وارد نمایید";
                return new ResultViewModel
                {
                    Validate = false,
                    ValidateMessage = "مقدار تاریخ شروع و پایان را صحیح وارد نمایید"
                };
            }
            var validateOfRequest = WorkFlowsDatabase.Instance.Action.GetValid(empNo);
            var timeRequest = validateOfRequest.FirstOrDefault(x => x.ActionId == (int)ActionEnum.OverTime);
            var requiredDesc = false;
            if (timeRequest != null)
            {
                requiredDesc = timeRequest.NeedDesc;
            }
            if (requiredDesc && string.IsNullOrEmpty(description))
                {
                    //this.lblErr.Text = "توضیحات درخواست را وارد نمایید";
                return new ResultViewModel
                {
                    Validate = false,
                    ValidateMessage = "توضیحات درخواست را وارد نمایید"
                };
            }
            return new ResultViewModel
            {
                Validate = true
            };

        }
        public ResultViewModel SaveFormForEdit(int PersonCode, int JPersonCode, string requestId, int OnlineUserID,
           int SessionID,
           string FormJson, string XmlRoot)
        {
            try
            {


                var jsonFormModel = JsonConvert.DeserializeObject<List<FormFrieldsValuesMobileViewModel>>(FormJson);
                var selectedPersonCode = "";

                var objPersonCombo = jsonFormModel.FirstOrDefault(x => x.ID == "99");
                if (objPersonCombo != null)
                {
                    selectedPersonCode = objPersonCombo.Value;
                    PersonCode = Int32.Parse(selectedPersonCode);
                }
      
                var currentRequest = WorkFlowsDatabase.Instance.Requests.GetDataById(int.Parse(requestId));
                int num = 0;
                List<FormFieldsMobileViewModel> list = new List<FormFieldsMobileViewModel>();
                var requestfrmManager = new RequestFormLoader();
                //PageModeEnum pageModeEnum = currentRequest.IsFinalApproved.HasValue ? PageModeEnum.NoDelete : PageModeEnum.Edit;
                switch (currentRequest.OperationsId)
                {
                    case 1:
                        //===================OverTime Form=======================
                        var startDateOverTime = jsonFormModel.FirstOrDefault(x => x.ID == "1");
                        var endDateOverTime = jsonFormModel.FirstOrDefault(x => x.ID == "2");
                        var endTimeOverTime = jsonFormModel.FirstOrDefault(x => x.ID == "3");
                        var descriptionOverTime = jsonFormModel.FirstOrDefault(x => x.ID == "8");
                        if (startDateOverTime != null && endDateOverTime != null && endTimeOverTime != null &&
                            descriptionOverTime != null)
                        {
                            var requstMng = new RegisterRequests();
                            var resultValidate =
                                requstMng.ValidParamOverTimeRequest(double.Parse(PersonCode.ToString()),Convert.ToDateTime(startDateOverTime.Value),
                                    Convert.ToDateTime(endDateOverTime.Value), descriptionOverTime.Value);
                            if (resultValidate.Validate)
                            {
                                var resultRequest = requstMng.RegisterOverTimeRequest(PersonCode.ToString(),
                                    currentRequest.Type.ToString(),
                                    Convert.ToDateTime(startDateOverTime.Value),
                                    Convert.ToDateTime(endDateOverTime.Value), endTimeOverTime.Value,
                                    descriptionOverTime.Value, OnlineUserID.ToString(),false,false, int.Parse(requestId));

                                return resultRequest;
                            }
                            return resultValidate;
                        }
                        //list = requestfrmManager.GetOverTimeForm(currentRequest.EmpNo.ToString(), currentRequest);
                        break;
                    case 2:
                        if (currentRequest.Type > 0)
                        {
                            switch (currentRequest.Type)
                            {
                                case 100:

                                    //====================forgottenInOutRequest=================
                                    var startDateForget = jsonFormModel.FirstOrDefault(x => x.ID == "1");
                                    var selectedTimeForget = jsonFormModel.FirstOrDefault(x => x.ID == "3");
                                    var descriptionForget = jsonFormModel.FirstOrDefault(x => x.ID == "8");
                                    var typeForget = jsonFormModel.FirstOrDefault(x => x.ID == "9");
                                    if (startDateForget != null && selectedTimeForget != null && descriptionForget != null && typeForget != null)
                                    {
                                        var requstMng = new RegisterRequests();
                                        var resultValidate = requstMng.ValidParamForgetIo(PersonCode.ToString(), Convert.ToDateTime(startDateForget.Value), selectedTimeForget.Value, descriptionForget.Value, false);
                                        if (resultValidate.Validate)
                                        {
                                            var resultRequest = requstMng.RegisterForgetIoRequest(Convert.ToDouble(PersonCode),
                                                currentRequest.Type,
                                                Convert.ToDateTime(startDateForget.Value), selectedTimeForget.Value, descriptionForget.Value, typeForget.Value, OnlineUserID.ToString(),false
                                                ,false,int.Parse(requestId));

                                            return resultRequest;
                                        }
                                        return resultValidate;
                                    }
                                    //list = requestfrmManager.GetForGetAttendanceForm(currentRequest.EmpNo.ToString(), currentRequest);
                                    break;
                                case 101:
                                    //====================inOutCorrectionRequest=================
                                    var startDateIoCorrect = jsonFormModel.FirstOrDefault(x => x.ID == "1");
                                    var selectedTimeIoCorrect = jsonFormModel.FirstOrDefault(x => x.ID == "3");
                                    var newSelectedTimeIoCorrect = jsonFormModel.FirstOrDefault(x => x.ID == "4");
                                    var requestTitleIoCorrect = jsonFormModel.FirstOrDefault(x => x.ID == "9");
                                    var descriptionIoCorrect = jsonFormModel.FirstOrDefault(x => x.ID == "8");
                                    if (startDateIoCorrect != null && selectedTimeIoCorrect != null &&
                                        newSelectedTimeIoCorrect != null &&
                                        requestTitleIoCorrect != null && descriptionIoCorrect != null)
                                    {
                                       // var requstMng = new RegisterRequests();
                                        var resultRequest = RegisterForCorectIoRequest(double.Parse(PersonCode.ToString()),
                                              currentRequest.Type,
                                              Convert.ToDateTime(startDateIoCorrect.Value), selectedTimeIoCorrect.Value
                                              , newSelectedTimeIoCorrect.Value, descriptionIoCorrect.Value, requestTitleIoCorrect.Value, OnlineUserID.ToString()
                                              ,false,false,int.Parse(requestId));

                                        return resultRequest;
                                    }
                                    //list = requestfrmManager.GetIoCorrectionForm(currentRequest.EmpNo.ToString(), currentRequest);
                                    break;
                                case 102:
                                    //====================shiftReplaceRequest=================
                                    break;
                                case 105:
                                    //====================inOutTypeCorrectionRequest=================
                                    var startDateIoCorrecionType = jsonFormModel.FirstOrDefault(x => x.ID == "1");
                                    var selectedTimeIoCorrecionType = jsonFormModel.FirstOrDefault(x => x.ID == "3");
                                    var descriptionIoCorrecionType = jsonFormModel.FirstOrDefault(x => x.ID == "8");
                                    var requestTitleIoCorrecionType = jsonFormModel.FirstOrDefault(x => x.ID == "9");
                                    if (startDateIoCorrecionType != null && selectedTimeIoCorrecionType != null && descriptionIoCorrecionType != null && requestTitleIoCorrecionType != null)
                                    {
                                        var requstMng = new RegisterRequests();
                                        var resultRequest = requstMng.RegisterCorrectionTypeIoRequest(Convert.ToDouble(PersonCode),
                                                currentRequest.Type,
                                                Convert.ToDateTime(startDateIoCorrecionType.Value), selectedTimeIoCorrecionType.Value, descriptionIoCorrecionType.Value, requestTitleIoCorrecionType.Value, OnlineUserID.ToString()
                                                ,false,false,int.Parse(requestId));

                                        return resultRequest;

                                    }
                                    // list = requestfrmManager.GetIoTypeCorrectionForm(currentRequest.EmpNo.ToString(), currentRequest);
                                    break;
                            }
                        }
                        break;
                    case 3:
                        if (currentRequest.Type > 0)
                        {
                            if (WorkFlowsDatabase.Instance.Cards.IsDayByCardNo(currentRequest.Type))
                            {
                                //====================DailyDuty=================
                                var sDateMissDl = jsonFormModel.FirstOrDefault(x => x.ID == "5");
                                var eDateMissDl = jsonFormModel.FirstOrDefault(x => x.ID == "6");
                                var descriptionsMissDl = jsonFormModel.FirstOrDefault(x => x.ID == "12");

                                var fromPlaceMissDl = jsonFormModel.FirstOrDefault(x => x.ID == "7");
                                var desctenitionMissDl = jsonFormModel.FirstOrDefault(x => x.ID == "8");
                                var toVehicelMissDl = jsonFormModel.FirstOrDefault(x => x.ID == "9");
                                var returnVehicelMissDl = jsonFormModel.FirstOrDefault(x => x.ID == "10");
                                var stayStatusMissDl = jsonFormModel.FirstOrDefault(x => x.ID == "13");
                                var transferPriceMissDl = jsonFormModel.FirstOrDefault(x => x.ID == "11");
                                if (sDateMissDl != null && eDateMissDl != null && descriptionsMissDl != null
                                    && fromPlaceMissDl != null && desctenitionMissDl != null && toVehicelMissDl != null
                                    && returnVehicelMissDl != null && stayStatusMissDl != null && transferPriceMissDl != null)
                                {
                                    var requstMng = new RegisterRequests();
                                    var resultValidate = requstMng.ValidParamDailyMisson(Convert.ToDateTime(sDateMissDl.Value), Convert.ToDateTime(eDateMissDl.Value), PersonCode.ToString(), descriptionsMissDl.Value,false);
                                    if (resultValidate.Validate)
                                    {
                                        var companyLocation = stayStatusMissDl.Value == "1";
                                        var resultRequest = requstMng.RegisterMissonDailyRequest(Convert.ToDouble(PersonCode),
                                             currentRequest.Type,
                                            Convert.ToDateTime(sDateMissDl.Value), Convert.ToDateTime(eDateMissDl.Value), companyLocation, transferPriceMissDl.Value, toVehicelMissDl.Value, returnVehicelMissDl.Value
                                            , Convert.ToDouble(OnlineUserID.ToString()), 3,
                                              descriptionsMissDl.Value
                                           , ActionEnum.DailyDuty, fromPlaceMissDl.Value, desctenitionMissDl.Value,
                                              false,false,int.Parse(requestId));

                                        return resultRequest;
                                    }
                                    return resultValidate;
                                }
                                //list = requestfrmManager.GetMissonDailyForm(currentRequest.EmpNo.ToString(), currentRequest);
                            }
                            else
                            {
                                //====================TimeDuty=================
                                var sDatemissHr = jsonFormModel.FirstOrDefault(x => x.ID == "1");
                                var sTimemisson = jsonFormModel.FirstOrDefault(x => x.ID == "2");
                                var durationmisson = jsonFormModel.FirstOrDefault(x => x.ID == "3");
                                var priceMisson = jsonFormModel.FirstOrDefault(x => x.ID == "11");
                                var toVehicelMissonHr = jsonFormModel.FirstOrDefault(x => x.ID == "9");
                                var returnVehicleMissonHr = jsonFormModel.FirstOrDefault(x => x.ID == "10");
                                var descriptionHrMisson = jsonFormModel.FirstOrDefault(x => x.ID == "8");
                                if (sDatemissHr != null && sTimemisson != null && durationmisson != null &&
                                    priceMisson != null && toVehicelMissonHr != null && returnVehicleMissonHr != null &&
                                    descriptionHrMisson != null)
                                {
                                    var requstMng = new RegisterRequests();
                                    var resultValidate = requstMng.ValidHrMissonParam(double.Parse(PersonCode.ToString()),durationmisson.Value, descriptionHrMisson.Value);
                                    if (resultValidate.Validate)
                                    {
                                        var resultRequest = requstMng.RegisterHrMissonRequest(PersonCode.ToString(), currentRequest.Type.ToString(), DateTime.Parse(sDatemissHr.Value), sTimemisson.Value
                                            , durationmisson.Value, priceMisson.Value, toVehicelMissonHr.Value, returnVehicleMissonHr.Value, descriptionHrMisson.Value, OnlineUserID.ToString()
                                            ,false,false,int.Parse(requestId));

                                        return resultRequest;
                                    }
                                    return resultValidate;
                                }
                                // list = requestfrmManager.GetMissonHrlyForm(currentRequest.EmpNo.ToString(), currentRequest);
                            }
                        }
                        break;
                    case 4:
                        //====================timeDutyNoReturn=================
                        var startDate = jsonFormModel.FirstOrDefault(x => x.ID == "1");
                        var startTime = jsonFormModel.FirstOrDefault(x => x.ID == "2");
                        var endTime = jsonFormModel.FirstOrDefault(x => x.ID == "3");
                        var startMisson = jsonFormModel.FirstOrDefault(x => x.ID == "9");
                        var descriptionHrMissons = jsonFormModel.FirstOrDefault(x => x.ID == "8");
                        if (startDate != null && startTime != null && endTime != null && startMisson != null &&
                            descriptionHrMissons != null)
                        {
                            var requstMng = new RegisterRequests();
                            var resultValidate = requstMng.ValidHrMissonParam(double.Parse(PersonCode.ToString()), "01:00", descriptionHrMissons.Value);
                            if (resultValidate.Validate)
                            {
                                var resultRequest = requstMng.RegisterHrMissonRequestWithoutResturn(PersonCode.ToString(), currentRequest.Type.ToString(), DateTime.Parse(startDate.Value)
                                    , startTime.Value
                                    , endTime.Value, startMisson.Value, descriptionHrMissons.Value, OnlineUserID.ToString(),
                                    false,false,int.Parse(requestId));

                                return resultRequest;
                            }
                            return resultValidate;
                        }
                        // list = requestfrmManager.GetMissonHrlyWhouthReturnForm(currentRequest.EmpNo.ToString(), currentRequest);
                        break;
                    case 5:
                        if (currentRequest.Type > 0)
                        {
                            if (WorkFlowsDatabase.Instance.Cards.IsDayByCardNo(currentRequest.Type))
                            {
                                //====================DailyRequest=================
                                var sDate = jsonFormModel.FirstOrDefault(x => x.ID == "5");
                                var eDate = jsonFormModel.FirstOrDefault(x => x.ID == "6");
                                var descriptions = jsonFormModel.FirstOrDefault(x => x.ID == "7");
                                if (sDate != null && eDate != null && descriptions != null)
                                {
                                    var requstMng = new RegisterRequests();
                                    var resultValidate = requstMng.ValidParam(Convert.ToDateTime(sDate.Value), Convert.ToDateTime(eDate.Value), currentRequest.Type.ToString(), descriptions.Value, PersonCode.ToString(),false);
                                    if (resultValidate.Validate)
                                    {
                                        var resultRequest = requstMng.RegisterDailyRequest(Convert.ToDouble(PersonCode),
                                             currentRequest.Type,
                                            Convert.ToDateTime(sDate.Value), Convert.ToDateTime(eDate.Value), null, null,
                                            ((Convert.ToDateTime(eDate.Value) - Convert.ToDateTime(sDate.Value)).TotalDays + 1)
                                                .ToString(),
                                            Convert.ToDouble(OnlineUserID), 5, descriptions.Value, ActionEnum.DailyLeave,false,false,int.Parse(requestId));

                                        return resultRequest;
                                    }
                                    return resultValidate;
                                }
                                //list = requestfrmManager.GetNormalDailyForm(currentRequest.EmpNo.ToString(), currentRequest);
                            }
                            else if (currentRequest.Type == 104)
                            {
                                //====================leaveAdditionRequest=================
                                var startDateAddReduce = jsonFormModel.FirstOrDefault(x => x.ID == "1");
                                var endTimeAddReduce = jsonFormModel.FirstOrDefault(x => x.ID == "3");
                                var typeAddReduce = jsonFormModel.FirstOrDefault(x => x.ID == "9");
                                var descriptionAddReduce = jsonFormModel.FirstOrDefault(x => x.ID == "8");
                                if (startDateAddReduce != null && endTimeAddReduce != null && typeAddReduce != null &&
                                    descriptionAddReduce != null)
                                {
                                    var requstMng = new RegisterRequests();
                                    var resultRequest = requstMng.RegisterAddReduceHolidays(PersonCode.ToString(),
                                        currentRequest.Type.ToString(),
                                        Convert.ToDateTime(startDateAddReduce.Value), endTimeAddReduce.Value, typeAddReduce.Value,
                                        descriptionAddReduce.Value, OnlineUserID.ToString(),false,false,int.Parse(requestId));

                                    return resultRequest;
                                }
                                //list = requestfrmManager.GetAddReduceHolidaysForm(currentRequest.EmpNo.ToString(), currentRequest);
                            }
                            else
                            {
                                //====================TimeLeave=================
                                var sDateHr = jsonFormModel.FirstOrDefault(x => x.ID == "1");
                                var sTime = jsonFormModel.FirstOrDefault(x => x.ID == "2");
                                var duration = jsonFormModel.FirstOrDefault(x => x.ID == "3");
                                //var telorance = jsonFormModel.FirstOrDefault(x => x.ID == "4");
                                var descriptionsHr = jsonFormModel.FirstOrDefault(x => x.ID == "8");
                                if (sDateHr != null && sTime != null && duration != null && descriptionsHr != null)
                                {
                                    var requstMng = new RegisterRequests();
                                    var resulttime = requstMng.SetStartTimeShift(double.Parse(PersonCode.ToString()),
                                        Convert.ToDateTime(sDateHr.Value));
                                    var resultValidate = requstMng.ValidParamTimeLeave(duration.Value, currentRequest.Type.ToString(), descriptionsHr.Value, PersonCode.ToString());
                                    if (resultValidate.Validate)
                                    {
                                        var resultRequest = requstMng.RegisterTimeLeaveRequest(sTime.Value, duration.Value,
                                            Convert.ToDateTime(sDateHr.Value), PersonCode.ToString(), descriptionsHr.Value,
                                            currentRequest.Type.ToString(), OnlineUserID.ToString(),
                                            false,false,int.Parse(requestId));

                                        return resultRequest;
                                    }
                                    return resultValidate;
                                }
                                // list = requestfrmManager.GetNormalHrlyForm(currentRequest.EmpNo.ToString(), currentRequest);
                            }
                        }
                        else
                        {
                            //====================LeaveAdditionRequest=================
                            var startDateAddReduce = jsonFormModel.FirstOrDefault(x => x.ID == "1");
                            var endTimeAddReduce = jsonFormModel.FirstOrDefault(x => x.ID == "3");
                            var typeAddReduce = jsonFormModel.FirstOrDefault(x => x.ID == "9");
                            var descriptionAddReduce = jsonFormModel.FirstOrDefault(x => x.ID == "8");
                            if (startDateAddReduce != null && endTimeAddReduce != null && typeAddReduce != null &&
                                descriptionAddReduce != null)
                            {
                                var requstMng = new RegisterRequests();
                                var resultRequest = requstMng.RegisterAddReduceHolidays(PersonCode.ToString(),
                                    currentRequest.Type.ToString(),
                                    Convert.ToDateTime(startDateAddReduce.Value), endTimeAddReduce.Value, typeAddReduce.Value,
                                    descriptionAddReduce.Value, OnlineUserID.ToString(), false, false, int.Parse(requestId));

                                return resultRequest;
                            }
                            // list = requestfrmManager.GetAddReduceHolidaysForm(currentRequest.EmpNo.ToString(), currentRequest);
                        }
                        break;
                }
                return new ResultViewModel
                {
                    Validate = false,
                    ValidateMessage = "اطلاعات فرم کامل نیست"
                };

            }
            catch (Exception ex)
            {

                return new ResultViewModel
                {
                    Validate = false,
                    ValidateMessage = ex.Message
                };
            }
        }
        //public string ToPersianNumber(string input)
        //{
        //    string[] persian = new string[10] { "۰", "۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹" };

        //    for (int j = 0; j < persian.Length; j++)
        //        input = input.Replace(j.ToString(), persian[j]);

        //    return input;
        //}
        private static readonly string[] pn = { "۰", "۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹" };
        private static readonly string[] en = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" };
        private static readonly string[] ar = { "٠", "١","٢","٣","٤","٥","٦","٧","٨","٩"};
        public static string ToEnglishNumber(string strNum)
        {
            string chash = strNum;
            for (int i = 0; i < 10; i++)
            {
                chash = chash.Replace(pn[i], en[i]);
                chash = chash.Replace(ar[i], en[i]);
            }
            return chash;
        }
        //public static string ToPersianNumber(this int intNum)
        //{
        //    string chash = intNum.ToString();
        //    for (int i = 0; i < 10; i++)
        //        chash = chash.Replace(en[i], pn[i]);
        //    return chash;
        //}
    }
    // Token: 0x02000013 RID: 19
    public class ParmfileRow
    {
        // Token: 0x17000072 RID: 114
        // (get) Token: 0x060000F7 RID: 247 RVA: 0x000031D5 File Offset: 0x000013D5
        // (set) Token: 0x060000F8 RID: 248 RVA: 0x000031DD File Offset: 0x000013DD
        public bool WfIsAttachment
        {
            get;
            set;
        }

        // Token: 0x17000073 RID: 115
        // (get) Token: 0x060000F9 RID: 249 RVA: 0x000031E6 File Offset: 0x000013E6
        // (set) Token: 0x060000FA RID: 250 RVA: 0x000031EE File Offset: 0x000013EE
        public bool DayArchiveKind
        {
            get;
            set;
        }

        // Token: 0x17000074 RID: 116
        // (get) Token: 0x060000FB RID: 251 RVA: 0x000031F7 File Offset: 0x000013F7
        // (set) Token: 0x060000FC RID: 252 RVA: 0x000031FF File Offset: 0x000013FF
        public int MyEste
        {
            get;
            set;
        }

        // Token: 0x17000075 RID: 117
        // (get) Token: 0x060000FD RID: 253 RVA: 0x00003208 File Offset: 0x00001408
        // (set) Token: 0x060000FE RID: 254 RVA: 0x00003210 File Offset: 0x00001410
        public short EmpEste
        {
            get;
            set;
        }

        // Token: 0x17000076 RID: 118
        // (get) Token: 0x060000FF RID: 255 RVA: 0x00003219 File Offset: 0x00001419
        // (set) Token: 0x06000100 RID: 256 RVA: 0x00003221 File Offset: 0x00001421
        public bool IsMorDay
        {
            get;
            set;
        }

        // Token: 0x17000077 RID: 119
        // (get) Token: 0x06000101 RID: 257 RVA: 0x0000322A File Offset: 0x0000142A
        // (set) Token: 0x06000102 RID: 258 RVA: 0x00003232 File Offset: 0x00001432
        public long CodePlace
        {
            get;
            set;
        }

        // Token: 0x17000078 RID: 120
        // (get) Token: 0x06000103 RID: 259 RVA: 0x0000323B File Offset: 0x0000143B
        // (set) Token: 0x06000104 RID: 260 RVA: 0x00003243 File Offset: 0x00001443
        public short StartDay
        {
            get;
            set;
        }

        // Token: 0x17000079 RID: 121
        // (get) Token: 0x06000105 RID: 261 RVA: 0x0000324C File Offset: 0x0000144C
        // (set) Token: 0x06000106 RID: 262 RVA: 0x00003254 File Offset: 0x00001454
        public string Domain
        {
            get;
            set;
        }

        // Token: 0x1700007A RID: 122
        // (get) Token: 0x06000107 RID: 263 RVA: 0x0000325D File Offset: 0x0000145D
        // (set) Token: 0x06000108 RID: 264 RVA: 0x00003265 File Offset: 0x00001465
        public string DomainFieldAdmin
        {
            get;
            set;
        }

        // Token: 0x1700007B RID: 123
        // (get) Token: 0x06000109 RID: 265 RVA: 0x0000326E File Offset: 0x0000146E
        // (set) Token: 0x0600010A RID: 266 RVA: 0x00003276 File Offset: 0x00001476
        public string DomainFieldEmpNo
        {
            get;
            set;
        }

        // Token: 0x1700007C RID: 124
        // (get) Token: 0x0600010B RID: 267 RVA: 0x0000327F File Offset: 0x0000147F
        // (set) Token: 0x0600010C RID: 268 RVA: 0x00003287 File Offset: 0x00001487
        public int Tolerance
        {
            get;
            set;
        }

        // Token: 0x1700007D RID: 125
        // (get) Token: 0x0600010D RID: 269 RVA: 0x00003290 File Offset: 0x00001490
        // (set) Token: 0x0600010E RID: 270 RVA: 0x00003298 File Offset: 0x00001498
        public bool WfRequestMandeControl
        {
            get;
            set;
        }

        // Token: 0x1700007E RID: 126
        // (get) Token: 0x0600010F RID: 271 RVA: 0x000032A1 File Offset: 0x000014A1
        // (set) Token: 0x06000110 RID: 272 RVA: 0x000032A9 File Offset: 0x000014A9
        public short KasrEsth
        {
            get;
            set;
        }

        // Token: 0x1700007F RID: 127
        // (get) Token: 0x06000111 RID: 273 RVA: 0x000032B2 File Offset: 0x000014B2
        // (set) Token: 0x06000112 RID: 274 RVA: 0x000032BA File Offset: 0x000014BA
        public float EndDateRamedan
        {
            get;
            set;
        }

        // Token: 0x17000080 RID: 128
        // (get) Token: 0x06000113 RID: 275 RVA: 0x000032C3 File Offset: 0x000014C3
        // (set) Token: 0x06000114 RID: 276 RVA: 0x000032CB File Offset: 0x000014CB
        public float StartDateRamedan
        {
            get;
            set;
        }

        // Token: 0x17000081 RID: 129
        // (get) Token: 0x06000115 RID: 277 RVA: 0x000032D4 File Offset: 0x000014D4
        // (set) Token: 0x06000116 RID: 278 RVA: 0x000032DC File Offset: 0x000014DC
        public short MorDebt
        {
            get;
            set;
        }

        // Token: 0x17000082 RID: 130
        // (get) Token: 0x06000117 RID: 279 RVA: 0x000032E5 File Offset: 0x000014E5
        // (set) Token: 0x06000118 RID: 280 RVA: 0x000032ED File Offset: 0x000014ED
        public bool DifKasrEsth
        {
            get;
            set;
        }

        // Token: 0x17000083 RID: 131
        // (get) Token: 0x06000119 RID: 281 RVA: 0x000032F6 File Offset: 0x000014F6
        // (set) Token: 0x0600011A RID: 282 RVA: 0x000032FE File Offset: 0x000014FE
        public int DayArchive
        {
            get;
            set;
        }

        // Token: 0x17000084 RID: 132
        // (get) Token: 0x0600011B RID: 283 RVA: 0x00003307 File Offset: 0x00001507
        // (set) Token: 0x0600011C RID: 284 RVA: 0x0000330F File Offset: 0x0000150F
        public bool WfParallelApprove
        {
            get;
            set;
        }

        // Token: 0x17000085 RID: 133
        // (get) Token: 0x0600011D RID: 285 RVA: 0x00003318 File Offset: 0x00001518
        // (set) Token: 0x0600011E RID: 286 RVA: 0x00003320 File Offset: 0x00001520
        public int InvalidMonthToEdit
        {
            get;
            set;
        }

        // Token: 0x17000086 RID: 134
        // (get) Token: 0x0600011F RID: 287 RVA: 0x00003329 File Offset: 0x00001529
        // (set) Token: 0x06000120 RID: 288 RVA: 0x00003331 File Offset: 0x00001531
        public double MmDate
        {
            get;
            set;
        }

        // Token: 0x17000087 RID: 135
        // (get) Token: 0x06000121 RID: 289 RVA: 0x0000333A File Offset: 0x0000153A
        // (set) Token: 0x06000122 RID: 290 RVA: 0x00003342 File Offset: 0x00001542
        public short WfLimitMonthShowDaily
        {
            get;
            set;
        }

        // Token: 0x17000088 RID: 136
        // (get) Token: 0x06000123 RID: 291 RVA: 0x0000334B File Offset: 0x0000154B
        // (set) Token: 0x06000124 RID: 292 RVA: 0x00003353 File Offset: 0x00001553
        public bool WfIsChangeUserName
        {
            get;
            set;
        }

        // Token: 0x17000089 RID: 137
        // (get) Token: 0x06000125 RID: 293 RVA: 0x0000335C File Offset: 0x0000155C
        // (set) Token: 0x06000126 RID: 294 RVA: 0x00003364 File Offset: 0x00001564
        public bool WfDifrenceRequest
        {
            get;
            set;
        }

        // Token: 0x1700008A RID: 138
        // (get) Token: 0x06000127 RID: 295 RVA: 0x0000336D File Offset: 0x0000156D
        // (set) Token: 0x06000128 RID: 296 RVA: 0x00003375 File Offset: 0x00001575
        public bool CnoEqPno
        {
            get;
            set;
        }

        // Token: 0x1700008B RID: 139
        // (get) Token: 0x06000129 RID: 297 RVA: 0x0000337E File Offset: 0x0000157E
        // (set) Token: 0x0600012A RID: 298 RVA: 0x00003386 File Offset: 0x00001586
        public bool WfNeedDesc
        {
            get;
            set;
        }

        // Token: 0x1700008C RID: 140
        // (get) Token: 0x0600012B RID: 299 RVA: 0x0000338F File Offset: 0x0000158F
        // (set) Token: 0x0600012C RID: 300 RVA: 0x00003397 File Offset: 0x00001597
        public short RegisterTolerance
        {
            get;
            set;
        }

        // Token: 0x1700008D RID: 141
        // (get) Token: 0x0600012D RID: 301 RVA: 0x000033A0 File Offset: 0x000015A0
        // (set) Token: 0x0600012E RID: 302 RVA: 0x000033A8 File Offset: 0x000015A8
        public bool ShowRemainKardex
        {
            get;
            set;
        }
    }
    // Token: 0x0200001B RID: 27
    public class RequestExistCountRow
    {
        // Token: 0x170000F4 RID: 244
        // (get) Token: 0x06000202 RID: 514 RVA: 0x000044EA File Offset: 0x000026EA
        // (set) Token: 0x06000203 RID: 515 RVA: 0x000044F2 File Offset: 0x000026F2
        public int NullCount
        {
            get;
            set;
        }

        // Token: 0x170000F5 RID: 245
        // (get) Token: 0x06000204 RID: 516 RVA: 0x000044FB File Offset: 0x000026FB
        // (set) Token: 0x06000205 RID: 517 RVA: 0x00004503 File Offset: 0x00002703
        public int TrueCount
        {
            get;
            set;
        }

        // Token: 0x170000F6 RID: 246
        // (get) Token: 0x06000206 RID: 518 RVA: 0x0000450C File Offset: 0x0000270C
        // (set) Token: 0x06000207 RID: 519 RVA: 0x00004514 File Offset: 0x00002714
        public int FalseCount
        {
            get;
            set;
        }

        // Token: 0x170000F7 RID: 247
        // (get) Token: 0x06000208 RID: 520 RVA: 0x0000451D File Offset: 0x0000271D
        // (set) Token: 0x06000209 RID: 521 RVA: 0x00004525 File Offset: 0x00002725
        public int NullTrueCount
        {
            get;
            set;
        }
    }
}
