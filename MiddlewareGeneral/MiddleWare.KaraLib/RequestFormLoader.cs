﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Interface.ViewModel;
using Interface.ViewModel.General;
using Newtonsoft.Json;
using PW.WorkFlows.SqlDal;

namespace MiddleWare.KaraLib
{
   public class RequestFormLoader
    {
       /// <summary>
       /// بدست اوردن لیست کاربر انلاین
       /// </summary>
       /// <param name="onlineUserId">کاربر آنلاین</param>
       /// <param name="filterStr">عبارت جستجو توسط فرد</param>
       /// <returns>لیست مورد نظر</returns>
       public ResultViewModel GetPersonAutoComplete(string onlineUserId, string filterStr)
       {
            try
            {
                filterStr = ConvertPersianToArabic(filterStr);
                //    return base.ExecuteDataTable("WF_EmployeeGetPerson", new object[]
                //{
                //    managerEmpNo
                //});
                var lstDataForm=new List<ComboDataMobileViewModel>();
                var constre = ConfigurationManager.ConnectionStrings["connectionStringProviderSystem"].ToString();
                var connection = new SqlConnection(constre);

                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }



                connection.Open();
                // var command = new SqlCommand(query, connection);
                var cmd = new SqlCommand("WF_EmployeeGetPerson", connection);
                cmd.Parameters.Add(new SqlParameter("@EmpNo", onlineUserId));

                cmd.CommandType = CommandType.StoredProcedure;
                var result = cmd.ExecuteReader();
             
                while (result.Read())
                {
                    var modelData=new ComboDataMobileViewModel();
                    modelData.Name = ConvertPersianToArabic(result["FullName"].ToString());
                    modelData.Id= result["EMP_NO"].ToString();
                    lstDataForm.Add(modelData);
                }
                connection.Close();
                connection.Dispose();
                if (filterStr!="" && filterStr != "\n      ")
                lstDataForm = lstDataForm.Where(x => x.Name.Contains(filterStr)).ToList();
                var countTotalPage = (lstDataForm.Count/10) + 1;
                 lstDataForm = lstDataForm.Select(x => new ComboDataMobileViewModel
                {
                    Id = x.Id,
                    Name = x.Name

                }).ToList();
                var resultObj=new ResultViewModel
                {
                    Validate = true,
                    Message = "{\"TotalPage\":\""+ 0 + "\",\"Data\":"+ JsonConvert.SerializeObject(lstDataForm)+"}"

                };
                return resultObj;
            }
            catch (Exception ex)
            {

                return new ResultViewModel
                {
                    Validate = false,
                    ValidateMessage = ex.Message
                };
            }
       }
        /// <summary>
        /// کلاسی جهت تبدیل ی و ک فارسی به عربی
        /// </summary>
        /// <param name="persiantext">
        /// متن مورد نظر 
        /// </param>
        /// <returns>
        /// متن به صورتی که ی و ک عربی شده است
        /// </returns>
        public string ConvertPersianToArabic(string persiantext)
        {
            string[] arabicChars = { "ي", "ك", "ى" };
            string[] persianChars = { "ی", "ک", "ی" };
            var charsLen = arabicChars.Length;
            var prefixText = persiantext;

            for (var i = 0; i < charsLen; i++)
            {
                prefixText = prefixText.Replace(persianChars[i], arabicChars[i]);
            }
            return prefixText;
        }
        /// <summary>
        /// بدست اوردن لیست کاربر انلاین
        /// </summary>
        /// <param name="onlineUserId">کاربر آنلاین</param>
        /// <returns>لیست مورد نظر</returns>
        public ResultViewModel GetPersonUnderOrder (string onlineUserId, string filterStr)
        {
            try
            {
                filterStr = ConvertPersianToArabic(filterStr);
                //    return base.ExecuteDataTable("WF_EmployeeGetPerson", new object[]
                //{
                //    managerEmpNo
                //});
                var lstDataForm = new List<GetOnlineUserPersonelAccessModel>();
                var constre = ConfigurationManager.ConnectionStrings["connectionStringProviderSystem"].ToString();
                var connection = new SqlConnection(constre);

                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }



                connection.Open();
                // var command = new SqlCommand(query, connection);
                var cmd = new SqlCommand("WF_EmployeeGetPerson", connection);
                cmd.Parameters.Add(new SqlParameter("@EmpNo", onlineUserId));

                cmd.CommandType = CommandType.StoredProcedure;
                var result = cmd.ExecuteReader();

                while (result.Read())
                {
                    var modelData = new GetOnlineUserPersonelAccessModel();
                    modelData.DisplayName = ConvertPersianToArabic(result["FullName"].ToString());
                    modelData.PersonID = result["EMP_NO"].ToString();
                    modelData.PersonnelPic =
                        "iVBORw0KGgoAAAANSUhEUgAAAKAAAABsCAYAAADt5bniAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAABCWSURBVHhe7Z1pcBzHdcehi45kWZKrlMOWZDtWyU4+xFLFRyS7bH/xB+dDKoq/KFWJyyVbvlRJKhXFUqIc8gdbSqpkXRQtUiRuAiRhSjxF8ca1i4sgAYIgAII4iN3pmT1mZo+ZAQVR2vZ7gyZFAI+4CGC7Z+dV/QqUZnem+73/vtc9R09ZqVuhMPwx22afyVnaI46lPeaY8X+Fv897lrZl0mJ7PIs1ezY7BZx3LS0Jf7Pw1wOmgA8F+G9PbMPPnIfvncLvurgPW9/sWfqvxb4f83KJhycntfvw2KIZoQXZOOc3cNu+00vHvupmtMcdW/t/14rvAXEMgWAc12aXQDR8LcFj4rE9WxuEv7vh7/+5pva4l9a/YkNbeVnZDaL5oalmhULhY64dfxCC+iRksmoIcB/8G4JNi0E+oK0WO+1lWFU+rT3p2IkvYZ9E90KTzSDD3ehl9S9DyXwaStwhEJxFB1ZhLGY6pnZwMqP9ws2wv8Q+i+6HVgzL59ndk5b29zC2qoOMkSKDFmQsLQU/tDrwwWN5xu4WbgltNc1xEn8M47Yfw/jtqGv7g346OCUG+gL8cgR4ouAYfyTcFdpKGOfstulMx/bDLz4U3YJoLvhqH862OWO3CTeGtlRzk/EHJzP6K5Dt0rSjQxYCfrCpSVt7ecpJ/IVwa2jzGQys18HM7+/AecfBeR/OdmjI8kBfQnk+Br59lPP+dcLdoV22ZLL/9nxa+zmUjvOUA0NWEIsNw8TtpwXD+Lhwf+laIZ3+xKSlP+WaLEY6K2TVgKw4AWPrf0unBz8hwlE6hmUATxK7NotTzglZO2AGHfey2s/6+0ugNDc0NNyUN+Pfg46fm+2IkOICyWBoeowY0BPcjh3/EmS9gzAYDicXkoKxcWztXceKBWfWHIvFbsW7S6BzF6lOh8gHjA8vwmTlV5yP/4EIo5o2aevfgg6dnd3BEDUAIZ6dtNk3RTjVsULBz3ovQCemZncqRC1gbDiFFawAlUyEV27Lm/qfwy+nk+qMElgx7qVGuasPcDd+irsXOrk7GuHO+SbuDB/jztBh4CB3BpF3p/8OHeLOOfj/uH2kmbtjUe5OnOCu1svdxBD30uOw3zh9PEUAIbbn0+zPRJjlNP8qhsUyVAekBEThGoPcudABAjrKnf7dPN+7g+dP1a08p3/HnbP7QMiNIM5u7iXPQxu0uW2SGNfUMiDCR0W45TE8h+RByYXM9wHVcKlIjUFW6/KzV753Oy2WtQJFOXwUMm0P98wJur2SgTF20trznPNbRPiLa7Y9eiek551UY6UBMx0E2Rk8wPM99bQYig38GJxzR6D0n4U2y50ZId4FoME0zTuEDIpjXnri067FOqhGSgEKb6KL5/veooMuKU7/XvjB9EL7pS/Rba6b+pSQw9oaTM8/Aw0YmtUgaXC1Pu6ceZsMsCrgmBHHqFT/JGLA0rT7hCzWxnJp7YswFhgjGlN8YCyFs1EqoGpSDzPrFqln0aCF0fdy8QeEPFbXLmaT90NpGKUaUnRSI1C+9hBBVB/Mhnh6iOy3BIAIRy5mE58XMlkdsyztPhDfANWAYuMmh2Gst5MMXmCAsSz2k+q/JPR7nnmPkMvKGs54YNzXRRy0+KTHgi++y4AIpc6EttaZTqdX9h5DzvlNMNvdQx2w6MDYKD+wnw5WUIFhhn/FhvKHBIAId6FmhHyu3zxLx+u65MGKjTPeTgcp4OAlP8ofsuBY2q+FfK7PvDT7W9dm71MHKTpYensbyAAFnp5t09eZKb9IAFRM0Iz2N0JGy7NsYuLzsJMEdQAZcMba6OCUCHgTBOUXiTAuZozPCTktzTjvvgV28M6sHcoDjP2cM7vJwJQMPdu5lxyh/SMLFtvHu7uXft0Yxn0/cS2tQO5UBsDxeJKWDEwJ4UycoP0jCaghKMdPCFktzvD6nmdpUq9I4LI+MiClBl71ofwjFbiQkpv8EyGvhQ3GfW+SO5IIvOGTCkip4QwcIP0jGzCR3SjkNb+5Ge0h+IL09/U5Y1EyIKUGXnpU4cZWKMUf4JORQmbXNvjgLmoHsuGMtpABKTn63vYnZJSPZGPSZjuFzGjz11C2NDnP+c0CT8SSASk18NKcxFdFrgbK8JSX1r8s5DbXXDNeR31RRkIBChQSIOJYWo2Q20ybtPXPwgdys78gK2EJFihUggXZSYu4gRXq838SH5YWt8SvglwGT8YrcOv+DCYz7Gkhu2nDJ5wcM44vYyG/ICP+sx5EQEoN/0ZVwj8y49raCdDczUJ+MPaz4w/CAHHNX9ZyPbjaaTIgpYYC14PnABPdSzMWQALxPUt9UGbwThAqIKWGM9JE+kd2QHP/4YuPP/fcjfAfR6kPSQ3eitWzjQxKKYFLh5D+kR7t8HOgvTL/HWoKzX6vYMX8UxBUUEoJl52h/SM/2QI+2J4z4w8TG5XAX+mACErJABVA+tux5sOMfa0sb2k/IzcqQMmfCzy9U7VzgDOx4j8pcy22gdyoALh8BRmYEgEXWKL8ogowG15f5llsP7VRCVIjJT0RwZPxpF8UAZLfXrz75TS1UQ00nj+ziwxO8KkXK2lRflEEi/WUwT+MORsUAhd8pAMUcHp34CCe9IkquDZjkAHjeWqjKpTqFREVr4DMxjW1LJZgpS7BzcGcWL1ldSUGH0kg/aEQMAacQgEq/+IYzAZUkIJL/fTC54QvVALvP0ABKv8CGVx+lw5UMHEG3iH9oBqgPQ8FmKQ2KgUMxou+4Pga4q+yT/lBOTSjzLW1fnqjWgRrVdR58Ge/F0gfKIel9ZU5lnaI3KgYrjFAByxgOMPHyf6riXYASjD7Lb1RMSzNvzuYClpwqFdh0fJFA9p7HU9E/9PsDarixk4RQQsO05MPtZ7/mB/tybJJi32D3qggeI9ggC/N4Ul3st+qkks8fPmGVKWvhlxNYNeMwYePFHv6bX60XKFg3oFPxN3oZVgT/SEFgSwYvHUDYewXtOxns+OoPf+5ENfU/pv4gLJMn5gOztqBeOd3sMZ+ECM7/qwvPrSpjPaQq8KbLhcNzIjxbZhEMJWjB7JfQup3gywZ1JqbjD8o5Oc/mL7Os1gv9WFlwRVUA3B1xBmL0P1TGYud4rx/nZDftLlmXLlngxdC9QmJv+qBys98XAPXYs8I2X1kFzPGn7q25lBfUBcoxapeoutt4K7/VnWqX+oCGsvjQlhCdjMNNtZTX1IanBUPHqSDLCs929W/3f4awIR3q5DbXMPnNCE9KrFA5ZIwQYSK3DOYw0kH66f7oTgw+Xjfy86zQCXYDZ4Z30d9WXlgLJXq2EoGXRbsrho+3lhOtz8AgAB3C51d29xMEk/JKH+XNMXwoY18/MgGnu2uJQVQTFJtFXxw38v8HLSRarvq4KmXRS1SjgZfqJi9gyAwfHgT79/9Ih/a/zJPRitIIaw1+GOYOPZbv11IUAUIE8LNQl4LG4yZ7vEkf1HNctCi9fzsnt9cCfbY4de51VlNCmO1yZ2s40bLZj/rXW4PMnI0iCVYS3npiU8LeS3OPEv/KaRNeV/VtQySXb/jsWMb+cDel64EHAWJZdnsrCKFstLkTm7lRusWfu6dV2YIDzn3zqs80dlAtl1VUENuhv1YyGrxxvv714FyD1I7VRUUoN68hWuNb0IZniuAkYOv+eJAkVDiuR5wghFv3OiX/9nHnT72637bsI1U25XF1A7gEtBCVkszPDkNO5H2da1LJdnV4AfZp2kLH4WgU2LArIjl2Wjd7AuHEtRCoIjNjiquNW3iwwdeJY+DYDaeOPrGlXYFTIBGJjO+vNe1XjYvE/8epFG1H15HrDhPRGs/EqAgfnwTmQ2vBrMWCjLeuIknIuW+sFCYmRO1PnZXNU+3V/rZM3Z8I2Sz9TPKPMVZAD/HmjbPaI8R3RqUy2/ve2n2qJDR9ZmXkfeV/YslO9YFAS6fEeyruXDkjTkTgoXATHn1pGYxnN3zImTE13zhU+3ANmbH1F/1ACaxK/PKfjSo4TfBDpVdxi03cZobkWoi2HPBcoiTAUo81wMKdeTd9fMI7yOwrbmYujegOmZ8L2pGyGdlDG/dh1J8gjqgrDjJUW6eOcz1lmtnvmuBE5WxQxsWLM/zgaLD8R9m19mldkGgzWb/Yb8PVN9kBTTS6a/9vBpW8OL3ulZ8iDqwLLipcZ4dP8nTvfthTFZBB3eJoHgwM+KkBbPj4L6XrpRfLKn4dwAYghJ+Hsrr2OENPHZsE2fEvpYK9gH7gn3CvlF9lgUQ34Dnxe4Rclkdey8XfwAONEY1YG3RuIsrY7EBnhnthGxxxJ9BGq2VZCBXGhTl1VCfWWmwb9hH7GtmtMvvO/oAfUH7aE0ZuZhN3i9ksrr2Xk77AohwnGjE6oAPnRvDPHvhFLeHmiEj7OOJjm1QplYmwykN+AB9gVkSfYM+Ql+t5RN0mJAwMQl5rI1dzBifc212jmrQ8gGhwZgnF+vj9nCUm30H1zSrBYkr2RJ8iL5En06PJ1dWmDgks/Ux+gbT1Ta8vgfq76AathBuepzntbM8M9IBJeUoT53cNX2ebhkThpBFAr41wMfoa/Q5+h5j4KaXt9gRxL7dTV34lJBDcSyTGb/LseLXfs0/3gyqD8Eguptbg4083bOXJ9rrw/IpE1jGISYYG4wRxgpjhrGjYgqVrwDie8u2R+8UMiiu4bU+GHO8kDeGP8xd6IGxSAtPnz7Ak507wvKpMH4ZhxhiLO1zLTw30cPzifMfOGnt+WVf311NS7bXPQ5p3qU6E6I+rBli21b/AxFuOS3Z2fAQa6k4Q3UgRGFaK04nO7Z/9DC5zGZ2HLjDaK15gzVv+YDsTIg6tJRfMiI1GzCmIrzqWKp9219DNrxAdixEeiB248m2+u+KcKpp5vCBO/RI9XrWtOV9qpMhUjKViFS/qmTWu5YZbXVfM1qrolCWC0SHQyQAkkRBb62KpDvrvyLCFizjvPHmRHvtP+otFROUA0KKBw6VWFvdP/DGxptFuIJrrHvvbUZLzTMgxDTljJC1w2iuSBmR6l8UYm23ivCUjmX79n8Sxof/y8AJlHNCVg/WXJ7UW6v/J9Oz6y4RjtI1X4jRmn+HKX+MclbICoLDn2j1U6HwCIu1td0Kzvk+a6ns8gfElANDlsz05KKyIwFjvFjbjtIrtcsxo73+r/RI1Ra9uTxHOTVkYaDM5oxo1Zvpru1fFW4NbalmdzfcaURrfwiztOPg1KnZTg6ZCUMftVQeM6I1j+M5WOHG0FbC7JM7P8uilf9itFZGwdmhGAUoOqO1KgKz2X/Wo1uLc3NoqVm8o/ZeFql9An7tb8PAuvTKdAuU10jVW0ZbzY9gXLe6DwKFNr91d2+6Re+q+7YRrf4lZMcmCI5DBk1hGPSJNVc2Ga3Vv0x1bv8WntQX3Q9NNsMT3cnotq8notVP6ZHKHZAhz0MQlSnZ0+M4aHMLtL21+qlEpO4R7JPoXmiqGef8Bv3Ujj/UIXPo0donQZTrIbiHIVvGYJbtFeOUz/Qxyz0d2wBt0VurXktGan6e6tj2zXz33ruxzaL5oQXZkv2Nt2st276Q6Nr+HTwHqUeq/wsE8QpQB/8+pjdXdIBQBkEoSRCsB5npEmsuR/FcJahyPOVR0FsrLvmfwc+2VAww/G5r1TH4zlbcZyJS+yweI9FW951U+84H8NiiGSVqZWW/BzJV9J2VwP5IAAAAAElFTkSuQmCC";
                    modelData.Code= result["EMP_NO"].ToString();
                    modelData.TotalPage = 0;
                    lstDataForm.Add(modelData);
                }
                connection.Close();
                connection.Dispose();
                if (filterStr != "" && filterStr != "\n      ")
                    lstDataForm = lstDataForm.Where(x => x.DisplayName.Contains(filterStr)).ToList();
               // var countTotalPage = (lstDataForm.Count / 10) + 1;
                //lstDataForm = lstDataForm.Select(x => new GetOnlineUserPersonelAccessModel
                //{
                //    PersonID = x.Id,
                //    DisplayName = x.Name

                //}).ToList();
                if (lstDataForm.Count == 1)
                {
                    var firstData = lstDataForm.FirstOrDefault();
                    if (firstData != null)
                    {
                        var firstPersonCode = firstData.PersonID;
                        if (firstPersonCode == onlineUserId)
                        {
                            var resultObjSingle = new ResultViewModel
                            {
                                Validate = true,
                                Message = ""

                            };
                            return resultObjSingle;
                        }
                    }
                }
                var resultObj = new ResultViewModel
                {
                    Validate = true,
                    Message =JsonConvert.SerializeObject(lstDataForm) 

                };
                return resultObj;
            }
            catch (Exception ex)
            {

                return new ResultViewModel
                {
                    Validate = false,
                    ValidateMessage = ex.Message
                };
            }
        }
        /// <summary>
        /// دسترسی به نوع مجوز های موجود
        /// </summary>
        /// <param name="empNumber">کد پرسنلی</param>
        /// <returns>لیست نوع مجوز ها</returns>
        public List<RequestModel> GetUserRequestTypes(string empNumber)
       {
           try
           {
                var generalRequestLst=new List<RequestModel>();
                var constre = ConfigurationManager.ConnectionStrings["connectionStringProviderSystem"].ToString();
                var connection = new SqlConnection(constre);

                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }

                string query = @"SELECT  era.ActionId,Fdesc FROM EmpRequestActions era 
                                JOIN [Action] a ON  a.ActionId = era.ActionId 
                                WHERE  era.EmpNo = " + empNumber+" AND a.IsPermission = 1 AND a.IsShow = 1";

                connection.Open();
                var command = new SqlCommand(query, connection);

                var result = command.ExecuteReader();
                while (result.Read())
                {
                    if (result["ActionId"].ToString() != "11")
                    {
                        var modelData = new RequestModel
                        {
                            Id = result["ActionId"].ToString(),
                            Title = result["Fdesc"].ToString()
                        };
                        generalRequestLst.Add(modelData);
                    }
                }
                connection.Close();
                connection.Dispose();

               return generalRequestLst;

           }
            catch (Exception)
            {
               return new List<RequestModel>();

            }
         
       }
        /// <summary>
        /// دسترسی به نوع مجوز های موجود
        /// </summary>
        /// <returns>لیست نوع مجوز ها</returns>
        public List<RequestModel> GetAllRequestTypes()
        {
            try
            {
                var generalRequestLst = new List<RequestModel>();
                var constre = ConfigurationManager.ConnectionStrings["connectionStringProviderSystem"].ToString();
                var connection = new SqlConnection(constre);

                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }

                string query = @"SELECT  a.ActionId,Fdesc FROM [Action] a
                                 WHERE   a.IsPermission = 1 AND a.IsShow = 1";

                connection.Open();
                var command = new SqlCommand(query, connection);

                var result = command.ExecuteReader();
                while (result.Read())
                {

                    var modelData = new RequestModel
                    {
                        Id = result["ActionId"].ToString(),
                        Title = result["Fdesc"].ToString()
                    };
                    generalRequestLst.Add(modelData);
                }
                connection.Close();
                connection.Dispose();

                return generalRequestLst;

            }
            catch (Exception)
            {
                return new List<RequestModel>();

            }

        }
        public List<GetCodePermissionMobileViewModel> GetFormCodesForModel(string emp, string actionCode)
       {
           try
           {

               switch (actionCode)
               {
                   case "1":
                       var result = CardRequestLst(emp,5,true,"1");
                       return result;
                    case "2":
                        var resultMamoriyat = CardRequestLst(emp, 3, true,"2");
                        return resultMamoriyat;
                    case "3":
                        var resultHourly = CardRequestLst(emp, 5, false,"3");
                        return resultHourly;
                    case "4":
                        var ioCorrction=new List<GetCodePermissionMobileViewModel>
                        {
                            new GetCodePermissionMobileViewModel
                            {
                                CodeID = "4-101",
                                CodeName = "اصلاح ورود و خروج"
                            }
                        };
                       return ioCorrction;
                        
                    case "5":
                        var overTime = new List<GetCodePermissionMobileViewModel>
                        {
                            new GetCodePermissionMobileViewModel
                            {
                                CodeID = "5-103",
                                CodeName = "اضافه کار"
                            }
                        };
                        return overTime;
                   case "7":
                        var addReduce = new List<GetCodePermissionMobileViewModel>
                        {
                            new GetCodePermissionMobileViewModel
                            {
                                CodeID = "7-104",
                                CodeName = "افزايش يا کاهش مرخصي"
                            }
                        };
                        return addReduce;
                    case "8":
                        var forgetIo = new List<GetCodePermissionMobileViewModel>
                        {
                            new GetCodePermissionMobileViewModel
                            {
                                CodeID = "8-100",
                                CodeName = "ورود و خروج فراموش شده"
                            }
                        };
                        return forgetIo;
                    case "9":
                        var resultHrMamoriyat = CardRequestLst(emp, 3, false,"9");
                        return resultHrMamoriyat;
                    case "11":
                        var showAttendances = new List<GetCodePermissionMobileViewModel>
                        {
                            new GetCodePermissionMobileViewModel()
                        };
                        return showAttendances;
                    case "12":
                        var resultHrMamoriyatOther = CardRequestLst(emp, 3, false,"12");
                        return resultHrMamoriyatOther;
                    case "13":
                        var editForType = new List<GetCodePermissionMobileViewModel>
                        {
                            new GetCodePermissionMobileViewModel
                            {
                                CodeID = "13-105",
                                CodeName = "اصلاح نوع ورود و خروج"
                            }
                        };
                        return editForType;
                }

                return new List<GetCodePermissionMobileViewModel>();

            }
           catch (Exception)
           {
               return new List<GetCodePermissionMobileViewModel>();
               
           }
       }
        public List<GetCodePermissionMobileViewModel> GetAllFormCodesForModel(string actionCode)
        {
            try
            {

                switch (actionCode)
                {
                    case "1":
                        var result = AllCardRequestLst( 5, true, "1");
                        return result;
                    case "2":
                        var resultMamoriyat = AllCardRequestLst( 3, true, "2");
                        return resultMamoriyat;
                    case "3":
                        var resultHourly = AllCardRequestLst( 5, false, "3");
                        return resultHourly;
                    case "4":
                        var ioCorrction = new List<GetCodePermissionMobileViewModel>
                        {
                            new GetCodePermissionMobileViewModel
                            {
                                CodeID = "4-101",
                                CodeName = "اصلاح ورود و خروج"
                            }
                        };
                        return ioCorrction;

                    case "5":
                        var overTime = new List<GetCodePermissionMobileViewModel>
                        {
                            new GetCodePermissionMobileViewModel
                            {
                                CodeID = "5-103",
                                CodeName = "اضافه کار"
                            }
                        };
                        return overTime;
                    case "7":
                        var addReduce = new List<GetCodePermissionMobileViewModel>
                        {
                            new GetCodePermissionMobileViewModel
                            {
                                CodeID = "7-104",
                                CodeName = "افزايش يا کاهش مرخصي"
                            }
                        };
                        return addReduce;
                    case "8":
                        var forgetIo = new List<GetCodePermissionMobileViewModel>
                        {
                            new GetCodePermissionMobileViewModel
                            {
                                CodeID = "8-100",
                                CodeName = "ورود و خروج فراموش شده"
                            }
                        };
                        return forgetIo;
                    case "9":
                        var resultHrMamoriyat = AllCardRequestLst( 3, false, "9");
                        return resultHrMamoriyat;
                    case "11":
                        var showAttendances = new List<GetCodePermissionMobileViewModel>
                        {
                            new GetCodePermissionMobileViewModel()
                        };
                        return showAttendances;
                    case "12":
                        var resultHrMamoriyatOther = AllCardRequestLst( 3, false, "12");
                        return resultHrMamoriyatOther;
                    case "13":
                        var editForType = new List<GetCodePermissionMobileViewModel>
                        {
                            new GetCodePermissionMobileViewModel
                            {
                                CodeID = "13-105",
                                CodeName = "اصلاح نوع ورود و خروج"
                            }
                        };
                        return editForType;
                }

                return new List<GetCodePermissionMobileViewModel>();

            }
            catch (Exception)
            {
                return new List<GetCodePermissionMobileViewModel>();

            }
        }
        public List<GetCodePermissionMobileViewModel> AllCardRequestLst( int cardType, bool isDaily, string ownerId)
        {
            try
            {
                var requestLst = new List<GetCodePermissionMobileViewModel>();
                var constre = ConfigurationManager.ConnectionStrings["connectionStringProviderSystem"].ToString();
                var connection = new SqlConnection(constre);
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
                connection.Open();
                var query = @"SELECT *
FROM   CARDS
WHERE  (CARD_TYPE ="+cardType+ @")
       AND (IS_DAY = '" + isDaily + @"')
       AND (SYS_ACTIVE = 1)
     
       AND LimitValue>0";
                var command = new SqlCommand(query, connection);
               

                var result = command.ExecuteReader();
                while (result.Read())
                {
                    var requestObj = new GetCodePermissionMobileViewModel
                    {
                        CodeID = ownerId + "-" + result["CARD_NO"],
                        CodeName = result["TITLE"].ToString()
                    };
                    requestLst.Add(requestObj);
                }
                connection.Close();
                connection.Dispose();



                return requestLst;
            }
            catch (Exception)
            {

                return new List<GetCodePermissionMobileViewModel>();
            }
        }
        public List<GetCodePermissionMobileViewModel> CardRequestLst(string empNumber,int cardType,bool isDaily,string ownerId)
       {
           try
           {
                var requestLst=new List<GetCodePermissionMobileViewModel>();
                var constre = ConfigurationManager.ConnectionStrings["connectionStringProviderSystem"].ToString();
                var connection = new SqlConnection(constre);
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
                connection.Open();
                var command = new SqlCommand("WF_CARDSGetAccessData", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@Emp_NO", SqlDbType.Float, 2000).Value = empNumber;
                command.Parameters.Add("@is_day", SqlDbType.Bit, 2000).Value = isDaily;
                command.Parameters.Add("@card_type", SqlDbType.Int, 2000).Value = cardType;

                var result = command.ExecuteReader();
                while (result.Read())
                {
                    var requestObj=new GetCodePermissionMobileViewModel
                    {
                        CodeID = ownerId+"-"+result["CARD_NO"],
                        CodeName = result["TITLE"].ToString()
                    };
                    requestLst.Add(requestObj);
                }
                connection.Close();
                connection.Dispose();



                return requestLst;
            }
           catch (Exception)
           {

                return new List<GetCodePermissionMobileViewModel>();
            }
       }
        /// <summary>
        /// ساخت فرم روزانه عادی
        /// </summary>
        /// <returns>لسیت کامپوننت ها</returns>
       public List<FormFieldsMobileViewModel> GetNormalDailyForm(string empCode, RequestRow currentRequest = null, bool lockPersonCombo = false, DateTime selectedDate = new DateTime())
       {
           try
           {
               var lstDataDailyForm = new List<FormFieldsMobileViewModel>();
                var list = new List<FormFieldsMobileViewModel>();
                var portfolioMng = new PortfolioManager(new WorkFlowsDatabase());
                var fullNameOnline = portfolioMng.GetEmpName(empCode);

                var lstSelectedData = new List<ComboDataMobileViewModel>
                {
                    new ComboDataMobileViewModel
                    {
                        Id = empCode,
                        Name = fullNameOnline+" "+empCode
                    }
                };
                lstDataDailyForm.Add(GetAutoCompleteComponent("99", "پرسنل", "Personles", "PersonAutoCompleteCombo", currentRequest == null, lstSelectedData, lockPersonCombo));
                lstDataDailyForm.Add(GetDateComponent("5", "از تاریخ", "StartDate", selectedDate));
                

                lstDataDailyForm.Add(GetDateComponent("6", "تا تاریخ", "EndDate", selectedDate));
                lstDataDailyForm.Add(GetDescriptionComponent("7", "شرح", "Description"));

               var labelText = portfolioMng.GetRemainCardexForForm(empCode);
             
                lstDataDailyForm.Add(GetLabelComponent("18", labelText, "CardexValue"));
                lstDataDailyForm.Add(GetAttachmentComponent("10", "ضمیمه", "AttachedFile"));
                //lstDataDailyForm.Add(GetCkeckBoxComponent("11", "وضعیت اسکان دوم", "StayModel", false,new List<ComboDataMobileViewModel>
                //{
                //    new ComboDataMobileViewModel
                //    {
                //        Id = "1",
                //        Name = "مشخص شده"
                //    },
                //     new ComboDataMobileViewModel
                //    {
                //        Id = "2",
                //        Name = "مشخص نشده"
                //    }
                //}));
                //lstDataDailyForm.Add(GetCkeckBoxComponent("12", "وضعیت اسکان دوم", "StaysModel", true, new List<ComboDataMobileViewModel>
                //{
                //    new ComboDataMobileViewModel
                //    {
                //        Id = "1",
                //        Name = "مشخص شده"
                //    },
                //     new ComboDataMobileViewModel
                //    {
                //        Id = "2",
                //        Name = "مشخص نشده"
                //    }
                //}));
                if (currentRequest != null)
                {
                    foreach (var formFieldsMobileViewModel in lstDataDailyForm)
                    {
                        switch (formFieldsMobileViewModel.Id)
                        {
                            case "6":
                                var jsonDates = "";
                                if (currentRequest.EndDate != null)
                                {
                                    var datemodelEdate = new
                                    {
                                        Y = currentRequest.EndDate != null ? currentRequest.EndDate.Value.Year : 2010,
                                        M = currentRequest.EndDate != null ? currentRequest.EndDate.Value.Month : 01,
                                        D = currentRequest.EndDate != null ? currentRequest.EndDate.Value.Day : 01
                                    };
                                    jsonDates = JsonConvert.SerializeObject(datemodelEdate);
                                }
                                formFieldsMobileViewModel.DefaultValue = jsonDates;
                                list.Add(formFieldsMobileViewModel);
                                break;
                            case "5":
                                var datemodel = new
                                {
                                    Y = currentRequest.StartDate.Year,
                                    M = currentRequest.StartDate.Month,
                                    D = currentRequest.StartDate.Day
                                };
                                var dateModel = JsonConvert.SerializeObject(datemodel);
                                formFieldsMobileViewModel.DefaultValue = dateModel;
                                list.Add(formFieldsMobileViewModel);
                                break;
                            case "7":
                                formFieldsMobileViewModel.DefaultValue = currentRequest.Description;
                                list.Add(formFieldsMobileViewModel);
                                break;
                            case "99":

                                fullNameOnline = portfolioMng.GetEmpName(currentRequest.EmpNo.ToString());

                                lstSelectedData = new List<ComboDataMobileViewModel>
                                                {
                                                    new ComboDataMobileViewModel
                                                    {
                                                        Id = currentRequest.EmpNo.ToString(),
                                                        Name = fullNameOnline+" "+currentRequest.EmpNo
                                                    }
                                                };
                                formFieldsMobileViewModel.Data = lstSelectedData;
                                list.Add(formFieldsMobileViewModel);
                                break;
                            case "18":
                                list.Add(formFieldsMobileViewModel);
                                break;

                        }
                    }
                    return list;
                }
                return lstDataDailyForm;
           }
           catch (Exception)
           {
               
               return new List<FormFieldsMobileViewModel>();
           }
       }
        /// <summary>
        /// ساخت فرم روزانه عادی
        /// </summary>
        /// <returns>لسیت کامپوننت ها</returns>
        public List<FormFieldsMobileViewModel> GetMissonDailyForm(string  empNum, RequestRow currentRequest = null, bool lockPersonCombo = false, DateTime selectedDate = new DateTime())
        {
            try
            {
                var lstDataDailyForm = new List<FormFieldsMobileViewModel>();
                var list = new List<FormFieldsMobileViewModel>();
                var portfolioMng = new PortfolioManager(new WorkFlowsDatabase());
                var fullNameOnline = portfolioMng.GetEmpName(empNum);

                var lstSelectedData = new List<ComboDataMobileViewModel>
                {
                    new ComboDataMobileViewModel
                    {
                        Id = empNum,
                        Name = fullNameOnline+" "+empNum
                    }
                };
                lstDataDailyForm.Add(GetAutoCompleteComponent("99", "پرسنل", "Personles", "PersonAutoCompleteCombo", currentRequest == null, lstSelectedData, lockPersonCombo));
                lstDataDailyForm.Add(GetDateComponent("5", "از تاریخ", "StartDate", selectedDate));
                lstDataDailyForm.Add(GetDateComponent("6", "تا تاریخ", "EndDate", selectedDate));
                lstDataDailyForm.Add(GetDescriptionComponent("7", "مبدا", "FromPalce"));
                lstDataDailyForm.Add(GetDescriptionComponent("8", "مقصد", "ToPlace"));
                var lstValiCombo=new List<ComboDataMobileViewModel>();
                var constre = ConfigurationManager.ConnectionStrings["connectionStringProviderSystem"].ToString();
                var connection = new SqlConnection(constre);
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
                connection.Open();
                var command = new SqlCommand("SELECT BaseCode,BaseValue FROM WFBase w WHERE w.BaseGroup='DIO'", connection);
               


                var result = command.ExecuteReader();
                while (result.Read())
                {
                   var dataComb=new ComboDataMobileViewModel();
                    dataComb.Id = result["BaseCode"].ToString();
                    dataComb.Name= result["BaseValue"].ToString();
                    lstValiCombo.Add(dataComb);
                }

                connection.Close();
                connection.Dispose();
                lstDataDailyForm.Add(GetComboComponent("9", "وسیله رفت", "ToVehicel", lstValiCombo));
                lstDataDailyForm.Add(GetComboComponent("10", "وسیله برگشت", "ReturnVehicel", lstValiCombo));
                var lstEghamatCombo = new List<ComboDataMobileViewModel>
                {
                    new ComboDataMobileViewModel
                    {
                        Id = "1",
                        Name = "تامیین شده"
                    },new ComboDataMobileViewModel
                    {
                        Id = "2",
                        Name = "تامیین نشده"
                    }
                };
                lstDataDailyForm.Add(GetComboComponent("13", "وضعیت اقامت", "StayStatus", lstEghamatCombo));
                lstDataDailyForm.Add(GetDescriptionComponent("11", "هزینه ایاب ذهاب", "TransferPrice"));
                lstDataDailyForm.Add(GetDescriptionComponent("12", "توضیحات", "Description"));
                lstDataDailyForm.Add(GetAttachmentComponent("10", "ضمیمه", "AttachedFile"));
  
                if (currentRequest != null)
                {
                    foreach (var formFieldsMobileViewModel in lstDataDailyForm)
                    {
                        switch (formFieldsMobileViewModel.Id)
                        {
                            case "5":
                                var jsonDates = "";
                                if (currentRequest.EndDate != null)
                                {
                                    var datemodelEdate = new
                                    {
                                        Y = currentRequest.EndDate != null ? currentRequest.EndDate.Value.Year : 2010,
                                        M = currentRequest.EndDate != null ? currentRequest.EndDate.Value.Month : 01,
                                        D = currentRequest.EndDate != null ? currentRequest.EndDate.Value.Day : 01
                                    };
                                    jsonDates = JsonConvert.SerializeObject(datemodelEdate);
                                }
                                formFieldsMobileViewModel.DefaultValue = jsonDates;
                                list.Add(formFieldsMobileViewModel);
                                break;
                            case "6":
                                var datemodel = new
                                {
                                    Y = currentRequest.StartDate.Year,
                                    M = currentRequest.StartDate.Month,
                                    D = currentRequest.StartDate.Day
                                };
                                var dateModel = JsonConvert.SerializeObject(datemodel);
                                formFieldsMobileViewModel.DefaultValue = dateModel;
                                list.Add(formFieldsMobileViewModel);
                                break;
                            case "7":
                                formFieldsMobileViewModel.DefaultValue = currentRequest.Source;
                                list.Add(formFieldsMobileViewModel);
                                break;
                            case "8":
                                formFieldsMobileViewModel.DefaultValue = currentRequest.Distination;
                                list.Add(formFieldsMobileViewModel);
                                break;
                            case "9":
                                formFieldsMobileViewModel.DefaultValue = currentRequest.DeviceWent.ToString();
                                list.Add(formFieldsMobileViewModel);
                                break;
                            case "10":
                                formFieldsMobileViewModel.DefaultValue = currentRequest.DeviceBack.ToString();
                                list.Add(formFieldsMobileViewModel);
                                break;
                            case "11":
                                formFieldsMobileViewModel.DefaultValue = currentRequest.TransferCost.ToString();
                                list.Add(formFieldsMobileViewModel);
                                break;
                            case "12":
                                formFieldsMobileViewModel.DefaultValue = currentRequest.Description;
                                list.Add(formFieldsMobileViewModel);
                                break;
                            case "13":
                                formFieldsMobileViewModel.DefaultValue = currentRequest.LocationStatus?"1":"2";
                                list.Add(formFieldsMobileViewModel);
                                break;

                            case "99":

                                fullNameOnline = portfolioMng.GetEmpName(currentRequest.EmpNo.ToString());

                                lstSelectedData = new List<ComboDataMobileViewModel>
                                                {
                                                    new ComboDataMobileViewModel
                                                    {
                                                        Id = currentRequest.EmpNo.ToString(),
                                                        Name = fullNameOnline+" "+currentRequest.EmpNo
                                                    }
                                                };
                                formFieldsMobileViewModel.Data = lstSelectedData;
                                list.Add(formFieldsMobileViewModel);
                                break;

                        }
                    }
                    return list;
                }
                return lstDataDailyForm;
            }
            catch (Exception)
            {

                return new List<FormFieldsMobileViewModel>();
            }
        }
        /// <summary>
        /// ساخت فرم ساعتی عادی
        /// </summary>
        /// <returns>لیست کامپوننت ها</returns>
        public List<FormFieldsMobileViewModel> GetNormalHrlyForm(string empNum, RequestRow currentRequest = null, bool lockPersonCombo = false, DateTime selectedDate = new DateTime())
        {
            try
            {
                var lstDataDailyForm = new List<FormFieldsMobileViewModel>();
                var list = new List<FormFieldsMobileViewModel>();
                var portfolioMng = new PortfolioManager(new WorkFlowsDatabase());
                var fullNameOnline = portfolioMng.GetEmpName(empNum);

                var lstSelectedData = new List<ComboDataMobileViewModel>
                {
                    new ComboDataMobileViewModel
                    {
                        Id = empNum,
                        Name = fullNameOnline+" "+empNum
                    }
                };
                lstDataDailyForm.Add(GetAutoCompleteComponent("99", "پرسنل", "Personles", "PersonAutoCompleteCombo", currentRequest == null, lstSelectedData, lockPersonCombo));
                lstDataDailyForm.Add(GetDateComponent("1", "تاریخ", "StartDate", selectedDate));
                var lstSelectedIsfirstShift = new List<ComboDataMobileViewModel>
                {
                    new ComboDataMobileViewModel
                    {
                        Id = "1",
                        Name = "مرخصی اول وقت نمی باشد"
                    }, new ComboDataMobileViewModel
                    {
                        Id = "2",
                        Name = "مرخصی اول وقت می باشد"
                    }

                };
                lstDataDailyForm.Add(GetComboComponent("89", "وضعیت مرخصی", "IsFirstTime", lstSelectedIsfirstShift));

                lstDataDailyForm.Add(GetTimeComponent("2", "ساعت", "StartTime"));
                lstDataDailyForm.Add(GetTimeComponent("3", "به مدت", "EndTime"));
                lstDataDailyForm.Add(GetDescriptionComponent("8", "شرح", "Description"));
  
                var labelText = portfolioMng.GetRemainCardexForForm(empNum);

                lstDataDailyForm.Add(GetLabelComponent("18", labelText, "CardexValue"));
                if (currentRequest != null)
                {
                    foreach (var formFieldsMobileViewModel in lstDataDailyForm)
                    {
                        switch (formFieldsMobileViewModel.Id)
                        {
                            case "2":

                                formFieldsMobileViewModel.DefaultValue = UtilityManagers.ConvertToTimeFormat( currentRequest.StartHour.ToString());
                                list.Add(formFieldsMobileViewModel);
                                break;
                            case "3":

                                formFieldsMobileViewModel.DefaultValue = UtilityManagers.ConvertToTimeFormat(currentRequest.Duration);
                                list.Add(formFieldsMobileViewModel);
                                break;
                            case "1":
                                var datemodel = new
                                {
                                    Y = currentRequest.StartDate.Year,
                                    M = currentRequest.StartDate.Month,
                                    D = currentRequest.StartDate.Day
                                };
                                var dateModel = JsonConvert.SerializeObject(datemodel);
                                formFieldsMobileViewModel.DefaultValue = dateModel;
                                list.Add(formFieldsMobileViewModel);
                                break;
                            case "8":
                                formFieldsMobileViewModel.DefaultValue = currentRequest.Description;
                                list.Add(formFieldsMobileViewModel);
                                break;
                            case "99":

                                fullNameOnline = portfolioMng.GetEmpName(currentRequest.EmpNo.ToString());

                                lstSelectedData = new List<ComboDataMobileViewModel>
                                                {
                                                    new ComboDataMobileViewModel
                                                    {
                                                        Id = currentRequest.EmpNo.ToString(),
                                                        Name = fullNameOnline+" "+currentRequest.EmpNo
                                                    }
                                                };
                                formFieldsMobileViewModel.Data = lstSelectedData;
                                list.Add(formFieldsMobileViewModel);
                                break;
                            case "18":
                                list.Add(formFieldsMobileViewModel);
                                break;

                        }
                    }
                    return list;
                }
                return lstDataDailyForm;
            }
            catch (Exception)
            {

                return new List<FormFieldsMobileViewModel>();
            }
        }
        /// <summary>
        /// ساخت فرم ساعتی عادی
        /// </summary>
        /// <returns>لیست کامپوننت ها</returns>
        public List<FormFieldsMobileViewModel> GetMissonHrlyForm(string empNum, RequestRow currentRequest = null, bool lockPersonCombo = false, DateTime selectedDate = new DateTime())
        {
            try
            {
                var lstDataDailyForm = new List<FormFieldsMobileViewModel>();
                var list = new List<FormFieldsMobileViewModel>();
                var portfolioMng = new PortfolioManager(new WorkFlowsDatabase());
                var fullNameOnline = portfolioMng.GetEmpName(empNum);

                var lstSelectedData = new List<ComboDataMobileViewModel>
                {
                    new ComboDataMobileViewModel
                    {
                        Id = empNum,
                        Name = fullNameOnline+" "+empNum
                    }
                };
                lstDataDailyForm.Add(GetAutoCompleteComponent("99", "پرسنل", "Personles", "PersonAutoCompleteCombo", currentRequest == null, lstSelectedData, lockPersonCombo));
                lstDataDailyForm.Add(GetDateComponent("1", "تاریخ", "StartDate", selectedDate));
                lstDataDailyForm.Add(GetTimeComponent("2", "ساعت", "StartTime"));
                lstDataDailyForm.Add(GetTimeComponent("3", "به مدت", "EndTime"));
                //lstDataDailyForm.Add(GetTimeComponent("4", "شناوری", "Tollerance"));
         
                lstDataDailyForm.Add(GetDescriptionComponent("11", "هزینه ایاب ذهاب", "TransferPrice"));
                var lstValiCombo = new List<ComboDataMobileViewModel>();
                var constre = ConfigurationManager.ConnectionStrings["connectionStringProviderSystem"].ToString();
                var connection = new SqlConnection(constre);
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
                connection.Open();
                var command = new SqlCommand("SELECT BaseCode,BaseValue FROM WFBase w WHERE w.BaseGroup='DIO'", connection);



                var result = command.ExecuteReader();
                while (result.Read())
                {
                    var dataComb = new ComboDataMobileViewModel();
                    dataComb.Id = result["BaseCode"].ToString();
                    dataComb.Name = result["BaseValue"].ToString();
                    lstValiCombo.Add(dataComb);
                }

                connection.Close();
                connection.Dispose();
                lstDataDailyForm.Add(GetComboComponent("9", "وسیله رفت", "ToVehicel", lstValiCombo));
                lstDataDailyForm.Add(GetComboComponent("10", "وسیله برگشت", "ReturnVehicel", lstValiCombo));
                lstDataDailyForm.Add(GetDescriptionComponent("8", "شرح", "Description"));
           
                if (currentRequest != null)
                {
                    foreach (var formFieldsMobileViewModel in lstDataDailyForm)
                    {
                        switch (formFieldsMobileViewModel.Id)
                        {
                            case "2":
                               
                                formFieldsMobileViewModel.DefaultValue = UtilityManagers.ConvertToTimeFormat(currentRequest.StartHour.ToString());
                                list.Add(formFieldsMobileViewModel);
                                break;
                            case "3":

                                formFieldsMobileViewModel.DefaultValue = UtilityManagers.ConvertToTimeFormat(currentRequest.Duration);
                                list.Add(formFieldsMobileViewModel);
                                break;
                            case "1":
                                var datemodel = new
                                {
                                    Y = currentRequest.StartDate.Year,
                                    M = currentRequest.StartDate.Month,
                                    D = currentRequest.StartDate.Day
                                };
                                var dateModel = JsonConvert.SerializeObject(datemodel);
                                formFieldsMobileViewModel.DefaultValue = dateModel;
                                list.Add(formFieldsMobileViewModel);
                                break;
                            case "7":
                                formFieldsMobileViewModel.DefaultValue = currentRequest.Source;
                                list.Add(formFieldsMobileViewModel);
                                break;
                            //case "8":
                            //    formFieldsMobileViewModel.DefaultValue = currentRequest.Distination;
                            //    list.Add(formFieldsMobileViewModel);
                            //    break;
                            case "9":
                                formFieldsMobileViewModel.DefaultValue = currentRequest.DeviceWent.ToString();
                                list.Add(formFieldsMobileViewModel);
                                break;
                            case "10":
                                formFieldsMobileViewModel.DefaultValue = currentRequest.DeviceBack.ToString();
                                list.Add(formFieldsMobileViewModel);
                                break;
                            case "11":
                                formFieldsMobileViewModel.DefaultValue = currentRequest.TransferCost.ToString();
                                list.Add(formFieldsMobileViewModel);
                                break;
                            case "8":
                                formFieldsMobileViewModel.DefaultValue = currentRequest.Description;
                                list.Add(formFieldsMobileViewModel);
                                break;
                            //case "13":
                            //    formFieldsMobileViewModel.DefaultValue = currentRequest.LocationStatus ? "1" : "2";
                            //    list.Add(formFieldsMobileViewModel);
                            //    break;

                            case "99":

                                fullNameOnline = portfolioMng.GetEmpName(currentRequest.EmpNo.ToString());

                                lstSelectedData = new List<ComboDataMobileViewModel>
                                                {
                                                    new ComboDataMobileViewModel
                                                    {
                                                        Id = currentRequest.EmpNo.ToString(),
                                                        Name = fullNameOnline+" "+currentRequest.EmpNo
                                                    }
                                                };
                                formFieldsMobileViewModel.Data = lstSelectedData;
                                list.Add(formFieldsMobileViewModel);
                                break;

                        }
                    }
                    return list;
                }
                return lstDataDailyForm;
            }
            catch (Exception)
            {

                return new List<FormFieldsMobileViewModel>();
            }
        }
        /// <summary>
        /// ساخت فرم ساعتی عادی
        /// </summary>
        /// <returns>لیست کامپوننت ها</returns>
        public List<FormFieldsMobileViewModel> GetMissonHrlyWhouthReturnForm(string empNum, RequestRow currentRequest = null, bool lockPersonCombo = false, DateTime selectedDate = new DateTime())
        {
            try
            {
                var lstDataDailyForm = new List<FormFieldsMobileViewModel>();
                var list = new List<FormFieldsMobileViewModel>();
                var portfolioMng = new PortfolioManager(new WorkFlowsDatabase());
                var fullNameOnline = portfolioMng.GetEmpName(empNum);

                var lstSelectedData = new List<ComboDataMobileViewModel>
                {
                    new ComboDataMobileViewModel
                    {
                        Id = empNum,
                        Name = fullNameOnline+" "+empNum
                    }
                };
                lstDataDailyForm.Add(GetAutoCompleteComponent("99", "پرسنل", "Personles", "PersonAutoCompleteCombo", currentRequest == null, lstSelectedData, lockPersonCombo));
                lstDataDailyForm.Add(GetDateComponent("1", "تاریخ", "StartDate", selectedDate));
                lstDataDailyForm.Add(GetTimeComponent("2", "ساعت شروع", "StartTime"));
                lstDataDailyForm.Add(GetTimeComponent("3", "ساعت پایان", "EndTime"));
                //lstDataDailyForm.Add(GetTimeComponent("4", "شناوری", "Tollerance"));


                var lstValiCombo = new List<ComboDataMobileViewModel>
                {
                    new ComboDataMobileViewModel
                    {
                        Id = StatusEnum.ExitFromOrg.ToString(),
                        Name = "شروع ماموریت از سازمان می باشد"
                    },
                    new ComboDataMobileViewModel
                    {
                        Id =  StatusEnum.ExitFromHome.ToString(),
                        Name = "شروع ماموریت خارج از سازمان می باشد"
                    }
                };
                //var constre = ConfigurationManager.ConnectionStrings["connectionStringProviderSystem"].ToString();
                //var connection = new SqlConnection(constre);
                //if (connection.State == ConnectionState.Open)
                //{
                //    connection.Close();
                //}
                //connection.Open();
                //var command = new SqlCommand("SELECT BaseCode,BaseValue FROM WFBase w WHERE w.BaseGroup='DIO'", connection);



                //var result = command.ExecuteReader();
                //while (result.Read())
                //{
                //    var dataComb = new ComboDataMobileViewModel();
                //    dataComb.Id = result["BaseCode"].ToString();
                //    dataComb.Name = result["BaseValue"].ToString();
                //    lstValiCombo.Add(dataComb);
                //}

                //connection.Close();
                //connection.Dispose();
                lstDataDailyForm.Add(GetComboComponent("9", "شروع ماموریت", "StartMisson", lstValiCombo));
               
                lstDataDailyForm.Add(GetDescriptionComponent("8", "شرح", "Description"));
      
                if (currentRequest != null)
                {
                    foreach (var formFieldsMobileViewModel in lstDataDailyForm)
                    {
                        switch (formFieldsMobileViewModel.Id)
                        {
                            case "2":

                                formFieldsMobileViewModel.DefaultValue = UtilityManagers.ConvertToTimeFormat(currentRequest.StartHour.ToString());
                                list.Add(formFieldsMobileViewModel);
                                break;
                            case "3":

                                formFieldsMobileViewModel.DefaultValue = UtilityManagers.ConvertToTimeFormat( currentRequest.EndHour.ToString());
                                list.Add(formFieldsMobileViewModel);
                                break;
                            case "1":
                                var datemodel = new
                                {
                                    Y = currentRequest.StartDate.Year,
                                    M = currentRequest.StartDate.Month,
                                    D = currentRequest.StartDate.Day
                                };
                                var dateModel = JsonConvert.SerializeObject(datemodel);
                                formFieldsMobileViewModel.DefaultValue = dateModel;
                                list.Add(formFieldsMobileViewModel);
                                break;
                            case "7":
                                formFieldsMobileViewModel.DefaultValue = currentRequest.Source;
                                list.Add(formFieldsMobileViewModel);
                                break;
                            //case "8":
                            //    formFieldsMobileViewModel.DefaultValue = currentRequest.Distination;
                            //    list.Add(formFieldsMobileViewModel);
                            //    break;
                            case "9":
                                formFieldsMobileViewModel.DefaultValue = currentRequest.Status== StatusEnum.ExitFromOrg?"1":"2";
                                list.Add(formFieldsMobileViewModel);
                                break;
                           case "8":
                                formFieldsMobileViewModel.DefaultValue = currentRequest.Description;
                                list.Add(formFieldsMobileViewModel);
                                break;
                            //case "13":
                            //    formFieldsMobileViewModel.DefaultValue = currentRequest.LocationStatus ? "1" : "2";
                            //    list.Add(formFieldsMobileViewModel);
                            //    break;

                            case "99":

                                fullNameOnline = portfolioMng.GetEmpName(currentRequest.EmpNo.ToString());

                                lstSelectedData = new List<ComboDataMobileViewModel>
                                                {
                                                    new ComboDataMobileViewModel
                                                    {
                                                        Id = currentRequest.EmpNo.ToString(),
                                                        Name = fullNameOnline+" "+currentRequest.EmpNo
                                                    }
                                                };
                                formFieldsMobileViewModel.Data = lstSelectedData;
                                list.Add(formFieldsMobileViewModel);
                                break;

                        }
                    }
                    return list;
                }
                return lstDataDailyForm;
            }
            catch (Exception)
            {

                return new List<FormFieldsMobileViewModel>();
            }
        }
        /// <summary>
        /// ساخت فرم ساعتی عادی
        /// </summary>
        /// <returns>لیست کامپوننت ها</returns>
        public List<FormFieldsMobileViewModel> GetOverTimeForm(string empNum, RequestRow currentRequest = null,bool lockPersonCombo=false,DateTime selectedDate=new DateTime())
        {
            try
            {
                var lstDataDailyForm = new List<FormFieldsMobileViewModel>();
                var list=new List<FormFieldsMobileViewModel>();
                var portfolioMng = new PortfolioManager(new WorkFlowsDatabase());
                var fullNameOnline = portfolioMng.GetEmpName(empNum);

                var lstSelectedData = new List<ComboDataMobileViewModel>
                {
                    new ComboDataMobileViewModel
                    {
                        Id = empNum,
                        Name = fullNameOnline+" "+empNum
                    }
                };
                lstDataDailyForm.Add(GetAutoCompleteComponent("99", "پرسنل", "Personles", "PersonAutoCompleteCombo", currentRequest==null, lstSelectedData, lockPersonCombo));
                lstDataDailyForm.Add(GetDateComponent("1", "از تاریخ", "StartDate", selectedDate));
                lstDataDailyForm.Add(GetDateComponent("2", "تا تاریخ", "EndDate", selectedDate));
                lstDataDailyForm.Add(GetTimeComponent("3", "به مدت", "EndTime"));
                lstDataDailyForm.Add(GetDescriptionComponent("8", "شرح", "Description"));
      
                var labelText = portfolioMng.SetCountInfo(double.Parse(empNum));

                lstDataDailyForm.Add(GetLabelComponent("18", labelText, "CardexValue"));
                if (currentRequest != null)
                {
                    foreach (var formFieldsMobileViewModel in lstDataDailyForm)
                    {
                        switch (formFieldsMobileViewModel.Id)
                        {
                            case "1":
                                var datemodel = new
                                {
                                    Y = currentRequest.StartDate.Year,
                                    M = currentRequest.StartDate.Month,
                                    D = currentRequest.StartDate.Day
                                };
                                var dateModel = JsonConvert.SerializeObject(datemodel);
                                formFieldsMobileViewModel.DefaultValue = dateModel;
                                list.Add(formFieldsMobileViewModel);
                                break;
                            case "2":
                                var jsonDates = "";
                                if (currentRequest.EndDate != null)
                                {
                                    var datemodelEdate = new
                                    {
                                        Y = currentRequest.EndDate != null ? currentRequest.EndDate.Value.Year : 2010,
                                        M = currentRequest.EndDate != null ? currentRequest.EndDate.Value.Month : 01,
                                        D = currentRequest.EndDate != null ? currentRequest.EndDate.Value.Day : 01
                                    };
                                    jsonDates = JsonConvert.SerializeObject(datemodelEdate);
                                }
                                formFieldsMobileViewModel.DefaultValue = jsonDates;
                                list.Add(formFieldsMobileViewModel);
                                break;
                            case "3":
                                formFieldsMobileViewModel.DefaultValue = UtilityManagers.ConvertToTimeFormat(currentRequest.Duration);
                                list.Add(formFieldsMobileViewModel);
                                break;
                            case "8":
                                formFieldsMobileViewModel.DefaultValue = currentRequest.Description;
                                list.Add(formFieldsMobileViewModel);
                                break;
                            case "99":
                           
                                 fullNameOnline = portfolioMng.GetEmpName(currentRequest.EmpNo.ToString());

                                 lstSelectedData = new List<ComboDataMobileViewModel>
                                                {
                                                    new ComboDataMobileViewModel
                                                    {
                                                        Id = currentRequest.EmpNo.ToString(),
                                                        Name = fullNameOnline+" "+currentRequest.EmpNo
                                                    }
                                                };
                                formFieldsMobileViewModel.Data = lstSelectedData;
                                list.Add(formFieldsMobileViewModel);
                                break;
                            case "18":
                                list.Add(formFieldsMobileViewModel);
                                break;

                        }
                    }
                    return list;
                }
                return lstDataDailyForm;
            }
            catch (Exception)
            {

                return new List<FormFieldsMobileViewModel>();
            }
        }
        /// <summary>
        /// ساخت فرم ساعتی عادی
        /// </summary>
        /// <returns>لیست کامپوننت ها</returns>
        public List<FormFieldsMobileViewModel> GetForGetAttendanceForm(string empNum, RequestRow currentRequest = null, bool lockPersonCombo = false, DateTime selectedDate = new DateTime())
        {
            try
            {
                var lstDataDailyForm = new List<FormFieldsMobileViewModel>();
                var list = new List<FormFieldsMobileViewModel>();
                var portfolioMng = new PortfolioManager(new WorkFlowsDatabase());
                var fullNameOnline = portfolioMng.GetEmpName(empNum);

                var lstSelectedData = new List<ComboDataMobileViewModel>
                {
                    new ComboDataMobileViewModel
                    {
                        Id = empNum,
                        Name = fullNameOnline+" "+empNum
                    }
                };
                lstDataDailyForm.Add(GetAutoCompleteComponent("99", "پرسنل", "Personles", "PersonAutoCompleteCombo", currentRequest == null, lstSelectedData, lockPersonCombo));
                lstDataDailyForm.Add(GetDateComponent("1", "از تاریخ", "StartDate", selectedDate));
                lstDataDailyForm.Add(GetTimeComponent("3", "ساعت", "SelectedTime"));
                var lstValiCombo = new List<ComboDataMobileViewModel>();
                var lastdataComb = new ComboDataMobileViewModel();
                lastdataComb.Id = "0";
                lastdataComb.Name = "عادی";
                lstValiCombo.Add(lastdataComb);
                var constre = ConfigurationManager.ConnectionStrings["connectionStringProviderSystem"].ToString();
                var connection = new SqlConnection(constre);
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
                connection.Open();
                var command = new SqlCommand("WF_CARDSGetCardsData", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@is_day", SqlDbType.Bit, 2000).Value = false;
                command.Parameters.Add("@EmpNo", SqlDbType.Float, 2000).Value = float.Parse(empNum);
                var result = command.ExecuteReader();
                while (result.Read())
                {
                    var dataComb = new ComboDataMobileViewModel();
                    dataComb.Id = result["CARD_NO"].ToString();
                    dataComb.Name = result["TITLE"].ToString();
                    lstValiCombo.Add(dataComb);
                }

                connection.Close();
                connection.Dispose();
               
                lstDataDailyForm.Add(GetComboComponent("9", "عنوان", "RequestTitle", lstValiCombo));
                lstDataDailyForm.Add(GetDescriptionComponent("8", "شرح", "Description"));
              
                var labelText = portfolioMng.SetCountInfoForgetIO(double.Parse(empNum));

                lstDataDailyForm.Add(GetLabelComponent("18", labelText, "CardexValue"));
                if (currentRequest != null)
                {
                    foreach (var formFieldsMobileViewModel in lstDataDailyForm)
                    {
                        switch (formFieldsMobileViewModel.Id)
                        {
                            case "1":
                                var datemodel = new
                                {
                                    Y = currentRequest.StartDate.Year,
                                    M = currentRequest.StartDate.Month,
                                    D = currentRequest.StartDate.Day
                                };
                                var dateModel = JsonConvert.SerializeObject(datemodel);
                                formFieldsMobileViewModel.DefaultValue = dateModel;
                                list.Add(formFieldsMobileViewModel);
                                break;
                            case "3":
                                formFieldsMobileViewModel.DefaultValue = UtilityManagers.ConvertToTimeFormat(currentRequest.StartHour.ToString());
                                list.Add(formFieldsMobileViewModel);
                                break;
                            case "9":
                                formFieldsMobileViewModel.DefaultValue = currentRequest.Duration;
                                list.Add(formFieldsMobileViewModel);
                                break;
                            case "8":
                                formFieldsMobileViewModel.DefaultValue = currentRequest.Description;
                                list.Add(formFieldsMobileViewModel);
                                break;
                            case "99":

                                fullNameOnline = portfolioMng.GetEmpName(currentRequest.EmpNo.ToString());

                                lstSelectedData = new List<ComboDataMobileViewModel>
                                                {
                                                    new ComboDataMobileViewModel
                                                    {
                                                        Id = currentRequest.EmpNo.ToString(),
                                                        Name = fullNameOnline+" "+currentRequest.EmpNo
                                                    }
                                                };
                                formFieldsMobileViewModel.Data = lstSelectedData;
                                list.Add(formFieldsMobileViewModel);
                                break;
                            case "18":
                                list.Add(formFieldsMobileViewModel);
                                break;

                        }
                    }
                    return list;
                }
                return lstDataDailyForm;
            }
            catch (Exception)
            {

                return new List<FormFieldsMobileViewModel>();
            }
        }
        /// <summary>
        /// ساخت فرم ساعتی عادی
        /// </summary>
        /// <returns>لیست کامپوننت ها</returns>
        public List<FormFieldsMobileViewModel> GetAddReduceHolidaysForm(string empNum, RequestRow currentRequest = null, bool lockPersonCombo = false, DateTime selectedDate = new DateTime())
        {
            try
            {
                var lstDataDailyForm = new List<FormFieldsMobileViewModel>();
                var list = new List<FormFieldsMobileViewModel>();
                var portfolioMng = new PortfolioManager(new WorkFlowsDatabase());
                var fullNameOnline = portfolioMng.GetEmpName(empNum);

                var lstSelectedData = new List<ComboDataMobileViewModel>
                {
                    new ComboDataMobileViewModel
                    {
                        Id = empNum,
                        Name = fullNameOnline+" "+empNum
                    }
                };
                lstDataDailyForm.Add(GetAutoCompleteComponent("99", "پرسنل", "Personles", "PersonAutoCompleteCombo", currentRequest == null, lstSelectedData, lockPersonCombo));
                lstDataDailyForm.Add(GetDateComponent("1", "تاریخ", "StartDate", selectedDate));
                //lstDataDailyForm.Add(GetDescriptionComponent("3", "به مدت", "EndTime"));
                lstDataDailyForm.Add(GetTimeDurationComponent("3", "مقدار", "DurationTimes"));
                var lstEghamatCombo = new List<ComboDataMobileViewModel>
                {
                    new ComboDataMobileViewModel
                    {
                        Id = "0",
                        Name = "افزایش"
                    },new ComboDataMobileViewModel
                    {
                        Id = "1",
                        Name = "کاهش"
                    }
                };


                lstDataDailyForm.Add(GetComboComponent("9", "نوع", "TypeAddReduce", lstEghamatCombo));
              
                lstDataDailyForm.Add(GetDescriptionComponent("8", "شرح", "Description"));
       
                if (currentRequest != null)
                {
                    foreach (var formFieldsMobileViewModel in lstDataDailyForm)
                    {
                        switch (formFieldsMobileViewModel.Id)
                        {
                            case "1":
                                var datemodel = new
                                {
                                    Y = currentRequest.StartDate.Year,
                                    M = currentRequest.StartDate.Month,
                                    D = currentRequest.StartDate.Day
                                };
                                var dateModel = JsonConvert.SerializeObject(datemodel);
                                formFieldsMobileViewModel.DefaultValue = dateModel;
                                list.Add(formFieldsMobileViewModel);
                                break;
                            case "3":
                                formFieldsMobileViewModel.DefaultValue = UtilityManagers.ConvertToTimeFormat(currentRequest.Duration);
                                list.Add(formFieldsMobileViewModel);
                                break;
                            case "9":
                                formFieldsMobileViewModel.DefaultValue = currentRequest.StartHour.ToString();
                                list.Add(formFieldsMobileViewModel);
                                break;
                            case "8":
                                formFieldsMobileViewModel.DefaultValue = currentRequest.Description;
                                list.Add(formFieldsMobileViewModel);
                                break;
                            case "99":

                                fullNameOnline = portfolioMng.GetEmpName(currentRequest.EmpNo.ToString());

                                lstSelectedData = new List<ComboDataMobileViewModel>
                                                {
                                                    new ComboDataMobileViewModel
                                                    {
                                                        Id = currentRequest.EmpNo.ToString(),
                                                        Name = fullNameOnline+" "+currentRequest.EmpNo
                                                    }
                                                };
                                formFieldsMobileViewModel.Data = lstSelectedData;
                                list.Add(formFieldsMobileViewModel);
                                break;

                        }
                    }
                    return list;
                }
                return lstDataDailyForm;
            }
            catch (Exception)
            {

                return new List<FormFieldsMobileViewModel>();
            }
        }
        /// <summary>
        /// ساخت فرم ساعتی عادی
        /// </summary>
        /// <returns>لیست کامپوننت ها</returns>
        public List<FormFieldsMobileViewModel> GetIoCorrectionForm(string empNum, RequestRow currentRequest = null, bool lockPersonCombo = false, DateTime selectedDate = new DateTime())
        {
            try
            {
                var lstDataDailyForm = new List<FormFieldsMobileViewModel>();
                var list = new List<FormFieldsMobileViewModel>();
                var portfolioMng = new PortfolioManager(new WorkFlowsDatabase());
                var fullNameOnline = portfolioMng.GetEmpName(empNum);

                var lstSelectedData = new List<ComboDataMobileViewModel>
                {
                    new ComboDataMobileViewModel
                    {
                        Id = empNum,
                        Name = fullNameOnline+" "+empNum
                    }
                };
                lstDataDailyForm.Add(GetAutoCompleteComponent("99", "پرسنل", "Personles", "PersonAutoCompleteCombo", false, lstSelectedData, lockPersonCombo));
                lstDataDailyForm.Add(GetDateComponent("1", " تاریخ", "StartDate", selectedDate));
                lstDataDailyForm.Add(GetTimeComponent("3", "ساعت قدیم", "SelectedTime"));
                lstDataDailyForm.Add(GetTimeComponent("4", "ساعت جدید", "NewSelectedTime"));
                var lstValiCombo = new List<ComboDataMobileViewModel>();
                var constre = ConfigurationManager.ConnectionStrings["connectionStringProviderSystem"].ToString();
                var connection = new SqlConnection(constre);
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
                connection.Open();
                var command = new SqlCommand("WF_CARDSGetCardsData", connection);
                command.CommandType=CommandType.StoredProcedure;
                command.Parameters.Add("@is_day", SqlDbType.Bit, 2000).Value = false;
                command.Parameters.Add("@EmpNo", SqlDbType.Float, 2000).Value = float.Parse(empNum);
                var result = command.ExecuteReader();
                while (result.Read())
                {
                    var dataComb = new ComboDataMobileViewModel();
                    dataComb.Id = result["CARD_NO"].ToString();
                    dataComb.Name = result["TITLE"].ToString();
                    lstValiCombo.Add(dataComb);
                }

                connection.Close();
                connection.Dispose();
                var lastdataComb = new ComboDataMobileViewModel();
                lastdataComb.Id = "0";
                lastdataComb.Name = "عادی";
                lstValiCombo.Add(lastdataComb);
                lstDataDailyForm.Add(GetComboComponent("9", "عنوان", "RequestTitle", lstValiCombo));
                lstDataDailyForm.Add(GetDescriptionComponent("8", "شرح", "Description"));
                
                if (currentRequest != null)
                {
                    foreach (var formFieldsMobileViewModel in lstDataDailyForm)
                    {
                        switch (formFieldsMobileViewModel.Id)
                        {
                            case "1":
                                var datemodel = new
                                {
                                    Y = currentRequest.StartDate.Year,
                                    M = currentRequest.StartDate.Month,
                                    D = currentRequest.StartDate.Day
                                };
                                var dateModel = JsonConvert.SerializeObject(datemodel);
                                formFieldsMobileViewModel.DefaultValue = dateModel;
                                list.Add(formFieldsMobileViewModel);
                                break;
                            case "3":
                                formFieldsMobileViewModel.DefaultValue = UtilityManagers.ConvertToTimeFormat(currentRequest.StartHour.ToString());
                                list.Add(formFieldsMobileViewModel);
                                break;
                            case "4":
                                formFieldsMobileViewModel.DefaultValue = UtilityManagers.ConvertToTimeFormat(currentRequest.EndHour.ToString());
                                list.Add(formFieldsMobileViewModel);
                                break;
                            case "9":
                                formFieldsMobileViewModel.DefaultValue =  currentRequest.Duration;
                                list.Add(formFieldsMobileViewModel);
                                break;
                            case "8":
                                formFieldsMobileViewModel.DefaultValue = currentRequest.Description;
                                list.Add(formFieldsMobileViewModel);
                                break;
                            case "99":

                                fullNameOnline = portfolioMng.GetEmpName(currentRequest.EmpNo.ToString());

                                lstSelectedData = new List<ComboDataMobileViewModel>
                                                {
                                                    new ComboDataMobileViewModel
                                                    {
                                                        Id = currentRequest.EmpNo.ToString(),
                                                        Name = fullNameOnline+" "+currentRequest.EmpNo
                                                    }
                                                };
                                formFieldsMobileViewModel.Data = lstSelectedData;
                                list.Add(formFieldsMobileViewModel);
                                break;

                        }
                    }
                    return list;
                }
                return lstDataDailyForm;
            }
            catch (Exception)
            {

                return new List<FormFieldsMobileViewModel>();
            }
        }
        /// <summary>
        /// ساخت فرم ساعتی عادی
        /// </summary>
        /// <returns>لیست کامپوننت ها</returns>
        public List<FormFieldsMobileViewModel> GetIoTypeCorrectionForm(string empNum, RequestRow currentRequest = null, bool lockPersonCombo = false, DateTime selectedDate = new DateTime())
        {
            try
            {
                var lstDataDailyForm = new List<FormFieldsMobileViewModel>();
                var list= new List<FormFieldsMobileViewModel>();
                var portfolioMng = new PortfolioManager(new WorkFlowsDatabase());
                var fullNameOnline = portfolioMng.GetEmpName(empNum);

                var lstSelectedData = new List<ComboDataMobileViewModel>
                {
                    new ComboDataMobileViewModel
                    {
                        Id = empNum,
                        Name = fullNameOnline+" "+empNum
                    }
                };
                lstDataDailyForm.Add(GetAutoCompleteComponent("99", "پرسنل", "Personles", "PersonAutoCompleteCombo", false, lstSelectedData, lockPersonCombo));
                lstDataDailyForm.Add(GetDateComponent("1", " تاریخ", "StartDate", selectedDate));
                lstDataDailyForm.Add(GetTimeComponent("3", "ساعت ", "SelectedTime"));
                //lstDataDailyForm.Add(GetTimeComponent("4", "ساعت جدید", "NewSelectedTime"));
                var lstValiCombo = new List<ComboDataMobileViewModel>();
                var constre = ConfigurationManager.ConnectionStrings["connectionStringProviderSystem"].ToString();
                var connection = new SqlConnection(constre);
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
                connection.Open();
                var command = new SqlCommand("WF_CARDSGetCardsData", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@is_day", SqlDbType.Bit, 2000).Value = false;
                command.Parameters.Add("@EmpNo", SqlDbType.Float, 2000).Value = float.Parse(empNum);
                var result = command.ExecuteReader();
                while (result.Read())
                {
                    var dataComb = new ComboDataMobileViewModel();
                    dataComb.Id = result["CARD_NO"].ToString();
                    dataComb.Name = result["TITLE"].ToString();
                    lstValiCombo.Add(dataComb);
                }

                connection.Close();
                connection.Dispose();
                var lastdataComb = new ComboDataMobileViewModel();
                lastdataComb.Id = "0";
                lastdataComb.Name = "عادی";
                lstValiCombo.Add(lastdataComb);
                lstDataDailyForm.Add(GetComboComponent("9", "عنوان", "RequestTitle", lstValiCombo));
                lstDataDailyForm.Add(GetDescriptionComponent("8", "شرح", "Description"));
            
                if (currentRequest != null)
                {
                    foreach (var formFieldsMobileViewModel in lstDataDailyForm)
                    {
                        switch (formFieldsMobileViewModel.Id)
                        {
                            case "1":
                                var datemodel = new
                                {
                                    Y = currentRequest.StartDate.Year,
                                    M = currentRequest.StartDate.Month,
                                    D = currentRequest.StartDate.Day
                                };
                                var dateModel = JsonConvert.SerializeObject(datemodel);
                                formFieldsMobileViewModel.DefaultValue = dateModel;
                                list.Add(formFieldsMobileViewModel);
                                break;
                            case "3":
                                formFieldsMobileViewModel.DefaultValue = UtilityManagers.ConvertToTimeFormat( currentRequest.StartHour.ToString());
                                list.Add(formFieldsMobileViewModel);
                                break;
                            //case "4":
                            //    formFieldsMobileViewModel.DefaultValue = currentRequest.EndHour.ToString();
                            //    list.Add(formFieldsMobileViewModel);
                            //    break;
                            case "9":
                                formFieldsMobileViewModel.DefaultValue =currentRequest.Duration;
                                list.Add(formFieldsMobileViewModel);
                                break;
                            case "8":
                                formFieldsMobileViewModel.DefaultValue = currentRequest.Description;
                                list.Add(formFieldsMobileViewModel);
                                break;
                            case "99":

                                fullNameOnline = portfolioMng.GetEmpName(currentRequest.EmpNo.ToString());

                                lstSelectedData = new List<ComboDataMobileViewModel>
                                                {
                                                    new ComboDataMobileViewModel
                                                    {
                                                        Id = currentRequest.EmpNo.ToString(),
                                                        Name = fullNameOnline+" "+currentRequest.EmpNo
                                                    }
                                                };
                                formFieldsMobileViewModel.Data = lstSelectedData;
                                list.Add(formFieldsMobileViewModel);
                                break;

                        }
                    }
                    return list;
                }
                return lstDataDailyForm;
            }
            catch (Exception)
            {

                return new List<FormFieldsMobileViewModel>();
            }
        }
        /// <summary>
        /// ساخت کامپوننت تاریخ
        /// </summary>
        /// <param name="id">شناسه</param>
        /// <param name="title">عنوان</param>
        /// <param name="name">نام</param>
        /// <param name="selectedDate">تاریخ</param>
        /// <returns>فرم ساعتی</returns>
        public FormFieldsMobileViewModel GetDateComponent(string id, string title,string name,DateTime selectedDate)
       {
           try
           {
                var datemodel = new
                {
                    Y = selectedDate.Year,
                    M = selectedDate.Month,
                    D = selectedDate.Day
                };
                var dateModel = JsonConvert.SerializeObject(datemodel);
                FormFieldsMobileViewModel firstDate = new FormFieldsMobileViewModel
                {
                    IsExtended = false,
                    Id = id,
                    Data = null,
                    DefaultValue = dateModel,
                    Greater = "",
                    Less = "",
                    Name = name,
                    Title = title,
                    Required = true,
                    Type = "DateComponent"
                };
               return firstDate;
           }
           catch (Exception)
           {
               
               return  new FormFieldsMobileViewModel();
           }
       }
        /// <summary>
        /// کامپوننت شرح
        /// </summary>
        /// <param name="id">شناسه </param>
        /// <param name="title">عنوان</param>
        /// <param name="name">نام</param>
        /// <returns>کامپوننت شرح</returns>
        public FormFieldsMobileViewModel GetDescriptionComponent(string id, string title, string name)
        {
            try
            {
               
                FormFieldsMobileViewModel firstDate = new FormFieldsMobileViewModel
                {
                    IsExtended = false,
                    Id = id,
                    Data = null,
                    DefaultValue = "",
                    Greater = "",
                    Less = "",
                    Name = name,
                    Title = title,
                    Required = false,
                    Type = "TextArea"
                };
                return firstDate;
            }
            catch (Exception)
            {

                return new FormFieldsMobileViewModel();
            }
        }

       /// <summary>
       /// کامپوننت شرح
       /// </summary>
       /// <param name="id">شناسه </param>
       /// <param name="title">عنوان</param>
       /// <param name="name">نام</param>
       /// <param name="multiSelect">چند انتخابی</param>
       /// <param name="data">گزینه ها</param>
       /// <returns>کامپوننت شرح</returns>
       public FormFieldsMobileViewModel GetCkeckBoxComponent(string id, string title, string name, bool multiSelect, List<ComboDataMobileViewModel> data)
        {
            try
            {

                FormFieldsMobileViewModel firstDate = new FormFieldsMobileViewModel
                {
                    IsExtended = false,
                    Id = id,
                    Data = data,
                    DefaultValue = "",
                    Greater = "",
                    Less = "",
                    Name = name,
                    Title = title,
                    Required = false,
                    Type = "SelectBox",
                    IsMultiSelect = multiSelect
                };
                return firstDate;
            }
            catch (Exception)
            {

                return new FormFieldsMobileViewModel();
            }
        }

       /// <summary>
       /// کامپوننت شرح
       /// </summary>
       /// <param name="id">شناسه </param>
       /// <param name="title">عنوان</param>
       /// <param name="name">نام</param>
       /// <param name="serviceName">نام سرویس پر کننده داده</param>
       /// <param name="multiSelect">آیا چند انتخابی هست یا خیر</param>
       /// <param name="data">داده مورد نیاز پر شده</param>
       /// <param name="notEditrable">ویرایش پذیر نیست</param>
       /// <returns>کامپوننت شرح</returns>
       public FormFieldsMobileViewModel GetAutoCompleteComponent(string id, string title, string name,string  serviceName,bool multiSelect,List<ComboDataMobileViewModel> data,bool notEditrable=false)
        {
            try
            {

                FormFieldsMobileViewModel firstDate = new FormFieldsMobileViewModel
                {
                    IsExtended = false,
                    Id = id,
                    Data = data,
                    DefaultValue = "",
                    Greater = "",
                    Less = "",
                    Name = name,
                    Title = title,
                    Required = false,
                    Type = "AutoCompleteField",
                    Icon = "ion-person-stalker",
                    IsMultiSelect = multiSelect,
                    ServiceName = serviceName,
                    NotEditable = notEditrable

                };
                return firstDate;
            }
            catch (Exception)
            {

                return new FormFieldsMobileViewModel();
            }
        }

        /// <summary>
        /// کامپوننت ساعت
        /// </summary>
        /// <param name="id">شناسه</param>
        /// <param name="title">عنوان</param>
        /// <param name="name">نام</param>
        /// <param name="defaultValue">مقدار پیشفرض</param>
        /// <returns>اطلاعات کامپوننت</returns>
        public FormFieldsMobileViewModel GetTimeComponent(string id, string title, string name,string defaultValue= "00:00")
        {
            try
            {

                FormFieldsMobileViewModel firstDate = new FormFieldsMobileViewModel
                {
                    IsExtended = false,
                    Id = id,
                    Data = null,
                    DefaultValue = defaultValue,
                    Greater = "",
                    Less = "",
                    Name = name,
                    Title = title,
                    Required = true,
                    Type = "TimeComponent"
                };
                return firstDate;
            }
            catch (Exception)
            {

                return new FormFieldsMobileViewModel();
            }
        }
        /// <summary>
        /// کامپوننت لیبال
        /// </summary>
        /// <param name="id">شناسه</param>
        /// <param name="title">عنوان</param>
        /// <param name="name">نام</param>
     /// <returns>اطلاعات کامپوننت</returns>
        public FormFieldsMobileViewModel GetLabelComponent(string id, string title, string name)
        {
            try
            {

                FormFieldsMobileViewModel firstDate = new FormFieldsMobileViewModel
                {
                    IsExtended = false,
                    Id = id,
                    Data = null,
                    DefaultValue = title,
                    Greater = "",
                    Less = "",
                    Name = name,
                    Title = "",
                    Required = false,
                    Type = "labelField"
                };
                return firstDate;
            }
            catch (Exception)
            {

                return new FormFieldsMobileViewModel();
            }
        }
        /// <summary>
        /// کامپوننت لیبال
        /// </summary>
        /// <param name="id">شناسه</param>
        /// <param name="title">عنوان</param>
        /// <param name="name">نام</param>
        /// <returns>اطلاعات کامپوننت</returns>
        public FormFieldsMobileViewModel GetAttachmentComponent(string id, string title, string name)
        {
            try
            {

                FormFieldsMobileViewModel firstDate = new FormFieldsMobileViewModel
                {
                    IsExtended = false,
                    Id = id,
                    Data = null,
                    DefaultValue = "3",
                    Greater = "",
                    Less = "",
                    Name = name,
                    Title = title,
                    Required = false,
                    Type = "AttachmentField"
                };
                return firstDate;
            }
            catch (Exception)
            {

                return new FormFieldsMobileViewModel();
            }
        }

        /// <summary>
        /// کامپوننت لیبال
        /// </summary>
        /// <param name="id">شناسه</param>
        /// <param name="title">عنوان</param>
        /// <param name="name">نام</param>
        /// <param name="defaultValue">مقدار پیش فرض</param>
        /// <returns>اطلاعات کامپوننت</returns>
        public FormFieldsMobileViewModel GetTimeDurationComponent(string id, string title, string name,string defaultValue="00:00")
        {
            try
            {

                FormFieldsMobileViewModel firstDate = new FormFieldsMobileViewModel
                {
                    IsExtended = false,
                    Id = id,
                    Data = null,
                    DefaultValue = defaultValue,
                    Greater = "",
                    Less = "",
                    Name = name,
                    Title = title,
                    Required = false,
                    Type = "TimeDurationField"
                };
                return firstDate;
            }
            catch (Exception)
            {

                return new FormFieldsMobileViewModel();
            }
        }
        /// <summary>
        /// کامپوننت ساعت
        /// </summary>
        /// <param name="id">شناسه</param>
        /// <param name="title">عنوان</param>
        /// <param name="name">نام</param>
        /// <param name="comboData">مقدار لیست کومبو</param>
        /// <returns>اطلاعات کامپوننت</returns>
        public FormFieldsMobileViewModel GetComboComponent(string id, string title, string name, List<ComboDataMobileViewModel> comboData)
        {
            try
            {

                FormFieldsMobileViewModel firstDate = new FormFieldsMobileViewModel
                {
                    IsExtended = false,
                    Id = id,
                    Data = comboData,
                    DefaultValue = "",
                    Greater = "",
                    Less = "",
                    Name = name,
                    Title = title,
                    Required = true,
                    Type = "ComboBox"
                };
                return firstDate;
            }
            catch (Exception)
            {

                return new FormFieldsMobileViewModel();
            }
        }

       public ResultViewModel GetFormFieldForEdit(string onlineUser, string requestId)
       {
           try
           {
                var currentRequest = WorkFlowsDatabase.Instance.Requests.GetDataById(int.Parse(requestId));
                int num = 0;
                List<FormFieldsMobileViewModel> list = new List<FormFieldsMobileViewModel>();
                var requestfrmManager = new RequestFormLoader();
                //PageModeEnum pageModeEnum = currentRequest.IsFinalApproved.HasValue ? PageModeEnum.NoDelete : PageModeEnum.Edit;
                switch (currentRequest.OperationsId)
                {
                    case 1:
                        //===================OverTime Form=======================
                        list = requestfrmManager.GetOverTimeForm(currentRequest.EmpNo.ToString(), currentRequest);
                        break;
                    case 2:
                        if (currentRequest.Type > 0)
                        {
                            switch (currentRequest.Type)
                            {
                                case 100:

                                    //====================forgottenInOutRequest=================
                                    list = requestfrmManager.GetForGetAttendanceForm(currentRequest.EmpNo.ToString(), currentRequest);
                                    break;
                                case 101:
                                    //====================inOutCorrectionRequest=================
                                    list = requestfrmManager.GetIoCorrectionForm(currentRequest.EmpNo.ToString(), currentRequest);
                                    break;
                                case 102:
                                    //====================shiftReplaceRequest=================
                                    break;
                                case 105:
                                    //====================inOutTypeCorrectionRequest=================
                                    list = requestfrmManager.GetIoTypeCorrectionForm(currentRequest.EmpNo.ToString(), currentRequest);
                                    break;
                            }
                        }
                        break;
                    case 3:
                        if (currentRequest.Type > 0)
                        {
                            if (WorkFlowsDatabase.Instance.Cards.IsDayByCardNo(currentRequest.Type))
                            {
                                //====================DailyDuty=================
                                list = requestfrmManager.GetMissonDailyForm(currentRequest.EmpNo.ToString(), currentRequest);
                            }
                            else
                            {
                                //====================TimeDuty=================
                                list = requestfrmManager.GetMissonHrlyForm(currentRequest.EmpNo.ToString(), currentRequest);
                            }
                        }
                        break;
                    case 4:
                        //====================timeDutyNoReturn=================
                        list = requestfrmManager.GetMissonHrlyWhouthReturnForm(currentRequest.EmpNo.ToString(), currentRequest);
                        break;
                    case 5:
                        if (currentRequest.Type > 0)
                        {
                            if (WorkFlowsDatabase.Instance.Cards.IsDayByCardNo(currentRequest.Type))
                            {
                                //====================DailyRequest=================
                                list = requestfrmManager.GetNormalDailyForm(currentRequest.EmpNo.ToString(), currentRequest);
                            }
                            else if (currentRequest.Type == 104)
                            {
                                //====================leaveAdditionRequest=================
                                list = requestfrmManager.GetAddReduceHolidaysForm(currentRequest.EmpNo.ToString(), currentRequest);
                            }
                            else
                            {
                                //====================TimeLeave=================
                                list = requestfrmManager.GetNormalHrlyForm(currentRequest.EmpNo.ToString(), currentRequest);
                            }
                        }
                        else
                        {
                            //====================LeaveAdditionRequest=================
                            list = requestfrmManager.GetAddReduceHolidaysForm(currentRequest.EmpNo.ToString(), currentRequest);
                        }
                        break;
                }
                return new ResultViewModel
                {
                    Validate = true,
                    Message = JsonConvert.SerializeObject(list)
                };
            
            }
           catch (Exception ex)
           {
               
              return new ResultViewModel
              {
                  Validate = false,
                  ValidateMessage = ex.Message
              };
           }
       }
        /// <summary>
        /// بدست اوردن نام دوره بر اساس نام ماه های شمسی
        /// </summary>
        /// <param name="workperiodNumber">شماره دوره</param>
        /// <returns>نام دوره</returns>
       public string WorkPeriodNameInShamsi(int workperiodNumber)
       {
           try
           {
               switch (workperiodNumber)
               {
                    case 1:
                       return "فروردین";
                    case 2:
                        return "اردیبهشت";
                    case 3:
                        return "خرداد";
                    case 4:
                        return "تیر";
                    case 5:
                        return "مرداد";
                    case 6:
                        return "شهریور";
                    case 7:
                        return "مهر";
                    case 8:
                        return "آبان";
                    case 9:
                        return "آذر";
                    case 10:
                        return "دی";
                    case 11:
                        return "بهمن";
                    case 12:
                        return "اسفند";
                }
                return "نام دوره ناموجود";
            }
           catch (Exception )
           {
               return "نام دوره ناموجود";

           }
       }
    }
    // Token: 0x02000018 RID: 24
    public enum PageModeEnum
    {
        // Token: 0x040000EF RID: 239
        View,
        // Token: 0x040000F0 RID: 240
        Insert,
        // Token: 0x040000F1 RID: 241
        Edit,
        // Token: 0x040000F2 RID: 242
        Delete,
        // Token: 0x040000F3 RID: 243
        ReadOnly,
        // Token: 0x040000F4 RID: 244
        NoDelete
    }
}
