﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiddleWare.KaraLib
{
   public class ShDate
    {
       
            // Token: 0x0600008A RID: 138 RVA: 0x00004C74 File Offset: 0x00002E74
            private static void IncMonthB(ref short Year, ref short Month, ref short Day, int NumberOfMonths)
            {
                if (NumberOfMonths >= 0)
                {
                    Year = Convert.ToInt16((int)Year + NumberOfMonths / 12);
                    NumberOfMonths = (int)Convert.ToInt16(NumberOfMonths % 12);
                    Month = Convert.ToInt16((int)Month + NumberOfMonths);
                    if (Month - 1 > 11)
                    {
                        Year += 1;
                        Month -= 12;
                    }
                }
                else
                {
                    int num = Math.Abs(NumberOfMonths);
                    Year = Convert.ToInt16((int)Year - num / 12);
                    num = (int)Convert.ToInt16(num % 12);
                    Month = Convert.ToInt16((int)Month - num);
                    if (Month < 1)
                    {
                        Year -= 1;
                        Month += 12;
                    }
                }
                object[] array = (object[])ShDate.MonthDaysArray[ShDate.IsLeapYear(Year) ? 0 : 1];
                if (Day > Convert.ToInt16(array[(int)(Month - 1)]))
                {
                    Day = Convert.ToInt16(array[(int)(Month - 1)]);
                }
            }

            // Token: 0x0600008B RID: 139 RVA: 0x00004D35 File Offset: 0x00002F35
            private static void civil_persian(ref short iYear, ref short iMonth, ref short iDay)
            {
                ShDate.jdn_persian(ShDate.civil_jdn(iYear, iMonth, iDay, 1), ref iYear, ref iMonth, ref iDay);
            }

            // Token: 0x0600008C RID: 140 RVA: 0x00004D4B File Offset: 0x00002F4B
            private static void persian_civil(ref short iYear, ref short iMonth, ref short iDay)
            {
                ShDate.jdn_civil(ShDate.persian_jdn(iYear, iMonth, iDay), ref iYear, ref iMonth, ref iDay);
            }

            // Token: 0x0600008D RID: 141 RVA: 0x00004D60 File Offset: 0x00002F60
            private static int Ceil(object objIn)
            {
                double value = Convert.ToDouble(objIn);
                return -Math.Sign(value) * ShDate.Int_(-Math.Abs(value));
            }

            // Token: 0x0600008E RID: 142 RVA: 0x00004D90 File Offset: 0x00002F90
            private static int Int_(object objIn)
            {
                double num = Convert.ToDouble(objIn);
                if (num >= 0.0)
                {
                    return (int)num;
                }
                if (ShDate.Frac(num) != 0.0)
                {
                    return (int)num - 1;
                }
                return (int)num;
            }

            // Token: 0x0600008F RID: 143 RVA: 0x00004DD0 File Offset: 0x00002FD0
            private static double Frac(object objIn)
            {
                return Convert.ToDouble(objIn) - (double)Convert.ToInt32(objIn);
            }

            // Token: 0x06000090 RID: 144 RVA: 0x00004DE0 File Offset: 0x00002FE0
            private static int persian_jdn(short iYear, short iMonth, short iDay)
            {
                int num;
                if (iYear >= 0)
                {
                    num = (int)(iYear - 474);
                }
                else
                {
                    num = (int)(iYear - 473);
                }
                int num2 = 474 + num % 2820;
                int num3;
                if (iMonth <= 7)
                {
                    num3 = (Convert.ToInt32(iMonth) - 1) * 31;
                }
                else
                {
                    num3 = (Convert.ToInt32(iMonth) - 1) * 30 + 6;
                }
                return Convert.ToInt32(iDay) + num3 + (num2 * 682 - 110) / 2816 + (num2 - 1) * 365 + num / 2820 * 1029983 + 1948320;
            }

            // Token: 0x06000091 RID: 145 RVA: 0x00004E68 File Offset: 0x00003068
            private static void jdn_julian(ref int jdn, ref short iYear, ref short iMonth, ref short iDay)
            {
                int num = jdn + 1402;
                int num2 = (num - 1) / 1461;
                int num3 = num - 1461 * num2;
                int num4 = (num3 - 1) / 365 - num3 / 1461;
                int num5 = num3 - 365 * num4 + 30;
                num = 80 * num5 / 2447;
                iDay = (short)(num5 - 2447 * num / 80);
                num5 = num / 11;
                iMonth = (short)(num + 2 - 12 * num5);
                iYear = (short)(4 * num2 + num4 + num5 - 4716);
            }

            // Token: 0x06000092 RID: 146 RVA: 0x00004EF4 File Offset: 0x000030F4
            private static void jdn_civil(int jdn, ref short iYear, ref short iMonth, ref short iDay)
            {
                if (jdn > 2299160)
                {
                    int num = jdn + 68569;
                    int num2 = 4 * num / 146097;
                    num -= (146097 * num2 + 3) / 4;
                    int num3 = 4000 * (num + 1) / 1461001;
                    num = num - 1461 * num3 / 4 + 31;
                    int num4 = 80 * num / 2447;
                    iDay = (short)(num - 2447 * num4 / 80);
                    num = num4 / 11;
                    iMonth = (short)(num4 + 2 - 12 * num);
                    iYear = (short)(100 * (num2 - 49) + num3 + num);
                    return;
                }
                ShDate.jdn_julian(ref jdn, ref iYear, ref iMonth, ref iDay);
            }

            // Token: 0x06000093 RID: 147 RVA: 0x00004F8C File Offset: 0x0000318C
            private static void jdn_persian(int jdn, ref short iYear, ref short iMonth, ref short iDay)
            {
                object expr_0C = jdn - 2121446;
                object value = Convert.ToInt32(expr_0C) / 1029983;
                object obj = Convert.ToInt32(expr_0C) % 1029983;
                object value2;
                if (Convert.ToInt32(obj) == 1029982)
                {
                    value2 = 2820;
                }
                else
                {
                    double num = (double)((int)obj / 366);
                    double num2 = (double)((int)obj % 366);
                    value2 = (double)((int)((2134.0 * num + 2816.0 * num2 + 2815.0) / 1028522.0)) + num + 1.0;
                }
                iYear = (short)(Convert.ToInt32(value2) + 2820 * Convert.ToInt32(value) + 474);
                if (iYear <= 0)
                {
                    iYear -= 1;
                }
                object obj2 = jdn - ShDate.persian_jdn(iYear, 1, 1) + 1;
                if ((int)obj2 <= 186)
                {
                    iMonth = (short)ShDate.Ceil(Convert.ToDouble(obj2) / 31.0);
                }
                else
                {
                    iMonth = (short)ShDate.Ceil((Convert.ToDouble(obj2) - 6.0) / 30.0);
                }
                iDay = (short)(jdn - ShDate.persian_jdn(iYear, iMonth, 1) + 1);
            }

            // Token: 0x06000094 RID: 148 RVA: 0x000050E4 File Offset: 0x000032E4
            private static int julian_jdn(ref short iYear, ref short iMonth, ref short iDay)
            {
                int num = Convert.ToInt32(iYear);
                int num2 = Convert.ToInt32(iMonth);
                int num3 = Convert.ToInt32(iDay);
                return 367 * num - 7 * (num + 5001 + (num2 - 9) / 7) / 4 + 275 * num2 / 9 + num3 + 1729777;
            }

            // Token: 0x06000095 RID: 149 RVA: 0x00005138 File Offset: 0x00003338
            private static int civil_jdn(short iYear, short iMonth, short iDay, short CalendarType)
            {
                int result;
                if (CalendarType == 1 & (iYear > 1582 | (iYear == 1582 & iMonth > 10) | (iYear == 1582 & iMonth == 10 & iDay > 14)))
                {
                    int num = Convert.ToInt32(iYear);
                    int num2 = Convert.ToInt32(iMonth);
                    int num3 = Convert.ToInt32(iDay);
                    result = 1461 * (num + 4800 + (num2 - 14) / 12) / 4 + 367 * (num2 - 2 - 12 * ((num2 - 14) / 12)) / 12 - 3 * ((num + 4900 + (num2 - 14) / 12) / 100) / 4 + num3 - 32075;
                }
                else
                {
                    result = ShDate.julian_jdn(ref iYear, ref iMonth, ref iDay);
                }
                return result;
            }

            // Token: 0x06000096 RID: 150 RVA: 0x000051EB File Offset: 0x000033EB
            public static string CurrentDateStr()
            {
                return ShDate.NowWithCentury();
            }

            // Token: 0x06000097 RID: 151 RVA: 0x000051F2 File Offset: 0x000033F2
            public static int CurrentDateInt()
            {
                return Convert.ToInt32(ShDate.RemoveDateSeparator(ShDate.CurrentDateStr()));
            }

            // Token: 0x06000098 RID: 152 RVA: 0x00005204 File Offset: 0x00003404
            public static string CurrentTime()
            {
                return ShDate.TehranDateTime.Hour.ToString("00") + ShDate.TehranDateTime.Minute.ToString("00");
            }

            // Token: 0x06000099 RID: 153 RVA: 0x0000524C File Offset: 0x0000344C
            public static string CurrentTimeWithSeparator()
            {
                return ShDate.TehranDateTime.Hour.ToString("00") + ":" + ShDate.TehranDateTime.Minute.ToString("00");
            }

            // Token: 0x0600009A RID: 154 RVA: 0x00005297 File Offset: 0x00003497
            public static short CurrentYear()
            {
                return ShDate.YearOf(ShDate.TehranDateTime);
            }

            // Token: 0x0600009B RID: 155 RVA: 0x000052A3 File Offset: 0x000034A3
            public static short CurrentMonth()
            {
                return ShDate.MonthOf(ShDate.TehranDateTime);
            }

            // Token: 0x0600009C RID: 156 RVA: 0x000052AF File Offset: 0x000034AF
            public static short CurrentDay()
            {
                return ShDate.DayOf(ShDate.TehranDateTime);
            }

            // Token: 0x0600009D RID: 157 RVA: 0x000052BB File Offset: 0x000034BB
            public static string CurrentDayName()
            {
                return ShDate.LongDayNames[(int)(ShDate.CurrentDayOfWeek() - 1)];
            }

            // Token: 0x0600009E RID: 158 RVA: 0x000052CA File Offset: 0x000034CA
            public static string CurrentMonthName()
            {
                return ShDate.LongMonthNames[(int)(ShDate.CurrentMonth() - 1)];
            }

            // Token: 0x0600009F RID: 159 RVA: 0x000052D9 File Offset: 0x000034D9
            public static string CurrentDayNameInMonth()
            {
                return ShDate.DayStrNum[(int)(ShDate.CurrentDay() - 1)];
            }

            // Token: 0x060000A0 RID: 160 RVA: 0x000052E8 File Offset: 0x000034E8
            public static short CurrentDayOfWeek()
            {
                return ShDate.DayOfWeek(ShDate.TehranDateTime);
            }

            // Token: 0x060000A1 RID: 161 RVA: 0x000052F4 File Offset: 0x000034F4
            public static string Now()
            {
                return ShDate.DateToStr(ShDate.TehranDateTime).Substring(2, 8);
            }

            // Token: 0x060000A2 RID: 162 RVA: 0x00005307 File Offset: 0x00003507
            public static string NowWithCentury()
            {
                return ShDate.DateToStr(ShDate.TehranDateTime);
            }

            // Token: 0x060000A3 RID: 163 RVA: 0x00005313 File Offset: 0x00003513
            public static string CurrentCompleteDateName()
            {
                return ShDate.CompleteDateName(ShDate.TehranDateTime);
            }

            // Token: 0x060000A4 RID: 164 RVA: 0x00005320 File Offset: 0x00003520
            public static DateTime EncodeDate(short ShYear, short ShMonth, short ShDay)
            {
                short year = ShYear;
                short month = ShMonth;
                short day = ShDay;
                ShDate.persian_civil(ref year, ref month, ref day);
                return new DateTime((int)year, (int)month, (int)day);
            }

            // Token: 0x060000A5 RID: 165 RVA: 0x00005346 File Offset: 0x00003546
            public static void DecodeDate(DateTime VDateTime, ref short shYear, ref short shMonth, ref short shDay)
            {
                shYear = (short)VDateTime.Year;
                shMonth = (short)VDateTime.Month;
                shDay = (short)VDateTime.Day;
                ShDate.civil_persian(ref shYear, ref shMonth, ref shDay);
            }

            // Token: 0x060000A6 RID: 166 RVA: 0x00005370 File Offset: 0x00003570
            public static void DecodeDate(string ShamsiDate, ref short shYear, ref short shMonth, ref short shDay)
            {
                string[] array = ShamsiDate.Split(ShDate.DateSeparator.ToCharArray());
                shYear = Convert.ToInt16(array[0]);
                shMonth = Convert.ToInt16(array[1]);
                shDay = Convert.ToInt16(array[2]);
            }

            // Token: 0x060000A7 RID: 167 RVA: 0x000053AC File Offset: 0x000035AC
            public static string DateToStr(DateTime VDateTime)
            {
                short num = 0;
                short num2 = 0;
                short num3 = 0;
                ShDate.DecodeDate(VDateTime, ref num, ref num2, ref num3);
                return ShDate.FormatDate(num, num2, num3);
            }

            // Token: 0x060000A8 RID: 168 RVA: 0x000053E2 File Offset: 0x000035E2
            public static string DateToStr(DateTime VDateTime, bool HaveSlash)
            {
                if (HaveSlash)
                {
                    return ShDate.DateToStr(VDateTime);
                }
                return ShDate.RemoveDateSeparator(ShDate.DateToStr(VDateTime));
            }

            // Token: 0x060000A9 RID: 169 RVA: 0x000053F9 File Offset: 0x000035F9
            public static int DateToInt(DateTime VDateTime)
            {
                return Convert.ToInt32(ShDate.DateToStr(VDateTime, false));
            }

            // Token: 0x060000AA RID: 170 RVA: 0x00005408 File Offset: 0x00003608
            public static DateTime StrToDate(string VDateTimeStr)
            {
                string text = VDateTimeStr.Trim();
                if (text.Split(ShDate.DateSeparator.ToCharArray()).Length == 3)
                {
                    if (text.Length != 8 && text.Length != 10)
                    {
                        throw new SystemException("فرمت تاريخ مورد نظر معتبر نيست ! ");
                    }
                    if (text.Length == 8)
                    {
                        text = "13" + text;
                    }
                }
                else
                {
                    if (text.Length != 6 && text.Length != 8)
                    {
                        throw new SystemException("فرمت تاريخ مورد نظر معتبر نيست ! ");
                    }
                    if (text.Length == 6)
                    {
                        text = "13" + text;
                    }
                    text = ShDate.FormatDate(text);
                }
                short shYear = 0;
                short shMonth = 0;
                short shDay = 0;
                ShDate.DecodeDate(text, ref shYear, ref shMonth, ref shDay);
                return ShDate.EncodeDate(shYear, shMonth, shDay);
            }

            // Token: 0x060000AB RID: 171 RVA: 0x000054B9 File Offset: 0x000036B9
            public static DateTime ToDateTime(string VDateTimeStr)
            {
                return ShDate.StrToDate(VDateTimeStr);
            }

            // Token: 0x060000AC RID: 172 RVA: 0x000054C1 File Offset: 0x000036C1
            public static DateTime ToDateTime(int VDateTimeInt)
            {
                return ShDate.ToDateTime(VDateTimeInt.ToString());
            }

            // Token: 0x060000AD RID: 173 RVA: 0x000054CF File Offset: 0x000036CF
            public static DateTime ToDateTime(short Year, short Month, short Day)
            {
                return ShDate.EncodeDate(Year, Month, Day);
            }

            // Token: 0x060000AE RID: 174 RVA: 0x000054DC File Offset: 0x000036DC
            public static string CompleteDateName(DateTime VDateTime)
            {
                short num = 0;
                short num2 = 0;
                short num3 = 0;
                ShDate.DecodeDate(VDateTime, ref num, ref num2, ref num3);
                string text = num.ToString();
                return string.Concat(new string[]
                {
                ShDate.DayName(ShDate.DayOfWeek(VDateTime)),
                " ",
                ShDate.DayStrNum[(int)(num3 - 1)],
                " ",
                ShDate.LongMonthNames[(int)(num2 - 1)],
                " ماه  سال ",
                text
                });
            }

            // Token: 0x060000AF RID: 175 RVA: 0x00005550 File Offset: 0x00003750
            public static bool IsDateValid(string TarikhShamsi)
            {
                bool result;
                try
                {
                    string text = TarikhShamsi.Split(ShDate.DateSeparator.ToCharArray())[0].Trim();
                    string text2 = TarikhShamsi.Split(ShDate.DateSeparator.ToCharArray())[1].Trim();
                    string text3 = TarikhShamsi.Split(ShDate.DateSeparator.ToCharArray())[2].Trim();
                    short num;
                    short num2;
                    short shYear;
                    if (text.Length < 4 || text2.Length < 2 || text3.Length < 2)
                    {
                        result = false;
                    }
                    else if (!short.TryParse(text2, out num))
                    {
                        result = false;
                    }
                    else if (num < 1 || num > 12)
                    {
                        result = false;
                    }
                    else if (!short.TryParse(text3, out num2))
                    {
                        result = false;
                    }
                    else if (num2 < 1 || num2 > 31)
                    {
                        result = false;
                    }
                    else if (!short.TryParse(text, out shYear))
                    {
                        result = false;
                    }
                    else if (!ShDate.IsLeapYear(shYear) && num >= 30)
                    {
                        result = false;
                    }
                    else
                    {
                        result = true;
                    }
                }
                catch
                {
                    result = false;
                }
                return result;
            }

            // Token: 0x060000B0 RID: 176 RVA: 0x00005644 File Offset: 0x00003844
            public static bool IsTateel(short Month, short Day)
            {
                bool result = false;
                if (Month == 1 && (Day == 1 || Day == 2 || Day == 3 || Day == 4 || Day == 12 || Day == 13))
                {
                    result = true;
                }
                if (Month == 3 && (Day == 14 || Day == 15))
                {
                    result = true;
                }
                if (Month == 11 && Day == 22)
                {
                    result = true;
                }
                if (Month == 12 && Day == 29)
                {
                    result = true;
                }
                return result;
            }

            // Token: 0x060000B1 RID: 177 RVA: 0x0000569C File Offset: 0x0000389C
            public static bool IsTateel(DateTime VDateTime)
            {
                short num = 0;
                short month = 0;
                short day = 0;
                ShDate.DecodeDate(VDateTime, ref num, ref month, ref day);
                return ShDate.IsTateel(month, day);
            }

            // Token: 0x060000B2 RID: 178 RVA: 0x000056C2 File Offset: 0x000038C2
            public static bool IsWeekend(DateTime VDateTime)
            {
                return ShDate.DayOfWeek(VDateTime) == 7;
            }

            // Token: 0x060000B3 RID: 179 RVA: 0x000056D0 File Offset: 0x000038D0
            public static int DifDays(DateTime StartDate, DateTime EndDate)
            {
                return (StartDate - EndDate).Days;
            }

            // Token: 0x060000B4 RID: 180 RVA: 0x000056EC File Offset: 0x000038EC
            public static int DifDays(string StartDateSh, string EndDateSh)
            {
                return (ShDate.StrToDate(EndDateSh) - ShDate.StrToDate(StartDateSh)).Days;
            }

            // Token: 0x060000B5 RID: 181 RVA: 0x00005714 File Offset: 0x00003914
            public static bool IsLeapYear(short ShYear)
            {
                double num;
                if (ShYear > 0)
                {
                    num = (double)(((ShYear - 474) % 2820 + 474 + 38) * 682);
                }
                else
                {
                    num = (double)(((ShYear - 473) % 2820 + 474 + 38) * 682);
                }
                return num % 2816.0 < 682.0;
            }

            // Token: 0x060000B6 RID: 182 RVA: 0x0000577C File Offset: 0x0000397C
            public static short YearOf(DateTime VDateTime)
            {
                short result = 0;
                short num = 0;
                short num2 = 0;
                ShDate.DecodeDate(VDateTime, ref result, ref num, ref num2);
                return result;
            }

            // Token: 0x060000B7 RID: 183 RVA: 0x0000579C File Offset: 0x0000399C
            public static DateTime FirstDayOfYear(DateTime VDateTime)
            {
                return ShDate.EncodeDate(ShDate.YearOf(VDateTime), 1, 1);
            }

            // Token: 0x060000B8 RID: 184 RVA: 0x000057AB File Offset: 0x000039AB
            public static short DaysInYear(DateTime VDateTime)
            {
                return (short) (ShDate.IsLeapYear(ShDate.YearOf(VDateTime)) ? 366 : 365);
            }

            // Token: 0x060000B9 RID: 185 RVA: 0x000057C8 File Offset: 0x000039C8
            public static short DaysPassedYear(DateTime VDateTime)
            {
                TimeSpan timeSpan = default(TimeSpan);
                return (short)(VDateTime - ShDate.FirstDayOfYear(VDateTime)).Days;
            }

            // Token: 0x060000BA RID: 186 RVA: 0x000057F2 File Offset: 0x000039F2
            public static short DaysRemainedYear(DateTime VDateTime)
            {
                return (short) (ShDate.DaysInYear(VDateTime) - 1 - ShDate.DaysPassedYear(VDateTime));
            }

            // Token: 0x060000BB RID: 187 RVA: 0x00005804 File Offset: 0x00003A04
            public static string AddYears(string ShamsiDate, int value)
            {
                string[] expr_10 = ShamsiDate.Split("/".ToCharArray());
                int num = Convert.ToInt32(expr_10[0]);
                int num2 = Convert.ToInt32(expr_10[1]);
                int num3 = Convert.ToInt32(expr_10[2]);
                bool arg_3B_0 = num2 == 12 && num3 == 30;
                num += value;
                if (arg_3B_0 && !ShDate.IsLeapYear((short)num))
                {
                    num3 = 29;
                }
                return string.Concat(new string[]
                {
                num.ToString("0000"),
                "/",
                num2.ToString("00"),
                "/",
                num3.ToString("00")
                });
            }

            // Token: 0x060000BC RID: 188 RVA: 0x000058A4 File Offset: 0x00003AA4
            public static short MonthOf(DateTime VDateTime)
            {
                short num = 0;
                short result = 0;
                short num2 = 0;
                ShDate.DecodeDate(VDateTime, ref num, ref result, ref num2);
                return result;
            }

            // Token: 0x060000BD RID: 189 RVA: 0x000058C4 File Offset: 0x00003AC4
            public static string AddMonths(string ShamsiDate, int NumberOfMonths)
            {
                return ShDate.DateToStr(ShDate.IncMonthA(ShDate.StrToDate(ShamsiDate), NumberOfMonths));
            }

            // Token: 0x060000BE RID: 190 RVA: 0x000058D8 File Offset: 0x00003AD8
            public static DateTime IncMonthA(DateTime VDateTime, int NumberOfMonths)
            {
                short shYear = 0;
                short shMonth = 0;
                short shDay = 0;
                ShDate.DecodeDate(VDateTime, ref shYear, ref shMonth, ref shDay);
                ShDate.IncMonthB(ref shYear, ref shMonth, ref shDay, NumberOfMonths);
                return ShDate.EncodeDate(shYear, shMonth, shDay);
            }

            // Token: 0x060000BF RID: 191 RVA: 0x0000590C File Offset: 0x00003B0C
            public static DateTime FirstDayOfMoon(DateTime VDateTime)
            {
                short shYear = 0;
                short shMonth = 0;
                short num = 0;
                ShDate.DecodeDate(VDateTime, ref shYear, ref shMonth, ref num);
                return ShDate.EncodeDate(shYear, shMonth, 1);
            }

            // Token: 0x060000C0 RID: 192 RVA: 0x00005933 File Offset: 0x00003B33
            public static string MonthName(short Month)
            {
                return ShDate.LongMonthNames[(int)(Month - 1)];
            }

            // Token: 0x060000C1 RID: 193 RVA: 0x00005940 File Offset: 0x00003B40
            public static DateTime LastDayOfMoon(DateTime VDateTime)
            {
                short year = 0;
                short month = 0;
                short num = 0;
                ShDate.DecodeDate(VDateTime, ref year, ref month, ref num);
                return ShDate.LastDayOfMoon(year, month);
            }

            // Token: 0x060000C2 RID: 194 RVA: 0x00005968 File Offset: 0x00003B68
            public static DateTime LastDayOfMoon(short Year, short Month)
            {
                short shDay;
                if (Month < 7)
                {
                    shDay = 31;
                }
                else if (Month > 6 && Month < 12)
                {
                    shDay = 30;
                }
                else
                {
                    shDay = (short) (ShDate.IsLeapYear(Year) ? 30 : 29);
                }
                return ShDate.EncodeDate(Year, Month, shDay);
            }

            // Token: 0x060000C3 RID: 195 RVA: 0x000059A8 File Offset: 0x00003BA8
            public static byte MonthDays(string ShamsiDateStr)
            {
                string[] expr_12 = ShamsiDateStr.Split("/".ToCharArray());
                short shYear = Convert.ToInt16(expr_12[0]);
                short num = Convert.ToInt16(expr_12[1]);
                byte result;
                if (num <= 6 && num >= 1)
                {
                    result = 31;
                }
                else if (num >= 7 && num <= 11)
                {
                    result = 30;
                }
                else if (ShDate.IsLeapYear(shYear))
                {
                    result = 30;
                }
                else
                {
                    result = 29;
                }
                return result;
            }

            // Token: 0x060000C4 RID: 196 RVA: 0x00005A04 File Offset: 0x00003C04
            public static byte MonthDays(DateTime VDateTime)
            {
                return ShDate.MonthDays(ShDate.DateToStr(VDateTime));
            }

            // Token: 0x060000C5 RID: 197 RVA: 0x00005A14 File Offset: 0x00003C14
            public static int DateToWeekNo(DateTime VDateTime)
            {
                DateTime dateTime = ShDate.FirstDayOfYear(VDateTime);
                TimeSpan ts = TimeSpan.FromDays((double)(ShDate.DaysRemainedWeek(dateTime) + 1));
                TimeSpan timeSpan = (VDateTime - dateTime).Add(TimeSpan.FromDays(1.0));
                if (timeSpan.Days > ts.Days)
                {
                    return (int)Math.Floor((double)(timeSpan.Subtract(ts).Subtract(TimeSpan.FromDays(1.0)).Days / 7)) + 2;
                }
                return (int)Math.Floor((double)(timeSpan.Days / 7)) + 1;
            }

            // Token: 0x060000C6 RID: 198 RVA: 0x00005AA9 File Offset: 0x00003CA9
            public static int DateToWeekNo()
            {
                return ShDate.DateToWeekNo(ShDate.TehranDateTime);
            }

            // Token: 0x060000C7 RID: 199 RVA: 0x00005AB5 File Offset: 0x00003CB5
            public static short DayOfWeek(DateTime VDateTime)
            {
                return (new byte[]
                {
                2,
                3,
                4,
                5,
                6,
                7,
                1
                })[(int)((byte)VDateTime.DayOfWeek)];
            }

            // Token: 0x060000C8 RID: 200 RVA: 0x00005AD4 File Offset: 0x00003CD4
            public static DateTime FirstDayOfWeek(DateTime VDateTime)
            {
                TimeSpan value = new TimeSpan((int)(ShDate.DayOfWeek(VDateTime) - 1), 0, 0, 0, 0);
                return VDateTime.Subtract(value);
            }

            // Token: 0x060000C9 RID: 201 RVA: 0x00005AFC File Offset: 0x00003CFC
            public static void WeekNoToRangeDate(short ShamsiWeekNo, short Year, ref string StrStartDate, ref string StrEndDate)
            {
                DateTime vDateTime = default(DateTime);
                vDateTime = ShDate.EncodeDate(Year, 1, 1);
                int num = (int)ShDate.DayOfWeek(vDateTime);
                DateTime vDateTime2 = default(DateTime);
                DateTime vDateTime3 = default(DateTime);
                if (ShamsiWeekNo == 1)
                {
                    vDateTime2 = vDateTime.AddDays((double)(-(double)num + 1));
                }
                else
                {
                    vDateTime2 = vDateTime.AddDays((double)((int)((ShamsiWeekNo - 2) * 7) + (7 - (num - 1))));
                }
                vDateTime3 = vDateTime2.AddDays(6.0);
                StrStartDate = ShDate.DateToStr(vDateTime2);
                StrEndDate = ShDate.DateToStr(vDateTime3);
            }

            // Token: 0x060000CA RID: 202 RVA: 0x00005B79 File Offset: 0x00003D79
            public static short DaysRemainedWeek(DateTime VDateTime)
            {
                return (short) (7 - ShDate.DayOfWeek(VDateTime));
            }

            // Token: 0x060000CB RID: 203 RVA: 0x00005B84 File Offset: 0x00003D84
            public static DateTime LastDayOfWeek(DateTime VDateTime)
            {
                TimeSpan value = new TimeSpan((int)ShDate.DayOfWeek(VDateTime), 0, 0, 0, 0);
                return VDateTime.AddDays(7.0).Subtract(value);
            }

            // Token: 0x060000CC RID: 204 RVA: 0x00005BBC File Offset: 0x00003DBC
            public static string AddDays(string SourceShamsiDate, double AddedDays)
            {
                DateTime vDateTime = ShDate.StrToDate(SourceShamsiDate).AddDays(AddedDays);
                return ShDate.DateToStr(vDateTime);
            }

            // Token: 0x060000CD RID: 205 RVA: 0x00005BDF File Offset: 0x00003DDF
            public static int AddDays(int SourceShamsiDate, double AddedDays)
            {
                return Convert.ToInt32(ShDate.AddDays(SourceShamsiDate.ToString("####/##/##"), AddedDays).Replace("/", ""));
            }

            // Token: 0x060000CE RID: 206 RVA: 0x00005C08 File Offset: 0x00003E08
            public static short DayOf(DateTime VDateTime)
            {
                short num = 0;
                short num2 = 0;
                short result = 0;
                ShDate.DecodeDate(VDateTime, ref num, ref num2, ref result);
                return result;
            }

            // Token: 0x060000CF RID: 207 RVA: 0x00005C28 File Offset: 0x00003E28
            public static string DayName(short Day)
            {
                if ((int)(Day - 1) > ShDate.LongDayNames.Length || Day - 1 < 0)
                {
                    return "؟";
                }
                return ShDate.LongDayNames[(int)(Day - 1)];
            }

            // Token: 0x060000D0 RID: 208 RVA: 0x00005C4B File Offset: 0x00003E4B
            public static DateTime FirstDayOfCalendar(DateTime VDateTime)
            {
                return ShDate.FirstDayOfWeek(ShDate.FirstDayOfMoon(VDateTime));
            }

            // Token: 0x060000D1 RID: 209 RVA: 0x00005C58 File Offset: 0x00003E58
            public static DateTime LastDayOfCalendar(DateTime VDateTime)
            {
                return ShDate.LastDayOfWeek(ShDate.LastDayOfMoon(VDateTime));
            }

            // Token: 0x060000D2 RID: 210 RVA: 0x00005C68 File Offset: 0x00003E68
            //public static CalendarDay[] GetCalendarDays(short Year, short Month)
            //{
            //    DateTime expr_08 = ShDate.EncodeDate(Year, Month, 1);
            //    DateTime dateTime = ShDate.FirstDayOfCalendar(expr_08);
            //    ShDate.LastDayOfCalendar(expr_08);
            //    DateTime dateTime2 = dateTime;
            //    IList list = new ArrayList();
            //    for (int i = 1; i <= 42; i++)
            //    {
            //        short num = ShDate.MonthOf(dateTime2);
            //        bool isOtherMonth = Month != num;
            //        CalendarDay value = new CalendarDay(dateTime2, isOtherMonth);
            //        list.Add(value);
            //        dateTime2 = dateTime2.AddDays(1.0);
            //    }
            //    CalendarDay[] array = new CalendarDay[list.Count];
            //    list.CopyTo(array, 0);
            //    return array;
            //}

            // Token: 0x060000D3 RID: 211 RVA: 0x00005CF4 File Offset: 0x00003EF4
            public static string FormatDate(object NumberDate)
            {
                string text = Convert.ToString(NumberDate);
                string result;
                if (text.Length == 8)
                {
                    result = string.Concat(new string[]
                    {
                    text.Substring(0, 4),
                    ShDate.DateSeparator,
                    text.Substring(4, 2),
                    ShDate.DateSeparator,
                    text.Substring(6, 2)
                    });
                }
                else if (text.Length == 6)
                {
                    result = string.Concat(new string[]
                    {
                    text.Substring(0, 2),
                    ShDate.DateSeparator,
                    text.Substring(2, 2),
                    ShDate.DateSeparator,
                    text.Substring(4, 2)
                    });
                }
                else
                {
                    result = text;
                }
                return result;
            }

            // Token: 0x060000D4 RID: 212 RVA: 0x00005DA4 File Offset: 0x00003FA4
            public static string FormatDate(string TarikhShamsi)
            {
                string text = ShDate.RemoveDateSeparator(TarikhShamsi);
                if (TarikhShamsi.Length == 8)
                {
                    text = text.Insert(4, ShDate.DateSeparator);
                    return text.Insert(7, ShDate.DateSeparator);
                }
                if (TarikhShamsi.Length == 6)
                {
                    text = text.Insert(2, ShDate.DateSeparator);
                    return text.Insert(5, ShDate.DateSeparator);
                }
                throw new Exception("تاريخ با فرمت 6 و 8 رقمي مجاز ميباشد");
            }

            // Token: 0x060000D5 RID: 213 RVA: 0x00005E0C File Offset: 0x0000400C
            public static string FormatDate(int TarikhShamsi)
            {
                int length = TarikhShamsi.ToString().Length;
                if (length == 8)
                {
                    return TarikhShamsi.ToString("####/##/##");
                }
                if (length == 6)
                {
                    return TarikhShamsi.ToString("##/##/##");
                }
                throw new Exception("تاریخ جهت فرمت کردن معتبر نیست فقط 6 یا 8 رقمی مجاز می باشد");
            }

            // Token: 0x060000D6 RID: 214 RVA: 0x00005E54 File Offset: 0x00004054
            public static string FormatDate(object Year, object Month, object Day)
            {
                string text = Convert.ToString(Year);
                string text2 = Convert.ToString(Month);
                string text3 = Convert.ToString(Day);
                if (text.Length == 1)
                {
                    text = "0" + text;
                }
                if (text2.Length == 1)
                {
                    text2 = "0" + text2;
                }
                if (text3.Length == 1)
                {
                    text3 = "0" + text3;
                }
                return string.Concat(new string[]
                {
                text,
                ShDate.DateSeparator,
                text2,
                ShDate.DateSeparator,
                text3
                });
            }

            // Token: 0x060000D7 RID: 215 RVA: 0x00005EDC File Offset: 0x000040DC
            public static string RemoveDateSeparator(DateTime VDateTime)
            {
                return ShDate.DateToStr(VDateTime).Replace(ShDate.DateSeparator, "");
            }

            // Token: 0x060000D8 RID: 216 RVA: 0x00005EF3 File Offset: 0x000040F3
            public static string RemoveDateSeparator(string TarikhShamsi)
            {
                return TarikhShamsi.Replace(ShDate.DateSeparator, "");
            }

            // Token: 0x060000D9 RID: 217 RVA: 0x00005F08 File Offset: 0x00004108
            public static string HolidayReason(short Month, short Day)
            {
                string result = "";
                if (Month == 1 && (Day == 1 || Day == 2 || Day == 3 || Day == 4 || Day == 5))
                {
                    result = "عید نوروز";
                }
                else if (Month == 1 && Day == 12)
                {
                    result = "روز جمهوری اسلامی";
                }
                else if (Month == 1 && Day == 13)
                {
                    result = "روز طبیعت";
                }
                else if (Month == 3 && Day == 14)
                {
                    result = "رحلت امام خمینی";
                }
                else if (Month == 3 && Day == 15)
                {
                    result = "قیام پانزدهم خرداد";
                }
                else if (Month == 11 && Day == 22)
                {
                    result = "پیروزی انقلاب اسلامی ایران";
                }
                else if (Month == 12 && Day == 29)
                {
                    result = "روز ملی شدن صنعت نفت ایران";
                }
                return result;
            }

            // Token: 0x060000DA RID: 218 RVA: 0x00005FA4 File Offset: 0x000041A4
            public static string HolidayReason(DateTime VDateTime)
            {
                short num = 0;
                short month = 0;
                short day = 0;
                ShDate.DecodeDate(VDateTime, ref num, ref month, ref day);
                return ShDate.HolidayReason(month, day);
            }

            // Token: 0x1700001B RID: 27
            // (get) Token: 0x060000DB RID: 219 RVA: 0x00005FCC File Offset: 0x000041CC
            public static DateTime TehranDateTime
            {
                get
                {
                    DateTime now = DateTime.Now;
                    TimeSpan utcOffset = TimeZone.CurrentTimeZone.GetUtcOffset(now);
                    TimeSpan t = ShDate.GetTehranDifferenceTime() - utcOffset;
                    return now + t;
                }
            }

            // Token: 0x060000DC RID: 220 RVA: 0x00005FFE File Offset: 0x000041FE
            public static TimeSpan GetTehranDifferenceTime()
            {
                return new TimeSpan(3, 30, 0);
            }

            // Token: 0x060000DD RID: 221 RVA: 0x0000600C File Offset: 0x0000420C
            public static int TimeSub(int Time1, int Time2)
            {
                if (Time1 < Time2)
                {
                    if (Time1 == 0)
                    {
                        int arg_0B_0 = Time1;
                        Time1 = Time2;
                        Time2 = arg_0B_0;
                    }
                    else if (Time2 != 0)
                    {
                        Time1 += 2400;
                    }
                }
                int num = Time1 % 100;
                int num2 = Time2 % 100;
                int time;
                if (num < num2)
                {
                    time = num + 60 - num2;
                    Time2 += 100;
                }
                else
                {
                    time = num - num2;
                }
                int arg_46_0 = Time1 / 100;
                int num3 = Time2 / 100;
                return ShDate.TimePlus((arg_46_0 - num3) * 100, time);
            }

            // Token: 0x060000DE RID: 222 RVA: 0x0000606C File Offset: 0x0000426C
            public static int TimePlus(int Time1, int Time2)
            {
                int num = 0;
                int num2 = Time1 % 100 + Time2 % 100;
                int num3;
                if (Time1 == 0)
                {
                    num3 = Time2;
                }
                else if (Time2 == 0)
                {
                    num3 = Time1;
                }
                else
                {
                    num3 = (Convert.ToInt32(Time1 / 100) + Convert.ToInt32(Time2 / 100) + Convert.ToInt32(num2 / 60)) * 100 + num2 % 60;
                }
                int num4 = num3;
                if (num4 < 0)
                {
                    num = num4;
                    num4 *= -1;
                }
                if (Time1 < 0 && Time2 > 0 && num4 % 100 > 0)
                {
                    Time1 *= -1;
                    if (Time1 > Time2)
                    {
                        num3 = ShDate.TimeSub(Time1, Time2);
                    }
                    else
                    {
                        num3 = ShDate.TimeSub(Time2, Time1);
                    }
                    if (num < 0)
                    {
                        num3 *= -1;
                    }
                }
                else if (Time1 > 0 && Time2 < 0 && num4 % 100 > 0)
                {
                    Time2 *= -1;
                    if (Time1 > Time2)
                    {
                        num3 = ShDate.TimeSub(Time1, Time2);
                    }
                    else
                    {
                        num3 = ShDate.TimeSub(Time2, Time1);
                    }
                    if (num < 0)
                    {
                        num3 *= -1;
                    }
                }
                return num3;
            }

            // Token: 0x060000DF RID: 223 RVA: 0x0000612C File Offset: 0x0000432C
            public static int Time2Min(int Time1)
            {
                int result;
                try
                {
                    result = Time1 / 100 * 60 + Time1 % 100;
                }
                catch
                {
                    result = -1;
                }
                return result;
            }

            // Token: 0x060000E0 RID: 224 RVA: 0x00006160 File Offset: 0x00004360
            public static string Min2Time(int Min)
            {
                string result;
                try
                {
                    result = TimeSpan.FromMinutes((double)Min).ToString();
                }
                catch
                {
                    result = "";
                }
                return result;
            }

            // Token: 0x060000E1 RID: 225 RVA: 0x000061A0 File Offset: 0x000043A0
            public static string StringOfChar(string cchar, int Len_)
            {
                string text = "";
                for (int i = 1; i >= Len_; i++)
                {
                    text += cchar;
                }
                return text;
            }

            // Token: 0x060000E2 RID: 226 RVA: 0x000061C8 File Offset: 0x000043C8
            public static string AddZeroToString(string MString, int Len_, bool ToLeft)
            {
                string text;
                if (Len_ - MString.Length > 0)
                {
                    text = ShDate.StringOfChar("0", Len_ - MString.Length);
                }
                else
                {
                    text = "";
                }
                if (ToLeft)
                {
                    text += MString;
                }
                else
                {
                    text = MString + text;
                }
                return text;
            }

            // Token: 0x060000E3 RID: 227 RVA: 0x00006211 File Offset: 0x00004411
            public static List<DateTime> GetCurrentStartAndEndPeriod(int day)
            {
                return ShDate.GetStartAndEndPeriod((int)ShDate.CurrentYear(), (int)ShDate.CurrentMonth(), day);
            }

            // Token: 0x060000E4 RID: 228 RVA: 0x00006224 File Offset: 0x00004424
            public static List<DateTime> GetStartAndEndPeriod(int persianYear, int persianMonth, int persianDay)
            {
                DateTime item = ShDate.StrToDate(string.Format("{0}/{1}/{2}", persianYear, persianMonth.ToString().PadLeft(2, '0'), persianDay.ToString().PadLeft(2, '0')));
                if (persianDay > 1)
                {
                    item = item.AddMonths(-1);
                }
                DateTime dateTime = item.AddMonths(1);
                return new List<DateTime>
            {
                item,
                dateTime.AddDays(-1.0)
            };
            }

            // Token: 0x0400002B RID: 43
            private const short Gregorian = 1;

            // Token: 0x0400002C RID: 44
            public static string[] ShortMonthNames = new string[]
            {
            "فروردين",
            "ارديبهشت",
            "خرداد",
            "تير",
            "مرداد",
            "شهريور",
            "مهر",
            "آبان",
            "آذر",
            "دي",
            "بهمن",
            "اسفند"
            };

            // Token: 0x0400002D RID: 45
            public static string[] LongMonthNames = new string[]
            {
            "فروردين",
            "ارديبهشت",
            "خرداد",
            "تير",
            "مرداد",
            "شهريور",
            "مهر",
            "آبان",
            "آذر",
            "دي",
            "بهمن",
            "اسفند"
            };

            // Token: 0x0400002E RID: 46
            public static string[] ShortDayNames = new string[]
            {
            "شنبه",
            "يکشنبه",
            "دوشنبه",
            "سه شنبه",
            "چهارشنبه",
            "پنجشنبه",
            "جمعه"
            };

            // Token: 0x0400002F RID: 47
            public static string[] LongDayNames = new string[]
            {
            "شنبه",
            "يکشنبه",
            "دوشنبه",
            "سه شنبه",
            "چهارشنبه",
            "پنجشنبه",
            "جمعه"
            };

            // Token: 0x04000030 RID: 48
            public static string[] DayStrNum = new string[]
            {
            "يکم",
            "دوم",
            "سوم",
            "چهارم",
            "پنجم",
            "ششم",
            "هفتم",
            "هشتم",
            "نهم",
            "دهم",
            "يازدهم",
            "دوازدهم",
            "سيزدهم",
            "چهاردهم",
            "پانزدهم",
            "شانزدهم",
            "هفدهم",
            "هجدهم",
            "نوزدهم",
            "بيستم",
            "بيست و يکم",
            "بيست و دوم",
            "بيست و سوم",
            "بيست و چهارم",
            "بيست و پنجم",
            "بيست و ششم",
            "بيست و هفتم",
            "بيست و هشتم",
            "بيست و نهم",
            "سي ام",
            "سي و يکم"
            };

            // Token: 0x04000031 RID: 49
            public static object[] MonthDaysArray = new object[]
            {
            new object[]
            {
                31,
                31,
                31,
                31,
                31,
                31,
                30,
                30,
                30,
                30,
                30,
                30
            },
            new object[]
            {
                31,
                31,
                31,
                31,
                31,
                31,
                30,
                30,
                30,
                30,
                30,
                29
            }
            };

            // Token: 0x04000032 RID: 50
            public static string DateSeparator = CultureInfo.CurrentCulture.DateTimeFormat.DateSeparator;

            // Token: 0x04000033 RID: 51
            public static PersianCalendar PCal;
        }
    }

