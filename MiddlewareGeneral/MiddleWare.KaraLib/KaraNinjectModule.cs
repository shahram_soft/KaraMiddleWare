﻿using Interface;
using Ninject.Modules;

namespace MiddleWare.KaraLib
{
  public  class KaraNinjectModule: NinjectModule
    {
        public override void Load()
        {
            Bind<IWebServiceMethods>().To<WebServiceMethods>();
           
        }
    }
}
