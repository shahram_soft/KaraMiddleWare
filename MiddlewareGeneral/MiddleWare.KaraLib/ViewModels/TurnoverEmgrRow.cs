﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiddleWare.KaraLib.ViewModels
{
   public class TurnoverEmgrRow : BaseRow
    {
        // Token: 0x06000244 RID: 580 RVA: 0x00002D48 File Offset: 0x00000F48
        public TurnoverEmgrRow()
        {
        }

        // Token: 0x06000245 RID: 581 RVA: 0x00004BFC File Offset: 0x00002DFC
        public TurnoverEmgrRow(SqlDataReader dr)
        {
            this.CurEmp_NO = Convert.ToDouble((dr["CurEmp_NO"] is DBNull || dr["CurEmp_NO"] == null) ? 0 : dr["CurEmp_NO"]);
            this.ManagerName = Convert.ToString(dr["ManagerName"]);
            this.InKartablCount = Convert.ToInt32((dr["InKartablCount"] is DBNull || dr["InKartablCount"] == null) ? 0 : dr["InKartablCount"]);
            this.AcceptCount = Convert.ToInt32((dr["AcceptCount"] is DBNull || dr["AcceptCount"] == null) ? 0 : dr["AcceptCount"]);
            this.RejectCount = Convert.ToInt32((dr["RejectCount"] is DBNull || dr["RejectCount"] == null) ? 0 : dr["RejectCount"]);
            this.InKartablCountRev = Convert.ToInt32((dr["InKartablCountRev"] is DBNull || dr["InKartablCountRev"] == null) ? 0 : dr["InKartablCountRev"]);
            this.AcceptCountRev = Convert.ToInt32((dr["AcceptCountRev"] is DBNull || dr["AcceptCountRev"] == null) ? 0 : dr["AcceptCountRev"]);
            this.RejectCountRev = Convert.ToInt32((dr["RejectCountRev"] is DBNull || dr["RejectCountRev"] == null) ? 0 : dr["RejectCountRev"]);
            this.MoveUpCount = Convert.ToInt32((dr["MoveUpCount"] is DBNull || dr["MoveUpCount"] == null) ? 0 : dr["MoveUpCount"]);
            this.TotalRequest = Convert.ToInt32((dr["TotalRequest"] is DBNull || dr["TotalRequest"] == null) ? 0 : dr["TotalRequest"]);
        }

        // Token: 0x06000246 RID: 582 RVA: 0x00004E4C File Offset: 0x0000304C
        public TurnoverEmgrRow(double curEmpNo, string managerName, int inKartablCount, int acceptCount, int rejectCount, int inKartablCountRev, int acceptCountRev, int rejectCountRev, int moveUpCount, int totalRequest)
        {
            this.CurEmp_NO = curEmpNo;
            this.ManagerName = managerName;
            this.InKartablCount = inKartablCount;
            this.AcceptCount = acceptCount;
            this.RejectCount = rejectCount;
            this.InKartablCountRev = inKartablCountRev;
            this.AcceptCountRev = acceptCountRev;
            this.RejectCountRev = rejectCountRev;
            this.MoveUpCount = moveUpCount;
            this.TotalRequest = totalRequest;
        }

        // Token: 0x17000111 RID: 273
        // (get) Token: 0x06000247 RID: 583 RVA: 0x00004EAC File Offset: 0x000030AC
        // (set) Token: 0x06000248 RID: 584 RVA: 0x00004EB4 File Offset: 0x000030B4
        public double CurEmp_NO
        {
            get;
            set;
        }

        // Token: 0x17000112 RID: 274
        // (get) Token: 0x06000249 RID: 585 RVA: 0x00004EBD File Offset: 0x000030BD
        // (set) Token: 0x0600024A RID: 586 RVA: 0x00004EC5 File Offset: 0x000030C5
        public string ManagerName
        {
            get;
            set;
        }

        // Token: 0x17000113 RID: 275
        // (get) Token: 0x0600024B RID: 587 RVA: 0x00004ECE File Offset: 0x000030CE
        // (set) Token: 0x0600024C RID: 588 RVA: 0x00004ED6 File Offset: 0x000030D6
        public int InKartablCount
        {
            get;
            set;
        }

        // Token: 0x17000114 RID: 276
        // (get) Token: 0x0600024D RID: 589 RVA: 0x00004EDF File Offset: 0x000030DF
        public string InKartabl
        {
            get
            {
                return this.InKartablCount + ((this.InKartablCountRev == 0) ? "" : ("(" + this.InKartablCountRev + ")"));
            }
        }

        // Token: 0x17000115 RID: 277
        // (get) Token: 0x0600024E RID: 590 RVA: 0x00004F1A File Offset: 0x0000311A
        public string Accept
        {
            get
            {
                return this.AcceptCount + ((this.AcceptCountRev == 0) ? "" : ("(" + this.AcceptCountRev + ")"));
            }
        }

        // Token: 0x17000116 RID: 278
        // (get) Token: 0x0600024F RID: 591 RVA: 0x00004F55 File Offset: 0x00003155
        public string Reject
        {
            get
            {
                return this.RejectCount + ((this.RejectCountRev == 0) ? "" : ("(" + this.RejectCountRev + ")"));
            }
        }

        // Token: 0x17000117 RID: 279
        // (get) Token: 0x06000250 RID: 592 RVA: 0x00004F90 File Offset: 0x00003190
        // (set) Token: 0x06000251 RID: 593 RVA: 0x00004F98 File Offset: 0x00003198
        public int AcceptCount
        {
            get;
            set;
        }

        // Token: 0x17000118 RID: 280
        // (get) Token: 0x06000252 RID: 594 RVA: 0x00004FA1 File Offset: 0x000031A1
        // (set) Token: 0x06000253 RID: 595 RVA: 0x00004FA9 File Offset: 0x000031A9
        public int RejectCount
        {
            get;
            set;
        }

        // Token: 0x17000119 RID: 281
        // (get) Token: 0x06000254 RID: 596 RVA: 0x00004FB2 File Offset: 0x000031B2
        // (set) Token: 0x06000255 RID: 597 RVA: 0x00004FBA File Offset: 0x000031BA
        public int InKartablCountRev
        {
            get;
            set;
        }

        // Token: 0x1700011A RID: 282
        // (get) Token: 0x06000256 RID: 598 RVA: 0x00004FC3 File Offset: 0x000031C3
        // (set) Token: 0x06000257 RID: 599 RVA: 0x00004FCB File Offset: 0x000031CB
        public int AcceptCountRev
        {
            get;
            set;
        }

        // Token: 0x1700011B RID: 283
        // (get) Token: 0x06000258 RID: 600 RVA: 0x00004FD4 File Offset: 0x000031D4
        // (set) Token: 0x06000259 RID: 601 RVA: 0x00004FDC File Offset: 0x000031DC
        public int RejectCountRev
        {
            get;
            set;
        }

        // Token: 0x1700011C RID: 284
        // (get) Token: 0x0600025A RID: 602 RVA: 0x00004FE5 File Offset: 0x000031E5
        // (set) Token: 0x0600025B RID: 603 RVA: 0x00004FED File Offset: 0x000031ED
        public int MoveUpCount
        {
            get;
            set;
        }

        // Token: 0x1700011D RID: 285
        // (get) Token: 0x0600025C RID: 604 RVA: 0x00004FF6 File Offset: 0x000031F6
        // (set) Token: 0x0600025D RID: 605 RVA: 0x00004FFE File Offset: 0x000031FE
        public int TotalRequest
        {
            get;
            set;
        }
    }
}
