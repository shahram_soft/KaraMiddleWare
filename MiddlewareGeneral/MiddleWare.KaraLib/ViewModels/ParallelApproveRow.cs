﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace MiddleWare.KaraLib.ViewModels
{
   public class ParallelApproveRow : BaseCls
    {
        // Token: 0x0600005B RID: 91 RVA: 0x00002550 File Offset: 0x00000750
        public ParallelApproveRow(DataRow dr)
        {
            this.CardNo = base.CastInt(dr["CardNo"]);
            this.SecNo = base.CastInt(dr["SecNo"]);
            this.EmpNo = base.CastDouble(dr["EmpNo"]);
            this.RequestId = base.CastInt(dr["RequestID"]);
            this.FullName = dr["FullName"].ToString();
            this.Section = dr["Section"].ToString();
            this.RequestDuration = base.CastInt(dr["RequestDuration"]);
            this.FromDate = base.CastDateTimeNullable(dr["FromDate"]);
            this.IsApprove = base.CastBool(dr["IsApprove"]);
            this.IsDaily = base.CastBool(dr["IS_DAY"]);
            this.CardTitle = dr["TITLE"].ToString();
            this.CardType = base.CastInt(dr["CARD_TYPE"]);
        }

        // Token: 0x0600005C RID: 92 RVA: 0x00002674 File Offset: 0x00000874
        public ParallelApproveRow(SqlDataReader dr)
        {
            this.CardNo = base.CastInt(dr["CardNo"]);
            this.SecNo = base.CastInt(dr["SecNo"]);
            this.EmpNo = base.CastDouble(dr["EmpNo"]);
            this.RequestId = base.CastInt(dr["RequestID"]);
            this.FullName = dr["FullName"].ToString();
            this.Section = dr["Section"].ToString();
            this.RequestDuration = base.CastInt(dr["RequestDuration"]);
            this.FromDate = base.CastDateTimeNullable(dr["FromDate"]);
            this.IsApprove = base.CastBool(dr["IsApprove"]);
            this.IsDaily = base.CastBool(dr["IS_DAY"]);
            this.CardTitle = dr["TITLE"].ToString();
            this.CardType = base.CastInt(dr["CARD_TYPE"]);
        }

        // Token: 0x17000027 RID: 39
        // (get) Token: 0x0600005D RID: 93 RVA: 0x00002798 File Offset: 0x00000998
        // (set) Token: 0x0600005E RID: 94 RVA: 0x000027A0 File Offset: 0x000009A0
        public int CardNo
        {
            get;
            set;
        }

        // Token: 0x17000028 RID: 40
        // (get) Token: 0x0600005F RID: 95 RVA: 0x000027A9 File Offset: 0x000009A9
        // (set) Token: 0x06000060 RID: 96 RVA: 0x000027B1 File Offset: 0x000009B1
        public int SecNo
        {
            get;
            set;
        }

        // Token: 0x17000029 RID: 41
        // (get) Token: 0x06000061 RID: 97 RVA: 0x000027BA File Offset: 0x000009BA
        // (set) Token: 0x06000062 RID: 98 RVA: 0x000027C2 File Offset: 0x000009C2
        public double EmpNo
        {
            get;
            set;
        }

        // Token: 0x1700002A RID: 42
        // (get) Token: 0x06000063 RID: 99 RVA: 0x000027CB File Offset: 0x000009CB
        // (set) Token: 0x06000064 RID: 100 RVA: 0x000027D3 File Offset: 0x000009D3
        public int RequestId
        {
            get;
            set;
        }

        // Token: 0x1700002B RID: 43
        // (get) Token: 0x06000065 RID: 101 RVA: 0x000027DC File Offset: 0x000009DC
        // (set) Token: 0x06000066 RID: 102 RVA: 0x000027E4 File Offset: 0x000009E4
        public string FullName
        {
            get;
            set;
        }

        // Token: 0x1700002C RID: 44
        // (get) Token: 0x06000067 RID: 103 RVA: 0x000027ED File Offset: 0x000009ED
        // (set) Token: 0x06000068 RID: 104 RVA: 0x000027F5 File Offset: 0x000009F5
        public string Section
        {
            get;
            set;
        }

        // Token: 0x1700002D RID: 45
        // (get) Token: 0x06000069 RID: 105 RVA: 0x000027FE File Offset: 0x000009FE
        // (set) Token: 0x0600006A RID: 106 RVA: 0x00002806 File Offset: 0x00000A06
        public DateTime? FromDate
        {
            get;
            set;
        }

        // Token: 0x1700002E RID: 46
        // (get) Token: 0x0600006B RID: 107 RVA: 0x0000280F File Offset: 0x00000A0F
        // (set) Token: 0x0600006C RID: 108 RVA: 0x00002817 File Offset: 0x00000A17
        public int RequestDuration
        {
            get;
            set;
        }

        // Token: 0x1700002F RID: 47
        // (get) Token: 0x0600006D RID: 109 RVA: 0x00002820 File Offset: 0x00000A20
        // (set) Token: 0x0600006E RID: 110 RVA: 0x00002828 File Offset: 0x00000A28
        public bool IsApprove
        {
            get;
            set;
        }

        // Token: 0x17000030 RID: 48
        // (get) Token: 0x0600006F RID: 111 RVA: 0x00002831 File Offset: 0x00000A31
        // (set) Token: 0x06000070 RID: 112 RVA: 0x00002839 File Offset: 0x00000A39
        public string CardTitle
        {
            get;
            set;
        }

        // Token: 0x17000031 RID: 49
        // (get) Token: 0x06000071 RID: 113 RVA: 0x00002842 File Offset: 0x00000A42
        // (set) Token: 0x06000072 RID: 114 RVA: 0x0000284A File Offset: 0x00000A4A
        public bool IsDaily
        {
            get;
            set;
        }

        // Token: 0x17000032 RID: 50
        // (get) Token: 0x06000073 RID: 115 RVA: 0x00002853 File Offset: 0x00000A53
        // (set) Token: 0x06000074 RID: 116 RVA: 0x0000285B File Offset: 0x00000A5B
        public int CardType
        {
            get;
            set;
        }
    }
}
