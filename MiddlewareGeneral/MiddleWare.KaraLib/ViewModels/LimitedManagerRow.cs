﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiddleWare.KaraLib.ViewModels
{
  public  class LimitedManagerRow
    {
        // Token: 0x02000017 RID: 23
      
            // Token: 0x06000167 RID: 359 RVA: 0x0000211C File Offset: 0x0000031C
            public LimitedManagerRow()
            {
            }

            // Token: 0x06000168 RID: 360 RVA: 0x0000365C File Offset: 0x0000185C
            public LimitedManagerRow(DataTable dt)
            {
                if (dt != null && dt.Rows != null && dt.Rows.Count > 0)
                {
                    IEnumerator enumerator = dt.Rows.GetEnumerator();
                    {
                        while (enumerator.MoveNext())
                        {
                            DataRow dataRow = (DataRow)enumerator.Current;
                            switch (Convert.ToInt32(dataRow["ActionId"]))
                            {
                                case 1:
                                    this.LimitDailyLeave = Convert.ToInt32(dataRow["LimitDuration"]);
                                    this.LimitDailyLeavePerMonth = Convert.ToInt32(dataRow["LimitDurationPerMonth"]);
                                    break;
                                case 2:
                                    this.LimitDailyDuty = Convert.ToInt32(dataRow["LimitDuration"]);
                                    this.LimitDailyDutyPerMonth = Convert.ToInt32(dataRow["LimitDurationPerMonth"]);
                                    break;
                                case 3:
                                    this.LimitTimeLeave = Convert.ToInt32(dataRow["LimitDuration"]);
                                    this.LimitTimeLeavePerMonth = Convert.ToInt32(dataRow["LimitDurationPerMonth"]);
                                    break;
                                case 4:
                                    this.LimitInOutCorrection = Convert.ToBoolean(dataRow["LimitDuration"]);
                                    this.LimitInOutCorrectionPerMonth = Convert.ToBoolean(dataRow["LimitDurationPerMonth"]);
                                    break;
                                case 5:
                                    this.LimitOverTime = Convert.ToInt32(dataRow["LimitDuration"]);
                                    this.LimitOverTimePerMonth = Convert.ToInt32(dataRow["LimitDurationPerMonth"]);
                                    break;
                                case 6:
                                    this.LimitShiftReplace = Convert.ToInt32(dataRow["LimitDuration"]);
                                    this.LimitShiftReplacePerMonth = Convert.ToInt32(dataRow["LimitDurationPerMonth"]);
                                    break;
                                case 7:
                                    this.LimitLeaveAddition = Convert.ToInt32(dataRow["LimitDuration"]);
                                    this.LimitLeaveAdditionPerMonth = Convert.ToInt32(dataRow["LimitDurationPerMonth"]);
                                    break;
                                case 8:
                                    this.LimitForgottenInOut = Convert.ToBoolean(dataRow["LimitDuration"]);
                                    this.LimitForgottenInOutPerMonth = Convert.ToBoolean(dataRow["LimitDurationPerMonth"]);
                                    break;
                                case 9:
                                    this.LimitTimeDuty = Convert.ToInt32(dataRow["LimitDuration"]);
                                    this.LimitTimeDutyPerMonth = Convert.ToInt32(dataRow["LimitDurationPerMonth"]);
                                    break;
                                case 10:
                                    this.LimitDailyLeaveOut = Convert.ToInt32(dataRow["LimitDuration"]);
                                    this.LimitDailyLeaveOutPerMonth = Convert.ToInt32(dataRow["LimitDurationPerMonth"]);
                                    break;
                                case 12:
                                    this.LimitTimeDutyNoReturn = Convert.ToBoolean(dataRow["LimitDuration"]);
                                    this.LimitTimeDutyNoReturnPerMonth = Convert.ToBoolean(dataRow["LimitDurationPerMonth"]);
                                    break;
                            }
                        }
                        return;
                    }
                }
                this.LimitDailyDuty = 0;
                this.LimitDailyLeave = 0;
                this.LimitDailyLeaveOut = 0;
                this.LimitForgottenInOut = false;
                this.LimitInOutCorrection = false;
                this.LimitLeaveAddition = 0;
                this.LimitOverTime = 0;
                this.LimitShiftReplace = 0;
                this.LimitTimeDuty = 0;
                this.LimitTimeLeave = 0;
                this.LimitTimeDutyNoReturn = false;
            }

            // Token: 0x170000A9 RID: 169
            // (get) Token: 0x06000169 RID: 361 RVA: 0x00003994 File Offset: 0x00001B94
            // (set) Token: 0x0600016A RID: 362 RVA: 0x0000399C File Offset: 0x00001B9C
            public int LimitDailyDuty
            {
                get;
                set;
            }

            // Token: 0x170000AA RID: 170
            // (get) Token: 0x0600016B RID: 363 RVA: 0x000039A5 File Offset: 0x00001BA5
            // (set) Token: 0x0600016C RID: 364 RVA: 0x000039AD File Offset: 0x00001BAD
            public int LimitDailyLeave
            {
                get;
                set;
            }

            // Token: 0x170000AB RID: 171
            // (get) Token: 0x0600016D RID: 365 RVA: 0x000039B6 File Offset: 0x00001BB6
            // (set) Token: 0x0600016E RID: 366 RVA: 0x000039BE File Offset: 0x00001BBE
            public int LimitDailyLeaveOut
            {
                get;
                set;
            }

            // Token: 0x170000AC RID: 172
            // (get) Token: 0x0600016F RID: 367 RVA: 0x000039C7 File Offset: 0x00001BC7
            // (set) Token: 0x06000170 RID: 368 RVA: 0x000039CF File Offset: 0x00001BCF
            public bool LimitForgottenInOut
            {
                get;
                set;
            }

            // Token: 0x170000AD RID: 173
            // (get) Token: 0x06000171 RID: 369 RVA: 0x000039D8 File Offset: 0x00001BD8
            // (set) Token: 0x06000172 RID: 370 RVA: 0x000039E0 File Offset: 0x00001BE0
            public bool LimitInOutCorrection
            {
                get;
                set;
            }

            // Token: 0x170000AE RID: 174
            // (get) Token: 0x06000173 RID: 371 RVA: 0x000039E9 File Offset: 0x00001BE9
            // (set) Token: 0x06000174 RID: 372 RVA: 0x000039F1 File Offset: 0x00001BF1
            public int LimitLeaveAddition
            {
                get;
                set;
            }

            // Token: 0x170000AF RID: 175
            // (get) Token: 0x06000175 RID: 373 RVA: 0x000039FA File Offset: 0x00001BFA
            // (set) Token: 0x06000176 RID: 374 RVA: 0x00003A02 File Offset: 0x00001C02
            public int LimitOverTime
            {
                get;
                set;
            }

            // Token: 0x170000B0 RID: 176
            // (get) Token: 0x06000177 RID: 375 RVA: 0x00003A0B File Offset: 0x00001C0B
            // (set) Token: 0x06000178 RID: 376 RVA: 0x00003A13 File Offset: 0x00001C13
            public int LimitShiftReplace
            {
                get;
                set;
            }

            // Token: 0x170000B1 RID: 177
            // (get) Token: 0x06000179 RID: 377 RVA: 0x00003A1C File Offset: 0x00001C1C
            // (set) Token: 0x0600017A RID: 378 RVA: 0x00003A24 File Offset: 0x00001C24
            public int LimitTimeDuty
            {
                get;
                set;
            }

            // Token: 0x170000B2 RID: 178
            // (get) Token: 0x0600017B RID: 379 RVA: 0x00003A2D File Offset: 0x00001C2D
            // (set) Token: 0x0600017C RID: 380 RVA: 0x00003A35 File Offset: 0x00001C35
            public int LimitTimeLeave
            {
                get;
                set;
            }

            // Token: 0x170000B3 RID: 179
            // (get) Token: 0x0600017D RID: 381 RVA: 0x00003A3E File Offset: 0x00001C3E
            // (set) Token: 0x0600017E RID: 382 RVA: 0x00003A46 File Offset: 0x00001C46
            public bool LimitTimeDutyNoReturn
            {
                get;
                set;
            }

            // Token: 0x170000B4 RID: 180
            // (get) Token: 0x0600017F RID: 383 RVA: 0x00003A4F File Offset: 0x00001C4F
            // (set) Token: 0x06000180 RID: 384 RVA: 0x00003A57 File Offset: 0x00001C57
            public int LimitDailyDutyPerMonth
            {
                get;
                set;
            }

            // Token: 0x170000B5 RID: 181
            // (get) Token: 0x06000181 RID: 385 RVA: 0x00003A60 File Offset: 0x00001C60
            // (set) Token: 0x06000182 RID: 386 RVA: 0x00003A68 File Offset: 0x00001C68
            public int LimitDailyLeavePerMonth
            {
                get;
                set;
            }

            // Token: 0x170000B6 RID: 182
            // (get) Token: 0x06000183 RID: 387 RVA: 0x00003A71 File Offset: 0x00001C71
            // (set) Token: 0x06000184 RID: 388 RVA: 0x00003A79 File Offset: 0x00001C79
            public int LimitDailyLeaveOutPerMonth
            {
                get;
                set;
            }

            // Token: 0x170000B7 RID: 183
            // (get) Token: 0x06000185 RID: 389 RVA: 0x00003A82 File Offset: 0x00001C82
            // (set) Token: 0x06000186 RID: 390 RVA: 0x00003A8A File Offset: 0x00001C8A
            public bool LimitForgottenInOutPerMonth
            {
                get;
                set;
            }

            // Token: 0x170000B8 RID: 184
            // (get) Token: 0x06000187 RID: 391 RVA: 0x00003A93 File Offset: 0x00001C93
            // (set) Token: 0x06000188 RID: 392 RVA: 0x00003A9B File Offset: 0x00001C9B
            public bool LimitInOutCorrectionPerMonth
            {
                get;
                set;
            }

            // Token: 0x170000B9 RID: 185
            // (get) Token: 0x06000189 RID: 393 RVA: 0x00003AA4 File Offset: 0x00001CA4
            // (set) Token: 0x0600018A RID: 394 RVA: 0x00003AAC File Offset: 0x00001CAC
            public int LimitLeaveAdditionPerMonth
            {
                get;
                set;
            }

            // Token: 0x170000BA RID: 186
            // (get) Token: 0x0600018B RID: 395 RVA: 0x00003AB5 File Offset: 0x00001CB5
            // (set) Token: 0x0600018C RID: 396 RVA: 0x00003ABD File Offset: 0x00001CBD
            public int LimitOverTimePerMonth
            {
                get;
                set;
            }

            // Token: 0x170000BB RID: 187
            // (get) Token: 0x0600018D RID: 397 RVA: 0x00003AC6 File Offset: 0x00001CC6
            // (set) Token: 0x0600018E RID: 398 RVA: 0x00003ACE File Offset: 0x00001CCE
            public int LimitShiftReplacePerMonth
            {
                get;
                set;
            }

            // Token: 0x170000BC RID: 188
            // (get) Token: 0x0600018F RID: 399 RVA: 0x00003AD7 File Offset: 0x00001CD7
            // (set) Token: 0x06000190 RID: 400 RVA: 0x00003ADF File Offset: 0x00001CDF
            public int LimitTimeDutyPerMonth
            {
                get;
                set;
            }

            // Token: 0x170000BD RID: 189
            // (get) Token: 0x06000191 RID: 401 RVA: 0x00003AE8 File Offset: 0x00001CE8
            // (set) Token: 0x06000192 RID: 402 RVA: 0x00003AF0 File Offset: 0x00001CF0
            public int LimitTimeLeavePerMonth
            {
                get;
                set;
            }

            // Token: 0x170000BE RID: 190
            // (get) Token: 0x06000193 RID: 403 RVA: 0x00003AF9 File Offset: 0x00001CF9
            // (set) Token: 0x06000194 RID: 404 RVA: 0x00003B01 File Offset: 0x00001D01
            public bool LimitTimeDutyNoReturnPerMonth
            {
                get;
                set;
            }
        }
}
