﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiddleWare.KaraLib.ViewModels
{
  public  class TurnoverManagerRow : BaseRow
    {// Token: 0x0600025E RID: 606 RVA: 0x00005007 File Offset: 0x00003207
        public TurnoverManagerRow(int EmpNO, string EmpName, int YD, short MD, short WD, int CountReq, string WDName)
        {
            this._EmpNO = EmpNO;
            this._EmpName = EmpName;
            this._YD = YD;
            this._MD = MD;
            this._WD = WD;
            this._CountReq = CountReq;
            this._WDName = WDName;
        }

        // Token: 0x0600025F RID: 607 RVA: 0x00002D48 File Offset: 0x00000F48
        public TurnoverManagerRow()
        {
        }

        // Token: 0x1700011E RID: 286
        // (get) Token: 0x06000260 RID: 608 RVA: 0x00005044 File Offset: 0x00003244
        // (set) Token: 0x06000261 RID: 609 RVA: 0x0000504C File Offset: 0x0000324C
        public int EmpNO
        {
            get
            {
                return this._EmpNO;
            }
            set
            {
                this._EmpNO = value;
            }
        }

        // Token: 0x1700011F RID: 287
        // (get) Token: 0x06000262 RID: 610 RVA: 0x00005055 File Offset: 0x00003255
        // (set) Token: 0x06000263 RID: 611 RVA: 0x0000505D File Offset: 0x0000325D
        public string EmpName
        {
            get
            {
                return this._EmpName;
            }
            set
            {
                this._EmpName = value;
            }
        }

        // Token: 0x17000120 RID: 288
        // (get) Token: 0x06000264 RID: 612 RVA: 0x00005066 File Offset: 0x00003266
        // (set) Token: 0x06000265 RID: 613 RVA: 0x0000506E File Offset: 0x0000326E
        public int YD
        {
            get
            {
                return this._YD;
            }
            set
            {
                this._YD = value;
            }
        }

        // Token: 0x17000121 RID: 289
        // (get) Token: 0x06000266 RID: 614 RVA: 0x00005077 File Offset: 0x00003277
        // (set) Token: 0x06000267 RID: 615 RVA: 0x0000507F File Offset: 0x0000327F
        public short MD
        {
            get
            {
                return this._MD;
            }
            set
            {
                this._MD = value;
            }
        }

        // Token: 0x17000122 RID: 290
        // (get) Token: 0x06000268 RID: 616 RVA: 0x00005088 File Offset: 0x00003288
        // (set) Token: 0x06000269 RID: 617 RVA: 0x00005090 File Offset: 0x00003290
        public short WD
        {
            get
            {
                return this._WD;
            }
            set
            {
                this._WD = value;
            }
        }

        // Token: 0x17000123 RID: 291
        // (get) Token: 0x0600026A RID: 618 RVA: 0x00005099 File Offset: 0x00003299
        // (set) Token: 0x0600026B RID: 619 RVA: 0x000050A1 File Offset: 0x000032A1
        public string WDName
        {
            get
            {
                return this._WDName;
            }
            set
            {
                this._WDName = value;
            }
        }

        // Token: 0x17000124 RID: 292
        // (get) Token: 0x0600026C RID: 620 RVA: 0x000050AC File Offset: 0x000032AC
        public string MonthName
        {
            get
            {
                return ShDate.MonthName(this.MD) + " سال" + this.YD.ToString();
            }
        }

        // Token: 0x17000125 RID: 293
        // (get) Token: 0x0600026D RID: 621 RVA: 0x000050DC File Offset: 0x000032DC
        // (set) Token: 0x0600026E RID: 622 RVA: 0x000050E4 File Offset: 0x000032E4
        public int CountReq
        {
            get
            {
                return this._CountReq;
            }
            set
            {
                this._CountReq = value;
            }
        }

        // Token: 0x17000126 RID: 294
        // (get) Token: 0x0600026F RID: 623 RVA: 0x000050ED File Offset: 0x000032ED
        // (set) Token: 0x06000270 RID: 624 RVA: 0x000050F5 File Offset: 0x000032F5
        public int OperationsID
        {
            get
            {
                return this._OperationsID;
            }
            set
            {
                this._OperationsID = value;
            }
        }

        // Token: 0x04000160 RID: 352
        private int _EmpNO;

        // Token: 0x04000161 RID: 353
        private string _EmpName;

        // Token: 0x04000162 RID: 354
        private int _YD;

        // Token: 0x04000163 RID: 355
        private short _MD;

        // Token: 0x04000164 RID: 356
        private short _WD;

        // Token: 0x04000165 RID: 357
        private string _WDName;

        // Token: 0x04000166 RID: 358
        private int _CountReq;

        // Token: 0x04000167 RID: 359
        private int _OperationsID;
    }
}
