﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiddleWare.KaraLib.ViewModels
{
  public  class RequestByEmpRow : BaseRow
    {
        // Token: 0x06000195 RID: 405 RVA: 0x00002D48 File Offset: 0x00000F48
        public RequestByEmpRow()
        {
        }

        // Token: 0x06000196 RID: 406 RVA: 0x00003B0C File Offset: 0x00001D0C
        public RequestByEmpRow(double empNo, int totalCount, int overTimr, int taradod, int duty, int leave, string name, string family, double secNo, string title)
        {
            this.EmpNo = empNo;
            this.TotalCount = totalCount;
            this.OverTimr = overTimr;
            this.Taradod = taradod;
            this.Duty = duty;
            this.Leave = leave;
            this.Name = name;
            this.Family = family;
            this.SecNo = secNo;
            this.Title = title;
        }

        // Token: 0x170000BF RID: 191
        // (get) Token: 0x06000197 RID: 407 RVA: 0x00003B6C File Offset: 0x00001D6C
        // (set) Token: 0x06000198 RID: 408 RVA: 0x00003B74 File Offset: 0x00001D74
        public double EmpNo
        {
            get;
            set;
        }

        // Token: 0x170000C0 RID: 192
        // (get) Token: 0x06000199 RID: 409 RVA: 0x00003B7D File Offset: 0x00001D7D
        // (set) Token: 0x0600019A RID: 410 RVA: 0x00003B85 File Offset: 0x00001D85
        public int TotalCount
        {
            get;
            set;
        }

        // Token: 0x170000C1 RID: 193
        // (get) Token: 0x0600019B RID: 411 RVA: 0x00003B8E File Offset: 0x00001D8E
        // (set) Token: 0x0600019C RID: 412 RVA: 0x00003B96 File Offset: 0x00001D96
        public int OverTimr
        {
            get;
            set;
        }

        // Token: 0x170000C2 RID: 194
        // (get) Token: 0x0600019D RID: 413 RVA: 0x00003B9F File Offset: 0x00001D9F
        // (set) Token: 0x0600019E RID: 414 RVA: 0x00003BA7 File Offset: 0x00001DA7
        public int Taradod
        {
            get;
            set;
        }

        // Token: 0x170000C3 RID: 195
        // (get) Token: 0x0600019F RID: 415 RVA: 0x00003BB0 File Offset: 0x00001DB0
        // (set) Token: 0x060001A0 RID: 416 RVA: 0x00003BB8 File Offset: 0x00001DB8
        public int Duty
        {
            get;
            set;
        }

        // Token: 0x170000C4 RID: 196
        // (get) Token: 0x060001A1 RID: 417 RVA: 0x00003BC1 File Offset: 0x00001DC1
        // (set) Token: 0x060001A2 RID: 418 RVA: 0x00003BC9 File Offset: 0x00001DC9
        public int Leave
        {
            get;
            set;
        }

        // Token: 0x170000C5 RID: 197
        // (get) Token: 0x060001A3 RID: 419 RVA: 0x00003BD2 File Offset: 0x00001DD2
        // (set) Token: 0x060001A4 RID: 420 RVA: 0x00003BDA File Offset: 0x00001DDA
        public string Name
        {
            get;
            set;
        }

        // Token: 0x170000C6 RID: 198
        // (get) Token: 0x060001A5 RID: 421 RVA: 0x00003BE3 File Offset: 0x00001DE3
        // (set) Token: 0x060001A6 RID: 422 RVA: 0x00003BEB File Offset: 0x00001DEB
        public string Family
        {
            get;
            set;
        }

        // Token: 0x170000C7 RID: 199
        // (get) Token: 0x060001A7 RID: 423 RVA: 0x00003BF4 File Offset: 0x00001DF4
        public string FullName
        {
            get
            {
                return this.Name + " " + this.Family;
            }
        }

        // Token: 0x170000C8 RID: 200
        // (get) Token: 0x060001A8 RID: 424 RVA: 0x00003C0C File Offset: 0x00001E0C
        // (set) Token: 0x060001A9 RID: 425 RVA: 0x00003C14 File Offset: 0x00001E14
        public double SecNo
        {
            get;
            set;
        }

        // Token: 0x170000C9 RID: 201
        // (get) Token: 0x060001AA RID: 426 RVA: 0x00003C1D File Offset: 0x00001E1D
        // (set) Token: 0x060001AB RID: 427 RVA: 0x00003C25 File Offset: 0x00001E25
        public string Title
        {
            get;
            set;
        }
    }
}
