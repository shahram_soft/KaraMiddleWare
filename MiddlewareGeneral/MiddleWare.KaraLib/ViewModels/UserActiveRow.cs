﻿using System;

namespace MiddleWare.KaraLib.ViewModels
{
   public class UserActiveRow
    {
        // Token: 0x17000127 RID: 295
        // (get) Token: 0x06000271 RID: 625 RVA: 0x000050FE File Offset: 0x000032FE
        // (set) Token: 0x06000272 RID: 626 RVA: 0x00005106 File Offset: 0x00003306
        public double EmpNo
        {
            get;
            set;
        }

        // Token: 0x17000128 RID: 296
        // (get) Token: 0x06000273 RID: 627 RVA: 0x0000510F File Offset: 0x0000330F
        // (set) Token: 0x06000274 RID: 628 RVA: 0x00005117 File Offset: 0x00003317
        public string Name
        {
            get;
            set;
        }

        // Token: 0x17000129 RID: 297
        // (get) Token: 0x06000275 RID: 629 RVA: 0x00005120 File Offset: 0x00003320
        // (set) Token: 0x06000276 RID: 630 RVA: 0x00005128 File Offset: 0x00003328
        public string Family
        {
            get;
            set;
        }

        // Token: 0x1700012A RID: 298
        // (get) Token: 0x06000277 RID: 631 RVA: 0x00005131 File Offset: 0x00003331
        public string FullName
        {
            get
            {
                return this.Name + " " + this.Family;
            }
        }

        // Token: 0x1700012B RID: 299
        // (get) Token: 0x06000278 RID: 632 RVA: 0x00005149 File Offset: 0x00003349
        // (set) Token: 0x06000279 RID: 633 RVA: 0x00005151 File Offset: 0x00003351
        public short GrpNo
        {
            get;
            set;
        }

        // Token: 0x1700012C RID: 300
        // (get) Token: 0x0600027A RID: 634 RVA: 0x0000515A File Offset: 0x0000335A
        // (set) Token: 0x0600027B RID: 635 RVA: 0x00005162 File Offset: 0x00003362
        public DateTime EndDate
        {
            get;
            set;
        }

        // Token: 0x1700012D RID: 301
        // (get) Token: 0x0600027C RID: 636 RVA: 0x0000516B File Offset: 0x0000336B
        // (set) Token: 0x0600027D RID: 637 RVA: 0x00005173 File Offset: 0x00003373
        public bool IsUniq
        {
            get;
            set;
        }

        // Token: 0x1700012E RID: 302
        // (get) Token: 0x0600027E RID: 638 RVA: 0x0000517C File Offset: 0x0000337C
        // (set) Token: 0x0600027F RID: 639 RVA: 0x00005184 File Offset: 0x00003384
        public bool SysActive
        {
            get;
            set;
        }

        // Token: 0x1700012F RID: 303
        // (get) Token: 0x06000280 RID: 640 RVA: 0x0000518D File Offset: 0x0000338D
        // (set) Token: 0x06000281 RID: 641 RVA: 0x00005195 File Offset: 0x00003395
        public bool IsCut
        {
            get;
            set;
        }

        // Token: 0x17000130 RID: 304
        // (get) Token: 0x06000282 RID: 642 RVA: 0x0000519E File Offset: 0x0000339E
        // (set) Token: 0x06000283 RID: 643 RVA: 0x000051A6 File Offset: 0x000033A6
        public bool IsWarden
        {
            get;
            set;
        }

        // Token: 0x17000131 RID: 305
        // (get) Token: 0x06000284 RID: 644 RVA: 0x000051AF File Offset: 0x000033AF
        // (set) Token: 0x06000285 RID: 645 RVA: 0x000051B7 File Offset: 0x000033B7
        public string Section
        {
            get;
            set;
        }

        // Token: 0x17000132 RID: 306
        // (get) Token: 0x06000286 RID: 646 RVA: 0x000051C0 File Offset: 0x000033C0
        // (set) Token: 0x06000287 RID: 647 RVA: 0x000051C8 File Offset: 0x000033C8
        public int SecNo
        {
            get;
            set;
        }

        // Token: 0x17000133 RID: 307
        // (get) Token: 0x06000288 RID: 648 RVA: 0x000051D1 File Offset: 0x000033D1
        // (set) Token: 0x06000289 RID: 649 RVA: 0x000051D9 File Offset: 0x000033D9
        public byte[] EmpImg
        {
            get;
            set;
        }

        // Token: 0x17000134 RID: 308
        // (get) Token: 0x0600028A RID: 650 RVA: 0x000051E2 File Offset: 0x000033E2
        // (set) Token: 0x0600028B RID: 651 RVA: 0x000051EA File Offset: 0x000033EA
        public bool IsSupervisor
        {
            get;
            set;
        }

        // Token: 0x17000135 RID: 309
        // (get) Token: 0x0600028C RID: 652 RVA: 0x000051F3 File Offset: 0x000033F3
        // (set) Token: 0x0600028D RID: 653 RVA: 0x000051FB File Offset: 0x000033FB
        public bool IsManager
        {
            get;
            set;
        }

        // Token: 0x17000136 RID: 310
        // (get) Token: 0x0600028E RID: 654 RVA: 0x00005204 File Offset: 0x00003404
        // (set) Token: 0x0600028F RID: 655 RVA: 0x0000520C File Offset: 0x0000340C
        public bool IsSubstitute
        {
            get;
            set;
        }

        // Token: 0x17000137 RID: 311
        // (get) Token: 0x06000290 RID: 656 RVA: 0x00005215 File Offset: 0x00003415
        // (set) Token: 0x06000291 RID: 657 RVA: 0x0000521D File Offset: 0x0000341D
        public int WardenDayCount
        {
            get;
            set;
        }

        // Token: 0x17000138 RID: 312
        // (get) Token: 0x06000292 RID: 658 RVA: 0x00005226 File Offset: 0x00003426
        // (set) Token: 0x06000293 RID: 659 RVA: 0x0000522E File Offset: 0x0000342E
        public bool WardenJustInShift
        {
            get;
            set;
        }

        // Token: 0x17000139 RID: 313
        // (get) Token: 0x06000294 RID: 660 RVA: 0x00005237 File Offset: 0x00003437
        // (set) Token: 0x06000295 RID: 661 RVA: 0x0000523F File Offset: 0x0000343F
        public bool IsParallelApprove
        {
            get;
            set;
        }

        // Token: 0x1700013A RID: 314
        // (get) Token: 0x06000296 RID: 662 RVA: 0x00005248 File Offset: 0x00003448
        // (set) Token: 0x06000297 RID: 663 RVA: 0x00005250 File Offset: 0x00003450
        public bool IsParallelSectionKartabl
        {
            get;
            set;
        }

        // Token: 0x1700013B RID: 315
        // (get) Token: 0x06000298 RID: 664 RVA: 0x00005259 File Offset: 0x00003459
        // (set) Token: 0x06000299 RID: 665 RVA: 0x00005261 File Offset: 0x00003461
        public bool IsPrintViewer
        {
            get;
            set;
        }

        // Token: 0x1700013C RID: 316
        // (get) Token: 0x0600029A RID: 666 RVA: 0x0000526A File Offset: 0x0000346A
        // (set) Token: 0x0600029B RID: 667 RVA: 0x00005272 File Offset: 0x00003472
        public bool IsConferment
        {
            get;
            set;
        }

        // Token: 0x1700013D RID: 317
        // (get) Token: 0x0600029C RID: 668 RVA: 0x0000527B File Offset: 0x0000347B
        // (set) Token: 0x0600029D RID: 669 RVA: 0x00005283 File Offset: 0x00003483
        public bool IsEmpMngr
        {
            get;
            set;
        }

        // Token: 0x1700013E RID: 318
        // (get) Token: 0x0600029E RID: 670 RVA: 0x0000528C File Offset: 0x0000348C
        // (set) Token: 0x0600029F RID: 671 RVA: 0x00005294 File Offset: 0x00003494
        public ActionAccessPrint ActionAccessPrint
        {
            get;
            set;
        }

        // Token: 0x1700013F RID: 319
        // (get) Token: 0x060002A0 RID: 672 RVA: 0x0000529D File Offset: 0x0000349D
        // (set) Token: 0x060002A1 RID: 673 RVA: 0x000052A5 File Offset: 0x000034A5
        public bool? WardenIsShowRequest
        {
            get;
            set;
        }

        // Token: 0x17000140 RID: 320
        // (get) Token: 0x060002A2 RID: 674 RVA: 0x000052AE File Offset: 0x000034AE
        // (set) Token: 0x060002A3 RID: 675 RVA: 0x000052B6 File Offset: 0x000034B6
        public bool? WardenIsShowReview
        {
            get;
            set;
        }
    }
    // Token: 0x02000004 RID: 4
    public class ActionAccessPrint
    {
        // Token: 0x17000001 RID: 1
        // (get) Token: 0x06000001 RID: 1 RVA: 0x00002050 File Offset: 0x00000250
        // (set) Token: 0x06000002 RID: 2 RVA: 0x00002058 File Offset: 0x00000258
        public bool DailyLeave
        {
            get;
            set;
        }

        // Token: 0x17000002 RID: 2
        // (get) Token: 0x06000003 RID: 3 RVA: 0x00002061 File Offset: 0x00000261
        // (set) Token: 0x06000004 RID: 4 RVA: 0x00002069 File Offset: 0x00000269
        public bool DailyDuty
        {
            get;
            set;
        }

        // Token: 0x17000003 RID: 3
        // (get) Token: 0x06000005 RID: 5 RVA: 0x00002072 File Offset: 0x00000272
        // (set) Token: 0x06000006 RID: 6 RVA: 0x0000207A File Offset: 0x0000027A
        public bool TimeLeave
        {
            get;
            set;
        }

        // Token: 0x17000004 RID: 4
        // (get) Token: 0x06000007 RID: 7 RVA: 0x00002083 File Offset: 0x00000283
        // (set) Token: 0x06000008 RID: 8 RVA: 0x0000208B File Offset: 0x0000028B
        public bool InOutCorrection
        {
            get;
            set;
        }

        // Token: 0x17000005 RID: 5
        // (get) Token: 0x06000009 RID: 9 RVA: 0x00002094 File Offset: 0x00000294
        // (set) Token: 0x0600000A RID: 10 RVA: 0x0000209C File Offset: 0x0000029C
        public bool OverTime
        {
            get;
            set;
        }

        // Token: 0x17000006 RID: 6
        // (get) Token: 0x0600000B RID: 11 RVA: 0x000020A5 File Offset: 0x000002A5
        // (set) Token: 0x0600000C RID: 12 RVA: 0x000020AD File Offset: 0x000002AD
        public bool ShiftReplace
        {
            get;
            set;
        }

        // Token: 0x17000007 RID: 7
        // (get) Token: 0x0600000D RID: 13 RVA: 0x000020B6 File Offset: 0x000002B6
        // (set) Token: 0x0600000E RID: 14 RVA: 0x000020BE File Offset: 0x000002BE
        public bool LeaveAddition
        {
            get;
            set;
        }

        // Token: 0x17000008 RID: 8
        // (get) Token: 0x0600000F RID: 15 RVA: 0x000020C7 File Offset: 0x000002C7
        // (set) Token: 0x06000010 RID: 16 RVA: 0x000020CF File Offset: 0x000002CF
        public bool ForgottenInOut
        {
            get;
            set;
        }

        // Token: 0x17000009 RID: 9
        // (get) Token: 0x06000011 RID: 17 RVA: 0x000020D8 File Offset: 0x000002D8
        // (set) Token: 0x06000012 RID: 18 RVA: 0x000020E0 File Offset: 0x000002E0
        public bool TimeDuty
        {
            get;
            set;
        }

        // Token: 0x1700000A RID: 10
        // (get) Token: 0x06000013 RID: 19 RVA: 0x000020E9 File Offset: 0x000002E9
        // (set) Token: 0x06000014 RID: 20 RVA: 0x000020F1 File Offset: 0x000002F1
        public bool DailyLeaveOut
        {
            get;
            set;
        }

        // Token: 0x1700000B RID: 11
        // (get) Token: 0x06000015 RID: 21 RVA: 0x000020FA File Offset: 0x000002FA
        // (set) Token: 0x06000016 RID: 22 RVA: 0x00002102 File Offset: 0x00000302
        public bool ShowDailyWork
        {
            get;
            set;
        }

        // Token: 0x1700000C RID: 12
        // (get) Token: 0x06000017 RID: 23 RVA: 0x0000210B File Offset: 0x0000030B
        // (set) Token: 0x06000018 RID: 24 RVA: 0x00002113 File Offset: 0x00000313
        public bool TimeDutyNoReturn
        {
            get;
            set;
        }
    }
}
