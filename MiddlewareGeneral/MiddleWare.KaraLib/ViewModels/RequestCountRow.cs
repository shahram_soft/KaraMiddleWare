﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiddleWare.KaraLib.ViewModels
{
  public  class RequestCountRow : BaseRow
    {
        // Token: 0x06000075 RID: 117 RVA: 0x00002864 File Offset: 0x00000A64
        public RequestCountRow()
        {
            this.IsHaveError = true;
        }

        // Token: 0x06000076 RID: 118 RVA: 0x00002874 File Offset: 0x00000A74
        public RequestCountRow(IOrderedDictionary dr)
        {
            this.RequestCount = Convert.ToInt32((dr["RequestCount"] is DBNull || dr["RequestCount"] == null) ? 0 : dr["RequestCount"]);
            this.ApproveCount = Convert.ToInt32((dr["ApproveCount"] is DBNull || dr["ApproveCount"] == null) ? 0 : dr["ApproveCount"]);
            this.RejectCount = Convert.ToInt32((dr["RejectCount"] is DBNull || dr["RejectCount"] == null) ? 0 : dr["RejectCount"]);
            this.CancelCount = Convert.ToInt32((dr["CancelCount"] is DBNull || dr["CancelCount"] == null) ? 0 : dr["CancelCount"]);
            this.ExpireCount = Convert.ToInt32((dr["ExpireCount"] is DBNull || dr["ExpireCount"] == null) ? 0 : dr["ExpireCount"]);
            this.MoveUpCount = Convert.ToInt32((dr["MoveUpCount"] is DBNull || dr["MoveUpCount"] == null) ? 0 : dr["MoveUpCount"]);
            this.ReviewCount = Convert.ToInt32((dr["ReviewCount"] is DBNull || dr["ReviewCount"] == null) ? 0 : dr["ReviewCount"]);
            this.IsHaveError = false;
        }

        // Token: 0x06000077 RID: 119 RVA: 0x00002A3C File Offset: 0x00000C3C
        public RequestCountRow(DataRow dr)
        {
            this.RequestCount = Convert.ToInt32((dr["RequestCount"] is DBNull || dr["RequestCount"] == null) ? 0 : dr["RequestCount"]);
            this.ApproveCount = Convert.ToInt32((dr["ApproveCount"] is DBNull || dr["ApproveCount"] == null) ? 0 : dr["ApproveCount"]);
            this.RejectCount = Convert.ToInt32((dr["RejectCount"] is DBNull || dr["RejectCount"] == null) ? 0 : dr["RejectCount"]);
            this.CancelCount = Convert.ToInt32((dr["CancelCount"] is DBNull || dr["CancelCount"] == null) ? 0 : dr["CancelCount"]);
            this.ExpireCount = Convert.ToInt32((dr["ExpireCount"] is DBNull || dr["ExpireCount"] == null) ? 0 : dr["ExpireCount"]);
            this.MoveUpCount = Convert.ToInt32((dr["MoveUpCount"] is DBNull || dr["MoveUpCount"] == null) ? 0 : dr["MoveUpCount"]);
            this.ReviewCount = Convert.ToInt32((dr["ReviewCount"] is DBNull || dr["ReviewCount"] == null) ? 0 : dr["ReviewCount"]);
            this.IsHaveError = false;
        }

        // Token: 0x17000033 RID: 51
        // (get) Token: 0x06000078 RID: 120 RVA: 0x00002C01 File Offset: 0x00000E01
        // (set) Token: 0x06000079 RID: 121 RVA: 0x00002C09 File Offset: 0x00000E09
        public int ApproveCount
        {
            get;
            set;
        }

        // Token: 0x17000034 RID: 52
        // (get) Token: 0x0600007A RID: 122 RVA: 0x00002C12 File Offset: 0x00000E12
        // (set) Token: 0x0600007B RID: 123 RVA: 0x00002C1A File Offset: 0x00000E1A
        public int RejectCount
        {
            get;
            set;
        }

        // Token: 0x17000035 RID: 53
        // (get) Token: 0x0600007C RID: 124 RVA: 0x00002C23 File Offset: 0x00000E23
        // (set) Token: 0x0600007D RID: 125 RVA: 0x00002C2B File Offset: 0x00000E2B
        public int RequestCount
        {
            get;
            set;
        }

        // Token: 0x17000036 RID: 54
        // (get) Token: 0x0600007E RID: 126 RVA: 0x00002C34 File Offset: 0x00000E34
        // (set) Token: 0x0600007F RID: 127 RVA: 0x00002C3C File Offset: 0x00000E3C
        public int CancelCount
        {
            get;
            set;
        }

        // Token: 0x17000037 RID: 55
        // (get) Token: 0x06000080 RID: 128 RVA: 0x00002C45 File Offset: 0x00000E45
        // (set) Token: 0x06000081 RID: 129 RVA: 0x00002C4D File Offset: 0x00000E4D
        public int ExpireCount
        {
            get;
            set;
        }

        // Token: 0x17000038 RID: 56
        // (get) Token: 0x06000082 RID: 130 RVA: 0x00002C56 File Offset: 0x00000E56
        // (set) Token: 0x06000083 RID: 131 RVA: 0x00002C5E File Offset: 0x00000E5E
        public int MoveUpCount
        {
            get;
            set;
        }

        // Token: 0x17000039 RID: 57
        // (get) Token: 0x06000084 RID: 132 RVA: 0x00002C67 File Offset: 0x00000E67
        // (set) Token: 0x06000085 RID: 133 RVA: 0x00002C6F File Offset: 0x00000E6F
        public int ReviewCount
        {
            get;
            set;
        }

        // Token: 0x1700003A RID: 58
        // (get) Token: 0x06000086 RID: 134 RVA: 0x00002C78 File Offset: 0x00000E78
        // (set) Token: 0x06000087 RID: 135 RVA: 0x00002C80 File Offset: 0x00000E80
        public bool IsHaveError
        {
            get;
            set;
        }
    }
    // Token: 0x02000007 RID: 7
    public class BaseRow : ICloneable
    {
        // Token: 0x1700000D RID: 13
        // (get) Token: 0x06000021 RID: 33 RVA: 0x00002234 File Offset: 0x00000434
        // (set) Token: 0x06000022 RID: 34 RVA: 0x0000223C File Offset: 0x0000043C
        [Conversion(DataTableConversion = true, AllowDbNull = false)]
        public int IsDell
        {
            get
            {
                return this._isDell;
            }
            set
            {
                this._isDell = value;
            }
        }

        // Token: 0x06000023 RID: 35 RVA: 0x00002245 File Offset: 0x00000445
        object ICloneable.Clone()
        {
            return base.MemberwiseClone();
        }

        // Token: 0x06000024 RID: 36 RVA: 0x0000224D File Offset: 0x0000044D
        public BaseRow Clone()
        {
            return (BaseRow)base.MemberwiseClone();
        }

        // Token: 0x0400002F RID: 47
        private int _isDell = 2;
    }
    // Token: 0x0200000F RID: 15
    [AttributeUsage(AttributeTargets.Property)]
    public class ConversionAttribute : Attribute
    {
        // Token: 0x17000043 RID: 67
        // (get) Token: 0x06000098 RID: 152 RVA: 0x00002D0D File Offset: 0x00000F0D
        // (set) Token: 0x06000099 RID: 153 RVA: 0x00002D15 File Offset: 0x00000F15
        public bool DataTableConversion
        {
            get;
            set;
        }

        // Token: 0x17000044 RID: 68
        // (get) Token: 0x0600009A RID: 154 RVA: 0x00002D1E File Offset: 0x00000F1E
        // (set) Token: 0x0600009B RID: 155 RVA: 0x00002D26 File Offset: 0x00000F26
        public bool AllowDbNull
        {
            get;
            set;
        }

        // Token: 0x17000045 RID: 69
        // (get) Token: 0x0600009C RID: 156 RVA: 0x00002D2F File Offset: 0x00000F2F
        // (set) Token: 0x0600009D RID: 157 RVA: 0x00002D37 File Offset: 0x00000F37
        public bool KeyField
        {
            get;
            set;
        }
    }
}
