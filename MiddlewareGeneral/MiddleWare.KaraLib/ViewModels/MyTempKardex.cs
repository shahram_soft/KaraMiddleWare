﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiddleWare.KaraLib.ViewModels
{
   public class MyTempKardex
    {
        // Token: 0x17000012 RID: 18
        // (get) Token: 0x06000030 RID: 48 RVA: 0x00002378 File Offset: 0x00000578
        // (set) Token: 0x06000031 RID: 49 RVA: 0x00002380 File Offset: 0x00000580
        public long? Id
        {
            get;
            set;
        }

        // Token: 0x17000013 RID: 19
        // (get) Token: 0x06000032 RID: 50 RVA: 0x00002389 File Offset: 0x00000589
        // (set) Token: 0x06000033 RID: 51 RVA: 0x00002391 File Offset: 0x00000591
        public double? SecNo
        {
            get;
            set;
        }

        // Token: 0x17000014 RID: 20
        // (get) Token: 0x06000034 RID: 52 RVA: 0x0000239A File Offset: 0x0000059A
        // (set) Token: 0x06000035 RID: 53 RVA: 0x000023A2 File Offset: 0x000005A2
        public double? EmpNo
        {
            get;
            set;
        }

        // Token: 0x17000015 RID: 21
        // (get) Token: 0x06000036 RID: 54 RVA: 0x000023AB File Offset: 0x000005AB
        // (set) Token: 0x06000037 RID: 55 RVA: 0x000023B3 File Offset: 0x000005B3
        public double? PersNo
        {
            get;
            set;
        }

        // Token: 0x17000016 RID: 22
        // (get) Token: 0x06000038 RID: 56 RVA: 0x000023BC File Offset: 0x000005BC
        // (set) Token: 0x06000039 RID: 57 RVA: 0x000023C4 File Offset: 0x000005C4
        public double? Saved
        {
            get;
            set;
        }

        // Token: 0x17000017 RID: 23
        // (get) Token: 0x0600003A RID: 58 RVA: 0x000023CD File Offset: 0x000005CD
        // (set) Token: 0x0600003B RID: 59 RVA: 0x000023D5 File Offset: 0x000005D5
        public double? SDate
        {
            get;
            set;
        }

        // Token: 0x17000018 RID: 24
        // (get) Token: 0x0600003C RID: 60 RVA: 0x000023DE File Offset: 0x000005DE
        // (set) Token: 0x0600003D RID: 61 RVA: 0x000023E6 File Offset: 0x000005E6
        public double? EDate
        {
            get;
            set;
        }

        // Token: 0x17000019 RID: 25
        // (get) Token: 0x0600003E RID: 62 RVA: 0x000023EF File Offset: 0x000005EF
        // (set) Token: 0x0600003F RID: 63 RVA: 0x000023F7 File Offset: 0x000005F7
        public double? Duration
        {
            get;
            set;
        }

        // Token: 0x1700001A RID: 26
        // (get) Token: 0x06000040 RID: 64 RVA: 0x00002400 File Offset: 0x00000600
        // (set) Token: 0x06000041 RID: 65 RVA: 0x00002408 File Offset: 0x00000608
        public double? IsuDate
        {
            get;
            set;
        }

        // Token: 0x1700001B RID: 27
        // (get) Token: 0x06000042 RID: 66 RVA: 0x00002411 File Offset: 0x00000611
        // (set) Token: 0x06000043 RID: 67 RVA: 0x00002419 File Offset: 0x00000619
        public short? IncTyp
        {
            get;
            set;
        }

        // Token: 0x1700001C RID: 28
        // (get) Token: 0x06000044 RID: 68 RVA: 0x00002422 File Offset: 0x00000622
        // (set) Token: 0x06000045 RID: 69 RVA: 0x0000242A File Offset: 0x0000062A
        public string Status
        {
            get;
            set;
        }

        // Token: 0x1700001D RID: 29
        // (get) Token: 0x06000046 RID: 70 RVA: 0x00002433 File Offset: 0x00000633
        // (set) Token: 0x06000047 RID: 71 RVA: 0x0000243B File Offset: 0x0000063B
        public string Babat
        {
            get;
            set;
        }

        // Token: 0x1700001E RID: 30
        // (get) Token: 0x06000048 RID: 72 RVA: 0x00002444 File Offset: 0x00000644
        // (set) Token: 0x06000049 RID: 73 RVA: 0x0000244C File Offset: 0x0000064C
        public bool? DelFlag
        {
            get;
            set;
        }

        // Token: 0x1700001F RID: 31
        // (get) Token: 0x0600004A RID: 74 RVA: 0x00002455 File Offset: 0x00000655
        // (set) Token: 0x0600004B RID: 75 RVA: 0x0000245D File Offset: 0x0000065D
        public double? Remtime
        {
            get;
            set;
        }

        // Token: 0x17000020 RID: 32
        // (get) Token: 0x0600004C RID: 76 RVA: 0x00002466 File Offset: 0x00000666
        // (set) Token: 0x0600004D RID: 77 RVA: 0x0000246E File Offset: 0x0000066E
        public double? Remtime2
        {
            get;
            set;
        }

        // Token: 0x17000021 RID: 33
        // (get) Token: 0x0600004E RID: 78 RVA: 0x00002477 File Offset: 0x00000677
        // (set) Token: 0x0600004F RID: 79 RVA: 0x0000247F File Offset: 0x0000067F
        public double? Inc
        {
            get;
            set;
        }

        // Token: 0x17000022 RID: 34
        // (get) Token: 0x06000050 RID: 80 RVA: 0x00002488 File Offset: 0x00000688
        // (set) Token: 0x06000051 RID: 81 RVA: 0x00002490 File Offset: 0x00000690
        public double? Dec
        {
            get;
            set;
        }

        // Token: 0x17000023 RID: 35
        // (get) Token: 0x06000052 RID: 82 RVA: 0x00002499 File Offset: 0x00000699
        // (set) Token: 0x06000053 RID: 83 RVA: 0x000024A1 File Offset: 0x000006A1
        public string FullName
        {
            get;
            set;
        }

        // Token: 0x17000024 RID: 36
        // (get) Token: 0x06000054 RID: 84 RVA: 0x000024AA File Offset: 0x000006AA
        // (set) Token: 0x06000055 RID: 85 RVA: 0x000024B2 File Offset: 0x000006B2
        public string RemTimestr
        {
            get;
            set;
        }

        // Token: 0x17000025 RID: 37
        // (get) Token: 0x06000056 RID: 86 RVA: 0x000024BB File Offset: 0x000006BB
        // (set) Token: 0x06000057 RID: 87 RVA: 0x000024C3 File Offset: 0x000006C3
        public string Family
        {
            get;
            set;
        }

        // Token: 0x17000026 RID: 38
        // (get) Token: 0x06000058 RID: 88 RVA: 0x000024CC File Offset: 0x000006CC
        // (set) Token: 0x06000059 RID: 89 RVA: 0x000024D4 File Offset: 0x000006D4
        public string Name
        {
            get;
            set;
        }

        // Token: 0x0600005A RID: 90 RVA: 0x000024E0 File Offset: 0x000006E0
        public MyTempKardex(long? id, double? empNo, double? saved, double? sDate, double? eDate, double? duration, double? remtime, double? iSuDate, short? incTyp, string status, string babat, bool? delFlag)
        {
            this.Id = id;
            this.EmpNo = empNo;
            this.Saved = saved;
            this.SDate = sDate;
            this.EDate = eDate;
            this.Duration = duration;
            this.Remtime = remtime;
            this.IsuDate = iSuDate;
            this.IncTyp = incTyp;
            this.Status = status;
            this.Babat = babat;
            this.DelFlag = delFlag;
        }
    }
}
