﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiddleWare.KaraLib.ViewModels
{
  public  class DayRequestInfoAndGeneralInfo
    {
      public string EndDayNames { get; set; }
      public string DayStatusName { get; set; }
      public DateTime DateOfRequest { get; set; }
    }
}
