﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileMiddleWare.Manager
{
    using MobileMiddleWare.Models;
    using MobileMiddleWare.Repository;

    using Newtonsoft.Json.Linq;

    public class CallingAPIService
    {
        public string CallingAPIServiceRest(string jsoninput, string tagname, string resulttagname, List<MobileGatewayWebServicesParameters> paramlist, ref ResultViewModel resultcommand, bool hasparam)
        {
            string apiResult = hasparam ? this.ServiceCreatorAndCallerWithparamsfromjsonAPI(jsoninput, paramlist, tagname) : this.ServiceCreatorAndCallerWithoutparametersAPI(tagname);

           

         //   var data = JObject.Parse(apiservice);
            return "";
        }

        public string ServiceCreatorAndCallerWithparamsfromjsonAPI(string json, List<MobileGatewayWebServicesParameters> neededparams, string servicetagname)
        {
            var servicecalling = new ExternalServiceCallerForRest();
            var repositoryGatewayCompany = new GatewayCompanyRepository();
            var getwaycompany = repositoryGatewayCompany.GetGatewayCompany(new Guid("57d4d22b-0a35-4d62-a075-85062681456a"));

            var apiservice = servicecalling.CallingMethod(
                getwaycompany.Url,
                "",//parameter
                "Login",
                "Module.TA.Core",
                "Post",
                "",
                "MobileCID",
                "TW9iaWxlQ1M");
            return "";
        }
        public string ServiceCreatorAndCallerWithoutparametersAPI(string servicetagname)
        {
            var servicecalling = new ExternalServiceCallerForRest();
            var repositoryGatewayCompany = new GatewayCompanyRepository();
            var getwaycompany = repositoryGatewayCompany.GetGatewayCompany(new Guid("57d4d22b-0a35-4d62-a075-85062681456a"));
            
            var apiservice = servicecalling.CallingMethod(
                getwaycompany.Url,
                "",//parameter
                "Login",
                "Module.TA.Core",
                "Post",
                "",
                "MobileCID",
                "TW9iaWxlQ1M");
            var data = JObject.Parse(apiservice);
            var messagedata = data["Message"].ToString();
            return "";
        }
    }
}