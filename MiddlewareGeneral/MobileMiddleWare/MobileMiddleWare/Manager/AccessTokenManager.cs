﻿namespace MobileMiddleWare.Manager
{
    using System;
    using System.Net;
    using System.Net.Http;
    using System.Security.Cryptography;
    using System.Text;
    using System.Web;

    using MobileMiddleWare.Models;
    using MobileMiddleWare.Repository;

    using Newtonsoft.Json.Linq;

    /// <summary>
    /// The access token manager.
    /// </summary>
    public class AccessTokenManager
    {

        //============================تولید توکن نسخه جدید===========================
        /// <summary>
        /// The _alg.
        /// </summary>
        private const string _alg = "HmacSHA256";

        /// <summary>
        /// The _salt.
        /// </summary>
        private const string _salt = "rz8LuOtFBXphj9WQfvFh"; // Generated at https://www.random.org/strings

        /// <summary>
        /// The _expiration minutes.
        /// </summary>
        private const int _expirationMinutes = 10;

        /// <summary>
        /// The generate token.
        /// </summary>
        /// <param name="username">
        /// The username.
        /// </param>
        /// <param name="password">
        /// The password.
        /// </param>
        /// <param name="ip">
        /// The ip.
        /// </param>
        /// <param name="userAgent">
        /// The user agent.
        /// </param>
        /// <param name="ticks">
        /// The ticks.
        /// </param>
        /// <param name="datetime">
        /// The datetime.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GenerateToken(string username, string password, string ip, string userAgent, long ticks, string datetime)
        {
            datetime = datetime.Replace(':', '/');
            string hash = string.Join(":", new string[] { username, ip, userAgent, ticks.ToString(), datetime });
            string hashLeft = "";
            string hashRight = "";

            using (HMAC hmac = HMACSHA256.Create(_alg))
            {
                hmac.Key = Encoding.UTF8.GetBytes(GetHashedPassword(password));
                hmac.ComputeHash(Encoding.UTF8.GetBytes(hash));

                hashLeft = Convert.ToBase64String(hmac.Hash);
                hashRight = string.Join(":", new string[] { username, ticks.ToString(), datetime });
            }

            return Convert.ToBase64String(Encoding.UTF8.GetBytes(string.Join(":", hashLeft, hashRight)));
        }

        public string GetHashedPassword(string password)
        {
            string key = string.Join(":", new string[] { password, _salt });

            using (HMAC hmac = HMACSHA256.Create(_alg))
            {
                // Hash the key.
                hmac.Key = Encoding.UTF8.GetBytes(_salt);
                hmac.ComputeHash(Encoding.UTF8.GetBytes(key));

                return Convert.ToBase64String(hmac.Hash);
            }
        }


        public bool IsTokenValid(string token, string ip, string userAgent)
        {
            bool result = false;

            try
            {
                // Base64 decode the string, obtaining the token:username:timeStamp.
                string key = Encoding.UTF8.GetString(Convert.FromBase64String(token));

                // Split the parts.
                string[] parts = key.Split(new char[] { ':' });
                if (parts.Length == 4)
                {
                    // Get the hash message, username, and timestamp.
                    string hash = parts[0];
                    string username = parts[1];
                    long ticks = long.Parse(parts[2]);
                    string creatingtime = parts[3];
                    DateTime timeStamp = new DateTime(ticks);

                    // Ensure the timestamp is valid.
                    //bool expired = Math.Abs((DateTime.UtcNow - timeStamp).TotalMinutes) > _expirationMinutes;
                    //if (!expired)
                    //{
                    //
                    // Lookup the user's account from the db.
                    //
                    if (username == "sh")
                    {
                        string password = "ff";


                        // Hash the message with the key to generate a token.
                        string computedToken = GenerateToken(username, password, ip, userAgent, ticks, creatingtime);

                        // Compare the computed token with the one supplied and ensure they match.
                        result = (token == computedToken);
                    }
                    // }
                }
            }
            catch
            {
            }

            return result;
        }
        public string GetIP(HttpRequestBase request)
        {
            string ip = request.Headers["X-Forwarded-For"]; // AWS compatibility

            if (string.IsNullOrEmpty(ip))
            {
                ip = request.UserHostAddress;
            }

            return ip;
        }

        //========================پایان تولید توکن نسخه جدید==============================
        /// <summary>
        /// بررسی وجود و صحت کلاینت آی دی و کلاینت سیکرت
        /// </summary>
        /// <param name="clientId">کلاینت آی دی</param>
        /// <returns>
        /// یک مدل یا شی توکن کلاینت بر می گرداند
        /// </returns>
        public TokenClient GetClient(string clientId)
        {
            TokenClient client = null;
            try
            {
                var repositorytoken = new TokenClientRepository();
               return repositorytoken.GetTokenClient(clientId);
                //TokenClient client;
                //using (var session = UnitOfWorkService.GetUnitOfWork().GetSession())
                //{
                //    client = (session.Query<TokenClient>().FirstOrDefault(i => i.ClientId == clientId));
                //    return client;
                //}
            }
            catch (Exception)
            {
                // ReSharper disable once ObjectCreationAsStatement
                new HttpResponseMessage
                {
                    StatusCode = HttpStatusCode.Forbidden,
                    ReasonPhrase = "Access Token is Invalid/Expired"
                };
            }
            return client;

        }

        /// <summary>
        /// اضافه کردن توکن
        /// </summary>
        /// <param name="tokenManager">
        /// یک مدل یا شی توکن منیجر
        /// </param>
        /// <returns>
        /// توکن ایجاد شده را بر می گرداند
        /// </returns>
        public string AddToken(TokenManager tokenManager)
        {
            try
            {
                var repositorytoken = new TokenManagerRepository();
               repositorytoken.SaveTokenManager(tokenManager);
                  return tokenManager.Token;
                //using (var session = UnitOfWorkService.GetUnitOfWork().GetSession())
                //{
                //    using (var trx = session.BeginTransaction())
                //    {
                //        session.Save(tokenManager);
                //        trx.Commit();
                //    }

                //}
              
            }
            catch (Exception ex)
            {
               // LogsService.Error("Error in Add token", ex, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
                return "";
            }
        }

        /// <summary>
        /// بررسی وجود وتاریخ انقضا توکن  
        /// </summary>
        /// <param name="token">
        /// توکن
        /// </param>
        /// <returns>
        /// نتیجه بررسی و صحت سنجی توکن
        /// </returns>
        public bool GetToken(string token)
        {
            try
            {
                bool flag = false;
                TokenManager tokenManager;
                //using (var session = UnitOfWorkService.GetUnitOfWork().GetSession())
                //{
                //    tokenManager = (from t in session.Query<TokenManager>()
                //                    where (t.Token == token)
                //                    select t).FirstOrDefault();
                //}
                var repositorytoken = new TokenManagerRepository();
                tokenManager = repositorytoken.GetToken(token);
                if (tokenManager != null)
                {
                    var exp = tokenManager.ExpireDate;
                    var date = DateTime.UtcNow;
                    if (exp > date)
                    {
                        flag = true;
                    }
                    else
                    {
                        RemoveToken(token);
                    }

                }
                return flag;
            }
            catch (Exception ex)
            {
                //LogsService.Error("Error in Get token", ex, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
                return false;
            }
        }


        /// <summary>
        /// حذف توکن
        /// </summary>
        /// <param name="token">
        /// شی توکن
        /// </param>
        public void RemoveToken(string token)
        {
            try
            {
                var repositorytoken = new TokenManagerRepository();
                repositorytoken.DeleteTokenManger(token);
                //using (var session = UnitOfWorkService.GetUnitOfWork().GetSession())
                //{
                //    tokenManager = (from t in session.Query<TokenManager>()
                //                    where (t.Token == token)
                //                    select t).FirstOrDefault();
                //    using (var trx = session.BeginTransaction())
                //    {
                //        session.Delete(tokenManager);
                //        trx.Commit();
                //    }

                //}

            }
            catch (Exception ex)
            {
               // LogsService.Error("Error in Remove token", ex, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

            }
        }
        /// <summary>
        /// سازنده توکن
        /// </summary>
        /// <param name="jsoninput">اطلاعات شامل کلاینت آی در و کلاینت سیکرت</param>
        /// <returns>
        /// یک مدا یا شی توکن منیجر بر می گرداند که حاوی اطلاعات در مورد توکن است
        /// </returns>
        public TokenPackage TokenGenerator(string jsoninput)
        {
            try
            {
                var data = JObject.Parse(jsoninput);
                var clientId = data["ClientId"].ToString();
                var clientSecret = data["ClientSecret"].ToString();
                var mytokenmanager = new AccessTokenManager();
                var result = GenerateToken(clientId, clientSecret, "192.168.0.1", "agent", 250000, DateTime.Now.ToString());
                var token = result;
                var recivedtoken = new TokenPackage
                {
                    Token = token,
                    TokenCreateTime = DateTime.Now,
                    TokenExpireTime = DateTime.Now
                };
                var myclientinfo = mytokenmanager.GetClient(clientId);
                var tokendata = new TokenManager
                {
                    Id = new Guid(),
                    Token = token,
                    RefreshToken = token,
                    IssueDate = DateTime.Now,
                    ExpireDate = DateTime.Now.AddDays(2),
                    Scope = "demo-client-scope",
                    TokenClient = myclientinfo
                };
                mytokenmanager.AddToken(tokendata);
                return recivedtoken;

            }
            catch (Exception ex)
            {
                // LogsService.Error("Error in Generate Token", ex, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
                return new TokenPackage();
            }

        }

       
    }
}