﻿
namespace MobileMiddleWare.Manager
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Net;
    using System.Text;
    using MobileMiddleWare.Models;
    using MobileMiddleWare.ViewModel;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;

    public class ExternalServiceCallerForRest
    {
     

        /// <summary>
        /// متد صدا زدن لایه سرویس خارجی
        /// </summary>
        /// <param name="url">
        /// آدرس سایت اصلی
        /// </param>
        /// <param name="parameters">
        /// پارامتر های ارسالی به سرویس
        /// </param>
        /// <param name="call">
        /// نام سرویس
        /// </param>
        /// <param name="module">
        /// نام ماژول.
        /// </param>
        /// <param name="method">
        /// نام متد پروتکل
        /// </param>
        /// <param name="action">
        /// نام اکشن
        /// </param>
        /// <param name="clientid">
        /// کلاینت ایدی
        /// </param>
        /// <param name="clientsecret">
        /// رمز توکن
        /// </param>
        /// <returns>
        /// خروجی سرویس
        /// </returns>
        public string CallingMethod(string url, string parameters, string call, string module, string method, string action, string clientid, string clientsecret)
        {
            var token = GetToken(url, clientid, clientsecret);
            var externalsystemdata = new ExternalServiceViewModel();
            if (token != null)
            {
                var data = JObject.Parse(token);
                externalsystemdata.Token = data["Token"].ToString();
            }
            externalsystemdata.Call = call;
            externalsystemdata.Module = module;
            externalsystemdata.Method = method;
            externalsystemdata.ActionName = action;
            externalsystemdata.Parameter = parameters;
            var jsoninput = JsonConvert.SerializeObject(externalsystemdata);
            var urlwithparameters = url + "Module.Service/api/ServiceCaller?jsoninput=" + jsoninput;
            HttpWebRequest request;


            request =
                (HttpWebRequest)
                    WebRequest.Create(urlwithparameters);
            request.ContentLength = 20000;
            request.SendChunked = true;
            request.Method = "Post";
            request.Timeout = 2000000;

            var requestWriter = new StreamWriter(request.GetRequestStream(), Encoding.ASCII);
            using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
            {
                if (response != null)
                {
                    var stream = response.GetResponseStream();
                    if (stream != null)
                    {
                        var reader = new StreamReader(stream);
                        return reader.ReadToEnd();
                    }
                    return null;
                }
                return null;
            }
        }
        /// <summary>
        /// متد صدا زدن لایه سرویس خارجی
        /// </summary>
        /// <param name="url">
        /// آدرس سایت اصلی
        /// </param>
        /// <param name="clintid">
        /// The clintid.
        /// </param>
        /// <param name="clintsecret">
        /// The clintsecret.
        /// </param>
        /// <returns>
        /// خروجی سرویس
        /// </returns>
        public string GetToken(string url, string clintid, string clintsecret)
        {

            var urlwithparameters = url + "Module.Service/api/TokenGenerator?jsoninput= {'ClientId':'" + clintid + "','ClientSecret':'" + clintsecret + "'}";

            HttpWebRequest request;


            request =
                (HttpWebRequest)
                    WebRequest.Create(urlwithparameters);
            request.ContentLength = 20000;
            request.SendChunked = true;
            request.Method = "Post";
            request.Timeout = 2000000;

            var requestWriter = new StreamWriter(request.GetRequestStream(), Encoding.ASCII);
            using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
            {
                if (response != null)
                {
                    var stream = response.GetResponseStream();
                    if (stream != null)
                    {
                        var reader = new StreamReader(stream);
                        return reader.ReadToEnd();

                    }
                    return null;
                }
                return null;
            }
        }
        /// <summary>
        ///  فراخوانی سرویس ها  پست از بدنه درخواست جهت داده های زیاد
        /// </summary>
        /// <param name="jsoninput">رشته جیسان شده که شامل پارامتر های صدا زدن سرویس به همراه پارامتر های سرویس مورد نظر است</param>
        /// <returns>مقدار بازگشتی از سرویس</returns>
        public String RestCallFromBody(string url, string parameters, string call, string module, string method, string action, string clientid, string clientsecret)
        {
            try
            {
                var token = GetToken(url, clientid, clientsecret);
                var externalsystemdata = new ExternalServiceViewModel();
                if (token != null)
                {
                    var data = JObject.Parse(token);
                    externalsystemdata.Token = data["Token"].ToString();
                }

                //مقادیر کنترلی را از جیسون ارسال شده میخواند


                HttpWebRequest request;
                // بر اساس نوع متد درخواست مورد نظر را انتخاب میکند
                if (method != "Post")
                {
                    if (method != "")
                    {
                        externalsystemdata.Call = call;
                        externalsystemdata.Module = module;
                        externalsystemdata.Method = method;
                        externalsystemdata.ActionName = action;
                        externalsystemdata.Parameter = parameters;
                        var jsoninput = JsonConvert.SerializeObject(externalsystemdata);
                        var urlwithparameters = url + "Module.Service/api/CallingServiceBody?jsoninput=" + jsoninput;
                        request = (HttpWebRequest)WebRequest.Create(urlwithparameters);
                        request.ContentLength = 20000;
                        request.SendChunked = true;
                        request.Method = method;
                        request.Timeout = 2000000;
                        //پیدا کردن یوزر نیم و پسورد کاربر جهت ارسال کردنشان به سرویس

                        var requestWriter = new StreamWriter(request.GetRequestStream(), Encoding.ASCII);
                        using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                        {
                            if (response != null)
                            {
                                var stream = response.GetResponseStream();
                                if (stream != null)
                                {
                                    var reader = new StreamReader(stream);
                                    return reader.ReadToEnd();
                                }
                                return null;
                            }
                            return null;
                        }
                    }
                    return null;
                }
                //زمانی  که متد ارسال به سرویس در حالت پست باشد مراحل زیر اجرا میگردد
                else
                {
                    externalsystemdata.Call = call;
                    externalsystemdata.Module = module;
                    externalsystemdata.Method = method;
                    externalsystemdata.ActionName = action;
                    externalsystemdata.Parameter = parameters;
                    var jsoninput = JsonConvert.SerializeObject(externalsystemdata);
                    // var urlwithparameters = url + "Module.Service/api/CallingServiceBody?jsoninput=" + jsoninput;
                    string webapiUrl = url + "Module.Service/api/CallingServiceBody";
                    using (var client = new WebClient())
                    {
                        client.Headers.Add("Content-Type", "application/x-www-form-urlencoded");
                        //پیدا کردن یوزر نیم و پسورد کاربر جهت ارسال کردنشای به سرویس

                        var result = client.UploadString(webapiUrl, "POST", "=" + jsoninput);
                        return result;
                    }
                }

            }
            catch (Exception)
            {

                return null;
            }
        }
    }
}