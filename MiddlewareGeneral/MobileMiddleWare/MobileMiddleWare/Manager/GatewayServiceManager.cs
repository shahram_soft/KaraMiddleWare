﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileMiddleWare.Manager
{
    using System.IO;
    using System.Net;
    using System.Xml;

    

    using MobileMiddleWare.Models;
    using MobileMiddleWare.Repository;

    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;

    public class GatewayServiceManager
    {
        /// <summary>
        ///  متدي که سرويس هاي گيت وي را فراخواني ميکند
        /// </summary>
        /// <param name="jsoninput">ورودي  جي سون</param>
        /// <returns>رشته ي جيسون خروجي</returns>
        public string CallService(string jsoninput)
        {
            try
            {
                JObject data = JObject.Parse(jsoninput);
                var data1 = data["Parameter"].ToString();
                JObject data2 = JObject.Parse(data1);
                var method = data2["MethodName"].ToString();
                var resultmodel = new ResultViewModel();
                var resultService = this.CallingMobileFromDb(data1, method, ref resultmodel);

                return resultService;
            }
            catch (Exception)
            {
                return "{'Validate':'1'}";
            }
        }

        /// <summary>
        /// ساخت رشته XML از جسون دریافتی و اطلاعات بانک
        /// </summary>
        /// <param name="jsoninput">
        /// پارامتر وروردی سرویس از موبایل
        /// </param>
        /// <param name="command">
        /// دستور سرویس
        /// </param>
        /// <param name="resultcommand"> نتیجه عملیات</param>
        /// <returns>
        /// خروجی از سرویس
        /// </returns>
        public string CallingMobileFromDb(string jsoninput, string command, ref ResultViewModel resultcommand)
        {
            try
            {
                //using (var session = UnitOfWorkService.GetUnitOfWork().GetSession())
                //{
                    //var commandParts =
                    //    session.Query<MobileGatewayWebServices>().FirstOrDefault(x => x.Command == command);
                var repositoryMobileGateway = new MobileGatewayWebServicesRepository();
                var commandParts = repositoryMobileGateway.GetMobileGatewayWebServices(command);
                
                    if (commandParts != null)
                    {
                        var tagname = commandParts.ServiceTagName;
                        var resulttagname = commandParts.ResultTagName;

                        //var paramlist =
                        //    session.Query<MobileGatewayWebServicesParameters>()
                        //        .Where(x => x.WebService.Id == commandParts.Id)
                        //        .ToList();
                        var paramlist = repositoryMobileGateway.GetMobileGatewayWebServicesParameters(commandParts.Id);
                        
                        if (commandParts.ServiceType == "Soap")
                        {

                            var result = CallingSoapServicesFromJson(jsoninput, tagname, resulttagname, paramlist, ref resultcommand, bool.Parse(commandParts.HasInArg));
                            if (resultcommand.Validate)
                                return result;
                            var jsonmodel = JsonConvert.SerializeObject(resultcommand);
                            return jsonmodel;
                        }
                    }
                    else
                    {
                        resultcommand = new ResultViewModel { Validate = false, Message = "" };
                        var jsonresult = JsonConvert.SerializeObject(resultcommand);
                        return jsonresult;
                    }
               // }
            }
            catch (Exception)
            {
                resultcommand = new ResultViewModel { Validate = false, Message = "" };
                return JsonConvert.SerializeObject(resultcommand);
            }
            return "";
        }

        /// <summary>
        /// صدا زدن سرویس های Soap
        /// </summary>
        /// <param name="jsoninput">
        /// پارامتر ورودی از سرویس
        /// </param>
        /// <param name="tagname">
        /// نام تگ سرویس
        /// </param>
        /// <param name="resulttagname">
        /// نام تگ نتیجه
        /// </param>
        /// <param name="paramlist">
        /// لیست پارامترهای سرویس
        /// </param>
        /// <param name="resultcommand">
        /// نتیجه اجرای سرویس
        /// </param>
        /// <param name="hasparam">
        /// دارای پارامتر ورودی هست یا خیر
        /// </param>
        /// <returns>
        /// نتیجه بدست امده از سرویس
        /// </returns>
        public string CallingSoapServicesFromJson(string jsoninput, string tagname, string resulttagname, List<MobileGatewayWebServicesParameters> paramlist, ref ResultViewModel resultcommand, bool hasparam)
        {
            try
            {
                string soapResult = hasparam ? this.ServiceCreatorAndCallerwithparamsfromjson(jsoninput, paramlist, tagname) : this.ServiceCreatorAndCallerwithoutparameters(tagname);
                if (soapResult != "")
                {
                    soapResult = soapResult.Replace("&lt;", "<");
                    soapResult = soapResult.Replace("&gt;", ">");

                    string strStart = "<" + resulttagname + ">";
                    string strEnd = "</" + resulttagname + ">";
                    string result = soapResult.Substring(soapResult.IndexOf(strStart, StringComparison.Ordinal), soapResult.Length - soapResult.IndexOf(strStart, StringComparison.Ordinal));
                    string xmlresult = result.Substring(0, result.IndexOf(strEnd, StringComparison.Ordinal) + strEnd.Length);
                    var doc = new XmlDocument();

                    doc.LoadXml(xmlresult);
                    resultcommand.Validate = true;
                    return JsonConvert.SerializeXmlNode(doc);

                }
            }
            catch (Exception)
            {

                resultcommand = new ResultViewModel { Validate = false, Message = "" };
                return "";
            }
            resultcommand = new ResultViewModel { Validate = false, Message = "" };
            return "";
        }

        /// <summary>
        /// صدا زدن سرویس از پارامتر های جیسون
        /// </summary>
        /// <param name="json">
        /// پارامتر جیسون موبایل
        /// </param>
        /// <param name="neededparams">
        /// پارامتر های مورد نیاز
        /// </param>
        /// <param name="servicetagname">
        /// تگ صدا زدن سرویس
        /// </param>
        /// <returns>
        /// نتیجه بازگشتی از سرویس
        /// </returns>
        public string ServiceCreatorAndCallerwithparamsfromjson(string json, List<MobileGatewayWebServicesParameters> neededparams, string servicetagname)
        {

            var request = CreateWebRequest(new Guid("48fc0f43-6468-4993-938a-83db0897b3b4"));
            var stringparamsand = "<" + servicetagname + " xmlns=\"http://tempuri.org/\">";
            JObject datafrommobile = JObject.Parse(json);
            foreach (var mobileGatewayWebServicesParameterse in neededparams)
            {
                var paramname = mobileGatewayWebServicesParameterse.ParameterName;
                var paramvaluefrommobile = datafrommobile[paramname].ToString();
                var tagstring = "<" + paramname + ">" + paramvaluefrommobile + "</" + paramname + ">";
                stringparamsand = stringparamsand + tagstring;
            }
            stringparamsand = stringparamsand + "</" + servicetagname + ">";
            var soapEnvelopeXml = new XmlDocument();
            var xmlmydoc = @"<?xml version=""1.0"" encoding=""utf-8""?>
                <soap:Envelope xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">
                <soap:Body>
                   " + stringparamsand + @"
                </soap:Body>
                </soap:Envelope>";
            soapEnvelopeXml.LoadXml(xmlmydoc);
            using (Stream stream = request.GetRequestStream())
            {
                soapEnvelopeXml.Save(stream);
            }
            using (WebResponse response = request.GetResponse())
            {
                var responseStream = response.GetResponseStream();
                if (responseStream != null)
                {
                    using (var rd = new StreamReader(responseStream))
                    {
                        string soapResult = rd.ReadToEnd();
                        return soapResult;
                    }
                }
            }
            return "";
        }


        /// <summary>
        /// صدا زدن سرویس بدون پارامتر ورودی
        /// </summary>
        /// <param name="servicetagname">
        /// نام تگ سرویس
        /// </param>
        /// <returns>
        /// نتیجه بازگشتی از سرویس
        /// </returns>
        public string ServiceCreatorAndCallerwithoutparameters(string servicetagname)
        {
            var request = CreateWebRequest(new Guid("48fc0f43-6468-4993-938a-83db0897b3b4"));
            var soapEnvelopeXml = new XmlDocument();
            var stringparamsand = "<" + servicetagname + " xmlns=\"http://tempuri.org/\"/>";
            var xmldata = (@"<?xml version=""1.0"" encoding=""utf-8""?>
                <soap:Envelope xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">
                <soap:Body>
                   " + stringparamsand + @"
                       
                   
                </soap:Body>
                </soap:Envelope>");
            soapEnvelopeXml.LoadXml(xmldata);
            using (Stream stream = request.GetRequestStream())
            {
                soapEnvelopeXml.Save(stream);
            }
            using (WebResponse response = request.GetResponse())
            {
                var responseStream = response.GetResponseStream();
                if (responseStream != null)
                {
                    using (var rd = new StreamReader(responseStream))
                    {
                        string soapResult = rd.ReadToEnd();
                        return soapResult;
                    }
                }
            }
            return "";
        }


        /// <summary>
        /// فراخواني يک وب متد بصورت دايناميک
        /// </summary>
        /// <param name="companyId">شناسه ي مشتري</param>
        /// <returns>ارسال درخواست توليد شده</returns>
        public HttpWebRequest CreateWebRequest(Guid companyId)
        {
            var url = GetCompanyUrl(companyId);
            var webRequest = (HttpWebRequest)WebRequest.Create(url);
            webRequest.Headers.Add(@"SOAP:Action");
            webRequest.ContentType = "text/xml;charset=\"utf-8\"";
            webRequest.Accept = "text/xml";
            webRequest.Method = "POST";
            return webRequest;

        }

        /// <summary>
        /// دريافت آدرس سرويس هاي مشتري خاص
        /// </summary>
        /// <param name="companyId">شناسه ي مشتري</param>
        /// <returns>آدرس سرور سرويس ها</returns>
        public string GetCompanyUrl(Guid companyId)
        {
            var repositoryGatewayCompany = new GatewayCompanyRepository();
               var getwaycompany = repositoryGatewayCompany.GetGatewayCompany(companyId);
               if (getwaycompany != null)
            {
                return getwaycompany.Url;
            }
            
            return "";
        }
    }
}