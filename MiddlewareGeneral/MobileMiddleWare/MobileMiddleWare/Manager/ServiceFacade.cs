﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileMiddleWare.Manager
{
    using System.IO;
    using System.Net;
    using System.Text;

    using Newtonsoft.Json.Linq;

    public class ServiceFacade
    {
        ///// <summary>
        /////  فراخوانی سرویس ها 
        ///// </summary>
        ///// <param name="jsoninput">رشته جیسان شده که شامل پارامتر های صدا زدن سرویس به همراه پارامتر های سرویس مورد نظر است</param>
        ///// <returns>مقدار بازگشتی از سرویس</returns>
        //public String RestCall(string jsoninput)
        //{
        //    try
        //    {
        //        string serviceUrl = String.Empty;
        //        JObject data = JObject.Parse(jsoninput);
        //        //مقادیر کنترلی را از جیسون ارسال شده میخواند
        //        var method = data["Method"].ToString();
        //        string fullserviceUrl;
        //        try
        //        {
        //            fullserviceUrl = data["FullServiceUrl"].ToString();
        //        }
        //        catch (Exception)
        //        {

        //            fullserviceUrl = string.Empty;
        //        }

        //        if (fullserviceUrl.IsNotEmpty())
        //        {
        //            serviceUrl = fullserviceUrl;
        //        }
        //        else
        //        {
        //            var apiname = data["Call"].ToString();
        //            var module = data["Module"].ToString();
        //            string actionname;
        //            try
        //            {
        //                actionname = data["ActionName"].ToString();
        //            }
        //            catch (Exception)
        //            {

        //                actionname = string.Empty;
        //            }
        //            if (!actionname.IsNotEmpty())
        //            {
        //                serviceUrl = _serviceFacadeRequrment.FindUrl() + "/" + _serviceFacadeRequrment.FindTenanat() +
        //                             "/" +
        //                             module + "/api/" + apiname;
        //            }
        //            else
        //            {
        //                serviceUrl = _serviceFacadeRequrment.FindUrl() + "/" + _serviceFacadeRequrment.FindTenanat() +
        //                             "/" +
        //                             module + "/api/" + apiname + "/" + actionname;
        //            }
        //        }

        //        HttpWebRequest request;
        //        // بر اساس نوع متد درخواست مورد نظر را انتخاب میکند
        //        //  if (method != "Post")
        //        //if (method != "")
        //        //{
        //        request =
        //            (HttpWebRequest)
        //                WebRequest.Create(serviceUrl + "?jsoninput=" +
        //                                  jsoninput);
        //        request.ContentLength = 20000;
        //        request.SendChunked = true;
        //        request.Method = method;
        //        request.Timeout = 2000000;
        //        //پیدا کردن یوزر نیم و پسورد کاربر جهت ارسال کردنشان به سرویس
        //        string logineusername = _serviceFacadeRequrment.FindUsername();
        //        string logineduserpassword = _serviceFacadeRequrment.FindPassword();
        //        //اتمام مرحله پیدا کردن یوزر نیم و پسورد
        //        request.Credentials = new NetworkCredential(logineusername, logineduserpassword);
        //        var requestWriter = new StreamWriter(request.GetRequestStream(), Encoding.ASCII);
        //        using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
        //        {
        //            if (response != null)
        //            {
        //                var stream = response.GetResponseStream();
        //                if (stream != null)
        //                {
        //                    var reader = new StreamReader(stream);
        //                    return reader.ReadToEnd();
        //                }
        //                return null;
        //            }
        //            return null;
        //        }
                

        //    }
        //    catch (Exception)
        //    {

        //        throw;
        //    }
        //}
        ///// <summary>
        /////  فراخوانی سرویس ها  پست از بدنه درخواست جهت داده های زیاد
        ///// </summary>
        ///// <param name="jsoninput">رشته جیسان شده که شامل پارامتر های صدا زدن سرویس به همراه پارامتر های سرویس مورد نظر است</param>
        ///// <returns>مقدار بازگشتی از سرویس</returns>
        //public String RestCallFromBody(string jsoninput)
        //{
        //    try
        //    {
        //        string serviceUrl = String.Empty;
        //        JObject data = JObject.Parse(jsoninput);
        //        //مقادیر کنترلی را از جیسون ارسال شده میخواند
        //        var method = data["Method"].ToString();
        //        string fullserviceUrl;
        //        try
        //        {
        //            fullserviceUrl = data["FullServiceUrl"].ToString();
        //        }
        //        catch (Exception)
        //        {

        //            fullserviceUrl = string.Empty;
        //        }

        //        if (fullserviceUrl.IsNotEmpty())
        //        {
        //            serviceUrl = fullserviceUrl;
        //        }
        //        else
        //        {
        //            var apiname = data["Call"].ToString();
        //            var module = data["Module"].ToString();
        //            string actionname;
        //            try
        //            {
        //                actionname = data["ActionName"].ToString();
        //            }
        //            catch (Exception)
        //            {

        //                actionname = string.Empty;
        //            }
        //            if (!actionname.IsNotEmpty())
        //            {
        //                serviceUrl = _serviceFacadeRequrment.FindUrl() + "/" + _serviceFacadeRequrment.FindTenanat() +
        //                             "/" +
        //                             module + "/api/" + apiname;
        //            }
        //            else
        //            {
        //                serviceUrl = _serviceFacadeRequrment.FindUrl() + "/" + _serviceFacadeRequrment.FindTenanat() +
        //                             "/" +
        //                             module + "/api/" + apiname + "/" + actionname;
        //            }
        //        }

        //        HttpWebRequest request;
        //        // بر اساس نوع متد درخواست مورد نظر را انتخاب میکند
        //        if (method != "Post")
        //        {
        //            if (method != "")
        //            {
        //                request = (HttpWebRequest)WebRequest.Create(serviceUrl + "?jsoninput=" + jsoninput);
        //                request.ContentLength = 20000;
        //                request.SendChunked = true;
        //                request.Method = method;
        //                request.Timeout = 2000000;
        //                //پیدا کردن یوزر نیم و پسورد کاربر جهت ارسال کردنشان به سرویس
        //                string logineusername = _serviceFacadeRequrment.FindUsername();
        //                string logineduserpassword = _serviceFacadeRequrment.FindPassword();
        //                //اتمام مرحله پیدا کردن یوزر نیم و پسورد
        //                request.Credentials = new NetworkCredential(logineusername, logineduserpassword);
        //                var requestWriter = new StreamWriter(request.GetRequestStream(), Encoding.ASCII);
        //                using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
        //                {
        //                    if (response != null)
        //                    {
        //                        var stream = response.GetResponseStream();
        //                        if (stream != null)
        //                        {
        //                            var reader = new StreamReader(stream);
        //                            return reader.ReadToEnd();
        //                        }
        //                        return null;
        //                    }
        //                    return null;
        //                }
        //            }
        //            return null;
        //        }
        //        //زمانی  که متد ارسال به سرویس در حالت پست باشد مراحل زیر اجرا میگردد
        //        else
        //        {
        //            var apiname = data["Call"].ToString();
        //            var module = data["Module"].ToString();
        //            string webapiUrl = _serviceFacadeRequrment.FindUrl() + "/" + _serviceFacadeRequrment.FindTenanat()
        //                                + "/" + module + "/api/" + apiname;
        //            using (var client = new WebClient())
        //            {
        //                client.Headers.Add("Content-Type", "application/x-www-form-urlencoded");
        //                //پیدا کردن یوزر نیم و پسورد کاربر جهت ارسال کردنشای به سرویس
        //                string logineusername = _serviceFacadeRequrment.FindUsername();
        //                string logineduserpassword = _serviceFacadeRequrment.FindPassword();
        //                //اتمام مرحله پیدا کردن یوزر نیم و پسورد
        //                client.Credentials = new NetworkCredential(logineusername, logineduserpassword);
        //                var result = client.UploadString(webapiUrl, "POST", "=" + jsoninput);
        //                return result;
        //            }
        //        }

        //    }
        //    catch (Exception)
        //    {

        //        throw;
        //    }
        //}
    }
}