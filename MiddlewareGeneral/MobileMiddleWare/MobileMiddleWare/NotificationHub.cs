﻿using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MobileMiddleWare.Models;
using System.Threading.Tasks;
using MobileMiddleWare.Manager;
using System.Net.Http;
using System.Net;
using MobileMiddleWare.Repository;
using Newtonsoft.Json.Linq;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace MobileMiddleWare
{
    public class NotificationHub : Hub 
    {
        private readonly static ConnectionMapping _connections =
            new ConnectionMapping();
        public int UserID;

        public static int GetUsers()
        {
            return _connections.Count;
        }
        public void SendNotification(string who, Notification n)
        {
            foreach (var connectionId in _connections.GetConnections(who))
            {
                var a = Newtonsoft.Json.JsonConvert.SerializeObject(n);
                var context = GlobalHost.ConnectionManager.GetHubContext<NotificationHub>();
                context.Clients.Client(connectionId).showNotification(a);
                Task.Run(() => SetNotificationSent(n.ID, Convert.ToInt32(who)));
            }
        }
        public void SetNotificationSent(int ID,int SelectedUserID)
        {
            try
            {
                System.Threading.Thread.Sleep(5000);
                GatewayServiceManager _gateway = new GatewayServiceManager();
                string result = _gateway.CallService("{'Parameter':{'MethodName':'UpdateNotificationStatus','ID':'" + ID.ToString() + "'}}");
                JObject j = JObject.Parse(result);
                if(JObject.Parse(j["UpdateNotificationStatus_MobileResult"].ToString())["Validate"].ToString().ToLower() !="true")
                {
                    string result2= _gateway.CallService("{'Parameter':{'MethodName':'UpdateNotificationStatus','ID':'" + ID.ToString() + "'}}");
                }
                var constre = ConfigurationManager.ConnectionStrings["connectionStringGeneral"].ToString();
                var connection = new SqlConnection(constre);
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
                connection.Open();
                string sp = "SELECT * FROM PersonNotifications WHERE PersonID=@PersonID";
                SqlDataAdapter da = new SqlDataAdapter(sp, connection);
                da.SelectCommand.Parameters.AddWithValue("@PersonID", SelectedUserID);
                DataTable dt = new DataTable();
                da.Fill(dt);
                connection.Close();
                if (dt.Rows.Count >= 1)
                {
                    for (int i = 0; i <= dt.Rows.Count; i++)
                    {
                        if (Convert.ToInt32(dt.Rows[i]["HaveNotification"].ToString()) >= 1)
                        {
                            if (connection.State == ConnectionState.Open)
                            {
                                connection.Close();
                            }
                            sp = "update PersonNotifications set HaveNotification = 0 where NotificationID = @ID";
                            SqlCommand cmd = new SqlCommand(sp, connection);
                            cmd.Parameters.AddWithValue("@ID", dt.Rows[i]["NotificationID"].ToString());
                            connection.Open();
                            cmd.ExecuteNonQuery();
                            connection.Close();
                        }
                    }
                }
            }
            catch(Exception ex)
            {

            }
        }
        public bool IsUserOnline(string who)
        {
            if (_connections.GetConnections(who).Count() >= 1)
            {
                return true;
            }
            return false;
        }

        public List<Notification> GetNotification(string UserID)
        {
            try
            {
                var constre = ConfigurationManager.ConnectionStrings["connectionStringGeneral"].ToString();
                var connection = new SqlConnection(constre);
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
                connection.Open();
                string sp = "SELECT * FROM PersonNotifications WHERE PersonID=@PersonID";
                SqlDataAdapter da = new SqlDataAdapter(sp, connection);
                da.SelectCommand.Parameters.AddWithValue("@PersonID", UserID);
                DataTable dt = new DataTable();
                da.Fill(dt);
                connection.Close();
                if (dt.Rows.Count >= 1)
                {
                    GatewayServiceManager _gateway = new GatewayServiceManager();
                    string result = _gateway.CallService("{'Parameter':{'MethodName':'GetPersonNotification','OnlineUserID':" + UserID + "}}");
                    JObject j = JObject.Parse(result);

                    var x = Newtonsoft.Json.JsonConvert.DeserializeObject<List<PersonNotification>>(JObject.Parse(j["GetPersonNotification_MobileResult"].ToString())["Message"].ToString());
                    List<Notification> list = new List<Notification>();
                    foreach (var item in x.ToList())
                    {
                        Notification n = new Notification()
                        {
                            ID = item.ID,
                            content = item.Message.ToString(),
                            title = "همراه کسرا"
                        };
                        list.Add(n);
                    }
                    return list;
                }
                else
                {
                    return null;
                }
               
            }
            catch
            {
                return null;
            }
        }

        public override Task OnConnected()
        {
            UserID = Int32.Parse(Context.QueryString["userId"]);
          
            _connections.Add(UserID.ToString(), Context.ConnectionId);
            
            /// get this person notification and send for him is he has some
            List<Notification> list = GetNotification(UserID.ToString());
            if (list != null)
            {
                foreach (var item in list)
                {
                    SendNotification(UserID.ToString(), item);
                }
            }
            return base.OnConnected();
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            string name = Context.ConnectionId;
            _connections.Remove(UserID.ToString(), Context.ConnectionId);
            return base.OnDisconnected(stopCalled);
        }

        public override Task OnReconnected()
        {
            UserID = Int32.Parse(Context.QueryString["userId"]);
            if (!_connections.GetConnections(UserID.ToString()).Contains(Context.ConnectionId))
            {
                _connections.Add(UserID.ToString(), Context.ConnectionId);
            }
            /// get this person notification and send for him is he has some
            List<Notification> list = GetNotification(UserID.ToString());
            if (list != null)
            {
                foreach (var item in list)
                {
                    SendNotification(UserID.ToString(), item);
                }
            }
            return base.OnReconnected();
        }
        internal class PersonNotification
        {
            public int ID { get; set; }
            public int NID { get; set; }
            public Int64 FromPersonID { get; set; }
            public Int64 ToPersonID { get; set; }
            public string Message { get; set; }
        }
    }
}