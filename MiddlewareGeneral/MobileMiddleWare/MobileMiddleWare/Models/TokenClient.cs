﻿using System;


namespace MobileMiddleWare.Models
{
    public class TokenClient
    {
        /// <summary>
        /// شناسه
        /// </summary>
        public virtual Guid Id { get; set; }
        ///// <summary>
        ///// شناسه کاربر
        ///// </summary>
        //public virtual Users Users { get; set; }
        /// <summary>
        /// شناسه نرم افزار کلاینت
        /// </summary>
        public virtual string ClientId { get; set; }
        /// <summary>
        /// رمز نرم افزار کلاینت
        /// </summary>
        public virtual string ClientSecret { get; set; }
        /// <summary>
        /// یو آر آی درخواست کننده توکن
        /// </summary>
        public virtual string DefaultCallback { get; set; }
        /// <summary>
        /// نام تننت
        /// </summary>
        public virtual string Tenant { get; set; }
        /// <summary>
        /// تاریخ ایجاد
        /// </summary>
        public virtual DateTime CreateDate { get; set; }
    }
}