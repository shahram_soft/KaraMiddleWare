﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileMiddleWare.Models
{
    public class ResultViewModel
    {
        /// <summary>
        /// تایید عملیات
        /// </summary>
        public bool Validate { get; set; }

        /// <summary>
        /// پیغام مورد نظر
        /// </summary>
        public string Message { get; set; }
    }
}