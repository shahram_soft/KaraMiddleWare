﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileMiddleWare.Models
{
    public class MobileGatewayWebServices
    {
        /// <summary>
        /// ای دی سرویس
        /// </summary>
        public virtual Guid Id { get; set; }

        /// <summary>
        /// دستوری که از طرف موبایل می آید
        /// </summary>
        public virtual string Command { get; set; }

        /// <summary>
        /// ایا برای صدا زدن سرویس نیاز به پارامتر ورودی میباشد یا خیر
        /// </summary>
        public virtual string HasInArg { get; set; }

        /// <summary>
        /// نام سرویس جهت استفاده در تگ صدا زدن
        /// </summary>
        public virtual string ServiceTagName { get; set; }

        /// <summary>
        /// نام تگ برای بدست اوردن نتیجه سرویس
        /// </summary>
        public virtual string ResultTagName { get; set; }

        /// <summary>
        /// نوع سرویس شامل Soap وRESTوغیره
        /// </summary>
        public virtual string ServiceType { get; set; }
    }
}