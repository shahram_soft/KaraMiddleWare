﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileMiddleWare.Models
{
    public class Notification
    {
        public int ID;
        public string title;
        public string content;
        public static List<Notification> ls = new List<Notification>();
        public void AddNotification(int ID, string title , string content)
        {
            Notification n = new Notification
            {
                ID = ID,
                title = title,
                content = content
            };
            ls.Add(n);
        }
        public List<Notification> GetNotifications()
        {
            return ls.ToList();
        }
        public int RemoveNotification(int ID)
        {
            try{
                ls.Remove(ls.Where(x => x.ID == ID).SingleOrDefault());
                return 0 ;
            }
            catch{
                return 1;
            }
          
        }

    }
}