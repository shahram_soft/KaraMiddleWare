﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileMiddleWare.Models
{
    public class MobileGatewayWebServicesParameters
    {
        /// <summary>
        /// ایدی پارامتر
        /// </summary>
        public virtual Guid Id { get; set; }


        public virtual string WebServiceId { get; set; }
        /// <summary>
        /// وبسرویس پارامتر
        /// </summary>
        public virtual MobileGatewayWebServices WebService { get; set; }

        /// <summary>
        /// نام پاراکتر جهت استفاده در سازنده XML
        /// </summary>
        public virtual string ParameterName { get; set; }

        /// <summary>
        /// نوع پاراکمتر 
        /// </summary>
        public virtual string ParameterType { get; set; }
    }
}