﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileMiddleWare.Models
{
    public class TokenManager
    {
        // <summary>
        /// شناسه توکن
        /// </summary>
        public virtual Guid Id { get; set; }
        /// <summary>
        /// توکن
        /// </summary>
        public virtual string Token { get; set; }
        /// <summary>
        ///  رفرش توکن
        /// </summary>
        public virtual string RefreshToken { get; set; }
        /// <summary>
        /// تاریخ ایجاد توکن
        /// </summary>
        public virtual DateTime IssueDate { get; set; }
        /// <summary>
        /// تاریخ اعتبار توکن
        /// </summary>
        public virtual DateTime ExpireDate { get; set; }
        /// <summary>
        /// شناسه صاحب توکن
        /// </summary>
        public virtual TokenClient TokenClient { get; set; }
        /// <summary>
        /// محدوده قابل دسترسی
        /// </summary>
        public virtual string Scope { get; set; }
    }
}