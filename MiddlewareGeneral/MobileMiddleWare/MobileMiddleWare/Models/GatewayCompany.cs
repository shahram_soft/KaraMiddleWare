﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileMiddleWare.Models
{
    public class GatewayCompany
    {
        /// <summary>
        /// شناسه 
        /// </summary>
        public virtual Guid Id { get; set; }
        /// <summary>
        ///  نام
        /// </summary>
        public virtual string Name { get; set; }
        /// <summary>
        /// آدرس سرور سرویس ها
        /// </summary>
        public virtual string Url { get; set; }
    }
}