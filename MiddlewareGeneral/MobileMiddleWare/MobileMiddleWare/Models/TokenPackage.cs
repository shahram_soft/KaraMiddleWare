﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileMiddleWare.Models
{
    public class TokenPackage
    {
        /// <summary>
        /// توکن به صورت رشته
        /// </summary>
        public string Token { get; set; }

        /// <summary>
        /// تاریخ ایجاد توکن
        /// </summary>
        public DateTime TokenCreateTime { get; set; }

        /// <summary>
        /// تاریخ انقضا توکن
        /// </summary>
        public DateTime TokenExpireTime { get; set; }
    }
}