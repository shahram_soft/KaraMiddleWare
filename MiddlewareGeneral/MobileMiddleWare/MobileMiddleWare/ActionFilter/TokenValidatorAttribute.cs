﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace MiddleWareNew.ActionFilter
{
    using MobileMiddleWare.Manager;

    using Newtonsoft.Json.Linq;
    
    /// 
    /// </summary>
    public class TokenValidatorAttribute : ActionFilterAttribute
    {
        /// <summary>
        /// متد زمان اتورایز
        /// </summary>
        /// <param name="actionContext">
        /// محتوای درخواست
        /// </param>
        public override void OnActionExecuting(HttpActionContext actionContext)
        {


            //var some_value = actionContext.ActionArguments["jsoninput"];
            var jsoninput = actionContext.ActionArguments["jsoninput"].ToString();
            var data = JObject.Parse(jsoninput);
            var token = data["Token"].ToString();
            var accessTokenManager = new AccessTokenManager();
            // اگر توکن در خواست با مقدار موجود در دیتابیس مطابقت نداشت یا تاریخ مصرف آن به پایان رسیده بود از دسترسی به ای پی آی جلوگیری می شود
            if (accessTokenManager.GetToken(token) == false)
            {
                actionContext.Response = new HttpResponseMessage
                {
                    StatusCode = HttpStatusCode.Forbidden,
                    ReasonPhrase = "Access Token is Invalid/Expired"
                };
            }
            else
            {
                base.OnActionExecuting(actionContext);
            }
        }
    }
}