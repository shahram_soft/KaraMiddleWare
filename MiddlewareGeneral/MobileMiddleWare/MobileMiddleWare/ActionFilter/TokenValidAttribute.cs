﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MiddleWareNew.ActionFilter
{
    using System.Net;
    using System.Net.Http;
    using System.Web.Http.Controllers;
    using System.Web.Http.Filters;
    using System.Web.Mvc;

    using MobileMiddleWare.Manager;

    using Newtonsoft.Json.Linq;

    public class TokenValidAttribute : AuthorizationFilterAttribute
    {
        [ValidateInput(false)]
        public override void OnAuthorization(HttpActionContext actionContext)
        {
            var jsoninput = HttpContext.Current.Request.QueryString["jsoninput"];
            var data = JObject.Parse(jsoninput);
            var token = data["Token"].ToString();
            var accessTokenManager = new AccessTokenManager();
            // اگر توکن در خواست با مقدار موجود در دیتابیس مطابقت نداشت یا تاریخ مصرف آن به پایان رسیده بود از دسترسی به ای پی آی جلوگیری می شود
            if (accessTokenManager.GetToken(token) == false)
            {
                actionContext.Response = new HttpResponseMessage
                {
                    StatusCode = HttpStatusCode.Forbidden,
                    ReasonPhrase = "Access Token is Invalid/Expired"
                };
            }
            else
            {
                base.OnAuthorization(actionContext);
            }
        }
    }
}