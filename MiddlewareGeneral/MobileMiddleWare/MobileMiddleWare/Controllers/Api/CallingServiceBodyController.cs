﻿
using System.Web.Http;

namespace MobileMiddleWare.Controllers.Api
{
    /// <summary>
    /// The calling service body controller.
    /// </summary>
    public class CallingServiceBodyController : ApiController
    {
        /// <summary>
        ///  متد آزمایشی که درخواست get 
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string Get()
        {
            return "ok";
        }

        // این اتریبیوت صحت توکن را بررسی می کند
        /// <summary>
        /// ابتدا بررسی می کند که ایا توکن معتبر است یا نه
        /// در صورت معتبر بودن توکن اجاز می دهد که از سرویس درخواست شده استفاده کند
        /// در صورت نا معتبر بودن توکن پیغام مناسب به کلاینت بر می گرداند
        /// </summary>
        /// <param name="jsoninput">
        /// پارامتر ورودی حاوی اطلاعات مورد نیاز برای استفاده از سرویس
        /// </param>
        /// <returns>
        ///  نتیجه سرویس را را در قالب جیسون به کلاینت بر می گرداند
        /// </returns>
        //[TokenValidator]
        //public HttpResponseMessage Post([FromBody] string jsoninput)
        //{
            //JObject.Parse(jsoninput);
            //var serviceFacade = new ServiceFacade();
            //var result = serviceFacade.RestCallFromBody(jsoninput);
            //var response = this.Request.CreateResponse(HttpStatusCode.OK);
            //response.Content = new StringContent(result, Encoding.UTF8, "application/json");
            //return response;
        //}
    }
}
