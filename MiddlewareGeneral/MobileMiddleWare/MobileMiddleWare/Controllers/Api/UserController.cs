﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MobileMiddleWare.Controllers.Api
{
    public class UserController : ApiController
    {
        [HttpGet]
        public int Users()
        {
            return NotificationHub.GetUsers();
        }
    }
}
