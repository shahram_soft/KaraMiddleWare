﻿using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MobileMiddleWare.Controllers.Api
{
    using System.Text;

    using MobileMiddleWare.Manager;

    /// <summary>
    ///کنترلر فراخوانی گیت وی
    /// </summary>
    public class GatewayServiceController : ApiController
    {
        
        /// <summary>
        /// متد ایندکس
        /// </summary>
        /// <param name="jsonInput">
        /// The json input.
        ///پارامتر رشته ی جیسون 
        /// </param>
        /// <returns>
        /// پاسخ ریکوئست مورد نظر
        /// </returns>
        public HttpResponseMessage Post(string jsonInput)
        {
            var cm = new GatewayServiceManager();
            var result = cm.CallService(jsonInput);
            var response = this.Request.CreateResponse(HttpStatusCode.OK);
            response.Content = new StringContent(result, Encoding.UTF8, "application/json");
            return response;
        }
    }
}
