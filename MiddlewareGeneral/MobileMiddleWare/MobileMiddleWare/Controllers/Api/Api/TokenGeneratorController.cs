﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MiddleWareNew.Controllers.Api
{
    using MiddleWareNew.Manager;
    using MiddleWareNew.Models;

    public class TokenGeneratorController : ApiController
    {
        /// <summary>
        /// این متد اطلاعات کلاینت را در قالب یک شی جیسون استرینگ می گیرد
        /// و در صورت صحت اطلاعات برای آن توکن تولید می کند
        /// </summary>
        /// <param name="jsoninput">
        /// اطلاعات کلاینت را در قالب یک شی جیسون استرینگ
        /// </param>
        /// <returns>
        /// پکیجی حاوی اطلاعات توکن ایجاد شده بر می گرداند
        /// </returns>
        public TokenPackage Post(string jsoninput)
        {
            var accessTokenManager = new AccessTokenManager();

            return accessTokenManager.TokenGenerator(jsoninput);
        }
    }
}
