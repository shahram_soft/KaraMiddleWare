﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MiddleWareNew.Controllers.Api
{
    using System.Text;

    using MiddleWareNew.ActionFilter;
    using MiddleWareNew.Manager;

    using Newtonsoft.Json.Linq;

    public class ServiceCallerController : ApiController
    {
        /// <summary>
        /// متد آزمایشی که درخواست get را پاسخ می دهد
        /// </summary>
        /// <returns>
        /// مقدار Ok را بر می گرداند
        /// </returns>
        public string Get()
        {
            return "ok";
        }

        // این اتریبیوت صحت توکن را بررسی می کند
        /// <summary>
        /// ابتدا بررسی می کند که ایا توکن معتبر است یا نه
        /// در صورت معتبر بودن توکن اجاز می دهد که از سرویس درخواست شده استفاده کند
        /// در صورت نا معتبر بودن توکن پیغام مناسب به کلاینت بر می گرداند
        /// </summary>
        /// <param name="jsoninput">
        /// پارامتر ورودی حاوی اطلاعات مورد نیاز برای استفاده از سرویس
        /// </param>
        /// <returns>
        ///  نتیجه سرویس را را در قالب جیسون به کلاینت بر می گرداند
        /// </returns>
        [TokenValid]
        public HttpResponseMessage Post(string jsoninput)
        {
            //JObject.Parse(jsoninput);
            //var serviceFacade = new ServiceFacade();
            //var result = serviceFacade.RestCall(jsoninput);
            //var response = this.Request.CreateResponse(HttpStatusCode.OK);
            //response.Content = new StringContent(result, Encoding.UTF8, "application/json");
            //return response;
            var cm = new GatewayServiceManager();
            var result = cm.CallService(jsoninput);
            var response = this.Request.CreateResponse(HttpStatusCode.OK);
            response.Content = new StringContent(result, Encoding.UTF8, "application/json");
            return response;
        }
    }
}
