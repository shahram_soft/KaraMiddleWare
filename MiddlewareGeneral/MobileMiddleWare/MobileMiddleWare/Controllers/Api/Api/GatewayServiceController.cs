﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MiddleWareNew.Controllers.Api
{
    using System.Text;

    using MiddleWareNew.Manager;

    public class GatewayServiceController : ApiController
    {
        //
        // کنترلر فراخوانی گیت وی

        /// <summary>
        /// متد ایندکس
        /// </summary>
        /// <param name="jsonInput">
        /// The json input.
        ///پارامتر رشته ی جیسون 
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public HttpResponseMessage Post(string jsonInput)
        {
            var cm = new GatewayServiceManager();
            var result = cm.CallService(jsonInput);
            var response = this.Request.CreateResponse(HttpStatusCode.OK);
            response.Content = new StringContent(result, Encoding.UTF8, "application/json");
            return response;
        }
    }
}
