﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Configuration;
using System.Data.SqlClient;
using System.Net.Http;
using System.IO;
using System.Net.Http.Headers;
using MobileMiddleWare.Models;


namespace MobileMiddleWare.Controllers.Api
{
    public class NotificationController : ApiController
    {

        NotificationHub notificationhub = new NotificationHub();
        [HttpPost]
        public string AddNotification(string jsoninput)
        {
            //{'ID':'123','NID':'321','FromPersonID':'12345' ,'ToPersonID': '54321','Message' :'Salam That is a Test'}
            try
            {
                JObject data = JObject.Parse(jsoninput);
                int ID = Convert.ToInt32(data["ID"].ToString());
                int NID = Convert.ToInt32(data["NID"].ToString());
                Int64 FromPersonID = Convert.ToInt64(data["FromPersonID"].ToString());
                Int64 ToPersonID = Convert.ToInt64(data["ToPersonID"].ToString());
                string Message = data["Message"].ToString();
                if (notificationhub.IsUserOnline(ToPersonID.ToString()))
                {
                    Notification n = new Notification()
                    {
                        ID = ID,
                        content = Message,
                        title = "همراه کسرا"
                    };
                    NotificationHub nb = new NotificationHub();
                    nb.SendNotification(ToPersonID.ToString(), n);
                }
                else{
                    var constre = ConfigurationManager.ConnectionStrings["connectionStringGeneral"].ToString();
                    var connection = new SqlConnection(constre);
                    if (connection.State == ConnectionState.Open)
                    {
                        connection.Close();
                    }
                    connection.Open();
                    string sp = "SELECT * FROM PersonNotifications WHERE PersonID=@PersonID";
                    SqlDataAdapter da = new SqlDataAdapter(sp, connection);
                    da.SelectCommand.Parameters.AddWithValue("@PersonID",ToPersonID);
                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    connection.Close();
                    if (dt.Rows.Count >= 1)
                    {
                        if (connection.State == ConnectionState.Open)
                        {
                            connection.Close();
                        }
                        sp = "update PersonNotifications set HaveNotification = 1 where NotificationID = @ID";
                        SqlCommand cmd = new SqlCommand(sp, connection);
                        cmd.Parameters.AddWithValue("@ID", dt.Rows[0]["NotificationID"].ToString());
                        connection.Open();
                        cmd.ExecuteNonQuery();
                        connection.Close();
                    }
                    else
                    {
                        if (connection.State == ConnectionState.Open)
                        {
                            connection.Close();
                        }
                        connection.Open();
                        sp = "insert into PersonNotifications  (NotificationID,PersonID,HaveNotification) values (@ID,@PersonID,@status)";
                        SqlCommand cmd = new SqlCommand(sp, connection);
                        cmd.Parameters.AddWithValue("@ID", Guid.NewGuid());
                        cmd.Parameters.AddWithValue("@PersonID", ToPersonID);
                        cmd.Parameters.AddWithValue("@status", 1);
                        cmd.ExecuteNonQuery();
                        connection.Close();
                    }
                }
                return " fs";                
            }
            catch (Exception ex)
            {
                return "Error";
            }
        } 
        [HttpGet]
        public HttpResponseMessage Send()
        {
            var path = "D:\\project\\MiddleWareNitification\\MobileMiddleWare\\MobileMiddleWare\\Views\\test.html";
            var response = new HttpResponseMessage();
            response.Content = new StringContent(File.ReadAllText(path));
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/html");
            return response;
        }
    }
}
