﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;
using MiddleWareNew.ActionFilter;
using Newtonsoft.Json.Linq;

namespace MobileMiddleWare.Controllers.Api
{
    public class ServiceCallerFileController : ApiController
    {
        public string Get()
        {
            return "Ok";
        }
        /// <summary>
        /// سرویس جهت ذخیره فایل
        /// </summary>
        /// <returns>شناسه ادرس</returns>
        [MobileMiddleWare.ActionFilter.TokenValidBody]
        public HttpResponseMessage Post()
        {
            var message = "";
            var response = this.Request.CreateResponse(HttpStatusCode.OK);
            try
            {
            var jsoninput = HttpContext.Current.Request.Form["jsoninput"];
            var data = JObject.Parse(jsoninput);
            var file = data["Files"].ToString();
            var onlineUser = data["OnlineUser"].ToString();
                var uniqNm = MakeUniqName();
            var fileName = onlineUser + "@" + uniqNm+".jpg";
            byte[] bytesFile = Convert.FromBase64String(file);
            File.WriteAllBytes(HttpContext.Current.Server.MapPath("~/App_Data") + "/" + fileName, bytesFile);
                var fileNameSendToMobile = onlineUser + "-" + uniqNm + ".jpg";
                message = "{\"FileName\":\""+ fileNameSendToMobile+ "\"}";
            }
            catch (Exception ex)
            {
                response = this.Request.CreateResponse(HttpStatusCode.InternalServerError);
                message =  "{\"FileName\":\"" + "با خطا" + "\"}"; 
            }
            
            response.Content = new StringContent(message, Encoding.UTF8, "application/json");
            return response;
        }
        public static string MakeUniqName()
        {
            return string.Concat(new string[]
            {
                DateTime.Now.Year.ToString(),
                DateTime.Now.Month.ToString(),
                DateTime.Now.Day.ToString(),
                DateTime.Now.Hour.ToString(),
                DateTime.Now.Minute.ToString(),
                DateTime.Now.Second.ToString(),
                DateTime.Now.Millisecond.ToString()
            });
        }
    }
}
