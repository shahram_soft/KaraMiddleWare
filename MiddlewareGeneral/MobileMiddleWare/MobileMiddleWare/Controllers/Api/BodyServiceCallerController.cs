﻿using MiddleWareNew.ActionFilter;
using MobileMiddleWare.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;

namespace MobileMiddleWare.Controllers.Api
{
    public class BodyServiceCallerController : ApiController
    {
        /// <summary>
        /// متد آزمایشی که درخواست get را پاسخ می دهد
        /// </summary>
        /// <returns>
        /// مقدار Ok را بر می گرداند
        /// </returns>
        public string Get()
        {
            return "ok";
        }

        /// این اتریبیوت صحت توکن را بررسی می کند
        /// <summary>
        /// ابتدا بررسی می کند که ایا توکن معتبر است یا نه
        /// در صورت معتبر بودن توکن اجاز می دهد که از سرویس درخواست شده استفاده کند
        /// در صورت نا معتبر بودن توکن پیغام مناسب به کلاینت بر می گرداند
        /// </summary>
        /// <param name="jsoninput">
        /// پارامتر ورودی حاوی اطلاعات مورد نیاز برای استفاده از سرویس
        /// </param>
        /// <returns>
        ///  نتیجه سرویس را را در قالب جیسون به کلاینت بر می گرداند
        /// </returns>
        [MobileMiddleWare.ActionFilter.TokenValidBody]
        public HttpResponseMessage Post()
        {
            var jsoninput = HttpContext.Current.Request.Form["jsoninput"];
            var cm = new GatewayServiceManager();
            var result = cm.CallService(jsoninput);
            var response = this.Request.CreateResponse(HttpStatusCode.OK);
            response.Content = new StringContent(result, Encoding.UTF8, "application/json");
            return response;
        }
    }
}
