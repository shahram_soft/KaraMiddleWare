﻿using System.Web.Http;

namespace MobileMiddleWare.Controllers.Api
{
    using MobileMiddleWare.Manager;
    using MobileMiddleWare.Models;

    /// <summary>
    /// The token generator controller.
    /// </summary>
    public class TokenGeneratorController : ApiController
    {
        /// <summary>
        /// این متد اطلاعات کلاینت را در قالب یک شی جیسون استرینگ می گیرد
        /// و در صورت صحت اطلاعات برای آن توکن تولید می کند
        /// </summary>
        /// <param name="jsoninput">
        /// اطلاعات کلاینت را در قالب یک شی جیسون استرینگ
        /// </param>
        /// <returns>
        /// پکیجی حاوی اطلاعات توکن ایجاد شده بر می گرداند
        /// </returns>
        public TokenPackage Post(string jsoninput)
        {
            var accessTokenManager = new AccessTokenManager();

            return accessTokenManager.TokenGenerator(jsoninput);
        }
    }
}
