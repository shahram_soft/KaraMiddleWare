﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Interface.ViewModel.General;
using Newtonsoft.Json.Linq;

namespace MobileMiddleWare.Controllers.Api
{
    public class DelPushnotficationOneSignalsController : ApiController
    {
        public string post(string jsoninput)
        {
            JObject data = JObject.Parse(jsoninput);
            string playerid = data["playerId"].ToString();
            string personid = data["personId"].ToString();
            try
            {
                var constre = ConfigurationManager.ConnectionStrings["connectionStringGeneral"].ToString();
                var connection = new SqlConnection(constre);
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
                connection.Open();
                string sp = "SELECT * FROM OneSignalPushNotfication WHERE PersonID=@PersonID";
                SqlDataAdapter da = new SqlDataAdapter(sp, connection);
                da.SelectCommand.Parameters.AddWithValue("@PersonID", personid);
                DataTable dt = new DataTable();
                da.Fill(dt);
                connection.Close();
                if (dt.Rows.Count >= 1)
                {
                    if (connection.State == ConnectionState.Open)
                    {
                        connection.Close();
                    }
                    sp = "Delete OneSignalPushNotfication where PlayerID=@PlayerID and PersonID=@PersonID ";
                    SqlCommand cmd = new SqlCommand(sp, connection);
                    cmd.Parameters.AddWithValue("@PlayerID", playerid);
                    cmd.Parameters.AddWithValue("@PersonID", personid);
                    connection.Open();
                    cmd.ExecuteNonQuery();
                    connection.Close();
                    return "all thing is ok";
                    return Newtonsoft.Json.JsonConvert.SerializeObject(new ResultViewModel { Validate = false, Message = "all thing is ok" });
                }
                else
                {
                    return Newtonsoft.Json.JsonConvert.SerializeObject(new ResultViewModel { Validate = false, Message = "There is no person " });

                }

            }
            catch (Exception ex)
            {
                return Newtonsoft.Json.JsonConvert.SerializeObject(new ResultViewModel { Validate = false, Message = ex.Message });
            }
        }
    }
}
