﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MobileMiddleWare.Models;
using Newtonsoft.Json.Linq;

namespace MobileMiddleWare.Controllers.Api
{
    public class PushnotficationOneSignalsController : ApiController
    {
        public string Post(string jsoninput)
        {
            JObject data = JObject.Parse(jsoninput);
            string playerid = data["playerId"].ToString();
            string personid = data["personId"].ToString();
            try
            {
                if (string.IsNullOrEmpty(playerid))
                {

                    var constre = ConfigurationManager.ConnectionStrings["connectionStringGeneral"].ToString();
                    var connection = new SqlConnection(constre);
                    if (connection.State == ConnectionState.Open)
                    {
                        connection.Close();
                    }
                    //connection.Open();
                    //string sp = "SELECT * FROM OneSignalPushNotfication WHERE PersonID=@PersonID";
                    //SqlDataAdapter da = new SqlDataAdapter(sp, connection);
                    //da.SelectCommand.Parameters.AddWithValue("@PersonID", personid);
                    //DataTable dt = new DataTable();
                    //da.Fill(dt);
                    //connection.Close();
                    //if (dt.Rows.Count >= 1)
                    //{
                    //    if (connection.State == ConnectionState.Open)
                    //    {
                    //        connection.Close();
                    //    }
                    //    sp = "update OneSignalPushNotfication set PlayerID=@PlayerID where PersonID=@PersonID";
                    //    SqlCommand cmd = new SqlCommand(sp, connection);
                    //    cmd.Parameters.AddWithValue("@PlayerID", playerid);
                    //    cmd.Parameters.AddWithValue("@PersonID", personid);
                    //    connection.Open();
                    //    cmd.ExecuteNonQuery();
                    //    connection.Close();
                    //}
                    //else
                    //{
                    if (connection.State == ConnectionState.Open)
                    {
                        connection.Close();
                    }
                    connection.Open();
                    string sp = "insert into OneSignalPushNotfication  (PersonID,PlayerID) values (@PersonID,@PlayerID)";
                    SqlCommand cmd = new SqlCommand(sp, connection);
                    cmd.Parameters.AddWithValue("@PlayerID", playerid);
                    cmd.Parameters.AddWithValue("@PersonID", personid);
                    cmd.ExecuteNonQuery();
                    connection.Close();
                }
                //}
                return Newtonsoft.Json.JsonConvert.SerializeObject(new ResultViewModel { Validate = true, Message = "all thing is ok" });

            }
            catch (Exception ex)
            {
                return Newtonsoft.Json.JsonConvert.SerializeObject(new ResultViewModel { Validate = false, Message = ex.Message });
            }
        }
    }
}
