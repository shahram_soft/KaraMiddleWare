﻿using System;


namespace MobileMiddleWare.Repository
{
    using System.Configuration;
    using System.Data;
    using System.Data.SqlClient;

    using MobileMiddleWare.Models;

    public class TokenManagerRepository
    {
        public void SaveTokenManager(TokenManager tokenManager)
        {
            var constre = ConfigurationManager.ConnectionStrings["connectionStringGeneral"].ToString();
            var connection = new SqlConnection(constre);

            if (connection.State == ConnectionState.Open)
            {
                connection.Close();
            }

            string query = "INSERT INTO TokenManager (Id,TokenClientId, Token,RefreshToken, IssueDate,ExpireDate,Scope)"
              + "VALUES(@Id,@TokenClientId,@Token, @RefreshToken, @IssueDate, @ExpireDate, @Scope);";
            
            connection.Open();
            var command = new SqlCommand(query, connection);
            command.Parameters.Add("@Id", SqlDbType.UniqueIdentifier).Value = Guid.NewGuid();
            command.Parameters.Add("@TokenClientId", SqlDbType.UniqueIdentifier).Value = tokenManager.TokenClient.Id;
            command.Parameters.Add("@Token", SqlDbType.VarChar, 2000).Value = tokenManager.Token;
            command.Parameters.Add("@RefreshToken", SqlDbType.VarChar, 2000).Value = tokenManager.RefreshToken;
            command.Parameters.Add("@IssueDate", SqlDbType.DateTime).Value = tokenManager.IssueDate;
            command.Parameters.Add("@ExpireDate", SqlDbType.DateTime).Value = tokenManager.ExpireDate;
            command.Parameters.Add("@Scope", SqlDbType.NVarChar , 255).Value = tokenManager.Scope;
           
            command.ExecuteNonQuery();
            connection.Close();
            connection.Dispose();
          
        }

        public void DeleteTokenManger(string token)
        {
            Guid Id;
            var constre = ConfigurationManager.ConnectionStrings["connectionStringGeneral"].ToString();
            var connection = new SqlConnection(constre);

            if (connection.State == ConnectionState.Open)
            {
                connection.Close();
            }
            using (var cmd = connection.CreateCommand())
            {

                connection.Open();
                cmd.CommandText = "SELECT * FROM TokenManager WHERE Token = @token";
                cmd.Parameters.AddWithValue("@token", token);
                using (var reader = cmd.ExecuteReader())
                {
                    Id = reader.GetGuid(reader.GetOrdinal("Id"));

                }
            }
            string sqlQuery = string.Format("delete from TokenManager where Id = {0}", Id);
            using (var cmd = new SqlCommand(sqlQuery, connection))
            {

                cmd.ExecuteNonQuery();
                connection.Close();
                cmd.Dispose();
                connection.Dispose();
            }
        }

        public TokenManager GetToken(string token)
        {
            TokenManager tokenmngobj;
            var constre = ConfigurationManager.ConnectionStrings["connectionStringGeneral"].ToString();
            var connection = new SqlConnection(constre);
            if (connection.State == ConnectionState.Open)
            {
                connection.Close();
            }
           var command = new SqlCommand("SELECT * FROM TokenManager WHERE Token = @token", connection);
            command.Parameters.AddWithValue("@token", token);
            connection.Open();
            var reader = command.ExecuteReader();
            if (!reader.Read())
            {
                connection.Close();
                connection.Dispose();
                reader.Close();
                return null;
            }
            tokenmngobj = new TokenManager
                              {
                                  Id = reader.GetGuid(reader.GetOrdinal("Id")),
                                  TokenClient = new TokenClient { Id = reader.GetGuid(reader.GetOrdinal("TokenClientId")) },
                                  Token = reader.GetString(reader.GetOrdinal("Token")),
                                  RefreshToken = reader.GetString(reader.GetOrdinal("RefreshToken")),
                                  IssueDate = reader.GetDateTime(reader.GetOrdinal("IssueDate")),
                                  ExpireDate = reader.GetDateTime(reader.GetOrdinal("ExpireDate")),
                                  Scope = reader.GetString(reader.GetOrdinal("Scope"))
                              };
            connection.Close();
            connection.Dispose();
            reader.Close();
            return tokenmngobj;
        }
        
    }
}