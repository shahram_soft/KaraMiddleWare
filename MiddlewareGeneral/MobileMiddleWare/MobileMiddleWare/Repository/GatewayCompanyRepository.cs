﻿using System;

namespace MobileMiddleWare.Repository
{
    using System.Configuration;
    using System.Data;
    using System.Data.SqlClient;
    using MobileMiddleWare.Models;

    public class GatewayCompanyRepository
    {
        public GatewayCompany GetGatewayCompany(Guid companyId)
        {
            var constre = ConfigurationManager.ConnectionStrings["connectionStringGeneral"].ToString();
            var connection = new SqlConnection(constre);
            if (connection.State == ConnectionState.Open)
            {
                connection.Close();
            }
            connection.Open();
            using (var cmd = connection.CreateCommand())
            {
                cmd.CommandText = "SELECT * FROM GatewayCompany WHERE Id = @companyId";
                cmd.Parameters.AddWithValue("@companyId", companyId);
              
                using (var reader = cmd.ExecuteReader())
                {
                    if (!reader.Read())
                    {
                        connection.Close();
                        connection.Dispose();
                        return null;
                    }
                    var getwaycompanyobj = new GatewayCompany()
                                                          {
                                                              Id = reader.GetGuid(reader.GetOrdinal("Id")),
                                                              Name = reader.GetString(reader.GetOrdinal("Name")),
                                                              Url = reader.GetString(reader.GetOrdinal("Url"))
                                                          };
                    connection.Close();
                    connection.Dispose();
                    return getwaycompanyobj;
                }
               
            }
        }
    }
}