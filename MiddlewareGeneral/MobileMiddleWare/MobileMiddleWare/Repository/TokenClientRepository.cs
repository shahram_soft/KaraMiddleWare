﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileMiddleWare.Repository
{
    using System.Configuration;
    using System.Data;
    using System.Data.SqlClient;
    using MobileMiddleWare.Models;

    public class TokenClientRepository
    {
       
        public TokenClient GetTokenClient(string clientId)
        {
           
                TokenClient tokenclientobj;
                var commandnitgen = "SELECT * FROM TokenClient WHERE ClientId = @clientId";

               var constr = ConfigurationManager.ConnectionStrings["connectionStringGeneral"].ToString();
                SqlConnection conn = new SqlConnection(constr);
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
                using (SqlCommand cmd = new SqlCommand(commandnitgen, conn))
                {
                    cmd.Parameters.AddWithValue("@clientId", clientId);
                    cmd.CommandType = CommandType.Text;
                    conn.Open();
                    SqlDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection);

                    if (!reader.Read())
                    {
                        conn.Close();
                        conn.Dispose();
                        reader.Close();
                        return null;
                    }
                    tokenclientobj = new TokenClient
                                         {
                                             Id = reader.GetGuid(reader.GetOrdinal("Id")),
                                             ClientId = reader.GetString(reader.GetOrdinal("ClientId")),
                                             ClientSecret =
                                                 reader.GetString(reader.GetOrdinal("ClientSecret")),
                                             DefaultCallback =
                                                 reader.GetString(reader.GetOrdinal("DefaultCallback")),
                                             Tenant = reader.GetString(reader.GetOrdinal("Tenant")),
                                             CreateDate =
                                                 reader.GetDateTime(reader.GetOrdinal("CreateDate"))
                                         };
                    conn.Close();
                    conn.Dispose();
                    reader.Close();
                    return tokenclientobj;
                }
        }
    }
}