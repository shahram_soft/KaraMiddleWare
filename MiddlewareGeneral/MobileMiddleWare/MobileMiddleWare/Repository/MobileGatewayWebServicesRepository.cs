﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace MobileMiddleWare.Repository
{
    using System.Configuration;
    using System.Data;
    using System.Data.SqlClient;

    using MobileMiddleWare.Models;

    public class MobileGatewayWebServicesRepository
    {
       public MobileGatewayWebServices GetMobileGatewayWebServices(string command)
        {
                var constre = ConfigurationManager.ConnectionStrings["connectionStringGeneral"].ToString();
                var connection = new SqlConnection(constre);
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
                connection.Open();
           using (var cmd = connection.CreateCommand())
           {
               cmd.CommandText = "SELECT * FROM MobileGatewayWebServices WHERE Command = @command";
               cmd.Parameters.AddWithValue("@command", command);
               using (var reader = cmd.ExecuteReader())
               {
                   if (!reader.Read())
                   {
                       return null;
                   }
                   return new MobileGatewayWebServices
                              {
                                  Id = reader.GetGuid(reader.GetOrdinal("Id")),
                                  Command = reader.GetString(reader.GetOrdinal("Command")),
                                  HasInArg = reader.GetString(reader.GetOrdinal("HasInArg")),
                                  ServiceTagName =
                                      reader.GetString(reader.GetOrdinal("ServiceTagName")),
                                  ResultTagName =
                                      reader.GetString(reader.GetOrdinal("ResultTagName")),
                                  ServiceType =
                                      reader.GetString(reader.GetOrdinal("ServiceType"))
                              };
               }
           }

        }

        public List<MobileGatewayWebServicesParameters> GetMobileGatewayWebServicesParameters(Guid MobileGatewayWebServicesRepositoryId)
        {
            DataTable dataTable = new DataTable();
            List<MobileGatewayWebServicesParameters> lstMobileGatewayWebServicesParameters;
            var constre = ConfigurationManager.ConnectionStrings["connectionStringGeneral"].ToString();
            var connection = new SqlConnection(constre);
            if (connection.State == ConnectionState.Open)
            {
                connection.Close();
            }
            connection.Open();
            using (var cmd = connection.CreateCommand())
            {
                cmd.CommandText = "SELECT * FROM MobileGatewayWebServicesParameters WHERE WebServiceId = @Id";
                cmd.Parameters.AddWithValue("@Id", MobileGatewayWebServicesRepositoryId);
                // create data adapter
                var da = new SqlDataAdapter(cmd);
                da.Fill(dataTable);
                connection.Close();
                connection.Dispose();
                da.Dispose();
               
                lstMobileGatewayWebServicesParameters = (from DataRow dr in dataTable.Rows
                                                         select new MobileGatewayWebServicesParameters()
                               {
                                   Id = (Guid)dr["Id"],
                                   ParameterName = dr["ParameterName"].ToString(),
                                   WebServiceId = dr["WebServiceId"].ToString(),
                                   ParameterType = dr["ParameterType"].ToString()
                               }).ToList();
                return lstMobileGatewayWebServicesParameters;
            }
        }
    }
}