﻿using System.Collections.Generic;
using System.Web.Services;
using Interface;
using Interface.ViewModel;
using MobileMiddleWare.Kernel_Manager;
using Ninject;


namespace MobileMiddleWare.SoapWebServices
{
    /// <summary>
    /// Summary description for MobileService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class MobileService : System.Web.Services.WebService
    {
        public IWebServiceMethods WebServiceMethods { get; set; }

        public MobileService()
        {
            var kernel = KernelFactory.GetInstance;
            WebServiceMethods = kernel.Get<IWebServiceMethods>();

        }

        [WebMethod]
        public string LoginMobile(string PersonCode, string ComputerName, string UserSessionID, string Language)
        {
            var result = WebServiceMethods.LoginMobile(PersonCode, ComputerName, UserSessionID, Language);
            return result;
        }

        [WebMethod]
        public string GetReserveFoodHeader(string Date, int PersonID, string PersonelCode, int FoodTerm,
            int CompanyFinatialPeriodID, int SessionID, string Language)
        {
            var result = WebServiceMethods.GetReserveFoodHeader(Date, PersonID, PersonelCode, FoodTerm,
                CompanyFinatialPeriodID, SessionID, Language);
            return result;
        }

        [WebMethod]
        public string GetReserveFood(string SDate, string EDate, int FoodTerm, int ResturantNO, string Code, int Flag,
            string CurrentDate, int CompanyFinatialPeriodID, int SessionID, string Language)
        {
            var result = WebServiceMethods.GetReserveFood(SDate, EDate, FoodTerm, ResturantNO, Code, Flag,
                CurrentDate, CompanyFinatialPeriodID, SessionID, Language);
            return result;
        }

        [WebMethod]
        public string ModifyFoodReserve(string Code, string XmlSave, string HostAddress, int CompanyFinatialPeriodID,
            int SessionID, int OnlineUser, string Language)
        {
            var result = WebServiceMethods.ModifyFoodReserve(Code, XmlSave, HostAddress, CompanyFinatialPeriodID,
                SessionID, OnlineUser, Language);
            return result;
        }


        private int validate(string Username, string Password)
        {
            var result = WebServiceMethods.validate(Username, Password);
            return result;
        }



        [WebMethod]
        public string Login_Mobile(string UserName, string Password, string DeviceUniqueID)
        {
            var result = WebServiceMethods.Login_Mobile(UserName, Password, DeviceUniqueID);
            return result;
        }

        [WebMethod]
        public string GetWorkPeriod_Mobile()
        {
            var result = WebServiceMethods.GetWorkPeriod_Mobile();
            return result;
        }

        [WebMethod]
        internal List<GetCodePermission_MobileModel> GetCodePermission_Mobile(int GroupCodeID, bool IsDaily,
            bool IsTimely, int PageID, int OnlineUserID)
        {
            var result = WebServiceMethods.GetCodePermission_Mobile(GroupCodeID, IsDaily, IsTimely, PageID, OnlineUserID);
            return result;
        }

        [WebMethod]
        public string GetWorkPeriodDates_Mobile(int PersonID, int WPID, int SessionID)
        {
            var result = WebServiceMethods.GetWorkPeriodDates_Mobile(PersonID, WPID, SessionID);
            return result;
        }

        [WebMethod]
        public string GetPersonWPDateResult_Mobile(int PersonID, int WPID, string Date, int SessionID)
        {
            var result = WebServiceMethods.GetPersonWPDateResult_Mobile(PersonID, WPID, Date, SessionID);
            return result;
        }

        [WebMethod]
        public string GetPersonDateAttendances_Mobile(int PersonID, string Date, int SessionID)
        {
            var result = WebServiceMethods.GetPersonDateAttendances_Mobile(PersonID, Date, SessionID);
            return result;
        }

        [WebMethod]
        public string ModifyAttendance_Mobile(int OnlineUserID, string PersonCode, string Time, string Date,
            int AttendanceID, int Type, int Delete, string Description, int SessionID)
        {
            var result = WebServiceMethods.ModifyAttendance_Mobile(OnlineUserID, PersonCode, Time, Date,
                AttendanceID, Type, Delete, Description, SessionID);
            return result;
        }


        [WebMethod]
        public string ModifyCredit_Mobile(int Type, string CreditType, int Daily, string StartDate, string EndDate,
            string StartTime, string EndTime, int PersonCode, int JPersonCode, int CreditID, string Description,
            int OnlineUserID, int SessionID)
        {
            var result = WebServiceMethods.ModifyCredit_Mobile(Type, CreditType, Daily, StartDate, EndDate,
                StartTime, EndTime, PersonCode, JPersonCode, CreditID, Description,
                OnlineUserID, SessionID);
            return result;
        }

        [WebMethod]
        public string GetEnterCredit_Mobile(int PersonID, int WPID, string Date, string Code, string Type, int SessionID)
        {
            var result = WebServiceMethods.GetEnterCredit_Mobile(PersonID, WPID, Date, Code, Type, SessionID);
            return result;
        }


        [WebMethod]
        public string GetPersonWorkPeriodResult_Mobile(int PersonID, int WPID, int SessionID)
        {
            var result = WebServiceMethods.GetPersonWorkPeriodResult_Mobile(PersonID, WPID, SessionID);
            return result;
        }

        [WebMethod(Description = "نمايش كارتابل ")]
        public string GetNewCartabl_Mobile(string CartablOwnerCode, string CartablOwnerID, string StartDate,
            string EndDate, string Requester, string DocType, string DepartmentID, string GroupID, string StrFilter,
            string PageSize, string PageNumber, int Sessionid, string Language)
        {
            var result = WebServiceMethods.GetNewCartabl_Mobile(CartablOwnerCode, CartablOwnerID, StartDate,
                EndDate, Requester, DocType, DepartmentID, GroupID, StrFilter,
                PageSize, PageNumber, Sessionid, Language);
            return result;
        }

        [WebMethod(Description = "روند گردش درخواستها")]
        public string WfGetLog_Mobile(int DocId, int DocTypeID, string Language)
        {
            var result = WebServiceMethods.WfGetLog_Mobile(DocId, DocTypeID, Language);
            return result;
        }

        [WebMethod(Description = "تاييد و عدم تاييد کارتابل ")]
        public string ModifyCasingWorkTable_Mobile(string DoX, int sessionId, string Language)
        {
            var result = WebServiceMethods.ModifyCasingWorkTable_Mobile(DoX, sessionId, Language);
            return result;
        }

        [WebMethod(Description = "ارجاع کارتابل ")]
        public string ModifyWorkTable_Mobile(string DoX, int sessionId, string Language)
        {
            var result = WebServiceMethods.ModifyWorkTable_Mobile(DoX, sessionId, Language);
            return result;
        }

        [WebMethod(Description = "دسترسی به دکمه ها ")]
        public string GetAccessForCartabl_Mobile(int SessionId)
        {
            var result = WebServiceMethods.GetAccessForCartabl_Mobile(SessionId);
            return result;
        }

        [WebMethod(Description = "نوع سند")]
        public string GetWfDocType_Mobile(string OnlineUserID)
        {

            var result = WebServiceMethods.GetWfDocType_Mobile(OnlineUserID);
            return result;

        }

        [WebMethod(Description = "نمايش کاردکس ")]
        public string GetCardexReport_Mobile(string Language, string PersonCode, string WPid, string cardexID,
            string CompanyID, string SessionID)
        {
            var result = WebServiceMethods.GetCardexReport_Mobile(Language, PersonCode, WPid, cardexID,
                CompanyID, SessionID);
            return result;
        }

        [WebMethod(Description = "نمایش ریز کاردکس")]
        public string GetCardexRestDetail_Mobile(string PersonCode, string WPid, string cardexID, string PageSize,
            string PageNumber)
        {
            var result = WebServiceMethods.GetCardexRestDetail_Mobile(PersonCode, WPid, cardexID, PageSize,
                PageNumber);
            return result;
        }

        [WebMethod(Description = "نمایش مجوزها برای موبایل")]
        public string GetDocInfo_Mobile(string SessionID, string OnlineUserID, string PersonCode, string WpID,
            string StartDate, string EndDate, string DocType, string DocState, string PageSize, string PageNumber)
        {
            var result = WebServiceMethods.GetDocInfo_Mobile(SessionID, OnlineUserID, PersonCode, WpID,
                StartDate, EndDate, DocType, DocState, PageSize, PageNumber);
            return result;
        }

        [WebMethod(Description = "جستجوی افراد زیرمجموعه ")]
        public string GetOnlineUserPersonelAccess_Mobile(string SessionID, string OnlineUserID, string PersonName,
            string pagesize, string pagenumber)
        {
            var result = WebServiceMethods.GetOnlineUserPersonelAccess_Mobile(SessionID, OnlineUserID, PersonName,
                pagesize, pagenumber);
            return result;
        }




        [WebMethod(Description = "گرفتن لیست کارتابل")]
        public string GetCartableFilterValue_Mobile(string SessionID, string OnlineUserID)
        {
            var result = WebServiceMethods.GetCartableFilterValue_Mobile(SessionID, OnlineUserID);
            return result;
        }



        [WebMethod(Description = "گرقتن نوع سند های پیشرفته")]
        public string GetExtendedNWFDoc_Mobile(string SessionID, string OnlineUserID, string DocTypeID)
        {
            var result = WebServiceMethods.GetExtendedNWFDoc_Mobile(SessionID, OnlineUserID, DocTypeID);
            return result;
        }




        [WebMethod(Description = "گرفتن نوتیفیکیشن های یک فرد")]
        public string GetPersonNotification_Mobile(string GetPersonNotification_Mobile)
        {
            var result = WebServiceMethods.GetPersonNotification_Mobile(GetPersonNotification_Mobile);
            return result;
        }



        [WebMethod(Description = "گرفتن دسترسی های یک فرد")]
        public string usbGetAccess_Mobile(string PersonID, string SessionID)
        {
            var result = WebServiceMethods.usbGetAccess_Mobile(PersonID, SessionID);
            return result;
        }


        [WebMethod(Description = "گرفتن دسترسی های  ثبت مجوز و تردد برای افراد زیر مجموعه")]
        public string usbSurveyAccess_Mobile(string PersonID)
        {
            var result = WebServiceMethods.usbSurveyAccess_Mobile(PersonID);
            return result;
        }

        [WebMethod(Description = "تغییر رمز عبور ")]
        public string ChangePassword_Mobile(string PersonID, string UserName, string OldPassword, string NewPassword)
        {
            var result = WebServiceMethods.ChangePassword_Mobile(PersonID, UserName, OldPassword, NewPassword);
            return result;
        }



        [WebMethod(Description = "به روزرسانی وضعیت نوتیفیکیشن ها")]
        public string UpdateNotificationStatus_Mobile(string ID)
        {
            var result = WebServiceMethods.UpdateNotificationStatus_Mobile(ID);
            return result;
        }


        [WebMethod(Description = "گرفتن مجوزهای یک فرد برای ثبت در مجوزهای دسته ای")]
        public string GetEnterCreditByWorkPeriod_Mobile(string PersonID, string SessionID, string WPID, string Code)
        {
            var result = WebServiceMethods.GetEnterCreditByWorkPeriod_Mobile(PersonID, SessionID, WPID, Code);
            return result;
        }




        [WebMethod(Description = "ثبت مجوز دسته ای")]
        public string ModifyGroupCredit_Mobile(string DocX, string OnlineUserID, string SessionID, string CreditID)
        {
            var result = WebServiceMethods.ModifyGroupCredit_Mobile(DocX, OnlineUserID, SessionID, CreditID);
            return result;
        }

        [WebMethod(Description = "گرفتن ورژن وب سرویس ها ")]
        public string GetWebServiceVersion_Mobile()
        {
            var result = WebServiceMethods.GetWebServiceVersion_Mobile();
            return result;
        }

        private static string txtAuthMode = "";

        private void CheckAdOption()
        {
            //return "";
        }



        [WebMethod(Description = " حذف مجوز از لیست مجوز های فرد زیر مجموعه ")]
        public string DeleteDocInfo_Mobile(string DocID, string DocTypeID, string DeleteDesc, string SessionID,
            string OnlineUserID)
        {
            var result = WebServiceMethods.DeleteDocInfo_Mobile(DocID, DocTypeID, DeleteDesc, SessionID,
                OnlineUserID);
            return result;
        }



        [WebMethod(Description = " گرفتن تفویض های کارتابل یک فرد ")]
        public string GetPersonShareWorkTable_Mobile(string OnlineUserID, string SessionID)
        {
            var result = WebServiceMethods.GetPersonShareWorkTable_Mobile(OnlineUserID, SessionID);
            return result;
        }




        [WebMethod(Description = " تایید همه ی آیتم های کارتابل")]
        public string ModifyWorkTableChkShAll_Mobile(string actorpid, string CartablOwnerCode, string StartDate,
            string EndDate, string Requester, string DocType, string StrFilter, string PageNumber, string SessionID)
        {
            var result = WebServiceMethods.ModifyWorkTableChkShAll_Mobile(actorpid, CartablOwnerCode, StartDate,
                EndDate, Requester, DocType, StrFilter, PageNumber, SessionID);
            return result;
        }


        [WebMethod(Description = " ثبت تردد های فرد با پاستیل ")]
        public string InsertAttendance_Mobile(string Time, string Date, int PersonelID, int CardKhanNo)
        {


            var result = WebServiceMethods.InsertAttendance_Mobile(Time, Date, PersonelID, CardKhanNo);
            return result;
        }



        [WebMethod(Description = "نمایش تردد های فرد با پاستیل")]
        public string GetAttendancePastil_Mobile(string Date, int PersonelID)
        {
            var result = WebServiceMethods.GetAttendancePastil_Mobile(Date, PersonelID);
            return result;
        }


        [WebMethod(Description = "گرفتن ترجمه ی کلمات")]
        public string GetTranslate_Mobile(string LanguageKey)
        {
            var result = WebServiceMethods.GetTranslate_Mobile(LanguageKey);
            return result;
        }


        [WebMethod(Description = "نوع مجوز ها به صورت داینامیک")]
        public string GetCreditType_Mobile(int OnlineUserID)
        {
            var result = WebServiceMethods.GetCreditType_Mobile(OnlineUserID);
            return result;
        }


        [WebMethod(Description = "گرفتن عکس های راهنمای موبایل")]
        public string GetHelpImage_Mobile(string Version, string Language)
        {
            var result = WebServiceMethods.GetHelpImage_Mobile(Version, Language);
            return result;
        }

        [WebMethod(Description = "وضعیت سند")]
        public string DocumentState_Mobile()
        {
            var result = WebServiceMethods.DocumentState_Mobile();
            return result;


        }


        [WebMethod(Description = "وب سرویس فرم ساز")]
        public string GetFormFields_Mobile(string CodeID, string AutoCompleteParams, string SessionID,string personCode,string callFrom)
        {
            var result = WebServiceMethods.GetFormFields_Mobile(CodeID, AutoCompleteParams, SessionID,personCode,callFrom);
            return result;
        }
        [WebMethod(Description = "وب سرویس فرم ساز ویرایش")]
        public string GetFormFieldsEdit_Mobile(string requestId, string SessionID)
        {
            var result = WebServiceMethods.GetFormFieldsEdit_Mobile(requestId, SessionID);
            return result;
        }

        [WebMethod(Description = "گرفتن لوکیشن های یک فرد")]
        public string GetLocationsPastil_Mobile(string GetLocationsPastil_Mobile)
        {
            var result = WebServiceMethods.GetLocationsPastil_Mobile(GetLocationsPastil_Mobile);
            return result;
        }



        [WebMethod(Description = "ثبت مجوز یک فرد با فرم ساز")]
        public string ModifyCreditByFormBuilder_Mobile(int PersonCode, int JPersonCode, string CreditID,
            int OnlineUserID,
            int SessionID, string FormJson, string XmlRoot)
        {
            var result = WebServiceMethods.ModifyCreditByFormBuilder_Mobile(PersonCode, JPersonCode, CreditID,
                OnlineUserID,
                SessionID, FormJson, XmlRoot);
            return result;
        }


        [WebMethod(Description = "ثبت مجوز دسته ای توسط فرم ساز")]
        public string ModifyGroupCreditByFormBuilder_Mobile(string FormJson, string OnlineUserID, string SessionID,
            string CreditID, string xmlRoot)
        {
            var result = WebServiceMethods.ModifyGroupCreditByFormBuilder_Mobile(FormJson, OnlineUserID, SessionID,
                CreditID, xmlRoot);
            return result;
        }

        [WebMethod(Description = "ورود فرد با همستر")]
        public string LoginHamster_Mobile(string HashId)
        {
            var result = WebServiceMethods.LoginHamster_Mobile(HashId);
            return result;
        }

        [WebMethod(Description = "کومبوی پرسنلی")]
        public string PersonAutoCompleteCombo_Mobile(string onlineUserId,string filterStr)
        {
            var result = WebServiceMethods.PersonAutoCompleteCombo(onlineUserId,filterStr);
            return result;
        }
        [WebMethod(Description = "حذف دسته ای مجوز ها")]
        public string DeleteCreditsBatch_Mobile(string onlineUserId, string data)
        {
            var result = WebServiceMethods.DeleteCreditsBatch_Mobile(onlineUserId, data);
            return result;
        }
        [WebMethod(Description = "ویرایش مجوز")]
        public string ModifyEditCreditByFormBuilder_Mobile(int PersonCode, int JPersonCode, string requestId,
            int OnlineUserID,
            int SessionID,
            string FormJson, string XmlRoot)
        {
             var result = WebServiceMethods.ModifyEditCreditByFormBuilder_Mobile( PersonCode,  JPersonCode,  requestId,
             OnlineUserID,
             SessionID,
             FormJson,  XmlRoot);
            return result;
        }
    }
}
