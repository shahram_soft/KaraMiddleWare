﻿using System;
using System.Threading.Tasks;
using Microsoft.Owin;
using Owin;
using Microsoft.AspNet.SignalR;

[assembly: OwinStartup(typeof(MobileMiddleWare.Startup))]

namespace MobileMiddleWare
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            // For more information on how to configure your application, visit http://go.microsoft.com/fwlink/?LinkID=316888
            app.MapSignalR(new HubConfiguration { EnableJSONP=true,EnableJavaScriptProxies=true});
        }
    }
}
