﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ninject;

namespace MobileMiddleWare.Kernel_Manager
{
    public class KernelFactory
    {
        private static StandardKernel kernel = null;
        private static object lockMe = new object();
        private KernelFactory()
        {

        }

        public static StandardKernel GetInstance
        {
            get
            {

                if (kernel == null)
                {
                    lock (lockMe)
                    {
                        if (kernel == null)
                        {
                            kernel = new StandardKernel();
                        }

                    }
                }
                return kernel;
            }
        }


    }
}