﻿
namespace MobileMiddleWare.ViewModel
{
    /// <summary>
    /// ویو مدلی جهت سریالایز کردن و دیسریالاید کردن برای لایه سرویس خارجی
    /// </summary>
    public class ExternalServiceViewModel
    {
        /// <summary>
        /// توکن جهت مجوز ورود
        /// </summary>
        public string Token { get; set; }

        /// <summary>
        /// تاریخ ایجاد توکن
        /// </summary>
        public string TokenCreateTime { get; set; }

        /// <summary>
        /// تاریخ انقضا
        /// </summary>
        public string TokenExpireTime { get; set; }

        /// <summary>
        /// سرویس مورد صدا زدن
        /// </summary>
        public string Call { get; set; }

        /// <summary>
        /// ماژول مورد نظر
        /// </summary>
        public string Module { get; set; }

        /// <summary>
        ///متد مورد نظر
        /// </summary>
        public string Method { get; set; }

        /// <summary>
        /// پارامتر های سرویس مورد نظر
        /// </summary>
        public string Parameter { get; set; }
        /// <summary>
        /// نام اکشن 
        /// </summary>
        public string ActionName { get; set; }
        /// <summary>
        /// ادرس کلی
        /// </summary>
        public string FullServiceUrl { get; set; }
    }
}